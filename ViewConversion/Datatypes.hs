{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}

module Datatypes where

--------------------------------------------------------------------------------
-- Test datatype: Maybe Int
data MaybeInt = JustInt Int | NothingInt deriving (Show)

-- Test datatype: Peano naturals
data Nat = Zero | Succ Nat deriving (Show)

-- The AST type from the Multirec paper
infix 1 :=

data Expr   =  Const  Int
            |  Add    Expr  Expr
            |  Mul    Expr  Expr
            |  EVar   Var
            |  Let    Decl  Expr
  deriving Show

data Decl   =  Var := Expr
            |  Seq    Decl  Decl
            |  None
  deriving Show

type Var   =  String
