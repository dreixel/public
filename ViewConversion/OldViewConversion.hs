{-# OPTIONS_GHC -fglasgow-exts    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

module Test where

import Generics.Regular.Rewriting
import Debug.Trace

deriving instance (Show (f a r), Show (f b r)) => Show ((f a :+: f b) r)
deriving instance (Show (f a r), Show (f b r)) => Show ((f a :*: f b) r)
deriving instance (Show (f a)) => Show (K (f a) r)
deriving instance (Show r) => Show (Id r)

-- Type and value level naturals
data Nat = Succ Nat | Zero deriving (Show)

data Zero
data Succ n

class FromNat n where
  fromNat :: n -> Nat
  
instance FromNat Zero where
  fromNat _ = Zero
  
instance (FromNat n) => FromNat (Succ n) where
  fromNat _ = Succ (fromNat (undefined :: n))

-- Type level lists
data Nil
data Last (x :: * -> *)
data Cons (h :: * -> *) (t :: *)

class R1C (t :: * -> *) where
  r1c :: t a -> R1 t a

instance R1C (a :+: b) where
  -- :: (f :+: g) a -> R1 (f :+: g) a ~ R2 (f :+: g) Nil a
  r1c t = r2c t

instance R1C (a :*: b) where
  r1c = undefined

type family R1 (t :: * -> *) :: * -> *
type instance R1 (a :+: b) = R2 (a :+: b) Nil
-- If there is no sum then we have nothing to do
type instance R1 (a :*: b) = (a :*: b)
type instance R1 (Con c)   = (Con c)
type instance R1 (K x)     = (K x)
type instance R1 Id        = Id
type instance R1 Unit      = Unit

-- Build up a stack of sum elements
class R2C (t :: * -> *) (l :: *) where
  r2c :: t a -> R2 t l a
  
instance R2C (a :+: b) l where
  r2c = undefined

type family R2 (t :: * -> *) (l :: *) :: * -> *
type instance R2 (a :+: b) l = R2 b (Cons a l)
-- Last element
type instance R2 (a :*: b) l = R3 (a :*: b) l
type instance R2 (K x)     l = R3 (K x) l
type instance R2 Id        l = R3 Id l
type instance R2 Unit      l = R3 Unit l
type instance R2 (Con c)   l = R3 (Con c) l

-- Basically a foldl of (:+:)
class R3C (t :: * -> *) (l :: *) where
  r3c :: t a -> R3 t l a

instance R3C z Nil where
  r3c = id
  
instance R3C z (Cons h t) where
  r3c t = undefined

type family R3 (z :: * -> *) (l :: *) :: * -> *
type instance R3 z Nil        = z
type instance R3 z (Cons h t) = R3 (z :+: h) t

test :: R1 (PF (Tree () ())) (Tree () ())
test = R (K ()) -- works!

-- Count the number of constructors (on a right-nested view)
type family NrCon (t :: * -> *) :: *
type instance NrCon (a :+: b) = Succ (NrCon b)
type instance NrCon (a :*: b) = Succ Zero
type instance NrCon (Con c)   = Succ Zero
type instance NrCon (K x)     = Succ Zero
type instance NrCon Id        = Succ Zero
type instance NrCon Unit      = Succ Zero
{-
class RegularL f a where
  fromL :: Nat -> Nat -> f a -> R1 f a
  --toL :: R1 (PF a) a -> a

instance (RegularL f a, RegularL g a) => RegularL (f :+: g) a where
  --fromL :: (f :+: g) a -> R1 (PF a) a
  fromL _ _ (L x) = undefined
  fromL t c (R x) = undefined -- fromL t (Succ c) x

instance RegularL (f :*: g) a where
  fromL _ _ x = x
  
instance RegularL Unit a where
  fromL _ _ x = x
  
instance RegularL Id a where
  --fromL x = x

instance RegularL (K x) a where
  fromL _ _ x = x

t :: (Regular a, RegularL (PF a) a) =>  a -> R1 (PF a) a
t = undefined --fromL . from
-}


data X = Inl X | Inr X | S deriving Show

conv1 :: X -> X
conv1 (Inl S) = Inr S
conv1 (Inr S) = Inl S
conv1 x = conv 1 x where
  conv :: Int -> X -> X
  conv n (Inl x) = conv' n x
  conv n (Inr x) = conv (n+1) x
  conv _ x       = Inr x
  
  conv' :: Int -> X -> X
  conv' 0 x = x
  conv' n x = Inl (conv' (n-1) x)


{-
data Spine :: * -> * where
  C :: ConInfo a -> Spine a
  (:<>:) :: Spine (a -> b) -> Typed a -> Spine b

infixl 0 :<>:

data ConInfo a = ConInfo { conName :: String, conVal :: a }
data Typed a = (:>:) { val :: a, rep :: Type a }

data Type :: * -> * where
  Int :: Type Int
  Maybe :: Type a -> Type (Maybe a)
  List :: Type a -> Type [a]
  Expr :: Type Expr

toSpine :: Type a -> a -> Spine a
toSpine Int x = C undefined
toSpine (Maybe a) Nothing = C undefined
toSpine (Maybe a) (Just x) = C undefined :<>: x :>: a
toSpine (List a) [] = C undefined
toSpine (List a) (x:xs) = C undefined :<>: x :>: a :<>: xs :>: List a
--toSpine Int x = C (undefined x)
-}
{-
type family S1 (t :: * -> *) :: *
type instance S1 (a :+: b) = S1 b
type instance S1 (Con c) = S1 c
type instance S1 (a :*: b) = 
-}
-------------------------

infixl 7 :**:
infixl 6 :++:

data Expr = Const Int | Expr :++: Expr | Expr :**: Expr deriving Show

instance Regular Expr where
  -- Right-nested
  type PF Expr = K Int :+: Id :*: Id :+: Id :*: Id

  from (Const n)    = L (K n)
  from (e1 :++: e2) = R (L  $ (Id e1) :*: (Id e2))
  from (e1 :**: e2) = R (R  $ (Id e1) :*: (Id e2))

  to (L (K n))                     = Const n
  to (R (L ((Id r1) :*: (Id r2)))) = r1 :++: r2
  to (R (R ((Id r1) :*: (Id r2)))) = r1 :**: r2
  
data Tree a b = Tip b | Bin (Tree a b) a (Tree a b) deriving Show

instance Regular (Tree a b) where
  -- Right-nested
  type PF (Tree a b) = K b :+: Id :*: K a :*: Id

  from (Tip x)       = L (K x)
  from (Bin t1 x t2) = R (Id t1 :*: K x :*: Id t2)

  to (L (K x))                     = Tip x
  to (R (Id t1 :*: K x :*: Id t2)) = Bin t1 x t2

{-
 -- with Con type constructors to specify constructor names.
instance Regular Expr where
  type PF Expr = Con (K Int) :+: Con (Id :*: Id) :+: Con (Id :*: Id)

  from (Const n)    = L (Con "Const" (K n))
  from (e1 :++: e2) = R (L (Con "(:++:)" $ (Id e1) :*: (Id e2)))
  from (e1 :**: e2) = R (R (Con "(:**:)" $ (Id e1) :*: (Id e2)))

  to (L (Con _ (K n)))                     = Const n
  to (R (L (Con _ ((Id r1) :*: (Id r2))))) = r1 :++: r2
  to (R (R (Con _ ((Id r1) :*: (Id r2))))) = r1 :**: r2
-}

instance Rewrite Expr

class Nest f where
  fnest :: f a -> (Int,Int)

instance Nest Id where
  fnest _ = (0,0)

instance Nest (K a) where
  fnest _ = (0,0)

instance Nest Unit where
  fnest _ = (0,0)

instance (Nest f, Nest g) => Nest (f :+: g)  where
  fnest (L x) = let (a,b) = fnest x in (a+1,b)
  fnest (R x) = let (a,b) = fnest x in (a,b+1)

instance Nest (f :*: g)  where
  fnest _ = (0,0)

instance Nest (Con f) where
  fnest _ = (0,0)

nest :: (Regular a, Nest (PF a)) => a -> (Int,Int)
nest = fnest . from
{-
nest' :: (Regular a, Nest (R1 (PF a))) => a -> (Int,Int)
nest' = fnest . from -- should be some from'!
-}
{-
test1' :: Expr
test1' = to (leftf left)

test1 :: (Regular a, Nest (R1 (PF a)), a ~ Expr) => a
test1 = to (leftf left)

test2' :: Expr
test2' = to (rightf right)

test2 :: (Regular a, Nest (R1 (PF a)), a ~ Expr) => a
test2 = to (rightf right)
-}
