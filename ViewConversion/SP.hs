{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}

module SP where

import qualified Datatypes as D

--------------------------------------------------------------------------------
-- SP representations
data U = U deriving (Show)
data Rec a = Rec a deriving (Show)
data Var a = Var a deriving (Show) -- currently the meaning of Var is undefined
data Plus a b = L a | R b deriving (Show)
data Times a b = Times a b deriving (Show)

--------------------------------------------------------------------------------
-- Test datatype: Maybe Int
type MaybeInt = Plus Int U

-- Test datatype: Peano naturals
type Nat = Plus U (Rec D.Nat)

-- The AST type from the Multirec paper
type Decl = Plus (Times D.Var D.Expr) (Plus (Times (Rec D.Decl) (Rec D.Decl)) U)
