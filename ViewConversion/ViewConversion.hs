{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}
-- {-# LANGUAGE UndecidableInstances   #-}

module ViewConversion where

import qualified SP as S
import qualified Regular as R
import qualified Multirec as M

--------------------------------------------------------------------------------
-- Multirec to Regular conversion
class M2RC (f :: (* -> *) -> * -> *) (r :: * -> *) ix where
  type M2R f r ix :: * -> *
  fromM :: f r ix -> M2R f r ix ix

instance M2RC M.U r ix where
  type M2R M.U r ix = R.U
  fromM M.U = R.U

instance M2RC (M.K a) r ix where
  type M2R (M.K a) r ix = R.K a
  fromM (M.K x) = R.K x
  
instance (r xi ~ ix) => M2RC (M.I xi) r ix where
  type M2R (M.I xi) r ix = R.I
  fromM (M.I r) = R.I r
{-
-- Conflicting family instance declarations...
instance () => M2RC (M.I xi) r ix where
  type M2R (M.I xi) r ix = R.K xi
  fromM (M.I r) = R.K r
-}
instance (M2RC f r ix, M2RC g r ix) => M2RC (M.Plus f g) r ix where
  type M2R (M.Plus f g) r ix = R.Plus (M2R f r ix) (M2R g r ix)
  fromM (M.L x) = R.L (fromM x)
  fromM (M.R x) = R.R (fromM x)
  
instance (M2RC f r ix, M2RC g r ix) => M2RC (M.Times f g) r ix where
  type M2R (M.Times f g) r ix = R.Times (M2R f r ix) (M2R g r ix)
  fromM (M.Times x y) = R.Times (fromM x) (fromM y)
  
instance (xi ~ ix, M2RC f r ix) => M2RC (M.Tag f xi) r ix where
  type M2R (M.Tag f xi) r ix = M2R f r ix
  fromM (M.Tag f) = fromM f

--------------------------------------------------------------------------------
-- Regular to SP conversion
type family R2S (f :: * -> *) r :: *
type instance R2S R.U r = S.U
type instance R2S (R.K a) r = a
type instance R2S R.I r = S.Rec r
type instance R2S (R.Plus f g) r = S.Plus (R2S f r) (R2S g r)
type instance R2S (R.Times f g) r = S.Times (R2S f r) (R2S g r)

class R2SC (f :: * -> *) (r :: *) where
  fromR :: f r -> R2S f r

instance R2SC R.U r where
  fromR R.U = S.U

instance R2SC (R.K a) r where
  fromR (R.K x) = x
  
instance R2SC R.I r where
  fromR (R.I r) = S.Rec r
  
instance (R2SC f r, R2SC g r) => R2SC (R.Plus f g) r where
  fromR (R.L x) = S.L (fromR x)
  fromR (R.R x) = S.R (fromR x)

instance (R2SC f r, R2SC g r) => R2SC (R.Times f g) r where
  fromR (R.Times x y) = S.Times (fromR x) (fromR y)
  
--------------------------------------------------------------------------------
-- Multirec to SP conversion (through Regular)
type M2S f r ix = R2S (M2R f r ix) ix
