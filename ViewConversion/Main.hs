{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}

module Main where

import qualified SP as S
import qualified Regular as R
import qualified Multirec as M
import Datatypes
import ViewConversion

--------------------------------------------------------------------------------
-- Tests
data a :=: b where Refl :: a :=: a

-- Test MaybeInt conversion
type IdR2SMaybeInt = S.MaybeInt :=: R2S R.MaybeInt MaybeInt
idR2SMaybeInt :: IdR2SMaybeInt
idR2SMaybeInt = Refl

type IdM2RMaybeInt =    R.MaybeInt MaybeInt 
                    :=: M2R M.MaybeInt M.MaybeIntW MaybeInt MaybeInt
idM2RMaybeInt :: IdM2RMaybeInt
idM2RMaybeInt = Refl

-- Test Nat conversion
type IdR2SNat = S.Nat :=: R2S R.Nat Nat
idR2SNat :: IdR2SNat
idR2SNat = Refl

type IdM2RNat = R.Nat Nat :=: M2R M.Nat M.NatW Nat Nat
idM2RNat :: IdM2RNat
idM2RNat = Refl

-- Test Decl (from AST) conversion
type IdR2SDecl = S.Decl :=: R2S R.Decl Decl
idR2SDecl :: IdR2SDecl
idR2SDecl = Refl

-- This is not yet possible since I'm unable to distinguish recursion into the 
-- same type from recursion into a new type in Multirec
type IdM2RDecl = R.Decl Decl :=: M2R M.AST M.ASTW Decl Decl
idM2RDecl :: IdM2RDecl
idM2RDecl = undefined -- Refl
