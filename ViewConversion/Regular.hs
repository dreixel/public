{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}

module Regular where

import qualified Datatypes as D

--------------------------------------------------------------------------------
-- Regular representations
data U r = U deriving (Show)
data I r = I r deriving (Show)
data K a r = K a deriving (Show)
data Plus f g r = L (f r) | R (g r) deriving (Show)
data Times f g r = Times (f r) (g r) deriving (Show)
  
--------------------------------------------------------------------------------
-- Test datatype: Maybe Int
type MaybeInt = Plus (K Int) U

-- Test datatype: Peano naturals
type Nat = Plus U I

-- The AST type from the Multirec paper
type Decl = Plus (Times (K D.Var) (K D.Expr)) (Plus (Times I I) U)
