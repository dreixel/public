{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}

module Multirec where

import qualified Datatypes as D

--------------------------------------------------------------------------------
-- Multirec representations
data U r ix = U deriving (Show)
data I xi r ix = I (r xi) deriving (Show)
data K a r ix = K a deriving (Show)
data Plus f g (r :: * -> *) ix = L (f r ix) | R (g r ix) deriving (Show)
data Times f g (r :: * -> *) ix = Times (f r ix) (g r ix) deriving (Show)
data Tag f ix :: (* -> *) -> * -> * where Tag :: f r ix -> (Tag f ix) r ix

--------------------------------------------------------------------------------
-- Test datatype: Maybe Int
data MaybeIntW :: * -> * where MaybeInt :: MaybeIntW D.MaybeInt
type MaybeInt = Tag (Plus (K Int) U) D.MaybeInt

-- Test datatype: Peano naturals
data NatW :: * -> * where Nat :: NatW D.Nat
type Nat = Tag (Plus U (I D.Nat)) D.Nat

-- The AST type from the Multirec paper
type (:+:) = Plus
type (:*:) = Times

data ASTW :: * -> * where
  Expr  ::  ASTW D.Expr
  Decl  ::  ASTW D.Decl
  Var   ::  ASTW D.Var
  
type AST  =    
      Tag (     (K Int)
           :+:  (I D.Expr :*: I D.Expr)
           :+:  (I D.Expr :*: I D.Expr)
           :+:  (I D.Var)
           :+:  (I D.Decl :*: I D.Expr)
          ) D.Expr
  :+: Tag (     (I D.Var  :*: I D.Expr)
           :+:  (I D.Decl :*: I D.Decl)
           :+:  U
          ) D.Decl
  :+: Tag (     (K String)
          ) D.Var
