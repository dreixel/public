{-# LANGUAGE FlexibleInstances    #-}

module Instant.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.Instant
import Auxiliary.Auxiliary (test)
import qualified Instant.Show.Show as G

instance G.Show Logic where
  {-# INLINE show' #-}
  show' = G.show
instance G.Show (Tree Int) where
  {-# INLINE show' #-}
  show' = G.show

mainTree :: IO ()
mainTree = test . putStr . show . last $ G.show bigTree ""

mainLogic :: IO ()
mainLogic = test . putStr . show . last $ G.show bigLogic ""
