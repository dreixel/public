{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE GADTs             #-}

module Instant.Show.Show where

import Generics.Instant.Base

import Prelude hiding (Show, show)
import qualified Prelude as P (Show, shows)
import Data.List (intersperse)

-- Generic show on Representable (worker)
class Show a where
  show' :: a -> ShowS

instance Show U where
  show' U = showString ""
  
instance (Show a, Show b) => Show (a :+: b) where
  show' (L x) = show' x
  show' (R x) = show' x
  
instance (Show a, Show b) => Show (a :*: b) where
  show' (a :*: b) = show' a . showChar ' ' . show' b
  
instance (Show a, Constructor c) => Show (C c a) where
  show' cx@(C a) = showParen True (showString (conName cx) . showChar ' ' . show' a)

instance Show a => Show (Var a) where
  show' (Var x) = show' x

instance Show a => Show (Rec a) where
  show' (Rec x) = show' x


-- Dispatcher
show :: (Representable a, Show (Rep a)) => a -> ShowS
show = show' . from


-- Adhoc instances
instance Show Int    where show' = P.shows
instance Show [Char] where show' = P.shows
