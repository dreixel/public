{-# LANGUAGE FlexibleInstances      #-}

module Instant.Eq.Main where

import qualified Instant.Eq.Eq as G (Eq(..), eq)
import Auxiliary.Tree (bigTree, tweakRightmost, Tree)
import Auxiliary.Logic (biggerLogic, tweakLogic, Logic)
import Auxiliary.Instant
import Auxiliary.Auxiliary (test)

instance G.Eq (Tree Int) where eq' = G.eq
instance G.Eq Logic where eq' = G.eq

mainTree = test . putStr . show $ 
        (G.eq bigTree bigTree, G.eq bigTree (tweakRightmost bigTree))

mainLogic = test . putStr . show . last . show $ 
              [ G.eq biggerLogic biggerLogic, 
                G.eq biggerLogic (tweakLogic biggerLogic),
                G.eq (tweakLogic biggerLogic) biggerLogic]
