{-# LANGUAGE FlexibleInstances #-}

module Instant.Enum.Main where

import Auxiliary.Nat (Nat(..), eqNat)
import Auxiliary.Tree (Tree(..), eqTree)
import Auxiliary.Logic (Logic(..), eqLogic)
import Auxiliary.Auxiliary (test)
import Auxiliary.Instant

import Instant.Enum.Enum

import Prelude hiding (Enum(..))


instance Enum Nat where enum' = enum
instance Enum (Tree Int) where enum' = enum
instance Enum Logic where enum' = enum

enumTree :: [Tree Int]
enumTree = enum

enumLogic :: [Logic]
enumLogic = enum


mainNat :: IO ()
mainNat = test . print $ (enum !! 2000001) `eqNat` (enum !! 2000000)

mainTree :: IO ()
mainTree = test . print $ (enumTree !! 1000001) `eqTree` (enumTree !! 1000000)

mainLogic :: IO ()
mainLogic = test . print $ (enumLogic !! 1000000) `eqLogic` (enumLogic !! 1000001)
