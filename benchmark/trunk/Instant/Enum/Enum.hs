{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverlappingInstances #-}

module Instant.Enum.Enum (Enum(..), enum) where

import Auxiliary.InstantBase

import Prelude hiding (Enum)
import qualified Prelude as P (Enum)

import Auxiliary.Auxiliary (diag, (|||))


class Enum a where
  enum' :: [a]

instance Enum U where
  enum' = [U]

-- Special case for our test
instance Enum (Var Int) where
  enum' = [Var 0]

-- Special case for our test
instance Enum (Var String) where
  enum' = [Var "x"]

instance (P.Enum x, Bounded x) => Enum (Var x) where
  enum' = map Var [minBound..]

instance (Enum x) => Enum (Rec x) where
  enum' = map Rec enum'

instance (Enum f, Enum g) => Enum (f :+: g) where
  enum' = map L enum' ||| map R enum'

instance (Enum f, Enum g) => Enum (f :*: g) where
  enum' = diag [ [ x :*: y | y <- enum' ] | x <- enum' ]

instance (Enum f) => Enum (C c f) where
  enum' = map C enum'


enum :: (Representable a, Enum (Rep a)) => [a]
enum = map to enum'
