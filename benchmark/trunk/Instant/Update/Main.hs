{-# LANGUAGE FlexibleInstances    #-}

module Instant.Update.Main where

import Auxiliary.Tree (smallerTree, eqTree, Tree)
import Auxiliary.Logic (smallerLogic, eqLogic, Logic)
import Auxiliary.Instant
import Auxiliary.Auxiliary (test, apply)
import Instant.Update.Update


instance MapOn [Char] where
  mapOn "" = ""  
  mapOn s  = last s : init s

instance MapOn Int where
  mapOn n | odd n     = n+1
          | otherwise = n-1

instance Update (Tree Int) where
  update' = update

instance Update Logic where 
  update' = update

mainTree :: IO ()
mainTree = test . putStr . show . eqTree smallerTree . (apply 40 update) $ smallerTree

mainLogic :: IO ()
mainLogic = test . putStr . show . eqLogic smallerLogic . (apply 700 update) $ smallerLogic
