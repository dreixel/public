{-# LANGUAGE FlexibleInstances    #-}

module Deriving.Map.Main where

import Auxiliary.Tree (smallerTree, eqTree, Tree(..))
import Auxiliary.Auxiliary (test, apply)
import Deriving.Map.Map
import Auxiliary.Deriving


{-# SPECIALISE gmap :: (a -> b) -> Tree a -> Tree b #-}

instance GFunctor Tree

mainTree :: IO ()
mainTree = let t1 = apply 50 (gmap (+ 1)) smallerTree
               t2 = apply 50 (gmap (\n -> n-1)) t1
           in test . putStr . show $ eqTree smallerTree t2
