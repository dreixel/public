{-# LANGUAGE FlexibleInstances    #-}

module Instant.Update.Main where

import Auxiliary.Tree (bigTree, sumTree, Tree)
import Auxiliary.Logic (biggerLogic, evalLogic, Logic)
import Auxiliary.Instant
import Auxiliary.Auxiliary (test, apply)
import Instant.Update.Update


instance MapOn [Char] where
  {-# INLINE mapOn #-}
  mapOn "" = ""  
  mapOn s  = last s : tail s

instance MapOn Int where
  {-# INLINE mapOn #-}
  mapOn n | odd n     = n+1
          | otherwise = n-1

instance Update (Tree Int) where
  {-# INLINE update' #-}
  update' = update

instance Update Logic where 
  {-# INLINE update' #-}
  update' = update

mainTree :: IO ()
mainTree = test . putStr . show . sumTree . (apply 4 update) $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . evalLogic (const True) . (apply 800 update) $ biggerLogic
