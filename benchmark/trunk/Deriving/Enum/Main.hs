{-# LANGUAGE FlexibleInstances #-}

module Deriving.Enum.Main where

import Auxiliary.Tree (Tree(..), eqTree)
import Auxiliary.Logic (Logic(..), eqLogic)
import Auxiliary.Nat (Nat(..), eqNat)
import Auxiliary.Auxiliary (test)
import Auxiliary.Deriving
import GHC.Generics

import Deriving.Enum.Enum

import Prelude hiding (Enum(..))


-- {-# SPECIALISE genum :: [Tree Int] #-}
-- {-# SPECIALISE genum :: [Logic] #-}

instance GEnum (Tree Int) where genum = map to enum'
instance GEnum Logic      where genum = map to enum'
instance GEnum Nat        where genum = map to enum'


mainTree :: IO ()
mainTree = test . print $ (genum !! 1000001) `eqTree` (genum !! 1000000)

mainLogic :: IO ()
mainLogic = test . print $ (genum !! 1000000) `eqLogic` (genum !! 1000001)

mainNat :: IO ()
mainNat = test . print $ (genum !! 2000001) `eqNat` (genum !! 2000000)
