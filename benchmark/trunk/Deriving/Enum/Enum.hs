{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeFamilies          #-}

module Deriving.Enum.Enum where

import Generics.Deriving.Base


{-# RULES "map/map1" forall f g l. map f (map g l) = map (f . g) l #-}
{-# RULES "map/map2" forall f x. map f (x:[]) = (f x):[] #-}

-----------------------------------------------------------------------------
-- Utility functions for Enum'
-----------------------------------------------------------------------------

infixr 5 |||

-- | Interleave elements from two lists. Similar to (++), but swap left and
-- right arguments on every recursive application.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE (|||) #-}
{-# RULES "ft |||" forall f a b. map f (a ||| b) = map f a ||| map f b #-}
(|||) :: [a] -> [a] -> [a]
[]     ||| ys = ys
(x:xs) ||| ys = x : ys ||| xs



-- | Diagonalization of nested lists. Ensure that some elements from every
-- sublist will be included. Handles infinite sublists.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE diag #-}
{-# RULES "ft diag" forall f l. map f (diag l) = diag (map (map f) l) #-}
diag :: [[a]] -> [a]
diag = concat . foldr skew [] . map (map (\x -> [x]))

skew :: [[a]] -> [[a]] -> [[a]]
skew []     ys = ys
skew (x:xs) ys = x : combine (++) xs ys

combine :: (a -> a -> a) -> [a] -> [a] -> [a]
combine _ xs     []     = xs
combine _ []     ys     = ys
combine f (x:xs) (y:ys) = f x y : combine f xs ys

--------------------------------------------------------------------------------
-- Generic enum
--------------------------------------------------------------------------------

class Enum' f where
  enum' :: [f a]

instance Enum' U1 where
  {-# INLINE enum' #-}
  enum' = [U1]

instance (GEnum c) => Enum' (K1 i c) where
  {-# INLINE enum' #-}
  enum' = map K1 genum

instance (Enum' f) => Enum' (M1 i c f) where
  {-# INLINE enum' #-}
  enum' = map M1 enum'

instance (Enum' f, Enum' g) => Enum' (f :+: g) where
  {-# INLINE enum' #-}
  enum' = map L1 enum' ||| map R1 enum'

instance (Enum' f, Enum' g) => Enum' (f :*: g) where
  {-# INLINE enum' #-}
  -- enum' = diag [ [ x :*: y | y <- enum' ] | x <- enum' ]
  enum' = diag (map (\x -> map (\y -> x :*: y) enum') enum')


class GEnum a where
  {-# INLINE genum #-}
  genum :: [a]
  default genum :: (Generic a, Enum' (Rep a)) => [a]
  genum = map to enum'

-- Special cases for our test
instance GEnum Int    where
  {-# INLINE genum #-}
  genum = [0]
instance GEnum String where
  {-# INLINE genum #-}
  genum = ["x"]
