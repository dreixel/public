{-# LANGUAGE FlexibleInstances      #-}

module Deriving.Eq.Main where

import Deriving.Eq.Eq
import Auxiliary.Tree (bigTree, tweakRightmost, Tree)
import Auxiliary.Logic (biggerLogic, tweakLogic, Logic)
import Auxiliary.Deriving
import Auxiliary.Auxiliary (test)

instance GEq (Tree Int)
instance GEq Logic

{-
-- Seems unnecessary
{-# SPECIALISE geq :: Tree Int -> Tree Int -> Bool #-}
{-# SPECIALISE geq :: Logic -> Logic -> Bool #-}
-}

mainTree = test . putStr . show $ 
        (geq bigTree bigTree, geq bigTree (tweakRightmost bigTree))

mainLogic = test . putStr . show . last . show $ 
              [ geq biggerLogic biggerLogic, 
                geq biggerLogic (tweakLogic biggerLogic),
                geq (tweakLogic biggerLogic) biggerLogic]
