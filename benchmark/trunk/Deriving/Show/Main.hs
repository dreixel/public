{-# LANGUAGE FlexibleInstances    #-}

module Deriving.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.Deriving
import Auxiliary.Auxiliary (test)
import Deriving.Show.Show

{-
{-# SPECIALISE gshowsPrec :: Int -> Tree Int -> ShowS #-}
{-# SPECIALISE gshowsPrec :: Int -> Logic -> ShowS #-}
-}
instance GShow Logic
instance GShow (Tree Int)

mainTree :: IO ()
mainTree = test . putStr . show . last $ gshow bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . last $ gshow bigLogic
