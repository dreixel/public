{-# LANGUAGE TypeOperators      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE FlexibleContexts   #-}

module Instant.Decode.Decode (Decode(..), decode) where

import Auxiliary.Auxiliary (Bit(..), decodeInt, decodeString)

import Generics.Instant


class Decode a where
  decode' :: [Bit] -> (a, [Bit])

instance Decode U where
  decode' bs = (U, bs)

-- Special case for our test
instance Decode (Var Int) where
  decode' bs = case decodeInt bs of
                 (i, bs') -> (Var i, bs')

-- Special case for our test
instance Decode (Var String) where
  decode' bs = case decodeString bs of
                 (s, bs') -> (Var s, bs')

instance (Decode x) => Decode (Rec x) where
  decode' bs = case decode' bs of
                 (x, bs') -> (Rec x, bs')

instance (Decode f, Decode g) => Decode (f :+: g) where
  decode' (0:bs) = case decode' bs of
                     (x, bs') -> (L x, bs')
  decode' (1:bs) = case decode' bs of
                     (x, bs') -> (R x, bs')
  decode' []     = error "decode': cannot decode"

instance (Decode f, Decode g) => Decode (f :*: g) where
  decode' bs0 = case decode' bs0 of
                  (x, bs1) -> case decode' bs1 of
                                (y, bs2) -> (x :*: y, bs2)

instance (Decode f) => Decode (C c f) where
  decode' bs = case decode' bs of 
                 (x, bs') -> (C x, bs')


decode :: (Representable a, Decode (Rep a)) => [Bit] -> (a, [Bit])
decode bs = case decode' bs of 
              (x, bs') -> (to x, bs')
