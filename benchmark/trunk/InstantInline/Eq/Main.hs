{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE CPP                    #-}

module InstantInline.Eq.Main where

import qualified InstantInline.Eq.Eq as G (Eq(..), eq)
import Auxiliary.Tree (bigTree, tweakRightmost, Tree)
import Auxiliary.Logic (biggerLogic, tweakLogic, Logic)
import Auxiliary.InstantInline
import Auxiliary.Auxiliary (test)

#if __GLASGOW_HASKELL__ >= 700
{-
{-# SPECIALIZE G.eq :: Logic -> Logic -> Bool #-}
{-# SPECIALIZE G.eq :: Tree Int -> Tree Int -> Bool #-}
-}
#endif

instance G.Eq (Tree Int) where eq' = G.eq
instance G.Eq Logic      where eq' = G.eq

mainTree = test . putStr . show $ 
        (G.eq bigTree bigTree, G.eq bigTree (tweakRightmost bigTree))

mainLogic = test . putStr . show . last . show $ 
              [ G.eq biggerLogic biggerLogic, 
                G.eq biggerLogic (tweakLogic biggerLogic),
                G.eq (tweakLogic biggerLogic) biggerLogic]
