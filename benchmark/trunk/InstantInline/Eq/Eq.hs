{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE GADTs                 #-}

module InstantInline.Eq.Eq where

import Auxiliary.InstantBase
import Prelude hiding (Eq)
import qualified Prelude as P (Eq)

-- Generic eq on Representable (worker)
class Eq a where
  eq' :: a -> a -> Bool

instance Eq U where
  {-# INLINE eq' #-}
  eq' U U = True
  
instance (Eq a, Eq b) => Eq (a :+: b) where
  {-# INLINE eq' #-}
  eq' (L x) (L x') = eq' x x'
  eq' (R x) (R x') = eq' x x'
  eq' _      _     = False
  
instance (Eq a, Eq b) => Eq (a :*: b) where
  {-# INLINE eq' #-}
  eq' (a :*: b) (a' :*: b') = eq' a a' && eq' b b'

instance (Eq a) => Eq (C c a) where
  {-# INLINE eq' #-}
  eq' (C a) (C a') = eq' a a'

instance Eq a => Eq (Var a) where
  {-# INLINE eq' #-}
  eq' (Var x) (Var x') = eq' x x'

instance (Eq a) => Eq (Rec a) where
  {-# INLINE eq' #-}
  eq' (Rec x) (Rec x') = eq' x x'


-- Dispatcher
{-# INLINABLE eq #-}
eq :: (Representable a, Eq (Rep a)) => a -> a -> Bool
eq x y = eq' (from x) (from y)


-- Adhoc instances
instance Eq Int where
  {-# INLINE eq' #-}
  eq' = (==)

instance (P.Eq a) => Eq [a] where
  {-# INLINE eq' #-}
  eq' = (==)
