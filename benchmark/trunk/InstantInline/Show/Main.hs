{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE CPP                  #-}

module InstantInline.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.InstantInline
import Auxiliary.Auxiliary (test)
import qualified InstantInline.Show.Show as G

#if __GLASGOW_HASKELL__ >= 700
{-
{-# SPECIALIZE G.show :: Logic -> ShowS #-}
{-# SPECIALIZE G.show :: Tree Int -> ShowS #-}
-}
#endif

instance G.Show Logic where
  {-# INLINE show' #-}
  show' = G.show
instance G.Show (Tree Int) where
  {-# INLINE show' #-}
  show' = G.show

mainTree :: IO ()
mainTree = test . putStr . show . last $ G.show bigTree ""

mainLogic :: IO ()
mainLogic = test . putStr . show . last $ G.show bigLogic ""
