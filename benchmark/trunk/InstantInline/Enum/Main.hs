{-# LANGUAGE FlexibleInstances #-}

module InstantInline.Enum.Main where

import Auxiliary.Nat (Nat(..), eqNat)
import Auxiliary.Tree (Tree(..), eqTree)
import Auxiliary.Logic (Logic(..), eqLogic)
import Auxiliary.Auxiliary (test)
import Auxiliary.InstantInline
import Auxiliary.InstantBase

import InstantInline.Enum.Enum



-- This works

instance GEnum Nat        where genum = map to genum'
instance GEnum (Tree Int) where genum = map to genum'
instance GEnum Logic      where genum = map to genum'
{-
enumTree :: [Tree Int]
enumTree = map to enum'

enumLogic :: [Logic]
enumLogic = map to enum'


-- This is what I would like to have

{-# SPECIALIZE enum :: [Logic] #-}
{-# SPECIALIZE enum :: [Tree Int] #-}

instance Enum (Tree Int) where enum' = enumTree
instance Enum Logic where enum' = enumLogic

enumTree :: [Tree Int]
enumTree = enum

enumLogic :: [Logic]
enumLogic = enum
-}

mainNat :: IO ()
mainNat = test . print $ (genum !! 2000001) `eqNat` (genum !! 2000000)

mainTree :: IO ()
mainTree = test . print $ (genum !! 1000001) `eqTree` (genum !! 1000000)

mainLogic :: IO ()
mainLogic = test . print $ (genum !! 1000000) `eqLogic` (genum !! 1000001)
