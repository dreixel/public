{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# OPTIONS_GHC -fenable-rewrite-rules #-}

module InstantInline.Enum.Enum (GEnum'(..), GEnum(..), enum) where

import Auxiliary.InstantBase

-----------------------------------------------------------------------------
-- Utility functions for GEnum
-----------------------------------------------------------------------------

infixr 5 |||

-- | Interleave elements from two lists. Similar to (++), but swap left and
-- right arguments on every recursive application.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE [0] (|||) #-}
{-# RULES "ft |||" forall f a b. map f (a ||| b) = map f a ||| map f b #-}
(|||) :: [a] -> [a] -> [a]
[]     ||| ys = ys
(x:xs) ||| ys = x : ys ||| xs



-- | Diagonalization of nested lists. Ensure that some elements from every
-- sublist will be included. Handles infinite sublists.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE [0] diag #-}
{-# RULES "ft diag" forall f l. map f (diag l) = diag (map (map f) l) #-}
{-# RULES "map/map" forall f g l. map f (map g l) = map (f . g) l #-}
{-# RULES "map1" forall f x. map f (x : []) = (f x) : [] #-}
diag :: [[a]] -> [a]
diag = concat . foldr skew [] . map (map (\x -> [x]))

skew :: [[a]] -> [[a]] -> [[a]]
skew []     ys = ys
skew (x:xs) ys = x : combine (++) xs ys

combine :: (a -> a -> a) -> [a] -> [a] -> [a]
combine _ xs     []     = xs
combine _ []     ys     = ys
combine f (x:xs) (y:ys) = f x y : combine f xs ys



class GEnum' a where
  genum' :: [a]

instance GEnum' U where
  {-# INLINE genum' #-}
  genum' = [U]

instance (GEnum x) => GEnum' (Var x) where
  {-# INLINE genum' #-}
  genum' = map Var genum

instance (GEnum x) => GEnum' (Rec x) where
  {-# INLINE genum' #-}
  genum' = map Rec genum

instance (GEnum' f, GEnum' g) => GEnum' (f :+: g) where
  {-# INLINE genum' #-}
  genum' = map L genum' ||| map R genum'

instance (GEnum' f, GEnum' g) => GEnum' (f :*: g) where
  {-# INLINE genum' #-}
  -- genum' = diag [ [ x :*: y | y <- genum' ] | x <- genum' ]
  genum' = diag (map (\x -> map (\y -> x :*: y) genum') genum')

instance (GEnum' f) => GEnum' (C c f) where
  {-# INLINE genum' #-}
  genum' = map C genum'

  
class GEnum a where
  genum :: [a]
  
-- Special case for our test
instance GEnum Int where
  {-# INLINE genum #-}
  genum = [0]

-- Special case for our test
instance GEnum String where
  {-# INLINE genum #-}
  genum = ["x"]

{-# INLINABLE enum #-}
enum :: (Representable a, GEnum' (Rep a)) => [a]
enum = map to genum'
