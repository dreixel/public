{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE GADTs                #-}

module InstantInline.Update.Update where

import Generics.Instant.Base
import Generics.Instant.Instances

import Control.Parallel

-- Generic update on Representable (worker)
class Update a where
  update' :: a -> a

instance Update U where
  {-# INLINE update' #-}
  update' U = U
  
instance (Update a, Update b) => Update (a :+: b) where
  {-# INLINE update' #-}
  update' (L x) = L (update' x)
  update' (R x) = R (update' x)
  
instance (Update a, Update b) => Update (a :*: b) where
  {-# INLINE update' #-}
  update' (a :*: b) = update' a :*: update' b
  --update' (a :*: b) = l `par` r `pseq` l :*: r
    where l = update' a
          r = update' b
  
instance (Update a, Constructor c) => Update (C c a) where
  {-# INLINE update' #-}
  update' (C a) = C (update' a)

instance Update a => Update (Rec a) where
  {-# INLINE update' #-}
  update' (Rec x) = Rec (update' x)

instance (MapOn a) => Update (Var a) where
  {-# INLINE update' #-}
  update' (Var x) = Var (mapOn x)

class MapOn a where
  {-# INLINE mapOn #-}
  mapOn :: a -> a
  mapOn = id


-- Dispatcher
{-# INLINABLE update #-}
update :: (Representable a, Update (Rep a)) => a -> a
update = to . update' . from


-- Adhoc instances


-- Generic instances
instance (MapOn a)          => Update (Maybe a) where update' = update
instance (MapOn a)          => Update [a]       where update' = update
instance (MapOn a, MapOn b) => Update (a,b)     where update' = update
