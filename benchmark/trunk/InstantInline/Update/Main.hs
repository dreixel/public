{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE CPP                  #-}

module InstantInline.Update.Main where

import Auxiliary.Tree (smallerTree, eqTree, Tree)
import Auxiliary.Logic (smallerLogic, eqLogic, Logic)
import Auxiliary.InstantInline
import Auxiliary.Auxiliary (test, apply)
import InstantInline.Update.Update


#if __GLASGOW_HASKELL__ >= 700
{-# SPECIALIZE update :: Logic -> Logic #-}
{-# SPECIALIZE update :: Tree Int -> Tree Int #-}
#endif

instance MapOn [Char] where
  {-# INLINE mapOn #-}
  mapOn "" = ""  
  mapOn s  = last s : init s

instance MapOn Int where
  {-# INLINE mapOn #-}
  mapOn n | odd n     = n+1
          | otherwise = n-1

instance Update (Tree Int) where
  {-# INLINE update' #-}
  update' = update

instance Update Logic where 
  {-# INLINE update' #-}
  update' = update

mainTree :: IO ()
mainTree = test . putStr . show . eqTree smallerTree . (apply 40 update) $ smallerTree

mainLogic :: IO ()
mainLogic = test . putStr . show . eqLogic smallerLogic . (apply 700 update) $ smallerLogic
