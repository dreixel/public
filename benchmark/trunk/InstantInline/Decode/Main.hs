{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE TypeOperators  #-}

module InstantInline.Decode.Main where

import Auxiliary.Tree (Tree, decodeTreeTest)
import Auxiliary.Logic (Logic(..), decodeLogicTest)
import Auxiliary.Auxiliary (test, allEqual, Bit)
import Auxiliary.InstantInline

import Generics.Instant ((:*:)(..))

import InstantInline.Decode.Decode

{-# SPECIALIZE decode :: [Bit] -> (Logic, [Bit]) #-}
{-# SPECIALIZE decode :: [Bit] -> (Tree Int, [Bit]) #-}

instance Decode (Tree Int) where decode' = decode
instance Decode Logic      where decode' = decode

decodeTree :: [Bit] -> (Tree Int, [Bit])
decodeTree = decode

decodeLogic :: [Bit] -> (Logic, [Bit])
decodeLogic = decode

mainTree :: IO ()
mainTree = test . putStr . show . allEqual $ decodeTreeTest decodeTree

mainLogic :: IO ()
mainLogic = test . putStr . show . allEqual $ decodeLogicTest decodeLogic
