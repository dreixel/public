{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TypeOperators        #-}

module InstantInline.Map.Main where

import Auxiliary.Tree (smallerTree, sumTree, Tree(..))
import Auxiliary.Auxiliary (test, apply)
import InstantInline.Map.Map
import Auxiliary.InstantInline

instance MapOn Int where
  {-# INLINE mapOn #-}
  mapOn = (+1)

instance Update (Tree Int) where 
  {-# INLINE update' #-}
  update' = update

mainTree :: IO ()
mainTree = let t = apply 1000 update smallerTree
           in test . putStr . show . sumTree $ t
