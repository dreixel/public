module Hand.Map.Main where

import Auxiliary.Tree (smallerTree, eqTree, mapTree, Tree(..))
import Auxiliary.Auxiliary (test, apply)


mainTree :: IO ()
mainTree = let t1 = apply 50 (mapTree (+ 1)) smallerTree
               t2 = apply 50 (mapTree (\n -> n-1)) t1
           in test . putStr . show $ eqTree smallerTree t2
