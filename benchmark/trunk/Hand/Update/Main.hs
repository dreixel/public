module Hand.Update.Main where

import Auxiliary.Tree (smallerTree, eqTree)
import Auxiliary.Logic (smallerLogic, eqLogic)
import Auxiliary.Auxiliary (test, apply)
import Hand.Update.Update

mainTree :: IO ()
mainTree = test . putStr . show . eqTree smallerTree . (apply 40 updateTree) $ smallerTree

mainLogic :: IO ()
mainLogic = test . putStr . show . eqLogic smallerLogic . (apply 700 updateLogic) $ smallerLogic
