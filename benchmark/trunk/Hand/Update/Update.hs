module Hand.Update.Update where

import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))

updateTree :: Tree Int -> Tree Int
updateTree Leaf        = Leaf
updateTree (Bin n l r) | odd n     = (Bin (n+1)) (updateTree l) (updateTree r)
                       | otherwise = (Bin (n-1)) (updateTree l) (updateTree r)

updateLogic :: Logic -> Logic
updateLogic (Var "")    = Var ""
updateLogic (Var s)     = Var (last s : init s)
updateLogic (Impl  p q) = Impl  (updateLogic p) (updateLogic q)
updateLogic (Equiv p q) = Equiv (updateLogic p) (updateLogic q)
updateLogic (Conj  p q) = Conj  (updateLogic p) (updateLogic q)
updateLogic (Disj  p q) = Disj  (updateLogic p) (updateLogic q)
updateLogic (Not p)     = Not (updateLogic p)
updateLogic T           = T
updateLogic F           = F
