module Hand.Arbitrary.Arbitrary where

import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))
import Test.QuickCheck
import Control.Monad (liftM, liftM2, liftM3)
import Auxiliary.Auxiliary ()


instance (Arbitrary a) => Arbitrary (Tree a) where
  arbitrary = sized tree'
    where tree' 0 = return Leaf
          tree' n | n>0 = oneof [ return Leaf
                                , liftM3 Bin arbitrary subtree subtree]
            where subtree = tree' (n `div` 2)
            
  coarbitrary = error "undefined coarbitrary for Tree"
{-
  coarbitrary Leaf          = variant 0
  coarbitrary (Bin x t1 t2) = variant 1 . coarbitrary x
                              . coarbitrary t1 . coarbitrary t2
-}

instance Arbitrary Logic where
  arbitrary = sized logic'
    where logic' 0 = oneof [ return T
                           , return F
                           , liftM Var arbitrary]
                            
          logic' n | n > 0 = oneof [ logic' 0
                                   , liftM2 Impl sublogic sublogic
                                   , liftM2 Equiv sublogic sublogic
                                   , liftM2 Disj sublogic sublogic
                                   , liftM2 Conj sublogic sublogic
                                   , liftM Not sublogic]
            where sublogic = logic' (n `div` 2)
            
  coarbitrary = error "undefined coarbitrary for Logic"
