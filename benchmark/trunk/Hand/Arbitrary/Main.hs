module Hand.Arbitrary.Main where

import Auxiliary.Tree (Tree(..), sumTree)
import Auxiliary.Logic (Logic(..), hashLogic)
import Auxiliary.Auxiliary (test, generate')
import Test.QuickCheck (arbitrary)

import Hand.Arbitrary.Arbitrary

mainTree :: IO ()
mainTree = test . print . sum . map sumTree . take 150000 $ 
             (generate' arbitrary :: [Tree Int])

mainLogic :: IO ()
mainLogic = test . print . sum . map hashLogic . take 25000 $ 
              (generate' arbitrary :: [Logic])
