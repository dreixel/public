{-# LANGUAGE StandaloneDeriving #-}

module Hand.Eq.Main where

import Auxiliary.Tree (Tree(..), bigTree, tweakRightmost, eqTree)
import Auxiliary.Logic (Logic(..), biggerLogic, tweakLogic, eqLogic)
import Auxiliary.Auxiliary (test)


mainTree :: IO ()
mainTree = test . putStr . show $ 
            (eqT bigTree bigTree, eqT bigTree (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $ 
             [ eqL biggerLogic biggerLogic, 
               eqL biggerLogic (tweakLogic biggerLogic),
               eqL (tweakLogic biggerLogic) biggerLogic]

eqL = eqLogic

eqT = eqTree
