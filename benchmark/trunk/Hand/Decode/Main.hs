
module Hand.Decode.Main where

import Auxiliary.Tree (decodeTreeTest)
import Auxiliary.Logic (decodeLogicTest)
import Auxiliary.Auxiliary (test, allEqual)
import Hand.Decode.Decode

mainTree :: IO ()
mainTree = test . putStr . show . allEqual $ decodeTreeTest decodeTree

mainLogic :: IO ()
mainLogic = test . putStr . show . allEqual $ decodeLogicTest decodeLogic
