
module Hand.Decode.Decode (decodeTree, decodeLogic) where

import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))
import Auxiliary.Auxiliary (Bit(..), decodeInt, decodeString)


decodeTree :: [Bit] -> (Tree Int, [Bit])
decodeTree [] = error "decodeTree: cannot decode"
decodeTree (0:t) = (Leaf, t)
decodeTree (1:t0) = let (x, t1) = decodeInt  t0
                        (l, t2) = decodeTree t1
                        (r, t3) = decodeTree t2
                    in (Bin x l r, t3)

decodeLogic :: [Bit] -> (Logic, [Bit])
decodeLogic [] = error "decodeLogic: cannot decode"
decodeLogic (0:t) = let (s, t') = decodeString t
                    in (Var s, t') -- simplification
decodeLogic (1:t0) = 
  case t0 of
   (0:t1) -> (T, t1)
   (1:t1) -> 
     case t1 of
       (0:t2) -> (F, t2)
       (1:t2) -> 
         case t2 of
           (0:t3) -> let (p, t') = decodeLogic t3
                     in  (Not p, t')
           (1:t3) -> 
             case t3 of
               (0:t4) -> let (p, t')  = decodeLogic t4
                             (q, t'') = decodeLogic t'
                         in  (Impl p q, t'')
               (1:t4) -> 
                 case t4 of
                   (0:t5) -> let (p, t')  = decodeLogic t5
                                 (q, t'') = decodeLogic t'
                             in  (Equiv p q, t'')
                   (1:t5) -> 
                     case t5 of
                       (0:t6) -> let (p, t')  = decodeLogic t6
                                     (q, t'') = decodeLogic t'
                                 in  (Conj p q, t'')
                       (1:t6) -> let (p, t')  = decodeLogic t6
                                     (q, t'') = decodeLogic t'
                                 in  (Disj p q, t'')
