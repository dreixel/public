module Hand.Enum.Enum (enumNat, enumTree, enumLogic) where

import Auxiliary.Nat (Nat(..))
import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))

import Auxiliary.Auxiliary (diag, (|||))


enumNat :: [Nat]
enumNat = [Ze] ||| map Su enumNat

enumTree :: [Tree Int]
enumTree = [Leaf] ||| diag [ [Bin 0 l r | r <- enumTree] | l <- enumTree]

enumLogic :: [Logic]
enumLogic = foldr1 (|||)
             ([ [Var "x"], [T], [F], [Not p | p <- enumLogic ]
              , diag [ [ Impl  p q  | q <- enumLogic ] | p <- enumLogic ]
              , diag [ [ Equiv p q  | q <- enumLogic ] | p <- enumLogic ]
              , diag [ [ Conj  p q  | q <- enumLogic ] | p <- enumLogic ]
              , diag [ [ Disj  p q  | q <- enumLogic ] | p <- enumLogic ]
              ])
