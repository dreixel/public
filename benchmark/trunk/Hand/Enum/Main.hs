module Hand.Enum.Main where

import Auxiliary.Nat (Nat(..), eqNat)
import Auxiliary.Tree (Tree(..), eqTree)
import Auxiliary.Logic (Logic(..), eqLogic)
import Auxiliary.Auxiliary (test)

import Hand.Enum.Enum

mainNat :: IO ()
mainNat = test . print $ (enumNat !! 2000001) `eqNat` (enumNat !! 2000000)

mainTree :: IO ()
mainTree = test . print $ (enumTree !! 1000001) `eqTree` (enumTree !! 1000000)

mainLogic :: IO ()
mainLogic = test . print $ (enumLogic !! 1000000) `eqLogic` (enumLogic !! 1000001)
