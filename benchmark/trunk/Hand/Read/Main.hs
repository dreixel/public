
module Hand.Read.Main where

import Auxiliary.Tree (Tree(..), smallerTreeString, sumTree)
import Auxiliary.Logic (Logic(..), smallerLogicString, evalLogic)
import Auxiliary.Auxiliary (test)
import Hand.Read.Read

mainTree :: IO ()
mainTree = let t = readTree smallerTreeString
           in test . putStr . show . sumTree $ t

mainLogic :: IO ()
mainLogic = let t = readLogic smallerLogicString
            in test . putStr . show . evalLogic (const True) $ t
