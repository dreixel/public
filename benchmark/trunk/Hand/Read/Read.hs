
module Hand.Read.Read (readTree, readLogic) where

import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))
import Text.Read
import Control.Monad (guard)

readTree :: String -> Tree Int
readTree s = case (readPrec_to_S readTreeP 0) s of
               [] -> error "readTree: no parse"
               l  -> fst (head l)

readLogic :: String -> Logic
readLogic s = case (readPrec_to_S readLogicP 0) s of
               [] -> error "readLogic: no parse"
               l  -> fst (head l)

readTreeP :: ReadPrec (Tree Int)
readTreeP = paren (readBin +++ readLeaf) where
  readBin, readLeaf :: ReadPrec (Tree Int)
  readBin = do  
                Ident s <- lexP
                guard (s == "Bin")
                Int x   <- paren lexP
                l       <- readTreeP
                r       <- readTreeP
                return (Bin (fromInteger x) l r)
  readLeaf = do
                 Ident s <- lexP
                 guard (s == "Leaf")
                 return Leaf


readLogicP :: ReadPrec Logic
readLogicP = paren (choice [readVar, read0 "T" T, read0 "F" F, read1 "Not" Not,
                             read2 "Impl" Impl, read2 "Equiv" Equiv,
                             read2 "Conj" Conj, read2 "Disj" Disj]) where
  read0 s c = do
                Ident s' <- lexP
                guard (s == s')
                return c
  read1 s c = do
                read0 s c
                x <- readLogicP
                return (c x)
  read2 s c = do
                read0 s c
                x <- readLogicP
                y <- readLogicP
                return (c x y)
  readVar = do
              Ident s <- lexP
              guard (s == "Var")
              String s' <- lexP
              return (Var s')

paren p = do
            Punc "(" <- lexP
            x <- p
            Punc ")" <- lexP
            return x
