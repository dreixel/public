module MultiRecInline.Eq.Main where

import Auxiliary.Tree (Tree, bigTree, tweakRightmost)
import Auxiliary.Logic (Logic, biggerLogic, tweakLogic)
import Auxiliary.MultiRecInline
import Auxiliary.Auxiliary (test)
import MultiRecInline.Eq.Eq

{-# SPECIALIZE eq :: TreeF (Tree Int) -> Tree Int -> Tree Int -> Bool #-}
{-# SPECIALIZE eq :: LogicF Logic -> Logic -> Logic -> Bool #-}

mainTree :: IO ()
mainTree = test . putStr . show $ 
            (eq Tree bigTree bigTree, eq Tree bigTree (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $
              [ eq Logic biggerLogic biggerLogic, 
                eq Logic biggerLogic (tweakLogic biggerLogic),
                eq Logic (tweakLogic biggerLogic) biggerLogic]
