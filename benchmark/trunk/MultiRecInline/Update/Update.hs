{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE OverlappingInstances   #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE Rank2Types             #-}
{-# LANGUAGE GADTs                  #-}

module MultiRecInline.Update.Update where

import Auxiliary.MultirecInlineBase

------ This doesn't belong here, but #4903 prevents me from putting it in Main
{-
import Auxiliary.Tree (Tree)
import Auxiliary.Logic (Logic)
import Auxiliary.MultiRecInline

{-# SPECIALIZE updateInt :: TreeF (Tree Int) -> Tree Int -> Tree Int #-}
{-# SPECIALIZE updateString :: LogicF Logic -> Logic -> Logic #-}
-}
------

class UpdateInt phi (f :: (* -> *) -> * -> *) where
  updateIntf :: (forall ix. phi ix -> r ix -> r ix)
             -> phi ix -> f r ix -> f r ix

instance UpdateInt phi U where
  {-# INLINE updateIntf #-}
  updateIntf _ _ = id

instance (El phi xi) => UpdateInt phi (I xi) where
  {-# INLINE updateIntf #-}
  updateIntf f _ (I x) = I (f proof x)

instance UpdateInt phi (K Int) where
  {-# INLINE updateIntf #-}
  updateIntf f _ (K n) | odd n     = K (n + 1)
                       | otherwise = K (n - 1)

instance UpdateInt phi (K x) where
  {-# INLINE updateIntf #-}
  updateIntf f _ (K x) = K x

instance (UpdateInt phi f, UpdateInt phi g) => UpdateInt phi (f :+: g) where
  {-# INLINE updateIntf #-}
  updateIntf f p (L x) = L (updateIntf f p x)
  updateIntf f p (R x) = R (updateIntf f p x)

instance (UpdateInt phi f, UpdateInt phi g) => UpdateInt phi (f :*: g) where
  {-# INLINE updateIntf #-}
  updateIntf f p (x :*: y) = updateIntf f p x :*: updateIntf f p y

instance (UpdateInt phi f) => UpdateInt phi (C c f) where
  {-# INLINE updateIntf #-}
  updateIntf f p (C x) = C (updateIntf f p x)

instance (UpdateInt phi f) => UpdateInt phi (f :>: xi) where
  {-# INLINE updateIntf #-}
  updateIntf f p (Tag x) = Tag (updateIntf f p x)
 
{-# INLINABLE updateInt #-}
updateInt :: (Fam phi, UpdateInt phi (PF phi)) => phi ix -> ix -> ix
updateInt p = to p . updateIntf (\p (I0 x) -> I0 (updateInt p x)) p . from p


class UpdateString phi (f :: (* -> *) -> * -> *) where
  updateStringf :: (forall ix. phi ix -> r ix -> r ix)
                -> phi ix -> f r ix -> f r ix

instance UpdateString phi U where
  {-# INLINE updateStringf #-}
  updateStringf _ _ = id

instance (El phi xi) => UpdateString phi (I xi) where
  {-# INLINE updateStringf #-}
  updateStringf f _ (I x) = I (f proof x)

instance UpdateString phi (K String) where
  {-# INLINE updateStringf #-}
  updateStringf f _ (K "") = K ""
  updateStringf f _ (K s)  = K (last s : tail s)

instance UpdateString phi (K x) where
  {-# INLINE updateStringf #-}
  updateStringf f _ (K x) = K x

instance (UpdateString phi f, UpdateString phi g) => UpdateString phi (f :+: g) where
  {-# INLINE updateStringf #-}
  updateStringf f p (L x) = L (updateStringf f p x)
  updateStringf f p (R x) = R (updateStringf f p x)

instance (UpdateString phi f, UpdateString phi g) => UpdateString phi (f :*: g) where
  {-# INLINE updateStringf #-}
  updateStringf f p (x :*: y) = updateStringf f p x :*: updateStringf f p y

instance (UpdateString phi f) => UpdateString phi (C c f) where
  {-# INLINE updateStringf #-}
  updateStringf f p (C x) = C (updateStringf f p x)

instance (UpdateString phi f) => UpdateString phi (f :>: xi) where
  {-# INLINE updateStringf #-}
  updateStringf f p (Tag x) = Tag (updateStringf f p x)

{-# INLINABLE updateString #-}
updateString :: (Fam phi, UpdateString phi (PF phi)) => phi ix -> ix -> ix
updateString p = to p . updateStringf (\p (I0 x) -> I0 (updateString p x)) p . from p
