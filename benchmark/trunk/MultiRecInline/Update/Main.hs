module MultiRecInline.Update.Main where

import Auxiliary.Tree (Tree, bigTree, sumTree)
import Auxiliary.Logic (Logic, biggerLogic, evalLogic)
import Auxiliary.MultiRecInline
import Auxiliary.Auxiliary (test, apply)
import MultiRecInline.Update.Update

{-# SPECIALIZE updateInt :: TreeF (Tree Int) -> Tree Int -> Tree Int #-}
{-# SPECIALIZE updateString :: LogicF Logic -> Logic -> Logic #-}

mainTree :: IO ()
mainTree = test . putStr . show . sumTree . (apply 4 (updateInt Tree)) $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . evalLogic (const True) . (apply 800 (updateString Logic)) $ biggerLogic
