module MultiRecInline.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.MultiRecInline
import Auxiliary.Auxiliary (test)
import qualified MultiRecInline.Show.Show as G


{-# SPECIALIZE G.show :: TreeF (Tree Int) -> Tree Int -> String #-}
{-# SPECIALIZE G.show :: LogicF Logic -> Logic -> String #-}

mainTree :: IO ()
mainTree = test . putStr . show . last . G.show Tree $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . last . G.show Logic $ bigLogic
