module HandPar.Enum.Enum (enumTree, enumLogic) where

import Auxiliary.Tree (Tree(..), seqTree)
import Auxiliary.Logic (Logic(..), seqLogic)

import Auxiliary.Auxiliary (diag, (|||))

import Control.Parallel.Strategies (parList, parListN, using)
import Control.Parallel (pseq)


enumTree :: [Tree Int]
enumTree = let e = [Leaf] ||| diag [ [Bin 0 l r | r <- e] | l <- e]
           in  e -- `using` parList (\x -> seqTree x (return x))

enumLogic :: [Logic]
enumLogic = foldr1 (|||)
             ([ [Var "x"], [T], [F], [Not p | p <- enumLogic ]
              , diag [ [ Impl  p q  | q <- enumLogic ] | p <- enumLogic ]
              , diag [ [ Equiv p q  | q <- enumLogic ] | p <- enumLogic ]
              , diag [ [ Conj  p q  | q <- enumLogic ] | p <- enumLogic ]
              , diag [ [ Disj  p q  | q <- enumLogic ] | p <- enumLogic ]
              ]) -- `using` parList (\x -> map seqLogic x `pseq` return x))
