module HandPar.Enum.Main where

import Auxiliary.Tree (Tree(..), eqTreePar)
import Auxiliary.Logic (Logic(..), eqLogicPar)
import Auxiliary.Auxiliary (test)

import HandPar.Enum.Enum

mainTree :: IO ()
mainTree = test . print $ (enumTree !! 1000001) `eqTreePar` (enumTree !! 1000000)

mainLogic :: IO ()
mainLogic = test . print $ (enumLogic !! 1000000) `eqLogicPar` (enumLogic !! 1000001)
