{-# LANGUAGE StandaloneDeriving #-}

module HandPar.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.Auxiliary (test)


mainTree :: IO ()
mainTree = test . putStr . show . last . show $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $ bigLogic
