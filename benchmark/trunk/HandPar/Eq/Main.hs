{-# LANGUAGE StandaloneDeriving #-}

module HandPar.Eq.Main where

import Auxiliary.Tree (Tree(..), bigTree, tweakRightmost, eqTreePar)
import Auxiliary.Logic (Logic(..), biggerLogic, tweakLogic, eqLogicPar)
import Auxiliary.Auxiliary (test)

mainTree :: IO ()
mainTree = test . putStr . show $ 
            (eqT bigTree bigTree, eqT bigTree (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $ 
             [ eqL biggerLogic biggerLogic, 
               eqL biggerLogic (tweakLogic biggerLogic),
               eqL (tweakLogic biggerLogic) biggerLogic]

eqL = eqLogicPar

eqT = eqTreePar
