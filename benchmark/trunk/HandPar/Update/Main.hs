module HandPar.Update.Main where

import Auxiliary.Tree (smallerTree, eqTree, eqTreePar)
import Auxiliary.Logic (smallerLogic, eqLogic, eqLogicPar)
import Auxiliary.Auxiliary (test, apply)
import HandPar.Update.Update

mainTree :: IO ()
mainTree = test . putStr . show . eqTreePar smallerTree . (apply 40 updateTree) $ smallerTree

mainLogic :: IO ()
mainLogic = test . putStr . show . eqLogicPar smallerLogic . (apply 700 updateLogic) $ smallerLogic
