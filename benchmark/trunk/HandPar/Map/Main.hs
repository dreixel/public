module HandPar.Map.Main where

import Auxiliary.Tree (smallerTree, eqTreePar, mapTreePar, Tree(..))
import Auxiliary.Auxiliary (test, apply)


mainTree :: IO ()
mainTree = let t1 = apply 50 (mapTreePar 0 (+ 1)) smallerTree
               t2 = apply 50 (mapTreePar 0 (\n -> n-1)) t1
           in test . putStr . show $ eqTreePar smallerTree t2
