
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,222.65625,0.0
,EMGM,246.875,0.0
,SYB,4032.8125,0.0
,MultiRec,560.15625,0.0
,Regular,463.28125,0.0
Eq/Logic,Hand,192.1875,0.0
,EMGM,192.1875,0.0
,SYB,1428.90625,0.0
,MultiRec,343.75,0.0
,Regular,309.375,0.0
Map/Tree,Hand,247.65625,0.0
,EMGM,1152.34375,0.0
,SYB,1732.03125,0.0
Read/Tree,Hand,175.78125,0.0
,EMGM,681.25,0.0
,SYB,1867.96875,0.0
,MultiRec,728.90625,0.0
,Regular,711.71875,0.0
Read/Logic,Hand,496.09375,0.0
,EMGM,1757.8125,0.0
,SYB,5608.59375,0.0
Show/Tree,Hand,1154.6875,0.0
,EMGM,1045.3125,0.0
,SYB,4871.09375,0.0
,MultiRec,2770.3125,0.0
,Regular,961.71875,0.0
Show/Logic,Hand,127.34375,0.0
,EMGM,139.0625,0.0
,SYB,364.84375,0.0
,MultiRec,338.28125,0.0
,Regular,133.59375,0.0
Update/Tree,Hand,207.03125,0.0
,EMGM,249.21875,0.0
,SYB,1835.15625,0.0
,MultiRec,802.34375,0.0
,Regular,210.9375,0.0
Update/Logic,Hand,471.09375,0.0
,EMGM,450.0,0.0
,SYB,2591.40625,0.0
,MultiRec,1573.4375,0.0
,Regular,475.0,0.0
-------------------------------------
