
Results:
Number of repetitions: 20
Compiler flags: -O2
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,278.125,0.0
,EMGM,280.46875,0.0
,SYB,5589.0625,0.0
,MultiRec,566.40625,0.0
,Regular,472.65625,0.0
Eq/Logic,Hand,214.0625,0.0
,EMGM,575.0,0.0
,SYB,2146.09375,0.0
,MultiRec,338.28125,0.0
,Regular,336.71875,0.0
Map/Tree,Hand,265.625,0.0
,EMGM,257.8125,0.0
,SYB,1835.9375,0.0
Read/Tree,Hand,183.59375,0.0
,EMGM,742.96875,0.0
,SYB,1525.78125,0.0
,MultiRec,758.59375,0.0
,Regular,726.5625,0.0
Read/Logic,Hand,521.09375,0.0
,EMGM,1822.65625,0.0
,SYB,4389.84375,0.0
Show/Tree,Hand,1316.40625,0.0
,EMGM,1918.75,0.0
,SYB,5132.03125,0.0
,MultiRec,3340.625,0.0
,Regular,1289.84375,0.0
Show/Logic,Hand,133.59375,0.0
,EMGM,243.75,0.0
,SYB,425.78125,0.0
,MultiRec,354.6875,0.0
,Regular,146.09375,0.0
Update/Tree,Hand,207.03125,0.0
,EMGM,270.3125,0.0
,SYB,1947.65625,0.0
,MultiRec,753.125,0.0
,Regular,278.90625,0.0
Update/Logic,Hand,517.1875,0.0
,EMGM,1460.9375,0.0
,SYB,1366.40625,0.0
,MultiRec,1592.1875,0.0
,Regular,880.46875,0.0
-------------------------------------
