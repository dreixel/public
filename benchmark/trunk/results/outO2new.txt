
Results:
Number of repetitions: 10
Compiler flags: -O2 -fforce-recomp
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,103.125
,EMGM,106.25
,SYB,2223.4375
,MultiRec,209.375
,Regular,175.0
Eq/Logic,Hand,2609.375
,EMGM,3500.0
,SYB,5215.625
,MultiRec,2990.625
,Regular,3039.0625
Map/Tree,Hand,93.75
,EMGM,95.3125
,SYB,757.8125
Read/Tree,Hand,182.8125
,EMGM,725.0
,SYB,1451.5625
,MultiRec,778.125
,Regular,739.0625
Read/Logic,Hand,562.5
,EMGM,2882.8125
,SYB,2890.625
Show/Tree,Hand,545.3125
,EMGM,764.0625
,SYB,2043.75
,MultiRec,1357.8125
,Regular,575.0
Show/Logic,Hand,3528.125
,EMGM,4704.6875
,SYB,5523.4375
,Regular,4248.4375
-------------------------------------
