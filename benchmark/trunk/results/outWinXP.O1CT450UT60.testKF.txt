
Results:
Number of repetitions: 20
Compiler flags: -O1 -funfolding-creation-threshold=450 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,235.9375,0.0
,EMGM,261.71875,0.0
,SYB,4262.5,0.0
,MultiRec,565.625,0.0
,Regular,462.5,0.0
Eq/Logic,Hand,208.59375,0.0
,EMGM,206.25,0.0
,SYB,1427.34375,0.0
,MultiRec,350.0,0.0
,Regular,310.9375,0.0
Map/Tree,Hand,230.46875,0.0
,EMGM,248.4375,0.0
,SYB,2132.03125,0.0
Read/Tree,Hand,178.90625,0.0
,EMGM,730.46875,0.0
,SYB,1914.84375,0.0
,MultiRec,734.375,0.0
,Regular,731.25,0.0
Read/Logic,Hand,490.625,0.0
,EMGM,1671.875,0.0
,SYB,5663.28125,0.0
Show/Tree,Hand,1188.28125,0.0
,EMGM,1084.375,0.0
,SYB,5117.1875,0.0
,MultiRec,2902.34375,0.0
,Regular,975.0,0.0
Show/Logic,Hand,128.90625,0.0
,EMGM,153.125,0.0
,SYB,384.375,0.0
,MultiRec,346.875,0.0
,Regular,135.9375,0.0
Update/Tree,Hand,210.9375,0.0
,EMGM,256.25,0.0
,SYB,1821.875,0.0
,MultiRec,762.5,0.0
,Regular,202.34375,0.0
Update/Logic,Hand,503.90625,0.0
,EMGM,468.75,0.0
,SYB,2767.96875,0.0
,MultiRec,1607.8125,0.0
,Regular,487.5,0.0
-------------------------------------
