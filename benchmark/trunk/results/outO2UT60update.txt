
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Update/Tree,Hand,82.03125
,EMGM,491.40625
,SYB,720.3125
,MultiRec,303.125
,Regular,85.9375
Update/Logic,Hand,2719.53125
,EMGM,2603.90625
,SYB,2616.40625
,MultiRec,2585.15625
,Regular,2562.5
-------------------------------------
