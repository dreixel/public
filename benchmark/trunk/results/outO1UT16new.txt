
Results:
Number of repetitions: 10
Compiler flags: -O1 -fforce-recomp -funfolding-use-threshold=16
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,98.4375
,EMGM,104.6875
,SYB,1737.5
,MultiRec,201.5625
,Regular,171.875
Eq/Logic,Hand,2620.3125
,EMGM,3360.9375
,SYB,4598.4375
,MultiRec,2995.3125
,Regular,2968.75
Map/Tree,Hand,92.1875
,EMGM,103.125
,SYB,742.1875
Read/Tree,Hand,192.1875
,EMGM,739.0625
,SYB,1865.625
,MultiRec,781.25
,Regular,740.625
Read/Logic,Hand,553.125
,EMGM,2826.5625
,SYB,4351.5625
Show/Tree,Hand,534.375
,EMGM,784.375
,SYB,1989.0625
,MultiRec,1357.8125
,Regular,398.4375
Show/Logic,Hand,3587.5
,EMGM,4715.625
,SYB,5315.625
,Regular,3848.4375
-------------------------------------
