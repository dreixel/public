
Results:
Number of repetitions: 20
Compiler flags: -fforce-recomp -O2
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Multirec,212.5
Multirec2,222.65625
-------------------------------------
Test,Show
Multirec,1258.59375
Multirec2,1678.90625
-------------------------------------
