
Results:
Number of repetitions: 10
Compiler flags: -O1
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,220.3125,0.0
,EMGM,250.0,0.0
,SYB,5487.5,0.0
,MultiRec,568.75,0.0
,Regular,467.1875,0.0
Eq/Logic,Hand,696.875,0.0
,EMGM,1804.6875,0.0
,SYB,11471.875,0.0
,MultiRec,1245.3125,0.0
,Regular,1203.125,0.0
Map/Tree,Hand,1081.25,0.0
,EMGM,1090.625,0.0
,SYB,13087.5,0.0
Read/Tree,Hand,478.125,0.0
,EMGM,3757.8125,0.0
,SYB,9289.0625,0.0
,MultiRec,3915.625,0.0
,Regular,3789.0625,0.0
Read/Logic,Hand,500.0,0.0
,EMGM,1714.0625,0.0
,SYB,4212.5,0.0
Show/Tree,Hand,989.0625,0.0
,EMGM,1790.625,0.0
,SYB,5159.375,0.0
,MultiRec,3342.1875,0.0
,Regular,1300.0,0.0
Show/Logic,Hand,126.5625,0.0
,EMGM,225.0,0.0
,SYB,434.375,0.0
,MultiRec,357.8125,0.0
,Regular,148.4375,0.0
Update/Tree,Hand,562.5,0.0
,EMGM,785.9375,0.0
,SYB,5739.0625,0.0
,MultiRec,2589.0625,0.0
,Regular,775.0,0.0
Update/Logic,Hand,1670.3125,0.0
,EMGM,9429.6875,0.0
,SYB,10528.125,0.0
,MultiRec,12185.9375,0.0
,Regular,4637.5,0.0
-------------------------------------
