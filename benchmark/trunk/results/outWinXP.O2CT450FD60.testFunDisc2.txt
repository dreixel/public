
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450 -funfolding-fun-discount=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,230.46875,0.0
,EMGM,239.0625,0.0
,SYB,3746.875,0.0
,MultiRec,533.59375,0.0
,Regular,465.625,0.0
Eq/Logic,Hand,180.46875,0.0
,EMGM,220.3125,0.0
,SYB,1479.6875,0.0
,MultiRec,310.15625,0.0
,Regular,325.78125,0.0
Map/Tree,Hand,234.375,0.0
,EMGM,234.375,0.0
,SYB,1591.40625,0.0
Read/Tree,Hand,171.875,0.0
,EMGM,663.28125,0.0
,SYB,1305.46875,0.0
,MultiRec,691.40625,0.0
,Regular,675.0,0.0
Read/Logic,Hand,480.46875,0.0
,EMGM,1525.78125,0.0
,SYB,4105.46875,0.0
Show/Tree,Hand,1178.90625,0.0
,EMGM,1414.84375,0.0
,SYB,4581.25,0.0
,MultiRec,2615.625,0.0
,Regular,1214.84375,0.0
Show/Logic,Hand,121.875,0.0
,EMGM,161.71875,0.0
,SYB,400.78125,0.0
,MultiRec,307.03125,0.0
,Regular,135.9375,0.0
Update/Tree,Hand,184.375,0.0
,EMGM,257.03125,0.0
,SYB,1831.25,0.0
,MultiRec,699.21875,0.0
,Regular,257.8125,0.0
Update/Logic,Hand,452.34375,0.0
,EMGM,643.75,0.0
,SYB,1261.71875,0.0
,MultiRec,1484.375,0.0
,Regular,801.5625,0.0
-------------------------------------
