
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp -funfolding-creation-threshold=180 -funfolding-use-threshold=64
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Hand,88.28125
EMGM,92.96875
SYB,1495.3125
Multirec,210.15625
Regular,181.25
-------------------------------------
Test,Map
Hand,92.1875
EMGM,94.53125
SYB,696.875
-------------------------------------
Test,Read
Hand,26.5625
EMGM,39.84375
SYB,46.875
-------------------------------------
Test,Show
Hand,439.0625
EMGM,397.65625
SYB,1840.625
Multirec,1353.90625
Regular,405.46875
-------------------------------------
