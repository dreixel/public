
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Regular,171.875
RegularDeep,645.3125
-------------------------------------
Test,Read
Regular,48.4375
RegularDeep,48.4375
-------------------------------------
Test,Show
Regular,554.6875
RegularDeep,657.8125
-------------------------------------
