
Results:
Number of repetitions: 20
Compiler flags: -O2 -fforce-recomp -funfolding-use-threshold=64
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Regular,179.6875
RegularDeep,612.5
-------------------------------------
Test,Read
Regular,46.09375
RegularDeep,46.875
-------------------------------------
Test,Show
Regular,414.84375
RegularDeep,537.5
-------------------------------------
