
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,230.46875
,EMGM,2346.09375
,SYB,3871.875
,MultiRec,557.03125
,Regular,456.25
Eq/Logic,Hand,185.15625
,EMGM,515.625
,SYB,1518.75
,MultiRec,325.78125
,Regular,298.4375
Map/Tree,Hand,240.625
,EMGM,1113.28125
,SYB,1742.96875
Read/Tree,Hand,169.53125
,EMGM,655.46875
,SYB,1675.78125
,MultiRec,682.8125
,Regular,670.3125
Read/Logic,Hand,465.625
,EMGM,1579.6875
,SYB,5240.625
Show/Tree,Hand,1142.1875
,EMGM,2663.28125
,SYB,4870.3125
,MultiRec,3284.375
,Regular,982.03125
Show/Logic,Hand,128.90625
,EMGM,235.15625
,SYB,358.59375
,MultiRec,360.9375
,Regular,128.125
Update/Tree,Hand,200.0
,EMGM,1131.25
,SYB,1798.4375
,MultiRec,746.09375
,Regular,207.8125
Update/Logic,Hand,457.8125
,EMGM,1485.9375
,SYB,2432.03125
,MultiRec,1625.0
,Regular,457.8125
-------------------------------------
