
Results:
Number of repetitions: 20
Compiler flags: -O1
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,255.46875,0.0
,EMGM,245.3125,0.0
,SYB,5564.0625,0.0
,MultiRec,572.65625,0.0
,Regular,520.3125,0.0
Eq/Logic,Hand,203.90625,0.0
,EMGM,560.9375,0.0
,SYB,2129.6875,0.0
,MultiRec,363.28125,0.0
,Regular,354.6875,0.0
Map/Tree,Hand,222.65625,0.0
,EMGM,237.5,0.0
,SYB,1871.875,0.0
Read/Tree,Hand,178.125,0.0
,EMGM,710.9375,0.0
,SYB,1442.96875,0.0
,MultiRec,807.8125,0.0
,Regular,762.5,0.0
Read/Logic,Hand,508.59375,0.0
,EMGM,1707.8125,0.0
,SYB,4250.0,0.0
Show/Tree,Hand,1257.8125,0.0
,EMGM,1852.34375,0.0
,SYB,4991.40625,0.0
,MultiRec,3439.84375,0.0
,Regular,1344.53125,0.0
Show/Logic,Hand,121.875,0.0
,EMGM,226.5625,0.0
,SYB,425.0,0.0
,MultiRec,383.59375,0.0
,Regular,155.46875,0.0
Update/Tree,Hand,200.0,0.0
,EMGM,243.75,0.0
,SYB,1804.6875,0.0
,MultiRec,769.53125,0.0
,Regular,288.28125,0.0
Update/Logic,Hand,485.15625,0.0
,EMGM,1432.03125,0.0
,SYB,1298.4375,0.0
,MultiRec,1685.15625,0.0
,Regular,950.0,0.0
-------------------------------------
