
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450 -funfolding-keeness-factor=8
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,231.25,0.0
,EMGM,254.6875,0.0
,SYB,4071.875,0.0
,MultiRec,592.1875,0.0
,Regular,478.90625,0.0
Eq/Logic,Hand,197.65625,0.0
,EMGM,245.3125,0.0
,SYB,1464.84375,0.0
,MultiRec,345.3125,0.0
,Regular,352.34375,0.0
Map/Tree,Hand,257.8125,0.0
,EMGM,236.71875,0.0
,SYB,1802.34375,0.0
Read/Tree,Hand,184.375,0.0
,EMGM,716.40625,0.0
,SYB,1867.1875,0.0
,MultiRec,779.6875,0.0
,Regular,742.1875,0.0
Read/Logic,Hand,512.5,0.0
,EMGM,1658.59375,0.0
,SYB,5671.875,0.0
Show/Tree,Hand,1286.71875,0.0
,EMGM,1417.96875,0.0
,SYB,5146.09375,0.0
,MultiRec,2938.28125,0.0
,Regular,1271.875,0.0
Show/Logic,Hand,128.125,0.0
,EMGM,155.46875,0.0
,SYB,410.9375,0.0
,MultiRec,346.09375,0.0
,Regular,137.5,0.0
Update/Tree,Hand,203.125,0.0
,EMGM,268.75,0.0
,SYB,1910.15625,0.0
,MultiRec,764.84375,0.0
,Regular,288.28125,0.0
Update/Logic,Hand,475.0,0.0
,EMGM,571.09375,0.0
,SYB,2556.25,0.0
,MultiRec,1638.28125,0.0
,Regular,854.6875,0.0
-------------------------------------
