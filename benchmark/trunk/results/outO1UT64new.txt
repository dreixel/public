
Results:
Number of repetitions: 10
Compiler flags: -O1 -fforce-recomp -funfolding-use-threshold=64
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,92.1875
,EMGM,100.0
,SYB,1600.0
,MultiRec,204.6875
,Regular,168.75
Eq/Logic,Hand,2414.0625
,EMGM,3585.9375
,SYB,4656.25
,MultiRec,2921.875
,Regular,2868.75
Map/Tree,Hand,95.3125
,EMGM,93.75
,SYB,779.6875
Read/Tree,Hand,190.625
,EMGM,731.25
,SYB,1840.625
,MultiRec,723.4375
,Regular,742.1875
Read/Logic,Hand,539.0625
,EMGM,2814.0625
,SYB,4334.375
Show/Tree,Hand,479.6875
,EMGM,751.5625
,SYB,1982.8125
,MultiRec,1315.625
,Regular,389.0625
Show/Logic,Hand,3515.625
,EMGM,4551.5625
,SYB,5273.4375
,Regular,3792.1875
-------------------------------------
