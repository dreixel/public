
Results:
Number of repetitions: 10
Compiler flags: -O1 -funfolding-creation-threshold=450 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,220.3125,0.0
,EMGM,242.1875,0.0
,SYB,4037.5,0.0
,MultiRec,556.25,0.0
,Regular,467.1875,0.0
Eq/Logic,Hand,712.5,0.0
,EMGM,754.6875,0.0
,SYB,7315.625,0.0
,MultiRec,1250.0,0.0
,Regular,1123.4375,0.0
Map/Tree,Hand,1168.75,0.0
,EMGM,1182.8125,0.0
,SYB,12681.25,0.0
Read/Tree,Hand,471.875,0.0
,EMGM,3746.875,0.0
,SYB,12162.5,0.0
,MultiRec,3776.5625,0.0
,Regular,3750.0,0.0
Read/Logic,Hand,484.375,0.0
,EMGM,1573.4375,0.0
,SYB,5518.75,0.0
Show/Tree,Hand,940.625,0.0
,EMGM,1075.0,0.0
,SYB,4918.75,0.0
,MultiRec,2792.1875,0.0
,Regular,984.375,0.0
Show/Logic,Hand,125.0,0.0
,EMGM,142.1875,0.0
,SYB,379.6875,0.0
,MultiRec,339.0625,0.0
,Regular,129.6875,0.0
Update/Tree,Hand,562.5,0.0
,EMGM,714.0625,0.0
,SYB,5606.25,0.0
,MultiRec,2676.5625,0.0
,Regular,554.6875,0.0
Update/Logic,Hand,1656.25,0.0
,EMGM,1612.5,0.0
,SYB,15554.6875,0.0
,MultiRec,12435.9375,0.0
,Regular,1682.8125,0.0
-------------------------------------
