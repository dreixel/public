
Results:
Number of repetitions: 20
Compiler flags: -O1
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,252.34375,0.0
,EMGM,257.03125,0.0
,SYB,5788.28125,0.0
,MultiRec,578.125,0.0
,Regular,483.59375,0.0
Eq/Logic,Hand,207.8125,0.0
,EMGM,568.75,0.0
,SYB,2157.03125,0.0
,MultiRec,347.65625,0.0
,Regular,331.25,0.0
Map/Tree,Hand,219.53125,0.0
,EMGM,247.65625,0.0
,SYB,1916.40625,0.0
Read/Tree,Hand,185.15625,0.0
,EMGM,721.875,0.0
,SYB,1469.53125,0.0
,MultiRec,766.40625,0.0
,Regular,746.09375,0.0
Read/Logic,Hand,502.34375,0.0
,EMGM,1750.78125,0.0
,SYB,4300.0,0.0
Show/Tree,Hand,1277.34375,0.0
,EMGM,1875.78125,0.0
,SYB,5091.40625,0.0
,MultiRec,3335.9375,0.0
,Regular,1299.21875,0.0
Show/Logic,Hand,122.65625,0.0
,EMGM,232.8125,0.0
,SYB,425.78125,0.0
,MultiRec,364.0625,0.0
,Regular,156.25,0.0
Update/Tree,Hand,203.125,0.0
,EMGM,249.21875,0.0
,SYB,1843.75,0.0
,MultiRec,742.96875,0.0
,Regular,271.875,0.0
Update/Logic,Hand,487.5,0.0
,EMGM,1466.40625,0.0
,SYB,1310.9375,0.0
,MultiRec,1642.96875,0.0
,Regular,917.1875,0.0
-------------------------------------
