
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp -funfolding-use-threshold=256
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Read
Hand,185.15625
EMGM,706.25
SYB,1815.625
Regular,735.15625
RegularDeep,728.90625
-------------------------------------
