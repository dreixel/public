
Results:
Number of repetitions: 20
Compiler flags: -O1 -funfolding-creation-threshold=450 -funfolding-keeness-factor=8
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,213.28125,0.0
,EMGM,261.71875,0.0
,SYB,4245.3125,0.0
,MultiRec,576.5625,0.0
,Regular,480.46875,0.0
Eq/Logic,Hand,203.125,0.0
,EMGM,242.1875,0.0
,SYB,1489.84375,0.0
,MultiRec,349.21875,0.0
,Regular,328.90625,0.0
Map/Tree,Hand,223.4375,0.0
,EMGM,225.0,0.0
,SYB,1929.6875,0.0
Read/Tree,Hand,181.25,0.0
,EMGM,742.96875,0.0
,SYB,1914.84375,0.0
,MultiRec,771.875,0.0
,Regular,737.5,0.0
Read/Logic,Hand,517.1875,0.0
,EMGM,1662.5,0.0
,SYB,5635.15625,0.0
Show/Tree,Hand,1323.4375,0.0
,EMGM,1546.875,0.0
,SYB,5059.375,0.0
,MultiRec,2911.71875,0.0
,Regular,1210.9375,0.0
Show/Logic,Hand,128.125,0.0
,EMGM,147.65625,0.0
,SYB,392.96875,0.0
,MultiRec,343.75,0.0
,Regular,134.375,0.0
Update/Tree,Hand,198.4375,0.0
,EMGM,266.40625,0.0
,SYB,1820.3125,0.0
,MultiRec,770.3125,0.0
,Regular,278.125,0.0
Update/Logic,Hand,492.96875,0.0
,EMGM,574.21875,0.0
,SYB,2681.25,0.0
,MultiRec,1637.5,0.0
,Regular,905.46875,0.0
-------------------------------------
