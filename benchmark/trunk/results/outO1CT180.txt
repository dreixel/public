
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp -funfolding-creation-threshold=180
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Hand,109.375
EMGM,107.03125
SYB,1983.59375
Multirec,206.25
Regular,160.15625
-------------------------------------
Test,Map
Hand,95.3125
EMGM,87.5
SYB,694.53125
-------------------------------------
Test,Read
Hand,28.125
EMGM,44.53125
SYB,42.96875
-------------------------------------
Test,Show
Hand,525.78125
EMGM,653.125
SYB,1878.125
Multirec,1435.15625
Regular,519.53125
-------------------------------------
