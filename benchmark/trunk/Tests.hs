{-# LANGUAGE ScopedTypeVariables #-}

module Tests (Library(..), TestName(..), Test(..), Datatype(..), tests) where


--------------------------------------------------------------------------------
-- Datatypes for representing libraries, tests, and datatypes tested on
--------------------------------------------------------------------------------

data Library = Hand          -- Handwritten code
             | HandPar       -- Handwritten code, parallelised
             | SYB           -- syb-0.1
             | MultiRec      -- multirec-0.4
             | MultiRecInline-- multirec-0.4 with INLINE pragmas
             | Instant       -- instant-generics-0.1
             | InstantInline -- instant-generics-0.1 with INLINE pragmas
             | Deriving      -- generic-deriving-0.4 with INLINE pragmas
                deriving (Eq, Ord, Show)

data TestName = Eq 
              | Map 
              | Read
              | Show
              | Update      -- Traversals
              | Arbitrary   -- QuickCheck's (1.2)
              | Enum
              | Decode
                 deriving (Eq, Ord, Show)

data Test = Test { lib :: Library,
                   testName :: TestName,
                   datatype :: Datatype
                 } deriving (Eq, Ord, Show)

data Datatype = Tree    -- Labelled binary trees
              | Logic   -- Logic expressions
              | Nat     -- Peano naturals
              | List    -- Haskell lists
                deriving (Eq, Ord, Show)


--------------------------------------------------------------------------------
-- Tests
--------------------------------------------------------------------------------

allTests, handTests, sybTests, multirecTests, instantTests, instantInlineTests,
  multirecInlineTests, derivingTests, tests :: [Test]
handTests = [ Test Hand Eq     Tree
            , Test Hand Map    Tree
            , Test Hand Read   Tree
            , Test Hand Show   Tree
            , Test Hand Update Tree
            , Test Hand Enum   Tree
            , Test Hand Enum   Nat
            , Test Hand Decode Tree
            , Test Hand Eq     Logic
            , Test Hand Read   Logic
            , Test Hand Show   Logic
            , Test Hand Update Logic
            , Test Hand Enum   Logic
            , Test Hand Decode Logic]

handParTests = [ Test HandPar Eq     Tree
               , Test HandPar Map    Tree
               --, Test HandPar Read   Tree
               , Test HandPar Show   Tree
               , Test HandPar Update Tree
               , Test HandPar Enum   Tree
               --, Test HandPar Decode Tree
               , Test HandPar Eq     Logic
               --, Test HandPar Read   Logic
               , Test HandPar Show   Logic
               , Test HandPar Update Logic
               , Test HandPar Enum   Logic
               --, Test HandPar Decode Logic
               ]

sybTests = [ Test SYB Eq     Tree
           , Test SYB Map    Tree
           , Test SYB Read   Tree
           , Test SYB Show   Tree
           , Test SYB Update Tree
           , Test SYB Enum   Tree
           , Test SYB Eq     Logic
           , Test SYB Read   Logic
           , Test SYB Show   Logic
           , Test SYB Update Logic
           , Test SYB Enum   Logic]

multirecTests = [ Test MultiRec Eq     Tree
                , Test MultiRec Show   Tree
                , Test MultiRec Read   Tree
                , Test MultiRec Update Tree
                , Test MultiRec Eq     Logic
                , Test MultiRec Show   Logic
                -- , Test MultiRec Read   Logic
                , Test MultiRec Update Logic]

multirecInlineTests = [ Test MultiRecInline Eq     Tree
                      -- , Test MultiRecInline Show   Tree
                      -- , Test MultiRecInline Read   Tree
                      , Test MultiRecInline Update Tree
                      , Test MultiRecInline Eq     Logic
                      -- , Test MultiRecInline Show   Logic
                      -- , Test MultiRecInline Read   Logic
                      -- , Test MultiRecInline Update Logic
                      ]

instantTests = [ Test Instant Eq     Tree
               , Test Instant Show   Tree
               , Test Instant Update Tree
               , Test Instant Enum   Tree
               , Test Instant Enum   Nat
               , Test Instant Decode Tree
               , Test Instant Eq     Logic
               , Test Instant Show   Logic
               , Test Instant Update Logic
               , Test Instant Enum   Logic
               , Test Instant Decode Logic]

instantInlineTests = [ Test InstantInline Eq     Tree
                     , Test InstantInline Show   Tree
                     , Test InstantInline Update Tree
                     , Test InstantInline Enum   Tree
                     , Test InstantInline Enum   Nat
                     , Test InstantInline Decode Tree
                     , Test InstantInline Eq     Logic
                     , Test InstantInline Show   Logic
                     , Test InstantInline Update Logic
                     , Test InstantInline Enum   Logic
                     , Test InstantInline Decode Logic]

derivingTests = [ Test Deriving Eq     Tree
                , Test Deriving Map    Tree
                , Test Deriving Show   Tree
                -- , Test Deriving Update Tree
                , Test Deriving Enum   Tree
                -- , Test Deriving Decode Tree
                , Test Deriving Eq     Logic
                , Test Deriving Show   Logic
                -- , Test Deriving Update Logic
                , Test Deriving Enum   Logic
                -- , Test Deriving Decode Logic
                , Test Deriving Enum   Nat
                ]


allTests =    handTests ++ handParTests
           ++ sybTests
           ++ multirecTests ++ multirecInlineTests
           ++ instantTests  ++ instantInlineTests
           ++ derivingTests

tests = [ t | t <- allTests
        --, testName t `elem` [Show]
        , lib t `elem` [Hand, Deriving]
        -- , datatype t == Nat
        ]
