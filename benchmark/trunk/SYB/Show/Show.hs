module SYB.Show.Show where

import Data.Data
import Data.Generics (extQ)

-- This file should not compile so it's not accidentally imported
error

------------------------------------------------------------------------------

-- | Generic show: an alternative to \"deriving Show\"
gshow :: Data a => a -> String

-- This is a prefix-show using surrounding "(" and ")",
-- where we recurse into subterms with gmapQ.
-- 
gshow = ( \t ->
                "("
             ++ showConstr (toConstr t)
             ++ concat (gmapQ ((++) " " . gshow) t)
             ++ ")"
        ) `extQ` (show :: String -> String)
