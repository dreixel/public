module SYB.Show.ShowOpt where

import Data.Generics (extQ, Data, showConstr, toConstr, gmapQ)

------------------------------------------------------------------------------
paren :: Bool -> ShowS -> ShowS
paren cond str = if not cond then str else showString "(" . str . showString ")"

-- | Generic show: an alternative to \"deriving Show\"
gshow :: Data a => a -> String
gshow x = gshowGeneral x ""

-- | Generic shows
gshows :: Data a => a -> ShowS

-- This is a prefix-show using surrounding "(" and ")",
-- where we recurse into subterms with gmapQ.
gshows = gshowGeneral `extQ` gshowString

gshowGeneral :: Data a => a -> ShowS
gshowGeneral t = let con    = showConstr . toConstr $ t
                     fields = gmapQ ((showChar ' ' .) . gshows) t
                 in paren (not (null fields)) $ 
                      showString con . foldr (.) id fields

gshowString :: String -> ShowS
gshowString = shows
