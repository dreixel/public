{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}

module SYB.Enum.Enum (enumTree, enumLogic) where

import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))
import Auxiliary.Auxiliary (diag)
import Auxiliary.SYB ()
import Data.Generics


-- Very nice, but terribly inefficient.
enum :: forall a. (Data a) => [a]
enum = general `extB` string `extB` int where
         general :: (Data a) => [a]
         general = diag [ fromConstrM enum (indexConstr dt i) 
                          | i <- [1..(maxConstrIndex dt)] ]
         string = ["x"]
         int = [0::Int]
         dt :: DataType
         dt = dataTypeOf (unList general)
         unList :: Data a => [a] -> a
         unList = error "Should never be evaluated"

enumTree :: [Tree Int]
enumTree = enum

enumLogic :: [Logic]
enumLogic = enum
