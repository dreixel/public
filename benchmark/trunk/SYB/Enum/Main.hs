module SYB.Enum.Main where

import Auxiliary.Tree (Tree(..), seqTree)
import Auxiliary.Logic (Logic(..))
import Auxiliary.Auxiliary (test)

import SYB.Enum.Enum

mainTree :: IO ()
mainTree = test . print $ seqTree (enumTree !! 3000) "x"

mainLogic :: IO ()
mainLogic = test . print $ (enumLogic !! 100000) == (enumLogic !! 100001)
