{-# OPTIONS_GHC -fglasgow-exts #-}

module SYB.Map.Map where

import Data.Generics (Data, Typeable, gmapT, gcast)


-- | Make a generic transformation;
--   start from a type-specific case;
--   preserve the term otherwise
mkT :: ( Typeable a
       , Typeable b)
    => (b -> b)
    -> a
    -> a
mkT = extT id

-- | Flexible type extension
ext0 :: (Typeable a, Typeable b) => c a -> c b -> c a
ext0 def ext = maybe def id (gcast ext)

-- | Extend a generic transformation by a type-specific case
extT :: ( Typeable a
        , Typeable b
        )
     => (a -> a)
     -> (b -> b)
     -> a
     -> a
extT def ext = unT ((T def) `ext0` (T ext))

-- | The type constructor for transformations
newtype T x = T { unT :: x -> x }

-- | Apply a transformation everywhere in bottom-up manner
everywhere :: (forall a. Data a => a -> a)
           -> (forall a. Data a => a -> a)

-- Use gmapT to recurse into immediate subterms;
-- recall: gmapT preserves the outermost constructor;
-- post-process recursively transformed result via f
everywhere f = f . gmapT (everywhere f)
