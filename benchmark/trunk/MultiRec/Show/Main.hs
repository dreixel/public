module MultiRec.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.MultiRec
import Auxiliary.Auxiliary (test)
import qualified MultiRec.Show.Show as G

mainTree :: IO ()
mainTree = test . putStr . show . last . G.show Tree $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . last . G.show Logic $ bigLogic
