module MultiRec.Map.Main where

import Tree (bigTree, sumTree)
import Generics.MultiRec.Fold


main :: IO ()
main = let t = undefined -- (+1) bigTree
       in putStrLn . show . sumTree $ t
