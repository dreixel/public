{-# LANGUAGE DeriveDataTypeable    #-}

module Auxiliary.Tree (
    Tree(..),
    bigTrees, bigTree, bigTreeString,
    smallerTrees, smallerTree, smallerTreeString,
    seqTree, mapTree, sumTree, tweakRightmost
  ) where

import Data.Generics (Data, Typeable, gshow)
import System.Random


-- Tree datatype
data Tree a = Bin a (Tree a) (Tree a) | Leaf deriving (Data, Typeable, Show, Eq)


-- Fully seq a Tree
seqTree :: Tree a -> b -> b
seqTree Leaf b          = b
seqTree (Bin a t1 t2) b = a `seq` (seqTree t1 b) `seq` (seqTree t2 b)

-- Sum all the values of a Tree Int
sumTree :: Tree Int -> Int
sumTree Leaf        = 0
sumTree (Bin i l r) = i + (sumTree l) + (sumTree r)

-- Map on a Tree
mapTree :: (a -> b) -> Tree a -> Tree b
mapTree _  Leaf       = Leaf
mapTree f (Bin x l r) = Bin (f x) (mapTree f l) (mapTree f r)

-- Tweak the rightmost element of a Tree
tweakRightmost :: Tree Int -> Tree Int
tweakRightmost Leaf = Leaf
tweakRightmost (Bin x l Leaf) = Bin (x+1) l Leaf
tweakRightmost (Bin x l r) = Bin x l (tweakRightmost r)


genTree :: [Int] -> Tree Int
genTree []    = Leaf
genTree [_]   = Leaf
genTree (h:t@(_:_)) | even h = Bin h (genTree t) (genTree (tail t))
                    | odd h  = Bin h (genTree (tail t)) (genTree t)


-- Big trees (size 1542685, 36924 even labels, 477304 odd)
bigTrees :: [Tree Int]
bigTrees = [bigTreeGen seed | seed <- [123456789..223456789]]

bigTreeGen :: Int -> Tree Int
bigTreeGen seed = genTree . take 30 $ randomRs (0,100) (mkStdGen seed)

bigTree :: Tree Int
bigTree = head bigTrees

bigTreeString :: String
bigTreeString = gshow bigTree

-- Smaller trees (size 85969)
smallerTrees :: [Tree Int]
smallerTrees = [smallerTreeGen seed | seed <- [123456789..223456789]]

smallerTreeGen :: Int -> Tree Int
smallerTreeGen seed = genTree . take 20 $ randomRs (0,100) (mkStdGen seed)

smallerTree :: Tree Int
smallerTree = head smallerTrees

smallerTreeString :: String
smallerTreeString = gshow smallerTree
