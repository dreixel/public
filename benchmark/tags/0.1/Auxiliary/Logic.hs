{-# LANGUAGE DeriveDataTypeable    #-}

module Auxiliary.Logic (
    Logic(..)
  , seqLogic, tweakLogic, evalLogic, hashLogic, Int32
  , bigLogic, bigLogicString, biggerLogic, bigLogics
  , smallerLogic, smallerLogicString, smallerLogics
  ) where

import Data.Generics (Data, Typeable, gshow)
import Data.HashTable (hashString)
import Data.Int (Int32(..))
import System.Random


-- Logic datatype
data Logic = Var String
           | Impl  Logic Logic            -- implication
           | Equiv Logic Logic            -- equivalence
           | Conj  Logic Logic            -- and (conjunction)
           | Disj  Logic Logic            -- or (disjunction)
           | Not Logic                    -- not
           | T                            -- true
           | F                            -- false
 deriving (Data, Typeable, Show, Eq)

-- Fully seq a Logic expression
seqLogic :: Logic -> a -> a
seqLogic (Var s)     b = seq s b
seqLogic (Impl p  q) b = seqLogic p (seqLogic q b)
seqLogic (Equiv p q) b = seqLogic p (seqLogic q b)
seqLogic (Conj p  q) b = seqLogic p (seqLogic q b)
seqLogic (Disj p  q) b = seqLogic p (seqLogic q b)
seqLogic (Not p)     b = seqLogic p b
seqLogic T           b = b
seqLogic F           b = b

-- Evaluate a Logic expression
evalLogic :: (String -> Bool) -> Logic -> Bool
evalLogic env (Var s)     = env s
evalLogic env (Impl  p q) = evalLogic env (Not p) || evalLogic env q
evalLogic env (Equiv p q) = evalLogic env p == evalLogic env q
evalLogic env (Conj  p q) = evalLogic env p && evalLogic env q
evalLogic env (Disj  p q) = evalLogic env p || evalLogic env q
evalLogic env (Not p)     = not (evalLogic env p)
evalLogic env T           = True
evalLogic env F           = False

hashLogic :: Logic -> Int32
hashLogic = hashString . show

-- Change all T into F
tweakLogic :: Logic -> Logic
tweakLogic (Var s)     = Var s
tweakLogic (Impl  p q) = Impl  (tweakLogic p) (tweakLogic q)
tweakLogic (Equiv p q) = Equiv (tweakLogic p) (tweakLogic q)
tweakLogic (Conj  p q) = Conj  (tweakLogic p) (tweakLogic q)
tweakLogic (Disj  p q) = Disj  (tweakLogic p) (tweakLogic q)
tweakLogic (Not p)     = Not (tweakLogic p)
tweakLogic T           = F
tweakLogic F           = F


genLogic :: [Int] -> Int -> Logic
genLogic []  n = case rem n 3 of
                   0 -> Var ("x" ++ show (rem n 10))
                   1 -> T
                   _ -> F
genLogic [x] _ = genLogic [] x
genLogic l   n = let (l1, l2) = (tail l, tail (tail l))
                 in case rem (head l) 5 of
                      0 -> Impl  (genLogic l1 n) (genLogic l2 n)
                      1 -> Equiv (genLogic l1 n) (genLogic l2 n)
                      2 -> Conj  (genLogic l1 n) (genLogic l2 n)
                      4 -> Disj  (genLogic l1 n) (genLogic l2 n)
                      _ -> Not (genLogic l1 n)


bigLogics :: [Logic]
bigLogics = [bigLogicGen seed | seed <- [123456789..223456789]]

bigLogicGen :: Int -> Logic
bigLogicGen seed = let (s1, s2) = select $ randomRs (0,100) (mkStdGen seed)
                       select (h:t) = (take 29 t, h)
                   in genLogic s1 s2
                   
biggerLogic :: Logic
biggerLogic = let (s1, s2) = select $ randomRs (0,100) (mkStdGen 123456789)
                  select (h:t) = (take 34 t, h)
              in genLogic s1 s2

-- Big Logic expression (size 810250)
bigLogic :: Logic
bigLogic = head bigLogics

bigLogicString :: String
bigLogicString = gshow bigLogic

smallerLogics :: [Logic]
smallerLogics = [smallerLogicGen seed | seed <- [123456788..223456789]]

smallerLogicGen :: Int -> Logic
smallerLogicGen seed = let (s1, s2) = select $ randomRs (0,100) (mkStdGen seed)
                           select (h:t) = (take 23 t, h)
                       in genLogic s1 s2

-- Smaller Logic expression (size 37138)
smallerLogic :: Logic
smallerLogic = head smallerLogics

smallerLogicString :: String
smallerLogicString = gshow smallerLogic
