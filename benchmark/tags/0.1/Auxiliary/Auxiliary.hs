module Auxiliary.Auxiliary (test, apply) where

import System.CPUTime (getCPUTime)

test :: IO () -> IO ()
test t = do
          t1 <- getCPUTime
          t
          t2 <- getCPUTime
          let diff = fromInteger (t2 - t1) / (1000000000 :: Double)
          putStrLn ("\t" ++ show diff)

apply :: Int -> (a -> a) -> a -> a
apply 1 f = f
apply n f | n > 1 = f . apply (pred n) f
