
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450 -funfolding-fun-discount=180
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,222.65625,0.0
,EMGM,232.03125,0.0
,SYB,3848.4375,0.0
,MultiRec,538.28125,0.0
,Regular,464.0625,0.0
Eq/Logic,Hand,178.125,0.0
,EMGM,228.125,0.0
,SYB,1416.40625,0.0
,MultiRec,317.96875,0.0
,Regular,314.0625,0.0
Map/Tree,Hand,232.8125,0.0
,EMGM,233.59375,0.0
,SYB,1682.8125,0.0
Read/Tree,Hand,173.4375,0.0
,EMGM,659.375,0.0
,SYB,1375.78125,0.0
,MultiRec,713.28125,0.0
,Regular,672.65625,0.0
Read/Logic,Hand,460.15625,0.0
,EMGM,1570.3125,0.0
,SYB,4062.5,0.0
Show/Tree,Hand,1200.78125,0.0
,EMGM,1470.3125,0.0
,SYB,4656.25,0.0
,MultiRec,2642.1875,0.0
,Regular,1196.875,0.0
Show/Logic,Hand,119.53125,0.0
,EMGM,163.28125,0.0
,SYB,405.46875,0.0
,MultiRec,317.96875,0.0
,Regular,143.75,0.0
Update/Tree,Hand,182.8125,0.0
,EMGM,249.21875,0.0
,SYB,1770.3125,0.0
,MultiRec,703.90625,0.0
,Regular,254.6875,0.0
Update/Logic,Hand,450.78125,0.0
,EMGM,661.71875,0.0
,SYB,1278.90625,0.0
,MultiRec,1509.375,0.0
,Regular,807.8125,0.0
-------------------------------------
