
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=90
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,241.40625,0.0
,EMGM,253.90625,0.0
,SYB,5492.1875,0.0
,MultiRec,564.0625,0.0
,Regular,487.5,0.0
Eq/Logic,Hand,195.3125,0.0
,EMGM,246.875,0.0
,SYB,2064.84375,0.0
,MultiRec,335.15625,0.0
,Regular,344.53125,0.0
Map/Tree,Hand,252.34375,0.0
,EMGM,253.125,0.0
,SYB,1797.65625,0.0
Read/Tree,Hand,179.6875,0.0
,EMGM,731.25,0.0
,SYB,1450.0,0.0
,MultiRec,764.0625,0.0
,Regular,732.03125,0.0
Read/Logic,Hand,506.25,0.0
,EMGM,1653.125,0.0
,SYB,4253.90625,0.0
Show/Tree,Hand,1271.875,0.0
,EMGM,1843.75,0.0
,SYB,4928.125,0.0
,MultiRec,3346.875,0.0
,Regular,1286.71875,0.0
Show/Logic,Hand,123.4375,0.0
,EMGM,201.5625,0.0
,SYB,428.90625,0.0
,MultiRec,358.59375,0.0
,Regular,143.75,0.0
Update/Tree,Hand,200.0,0.0
,EMGM,278.125,0.0
,SYB,1886.71875,0.0
,MultiRec,758.59375,0.0
,Regular,274.21875,0.0
Update/Logic,Hand,485.9375,0.0
,EMGM,714.0625,0.0
,SYB,1362.5,0.0
,MultiRec,1592.96875,0.0
,Regular,880.46875,0.0
-------------------------------------
