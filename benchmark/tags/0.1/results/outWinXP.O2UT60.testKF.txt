
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,246.875,0.0
,EMGM,2376.5625,0.0
,SYB,4224.21875,0.0
,MultiRec,576.5625,0.0
,Regular,473.4375,0.0
Eq/Logic,Hand,184.375,0.0
,EMGM,535.15625,0.0
,SYB,1446.09375,0.0
,MultiRec,342.1875,0.0
,Regular,306.25,0.0
Map/Tree,Hand,255.46875,0.0
,EMGM,1160.15625,0.0
,SYB,1762.5,0.0
Read/Tree,Hand,180.46875,0.0
,EMGM,716.40625,0.0
,SYB,1885.15625,0.0
,MultiRec,722.65625,0.0
,Regular,722.65625,0.0
Read/Logic,Hand,496.875,0.0
,EMGM,1710.15625,0.0
,SYB,5642.96875,0.0
Show/Tree,Hand,1204.6875,0.0
,EMGM,2924.21875,0.0
,SYB,4921.09375,0.0
,MultiRec,3293.75,0.0
,Regular,982.03125,0.0
Show/Logic,Hand,125.78125,0.0
,EMGM,235.9375,0.0
,SYB,374.21875,0.0
,MultiRec,368.75,0.0
,Regular,136.71875,0.0
Update/Tree,Hand,208.59375,0.0
,EMGM,1134.375,0.0
,SYB,1922.65625,0.0
,MultiRec,814.84375,0.0
,Regular,206.25,0.0
Update/Logic,Hand,481.25,0.0
,EMGM,1530.46875,0.0
,SYB,2632.8125,0.0
,MultiRec,1608.59375,0.0
,Regular,475.78125,0.0
-------------------------------------
