
Results:
Number of repetitions: 10
Compiler flags: -O2 -fforce-recomp -funfolding-use-threshold=256
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,100.0
,EMGM,796.875
,SYB,1721.875
,MultiRec,218.75
,Regular,179.6875
Eq/Logic,Hand,2423.4375
,EMGM,3453.125
,SYB,4512.5
,MultiRec,3053.125
,Regular,2942.1875
Map/Tree,Hand,96.875
,EMGM,460.9375
,SYB,750.0
Read/Tree,Hand,184.375
,EMGM,723.4375
,SYB,1854.6875
,MultiRec,717.1875
,Regular,718.75
Read/Logic,Hand,554.6875
,EMGM,2770.3125
,SYB,4318.75
Show/Tree,Hand,501.5625
,EMGM,1123.4375
,SYB,1970.3125
,MultiRec,1292.1875
,Regular,407.8125
Show/Logic,Hand,3575.0
,EMGM,4665.625
,SYB,5421.875
,Regular,3882.8125
-------------------------------------
