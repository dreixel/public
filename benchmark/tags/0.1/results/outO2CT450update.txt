
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Update/Tree,Hand,85.15625
,EMGM,104.6875
,SYB,685.9375
,MultiRec,317.96875
,Regular,110.15625
Update/Logic,Hand,2576.5625
,EMGM,2546.09375
,SYB,2559.375
,MultiRec,2577.34375
,Regular,2623.4375
-------------------------------------
