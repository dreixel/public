
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=500 -funfolding-use-threshold=80
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,263.28125,0.0
,EMGM,277.34375,0.0
,SYB,4192.1875,0.0
,MultiRec,625.78125,0.0
,Regular,523.4375,0.0
Eq/Logic,Hand,212.5,0.0
,EMGM,224.21875,0.0
,SYB,1507.8125,0.0
,MultiRec,366.40625,0.0
,Regular,327.34375,0.0
Map/Tree,Hand,264.84375,0.0
,EMGM,254.6875,0.0
,SYB,1813.28125,0.0
Read/Tree,Hand,188.28125,0.0
,EMGM,729.6875,0.0
,SYB,1953.90625,0.0
,MultiRec,764.0625,0.0
,Regular,759.375,0.0
Read/Logic,Hand,512.5,0.0
,EMGM,1653.125,0.0
,SYB,5814.84375,0.0
Show/Tree,Hand,1217.96875,0.0
,EMGM,1093.75,0.0
,SYB,5252.34375,0.0
,MultiRec,2935.9375,0.0
,Regular,1035.15625,0.0
Show/Logic,Hand,132.03125,0.0
,EMGM,146.09375,0.0
,SYB,379.6875,0.0
,MultiRec,344.53125,0.0
,Regular,146.09375,0.0
Update/Tree,Hand,214.0625,0.0
,EMGM,264.0625,0.0
,SYB,1910.9375,0.0
,MultiRec,834.375,0.0
,Regular,219.53125,0.0
Update/Logic,Hand,505.46875,0.0
,EMGM,479.6875,0.0
,SYB,2792.1875,0.0
,MultiRec,1655.46875,0.0
,Regular,514.0625,0.0
-------------------------------------
