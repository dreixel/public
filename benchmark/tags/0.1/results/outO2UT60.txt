
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,102.34375
,EMGM,825.0
,SYB,1692.96875
,MultiRec,214.84375
,Regular,176.5625
Eq/Logic,Hand,2363.28125
,EMGM,3431.25
,SYB,4510.9375
,MultiRec,2935.9375
,Regular,2872.65625
Map/Tree,Hand,100.78125
,EMGM,466.40625
,SYB,746.09375
Read/Tree,Hand,186.71875
,EMGM,731.25
,SYB,1853.90625
,MultiRec,725.78125
,Regular,726.5625
Read/Logic,Hand,546.09375
,EMGM,2725.78125
,SYB,4314.0625
Show/Tree,Hand,496.875
,EMGM,1128.90625
,SYB,1982.8125
,MultiRec,1299.21875
,Regular,393.75
Show/Logic,Hand,3567.1875
,EMGM,4572.65625
,SYB,5324.21875
,Regular,3787.5
-------------------------------------
