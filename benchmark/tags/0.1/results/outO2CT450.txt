
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,102.34375
,EMGM,107.03125
,SYB,2239.84375
,MultiRec,221.875
,Regular,175.78125
Eq/Logic,Hand,2497.65625
,EMGM,2714.84375
,SYB,5416.40625
,MultiRec,2952.34375
,Regular,3002.34375
Map/Tree,Hand,93.75
,EMGM,100.0
,SYB,746.875
Read/Tree,Hand,184.375
,EMGM,725.0
,SYB,1445.3125
,MultiRec,782.8125
,Regular,750.0
Read/Logic,Hand,585.15625
,EMGM,2712.5
,SYB,2870.3125
Show/Tree,Hand,530.46875
,EMGM,641.40625
,SYB,1939.84375
,MultiRec,1320.3125
,Regular,605.46875
Show/Logic,Hand,3522.65625
,EMGM,4166.40625
,SYB,5357.8125
,Regular,4121.09375
-------------------------------------
