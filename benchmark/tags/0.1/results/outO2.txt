
Results:
Number of repetitions: 20
Compiler flags: -O2
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,103.90625
,EMGM,104.6875
,SYB,2156.25
,MultiRec,212.5
,Regular,182.8125
Eq/Logic,Hand,2556.25
,EMGM,3393.75
,SYB,5128.90625
,MultiRec,2874.21875
,Regular,2985.15625
Map/Tree,Hand,93.75
,EMGM,94.53125
,SYB,742.96875
Read/Tree,Hand,185.15625
,EMGM,717.96875
,SYB,1408.59375
,MultiRec,767.1875
,Regular,749.21875
Read/Logic,Hand,566.40625
,EMGM,2860.9375
,SYB,2868.75
Show/Tree,Hand,521.875
,EMGM,770.3125
,SYB,2016.40625
,MultiRec,1306.25
,Regular,574.21875
Show/Logic,Hand,3473.4375
,EMGM,4574.21875
,SYB,5375.0
,Regular,4189.84375
-------------------------------------
