
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp -funfolding-use-threshold=16
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Hand,87.5
EMGM,92.1875
SYB,1576.5625
Multirec,204.6875
Regular,164.84375
-------------------------------------
Test,Map
Hand,82.03125
EMGM,89.84375
SYB,680.46875
-------------------------------------
Test,Read
Hand,26.5625
EMGM,46.875
SYB,46.875
-------------------------------------
Test,Show
Hand,475.0
EMGM,722.65625
SYB,1710.9375
Multirec,1457.8125
Regular,356.25
-------------------------------------
