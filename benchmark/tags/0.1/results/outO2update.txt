
Results:
Number of repetitions: 20
Compiler flags: -O2
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Update/Tree,Hand,85.15625
,EMGM,100.78125
,SYB,657.8125
,MultiRec,311.71875
,Regular,101.5625
Update/Logic,Hand,2611.71875
,EMGM,2565.625
,SYB,2546.875
,MultiRec,2571.875
,Regular,2596.875
-------------------------------------
