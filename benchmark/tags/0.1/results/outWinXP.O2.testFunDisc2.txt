
Results:
Number of repetitions: 20
Compiler flags: -O2
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,230.46875,0.0
,EMGM,234.375,0.0
,SYB,5125.0,0.0
,MultiRec,531.25,0.0
,Regular,453.125,0.0
Eq/Logic,Hand,181.25,0.0
,EMGM,510.9375,0.0
,SYB,1960.15625,0.0
,MultiRec,318.75,0.0
,Regular,328.125,0.0
Map/Tree,Hand,240.625,0.0
,EMGM,228.125,0.0
,SYB,1668.75,0.0
Read/Tree,Hand,164.84375,0.0
,EMGM,666.40625,0.0
,SYB,1343.75,0.0
,MultiRec,705.46875,0.0
,Regular,686.71875,0.0
Read/Logic,Hand,471.09375,0.0
,EMGM,1659.375,0.0
,SYB,4031.25,0.0
Show/Tree,Hand,1196.875,0.0
,EMGM,1749.21875,0.0
,SYB,4592.96875,0.0
,MultiRec,3131.25,0.0
,Regular,1200.78125,0.0
Show/Logic,Hand,119.53125,0.0
,EMGM,222.65625,0.0
,SYB,409.375,0.0
,MultiRec,353.125,0.0
,Regular,135.15625,0.0
Update/Tree,Hand,190.625,0.0
,EMGM,257.8125,0.0
,SYB,1788.28125,0.0
,MultiRec,717.96875,0.0
,Regular,262.5,0.0
Update/Logic,Hand,457.8125,0.0
,EMGM,1314.0625,0.0
,SYB,1282.8125,0.0
,MultiRec,1544.53125,0.0
,Regular,837.5,0.0
-------------------------------------
