
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,234.375,0.0
,EMGM,2846.875,0.0
,SYB,4355.46875,0.0
,MultiRec,621.09375,0.0
,Regular,458.59375,0.0
Eq/Logic,Hand,208.59375,0.0
,EMGM,568.75,0.0
,SYB,1509.375,0.0
,MultiRec,371.09375,0.0
,Regular,300.0,0.0
Map/Tree,Hand,253.90625,0.0
,EMGM,1216.40625,0.0
,SYB,1790.625,0.0
Read/Tree,Hand,180.46875,0.0
,EMGM,757.03125,0.0
,SYB,1942.96875,0.0
,MultiRec,756.25,0.0
,Regular,710.9375,0.0
Read/Logic,Hand,510.15625,0.0
,EMGM,1815.625,0.0
,SYB,5771.875,0.0
Show/Tree,Hand,1251.5625,0.0
,EMGM,3068.75,0.0
,SYB,5082.8125,0.0
,MultiRec,3436.71875,0.0
,Regular,961.71875,0.0
Show/Logic,Hand,134.375,0.0
,EMGM,244.53125,0.0
,SYB,385.9375,0.0
,MultiRec,386.71875,0.0
,Regular,135.15625,0.0
Update/Tree,Hand,217.96875,0.0
,EMGM,1200.78125,0.0
,SYB,1941.40625,0.0
,MultiRec,842.96875,0.0
,Regular,203.90625,0.0
Update/Logic,Hand,507.8125,0.0
,EMGM,1603.125,0.0
,SYB,2689.0625,0.0
,MultiRec,1592.1875,0.0
,Regular,482.03125,0.0
-------------------------------------
