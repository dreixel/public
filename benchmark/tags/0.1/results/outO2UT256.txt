
Results:
Number of repetitions: 20
Compiler flags: -O2 -fforce-recomp -funfolding-use-threshold=256
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Hand,91.40625
EMGM,736.71875
SYB,1491.40625
Multirec,202.34375
Regular,160.9375
-------------------------------------
Test,Map
Hand,87.5
EMGM,418.75
SYB,707.8125
-------------------------------------
Test,Read
Hand,24.21875
EMGM,43.75
SYB,50.0
-------------------------------------
Test,Show
Hand,450.78125
EMGM,970.3125
SYB,1812.5
Multirec,1342.1875
Regular,371.875
-------------------------------------
