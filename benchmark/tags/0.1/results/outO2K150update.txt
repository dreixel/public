
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-keeness-factor=150
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Update/Tree,Hand,83.59375
,EMGM,98.4375
,SYB,739.84375
,MultiRec,300.0
,Regular,106.25
Update/Logic,Hand,2539.84375
,EMGM,2607.03125
,SYB,2599.21875
,MultiRec,2555.46875
,Regular,2567.96875
-------------------------------------
