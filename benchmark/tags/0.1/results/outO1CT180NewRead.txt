
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp -funfolding-creation-threshold=180
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Read
Hand,185.9375
EMGM,709.375
SYB,1398.4375
Regular,697.65625
RegularDeep,705.46875
-------------------------------------
