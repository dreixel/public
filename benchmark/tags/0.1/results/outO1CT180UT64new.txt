
Results:
Number of repetitions: 10
Compiler flags: -O1 -fforce-recomp -funfolding-creation-threshold=180 -funfolding-use-threshold=64
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,98.4375
,EMGM,104.6875
,SYB,1653.125
,MultiRec,210.9375
,Regular,178.125
Eq/Logic,Hand,2418.75
,EMGM,2440.625
,SYB,4540.625
,MultiRec,2929.6875
,Regular,2865.625
Map/Tree,Hand,92.1875
,EMGM,96.875
,SYB,760.9375
Read/Tree,Hand,178.125
,EMGM,723.4375
,SYB,1821.875
,MultiRec,726.5625
,Regular,717.1875
Read/Logic,Hand,540.625
,EMGM,2664.0625
,SYB,4259.375
Show/Tree,Hand,471.875
,EMGM,445.3125
,SYB,1931.25
,MultiRec,1135.9375
,Regular,395.3125
Show/Logic,Hand,3490.625
,EMGM,3635.9375
,SYB,5270.3125
,Regular,3821.875
-------------------------------------
