
Results:
Number of repetitions: 20
Compiler flags: -O1
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,242.1875
,EMGM,246.09375
,SYB,5407.03125
,MultiRec,572.65625
,Regular,456.25
Eq/Logic,Hand,193.75
,EMGM,558.59375
,SYB,1878.125
,MultiRec,335.9375
,Regular,319.53125
Map/Tree,Hand,229.6875
,EMGM,221.09375
,SYB,1749.21875
Read/Tree,Hand,171.875
,EMGM,665.625
,SYB,1286.71875
,MultiRec,680.46875
,Regular,662.5
Read/Logic,Hand,478.90625
,EMGM,1660.15625
,SYB,3829.6875
Show/Tree,Hand,1238.28125
,EMGM,1811.71875
,SYB,4982.03125
,MultiRec,3314.0625
,Regular,1220.3125
Show/Logic,Hand,120.3125
,EMGM,235.15625
,SYB,419.53125
,MultiRec,342.96875
,Regular,144.53125
Update/Tree,Hand,189.84375
,EMGM,224.21875
,SYB,1627.34375
,MultiRec,732.03125
,Regular,264.0625
Update/Logic,Hand,460.15625
,EMGM,1465.625
,SYB,1236.71875
,MultiRec,1503.90625
,Regular,835.15625
-------------------------------------
