
Results:
Number of repetitions: 20
Compiler flags: -O1 -funfolding-creation-threshold=450 -funfolding-keeness-factor=15
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,218.75,0.0
,EMGM,253.125,0.0
,SYB,4179.6875,0.0
,MultiRec,601.5625,0.0
,Regular,540.625,0.0
Eq/Logic,Hand,203.90625,0.0
,EMGM,207.8125,0.0
,SYB,1453.125,0.0
,MultiRec,389.0625,0.0
,Regular,354.6875,0.0
Map/Tree,Hand,225.78125,0.0
,EMGM,246.09375,0.0
,SYB,1754.6875,0.0
Read/Tree,Hand,185.9375,0.0
,EMGM,732.8125,0.0
,SYB,1910.9375,0.0
,MultiRec,830.46875,0.0
,Regular,777.34375,0.0
Read/Logic,Hand,517.96875,0.0
,EMGM,1685.15625,0.0
,SYB,5660.9375,0.0
Show/Tree,Hand,1250.78125,0.0
,EMGM,1392.1875,0.0
,SYB,4870.3125,0.0
,MultiRec,3053.125,0.0
,Regular,1346.09375,0.0
Show/Logic,Hand,131.25,0.0
,EMGM,148.4375,0.0
,SYB,392.96875,0.0
,MultiRec,364.0625,0.0
,Regular,144.53125,0.0
Update/Tree,Hand,201.5625,0.0
,EMGM,258.59375,0.0
,SYB,1939.0625,0.0
,MultiRec,785.9375,0.0
,Regular,296.09375,0.0
Update/Logic,Hand,488.28125,0.0
,EMGM,472.65625,0.0
,SYB,2786.71875,0.0
,MultiRec,1803.125,0.0
,Regular,935.15625,0.0
-------------------------------------
