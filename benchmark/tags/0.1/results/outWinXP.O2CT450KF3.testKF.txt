
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450 -funfolding-keeness-factor=3
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,238.28125,0.0
,EMGM,255.46875,0.0
,SYB,4126.5625,0.0
,MultiRec,604.6875,0.0
,Regular,486.71875,0.0
Eq/Logic,Hand,188.28125,0.0
,EMGM,251.5625,0.0
,SYB,1550.78125,0.0
,MultiRec,350.0,0.0
,Regular,331.25,0.0
Map/Tree,Hand,253.125,0.0
,EMGM,240.625,0.0
,SYB,1850.78125,0.0
Read/Tree,Hand,181.25,0.0
,EMGM,696.875,0.0
,SYB,1949.21875,0.0
,MultiRec,785.9375,0.0
,Regular,730.46875,0.0
Read/Logic,Hand,510.15625,0.0
,EMGM,1599.21875,0.0
,SYB,5757.8125,0.0
Show/Tree,Hand,1291.40625,0.0
,EMGM,1424.21875,0.0
,SYB,5245.3125,0.0
,MultiRec,2894.53125,0.0
,Regular,1235.15625,0.0
Show/Logic,Hand,131.25,0.0
,EMGM,150.78125,0.0
,SYB,410.9375,0.0
,MultiRec,371.875,0.0
,Regular,150.0,0.0
Update/Tree,Hand,206.25,0.0
,EMGM,250.0,0.0
,SYB,2010.15625,0.0
,MultiRec,774.21875,0.0
,Regular,282.03125,0.0
Update/Logic,Hand,464.84375,0.0
,EMGM,739.0625,0.0
,SYB,2675.78125,0.0
,MultiRec,1567.1875,0.0
,Regular,880.46875,0.0
-------------------------------------
