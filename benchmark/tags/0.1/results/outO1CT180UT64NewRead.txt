
Results:
Number of repetitions: 20
Compiler flags: -O1 -fforce-recomp -funfolding-creation-threshold=180 -funfolding-use-threshold=64
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Read
Hand,185.9375
EMGM,700.0
SYB,1789.84375
Regular,700.78125
RegularDeep,712.5
-------------------------------------
