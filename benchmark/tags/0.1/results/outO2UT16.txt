
Results:
Number of repetitions: 20
Compiler flags: -O2 -fforce-recomp -funfolding-use-threshold=16
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

-------------------------------------
Test,Eq
Hand,89.84375
EMGM,91.40625
SYB,1500.0
Multirec,207.03125
Regular,164.84375
-------------------------------------
Test,Map
Hand,89.84375
EMGM,88.28125
SYB,701.5625
-------------------------------------
Test,Read
Hand,23.4375
EMGM,41.40625
SYB,48.4375
-------------------------------------
Test,Show
Hand,490.625
EMGM,725.0
SYB,1790.625
Multirec,1393.75
Regular,369.53125
-------------------------------------
