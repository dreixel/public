
Results:
Number of repetitions: 20
Compiler flags: -O2 -funfolding-creation-threshold=450 -funfolding-use-threshold=60
Environment: mingw32,i386,Version {versionBranch = [6,10], versionTags = []}
CPU time precision: 1.0 (ms)

Eq/Tree,Hand,222.65625,0.0
,EMGM,254.6875,0.0
,SYB,4025.78125,0.0
,MultiRec,564.84375,0.0
,Regular,460.9375,0.0
Eq/Logic,Hand,186.71875,0.0
,EMGM,195.3125,0.0
,SYB,1400.78125,0.0
,MultiRec,333.59375,0.0
,Regular,297.65625,0.0
Map/Tree,Hand,246.875,0.0
,EMGM,1167.1875,0.0
,SYB,1726.5625,0.0
Read/Tree,Hand,175.78125,0.0
,EMGM,691.40625,0.0
,SYB,1871.875,0.0
,MultiRec,716.40625,0.0
,Regular,714.84375,0.0
Read/Logic,Hand,492.96875,0.0
,EMGM,1573.4375,0.0
,SYB,5561.71875,0.0
Show/Tree,Hand,1158.59375,0.0
,EMGM,1066.40625,0.0
,SYB,4867.1875,0.0
,MultiRec,2771.09375,0.0
,Regular,966.40625,0.0
Show/Logic,Hand,126.5625,0.0
,EMGM,141.40625,0.0
,SYB,363.28125,0.0
,MultiRec,338.28125,0.0
,Regular,133.59375,0.0
Update/Tree,Hand,203.125,0.0
,EMGM,256.25,0.0
,SYB,1835.15625,0.0
,MultiRec,807.03125,0.0
,Regular,198.4375,0.0
Update/Logic,Hand,468.75,0.0
,EMGM,448.4375,0.0
,SYB,2590.625,0.0
,MultiRec,1561.71875,0.0
,Regular,475.78125,0.0
-------------------------------------
