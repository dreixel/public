ghc --make -fforce-recomp -O2 Main

./Main -n 10 -o results/outMac.O0.011009.txt -f "-O0"
./Main -n 10 -o results/outMac.O1.011009.txt -f "-O1"
./Main -n 10 -o results/outMac.O2.011009.txt -f "-O2"
./Main -n 10 -o results/outMac.O1UT60.011009.txt -f "-O1 -funfolding-use-threshold=60"
./Main -n 10 -o results/outMac.O1CT90.011009.txt -f "-O1 -funfolding-creation-threshold=90"
./Main -n 10 -o results/outMac.O1CT450UT60.011009.txt -f "-O1 -funfolding-creation-threshold=450 -funfolding-use-threshold=60"
./Main -n 10 -o results/outMac.O1CT1000UT180.011009.txt -f "-O1 -funfolding-creation-threshold=1000 -funfolding-use-threshold=180"