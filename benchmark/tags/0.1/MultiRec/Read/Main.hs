module MultiRec.Read.Main where

import Auxiliary.Tree (Tree(..), smallerTreeString, sumTree)
import Auxiliary.Logic (Logic(..), smallerLogicString, evalLogic)
import Auxiliary.MultiRec
import Auxiliary.Auxiliary (test)
import MultiRec.Read.Read as G


mainTree :: IO ()
mainTree = let t = G.read Tree smallerTreeString
           in test. putStr . show . sumTree $ t

mainLogic :: IO ()
mainLogic = let t = G.read Logic smallerLogicString
            in test. putStr . show . evalLogic (const True) $ t
