module MultiRec.Eq.Main where

import Auxiliary.Tree (bigTree, tweakRightmost)
import Auxiliary.Logic (biggerLogic, tweakLogic)
import Auxiliary.MultiRec
import Auxiliary.Auxiliary (test)
import MultiRec.Eq.Eq

mainTree :: IO ()
mainTree = test . putStr . show $ 
            (eq Tree bigTree bigTree, eq Tree bigTree (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $
              [ eq Logic biggerLogic biggerLogic, 
                eq Logic biggerLogic (tweakLogic biggerLogic),
                eq Logic (tweakLogic biggerLogic) biggerLogic]
