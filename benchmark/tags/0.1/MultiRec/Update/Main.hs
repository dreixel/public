module MultiRec.Update.Main where

import Auxiliary.Tree (bigTree, sumTree)
import Auxiliary.Logic (biggerLogic, evalLogic)
import Auxiliary.MultiRec
import Auxiliary.Auxiliary (test, apply)
import MultiRec.Update.Update

mainTree :: IO ()
mainTree = test . putStr . show . sumTree . (apply 4 (updateInt Tree)) $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . evalLogic (const True) . (apply 800 (updateString Logic)) $ biggerLogic
