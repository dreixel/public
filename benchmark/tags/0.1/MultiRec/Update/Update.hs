{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE OverlappingInstances   #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE Rank2Types             #-}
{-# LANGUAGE GADTs                 #-}

module MultiRec.Update.Update where

import Generics.MultiRec


class UpdateInt phi (f :: (* -> *) -> * -> *) where
  updateIntf :: (forall ix. phi ix -> r ix -> r ix)
             -> phi ix -> f r ix -> f r ix

instance UpdateInt phi U where
  updateIntf _ _ = id

instance (El phi xi) => UpdateInt phi (I xi) where
  updateIntf f _ (I x) = I (f proof x)

instance UpdateInt phi (K Int) where
  updateIntf f _ (K n) | odd n     = K (n + 1)
                       | otherwise = K (n - 1)

instance UpdateInt phi (K x) where
  updateIntf f _ (K x) = K x

instance (UpdateInt phi f, UpdateInt phi g) => UpdateInt phi (f :+: g) where
  updateIntf f p (L x) = L (updateIntf f p x)
  updateIntf f p (R x) = R (updateIntf f p x)

instance (UpdateInt phi f, UpdateInt phi g) => UpdateInt phi (f :*: g) where
  updateIntf f p (x :*: y) = updateIntf f p x :*: updateIntf f p y

instance (UpdateInt phi f) => UpdateInt phi (C c f) where
  updateIntf f p (C x) = C (updateIntf f p x)

instance (UpdateInt phi f) => UpdateInt phi (f :>: xi) where
  updateIntf f p (Tag x) = Tag (updateIntf f p x)
 
updateInt :: (Fam phi, UpdateInt phi (PF phi)) => phi ix -> ix -> ix
updateInt p = to p . updateIntf (\p (I0 x) -> I0 (updateInt p x)) p . from p


class UpdateString phi (f :: (* -> *) -> * -> *) where
  updateStringf :: (forall ix. phi ix -> r ix -> r ix)
                -> phi ix -> f r ix -> f r ix

instance UpdateString phi U where
  updateStringf _ _ = id

instance (El phi xi) => UpdateString phi (I xi) where
  updateStringf f _ (I x) = I (f proof x)

instance UpdateString phi (K String) where
  updateStringf f _ (K "") = K ""
  updateStringf f _ (K s)  = K (last s : tail s)

instance UpdateString phi (K x) where
  updateStringf f _ (K x) = K x

instance (UpdateString phi f, UpdateString phi g) => UpdateString phi (f :+: g) where
  updateStringf f p (L x) = L (updateStringf f p x)
  updateStringf f p (R x) = R (updateStringf f p x)

instance (UpdateString phi f, UpdateString phi g) => UpdateString phi (f :*: g) where
  updateStringf f p (x :*: y) = updateStringf f p x :*: updateStringf f p y

instance (UpdateString phi f) => UpdateString phi (C c f) where
  updateStringf f p (C x) = C (updateStringf f p x)

instance (UpdateString phi f) => UpdateString phi (f :>: xi) where
  updateStringf f p (Tag x) = Tag (updateStringf f p x)
 
updateString :: (Fam phi, UpdateString phi (PF phi)) => phi ix -> ix -> ix
updateString p = to p . updateStringf (\p (I0 x) -> I0 (updateString p x)) p . from p
