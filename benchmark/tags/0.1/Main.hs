module Main where
{-
import qualified Hand.Eq.Main
import qualified Hand.Map.Main
import qualified Hand.Read.Main
import qualified Hand.Show.Main
import qualified Hand.Update.Main
--import qualified Hand.Arbitrary.Main

import qualified EMGM.Eq.Main
import qualified EMGM.Map.Main
import qualified EMGM.Read.Main
import qualified EMGM.Show.Main
import qualified EMGM.Update.Main

import qualified SYB.Eq.Main
import qualified SYB.Map.Main
import qualified SYB.Read.Main
import qualified SYB.Show.Main
import qualified SYB.Update.Main

import qualified MultiRec.Eq.Main
import qualified MultiRec.Show.Main
import qualified MultiRec.Read.Main
import qualified MultiRec.Update.Main

import qualified Regular.Eq.Main
import qualified Regular.Show.Main
import qualified Regular.Read.Main
import qualified Regular.Update.Main

import qualified RegularDeep.Eq.Main
import qualified RegularDeep.Show.Main
import qualified RegularDeep.Read.Main
-}

-- ParseArgs library
import System.Console.ParseArgs

import System.CPUTime (cpuTimePrecision)
import Data.List (groupBy, sortBy, intersperse)
import System.FilePath ((</>), takeExtension)
import System.Directory (getDirectoryContents, removeFile)
import System.Cmd (system)
import System.IO
  (hPutStrLn, Handle, IOMode(..), stdout, hFlush, hIsEOF, hGetChar, hClose,
   openFile, hFileSize)
import System.Exit (ExitCode(..))
import System.Info (os, arch, compilerVersion)
import Control.Monad (when)

data Library = Hand         -- Handwritten code
             | EMGM         -- emgm-0.3.1
             | SYB          -- syb-0.1
             | MultiRec     -- multirec-0.4
             | Regular      -- regular-0.1
             | RegularDeep  -- a version of regular-0.1 with deep encodings
                deriving (Eq, Ord, Show)

data TestName = Eq 
              | Map 
              | Read
              | Show
              | Update      -- Traversals
              | Arbitrary   -- QuickCheck's (1.2)
                 deriving (Eq, Ord, Show)

data Test = Test { lib :: Library,
                   testName :: TestName,
                   datatype :: Datatype
                 } deriving (Eq, Ord, Show)

data Datatype = Tree    -- Labelled binary trees
              | Logic   -- Logic expressions
                deriving (Eq, Ord, Show)


handTests, emgmTests, sybTests, multirecTests, regularTests, 
  regularDeepTests, tests :: [Test]
handTests = [ Test Hand Eq     Tree
            , Test Hand Map    Tree
            , Test Hand Read   Tree
            , Test Hand Show   Tree
            , Test Hand Update Tree
            , Test Hand Eq     Logic
            , Test Hand Read   Logic
            , Test Hand Show   Logic
            , Test Hand Update Logic]
            
emgmTests = [ Test EMGM Eq     Tree
            , Test EMGM Map    Tree
            , Test EMGM Read   Tree
            , Test EMGM Show   Tree
            , Test EMGM Update Tree
            , Test EMGM Eq     Logic
            , Test EMGM Read   Logic
            , Test EMGM Show   Logic
            , Test EMGM Update Logic]

sybTests = [ Test SYB Eq     Tree
           , Test SYB Map    Tree
           , Test SYB Read   Tree
           , Test SYB Show   Tree
           , Test SYB Update Tree
           , Test SYB Eq     Logic
           , Test SYB Read   Logic
           , Test SYB Show   Logic
           , Test SYB Update Logic]

multirecTests = [ Test MultiRec Eq     Tree
                , Test MultiRec Show   Tree
                , Test MultiRec Read   Tree
                , Test MultiRec Update Tree
                , Test MultiRec Eq     Logic
                , Test MultiRec Show   Logic
                -- , Test MultiRec Read   Logic
                , Test MultiRec Update Logic]
               
regularTests = [ Test Regular Eq     Tree
               , Test Regular Show   Tree
               , Test Regular Read   Tree
               , Test Regular Update Tree
               , Test Regular Eq     Logic
               , Test Regular Show   Logic
               -- , Test Regular Read   Logic
               , Test Regular Update Logic]

regularDeepTests = [ Test RegularDeep Eq     Tree
                   , Test RegularDeep Show   Tree
                   , Test RegularDeep Read   Tree]

tests = handTests ++ emgmTests ++ sybTests ++ multirecTests ++ regularTests

inCommas :: [String] -> String
inCommas = concat . intersperse ","


printGroupStats :: (Enum a, Fractional a, Floating a, Num a)
                => Handle -> IO [(Test, Int, a)] -> IO ()
printGroupStats h l = do
  l' <- l
  let --group1 :: [[(Test, Int, a)]]
      group1 = groupBy g (sortBy f l')
      f (t1,_,_) (t2,_,_) = compare t1 t2
      g (t1,_,_) (t2,_,_) = t1 == t2
      
      --calcAvgStdDev :: [(Test, Int, a)] -> (Test, a, a)
      calcAvgStdDev x = (fst' (head x), avg x, stddev (avg x))
        where
          avg  l = sum' l / toEnum (length l)
          avg' l = sum' l / (toEnum (length l) - 1) -- sample standard deviation
          stddev a = sqrt (avg' [ (t,d,(y - a)^2) | (t,d,y) <- x ])
          fst' (a,_,_) = a
          --sum' :: [(Test, Int, a)] -> a
          sum' [] = 0
          sum' ((_,_,d):ts) = d + sum' ts
      
      --group2 :: [(Test, a, a)] -> [[(Test, a, a)]]
      group2 = groupBy g' . sortBy f'
      f' (t1,_,_) (t2,_,_) = compare (testName t1, datatype t1) 
                                     (testName t2, datatype t2)
      g' (t1,_,_) (t2,_,_) =    testName t1 == testName t2 
                             && datatype t1 == datatype t2
  sequence_ $ map (handleGroup h) $ group2 $ map calcAvgStdDev group1

handleGroup :: (Show a) => Handle -> [(Test, a, a)] -> IO ()
handleGroup _ [] = error "handleGroup []"
handleGroup h g  = do
                    let (t,a,d) = head g
                        name = show (testName t) ++ "/" ++ show (datatype t)
                    --hPutStrLn h ("-------------------------------------")
                    -- Header
                    --hPutStrLn h (inCommas ["Test", name])
                    -- First line is different
                    hPutStrLn h (inCommas [name, show (lib t), show a, show d])
                    -- Then the rest
                    sequence_ $ map (hPutStrLn h)
                      [ inCommas ["", show (lib t), show a, show d] | (t,a,d) <- tail g ]

-- Arguments
data MyArgs = N | O | F | P | B | C deriving (Eq, Ord, Show)

myArgs :: [Arg MyArgs]
myArgs = [
          Arg { argIndex = N,
                argAbbr = Just 'n',
                argName = Just "number-times",
                argData = argDataDefaulted "int" ArgtypeInt 1,
                argDesc = "Number of times to run the input program"
              },
          Arg { argIndex = O,
                argAbbr = Just 'o',
                argName = Just "output",
                argData = argDataOptional "file" ArgtypeString,
                argDesc = "Output report file"
              },
          Arg { argIndex = F,
                argAbbr = Just 'f',
                argName = Just "ghc-flags",
                argData = argDataDefaulted "flags" ArgtypeString "",
                argDesc = "Flags to pass to the compiler"
              },
          Arg { argIndex = P,
                argAbbr = Just 'p',
                argName = Just "profiling",
                argData = Nothing,
                argDesc = "Profile, do not benchmark"
              },
          Arg { argIndex = B,
                argAbbr = Just 'b',
                argName = Just "binary-size",
                argData = Nothing,
                argDesc = "Compute binary sizes (Win only), do not benchmark"
              },
          Arg { argIndex = C,
                argAbbr = Just 'c',
                argName = Just "path-to-ghc",
                argData = argDataDefaulted "path" ArgtypeString "ghc",
                argDesc = "Path to GHC. Defaults to \"ghc\""
              }
         ]

sequenceProgress_ :: [IO ExitCode] -> IO ()
sequenceProgress_ [] = return ()
sequenceProgress_ l  = do
  let seq :: [IO ExitCode] -> Int -> IO ()
      seq []    _ = putStrLn "done."
      seq (h:t) n = do
                      putStr ((show n) ++ " ") >> hFlush stdout
                      sequenceError_ [h]
                      seq t (n + 1)
  putStr ("Total number of elements: " ++ show (length l) ++ ". ")
  seq l 1

-- sequence_ accounting for errors
sequenceError_ :: [IO ExitCode] -> IO ()
sequenceError_ []    = return ()
sequenceError_ (h:t) = do
                         e <- h
                         case e of
                           ExitSuccess   -> sequenceError_ t
                           ExitFailure n -> error ("Execution returned exit code "
                                                    ++ show n ++ ", aborted.")

-- Stricter readFile
hGetContents' hdl = do e <- hIsEOF hdl
                       if e then return []
                         else do c <- hGetChar hdl
                                 cs <- hGetContents' hdl
                                 return (c:cs)

readFile' fn = do hdl <- openFile fn ReadMode
                  xs <- hGetContents' hdl
                  hClose hdl
                  return xs


main :: IO ()
main = do
        args <- parseArgsIO ArgsComplete myArgs
        
        -- Some variables
        let profiling = gotArg args P
            binsize   = gotArg args B
            n :: Int
            n     = if profiling then 1 else (getRequiredArg args N)
            ghc   = getRequiredArg args C
            flags = " -fforce-recomp --make " ++ getRequiredArg args F ++ " "
                    ++ (if profiling then " -prof -auto-all " else "") 
            mainis t = "-main-is " ++ show (lib t) ++ "." 
                         ++ show (testName t) 
                         ++ ".Main.main" ++ show (datatype t)
                         ++ " -o " ++ path t ++ show (lib t) ++ show (testName t) ++ show (datatype t) ++ " "
            path t = show (lib t) </> show (testName t) </> "Main"
            out t = "out" </> show (lib t) ++ "." ++ show (testName t) ++ "." 
                      ++ show (datatype t) ++ ".compile.out"
            redirect t = " > " ++ out t ++ " 2>&1 "
            cmd t = ghc ++ flags ++ mainis t ++ path t ++ redirect t
        
        -- Sanity check
        when (profiling && binsize) $ do
          putStrLn "Cannot profile and compute binary sizes."
          return ()
        
        -- Compilation
        putStrLn "Compiling..." >> hFlush stdout
        --sequence_ [ putStrLn (cmd t) | t <- tests ]
        sequenceProgress_ [ system (cmd t) | t <- tests ]
        
        -- Remove old outputs
        putStrLn "Removing old outputs..." >> hFlush stdout
        files <- getDirectoryContents "out"
        let filesToDelete = filter ((==) ".out" . takeExtension) files
        mapM removeFile (map ("out" </>) filesToDelete)
        
        -- Running tests
        let newout t m = "out" </> show (lib t) ++ "." ++ show (testName t) 
                          ++ "." ++ show (datatype t) ++ "." ++ show m ++ ".out"
            newpath t = show (lib t) </> show (testName t) </> "Main" 
                          ++ show (lib t) ++ show (testName t) ++ show (datatype t)
            run t m =    newpath t 
                      ++ " +RTS -K32M " ++ if profiling then " -p " else "" ++ " -RTS"
                      ++ " > " 
                      ++ newout t m
        when (not binsize) $ do
          putStrLn "Running tests..." >> hFlush stdout
          --sequence_ [ putStrLn (run t m) | t <- tests, m <- [1..n]]
          sequenceProgress_ [ system (run t m) | t <- tests, m <- [1..n]]
        
        -- Calculating binary size
        let filename t = newpath t ++ ".exe" -- TODO: this is not portable!
            size s = do 
                       h' <- openFile s ReadMode
                       x <- hFileSize h'
                       hClose h'
                       return (fromInteger x)
            sizes = sequence [ size (filename t) | t <- tests ]

        -- Results output
        h <- getArgStdio args O WriteMode
        putStrLn ("-------------------------------------")
        hPutStrLn h "\nResults:"
        hPutStrLn h ("Number of repetitions: " ++ show n)
        hPutStrLn h ("Compiler flags: " ++ (getRequiredArg args F :: String))
        hPutStrLn h ("Environment: " ++ inCommas [os, arch, show compilerVersion])
        hPutStrLn h ("CPU time precision: " ++ show (fromInteger cpuTimePrecision / (1000000000 :: Double)) ++ " (ms)")
        hPutStrLn h ""
        let parse :: Test -> Int -> IO Double
            parse t m = readFile' (newout t m) >>= return . read . tail . dropWhile (/= '\t')
            liftIOList :: [(a, b, IO c)] -> IO [(a, b, c)]
            liftIOList [] = return []
            liftIOList ((a,b,c):t) = do  c' <- c
                                         t' <- liftIOList t
                                         return ((a,b,c'):t')
        case (profiling, binsize) of
          (True, False)  -> hPutStrLn h ("Profiling run, no benchmarking results.")
          (False, True)  -> sizes >>= \l -> printGroupStats h (return [ (t,1,s) | (t,s) <- zip tests l ])
          (False, False) -> printGroupStats h (liftIOList [ (t, m, parse t m) | t <- tests, m <- [1..n]])
          (True, True)   -> error "Internal error #1 (can never happen)"
        hPutStrLn h ("-------------------------------------")
        hClose h

