module EMGM.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.EMGM
import Auxiliary.Auxiliary (test)
import qualified EMGM.Show.Show as G

mainTree :: IO ()
mainTree = test . putStr . show . last . G.show $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . last . G.show $ bigLogic
