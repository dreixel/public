module EMGM.Eq.Main where

import qualified EMGM.Eq.Compare as G
import Auxiliary.EMGM
import Auxiliary.Tree (bigTree, tweakRightmost)
import Auxiliary.Logic (biggerLogic, tweakLogic)
import Auxiliary.Auxiliary (test)


mainTree :: IO ()
mainTree = test . putStr . show $ 
            (G.eq bigTree bigTree, G.eq bigTree (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $
              [ G.eq biggerLogic biggerLogic, 
                G.eq biggerLogic (tweakLogic biggerLogic),
                G.eq (tweakLogic biggerLogic) biggerLogic]

