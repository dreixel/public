module EMGM.Read.Main where

import Auxiliary.Tree (Tree(..), smallerTreeString, sumTree)
import Auxiliary.Logic (Logic(..), smallerLogicString, evalLogic)
import Auxiliary.EMGM
import Auxiliary.Auxiliary (test)

import qualified EMGM.Read.Read as G

mainTree :: IO ()
mainTree = let (Just t) = G.read smallerTreeString
           in test . putStr . show . sumTree $ t

mainLogic :: IO ()
mainLogic = let (Just t) = G.read smallerLogicString
            in test . putStr . show . evalLogic (const True) $ t
