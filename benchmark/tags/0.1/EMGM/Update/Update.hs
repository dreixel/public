{-# LANGUAGE FlexibleContexts #-}

module EMGM.Update.Update where

import Auxiliary.Tree (Tree(..))
import Auxiliary.Logic (Logic(..))
import Auxiliary.EMGM

import Generics.EMGM

updateTree :: (Rep (Everywhere Int) a) => Tree a -> Tree a
updateTree = everywhere f
  where f :: Int -> Int
        f n | odd n     = n + 1
            | otherwise = n - 1

updateLogic :: Logic -> Logic
updateLogic = everywhere f
  where f :: String -> String
        f "" = ""
        f s  = last s : tail s
