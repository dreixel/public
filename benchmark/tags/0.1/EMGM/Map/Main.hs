module EMGM.Map.Main where

import Auxiliary.Tree (smallerTree, sumTree)
import Auxiliary.Auxiliary (test, apply)
import Auxiliary.EMGM
import qualified EMGM.Map.Map as G


mainTree :: IO ()
mainTree = let t = apply 1000 (G.map (+1)) smallerTree
           in test . putStr . show . sumTree $ t
