module SYB.Read.Read where

import Data.Generics (extR, Data, dataTypeOf, fromConstrM, Constr, readConstr)
import Control.Monad (mzero)
import Text.ParserCombinators.ReadP

------------------------------------------------------------------------------

-- | Generic read: an alternative to \"deriving Read\"
gread :: Data a => ReadS a

{-

This is a read operation which insists on prefix notation.  (The
Haskell 98 read deals with infix operators subject to associativity
and precedence as well.) We use fromConstrM to "parse" the input. To be
precise, fromConstrM is used for all types except String. The
type-specific case for String uses basic String read.

-}

gread = readP_to_S gread'

 where

  -- Helper for recursive read
  gread' :: Data a' => ReadP a'
  gread' = allButString `extR` stringCase

   where

    -- A specific case for strings
    stringCase :: ReadP String
    stringCase = readS_to_P reads

    -- Determine result type
    myDataType = dataTypeOf (getArg allButString)
     where
      getArg :: ReadP a'' -> a''
      getArg = undefined

    -- The generic default for gread
    allButString =
      do
                -- Drop "  (  "
         skipSpaces                     -- Discard leading space
         char '('                       -- Parse '('
         skipSpaces                     -- Discard following space

                -- Do the real work
         str  <- parseConstr            -- Get a lexeme for the constructor
         con  <- str2con str            -- Convert it to a Constr (may fail)
         x    <- fromConstrM gread' con -- Read the children

                -- Drop "  )  "
         skipSpaces                     -- Discard leading space
         char ')'                       -- Parse ')'
         skipSpaces                     -- Discard following space

         return x

    -- Turn string into constructor driven by the requested result type,
    -- failing in the monad if it isn't a constructor of this data type
    str2con :: String -> ReadP Constr
    str2con = maybe mzero return
            . readConstr myDataType

    -- Get a Constr's string at the front of an input string
    parseConstr :: ReadP String
    parseConstr =
               string "[]"     -- Compound lexeme "[]"
          <++  infixOp         -- Infix operator in parantheses
          <++  readS_to_P lex  -- Ordinary constructors and literals

    -- Handle infix operators such as (:)
    infixOp :: ReadP String
    infixOp = do c1  <- char '('
                 str <- munch1 (not . (==) ')')
                 c2  <- char ')'
                 return $ [c1] ++ str ++ [c2]
