module SYB.Read.Main where

import Auxiliary.Tree (Tree(..), smallerTreeString, sumTree)
import Auxiliary.Logic (Logic(..), smallerLogicString, evalLogic)
import Auxiliary.Auxiliary (test)
import Data.Maybe
import SYB.Read.Read


mainTree :: IO ()
mainTree = let ((t,_):_) = gread smallerTreeString
       in test . putStr . show . sumTree $ t

mainLogic :: IO ()
mainLogic = let ((t,_):_) = gread smallerLogicString
            in test . putStr . show . evalLogic (const True) $ t
