module SYB.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.Auxiliary (test)
import SYB.Show.ShowOpt

mainTree :: IO ()
mainTree = test . putStr . show . last . gshow $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . last . gshow $ bigLogic
