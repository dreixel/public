module SYB.Show.ShowOpt where

import Data.Generics (extQ, Data, showConstr, toConstr, gmapQ)

------------------------------------------------------------------------------
-- | Generic show: an alternative to \"deriving Show\"
gshow :: Data a => a -> String
gshow x = gshowGeneral x ""

-- | Generic shows
gshows :: Data a => a -> ShowS

-- This is a prefix-show using surrounding "(" and ")",
-- where we recurse into subterms with gmapQ.
gshows = gshowGeneral `extQ` gshowString

gshowGeneral :: Data a => a -> ShowS
gshowGeneral t =   showChar '('
                 . (showString . showConstr . toConstr $ t)
                 . (foldr (.) id . gmapQ ((showChar ' ' .) . gshows) $ t)
                 . showChar ')'

gshowString :: String -> ShowS
gshowString = shows
