module Main where

import Tree (Tree(..), bigTree)
import Show

main :: IO ()
main = putStrLn . show . last . gshow $ bigTree
