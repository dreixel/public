module SYB.Map.Main where

import Auxiliary.Tree (smallerTree, sumTree)
import Auxiliary.Auxiliary (test, apply)
import SYB.Map.Map (everywhere, mkT)


mainTree :: IO ()
mainTree = let t = apply 1000 (everywhere (mkT (+(1 :: Int)))) smallerTree
           in test . putStr . show . sumTree $ t
