module SYB.Eq.Main where

import Auxiliary.Tree (bigTree, tweakRightmost)
import Auxiliary.Logic (biggerLogic, tweakLogic)
import Auxiliary.Auxiliary (test)
import SYB.Eq.Twins (geq)


mainTree :: IO ()
mainTree = test . putStr . show $ 
        (geq bigTree bigTree, geq bigTree (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $ 
              [ geq biggerLogic biggerLogic, 
                geq biggerLogic (tweakLogic biggerLogic),
                geq (tweakLogic biggerLogic) biggerLogic]
