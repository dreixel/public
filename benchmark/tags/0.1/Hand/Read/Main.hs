{-# LANGUAGE StandaloneDeriving #-}

module Hand.Read.Main where

import Auxiliary.Tree (Tree(..), smallerTreeString, sumTree)
import Auxiliary.Logic (Logic(..), smallerLogicString, evalLogic)
import Auxiliary.Auxiliary (test)

deriving instance (Read a) => Read (Tree a)
deriving instance Read Logic

mainTree :: IO ()
mainTree = let t = read smallerTreeString
           in test . putStr . show . sumTree $ t

mainLogic :: IO ()
mainLogic = let t = read smallerLogicString
            in test . putStr . show . evalLogic (const True) $ t
