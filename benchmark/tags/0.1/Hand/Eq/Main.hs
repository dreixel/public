{-# LANGUAGE StandaloneDeriving #-}

module Hand.Eq.Main where

import Auxiliary.Tree (Tree(..), bigTree, tweakRightmost)
import Auxiliary.Logic (Logic(..), biggerLogic, tweakLogic)
import Auxiliary.Auxiliary (test)

-- This crashes 6.8.3 with O flags
--deriving instance Eq Logic
--deriving instance (Eq a) => Eq (Tree a)

mainTree :: IO ()
mainTree = test . putStr . show $ 
            (bigTree == bigTree, bigTree == (tweakRightmost bigTree))

mainLogic :: IO ()
mainLogic = test . putStr . show . last . show $ 
             [ biggerLogic == biggerLogic, 
               biggerLogic == tweakLogic biggerLogic,
               tweakLogic biggerLogic == biggerLogic]
