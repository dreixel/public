{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TypeOperators        #-}

module Main where

import Tree (Tree, bigTree, sumTree)
import Map
import Generics.Regular.Base

main :: IO ()
main = let t = to . gmapf (+(1::Int)) . from $ bigTree
       in putStrLn . show . sumTree $ t
