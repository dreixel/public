module Regular.Eq.Main where

import Regular.Eq.Eq (eq)
import Auxiliary.Tree (bigTree, tweakRightmost)
import Auxiliary.Logic (biggerLogic, tweakLogic)
import Auxiliary.Regular
import Auxiliary.Auxiliary (test)

mainTree = test . putStr . show $ 
        (eq bigTree bigTree, eq bigTree (tweakRightmost bigTree))

mainLogic = test . putStr . show . last . show $ 
              [ eq biggerLogic biggerLogic, 
                eq biggerLogic (tweakLogic biggerLogic),
                eq (tweakLogic biggerLogic) biggerLogic]
