{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverlappingInstances #-}

module Regular.Update.Update where

import Generics.Regular


class UpdateInt f where
  updateIntf :: (a -> a) -> f a -> f a

instance UpdateInt U where
  updateIntf _ = id

instance UpdateInt I where
  updateIntf f (I x) = I (f x)

instance UpdateInt (K Int) where
  updateIntf f (K n) | odd n     = K (n + 1)
                     | otherwise = K (n - 1)

instance UpdateInt (K x) where
  updateIntf f (K x) = K x

instance (UpdateInt f, UpdateInt g) => UpdateInt (f :+: g) where
  updateIntf f (L x) = L (updateIntf f x)
  updateIntf f (R x) = R (updateIntf f x)

instance (UpdateInt f, UpdateInt g) => UpdateInt (f :*: g) where
  updateIntf f (x :*: y) = updateIntf f x :*: updateIntf f y

instance (UpdateInt f) => UpdateInt (C c f) where
  updateIntf f (C x) = C (updateIntf f x)
  
updateInt :: (Regular a, UpdateInt (PF a)) => a -> a
updateInt = to . updateIntf (updateInt) . from


class UpdateString f where
  updateStringf :: (a -> a) -> f a -> f a

instance UpdateString U where
  updateStringf _ = id

instance UpdateString I where
  updateStringf f (I x) = I (f x)

instance UpdateString (K String) where
  updateStringf f (K "") = K ""
  updateStringf f (K s)  = K (last s : tail s)

instance UpdateString (K x) where
  updateStringf f (K x) = K x

instance (UpdateString f, UpdateString g) => UpdateString (f :+: g) where
  updateStringf f (L x) = L (updateStringf f x)
  updateStringf f (R x) = R (updateStringf f x)

instance (UpdateString f, UpdateString g) => UpdateString (f :*: g) where
  updateStringf f (x :*: y) = updateStringf f x :*: updateStringf f y

instance (UpdateString f) => UpdateString (C c f) where
  updateStringf f (C x) = C (updateStringf f x)
  
updateString :: (Regular a, UpdateString (PF a)) => a -> a
updateString = to . updateStringf (updateString) . from
