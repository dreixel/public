module Regular.Update.Main where

import Auxiliary.Tree (bigTree, sumTree)
import Auxiliary.Logic (biggerLogic, evalLogic)
import Auxiliary.Regular
import Auxiliary.Auxiliary (test, apply)
import Regular.Update.Update

mainTree :: IO ()
mainTree = test . putStr . show . sumTree . (apply 4 updateInt) $ bigTree

mainLogic :: IO ()
mainLogic = test . putStr . show . evalLogic (const True) . (apply 800 updateString) $ biggerLogic
