{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TypeOperators    #-}

-----------------------------------------------------------------------------
-- |
-- Module      :  Generics.MultiRec.Show
-- Copyright   :  (c) 2008 Universiteit Utrecht
-- License     :  BSD3
--
-- Maintainer  :  generics@haskell.org
-- Stability   :  experimental
-- Portability :  non-portable
--
-- Generic show.
--
-----------------------------------------------------------------------------

module Regular.Show.Show where

import Generics.Regular.Base

-- | The @GShow@ class defines a show on values.
class GShow f where
  gshowf :: (a -> ShowS) -> f a -> ShowS

instance GShow I where
  gshowf f (I r) = f r

instance Show a => GShow (K a) where
  gshowf _ (K x) = shows x

instance GShow U where
  gshowf _ U = id

instance (GShow f, GShow g) => GShow (f :+: g) where
  gshowf f (L x) = gshowf f x
  gshowf f (R x) = gshowf f x

instance (GShow f, GShow g) => GShow (f :*: g) where
  gshowf f (x :*: y) = gshowf f x . showChar ' ' . gshowf f y


instance (Constructor c, GShow f) => GShow (C c f) where
  gshowf f cx@(C x) = 
    showParen True (showString (conName cx) . showChar ' ' . gshowf f x)


gshow :: (Regular a, GShow (PF a)) => a -> ShowS
gshow x = gshowf gshow (from x)
