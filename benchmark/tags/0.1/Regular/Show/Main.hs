module Regular.Show.Main where

import Auxiliary.Tree (Tree(..), bigTree)
import Auxiliary.Logic (Logic(..), bigLogic)
import Auxiliary.Regular
import Auxiliary.Auxiliary (test)
import qualified Regular.Show.Show as G


mainTree :: IO ()
mainTree = test . putStr . show . last $ G.gshow bigTree ""

mainLogic :: IO ()
mainLogic = test . putStr . show . last $ G.gshow bigLogic ""
