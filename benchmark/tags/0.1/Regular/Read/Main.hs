module Regular.Read.Main where

import Auxiliary.Tree (Tree(..), smallerTreeString, sumTree)
import Auxiliary.Logic (Logic(..), smallerLogicString, evalLogic)
import Auxiliary.Regular
import Auxiliary.Auxiliary (test)
import Regular.Read.Read as G


mainTree :: IO ()
mainTree = let t = G.read smallerTreeString
           in test. putStr . show . sumTree $ t

mainLogic :: IO ()
mainLogic = let t = G.read smallerLogicString
            in test. putStr . show . evalLogic (const True) $ t
