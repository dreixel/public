module Regular.Arbitrary.Main where

import Auxiliary.Tree (Tree(..), sumTree)
import Auxiliary.Logic (Logic(..), hashLogic)
import Auxiliary.Auxiliary (test, generate')
import Auxiliary.Regular

import Regular.Arbitrary.Arbitrary (arbitrary)

mainTree :: IO ()
mainTree = test . print . sum . map sumTree . take 100000 $ 
             (generate' arbitrary :: [Tree Int])

mainLogic :: IO ()
mainLogic = test . print . sum . map hashLogic . take 25000 $ 
              (generate' arbitrary :: [Logic])
