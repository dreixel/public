PATH=C:\ghc\ghc-6.8.3\bin\;%PATH%

ghc --make -fforce-recomp -O1 Main

Main.exe -n 10 -o results/outWinXP.O0.041109.ghc-6.8.3.txt -f "-O0"
Main.exe -n 10 -o results/outWinXP.O1.041109.ghc-6.8.3.txt -f "-O1"
Main.exe -n 10 -o results/outWinXP.O2.041109.ghc-6.8.3.txt -f "-O2"
Main.exe -n 10 -o results/outWinXP.O1UT60.041109.ghc-6.8.3.txt -f "-O1 -funfolding-use-threshold=60"
Main.exe -n 10 -o results/outWinXP.O1CT90.041109.ghc-6.8.3.txt -f "-O1 -funfolding-creation-threshold=90"
Main.exe -n 10 -o results/outWinXP.O1CT450UT60.041109.ghc-6.8.3.txt -f "-O1 -funfolding-creation-threshold=450 -funfolding-use-threshold=60"
Main.exe -n 10 -o results/outWinXP.O1CT1000UT180.041109.ghc-6.8.3.txt -f "-O1 -funfolding-creation-threshold=1000 -funfolding-use-threshold=180"

PATH=C:\ghc\ghc-6.10.4\bin\;%PATH%

ghc --make -fforce-recomp -O1 Main

Main.exe -n 10 -o results/outWinXP.O0.041109.ghc-6.10.4.txt -f "-O0"
Main.exe -n 10 -o results/outWinXP.O1.041109.ghc-6.10.4.txt -f "-O1"
Main.exe -n 10 -o results/outWinXP.O2.041109.ghc-6.10.4.txt -f "-O2"
Main.exe -n 10 -o results/outWinXP.O1UT60.041109.ghc-6.10.4.txt -f "-O1 -funfolding-use-threshold=60"
Main.exe -n 10 -o results/outWinXP.O1CT90.041109.ghc-6.10.4.txt -f "-O1 -funfolding-creation-threshold=90"
Main.exe -n 10 -o results/outWinXP.O1CT450UT60.041109.ghc-6.10.4.txt -f "-O1 -funfolding-creation-threshold=450 -funfolding-use-threshold=60"
Main.exe -n 10 -o results/outWinXP.O1CT1000UT180.041109.ghc-6.10.4.txt -f "-O1 -funfolding-creation-threshold=1000 -funfolding-use-threshold=180"

PATH=C:\ghc\ghc-6.12.0.20091010\bin\;%PATH%

ghc --make -fforce-recomp -O1 Main

Main.exe -n 10 -o results/outWinXP.O0.041109.ghc-6.12-rc1.txt -f "-O0"
Main.exe -n 10 -o results/outWinXP.O1.041109.ghc-6.12-rc1.txt -f "-O1"
Main.exe -n 10 -o results/outWinXP.O2.041109.ghc-6.12-rc1.txt -f "-O2"
Main.exe -n 10 -o results/outWinXP.O1UT60.041109.ghc-6.12-rc1.txt -f "-O1 -funfolding-use-threshold=60"
Main.exe -n 10 -o results/outWinXP.O1CT90.041109.ghc-6.12-rc1.txt -f "-O1 -funfolding-creation-threshold=90"
Main.exe -n 10 -o results/outWinXP.O1CT450UT60.041109.ghc-6.12-rc1.txt -f "-O1 -funfolding-creation-threshold=450 -funfolding-use-threshold=60"
Main.exe -n 10 -o results/outWinXP.O1CT1000UT180.041109.ghc-6.12-rc1.txt -f "-O1 -funfolding-creation-threshold=1000 -funfolding-use-threshold=180"