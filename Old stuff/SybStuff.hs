{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE FlexibleContexts     #-}

module SybStuff where

import Data.Generics
import Language.Haskell.Parser
import Control.Monad.Identity

ext1B :: (Data a, Typeable1 t)
      => a -> (forall b. Data b => (t b)) -> a
ext1B def ext = runIdentity (maybe (Identity def) id (dataCast1 (Identity ext)))

single :: forall a. Data a => a
single = general 
      `extB` char 
      `extB` int 
      `extB` float 
      `extB` double
      `ext1B` list where
  general :: Data a => a
  general = fromConstrB single (indexConstr (dataTypeOf general) 1)

  char   = '\NUL'
  int    = 0      :: Int
  float  = 0.0    :: Float
  double = 0.0    :: Double
  list :: Data b => [b]
  list  = [single]
  
empty :: forall a. Data a => a
empty = general 
      `extB` char 
      `extB` int 
      `extB` float 
      `extB` double where
  general :: Data a => a
  general = fromConstrB empty (indexConstr (dataTypeOf general) 1)

  char   = '\NUL'
  int    = 0      :: Int
  float  = 0.0    :: Float
  double = 0.0    :: Double

constrs :: forall a. Data a => [a]
constrs = general `extB` char `extB` int `extB` float `extB` double where
  general :: Data a => [a]
  general = map (fromConstrB empty)
              (dataTypeConstrs (dataTypeOf (unList general))) where
    unList :: Data a => [a] -> a
    unList = undefined

  char   = "\NUL"
  int    = [0   :: Int]
  float  = [0.0 :: Float]
  double = [0.0 :: Double]


main = do s <- readFile "SybStuff.hs" 
          case parseModule s of
            ParseOk p           -> putStrLn $ show $ gsize s
            e@(ParseFailed _ _) -> error $ show e