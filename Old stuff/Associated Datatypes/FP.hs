{-# OPTIONS -fglasgow-exts #-}

module FP where

type Name =  String

---------------------------------
-- Structure types
---------------------------------

-- For FP view
data UnitV         r  = UnitVC                        deriving Show
data IdV           r  = IdVC    { unId :: r }         deriving Show
data KV     a      r  = KVC     a                     deriving Show
data SumV   a  bT  r  = InlVC   (a r) | InrVC (bT r)  deriving Show
data ProdV  a  bT  r  = ProdVC  (a r) (bT r)          deriving Show

-- Fix
newtype Fix f = In {out :: f (Fix f)}

---------------------------------
-- Fixed-Point view
---------------------------------
  
class FPView a where
  type FP a :: * -> *
  fromFP  :: Fix (FP a) -> a
  toFP    :: a -> Fix (FP a)

---------------------------------
-- Generic function classes
---------------------------------
-- I'm not sure this is how one should do it...
class GenericFP g where
  unitFP      ::  g r -> g (UnitV r)
  idFP        ::  g r -> g (IdV r)
  kFP         ::  a -> g r -> g (KV a r)
  plusFP      ::  g (a r) -> g (b r) -> g r -> g (SumV a b r)
  prodFP      ::  g (a r) -> g (b r) -> g r -> g (ProdV a b r)
  datatypeFP  ::  FPView a => g (Fix (FP a)) -> g a
--constrFP  ::  Name -> g a -> g a
  
  --fix          ::  FPView f => g f -> g (Fix (FP f))
  --fix          ::  FPView f => g (f r) -> g (Fix (FP f))

---------------------------------
-- Type representation classes
---------------------------------

-- Arity 0
{-
class GRepFP gT aT where
  grepfp :: gT aT

instance GenericFP gT r => GRepFP gT (UnitV r) where
  grepfp = unitFP

instance GenericFP gT r => GRepFP gT (IdV r) where
  grepfp = idFP

instance (GenericFP gT r, GRepFP gT aT) => GRepFP gT (KV aT r) where
  grepfp = kFP grepfp

instance (GenericFP gT r, GRepFP gT aT, GRepFP gT bT) => GRepFP gT (SumV aT bT r) where
  grepfp = plusFP grepfp grepfp

instance (GenericFP gT r, GRepFP gT aT, GRepFP gT bT) => GRepFP gT (ProdV aT bT r) where
  grepfp = prodFP grepfp grepfp
-}

---------------------------------
-- Tree a
---------------------------------

-- Datatype

data Tree a = Leaf | Branch a (Tree a) (Tree a) deriving Show
  
-- FP view

instance FPView (Tree a) where
  type FP (Tree a) = SumV UnitV (ProdV (KV a) (ProdV IdV IdV))
  
  toFP Leaf             = In (InlVC UnitVC)
  toFP (Branch x t1 t2) = In (InrVC (ProdVC (KVC x) (ProdVC (IdVC (toFP t1)) (IdVC (toFP t2)))))

  fromFP (In (InlVC UnitVC))                                        = Leaf
  fromFP (In (InrVC (ProdVC (KVC x) (ProdVC (IdVC t1) (IdVC t2))))) = Branch x (fromFP t1) (fromFP t2)

---------------------------------
-- [a]
---------------------------------
  
-- FP view

instance FPView [aT] where
  type FP [aT] = SumV UnitV (ProdV (KV aT) IdV)
  
  toFP []        = In (InlVC UnitVC)
  toFP (x : xs)  = In (InrVC (ProdVC (KVC x) (IdVC (toFP xs))))

  fromFP (In (InlVC UnitVC))                     = []
  fromFP (In (InrVC (ProdVC (KVC x) (IdVC r))))  = x : fromFP r

---------------------------------
-- gempty using the FP view
---------------------------------

newtype Gempty a = Gempty { gempty' :: a }
-- I'm not sure this is how one should do it...
instance GenericFP Gempty where
  unitFP r = Gempty UnitVC
  idFP r = Gempty (IdVC (gempty' r))
  kFP a r = Gempty (KVC a)
  plusFP a b r = Gempty (InlVC (gempty' a))
  prodFP a b r = Gempty (ProdVC (gempty' a) (gempty' b))
  datatypeFP d = Gempty (fromFP (gempty' d))

  --fix     f      = Gempty (In (gempty' f))

--gempty :: ?
--gempty = gempty' ?