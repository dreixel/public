{-# OPTIONS_GHC -fglasgow-exts -Wall #-}

module Main where

import Prelude hiding (lookup)
import Char (ord)
import qualified Data.Map as Map


-- Generic maps as ATs
-- -------------------
data UnitT      = Unit deriving Show
data aT :+: bT  = L aT | R bT deriving Show
data aT :*: bT  = Prod aT bT deriving Show


-- Generalized tries, as from http://www.haskell.org/haskellwiki/GHC/Indexed_types

class View k => GMapKey k where
  data GMap k :: * -> *
  empty       :: GMap k v
  lookup      :: k -> GMap k v -> Maybe v
  insert      :: k -> v -> GMap k v -> GMap k v

instance GMapKey Int where
  data GMap Int v        = GMapInt (Map.Map Int v)
  empty                  = GMapInt Map.empty
  lookup k (GMapInt m)   = Map.lookup k m
  insert k v (GMapInt m) = GMapInt (Map.insert k v m)

instance GMapKey Char where
  data GMap Char v        = GMapChar (GMap Int v)
  empty                   = GMapChar empty
  lookup k (GMapChar m)   = lookup (ord k) m
  insert k v (GMapChar m) = GMapChar (insert (ord k) v m)

instance GMapKey UnitT where
  data GMap UnitT v          = GMapUnit (Maybe v)
  empty                      = GMapUnit Nothing
  lookup Unit (GMapUnit v)   = v
  insert Unit v (GMapUnit _) = GMapUnit $ Just v

instance (GMapKey a, GMapKey b) => GMapKey (a :*: b) where
  data GMap (a :*: b) v             = GMapProd (GMap a (GMap b v))
  empty                             = GMapProd empty
  lookup (Prod a b) (GMapProd gm)   = lookup a gm >>= lookup b 
  insert (Prod a b) v (GMapProd gm) = 
    GMapProd $ case lookup a gm of
      Nothing  -> insert a (insert b v empty) gm
      Just gm2 -> insert a (insert b v gm2  ) gm

instance (GMapKey a, GMapKey b) => GMapKey (a :+: b) where
  data GMap (a :+: b) v              = GMapSum (GMap a v) (GMap b v)
  empty                              = GMapSum empty empty
  lookup (L  a) (GMapSum gm1  _gm2)  = lookup a gm1
  lookup (R b) (GMapSum _gm1 gm2 )   = lookup b gm2
  insert (L  a) v (GMapSum gm1 gm2)  = GMapSum (insert a v gm1) gm2
  insert (R a) v (GMapSum gm1 gm2)   = GMapSum gm1 (insert a v gm2)
  
{-
-- Example list instance

instance GMapKey k => GMapKey [k] where
  data GMap [k] v = GMapL (Maybe v) (GMap k (GMap [k] v))
  
  empty = GMapL Nothing empty
  lookup [] (GMapL tn tc) = tn
  lookup (h:t) (GMapL tn tc) = (lookup h >=> lookup t) tc
  insert [] v (GMapL tn tc) = GMapL (Just v) tc
  insert (h:t) v m@(GMapL tn tc) = GMapL tn (insert h (insert t v m) tc)
-}
  
-- Sum of Products View
class View a where
  type SP a
  from :: SP a -> a
  to :: a -> SP a
  -- defaults
  {-
  type SP a = a -- type synonyms are not yet implemented!
  from = id
  to = id
  -}
  
instance View Int where 
  type SP Int = Int
  from = id
  to = id
  
instance View Char where 
  type SP Char = Char
  from = id
  to = id
  
instance View UnitT where 
  type SP UnitT = UnitT
  from = id
  to = id

instance (View a, View b) => View (a :*: b) where 
  type SP (a :*: b) = a :*: b
  from = id
  to = id

instance (View a, View b) => View (a :+: b) where 
  type SP (a :+: b) = a :+: b
  from = id
  to = id
 
 
instance View [a] where
  type SP [a] = UnitT :+: (a :*: [a])
  to  []                 =  L Unit
  to  (a:as)             =  R (Prod a as)
  from  (L Unit)         =  []
  from  (R (Prod a as))  =  (a:as)
  
instance GMapKey k => GMapKey [k] where
  data GMap [k] v = GMapList (GMap (SP [k]) v)
  
  empty = GMapList empty
  lookup k (GMapList m) = lookup (to k) m
  insert k v (GMapList m) = GMapList (insert (to k) v m)

t1 :: Maybe String
t1 = lookup [1,2,3] $ insert ([1..3] :: [Int]) "[1,2,3]" $ empty