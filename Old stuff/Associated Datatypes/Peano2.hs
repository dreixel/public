{-# OPTIONS -fglasgow-exts -fallow-undecidable-instances #-}

-- static Peano constructors and numerals

data Zero
data Succ n

type One   = Succ Zero
type Two   = Succ One
type Three = Succ Two
type Four  = Succ Three


-- dynamic representatives for static Peanos

zero  = undefined :: Zero
one   = undefined :: One
two   = undefined :: Two
three = undefined :: Three
four  = undefined :: Four


-- addition, a la Prolog

type family Sum n m
type instance Sum Zero a = a
type instance Sum (Succ a) b = Succ (Sum a b)

-- partially incorrect: cannot add zero zero
add :: (Sum a b ~ Succ c) => a -> b -> Succ c
add = undefined

-- multiplication, a la Prolog

type family Mul n m
type instance Mul Zero a = Zero
type instance Mul (Succ a) b = Sum b (Mul a b)

-- partially incorrect: cannot multiply by zero
mul :: (Mul a b ~ Succ c) => a -> b -> Succ c
mul = undefined


type family Fac a
type instance Fac Zero = One
type instance Fac (Succ n) = Mul (Succ n) (Fac n)

fac :: (Fac a ~ Succ b) => a -> Succ b
fac = undefined