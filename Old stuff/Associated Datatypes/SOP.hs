{-# OPTIONS -fglasgow-exts -fallow-undecidable-instances -Wall #-}
module SOP where

type Name =  String

---------------------------------
-- Structure types
---------------------------------

-- For SOP view
data UnitT        = Unit
data SumT aT bT   = Inl aT | Inr bT
data ProdT aT bT  = Prod aT bT

---------------------------------
-- Embedding-projection pair
---------------------------------

--data EP bT cT = EP_ {from :: (bT -> cT), to :: (cT -> bT)}

---------------------------------
-- Sum of products view
---------------------------------

class SumOfProductsView a where
  type SOP a
  fromSOP :: SOP a -> a
  toSOP   :: a -> SOP a

---------------------------------
-- Generic function classes
---------------------------------

class Generic gT where
  unit      ::  gT UnitT
  int       ::  gT Int
  char      ::  gT Char
  plus      ::  gT aT -> gT bT -> gT (SumT aT bT)
  prod      ::  gT aT -> gT bT -> gT (ProdT aT bT)
  datatype  ::  SumOfProductsView aT => gT (SOP aT) -> gT aT
  constr    ::  Name -> gT aT -> gT aT  

---------------------------------
-- Type representation classes
---------------------------------

-- Arity 0

class GRep gT aT where
  grep :: gT aT

instance Generic gT => GRep gT UnitT where
  grep = unit

instance Generic gT => GRep gT Int where
  grep = int

instance Generic gT => GRep gT Char where
  grep = char

instance (Generic gT, GRep gT aT, GRep gT bT) => GRep gT (SumT aT bT) where
  grep = plus grep grep

instance (Generic gT, GRep gT aT, GRep gT bT) => GRep gT (ProdT aT bT) where
  grep = prod grep grep

---------------------------------
-- Tree a
---------------------------------

-- Datatype

data Tree a = Leaf | Branch a (Tree a) (Tree a) deriving Show

-- Embedding-projection pair

fromTree :: Tree aT -> SumT UnitT (ProdT aT (ProdT (Tree aT) (Tree aT)))
fromTree  Leaf               =  Inl Unit
fromTree  (Branch a lt rt)   =  Inr (Prod a (Prod lt rt))

toTree :: SumT UnitT (ProdT aT (ProdT (Tree aT) (Tree aT))) -> Tree aT
toTree  (Inl Unit)                   = Leaf
toTree  (Inr (Prod a (Prod lt rt)))  = Branch a lt rt

-- Representation instances

--rTree :: Generic gT => gT a -> gT (Tree a)
rTree :: (SumT UnitT (ProdT a (ProdT b b)) ~ SOP b,
         SumOfProductsView b,
         Generic gT) => gT a -> gT b
rTree rA = datatype  (plus unit (prod rA (prod (rTree rA) (rTree rA))))

instance (Generic gT, GRep gT aT) => GRep gT (Tree aT) where
  grep = rTree grep

-- SOP view

instance SumOfProductsView (Tree a) where
  type SOP (Tree a) = SumT UnitT (ProdT a (ProdT (Tree a) (Tree a)))
  toSOP  = fromTree
  fromSOP = toTree

---------------------------------
-- [a]
---------------------------------

-- Embedding-projection pair

fromList :: [aT] -> SumT UnitT (ProdT aT [aT])
fromList  []                =  Inl Unit
fromList  (a:as)            =  Inr (Prod a as)

toList :: SumT UnitT (ProdT aT [aT]) -> [aT]
toList  (Inl Unit)          =  []
toList  (Inr (Prod a as))   =  a:as

-- Representation instances

--rList :: Generic gT => gT a -> gT [a]
rList :: (SumT UnitT (ProdT a b) ~ SOP b,
         SumOfProductsView b,
         Generic gT) => gT a -> gT b
rList rA = datatype  (plus  (constr "[]" unit)
                            (constr "(:)" (prod rA (rList rA))))

instance (Generic gT, GRep gT aT) => GRep gT [aT] where
  grep = rList grep

-- SOP view

instance SumOfProductsView [a] where
  type SOP [a] = SumT UnitT (ProdT a [a])
  toSOP  = fromList
  fromSOP = toList

---------------------------------
-- gempty
---------------------------------

newtype GemptyT aT = Gempty { gempty' :: aT }

instance Generic GemptyT where
  unit                  =  Gempty Unit
  int                   =  Gempty 0
  char                  =  Gempty '\NUL'
  plus          rA  _   =  Gempty (Inl (gempty' rA))
  prod          rA  rB  =  Gempty (Prod (gempty' rA) (gempty' rB))
  datatype      rA      =  Gempty (fromSOP (gempty' rA))
  constr    _   rA      =  Gempty (gempty' rA)

gempty :: GRep GemptyT aT => aT
gempty = gempty' grep

---------------------------------
-- Tests
---------------------------------

e1 :: [Int]
e1 = gempty

e2 :: Tree Int
e2 = gempty