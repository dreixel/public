{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -XTypeSynonymInstances #-}

module Main where

-- Adding variables to datatypes
-- -----------------------------

class Extend a where
  data ExtendWith a :: * -> *
  match             :: (Show v) => ExtendWith a v -> a -> Maybe [(v,a)]
  
  build :: Eq v => [(v,a)] -> ExtendWith a v -> Maybe a
  
  apply :: (Eq v, Show v) => (ExtendWith a v, ExtendWith a v) -> a -> Maybe a
  apply (lhs, rhs) x = do s <- match lhs x
                          build s rhs

type FExtend v f r = Either v (f r)

instance Children f => Extend (Fix f) where
  data ExtendWith (Fix f) v = ExtendWithFix (Fix (FExtend v f))
  match (ExtendWithFix (In (Left v))) x = Just [(v,x)]
  match (ExtendWithFix (In (Right e))) x = 
    gzipWithM match (cfmap ExtendWithFix e) (out x) >>= reduce (Just []) mergeSubst
    
  build s (ExtendWithFix (In (Left v)))  = lookup v s
  build s (ExtendWithFix (In (Right e))) = mfmap (build s . ExtendWithFix) e >>= return . In

mergeSubst :: Maybe [(v,a)] -> Maybe [(v,a)] -> Maybe [(v,a)]
mergeSubst s1 s2 = do s1' <- s1
                      s2' <- s2
                      return (s1' ++ s2')


-- Fix datatype
data Fix f = In (f (Fix f))
out (In x) = x

class Children f where
  children :: f a -> [a]
  gzipWith :: (a -> b -> c) -> f a -> f b -> f c
  gzipWithM :: Monad m => (a -> b -> c) -> f a -> f b -> m (f c)
  reduce   :: a -> (a -> a -> a) -> f a -> a
  cfmap    :: (a -> b) -> (f a -> f b)
  mfmap    :: Monad m => (a -> m b) -> (f a -> m (f b))

data Logic = LTrue | LFalse | Not Logic | Or Logic Logic
  deriving Show

type FLogic = Fix FunLogic

instance Show FLogic where
  show (In f) = "(In " ++ show f ++ ")"

data FunLogic r = FunTrue | FunFalse | FunNot r | FunOr r r 
  deriving Show

-- Utility conversion functions
toLogic :: FLogic -> Logic
toLogic (In FunTrue) = LTrue
toLogic (In FunFalse) = LFalse
toLogic (In (FunNot l)) = Not (toLogic l)
toLogic (In (FunOr l r)) = Or (toLogic l) (toLogic r)

fromLogic :: Logic -> FLogic
fromLogic LTrue = In FunTrue
fromLogic LFalse = In FunFalse
fromLogic (Not l) = In (FunNot (fromLogic l))
fromLogic (Or l r) = (In (FunOr (fromLogic l) (fromLogic r)))

instance Children FunLogic where
  children (FunOr l r) = [l,r]
  children (FunNot l) = [l]
  children (FunTrue) = []
  children (FunFalse) = []
  
  gzipWith f FunTrue FunTrue = FunTrue
  gzipWith f FunFalse FunFalse = FunFalse
  gzipWith f (FunNot l1) (FunNot l2) = FunNot (f l1 l2)
  gzipWith f (FunOr l1 r1) (FunOr l2 r2) = FunOr (f l1 l2) (f r1 r2)
  gzipWith f _ _ = error "gzipWith fail"
  
  gzipWithM f FunTrue FunTrue = return FunTrue
  gzipWithM f FunFalse FunFalse = return FunFalse
  gzipWithM f (FunNot l1) (FunNot l2) = return $ FunNot (f l1 l2)
  gzipWithM f (FunOr l1 r1) (FunOr l2 r2) = return $ FunOr (f l1 l2) (f r1 r2)
  gzipWithM f _ _ = fail "gzipWith fail"
  
  reduce e f (FunNot l) = f l e
  reduce e f (FunOr l r) = f l r
  reduce e f _ = e
  
  cfmap f FunTrue = FunTrue
  cfmap f FunFalse = FunFalse
  cfmap f (FunNot l) = FunNot (f l)
  cfmap f (FunOr l r) = FunOr (f l) (f r)
  
  mfmap f FunTrue = return FunTrue
  mfmap f FunFalse = return FunFalse
  mfmap f (FunNot l) = f l >>= return . FunNot
  mfmap f (FunOr l r) = do l' <- f l
                           r' <- f r
                           return (FunOr l' r')
  

-- Testing rewrite rules
doubleNeg = (lhs, rhs) where
  -- Not (Not p)
  lhs :: ExtendWith FLogic String
  lhs = ExtendWithFix (In (Right (FunNot (
                        In (Right (FunNot (
                          In (Left "p")
                      )))))))
  -- p
  rhs :: ExtendWith FLogic String
  rhs = ExtendWithFix (In (Left "p"))
  
l1 = fromLogic (Not (Not (Or LTrue LFalse)))
l2 = fromLogic (Or (Not (Not (Or LTrue LFalse))) (LFalse))

test1 = fmap toLogic (apply doubleNeg l1)
test2 = apply doubleNeg l2