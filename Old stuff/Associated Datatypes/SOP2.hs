{-# OPTIONS -fglasgow-exts -fallow-undecidable-instances -Wall #-}
module SOP2 where

---------------------------------
-- Structure types
---------------------------------

-- For SOP view
data UnitT        = Unit
data SumT aT bT   = Inl aT | Inr bT
data ProdT aT bT  = Prod aT bT

---------------------------------
-- Sum of products view
---------------------------------

class SumOfProductsView a where
  type SOP a
  fromSOP :: SOP a -> a
  toSOP   :: a -> SOP a
  
---------------------------------
-- 
---------------------------------

instance SumOfProductsView [a] where
  type SOP [a] = SumT UnitT (ProdT a [a])
  toSOP [] = Inl Unit
  toSOP (x:xs) = Inr (Prod x xs)
  fromSOP (Inl Unit) = []
  fromSOP (Inr (Prod x xs)) = x:xs

class GShow a where
  gshow' :: a -> String

instance GShow UnitT where
  gshow' Unit = "Unit"
  
instance GShow Int where
  gshow' = show
  
instance (GShow a, GShow b) => GShow (SumT a b) where
  gshow' (Inl l) = "Left (" ++ gshow' l ++ ")"
  gshow' (Inr r) = "Right (" ++ gshow' r ++ ")"
  
instance (GShow a, GShow b) => GShow (ProdT a b) where
  gshow' (Prod l r) = "Prod (" ++ gshow' l ++ ") (" ++ gshow' r ++ ")"

{-
-- This is what I want to do
instance (SumOfProductsView a) => GShow a where
  gshow' x = gshow' (toSOP x)
-}
-- But this is what I have to do  
instance GShow a => GShow [a] where
  gshow' x = gshow' (toSOP x)

gshow :: (GShow a) => a -> String
gshow x = gshow' x

example1 :: String
example1 = gshow ([1,2,3]::[Int])
