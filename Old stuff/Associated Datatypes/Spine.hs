{-# OPTIONS -fglasgow-exts -XOverlappingInstances  -fallow-undecidable-instances -Wall #-}
module Spine where

---------------------------------
-- Structure types
---------------------------------
{-
data Spine a where
  Con   ::  a -> Spine a
  (:$)  ::  Spine (a -> b) -> a -> Spine b
-}

data Con a = Con a

data App f a = f :$ a
  
data Sum a b = Inl a | Inr b
--data Typed a  =  (:>) { typeOf :: Type a, val :: a }

class SpineView a where
  type SV a
  toSpine   :: a -> SV a
  fromSpine :: SV a -> a
{-
fromSpine :: SV a -> a
fromSpine (Con c)  = c
fromSpine (f :$ x) = (fromSpine f) x
-}
instance SpineView Int where
  type SV Int = Con Int
  toSpine = Con
  fromSpine (Con n) = n
  
instance SpineView a => SpineView [a] where
  type SV [a] = Sum (Con [a]) (App (Con [a]) (SV a))
  toSpine [] = Inl (Con [])
  toSpine (x:xs) = Inr (Con (:) :$ x :$ xs)
  
  fromSpine (Inl (Con [])) = []
  fromSpine (Inr (Con (:) :$ x :$ xs)) = x:xs
{-
class GSum' a where
  gsum' :: a -> Int

instance (GSum a, GSum (Spine (a -> b))) => GSum' (Spine a) where
  gsum' (Con _) = 0
  gsum' (f :$ x) = gsum f + gsum x


class (SpineView a, GSum'  => GSum a where
  gsum :: a -> Int
  
instance GSum Int where
  gsum n = n
  -}
{-
example1, example2 :: Int
example1 = gsum (IntR :> (5::Int)) -- == 5, good
example2 = gsum ((ListR IntR) :> ([1,2,3]::[Int])) -- == 0, why?
-}
-- Note, however, that this all works without Typed as well...
-- just comment the code above and uncomment below.
{-
data Spine aT where
  Con   ::  aT -> Spine aT
  (:$)  ::  SpineView aT => Spine (aT -> bT) -> aT -> Spine bT

--data Typed aT  =  (:>) { typeOf :: Type aT, val :: aT }

class SpineView a where
  data Type a
  toSpine   :: a -> Spine a
  
fromSpine :: Spine aT -> aT
fromSpine (Con c)  = c
fromSpine (f :$ x) = (fromSpine f) x

instance SpineView Int where
  data Type Int = IntR
  toSpine n = Con n
  
instance SpineView a => SpineView [a] where
  data Type [a] = ListR (Type a)
  toSpine [] = Con []
  toSpine (x:xs)  = Con (:) :$ x :$ xs

class SpineView aT => GSum aT where
  gsum :: aT -> Int

instance GSum Int where
  gsum n = n

instance SpineView aT => GSum aT where
  gsum x = gsum' (toSpine x)

gsum' :: Spine aT -> Int
gsum' (Con _)  = 0
gsum' (f :$ x) = gsum' f + gsum x

example1, example2 :: Int
example1 = gsum (5::Int) -- == 5, good
example2 = gsum ([1,2,3]::[Int]) -- == 0, why?
-}
---------------------------------
-- Spine view
---------------------------------
  


---------------------------------
-- Tree a
---------------------------------

-- Datatype

data Tree a = Leaf | Branch a (Tree a) (Tree a) deriving Show

---------------------------------
-- [a]
---------------------------------


---------------------------------
-- some generic function using the Spine view
---------------------------------

---------------------------------
-- Tests
---------------------------------

