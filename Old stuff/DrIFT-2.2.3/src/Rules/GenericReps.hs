-----------------------------------------------------------------------------
-- |
-- Module      :  Rules.GenericReps
-- Copyright   :  (c) Universiteit Utrecht, 2008
-- License     :  BSD-style
-- 
-- Maintainer  :  generics@haskell.org
-- Stability   :  experimental
-- Portability :  non-portable
--
-- This module provides DrIFT rules for derivation of diverse 
-- generic programming datatype representations.
--
-----------------------------------------------------------------------------

module Rules.GenericReps (rules) where

import RuleUtils hiding (opt, block, parenList)
import Data.List (mapAccumL)
import Char (ord)

rules = [("SPView", spviewfn, "Representation", "Sum of Products View", Nothing),
         ("PFView", pfviewfn, "Representation", "Pattern Functor View", Nothing),
         ("GRep", grepfn, "Representation", "EMGM representation", Nothing)]

-- shortcuts
spviewD, instanceD, whereD, dataD, zeroD, sumD, prodD, 
  unitD, conD, conInfoD, toD, fromD, inlD, inrD, undefinedD,
  pfviewD, kD, idD, grepD, epD, lD, rD, genericD,
  rtypeD, rsumD, rconD, rprodD, runitD :: Doc
spviewD = text "SPView"
instanceD = text "instance"
whereD = text "where"
dataD = text "Data"
zeroD = text "Zero"
sumD = text "Sum"
prodD = text "Prod"
unitD = text "Unit"
conD = text "Con"
conInfoD = text "ConInfo"
toD = text "to"
fromD = text "from"
inlD = text "Inl"
inrD = text "Inr"
undefinedD = text "undefined"
pfviewD = text "PFView"
kD = text "K"
idD = text "Id"
grepD = text "GRep g"
epD = text "ep"
lD = text "L"
rD = text "R"
conDescrD = text "ConDescr"
genericD = text "Generic g"
rtypeD = text "rtype"
rsumD = text "rsum"
rconD = text "rcon"
rprodD = text "rprod"
runitD = text "runit"

spviewfn :: Data -> Doc
spviewfn d = instanceHead $$ block [typeFam, tofn, fromfn] $$ conInfos where
  -- Datatype applied to all its variables, parenthesized when necessary
  appType = (if (null (vars d)) then id else parens) $ 
    (text (name d) <+> hsep (map text (vars d)))
  -- first line of instance declaration
  instanceHead = instanceD <+> constrs <+> spviewD <+> appType <+> whereD
  -- constraints of instance
  constrs = parenList (map constrsText (vars d)) <+> text "=>"
  constrsText x = dataD <+> text x <> comma <+> spviewD <+> text x
  -- associated type (always a sum of products)
  typeFam = text "type SP" <+> appType <+> equals <+> sumT (body d)
  -- sum at type level
  sumT :: [Body] -> Doc
  sumT [] = zeroD
  sumT (b:t) = sumD <+> conT b <+> parens (sumT t)
  -- product at type level
  prodT :: [Type] -> Doc
  prodT [] = unitD
  prodT (b:t) = prodD <+> parens (prettyType b) <+> parens (prodT t)
  -- constructor at type level
  conT :: Body -> Doc
  conT b = parens (conD <+> parens (prodT (types b)))
  -- to function
  --mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
  tofn = vcat $ snd $ mapAccumL tofn1 0 (body d)
  -- one line of to function
  tofn1 n b = (succ n, toD <+> tolhs b <+> equals <+> torhs n b)
  -- pattern matching on the left hand side
  tolhs b = parens (text (constructor b) <+> hsep (varNames (types b)))
  torhs 0 b = inlD <+> parens (con b)
  torhs n b = inrD <+> parens (torhs (n-1) b)
  -- constructor at value level
  con b = conD <+> confnName b <+> parens (prod (varNames (types b)))
  prod [] = unitD
  prod (x:t) = prodD <+> x <+> parens (prod t)
  -- from function
  fromfn = vcat $ snd $ mapAccumL fromfn1 0 (body d)
  -- one line of from function
  fromfn1 n b = (succ n, fromD <+> parens (torhs n b) <+> equals <+> tolhs b)
  -- ConInfo for each constructor
  confnName b = 
    if (isInfix (constructor b))
      -- TODO: Fix terrible idea below to deal with symbols in infix constructors
      then text "con" <> text (concat (map (show . ord) (constructor b)))
        else text "con" <> text (constructor b)
  conInfos = conInfoty (body d) $$ vcat (map conInfo (body d))
  conInfoty bs = list (map confnName bs) <+> text "::" <+> conInfoD 
  conInfo b = confnName b <+> text "=" <+> conInfoD <+> text "{" <+>
                list [conInfoName b, conInfoArity b, conInfoFixity b, conInfoAssoc b]
                <+> text "}"
  conInfoName b = text "name = \"" <> text (constructor b) <> text "\""
  conInfoArity b = text "arity = " <> text (show (length (types b)))
  conInfoFixity b = text "fixity = " <> 
                      text (if isInfix (constructor b) then "Infix" else "Prefix")
  conInfoAssoc b = text "associativity = " <> undefinedD
  
pfviewfn :: Data -> Doc
pfviewfn d = instanceHead $$ block [typeFam, tofn, fromfn] where
  -- Datatype applied to all its variables, parenthesized when necessary
  appType = (if (null (vars d)) then id else parens) $ 
    (text (name d) <+> hsep (map text (vars d)))
  -- first line of instance declaration
  instanceHead = instanceD <+> pfviewD <+> appType <+> whereD
  -- associated type (always a sum of products)
  typeFam = text "type PF" <+> appType <+> equals <+> sumT (body d)
  -- sum at type level
  sumT :: [Body] -> Doc
  sumT [] = zeroD
  sumT (b:t) = sumD <+> conT b <+> parens (sumT t)
  -- constructor at type level
  conT :: Body -> Doc
  conT b = parens (conD <+> parens (prodT (types b)))
  -- product at type level
  prodT :: [Type] -> Doc
  prodT [] = unitD
  prodT (h@(Con s):t) | s == name d = prodD <+> idD <+> parens (prodT t)
                      | otherwise   = prodTgeneral h <+> parens (prodT t)
  prodT (h@(LApply (Con s) _):t) | s == name d = prodD <+> idD <+> parens (prodT t)
                                 | otherwise   = prodTgeneral h <+> parens (prodT t)
  prodT (h:t) = prodTgeneral h <+> parens (prodT t)
  prodTgeneral b = prodD <+> parens (kD <+> prettyType b)
  -- from function
  -- mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
  fromfn = vcat $ snd $ mapAccumL fromfn1 0 (body d)
  -- one line of from function
  fromfn1 n b = (succ n, fromD <+> fromlhs b <+> equals <+> fromrhs n b)
  -- pattern matching on the left hand side
  fromlhs b = parens (text (constructor b) <+> hsep (varNames (types b)))
  fromrhs 0 b = inlD <+> parens (con b)
  fromrhs n b = inrD <+> parens (fromrhs (n-1) b)
  -- constructor at value level
  con b = conD <+> confnName b <+> parens (prod (varNames (types b)) (types b))
  prod [] _ = unitD
  prod (x:t) (y:t') = prodD <+> parens (unit x y) <+> parens (prod t t')
  unit x t@(Con s) | s == name d = idD <+> x
                   | otherwise   = kD <+> x
  unit x t@(LApply (Con s) _) | s == name d = idD <+> x 
                              | otherwise   = kD <+> x
  unit x _ = kD <+> x
  -- to function
  tofn = vcat $ snd $ mapAccumL tofn1 0 (body d)
  -- one line of to function
  tofn1 n b = (succ n, toD <+> parens (fromrhs n b) <+> equals <+> fromlhs b)
  -- String name for each constructor
  confnName b = text "\"" <> text (constructor b) <> text "\""

grepfn :: Data -> Doc
grepfn d = epfn <+> whereD $$ 
            block 
              [fromfn, tofn] $$
            emptyLine $$
            conInfos $$
            emptyLine $$
            rType $$
            emptyLine $$
            instanceHead $$
            block [instanceBody] $$
           emptyLine where
  -- Datatype applied to all its variables, parenthesized when necessary
  appType = (if (null (vars d)) then id else parens) $ 
    (text (name d) <+> hsep (map text (vars d)))
  -- first line of instance declaration
  instanceHead = instanceD <+> constrs <+> grepD <+> appType <+> whereD
  -- constraints of instance
  constrs = parenList (genericD : map constrsText (vars d)) <+> text "=>"
  constrsText x = grepD <+> text x
  -- instance body
  instanceBody = text "grep" <+> equals <+> text "r" <> text (name d) <+> 
                  hsep (replicate (length (vars d)) (text "grep"))
  -- Representation
  rType = rTypeType $$ rTypeLhs <+> equals <+> rTypeRhs
  -- Type of the type representation function
  rTypeType = text "r" <> text (name d) <+> text "::" <+> genericD <+> text "=>" <+>
                funSep (map ((text "g" <+>) . text) (vars d)) 
                  <+> text "g" <+> appType
  rTypeLhs = text "r" <> text (name d) <+> rTypeParams
  rTypeParams = hsep $ map (text . ('r':)) (vars d)
  rTypeRhs = rtypeD <+> epD <> text (name d) <+> parens (typeRepS (body d))
  typeRepS :: [Body] -> Doc
  typeRepS [] = error "[] in typeRepS"
  typeRepS [b] = parens (typeRepC b)
  typeRepS (b:t) = rsumD <+> parens (typeRepC b) <+> parens (typeRepS t)
  typeRepC :: Body -> Doc
  typeRepC b = rconD <+> confnName b <+> parens (typeRepP (map prettyType' (types b)))
  typeRepP :: [Doc] -> Doc
  typeRepP [] = runitD
  typeRepP [b] = b
  typeRepP (b:t) = rprodD <+> parens b <+> parens (typeRepP t)
  -- We need to redefine prettyType for type applications
  prettyType' :: Type -> Doc
  prettyType' (LApply t ts) = text "r" <> prettyType t <+> 
                                hsep (map (\x -> text "r" <> prettyType x) ts)
  prettyType' x = text "r" <> prettyType x
  -- embedding-projection pair
  epfn = epD <> text (name d) <+> equals <+> text "EP from to"
  -- from function
  --mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
  fromfn = case (body d) of 
            -- Special case for datatypes with a single constructor (do not build a sum)
            [b] -> fromD <+> tolhs b <+> equals <+> con b
            _ -> vcat $ snd $ mapAccumL fromfn1 0 (body d)
  -- one line of from function
  fromfn1 n b = (succ n, fromD <+> tolhs b <+> equals <+> torhs n b)
  -- pattern matching on the left hand side
  tolhs b = parens (text (constructor b) <+> hsep (varNames (types b)))
  torhs 0 b = lD <+> parens (con b)
  torhs 1 b = rD <+> parens (con b)
  torhs n b = rD <+> parens (torhs (n-1) b)
  -- constructor at value level
  con b = prod (varNames (types b))
  prod [] = unitD
  prod [x] = x
  prod (x:t) = x <+> text ":*:" <+> parens (prod t)
  -- to function
  tofn = case body d of 
          -- Special case for datatypes with a single constructor (do not build a sum)
          [b] -> toD <+> parens (con b) <+> equals <+> tolhs b
          _   -> vcat $ snd $ mapAccumL tofn1 0 (body d)
  -- one line of to function
  tofn1 n b = (succ n, toD <+> parens (torhs n b) <+> equals <+> tolhs b)
  -- ConInfo for each constructor
  confnName b = 
    if (isInfix (constructor b))
      -- TODO: Fix terrible idea below to deal with symbols in infix constructors
      then text "con" <> text (concat (map (show . ord) (constructor b)))
        else text "con" <> text (constructor b)
  conInfos = conInfoty (body d) $$ vcat (map conInfo (body d))
  conInfoty bs = list (map confnName bs) <+> text "::" <+> conDescrD
  conInfo b = confnName b <+> text "=" <+> conDescrD <+> text "{" $$
                (vsepComma $ sepWith comma $ map (nest 2) $ 
                  [conInfoName b, conInfoArity b, conInfoLabels b, conInfoFixity b])
                $$ nest 2 (text "}")
  conInfoName b = text "conName = \"" <> text (constructor b) <> text "\""
  conInfoArity b = text "conArity = " <> text (show (length (types b)))
  conInfoLabels b = text "conLabels = " <> text (show (labels b))
  conInfoFixity b = text "conFixity = " <> 
                      -- TODO: Precedence is not always 5
                      text (if isInfix (constructor b) then "Infix 5" else "Nonfix")

-- TODO: I'm afraid this is just a simplified approach to the issue.
isInfix :: Constructor -> Bool
isInfix ('(':_) = True
isInfix _ = False

opt f p = if isEmpty p then empty else f p
block = nest 2 . vcat
parenList = parens . hsepComma . sepWith comma
list = hsepComma . sepWith comma
hsepComma [] = empty
hsepComma [x] = x
hsepComma (h1:h2:t) = h1 <> h2 <+> hsepComma t
vsepComma [] = empty
vsepComma [x] = x
vsepComma (h1:h2:t) = h1 <> h2 $$ hsepComma t
emptyLine = text ""
funSep :: [Doc] -> Doc
funSep [] = empty
funSep x = (<+> text "->") $ hsep $ sepWith (text "->") x