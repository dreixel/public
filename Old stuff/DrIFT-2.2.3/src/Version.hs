module Version(package, version, fullName) where

package = "DrIFT"

version = "2.2.3"


fullName = package ++ "-" ++ version
