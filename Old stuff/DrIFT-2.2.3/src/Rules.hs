module Rules (rules) where

import qualified Rules.Arbitrary
import qualified Rules.Binary
import qualified Rules.BitsBinary
import qualified Rules.FunctorM
import qualified Rules.Generics
import qualified Rules.GhcBinary
import qualified Rules.Monoid
import qualified Rules.Standard
import qualified Rules.Utility
import qualified Rules.Xml
import qualified Rules.GenericReps

rules = concat [
         Rules.Arbitrary.rules,
         Rules.Binary.rules,
         Rules.BitsBinary.rules,
         Rules.FunctorM.rules,
         Rules.Generics.rules,
         Rules.GhcBinary.rules,
         Rules.Monoid.rules,
         Rules.Standard.rules,
         Rules.Utility.rules,
         Rules.Xml.rules,
         Rules.GenericReps.rules
        ]
