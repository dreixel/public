This directory contains various modules which are associated with deriving
rules. They may either be used as-is or some may require modification or have a
suitable replacement in the standard libraries already.
