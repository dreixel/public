{-# OPTIONS_GHC -fglasgow-exts #-}

module Main where

data ComplexF s = CF { _new :: (Double, Double) -> s,
                       _add :: ADT ComplexF -> s,
                       _rea :: Double,
                       _ima :: Double}

instance Functor ComplexF where
  --fmap :: (s -> s') -> ComplexF s -> ComplexF s'
  fmap f c = CF { _new = f . (_new c),
                  _add = f . (_add c),
                  _rea = _rea c,
                  _ima = _ima c}

data Functor f => ADT f = forall s. D (s -> f s) s

re :: ADT ComplexF -> Double
re (D f s) = _rea (f s)

im :: ADT ComplexF -> Double
im (D f s) = _ima (f s)

fc :: (Double, Double) -> ComplexF (Double, Double)
fc (x,y) = CF {_new = id, 
	       _add = \c -> (x + re c, y + im c), 
               _rea = x, 
               _ima = y}

zeroCG :: ADT ComplexF
zeroCG = D fc (0.0, 0.0)

main = print $ re zeroCG
{-
data Complex = forall s. C 
		(s -> (Double, Double) -> s)
		(s -> Complex -> s)
		(s -> Double)
		(s -> Double)
		s

new :: Complex -> Double -> Double -> Complex
new (C n a r i s) x y = C n a r i (n s (x,y))

add (C n a r i s) c = C n a r i (a s c)

re (C n r i s) = r s
im (C n a r i s) = i s

zeroC :: Complex
zeroC = C (\(x,y) z -> z)
          (\(x,y) c -> (x + re c, y + im c))
          (\(x,y) -> x)
          (\(x,y) -> y)
          (0.0, 0.0)

-}

{-
re :: Complex -> Double
re (C f s) = _rea (f s)

im :: Complex -> Double
im (C f s) = _ima (f s)

zeroC :: Complex
zeroC = C fc (0.0, 0.0)
-}

{-
data Functor f => Tree f = T {unT :: f (Tree f)}

tree :: Functor f => ADT f -> Tree f
tree (D h s) = unfold h s where
	unfold :: Functor f => (a -> f a) -> a -> Tree f
	unfold f x = T (fmap (unfold f) (f x))
-}
