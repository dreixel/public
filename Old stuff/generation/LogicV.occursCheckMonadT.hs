{-# OPTIONS  -Wall             #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies     #-}
{-# LANGUAGE TypeOperators    #-}

module Main where

import Generics.Regular.Rewriting
import Test.QuickCheck
import Prelude hiding (sum, max)
import qualified Data.IntMap as M
import Control.Monad.State

data Info = Info { freq :: Int, max :: Int }

class HasInfo (t :: * -> *) where
  type WithInfo t
  getInfo :: t a -> WithInfo t -> Info
  
instance (HasInfo f, HasInfo g) => HasInfo (f :+: g) where
  type WithInfo (f :+: g) = ((Info, Info), (WithInfo f, WithInfo g))
  getInfo (L f) (_, (i1, _)) = getInfo f i1
  getInfo (R f) (_, (_, i2)) = getInfo f i2

-- I want defaults for associated type synonyms...
instance HasInfo (Con f) where
  type WithInfo (Con f) = Info
  getInfo _ i = i

instance HasInfo (f :*: g) where
  type WithInfo (f :*: g) = Info
  getInfo _ i = i

instance HasInfo Unit where
  type WithInfo Unit = Info
  getInfo _ i = i

instance HasInfo (K a) where
  type WithInfo (K a) = Info
  getInfo _ i = i

instance HasInfo Id where
  type WithInfo Id = Info
  getInfo _ i = i


class Index f where
  index' :: Int -> f a -> Int
  index' n _ = n

instance (Index f, Index g) => Index (f :+: g) where
  index' n (L p) = index' n p
  index' n (R p) = index' (succ n) p

type MyState = M.IntMap Int

emptyState :: MyState
emptyState = M.empty


data ArbParams a = AP {
    rec     :: ArbParams a -> StateT MyState Gen a
  }

class GArbitrary f where
  garbitrary'   :: Either (WithInfo f) Info -> ArbParams a -> StateT MyState Gen (f a)

instance GArbitrary Unit where
  garbitrary' _ _ = return Unit

instance GArbitrary Id where
  garbitrary' _ p@(AP r) = fmap Id (r p)

instance (Arbitrary a) => GArbitrary (K a) where
  garbitrary' _ _ = lift $ fmap K arbitrary

instance (HasInfo f, HasInfo g, GArbitrary f, GArbitrary g) 
        => GArbitrary (f :+: g) where
  garbitrary' (Left ((a,b),(li,ri))) p =

    do
      l <- garbitrary' (Left li) p
      r <- garbitrary' (Left ri) p
      lift (frequency [(freq a, fmap L l), (freq b, fmap R r)])-- :: StateT MyState Gen ((f :+: g) a)

{-
    do
      let g lOrRi = garbitrary' (Left lOrRi) p
      let f lOrR  = getInfo (lOrR undefined) i
      let rec' lOrR lOrRi = g lOrRi >>= \x -> return (f lOrR, x)
      frequency [rec' L li, rec' R ri]
-}
{-
    lift $ frequency [rec' L li, rec' R ri]
        where rec' lOrR lOrRi = (freq (getInfo (lOrR undefined) i), 
                                 fmap lOrR (garbitrary' (Left lOrRi) p))
-}

  garbitrary' _ _ = error "Malformed Regular instance."

instance (GArbitrary f, GArbitrary g) => GArbitrary (f :*: g) where
  garbitrary' (Right i) p = do l <- garbitrary' (Right i) p
                               r <- garbitrary' (Right i) p
                               return (l :*: r)
  garbitrary' _ _ = error "Malformed Regular instance."

instance GArbitrary f => GArbitrary (Con f) where
  garbitrary' (Left i) p = fmap (Con "") (garbitrary' (Right i) p)
  garbitrary' _ _ = error "Malformed Regular instance."


garbitrary :: (GArbitrary (PF a), Regular a) => WithInfo (PF a) -> Gen a
garbitrary i = evalStateT (garbitraryHelper i (AP undefined)) emptyState

garbitraryHelper :: (Regular a, GArbitrary (PF a)) 
                 => WithInfo (PF a) -> ArbParams a -> StateT MyState Gen a
garbitraryHelper i p = fmap to $ garbitrary' (Left i) p {rec = garbitraryHelper i}


-------------------------------------------------------------------------------

-- | The data type Logic is the abstract syntax for the domain
--   of logic expressions.
data Logic = Nor Logic Logic -- Nor
           | T               -- true
           | F               -- false
  deriving (Show, Eq, Ord)
{-
not :: Logic -> Logic
not p = p `Nor` p  

infixr 1 :<->:
infixr 2 :->: 
infixr 3 :||: 
infixr 4 :&&:

(:<->:), (:->:), (:||:), (:&&:) :: Logic -> Logic -> Logic
p :&&:  q = (not p) `Nor` (not q)
p :||:  q = not (p `Nor` q)
p :->:  q = (not p) :||: q
p :<->: q = p :->: q :&&: q :->: p
-}

instance Regular Logic where
  type PF Logic = (Con (Id :*: Id)) :+: (Con Unit :+: Con Unit)

  from (p `Nor` q) = L (Con "Nor" (Id p :*: Id q))
  from T           = R (L (Con "T" Unit))
  from F           = R (R (Con "F" Unit))

  to (L (Con _ (Id p :*: Id q))) = p `Nor` q
  to (R (L (Con _ Unit)))        = T
  to (R (R (Con _ Unit)))        = F

logicInfo :: WithInfo (PF Logic)
logicInfo = ((i1,i2),(i1,((i2,i3),(i2,i3)))) where
  i1 = Info {freq = 2, max = 3} 
  i2 = Info {freq = 3, max = 10}
  i3 = Info {freq = 3, max = 10}

test :: IO ()
test = undefined--sample $ garbitrary logicInfo

instance Regular [a] where
  type PF [a] = Con Unit :+: Con ((K a) :*: Id)

  from []       = L (Con "[]" Unit)
  from (x : xs) = R (Con "(:)" ((K x) :*: (Id xs)))

  to (L (Con _ Unit))               = []
  to (R (Con _ ((K x) :*: (Id r)))) = x : r
  
listInfo :: WithInfo (PF [Int])
listInfo = ((i1,i2),(i1,i2)) where
  i1 = Info {freq = 1, max = undefined}
  i2 = Info {freq = 2, max = undefined}

testList :: IO ()
testList = undefined--sample $ garbitrary listInfo

--getInfo :: WithInfo (PF a) -> [Info]
--getInfo (li, ri) = getInfo li ++ getInfo ri
--getInfo i = [i]
     
--type LogicV = SchemeOf Logic


{-
instance Regular (Unit r) where
  type PF (Unit r) = Unit
  from Unit = Unit
  to Unit = Unit
  
instance Regular (Id r) where
  type PF (Id r) = Id
  from (Id x) = Id (Id x)
  to (Id (Id x)) = Id x
  
instance (Functor f) => Regular (Con f r) where
  type PF (Con f r) = Con f
  --from (Con s f) = Con s f
  --to (Con s (Con s' f)) = Con s f
  
instance (Functor f, Functor g) => Regular ((f :+: g) r) where
  type PF ((f :+: g) r) = f :+: g
  from (L f) = L f
-}  
{-
instance (Functor f) => Regular (Con f r) where
  type PF (Con f r) = Con f
  from (Con s f) = Con s f
  --to Unit = Unit
-}
