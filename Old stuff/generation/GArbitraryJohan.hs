{-# LANGUAGE RankNTypes
           , FlexibleContexts
           , FlexibleInstances
           , TypeOperators
           , MultiParamTypeClasses
           , GADTs
           , KindSignatures
           , TypeFamilies
           , ScopedTypeVariables
           , EmptyDataDecls 
           #-}
module Generics.MultiRec.GArbitrary where

import Generics.MultiRec
import Test.QuickCheck

{- 
-------------------------------------------------------------------------------
-- Original Gen (by Erik). Doesn't work because Eq_ and eq_ are not exported --
-- but is part of Erik's lib.                                                --
-------------------------------------------------------------------------------

class HGen f s where -- Het 's' argument is nodig voor de context van de instance voor 'I'
    hgen :: Int -> s ix -> (forall ix. Ix s ix => s ix -> [r ix]) -> [f s r ix]

instance (Ix s xi) => HGen (I xi) s where
    hgen n ix f = map I $ f index

instance HGen (K Char) s where
    hgen 0 _ _ = []
    hgen n ix f = [K '0']

instance HGen (K [a]) s where
    hgen 0 _ _ = []
    hgen n ix f = [K []]

instance HGen (K ()) s where
    hgen n ix f = [K ()]

instance HGen (K Float) s where
    hgen 0 _ _ = []
    hgen n ix f = [K 0]

instance HGen (K Int) s where
    hgen 0 _ _ = []
    hgen n ix f = [K 0]

instance (HGen f s, HGen g s) => HGen (f :+: g) s where
    hgen n ix f = map L (hgen n ix f) ++ map R (hgen n ix f)

instance (HGen f s, HGen g s) => HGen (f :*: g) s where
    hgen n ix f = [ x :*: y | x <- hgen n ix f, y <- hgen n ix f ]

instance (Eq_ s, Ix s t, HGen f s) => HGen (f :>: t) s where
    hgen n ix f = case eq_ ix (index :: s t) of
        Nothing -> []
        Just Refl -> map Tag (hgen n ix f)

gen :: (Ix s ix, HGen (PF s) s) => Int -> s ix -> [ix]
gen 0 _  = []
gen n ix = map to $ hgen (n - 1) ix (\ix -> map I0 (gen (n - 1) ix))
-}

-------------------------------------------------------------------------------
-- Gen adapted to return a Gen instead of a list.                            --
-- Works, but I sometimes get pick of an empty list (which doesn't surprise  --
-- me, since I am generating a lot of empty lists).                          --
-------------------------------------------------------------------------------


class HGen f s where 
    hgen :: Int -> s ix -> (forall ix. Ix s ix => s ix -> Gen (r ix)) -> Gen (f s r ix)

instance (Ix s xi) => HGen (I xi) s where
    hgen n ix f = fmap I $ f index

instance (HGen f s,Constructor c) => HGen (C c f) s where
    hgen 0 _ _ = elements [] 
    hgen n ix f = do x <- hgen n ix f
                     return (C x)

instance HGen (K Char) s where
    hgen 0 _ _ = elements [] 
    hgen n ix f = return (K '0')

instance HGen (K [a]) s where
    hgen 0 _ _ = elements [] 
    hgen n ix f = return (K [])

instance HGen (K ()) s where
    hgen n ix f = return (K ())

instance HGen (K Float) s where
    hgen 0 _ _ = elements [] 
    hgen n ix f = return (K 0)

instance HGen (K Int) s where
    hgen 0 _ _ = elements [] 
    hgen n ix f = return (K 0)

instance (HGen f s, HGen g s) => HGen (f :+: g) s where
    hgen n ix f = frequency [(n `div` 2,fmap L (hgen n ix f)) 
                            ,(n `div` 2,fmap R (hgen n ix f))
                            ]

instance (HGen f s, HGen g s) => HGen (f :*: g) s where
    hgen n ix f = do x <- hgen n ix f
                     y <- hgen n ix f 
                     return (x :*: y)

{-
-- Not needed for a single type
instance (Eq_ s, Ix s t, HGen f s) => HGen (f :>: t) s where
    hgen n ix f = case eq_ ix (index :: s t) of
        Nothing -> return undefined
        Just Refl -> fmap Tag (hgen n ix f)
-}

gen :: (Ix s ix, HGen (PF s) s) => Int -> s ix -> Gen ix
gen 0 _  = elements [] 
gen n ix = fmap to $ hgen (n - 1) ix (\ix -> fmap I0 (gen (n - 1) ix))

genLogic :: Gen Logic
genLogic = gen 10 Logic

{-
class ConstructorNames f s where 
    gconstructorNames ::  s ix -> (forall ix. Ix s ix => f s r ix ->[ String ]) -> [String] -- [ (f s r ix) ]

instance (Constructor c) => ConstructorNames (C c f) s where
    gconstructorNames ix f = undefined -- fmap C (gconstructorNames ix f)

class ConstructorNames f s where
  gconstructorNames ::  (forall ix. Ix s ix => s ix -> [String]) -> f s r ix -> [String]

instance (Constructor c) => ConstructorNames (C c f) s where
  gconstructorNames _ _ = undefined -- conName (C undefined)

constructorNames :: Ix s ix => s ix -> [String]
constructorNames _ = undefined

constructorNamesLogic = constructorNames Logic
-}
{-
-------------------------------------------------------------------------------
-- Adapted the above to use a HasInfo instead of an Int.                     --
-------------------------------------------------------------------------------
     
data Info = Info { freq :: Int, max :: Int }

type family HasInfo (f :: (* -> *) -> (* -> *) -> * -> *) (s :: * -> *)
  ::    (* -> *)  -- r
     -> *         -- ix
     -> *
     
type instance HasInfo (f :+: g) s = HasInfoS f g s
type HasInfoS (f :: (* -> *) -> (* -> *) -> * -> *) 
              (g :: (* -> *) -> (* -> *) -> * -> *)  
              (s :: * -> *) (r :: * -> *) ix = Info

type instance HasInfo (f :*: g) s = HasInfoP f g s
type HasInfoP (f :: (* -> *) -> (* -> *) -> * -> *) 
              (g :: (* -> *) -> (* -> *) -> * -> *)  
              (s :: * -> *) (r :: * -> *) ix = Info

type instance HasInfo (C c f) s = HasInfoC f s
type HasInfoC (f :: (* -> *) -> (* -> *) -> * -> *) (s :: * -> *) (r :: * -> *) ix = Info 

class HArbitrary f s where
  harbitrary  ::  HasInfo f s r ix
              ->  s ix 
              ->  (forall ix. Ix s ix => s ix -> Gen (r ix)) 
              ->  Gen (f s r ix)

instance HArbitrary (K ()) s where
    harbitrary n ix f = return (K ())

instance (Ix s xi) => HArbitrary (I xi) s where
    harbitrary n ix f = fmap I $ f index

instance (HGen f s,Constructor c) => HArbitrary (C c f) s where
    harbitrary i ix f = do x <- hgen  (freq i) ix f
                           return (C x)

class AcumFreq (f :: (* -> *) -> (* -> *) -> * -> *) s where 
  getAcumFreq :: HasInfo f s r ix -> Int

{-
instance (AcumFreq f s,HArbitrary f s, AcumFreq g s,HArbitrary g s) => HArbitrary (f :+: g) s where
    harbitrary t ix f = frequency 
                             [(getAcumFreq t,fmap L (harbitrary l ix f)) 
                             ,(getAcumFreq t,fmap R (harbitrary r ix f))
                             ]

-}

-- doesn't work
instance (HasInfo f s ~ Info,HArbitrary f s, HArbitrary g s) => HArbitrary (f :*: g) s where
    harbitrary n ix f = do x <- harbitrary n ix f
                           y <- harbitrary n ix f 
                           return (x :*: y)
-}

-- | The data type Logic is the abstract syntax for the domain
--   of logic expressions.
data Logic = Nor Logic Logic -- Nor
           | T               -- true
           | F               -- false
  deriving (Show, Eq, Ord)

data AST :: * -> * where
  Logic :: AST Logic

data Nor
instance Constructor Nor  where conName _ = "Nor"
data T
instance Constructor T    where conName _ = "T"
data F
instance Constructor F    where conName _ = "F"

type instance PF AST = 
   (  C Nor (I Logic :*: I Logic) 
  :+: C T   (K ())                
  :+: C F   (K ())                
   )
  
instance Ix AST Logic where

  from_ (Nor l r)  = (L     (C (I (I0 l) :*: I (I0 r))))
  from_ T          = (R (L  (C (K ()))))
  from_ F          = (R (R  (C (K ()))))

  to_ ((L    (C (I (I0 l) :*: I (I0 r)))))   = (Nor l r)
  to_ ((R (L (C (K ())))))                   = T
  to_ ((R (R (C (K ())))))                   = F
  
  index = Logic

instance Eq_ AST where
  eq_ l r = undefined

-- Stuff copied from Erik's Base

data (:=:) :: * -> * -> * where
    Refl :: a :=: a

class Eq_ s where
  eq_ :: s ix -> s ix' -> Maybe (ix :=: ix')
