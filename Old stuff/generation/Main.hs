{-# OPTIONS -fglasgow-exts #-}

module Main where

import GArbitrary
import Generics.Regular.Rewriting
import Test.QuickCheck

instance Regular [a] where
  type PF [a] = Unit :+: (K a) :*: Id

  from []       = L Unit
  from (x : xs) = R ((K x) :*: (Id xs))

  to (L Unit)               = []
  to (R ((K x) :*: (Id r))) = x : r

-- Tree data type definition.

data Tree a = Leaf | Branch a (Tree a) (Tree a)
  deriving (Show, Eq, Ord)

-- Regular instance for the tree data type.

instance Regular (Tree a) where
  type PF (Tree a) = Unit :+: (K a) :*: Id :*: Id

  to (L Unit)                          = Leaf
  to (R ((K a) :*: (Id l) :*: (Id r))) = Branch a l r

  from Leaf           = L Unit
  from (Branch a l r) = R $ (K a) :*: (Id l) :*: (Id r)
  
-- | The data type Logic is the abstract syntax for the domain
-- | of logic expressions.
infixr 1 :<->:
infixr 2 :->: 
infixr 3 :||: 
infixr 4 :&&:

data Logic = Var String
           | Logic :->:  Logic            -- implication
           | Logic :<->: Logic            -- equivalence
           | Logic :&&:  Logic            -- and (conjunction)
           | Logic :||:  Logic            -- or (disjunction)
           | Not Logic                    -- not
           | T                            -- true
           | F                            -- false
  deriving (Show, Eq, Ord)


instance Regular Logic where
  type PF Logic =
    (((K String) :+: Id :*: Id) :+: (Id :*: Id :+: Id :*: Id))
    :+:
    ((Id :*: Id :+: Id) :+: (Unit :+: Unit))

  from (Var x)     = L (L (L (K x)))
  from (p :<->: q) = L (L (R ((Id p) :*: (Id q))))
  from (p :->: q)  = L (R (L ((Id p) :*: (Id q))))
  from (p :&&: q)  = L (R (R ((Id p) :*: (Id q))))
  from (p :||: q)  = R (L (L ((Id p) :*: (Id q))))
  from (Not p)     = R (L (R (Id p)))
  from T           = R (R (L Unit))
  from F           = R (R (R Unit))

  to (L (L (L (K x))))               = Var x
  to (L (L (R ((Id p) :*: (Id q))))) = p :<->: q
  to (L (R (L ((Id p) :*: (Id q))))) = p :->: q
  to (L (R (R ((Id p) :*: (Id q))))) = p :&&: q
  to (R (L (L ((Id p) :*: (Id q))))) = p :||: q
  to (R (L (R (Id p))))              = Not p
  to (R (R (L Unit)))                = T
  to (R (R (R Unit)))                = F