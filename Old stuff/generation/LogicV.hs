{-# OPTIONS -Wall                 #-}
{-# LANGUAGE EmptyDataDecls       #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Main where

import Generics.Regular.Rewriting
import Test.QuickCheck
import Data.List (sort, group)
import qualified Data.IntMap as M
import Prelude hiding (sum, max, lookup)
--import Types.Data.Num hiding ((:+:), (:*:))

-------------------------------------------------------------------------------
-- Extensions to Base (not used)
data ZeroS a -- Zero for :+:
data ZeroP a -- Zero for :*:

-- Type level peano naturals
data Succ a
data Zero

-- Useful synonyms
type D0 = Zero
type D1 = Succ D0
type D2 = Succ D1
type D3 = Succ D2
type D4 = Succ D3
type D5 = Succ D4
type D6 = Succ D5
type D7 = Succ D6

-- Value-level counterparts
d0 :: D0
d0 = undefined
d1 :: D1
d1 = undefined
d2 :: D2
d2 = undefined
d3 :: D3
d3 = undefined
d4 :: D4
d4 = undefined
d5 :: D5
d5 = undefined
d6 :: D6
d6 = undefined
d7 :: D7
d7 = undefined

class ToInt n where
  toInt :: n -> Int

instance ToInt D0 where toInt _ = 0
instance ToInt D1 where toInt _ = 1
instance ToInt D2 where toInt _ = 2
instance ToInt D3 where toInt _ = 3
instance ToInt D4 where toInt _ = 4
instance ToInt D5 where toInt _ = 5
instance ToInt D6 where toInt _ = 6
instance ToInt D7 where toInt _ = 7

-- Addition
type family   Add a        b
type instance Add Zero     n = n
type instance Add (Succ m) n = Succ (Add m n)

add :: a -> b -> Add a b
add _ _ = undefined


class Nat a
instance Nat D0
instance (Nat n) => Nat (Succ n)

-- Vectors
data Nat n => Vector n a = Vector { toList :: [a] }

nilV :: Vector D0 a
nilV = Vector []

consV :: a -> Vector n a -> Vector (Succ n) a
consV a (Vector l) = Vector (a : l)

infixr 5 <:
(<:) :: a -> Vector n a -> Vector (Succ n) a
(<:) = consV

headV :: Vector (Succ n) a -> a
headV (Vector (h:_)) = h
headV _              = error "Doesn't happen."

tailV :: Vector (Succ n) a -> Vector n a
tailV (Vector (_:t)) = Vector t
tailV _              = error "Doesn't happen."

{- Agda:
data Fin : N -> Set where
  zero : {n : N} -> Fin (suc n)
  suc  : {n : N} (i : Fin n) -> Fin (suc n)
-}

-- Fin n has n inhabitants (plus bottoms)
data Fin n where
  FZero :: Nat n => Fin (Succ n)
  FSucc :: Nat n => Fin n -> Fin (Succ n)

{-f0 :: Fin D1
f0 = FZero
f1 :: Fin D2
f1 = FSucc f0
f2 :: Fin D3
f2 = FSucc f1
f3 :: Fin D4
f3 = FSucc f2
f4 :: Fin D5
f4 = FSucc f3
f5 :: Fin D6
f5 = FSucc f4
f6 :: Fin D7
f6 = FSucc f5
-}
{- ?
toFin :: Nat n => n -> Fin n
toFin n = foldr ($) FZero (replicate (toInt n) FSucc)
-}
{-
instance ToInt (Fin n) where
  toInt FZero      = 0
  toInt (FSucc fn) = 1 + toInt fn

lookupV' :: ToInt n => Vector m a -> n -> Maybe a
lookupV' (Vector l) n = if (toInt n >= length l) 
                       then Nothing
                       else Just (l !! (toInt n))

lookupV :: ToInt (Fin n) => Vector n a -> Fin n -> Maybe a
lookupV (Vector l) fn = if (toInt fn >= length l) 
                        then Nothing 
                        else Just (l !! (toInt fn))

testVec0 :: Vector D0 Int
testVec0 = nilV

testVec1 :: Vector D1 Int
testVec1 = Vector [1]

testVec2 :: Vector D2 Int
testVec2 = Vector [2,7]
-}
-------------------------------------------------------------------------------

data Info = Info { freq :: Int, max :: Int } deriving Show

-- Number of constructors in a datatype
type family   Length (t :: * -> *)
type instance Length (f :+: g)     = Succ (Length g)
type instance Length (Con f)       = Succ D0

lengthT :: t a -> Length t
lengthT _ = undefined

-- Lookup a constructor by number (unused)
type family   Lookup a (t :: * -> *) :: * -> *
type instance Lookup D0       (f :+: g) = f
type instance Lookup (Succ n) (f :+: g) = Lookup n g

lookupT :: n -> t a -> (Lookup n t) a
lookupT _ _ = undefined

type InfoVec t = Vector (Length t) Info

-- WithInfo type-indexed datatype
class HasInfo (t :: * -> *) where
  data WithInfo t
  createInfo :: InfoVec t -> WithInfo t
  createInfo _  = error "Malformed Regular instance (createInfo default)."
  
  index :: t a -> Int
  index _ = 0

instance (HasInfo f, HasInfo g) => HasInfo (f :+: g) where
  data WithInfo (f :+: g) = Rec Info Info (WithInfo g)
  -- TODO: Check that each (user-supplied) freq is > 0
  --createInfo (h:t) = Rec i1 i2 (createInfo t) where
  createInfo v = Rec i1 i2 (createInfo t) where
                   h = headV v
                   t = tailV v
                   i1 = h
                   i2 = Info {freq = freqs (toList t), max = undefined}
                   
                   freqs :: [Info] -> Int
                   freqs []     = 0
                   freqs (i:is) = freq i + freqs is
  
  index (L _) = 0
  index (R t) = 1 + index t

instance HasInfo (Con f) where
  data WithInfo (Con f)
  
instance HasInfo (f :*: g) where
  data WithInfo (f :*: g)

instance HasInfo Unit where
  data WithInfo Unit

instance HasInfo (K a) where
  data WithInfo (K a)

instance HasInfo Id where
  data WithInfo Id

-- We need this to avoid undecidable instances
data ArbParams a = AP {
    rec     :: ArbParams a -> Gen a
  }

-- Main function for garbitrary
class GArbitrary f where
  garbitrary'   :: Maybe (WithInfo f) -> M.IntMap Int -> ArbParams a -> Gen (f a)

instance GArbitrary Unit where
  garbitrary' _ _ _ = return Unit

instance GArbitrary Id where
  garbitrary' _ _ p@(AP r) = fmap Id (r p)

instance (Arbitrary a, CoArbitrary a) => GArbitrary (K a) where
  garbitrary' _ _ _ = fmap K arbitrary

-- Problematic case
instance (HasInfo f, HasInfo g, GArbitrary f, GArbitrary g) 
        => GArbitrary (f :+: g) where
  garbitrary' (Just (Rec a b ri)) m p = 

    let l = fmap L (garbitrary' Nothing m p)
        r = fmap R (garbitrary' (Just ri) m p)
        --cl = m M.! (index l)

    in frequency [(freq a, l), (freq b, r)]
        
  garbitrary' _ _ _ = error "Malformed Regular instance."

instance (GArbitrary f, GArbitrary g) => GArbitrary (f :*: g) where
  garbitrary' _ m p = do l <- garbitrary' Nothing m p
                         r <- garbitrary' Nothing m p
                         return (l :*: r)

instance GArbitrary f => GArbitrary (Con f) where
  garbitrary' _ m p = fmap (Con "") (garbitrary' Nothing m p)


-- User exposed function and helper
garbitrary :: (GArbitrary (PF a), Regular a) => WithInfo (PF a) -> Gen a
garbitrary i = garbitraryHelper i (AP undefined)

garbitraryHelper :: (Regular a, GArbitrary (PF a)) 
                 => WithInfo (PF a) -> ArbParams a -> Gen a
garbitraryHelper i p = fmap to 
                        (garbitrary' (Just i) initMap p {rec = garbitraryHelper i})
  where initMap = M.fromList [(n,0) | n <- [0..10]] 
  -- not pretty, but works until a function which counts the number 
  -- of constructors exists.


-------------------------------------------------------------------------------
-- | The data type Logic is the abstract syntax for the domain
--   of logic expressions.
data Logic = Nor Logic Logic -- Nor
           | T               -- true
           | F               -- false
  deriving (Show, Eq, Ord)


instance Regular Logic where
  type PF Logic = (Con (Id :*: Id)) :+: (Con Unit :+: Con Unit)

  from (p `Nor` q) = L (Con "Nor" (Id p :*: Id q))
  from T           = R (L (Con "T" Unit))
  from F           = R (R (Con "F" Unit))

  to (L (Con _ (Id p :*: Id q))) = p `Nor` q
  to (R (L (Con _ Unit)))        = T
  to (R (R (Con _ Unit)))        = F

logicInfo' :: WithInfo (PF Logic)
logicInfo' = Rec i1 i2m (Rec i2 i3 undefined) where
  i1  = Info {freq = 1, max = 3}
  i2m = Info {freq = 2, max = 10}
  i2  = Info {freq = 1, max = 10}
  i3  = Info {freq = 1, max = 10}

logicInfo :: WithInfo (PF Logic)
logicInfo = createInfo (i1 <: i2 <: i3 <: nilV) where
  i1  = Info {freq = 3, max = 3}
  i2  = Info {freq = 2, max = 99}
  i3  = Info {freq = 2, max = 99}

testLogic :: IO ()
testLogic = sample $ (garbitrary logicInfo :: Gen Logic)

-- Lists
instance Regular [a] where
  type PF [a] = Con Unit :+: Con ((K a) :*: Id)

  from []       = L (Con "[]" Unit)
  from (x : xs) = R (Con "(:)" ((K x) :*: (Id xs)))

  to (L (Con _ Unit))               = []
  to (R (Con _ ((K x) :*: (Id r)))) = x : r

listInfo' :: WithInfo (PF [Int])
listInfo' = Rec i1 i2 undefined where
  i1  = Info {freq = 1, max = 10}
  i2  = Info {freq = 1, max = 99}

listInfo :: WithInfo (PF [Int])
listInfo = createInfo (i1 <: i2 <: nilV) where
  i1  = Info {freq = 1, max = 1}
  i2  = Info {freq = 5, max = 10}
  
testList :: IO ()
testList = sample $ (garbitrary listInfo :: Gen [Int])


-- Alternative datatype
data Choice = A | B | C | D deriving (Show, Eq, Ord)

instance Regular Choice where
  type PF Choice = Con Unit :+: Con Unit :+: Con Unit :+: Con Unit

  from A = L $         Con "A" Unit
  from B = R $ L $     Con "B" Unit
  from C = R $ R $ L $ Con "C" Unit
  from D = R $ R $ R $ Con "D" Unit

  to (L       (Con _ Unit))   = A
  to (R (L    (Con _ Unit)))  = B
  to (R (R (L (Con _ Unit)))) = C
  to (R (R (R (Con _ Unit)))) = D
{-
-- Old way
choiceInfo' :: WithInfo (PF Choice)
choiceInfo' = Rec i1 i2m (Rec i2 i3m (Rec i3 i4 undefined)) where
  i1  = Info {freq = 1, max = 1}
  i2m = Info {freq = 3, max = 1}
  i2  = Info {freq = 1, max = 1}
  i3m = Info {freq = 2, max = 1}
  i3  = Info {freq = 1, max = 1}
  i4  = Info {freq = 1, max = 1}
-}

-- Better way
choiceInfo :: WithInfo (PF Choice)
choiceInfo = createInfo (i1 <: i2 <: i3 <: i4 <: nilV) where
  i1  = Info {freq = 1, max = 1}
  i2  = Info {freq = 3, max = 1}
  i3  = Info {freq = 4, max = 1}
  i4  = Info {freq = 2, max = 1}

testChoice :: IO [(Choice, Int)]
testChoice = fmap (\[a,b,c,d] -> [(A,a),(B,b),(C,c),(D,d)]) .
             fmap (map length . group . sort . concat) .
             sequence . take 909 . repeat $
             sample' (garbitrary choiceInfo :: Gen Choice)
