{-# OPTIONS 
    -Wall 
    -XTypeFamilies
    -XDeriveDataTypeable
    -XEmptyDataDecls
  #-}
module SP where

import Data.Generics hiding (toConstr, dataTypeOf, Fixity, Prefix, Infix, Unit, Inl, Inr)
import qualified Data.Generics as G

---------------------------------
-- Structure types
---------------------------------

-- For SP view
data Zero deriving Typeable
data Unit        = Unit deriving (Data, Typeable)
data Sum aT bT   = Inl aT | Inr bT deriving (Data, Typeable)
data Prod aT bT  = Prod aT bT deriving (Data, Typeable)
data Con cT      = Con ConInfo cT deriving (Data, Typeable)

data ConInfo = ConInfo {
    name :: String,
    arity :: Int,
    fixity :: Fixity,
    associativity :: Assoc
  } deriving (Data, Typeable)
  
data Fixity = Prefix | Infix | InfixR | InfixL | InfixA deriving (Data, Typeable)

data Assoc = AssocNone | AssocLeft | AssocRight deriving (Data, Typeable)

instance Show ConInfo where 
  show = name

---------------------------------
-- Sum of products view
---------------------------------

class Data a => SPView a where
  type SP a
  from :: SP a -> a
  to   :: a -> SP a
  toConstr :: a -> Constr
  dataTypeOf :: a -> DataType
  -- defaults
  {-
  type SP a = a -- type synonyms are not yet implemented!
  from = id
  to = id
  -}
  toConstr = G.toConstr
  dataTypeOf = G.dataTypeOf

---------------------------------
-- Instances on SPView
---------------------------------
  
instance SPView Int where 
  type SP Int = Int
  from = id
  to = id
  
instance SPView Char where 
  type SP Char = Char
  from = id
  to = id
  
instance SPView Bool where 
  type SP Bool = Bool
  from = id
  to = id
  
instance SPView Unit where 
  type SP Unit = Unit
  from = id
  to = id

instance (Data a, Data b, SPView a, SPView b) => SPView (Prod a b) where 
  type SP (Prod a b) = Prod a b
  from = id
  to = id

instance (Data a, Data b, SPView a, SPView b) => SPView (Sum a b) where 
  type SP (Sum a b) = Sum a b
  from = id
  to = id
  
instance (Data a, SPView a) => SPView (Con a) where 
  type SP (Con a) = Con a
  from = id
  to = id
  
-- Lists
instance Data a => SPView [a] where
  type SP [a] = Sum (Con Unit) (Con (Prod a [a]))
  to []     = Inl (Con conEmpty Unit)
  to (a:as) = Inr (Con conCons (Prod a as))
  from (Inl (Con _ Unit))        = []
  from (Inr (Con _ (Prod a as))) = (a:as)
conEmpty, conCons :: ConInfo
conEmpty = ConInfo {
  name = "[]",
  arity = 0,
  fixity = Prefix,
  associativity = AssocNone }
conCons  = ConInfo {
  name = "(:)",
  arity = 2,
  fixity = Prefix,
  associativity = AssocNone }

-- Maybe
instance Data a => SPView (Maybe a) where
  type SP (Maybe a) = Sum (Con Unit) (Con a)
  to Nothing = Inl (Con conNothing Unit)
  to (Just x) = Inr (Con conJust x)
  from (Inl (Con _ Unit)) = Nothing
  from (Inr (Con _ x)) = Just x
conNothing, conJust :: ConInfo
conNothing = ConInfo {
  name = "Nothing",
  arity = 0,
  fixity = Prefix,
  associativity = AssocNone }
conJust = ConInfo {
  name = "Just",
  arity = 1,
  fixity = Prefix,
  associativity = AssocNone }

-- Pairs
instance (Data a, Data b) => SPView (a,b) where
  type SP (a,b) = Con (Prod a b)
  to (a,b) = Con conPair (Prod a b)
  from (Con _ (Prod a b)) = (a,b)
conPair :: ConInfo
conPair = ConInfo {
  name = "(,)",
  arity = 2,
  fixity = Prefix,
  associativity = AssocNone }