{-# OPTIONS 
 -fglasgow-exts 
 -fallow-overlapping-instances #-}

module SubSelects where

import Data.Generics hiding (gshow)
import Data.Dynamic
import Prelude hiding (showList)
import Data.Maybe (fromJust, mapMaybe, catMaybes)
import Data.List (intersperse, unfoldr, (\\), minimumBy, maximumBy)
import qualified Data.Map as M

import Control.Monad.State
import Control.Monad.Writer

import Datatypes

-- gshow has to be redefined because we want some specific cases
gshow :: Data a => a -> String
gshow = ( \t ->
                "("
             ++ showConstr (toConstr t)
             ++ concat (gmapQ ((++) " " . gshow) t)
             ++ ")"
        ) 
        `ext1Q` showList
        `ext2Q` showPair
        `extQ` (show :: String -> String)
        `extQ` (show :: Char -> String) -- gshow doesn't print quotes

newtype Q r a = Q { unR :: a -> r }
ext2Q :: (Data d, Typeable2 t) => 
  (d -> q) -> (forall d1 d2. (Data d1, Data d2) => t d1 d2 -> q) -> d -> q
ext2Q def ext arg = 
  case dataCast2 (Q ext) of
    Just (Q ext') -> ext' arg
    Nothing       -> def arg

showList :: Data a => [a] -> String
showList l = "[" ++ concat (intersperse "," (map gshow l)) ++ "]"

showPair :: (Data a, Data b) => (a, b) -> String
showPair (a,b) = "(" ++ gshow a ++ "," ++ gshow b ++ ")"

-- Valid selections generator

type Selections = M.Map (Int, Int) Dynamic
type MyState = StateT Int (Writer Selections) ()

selections :: Data a => a -> Selections
selections t = execWriter (evalStateT (sels' t) 0) where
  sels' :: Data a => a -> MyState
  sels' t = do
              n <- get
              let m = n + length (gshow t)
              tell (M.singleton (n, m) (toDyn t))
              (selsConstr `ext2Q` selsPair `ext1Q` selsList `extQ` selsString) t
              put m
  
  selsConstr :: Data a => a -> MyState
  selsConstr t = do
                  when (nrChildren t > 0) $ 
                      modify (+ (2 + length (showConstr (toConstr t))))
                  sequence_ $ intersperse (modify (+1)) $ gmapQ sels' t

  selsPair :: (Data a, Data b) => (a,b) -> MyState
  selsPair (a,b) = do
                    modify (+1)
                    sels' a
                    modify (+1)
                    sels' b

  selsString :: String -> MyState  
  selsString t = return ()
  
  -- does not allow for ranges inside lists
  selsList :: Data a => [a] -> MyState
  selsList l = do
                modify (+1)
                sequence_ $ intersperse (modify (+1)) $ map sels' l

nrChildren :: Data a => a -> Int
nrChildren = gmapQr (+) 0 (const 1)


-- Testing

allValidSubs e = selections e

main t = do
  let term = gshow t
      subs = [(x, y, subList (x,y) term, d) | ((x,y),d) <- M.toList (allValidSubs t)]
  putStrLn (show (length subs) ++ " valid subselections:")
  sequence_ (map (\(a, b, c, d) -> putStrLn (show a ++ "--" ++ show b ++ ": " ++ c ++ " :: " ++ show d)) subs)

subList (lo, up) = drop lo . take up