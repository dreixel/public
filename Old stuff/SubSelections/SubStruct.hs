{-# OPTIONS -fglasgow-exts -fallow-overlapping-instances -fallow-undecidable-instances #-}

module Main where

import Data.Generics hiding ((:*:))
import Control.Monad
import Test.QuickCheck
import Datatypes

-- Implementation
isSubStruct :: (Data a, Data b) => a -> b -> Bool
isSubStruct a b = everything (||) (False `mkQ` (geq a)) b

-- Tests
main = do 
         print $ isSubStruct a1 a1 -- Substructure of itself
         print $ isSubStruct a2 a1 -- Substructure of simple datatype
         print $ isSubStruct b2 b1 -- Inner list is NOT substructure
         print $ isSubStruct b3 b1 -- ... but a tail IS
         print $ isSubStruct c2 c1 -- complete subtrees are substructure
         print $ isSubStruct e2 d1 -- Mutually-recursive datatypes are no problem
         print $ isSubStruct f4 f1 -- An example with rose trees
         print $ isSubStruct g2 g1 -- A type of kind * -> * -> * is no problem
         print $ isSubStruct g4 g1 -- Counter-example

-- Alternative (much worse) implementation
subStructs :: Data a => a -> [a]
subStructs x = x : foldr (\x y -> subStructs x ++ y) [] (children x)

children :: Data a => a -> [a]
children t = [c | Just c <- gmapQ cast t]

-- Notice the single variable in the type (isSubStruct has 2)
isSubStruct' :: (Data a) => a -> a -> Bool
isSubStruct' a b = any (geq a) (subStructs b)

instance Arbitrary Exp where
  arbitrary = sized exp'
    where 
    exp' 0 = liftM Const arbitrary
    exp' n | n > 0 = 
    oneof [liftM Const arbitrary,
             liftM2 (:+:) subExp subExp,
         liftM2 (:*:) subExp subExp,
         liftM2 (:/:) subExp subExp,
         liftM Neg (exp' (n - 1))]
        where subExp = exp' (n `div` 2)
  coarbitrary (Const n) = 
  variant 0 . coarbitrary n
  coarbitrary (t1 :+: t2) = 
  variant 1 . coarbitrary t1 . coarbitrary t2
  coarbitrary (t1 :*: t2) = 
  variant 2 . coarbitrary t1 . coarbitrary t2
  coarbitrary (t1 :/: t2) = 
  variant 3 . coarbitrary t1 . coarbitrary t2
  coarbitrary (Neg e) = 
  variant 4 . coarbitrary e

data ExpAssoc = 
    AssocP [ExpAssoc]
  | AssocT [ExpAssoc]
  | NADiv ExpAssoc ExpAssoc
  | NANeg ExpAssoc
  | NAConst Int deriving (Eq, Show, Data, Typeable)
{-
instance Arbitrary ExpAssoc where
  arbitrary = sized exp'
    where 
	  exp' 0 = liftM NAConst arbitrary
	  exp' n | n > 0 = 
		oneof [liftM NAConst arbitrary,
	           liftM AssocP subExp,
			   liftM AssocT subExp,
			   liftM2 NADiv subExp subExp,
			   liftM NANeg (exp' (n - 1))]
  	    where subExp = exp' (n `div` 2)
  coarbitrary (NAConst n) = 
	variant 0 . coarbitrary n
  coarbitrary (AssocP t) = 
	variant 1 . coarbitrary t
  coarbitrary (AssocT t) = 
	variant 2 . coarbitrary t
  coarbitrary (NADiv e1 e2) = 
	variant 3 . coarbitrary e1 . coarbitrary e2
  coarbitrary (NANeg e) = 
	variant 4 . coarbitrary e
-}

assocExp :: Exp -> ExpAssoc
assocExp = (everywhere (mkT concatExp)) . assocExp' . applyAssoc
  
assocExp' :: Exp -> ExpAssoc
assocExp' (e1 :+: e2) = 
  let
  e1' = assocExp' e1
  e2' = assocExp' e2
  in AssocP [e1', e2']
assocExp' (e1 :*: e2) =
  let
  e1' = assocExp' e1
  e2' = assocExp' e2
  in AssocT [e1', e2']
assocExp' (e1 :/: e2) = NADiv (assocExp' e1) (assocExp' e2)
assocExp' (Neg e) = NANeg (assocExp' e)
assocExp' (Const e) = NAConst e

concatExp :: ExpAssoc -> ExpAssoc
concatExp (AssocP x) = AssocP (everywhere (mkT concatExp') x)
  where
  concatExp' :: [ExpAssoc] -> [ExpAssoc]
  concatExp' ((AssocP x):_) = x
  concatExp' x = x
concatExp x = x

unassocExp :: ExpAssoc -> Exp
unassocExp (AssocP x) = foldr1 (:+:) (map unassocExp x)
unassocExp (AssocT x) = foldr1 (:*:) (map unassocExp x)
unassocExp (NADiv e1 e2) = (unassocExp e1) :/: (unassocExp e2)
unassocExp (NANeg e) = Neg (unassocExp e)
unassocExp (NAConst i) = Const i

propAssocUnassoc :: ExpAssoc -> Bool
propAssocUnassoc e = e == assocExp (unassocExp e)

propUnassocAssoc :: Exp -> Bool
propUnassocAssoc e = applyAssoc e == unassocExp (assocExp e)

replace :: (Data a, Data b) => a -> a -> b -> b
replace a b = everywhere (mkT (\x -> if (geq x a) then b else x))

assocLaw :: Exp -> Exp
assocLaw ((a :+: b) :+: c) = (a :+: (b :+: c))
assocLaw x = x

--applyLaw :: (Data a) => (Exp -> Exp) -> a -> a
applyLaw l = everywhere (mkT l)

repeatF :: (Data a) => (a -> a) -> a -> a
repeatF f t = if (f t `geq` t) then t else (repeatF f (f t))

applyAssoc :: (Data a) => a -> a
applyAssoc = repeatF (applyLaw assocLaw)
