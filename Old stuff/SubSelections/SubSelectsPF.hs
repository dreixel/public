{-# OPTIONS -Wall               #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE TypeFamilies       #-}
{-# LANGUAGE TypeOperators      #-}

module SubSelectsPF where

-- Generic library
import Generics.Regular.Rewriting

-- Standard modules
import qualified Data.Map as M

errorNever :: Int -> a
errorNever n = error ("The impossible happened: error code #" ++ show n)

-- Valid selections generator
type Range = (Int, Int)
type Selections a = M.Map Range a

-- Returns the range (lowest and highest indices) of these selections
getRange :: Selections a -> Range
getRange m = (fst $ fst $ M.findMin m, maximum $ map (\((_,x),_) -> x) $ M.toList m)

{-
-- Type-indexed datatype with additional information
class HasInfo f where
  type WithInfo f
  
instance HasInfo (f :+: g) where
  type WithInfo (f :+: g) = ?
-}

class SubSel' f where
  subsel' :: (Int -> a -> Selections a) -> Int -> f a -> Selections a

instance SubSel' Unit where
  subsel' _ _ Unit = M.empty
  
instance (SubSel' f, SubSel' g) => SubSel' (f :+: g) where
  subsel' f i (L x) = subsel' f i x
  subsel' f i (R x) = subsel' f i x
  
instance (SubSel' a, SubSel' b) => SubSel' (a :*: b) where
  subsel' f i ((:*:) l r) = 
    let ml = subsel' f i l
        ri = if M.null ml then errorNever 0 else 1 + snd (getRange ml)
        mr = subsel' f ri r
    in M.union ml mr
  
instance SubSel' a => SubSel' (Con a) where
  subsel' f i (Con c x) =
    let lCon = length c
        m = subsel' f (i + (2 + lCon)) x
        ri = 2 + if M.null m then (i + lCon) else snd (getRange m)
        own = M.singleton (i, ri) undefined
    in M.union own m

instance SubSel' (K a) where
  subsel' _ _ _ = M.empty

instance SubSel' Id where
  subsel' f i (Id r) = 
    let 
      m = f i r
      own = if M.null m then M.empty else M.singleton (i, snd (getRange m)) undefined
    in M.union m own


subsel :: (Regular a, SubSel' (PF a)) => Int -> a -> Selections a
subsel i = subsel' subsel i . from


-- Dispatcher class
{-
class (Regular a, SubSel' (PF a)) => SubSel a where
  subsel :: Int -> a -> Selections
  subsel i = subsel' subsel i . from
  
instance SubSel Unit where
  subsel = subsel' subsel
  
instance (SubSel (a), SubSel (b)) => SubSel ((:*:) a b) where
  subsel = subsel' subsel
  
instance (SubSel (a), SubSel (b)) => SubSel ((:+:) a b) where
  subsel = subsel' subsel
  
instance SubSel (a) => SubSel (Con a) where
  subsel = subsel' subsel
  
instance SubSel (K a) where
  subsel = subsel' subsel
  
instance SubSel Id where
  subsel = subsel' subsel
-}

{-
-- An adhoc instance for lists
instance (Typeable a, SubSel a) => SubSel [a] where
  subsel i x = 
    let
      -- To allow selecting the square brackets as well
      own = M.singleton (i, finalAcc + 1) (toDyn x)
      
      -- To allow selecting each individual element
      -- recall mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
      (finalAcc, y) = mapAccumL f (i + 1) x
      f acc z = (1 + snd (getRange (subsel acc z)), subsel acc z)
      
      -- To allow selecting any number of adjacent elements
      closure :: [Selections] -> [Selections]
      closure l | length l < 2 = l
                | otherwise = case l of 
                  (m:t) -> m : map (M.mapKeys (\(_,u) -> (fst (getRange m),u))) t ++ closure t
                  _ -> errorNever 4
    -- We return the union of everything
    in M.union own $ M.unions $ closure y

-- An adhoc instance for Strings
instance SubSel String where
  subsel i s = M.singleton (i, i + 2 + length s) (toDyn s)
  
-- An adhoc instance for pairs
instance (SubSel a, Typeable a, SubSel b, Typeable b) => SubSel (a,b) where
  subsel i (x,y) = 
    let own = M.singleton (i, i2 + 1) (toDyn (x,y))
        m1 = subsel (i+1) x
        i1 = if M.null m1 then errorNever 2 else 1 + snd (getRange m1)
        m2 = subsel i1 y
        i2 = if M.null m2 then errorNever 3 else 1 + snd (getRange m2)
    in M.union own (M.union m1 m2)
  
-- A generic instance for Maybe
instance (SubSel a, Typeable a) => SubSel (Maybe a)
-}

-- Testing
main :: (Show a, SubSel' (PF a), Regular a) => a -> IO ()
main t = do
  let term = show t
      subs = [(l, u, subList (l,u) term, val) | ((l,u),val) <- M.toAscList (subsel 0 t)]
  putStrLn (show (length subs) ++ " valid subselections:")
  sequence_ (map (\(a, b, c, _d) -> putStrLn (show a ++ "--" ++ show b ++ ": " ++ c ++ "\t :: " ++ show "temporarily disabled")) subs)

subList :: Range -> [a] -> [a]
subList (lo, up) = drop lo . take up

t1, t2, t3 :: IO ()
t1 = main (T)
t2 = main (Not F)
t3 = main (T :||: F)

infixr 1 :<->:
infixr 2 :->: 
infixr 3 :||: 
infixr 4 :&&:

data Logic = Var String
           | Logic :->:  Logic            -- implication
           | Logic :<->: Logic            -- equivalence
           | Logic :&&:  Logic            -- and (conjunction)
           | Logic :||:  Logic            -- or (disjunction)
           | Not Logic                    -- not
           | T                            -- true
           | F                            -- false
 deriving (Show, Eq, Ord)

instance Regular Logic where
  type PF Logic =
     (:+:) ((:+:) ((:+:) (Con (K String))   (Con ((:*:) Id Id)))
              ((:+:) (Con ((:*:) Id Id)) (Con ((:*:) Id Id))))
         ((:+:) ((:+:) (Con ((:*:) Id Id)) (Con Id))
              ((:+:) (Con Unit) (Con Unit)))

  from (Var x)     = L (L (L (Con "Var" (K x))))
  from (p :<->: q) = L (L (R (Con ":<->:" ((Id p) :*: (Id q)))))
  from (p :->: q)  = L (R (L (Con ":->:"  ((Id p) :*: (Id q)))))
  from (p :&&: q)  = L (R (R (Con ":&&:"  ((Id p) :*: (Id q)))))
  from (p :||: q)  = R (L (L (Con ":||:"  ((Id p) :*: (Id q)))))
  from (Not p)     = R (L (R (Con "Not" (Id p))))
  from T           = R (R (L (Con "T" Unit)))
  from F           = R (R (R (Con "F" Unit)))

  to (L (L (L (Con _ (K x))))) = Var x
  to (L (L (R (Con _ ((Id p) :*: (Id q)))))) = p :<->: q
  to (L (R (L (Con _ ((Id p) :*: (Id q)))))) = p :->: q
  to (L (R (R (Con _ ((Id p) :*: (Id q)))))) = p :&&: q
  to (R (L (L (Con _ ((Id p) :*: (Id q)))))) = p :||: q
  to (R (L (R (Con _ (Id p))))) = Not p
  to (R (R (L (Con _ Unit)))) = T
  to (R (R (R (Con _ Unit)))) = F