{-# OPTIONS 
    -fglasgow-exts
    -fallow-undecidable-instances
    -fallow-overlapping-instances
  #-}

-- | Subselections in EMGM
module SubSelectsEMGM where

-- Generic library
import Generics.EMGM

-- Standard modules
import Prelude hiding (showList)
import Data.Dynamic
import Data.Generics (Data, showConstr, gmapQ, extQ, ext1Q, toConstr, dataCast2)
import qualified Data.Map as M
import Data.Ratio (Rational, denominator, numerator)
import Data.List (intersperse, mapAccumL)

-- Datatypes for testing purposes
--import Datatypes

-- GroteTrap's language definition
import qualified Language.GroteTrap.Language as GT

deriving instance Show Unit
deriving instance (Show a, Show b) => Show (Sum a b)
deriving instance (Show a, Show b) => Show (Prod a b)
deriving instance Typeable Unit
deriving instance Typeable2 Prod
deriving instance Typeable2 Sum


-- | gshow, redefined because we want some specific cases
gshow :: Data a => a -> String
gshow = ( \t ->
                "("
             ++ showConstr (toConstr t)
             ++ concat (gmapQ ((++) " " . gshow) t)
             ++ ")"
        ) 
        `ext1Q` showList
        `ext2Q` showPair
        `extQ` (show :: String -> String)
        `extQ` (show :: Char -> String) -- gshow doesn't print quotes
        `extQ` showRational

newtype Q r a = Q { unR :: a -> r }
ext2Q :: (Data d, Typeable2 t) => 
  (d -> q) -> (forall d1 d2. (Data d1, Data d2) => t d1 d2 -> q) -> d -> q
ext2Q def ext arg = 
  case dataCast2 (Q ext) of
    Just (Q ext') -> ext' arg
    Nothing       -> def arg

showRational :: Rational -> String
showRational r = "(" ++ gshow (numerator r) ++ "%" ++ gshow (denominator r) ++ ")"

showList :: Data a => [a] -> String
showList l = "[" ++ concat (intersperse "," (map gshow l)) ++ "]"

showPair :: (Data a, Data b) => (a, b) -> String
showPair (a,b) = "(" ++ gshow a ++ "," ++ gshow b ++ ")"


-- Valid selections generator
type Range = (Int, Int)
type Selections = M.Map Range Dynamic
{-
instance Eq Dynamic where -- workaround
  a == b = False
-}
-- | Language class elements have a 'GT.Language' from 'GroteTrap'
class Language a where
  langDef :: GT.Language a
  
instance (Language a, Language b) => Language (Sum a b) where
  langDef = langDef

instance (Language a, Language b) => Language (Prod a b) where
  langDef = langDef
  
newtype GSelect a = GSelect 
  { gselect' :: () => Int -> a -> Selections }

instance Generic GSelect where
  unit        = GSelect (\_ _ -> M.empty)
  int         = GSelect (\i n -> M.singleton (i, i + (length . gshow) n) (toDyn n))
  char        = GSelect (\i c -> M.singleton (i, i + (length . gshow) c) (toDyn c))
  plus rA rB  = GSelect (\i x -> case x of
                                      Inl l -> (gselect' rA) i l
                                      Inr r -> (gselect' rB) i r)
  prod        = gselectProd
  datatype ep rA = GSelect (\i x -> (gselect' rA) i ((from ep) x))
  constr      = gselectConstr
  
gselectConstr :: Typeable a => Name -> GSelect a -> GSelect a
gselectConstr s rA = GSelect (\i x -> 
    let lCon = length s
        m = (gselect' rA) (i + (2 + lCon)) x
        ri = 2 + if M.null m then (i + lCon) else snd (getRange m)
        own = M.singleton (i, ri) (toDyn x)
    in M.union own m)
  
gselectProd :: GSelect a -> GSelect b -> GSelect (Prod a b)
gselectProd rA rB = GSelect (\i (Prod l r) -> 
    let ml = (gselect' rA) i l
        ri = if M.null ml then (error "does this ever happen?") else 1 + snd (getRange ml)
        mr = (gselect' rB) ri r
    in M.union ml mr)
  
gselect :: GRep GSelect a => a -> Selections
gselect = gselect' grep 0
  
-- | Returns the range (lowest and highest indices) of these selections
getRange :: Selections -> Range
getRange m = (fst $ fst $ M.findMin m, maximum $ map (\((_,x),_) -> x) $ M.toList m)
-- Older implementation, possibly faster but possibly flawed
--getRange m = (fst $ fst $ M.findMin m, snd $ fst $ M.findMax m)
  
-- Special case for lists
  
-- Embedding-projection pair

fromList :: [a] -> Sum Unit (Prod a ([a]))
fromList [] = Inl Unit
fromList (h:t) = Inr (Prod h t)

toList :: Sum Unit (Prod a ([a])) -> [a]
toList (Inl Unit) = []
toList (Inr (Prod h t)) = h:t

-- Representation instances

rList :: (Typeable a, Generic g) => g a -> g [a]
rList rA = datatype  (EP fromList toList)
                     (plus  (constr "[]" unit)
                            (constr "(:)" (prod rA (rList rA))))

class Generic g => GenericList g where
  list :: Typeable a => g a -> g [a]
  list = rList
  
instance (Typeable a, GenericList g, GRep g a) => GRep g [a] where
  grep = list grep

instance GenericList GSelect where
  list rA = GSelect (\i x -> 
    let
      -- To allow selecting the square brackets as well
      own = M.singleton (i, finalAcc + 1) (toDyn x) 
      
      -- To allow selecting each individual element
      -- recall mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
      (finalAcc, y) = mapAccumL f (i + 1) x
      f acc x = (1 + snd (getRange ((gselect' rA) acc x)), (gselect' rA) acc x)
      
      -- To allow selecting any number of adjacent elements
      closure :: [Selections] -> [Selections]
      closure l | length l < 2 = l
                | otherwise = case l of 
                  (m:t) -> m : map (M.mapKeys (\(_,u) -> (fst (getRange m),u))) t ++ closure t
    -- We return the union of everything
    in M.union own $ M.unions $ closure y)

-- Special case for 'String's
class Generic g => GenericString g where
  string :: g String
  string = rList char
  
instance (GenericString g, GRep g String) => GRep g String where
  grep = string

instance GenericString GSelect where
  string = GSelect (\i s -> M.singleton (i, i + 2 + length s) (toDyn s))
  
{-
-- Special case for 'Language's
class Generic g => GenericLanguage g where
  language :: Language a => g a -> g b
  --language = undefined
  
instance (Language a, GenericLanguage g) => GRep g a where
  grep = language grep

instance GenericLanguage GSelect where
  language rA = let
                  x = langDef
                in undefined
-}

-- Testing
main t = do
  let term = gshow t
      subs = [(l, u, subList (l,u) term, t) | ((l,u),t) <- M.toAscList (gselect t)]
  putStrLn (show (length subs) ++ " valid subselections:")
  sequence_ (map (\(a, b, c, d) -> putStrLn (show a ++ "--" ++ show b ++ ": " ++ c ++ "\t :: " ++ (showsTypeRep (dynTypeRep d) ""))) subs)

subList (lo, up) = drop lo . take up

t1 = main ([1..5] :: [Int])
t2 = main ["a", "bc", "def"]