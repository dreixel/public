{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies       #-}
{-# LANGUAGE EmptyDataDecls     #-}

module Representations where


-- --------------------------------------
-- Structure constructors
-- --------------------------------------
data Zero r
data Id r       = Id { unId :: r } deriving Show
data K a r      = K a deriving Show
data Unit r     = Unit deriving Show
data Sum f g r  = Inl (f r) | Inr (g r) deriving Show
data Prod f g r = Prod (f r) (g r) deriving Show
data Con f r    = Con String (f r) deriving Show


-- --------------------------------------
-- Fixed-point constructor
-- --------------------------------------
newtype Fix f = In {out :: f (Fix f)}
deriving instance Show (f (Fix f)) => Show (Fix f)


-- --------------------------------------
-- Generic recursive view on data types
-- --------------------------------------
class Functor (PF a) => PFView a where
  type PF a :: * -> *
  from      :: a -> PF a a
  to        :: PF a a -> a


