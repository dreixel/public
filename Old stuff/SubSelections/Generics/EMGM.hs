{-# OPTIONS_GHC -fglasgow-exts #-}

module Generics.EMGM (module Generics.EMGM, module Generics.Structure) where

import Generics.Structure

import Data.Dynamic

---------------------------------
-- Generic function classes
---------------------------------

class Generic g where
  unit      ::  g Unit
  int       ::  g Int
  char      ::  g Char
  plus      ::  g a -> g b -> g (Sum a b)
  prod      ::  g a -> g b -> g (Prod a b)
  datatype  ::  EP b a -> g a -> g b
  -- TODO: Analyze if this Typeable below is *really* necessary
  constr    ::  Typeable a => Name -> g a -> g a

class Generic2 g where
  unit2       ::  g Unit Unit
  int2        ::  g Int Int
  char2       ::  g Char Char
  plus2       ::  g a1 a2 -> g b1 b2 -> g (Sum a1 b1) (Sum a2 b2)
  prod2       ::  g a1 a2 -> g b1 b2 -> g (Prod a1 b1) (Prod a2 b2)
  datatype2   ::  EP a2 a1 -> EP b2 b1 -> g a1 b1 -> g a2 b2

class Generic3 g where
  unit3       ::  g Unit Unit Unit
  int3        ::  g Int Int Int
  char3       ::  g Char Char Char
  plus3       ::  g a1 a2 a3 -> g b1 b2 b3 -> g (Sum a1 b1) (Sum a2 b2) (Sum a3 b3)
  prod3       ::  g a1 a2 a3 -> g b1 b2 b3 -> g (Prod a1 b1) (Prod a2 b2) (Prod a3 b3)
  datatype3   ::  EP a2 a1 -> EP b2 b1 -> EP c2 c1 -> g a1 b1 c1 -> g a2 b2 c2

---------------------------------
-- Type representation classes
---------------------------------

-- Arity 0

class GRep g a where
  grep :: g a

instance Generic g => GRep g Unit where
  grep = unit

instance Generic g => GRep g Int where
  grep = int

instance Generic g => GRep g Char where
  grep = char

instance (Generic g, GRep g a, GRep g b) => GRep g (Sum a b) where
  grep = plus grep grep

instance (Generic g, GRep g a, GRep g b) => GRep g (Prod a b) where
  grep = prod grep grep

-- Arity 1

class FRep g f where
  frep :: g a -> g (f a)

-- Arity 2

class FRep2 g f where
  frep2 :: g a b -> g (f a) (f b)

-- Arity 3

class FRep3 g f where
  frep3 :: g a b c -> g (f a) (f b) (f c)
  

{-
---------------------------------
-- Tree a
---------------------------------

-- Datatype

data Tree a = Leaf | Branch a (Tree a) (Tree a) deriving Show

-- Embedding-projection pair

fromTree :: Tree a -> Sum Unit (Prod a (Prod (Tree a) (Tree a)))
fromTree  Leaf               =  Inl Unit
fromTree  (Branch a lt rt)   =  Inr (Prod a (Prod lt rt))

toTree :: Sum Unit (Prod a (Prod (Tree a) (Tree a))) -> Tree a
toTree  (Inl Unit)                   = Leaf
toTree  (Inr (Prod a (Prod lt rt)))  = Branch a lt rt

-- Representation instances

rTree :: Generic g => g a -> g (Tree a)
rTree rA = datatype (EP fromTree toTree)
                    (plus 
                      (constr "Leaf" unit) 
                      (constr "Branch" 
                        (prod rA (prod (rTree rA) (rTree rA)))))

instance (Generic g, GRep g a) => GRep g (Tree a) where
  grep = rTree grep

instance Generic g => FRep g Tree where
  frep = rTree

rTree2 :: Generic2 g => g a b -> g (Tree a) (Tree b)
rTree2 rA = datatype2  (EP fromTree toTree)
                       (EP fromTree toTree)
                       (plus2 unit2 (prod2 rA (prod2 (rTree2 rA) (rTree2 rA))))

instance Generic2 g => FRep2 g Tree where
  frep2 = rTree2

rTree3 :: Generic3 g => g a b c -> g (Tree a) (Tree b) (Tree c)
rTree3 rA = datatype3  (EP fromTree toTree)
                       (EP fromTree toTree)
                       (EP fromTree toTree)
                       (plus3 unit3 (prod3 rA (prod3 (rTree3 rA) (rTree3 rA))))

instance Generic3 g => FRep3 g Tree where
  frep3 = rTree3

---------------------------------
-- List' a
---------------------------------

-- Datatype

data List' a   = Nil | Cons a (List' a) deriving Show

-- Embedding-projection pair

fromList :: List' a -> Sum Unit (Prod a (List' a))
fromList  Nil               =  Inl Unit
fromList  (Cons a as)       =  Inr (Prod a as)

toList :: Sum Unit (Prod a (List' a)) -> List' a
toList  (Inl Unit)          =  Nil
toList  (Inr (Prod a as))   =  Cons a as

-- Representation instances

rList :: Generic g => g a -> g (List' a)
rList rA = datatype  (EP fromList toList)
                     (plus  (constr "Nil" unit)
                            (constr "Cons" (prod rA (rList rA))))

instance (Generic g, GRep g a) => GRep g (List' a) where
  grep = rList grep

instance Generic g => FRep g List' where
  frep = rList

rList2 :: Generic2 g => g a b -> g (List' a) (List' b)
rList2 rA = datatype2  (EP fromList toList)
                       (EP fromList toList)
                       (plus2 unit2 (prod2 rA (rList2 rA)))

instance Generic2 g => FRep2 g List' where
  frep2 = rList2

rList3 :: Generic3 g => g a b c -> g (List' a) (List' b) (List' c)
rList3 rA = datatype3  (EP fromList toList)
                       (EP fromList toList)
                       (EP fromList toList)
                       (plus3 unit3 (prod3 rA (rList3 rA)))

instance Generic3 g => FRep3 g List' where
  frep3 = rList3

---------------------------------
-- gempty
---------------------------------

newtype GemptyT a = Gempty { gempty' :: a }

instance Generic GemptyT where
  unit                  =  Gempty Unit
  int                   =  Gempty 0
  char                  =  Gempty '\NUL'
  plus          rA  rB  =  Gempty (Inl (gempty' rA))
  prod          rA  rB  =  Gempty (Prod (gempty' rA) (gempty' rB))
  datatype  ep  rA      =  Gempty (to ep (gempty' rA))
  constr    s   rA      =  Gempty (gempty' rA)

gempty :: GRep GemptyT a => a
gempty = gempty' grep
-}
