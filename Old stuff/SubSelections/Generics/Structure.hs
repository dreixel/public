module Generics.Structure where

type Name =  String

---------------------------------
-- Structure types
---------------------------------

data Unit      = Unit
data Sum a b   = Inl a | Inr b
data Prod a b  = Prod a b

---------------------------------
-- Embedding-projection pair
---------------------------------

data EP b c = EP {from :: (b -> c), to :: (c -> b)}