{-# OPTIONS -fglasgow-exts -fallow-overlapping-instances -fallow-undecidable-instances #-}

module Datatypes where

import Data.Generics (Data, Typeable)

{-
import SP

{-! global : SPView !-}
-}

{-
import Base
import Representations

instance PFView Int where
  type PF Int = K Int
  from = K
  to (K i) = i

{-! for A1 derive : PFView !-}
{-! for A2 derive : PFView !-}
{-! for B derive : PFView !-}
{-! for C derive : PFView !-}
{-! for G derive : PFView !-}
{-! for Tree derive : PFView !-}
{-! for Exp derive : PFView !-}
-}

-- Example datatypes
data A = A A1 A2 deriving (Data, Typeable, Show)
data A1 = A1 Int deriving (Data, Typeable, Show)
data A2 = A2 Char deriving (Data, Typeable, Show) 

a1 = A (A1 1) (A2 'a')
a2 = A1 1
a3 = A1 2

data B = B1 Int B | B2 deriving (Data, Typeable, Show)
b1 = B1 1 (B1 2 (B1 3 (B1 4 B2)))
b2 = B1 2 (B1 3 B2)
b3 = B1 3 (B1 4 B2)

data C = C1 Int C C | C2 deriving (Data, Typeable, Show)

c1 = C1 3 c2 c3
c2 = C1 2 (C1 0 C2 C2) (C1 1 C2 C2)
c3 = C1 4 (C1 5 C2 C2) C2

data D = D1 Int E | D2 deriving (Data, Typeable, Show)
data E = E1 Int D | E2 deriving (Data, Typeable, Show)

d1 = D1 1 e1
e1 = E1 1 d2
d2 = D1 2 e2
e2 = E1 2 d3
d3 = D1 3 e3
e3 = E2

data F = F Int [F] deriving (Data, Typeable, Show)

f1 = F 1 [f2, f3, f4]
f2 = F 0 []
f3 = F 0 [f2, f2]
f4 = F 2 [F 4 []]

data G a b = G a b deriving (Data, Typeable, Show)

g1 = G g2 g3
g2 = G 1 '1' :: G Int Char
g3 = [1..4] :: [Int]
g4 = G 1 1 :: G Int Int

data Tree x = Leaf x | Branch x (Tree x) (Tree x) deriving (Data, Typeable, Show)

t1 :: Tree Int
t1 = Branch 1 (Branch 2 (Leaf 3) (Leaf 4)) (Leaf 5)
t2 :: Tree Int
t2 = Branch 0 t1 t1
t3 :: Tree String
t3 = Branch "" (Leaf "hello") (Leaf "world")

data Exp = 
    Exp :+: Exp
  | Exp :*: Exp
  | Exp :/: Exp
  | Neg Exp
  | Const Int deriving (Eq, Show, Data, Typeable)

exp1 = Const 1 :+: (Const 2 :+: (Const 3 :+: Const 4))
exp2 = (Const 1 :+: Const 2) :+: (Const 3 :+: Const 4)
exp3 = (((Const 1 :+: Const 2) :+: Const 3) :+: Const 4)
exp4 = Const 1 :+: ((Const 2 :+: Const 3) :+: Const 4)
exp5 = (exp2 :+: exp2) :+: (exp2 :+: exp2)
