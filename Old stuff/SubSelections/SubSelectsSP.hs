{-# OPTIONS -Wall -fno-warn-orphans #-}
{-# LANGUAGE StandaloneDeriving     #-}
{-# LANGUAGE DeriveDataTypeable     #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE TypeSynonymInstances   #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE OverlappingInstances   #-}
{-# LANGUAGE UndecidableInstances   #-}

module SubSelectsSP where

-- Generic library
import Generics.SP.Base
import Generics.SP.Representation
import qualified Generics.SP.Show as G

-- Standard modules
import qualified Data.Map as M
import Data.Dynamic
import Data.Generics hiding (toConstr, dataTypeOf, Infix, Unit, (:*:), (:+:))
import Control.Monad.State

deriving instance Typeable Unit
deriving instance Typeable1 Var
deriving instance Typeable2 (:*:)

errorNever :: Int -> a
errorNever n = error ("The impossible happened: error code #" ++ show n)

-- Valid selections generator
type Range = (Int, Int)
type Selections = M.Map Range Dynamic

data MyState = MyState {
                    pos :: Int,
                    isInfix :: Bool,
                    lenInfix :: Int
                  }

newState :: MyState
newState = MyState { pos = 0, isInfix = False, lenInfix = 0 }

incState :: State MyState ()
incState = do
  s <- get
  let i = pos s
  put (s { pos = i + 1 })

-- | Returns the range (lowest and highest indices) of these selections
getRange :: Selections -> Range
getRange m = (fst $ fst $ M.findMin m, maximum $ map (\((_,x),_) -> x) $ M.toList m)

class SubSel' a where
  subsel' :: a -> State MyState Selections

instance SubSel' Unit where
  subsel' Unit = return M.empty
  
instance (SubSel a, SubSel b) => SubSel' (a :+: b) where
  subsel' (L x) = subsel x
  subsel' (R x) = subsel x
  
instance (SubSel a, SubSel b) => SubSel' (a :*: b) where
  subsel' (l :*: r) = do
    s <- get
    if (lenInfix s == 0)
      then do
            ml <- subsel l
            if M.null ml 
             then errorNever 0 
              else modify (\s' -> s' {pos = pos s + 1 + snd (getRange ml)})
            mr <- subsel r
            return $ M.union ml mr
      else do
            let lCon = lenInfix s
            incState
            ml <- subsel l
            let li = 2 + if M.null ml then errorNever 5 else snd (getRange ml)
            modify (\s' -> s' { pos = (li + 2 + lCon) })
            mr <- subsel r
            let ri = 1 + if M.null mr then errorNever 6 else snd (getRange mr)
                --own = M.singleton (i, ri) (toDyn x)
            return $ {-M.union own-} (M.union ml mr)
  
instance (Typeable a, SubSel a) => SubSel' (Con a) where
  subsel' (Con c x) =
    case conFixity c of
      Nonfix -> do
                  s <- get
                  let lCon = length (conName c)
                      i = pos s
                  modify (\s' -> s' { pos = i + 2 + lCon })
                  m <- subsel x
                  let ri = 2 + if M.null m then (i + lCon) else snd (getRange m)
                      own = M.singleton (i, ri) (toDyn x)
                  return $ M.union own m
      Infix _ -> let lCon = length (conName c) 
                 in modify (\s' -> s' { lenInfix = lCon }) >> subsel x
      Infixl _ -> let lCon = length (conName c) 
                  in modify (\s' -> s' { lenInfix = lCon }) >> subsel x
      Infixr _ -> let lCon = length (conName c) 
                  in modify (\s' -> s' { lenInfix = lCon }) >> subsel x

instance (SubSel a) => SubSel' (Var a) where
  subsel' (Var x) = subsel x

baseType :: (Show a, Typeable a) => a -> State MyState Selections
baseType x = do
    s <- get
    let i = pos s
    return $ M.singleton (i, i + (length . show) x) (toDyn x)
  
instance SubSel' Char where
  subsel' = baseType
  
instance SubSel' Int where
  subsel' = baseType

instance SubSel' Bool where
  subsel' = baseType


-- Dispatcher class
class (SPView a, SubSel' (SP a)) => SubSel a where
  subsel :: a -> State MyState Selections
  subsel = subsel' . to

instance SubSel Unit where
  subsel = subsel'
  
instance (SubSel a, SubSel b) => SubSel (a :+: b) where
  subsel = subsel'
  
instance (SubSel a, SubSel b) => SubSel (a :*: b) where
  subsel = subsel'
  
instance (Typeable a, SubSel a) => SubSel (Con a) where
  subsel = subsel'

instance (Typeable a, SubSel a) => SubSel (Var a) where
  subsel = subsel'

instance SubSel Char
instance SubSel Int

-- An adhoc instance for lists
{-
instance (Typeable a, SubSel a) => SubSel [a] where
  subsel x = do
    s <- get
    let
      i = pos s
      -- To allow selecting the square brackets as well
      own = M.singleton (i, finalAcc + 1) (toDyn x)
      
      -- To allow selecting each individual element
      -- recall mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
      (finalAcc, y) = mapAccumL f (i + 1) x
      f acc z = (1 + snd (getRange (subsel acc z)), subsel acc z)
      
      -- To allow selecting any number of adjacent elements
      closure :: [Selections] -> [Selections]
      closure l | length l < 2 = l
                | otherwise = case l of 
                  (m:t) -> m : map (M.mapKeys (\(_,u) -> (fst (getRange m),u))) t ++ closure t
                  _ -> errorNever 4
    -- We return the union of everything
    return $ M.union own $ M.unions $ closure y
-}

-- An adhoc instance for Strings
instance SubSel String where
  subsel s = do
    s' <- get
    let i = pos s'
    return $ M.singleton (i, i + 2 + length s) (toDyn s)

-- An adhoc instance for pairs
instance (SubSel a, Typeable a, SubSel b, Typeable b) => SubSel (a,b) where
  subsel (x,y) = do
      s <- get
      let i = pos s
      modify (\s' -> s' { pos = i + 1 })
      m1 <- subsel x
      let i1 = if M.null m1 then errorNever 2 else 1 + snd (getRange m1)
      modify (\s' -> s' { pos = i1} )
      m2 <- subsel y
      let i2 = if M.null m2 then errorNever 3 else 1 + snd (getRange m2)
          own = M.singleton (i, i2 + 1) (toDyn (x,y))
      return $ M.union own (M.union m1 m2)

-- A generic instance for everything that has an SPView
-- Disabled for now
--instance (SubSel a, SubSel' (SP a), Typeable a, SPView a) => SubSel a
instance (Typeable a, SubSel a) => SubSel (Maybe a)
instance G.Show Logic
instance SubSel Bool
instance SubSel Logic


-- Testing
main :: (G.Show a, SubSel a) => a -> IO ()
main t = do
  let term = G.show t
      subs = [(l, u, subList (l,u) term, ty) | ((l,u),ty) <- M.toAscList (evalState (subsel t) newState)]
  putStrLn (show (length subs) ++ " valid subselections:")
  sequence_ (map (\(a, b, c, d) -> putStrLn (show a ++ "--" ++ show b ++ ": " ++ c ++ "\t :: " ++ (showsTypeRep (dynTypeRep d) ""))) subs)

subList :: Range -> [a] -> [a]
subList (lo, up) = drop lo . take up

t1, t2, t3, t4, t5, t6 :: IO ()
t1 = undefined --main ([1..5] :: [Int])
t2 = main "def"
t3 = main ('a', 'b')
t4 = main (123::Int, Nothing::Maybe Char)
t5 = main (Just 'c')
t6 = main (Just (Just (Just (2::Int))))

infixr 6 :||:

data Logic
  = VarL String
  | Const Bool
  | Not Logic
  | Logic :||: Logic
  deriving (Show, Eq, Data, Typeable)

instance SPView Logic where
  type SP Logic = 
      (Con String)
    :+:
      (Con Bool)
    :+:
      (Con Logic)
    :+:
      (Con (Logic :*: Logic))
  
  to (VarL s) = L (Con conVarL s)
  to (Const b) = R (L (Con conConst b))
  to (Not p) = R (R (L (Con conNot p)))
  to (p :||: q) = R (R (R (Con conOr (p :*: q))))
  from (L (Con _ s)) = VarL s
  from (R (L (Con _ b))) = Const b
  from (R (R (L (Con _ p)))) = Not p
  from (R (R (R (Con _ (p :*: q))))) = p :||: q
  
conVarL, conConst, conNot, conOr :: ConDescr
conVarL = ConDescr {
  conName = "VarL",
  conArity = 1,
  conLabels = [],
  conFixity = Nonfix }
conConst = ConDescr {
  conName = "Const",
  conArity = 1,
  conLabels = [],
  conFixity = Nonfix }
conNot = ConDescr {
  conName = "Not",
  conArity = 1,
  conLabels = [],
  conFixity = Nonfix }
conOr = ConDescr {
  conName = ":||:",
  conArity = 2,
  conLabels = [],
  conFixity = Infixr 6 }

t7 :: IO ()
t7 = main (VarL "p" :||: (Not (Const True)))
