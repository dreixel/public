{-# OPTIONS_GHC -fglasgow-exts #-}

module Test where

import Prelude hiding (fmap)
import Control.Monad hiding (fmap)
{-
data EP a b = EP { from :: a -> b, to :: b -> a}

data Fix f = In {out :: f (Fix f)}

epFix :: (forall a b. EP a b -> EP (f a) (g b)) -> 
           EP (Fix f) (Fix g)
epFix epfg = EP from' to' where
  from' = In . (from $ epfg $ epFix epfg) . out
  to' = In . (to $ epfg $ epFix epfg) . out


data HFix f a = HIn {hOut :: f (HFix f) a}

epHFix ::  (forall (c1 :: * -> *) (c2 :: * -> *) d1 d2. 
             (forall e1 e2. EP e1 e2 -> EP (c1 e1) (c2 e2)) 
             -> EP d1 d2 -> EP (f c1 d1) (g c2 d2)) 
           -> EP a b -> EP (HFix f a) (HFix g b)
epHFix epf epx = EP from' to' where
  from'   = HIn .  (from  $ epf (epHFix epf) epx) .  hOut
  to'     = HIn .  (to    $ epf (epHFix epf) epx) .  hOut

  
f :: forall (f :: (* -> *) -> * -> *) a.
     (forall (g :: * -> *) e1.
      (forall d. [d] -> [g d]) -> [e1] -> [f g e1])
     -> (forall (g :: * -> *) e2.
         (forall c. [c] -> [g c])
         -> (forall b.
             [b] -> (b -> b -> Bool) -> g b -> g b -> Bool)
         -> [e2]
         -> (e2 -> e2 -> Bool)
         -> f g e2
         -> f g e2
         -> Bool)
     -> [a]
     -> (a -> a -> Bool)
     -> HFix f a
     -> HFix f a
     -> Bool
-}
data HFix f a = HIn (f (HFix f) a)
     
data Fix f = In {out :: f (Fix f)}
data Unit r = Unit
data Id r = Id { unId :: r }
data K a r = K a deriving Show
data Sum f g r = Inl (f r) | Inr (g r)
data Prod f g r = Prod (f r) (g r)
type PFTree a = Sum (K a) (Prod Id Id)
type FTree a = Fix (PFTree a)

class FixedPointView a where
    type PF a :: * -> *
    from :: a -> Fix (PF a)
    to :: Fix (PF a) -> a

instance FixedPointView (Tree a) where
    type PF (Tree a) = PFTree a
    from = fromTree
    to = toTree

type Hole f = Fix (Sum Unit (PF f))

-- Example datatype
data Tree a = Leaf a | Node (Tree a) (Tree a) deriving (Show)

fromTree :: Tree a -> FTree a
fromTree (Leaf a) = In (Inl (K a))
fromTree (Node l r) = In (Inr (Prod (Id $ fromTree l) (Id $ fromTree r)))

toTree :: FTree a -> Tree a
toTree (In (Inl (K a))) = Leaf a
toTree (In (Inr (Prod (Id l) (Id r)))) = Node (toTree l) (toTree r)

hole1, hole2, hole3 :: Hole (Tree Int)
hole1 = In (Inl Unit)
hole2 = In (Inr (Inl (K 1)))
hole3 = In (Inr (Inr (Prod (Id hole1) (Id hole2))))

class FMap f where
  fmap :: (a -> b) -> f a -> f b

instance FMap Unit where
  fmap _ _ = Unit
  
instance FMap Id where
  fmap f = Id . f . unId
  
instance FMap (K a) where
  fmap f (K x) = K x
  
instance (FMap a, FMap b) => FMap (Sum a b) where
  fmap f (Inl x) = Inl (fmap f x)
  fmap f (Inr x) = Inr (fmap f x)
  
instance (FMap a, FMap b) => FMap (Prod a b) where  
  fmap f (Prod x y) = Prod (fmap f x) (fmap f y)

  
insert :: FixedPointView a => [a] -> Hole a -> Maybe a
insert as h = return snd `ap` insert' as h
    where
        insert' :: [a] -> Hole a -> Maybe ([a], a)
        insert' []     (In (Inl Unit)) = Nothing
        insert' (a:as) (In (Inl Unit)) = Just (as, a)
        -- x :: PF a (Fix (Sum Unit (PF a)))
        insert' as     (In (Inr x)) = undefined