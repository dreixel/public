{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

{-# OPTIONS_GHC -Wall -fno-warn-name-shadowing #-}

module Builder where

import Generics.MultiRec.Base


data IxEl s where
  IxEl :: s ix -> ix -> IxEl s

infix 4 :=:

data (:=:) :: * -> * -> * where
    Refl :: a :=: a

class Eq_ s where
  eq_ :: s ix -> s ix' -> Maybe (ix :=: ix')


class Builder (f :: (* -> *) -> (* -> *) -> * -> *) s where
  hbuilder :: s ix -> f s I0 ix -> ([IxEl s] -> f s I0 ix)


instance Builder (K x) s where
  hbuilder _ x = \[] -> x

instance Builder U s where
  hbuilder _ x = \[] -> x

instance (Builder f s) => Builder (C c f) s where
  hbuilder s (C f) = C . hbuilder s f

instance (Builder f s, Builder g s) => Builder (f :+: g) s where
  hbuilder s (L f) = L . hbuilder s f
  hbuilder s (R f) = R . hbuilder s f

instance (Builder f s, Builder g s) => Builder (f :*: g) s where
  hbuilder s (f :*: g) = \(h:t) -> hbuilder s f [h] :*: hbuilder s g t

instance (Builder f s) => Builder (f :>: ix) s where
  hbuilder s (Tag f) = Tag . hbuilder s f

instance (Eq_ s, Ix s xi) => Builder (I xi) s where
  hbuilder s x = case eq_ s (index :: s xi) of
                   Nothing   -> \[]  -> x
                   Just Refl -> \[IxEl s' ix] -> 
                     case eq_ s s' of
                       Nothing   -> x
                       Just Refl -> I (I0 ix)


builder :: (Ix s ix, Builder (PF s) s) => s ix -> ix -> ([ix] -> ix)
builder s x xs = to (hbuilder s (from x) (toIxEl s xs)) where
  toIxEl :: s ix -> [ix] -> [IxEl s]
  toIxEl s = map (\x -> IxEl s x)
