{-# OPTIONS_GHC -fglasgow-exts #-}

module Test where

class E a c x where
  e :: a x -> c x

class I a c y where
  i :: c y -> a y

type Map t x y = (x -> y) -> t x -> t y
gmap :: (E a c x, I a c y) => Map c x y -> Map a x y
gmap gmc gmx = i . gmc gmx . e

type Eq' t x = (x -> x -> Bool) -> t x -> t x -> Bool
geq :: E a c x => Eq' c x -> Eq' a x
geq gec gsx = \a1 a2 -> gec gsx (e a1) (e a2)

type Fold t x r = (x -> r) -> t x -> r
gfold :: E a c x => Fold c x r -> Fold a x r
gfold gfc gfx = gfc gfx . e

class Linear l a where
  dsc0 :: l a -> Bool
  con0 :: l a
  con1 :: a -> l a -> l a
  sel10 :: l a -> a
  sel11 :: l a -> l a

tl :: (Linear g a, Linear f a) => g a -> f a
tl x = if dsc0 x then con0 else con1 (sel10 x) (tl (sel11 x))

instance (Linear a x, Linear c x) => E a c x where
  e = tl

instance (Linear a x, Linear c x) => I a c x where
  i = tl

instance Linear [] a where
  dsc0 = null
  con0 = []
  con1 = (:)
  sel10 = last
  sel11 = init
