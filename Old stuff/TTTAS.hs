{-# OPTIONS -fglasgow-exts  #-}
import Prelude hiding (lookup)
import qualified Prelude
import Maybe (isJust)
import List (sortBy, groupBy)
import Control.Monad.State
import Data.Map (Map)
import qualified Data.Map as Map (map, insert, lookup, fromList, adjust, empty, toList)
import Char
-- import Control.Arrow
type Grammar_    = Map NT_ [Prod_]
type NT_         = String
type Prod_       = [Symbol_]
type Symbol_     = String
isNonterminal_   = isUpper . head
isTerminal_      = isLower . head
grammar_ =  Map.fromList 
            [ ("A", [["a","A"], ["B"]])
            , ("B", [["A","b"],["c"]])]
type Step_State = (Grammar_, [Symbol_])
type Step_ a = State  Step_State a
leftcorner_ :: Grammar_ -> Grammar_
leftcorner_ g  =  fst  . snd . runState (rules1_ g g) 
                  $ (Map.empty,[])
rules1_  :: Grammar_ -> Grammar_ -> Step_ ()
rules1_ gram nts = mapM_  onent (Map.toList nts)
 where  
 onent  (a, prods) = 
        do  ps    <- mapM (rule1_ gram a) prods
            modify  (\ (g, _) 
                    -> (Map.insert a (concat ps) g,[]))
rule1_ :: Grammar_ -> NT_ -> Prod_ -> Step_ [Prod_]
rule1_ grammar a (x : beta) =
  insert_ grammar a x beta
rule2a_  :: NT_ -> Symbol_ -> Prod_
rule2a_ a_b b = [b, a_b]
rule2b_  :: Grammar_ -> NT_ -> NT_ -> Prod_ -> Step_ [Prod_]
rule2b_ grammar a a_b (y : beta) 
 = insert_ grammar a y (beta++[a_b])
insert_  ::  Grammar_ -> NT_  -> Symbol_ -> Prod_ 
                              -> Step_ [Prod_]
insert_ grammar a x p =
  do  let a_x = a ++ "_" ++ x
      (gram, lcs) <- get 
      if  x `elem` lcs
          then do   put (Map.adjust (p:) a_x gram, lcs)
                    return []
          else do  put (Map.insert a_x [p] gram, x:lcs)
                   rule2_ grammar a x                       
rule2_ :: Grammar_ -> NT_ -> Symbol_ -> Step_ [Prod_]
rule2_ grammar a b 
  | isTerminal_ b  = return [rule2a_ a_b b] 
  | otherwise      = 
      do  let Just prods = Map.lookup b grammar
          rs  <- mapM  (rule2b_ grammar a a_b) prods 
          return (concat rs)
  where a_b = a++"_"++b
data Equal a b where
 Eq :: Equal a a
reflex       :: Equal a a
reflex       = Eq
symm         :: Equal a b -> Equal b a
symm Eq      = Eq
trans        :: Equal a b -> Equal b c -> Equal a c
trans Eq Eq  = Eq
cast :: Equal a b -> a -> b
cast Eq = id
data Ref a env where
 Zero  ::  Ref a (a,env')
 Suc   ::  Ref a env' -> Ref a (x,env')
match :: Ref a env -> Ref b env -> Maybe (Equal a b)
match  Zero     Zero        =  Just Eq
match  (Suc x)  (Suc y)     =  match x y
match  _        _           =  Nothing
lookup :: Ref a env -> env -> a
lookup  Zero     (a,_ )  =  a
lookup  (Suc r)  (_, e)  =  lookup r e
update :: (a -> a) -> Ref a env -> env -> env  
update f  Zero     (a,e)   =  (f a, e)
update f  (Suc r)  (x, e)  =  (x, update f r e)
type ExampleEnv  =   (Int,(Char,(String,()))) 
example       ::  ExampleEnv
example       =   (1,('a',("b",())))
index_char    ::  Ref Char ExampleEnv
index_char    =   Suc Zero
index_int     ::  Ref Int ExampleEnv
index_int     =   Zero 
data Expr1 a where
     IntVal1    :: Int                               -> Expr1 Int
     BoolVal1   :: Bool                              -> Expr1 Bool
     Add1       :: Expr1 Int -> Expr1 Int            -> Expr1 Int
     LessThan1  :: Expr1 Int -> Expr1 Int            -> Expr1 Bool
     If1        :: Expr1 Bool -> Expr1 a -> Expr1 a  -> Expr1 a  
expr :: Expr1 Int
expr = If1  (LessThan1  (Add1 (IntVal1 3) (IntVal1 1)) 
                        (IntVal1 4)) 
            (IntVal1 5)
            (Add1 (IntVal1 1) (IntVal1 2)) 
data Expr a env where
     Var       ::  Ref a env      -> Expr a env
     IntVal    ::  Int            -> Expr Int env
     BoolVal   ::  Bool           -> Expr Bool env 
     Add       ::  Expr Int env   -> Expr Int env  
                                  -> Expr Int env
     LessThan  ::  Expr Int env   -> Expr Int env
                                  -> Expr Bool env 
     If        ::  Expr Bool env  -> Expr a env  
                   -> Expr a env  -> Expr a env
eval :: Expr a env -> env ->  a
eval (Var r)         e   =  lookup r e
eval (IntVal i)      _   =  i
eval (BoolVal b)     _   =  b
eval (Add x y)       e   =  eval x e + eval y e
eval (LessThan x y)  e   =  eval x e < eval y e
eval (If x y z)      e   =  if  eval x e 
                            then eval y e
                            else eval z e
ref_a         =   Var Zero
ref_b         =   Var (Suc Zero)

testexpr     :: Expr Int (Int,(Bool, env))
testexpr     =  If ref_b (IntVal 3) ref_a
testenv       ::  (Int,(Bool,()))
testenv       =   (11,(False,()))

test          :: Int
test          =  eval testexpr testenv
data Env t use def  where
  EMPTY  ::  Env t use ()
  PAIR   ::  t a use  -> Env t use       def' 
                      -> Env t use  (a,  def')
env :: Env Expr (Int,(Int,())) (Int,(Int,()))
env =  PAIR   (Add (IntVal 5) (Var (Suc Zero))) 
       (PAIR  (IntVal 3) 
       EMPTY) 
lookupEnv :: Ref a env -> Env t s env -> t a s
lookupEnv  Zero     (PAIR p _ )  =  p
lookupEnv  (Suc r)  (PAIR _ ps)  =  lookupEnv r ps

updateEnv  ::  (t a s -> t a s)  ->  Ref a env 
           ->  Env t s env     ->  Env t s env
updateEnv  f    Zero     (  PAIR      ta   rs)
             =              PAIR (f   ta)  rs
updateEnv  f    (Suc r)  (  PAIR      x    rs) 
             =              PAIR      x    (updateEnv f r rs)
newtype T e s
 = T {unT :: forall x . Ref x e -> Ref x s}
data Step m t a b =  
  Step( forall env1 . m env1 -> StepT m t a b env1)
data StepT m t a b env1 = 
  forall env2 . StepT 
                ( m env2)
                ( forall s  .      a s ->  T env2 s  ->  Env t s env1 ->
                                (  b s,    T env1 s  ,   Env t s env2)
                )
newSRef :: Step Unit t (t a) (Ref a)

data Unit s         = Unit
newSRef 
  =  Step  
     (\Unit ->  StepT  
                Unit
                (\ta (T tr) env ->
                    ( tr Zero
                    , T (tr . Suc)
                    , PAIR ta env
     )          )   )
runStep   ::  Step m t a b ->  (m ()) -> (forall s . a s)
          ->  Result m t b

data Result m t b  =  forall s . 
                      Result  (m s) (b s) (Env t s s)
runStep  (Step st) m a 
  = case st m of
        StepT m' f ->  let (b, _, e) = f  a (T id) EMPTY 
                       in  Result m' b e
arr :: (forall s . a s -> b s) -> Step m t a b
arr f 
  =  Step  (\m -> StepT m (\a t e -> (f a, t, e)) )
(>>>) :: Step m t a b -> Step m t b c -> Step m t a c
(>>>) (Step sa) (Step sb) = 
  Step 
  (\m1 ->
     case sa m1 of 
      StepT m2 f1 -> case sb m2 of 
       StepT m3 f2 -> 
         StepT  
         m3 
         (\a t3s e1 -> let  (b, t1s, e2) = f1 a t2s e1
                            (c, t2s, e3) = f2 b t3s e2
                       in   (c, t1s, e3)
         ))
newtype Tuple a b s = TP (a s, b s)

first :: Step f t a b -> Step f t (Tuple a c) (Tuple b c)
first (Step s) 
  =  Step
     (\m1 -> case s m1 of
        StepT m2 f -> 
           StepT  m2
                  (\(TP (a, c)) t2s e1 -> 
                                    let (b,t12,e2) = f a t2s e1 
                                    in  (TP (b, c),t12,e2)
                  )              
     ) 
second :: Step m t b c -> Step m t  (Tuple d b)
                                    (Tuple d c)
second f = arr swap >>> first f >>> arr swap
            where   swap ~(TP (x, y)) = TP (y, x)

(***)  ::  Step m t b  c 
       ->  Step m t b' c'
       ->  Step m t (Tuple b b') (Tuple c c')
f *** g =  first f >>> second g  
(&&&)  ::  Step m t b c 
       ->  Step m t b c'
       ->  Step m t b (Tuple c c')
f &&& g = arr (\b -> TP (b, b)) >>> (f *** g)      
loop  :: Step m t (Tuple a x) (Tuple b x)
      -> Step m t a b
loop (Step st) =
  Step 
  (\m -> case st m of
          StepT m1 f1 ->
           StepT  m1
           (\a t e -> 
             let (TP (b, x),t1,e1) = f1 (TP (a, x)) t e                                                                            
             in (b,t1,e1)
           ) 
  )
newtype List a s = List [a s]
sequenceA :: [Step m t a b] -> Step m t a (List b)
sequenceA [] = arr (const (List []))
sequenceA (x:xs) 
     =  (x &&& sequenceA xs) >>> 
        arr (\(TP (a,List as)) -> List (a:as))
data Grammar a  
  = forall env . Grammar  (Ref a env)  
                          (Env Productions env env)
newtype Productions a env 
  = PS {unPS :: [Prod a env]}
data Symbol a env where
  Nont :: Ref a env ->  Symbol  a       env
  Term :: String    ->  Symbol  String  env
data Prod a env where
      Seq   ::  Symbol    b        env  ->  Prod   (  b -> a)  env  
                                        ->  Prod      a        env
      End   ::  a                       ->  Prod      a        env
infixr 5 `cons` , .*.

cons prods g = PAIR (PS prods) g
(.*.)        = Seq
_A =  Nont       Zero
_B =  Nont (Suc  Zero)
_a =  Term "a"
_b =  Term "b"
_c =  Term "c"

-- A ::= aA $\mid$ B
-- B ::= Ab $\mid$ c
type NT_Types = (String,(Int, ()))
grammar :: Grammar String
grammar =  Grammar Zero productions
productions :: Env  Productions NT_Types NT_Types
productions 
 =  [ _a   .*. _A  .*.  End (++)
    , _B           .*.  End show
    ] `cons`             
    [ _A   .*. _b  .*.  End (\y x -> length x + length y)
    , _c           .*.  End (const 1)
    ] `cons` EMPTY 
append  :: (a -> b -> c) -> Prod a env -> Symbol b env 
        -> Prod c env
append g (End f     )  s  =  Seq s  (End (g f))
append g (Seq t ts  )  s  
  =  Seq t (append  (\b c d -> g (b d) c) ts s)
matchSym  ::  Symbol a env -> Symbol b env 
          ->  Maybe (Equal a b)
matchSym (Nont x)  (Nont y)             = match x y
matchSym (Term x)  (Term y) | x == y    = Just Eq
matchSym _         _                    = Nothing
mapProd  :: T env1 env2 -> Prod a env1 -> Prod a env2
mapProd t (End x)           = End x
mapProd t (Seq (Nont x) r)  = Seq (Nont (unT t x)) 
                                  (mapProd t r)
mapProd t (Seq (Term x) r)  = Seq (Term x) 
                                  (mapProd t r)
newtype MapA_X env a env2
  =  MapA_X 
     ( forall x  .   Symbol x env 
                 ->  Maybe (Ref (x -> a) env2)
     )
emptyA_X  ::  MapA_X env a env2 
emptyA_X  =   MapA_X (const Nothing)
initA_X  ::  Step (MapA_X env a) t c d 
         ->  Step Unit t c d
initA_X (Step st)
  = Step  (\_ ->  case st emptyA_X of
                    StepT _  f -> StepT Unit f
          )
newNont ::  forall x env t a 
        .   Symbol x env 
        ->  Step  (MapA_X env a) 
                  Productions 
                  (Productions (x->a)) 
                  (Ref (x->a))
newNont x = 
  Step 
  (\(MapA_X m :: MapA_X env a env') ->
     let  m2 :: MapA_X env a (x->a, env')
          m2 = MapA_X  (\s -> case matchSym s x of
                           Just Eq -> Just Zero
                           Nothing -> fmap Suc (m s)
                       )
     in StepT  m2 
               (\tx (T t) e ->  (  t Zero
                                ,  T (t . Suc) 
                                ,  PAIR tx e
  )            )                )

newtype Mapping old new 
         = Mapping (Env Ref new old) 

map2trans :: Mapping env s -> T env s
map2trans (Mapping env) 
   = T (\r -> (lookupEnv r env))

type LCStep env a inp out = Step  (MapA_X env a)
                                  Productions
                                  (Tuple (T env) inp)
                                  out

insert ::  forall env a x
       .   Env Productions env env 
       ->  Symbol x env
       ->  LCStep env a  (Prod (x->a))
                         (Productions a)
insert grammar x =
  Step
  (\(MapA_X m) -> case m x of
     Nothing  ->  
       case  ( second (arr  (\p -> PS [p]) >>> newNont x)
             ) >>> rule2 grammar x
       of Step step -> step (MapA_X m)
     Just r   -> StepT (MapA_X m) 
                      (\(TP (_,p)) t e -> 
                          ( PS []
                          , t
                          , updateEnv  (\  (  PS      ps) 
                                       ->     PS (p:  ps)
                                       ) r e
                          )                   
                      ))

rule2  :: Env Productions env env 
       -> Symbol x env 
       -> LCStep env a (Ref (x -> a)) (Productions a)
rule2 grammar (Term a) = 
  arr (\  (TP (_, a_x)) -> PS [rule2a a a_x])
rule2 grammar (Nont b) = 
  case lookupEnv b grammar of
   PS ps ->  sequenceA  (map  ( rule2b grammar) ps
                        ) >>> 
             arr  (\(List pss) -> 
                      PS (concatMap unPS pss)
                  )

rule2a :: String -> Ref (String -> a) s -> Prod a s
rule2a a refA_a
  =  Term a .*. Nont refA_a .*. End ($) 

rule2b  :: Env Productions env env 
        -> Prod b env 
        -> LCStep env a (Ref (b -> a)) (Productions a)
rule2b grammar (Seq x beta) 
 = (arr (\(TP (env2s, a_b)) -> 
             TP (env2s,append  (flip (.)) 
                     (mapProd env2s beta) 
                     (Nont a_b)) 
       )) >>> insert grammar x

rule1  :: Env Productions env env -> Prod a env 
       -> LCStep env a Unit (Productions a)
rule1 grammar (Seq x beta) =
  arr  (\(TP (env2s, Unit))
            -> TP(env2s,mapProd env2s beta)
       ) 
  >>> insert grammar x

rules1  ::  Env Productions env env
        ->  Env Productions env env' 
        ->  Step  Unit 
                  Productions
                  (T env) 
                  (Mapping env')

rules1 productions (PAIR nt@(PS prods) ps) = 
  (  (  initA_X 
        (  arr (\tenv_s -> TP (tenv_s, Unit)) >>>
           sequenceA  (map  ( rule1 productions                              
                            ) prods
                      ) >>> 
           arr (\(List pss) -> PS (concatMap unPS pss))
        ) >>> newSRef 
     ) 
  &&& 
  rules1 productions ps
  ) >>>  arr  (\(TP (r, (Mapping  e)))
                    -> Mapping (PAIR   r e)
              )
rules1 _ EMPTY          
     = arr (const (Mapping EMPTY))  

leftcorner :: forall a . Grammar a -> Grammar a
leftcorner (Grammar start productions)
   = let result = runStep  
                  ( loop(
                  arr ( \ (TP (_, menv_s)) -> map2trans menv_s)
                  >>> (arr (\tenv_s -> unT tenv_s start) 
                       &&& rules1 productions productions)
                       
                  ))
                  Unit       -- meta-data
                  undefined  -- input
     in case result of 
       Result _ r gram -> Grammar r gram

mapEnv  ::  (forall a . f a s -> g a s)  
        ->  Env f s env -> Env g s env
mapEnv  f EMPTY          = EMPTY
mapEnv  f (PAIR v rest)  = PAIR (f v) (mapEnv f rest)

newtype Const f a s = C {unC :: f a}

compile :: forall a . Grammar a -> Parser a
compile (Grammar (start :: Ref a env) rules) 
                       = unC (lookupEnv start result)
  where  result  =  
          mapEnv 
          (\ (PS ps) -> C (choice [ comp p | p <- ps]))
          rules
         comp :: forall a . Prod a env -> Parser a
         comp (End x)   = succeed x 
         comp (Seq (Term t) ss) 
                       = (flip ($))  <$> token t <*> comp ss 
         comp (Seq (Nont n) ss) 
                       = (flip ($))  <$> unC (lookupEnv n result)
                                     <*> comp ss

infixl 6 <*>
infixr 5 <|>

parse    :: Parser a -> [Char] -> a
symbol   :: Char -> Parser Char
succeed  :: a -> Parser a
(<|>)    :: Parser a -> Parser a -> Parser a
(<*>)    :: Parser (a -> b) -> Parser a -> Parser b

newtype Parser a = P (String -> [(a,String)])
parse (P p) inp = case [r | (r,[]) <- p inp ] of
                    []     -> error "parse failed"
                    (x:_)  -> x 
succeed x = P (\inp -> [(x,inp)])
symbol  x = P (\inp -> case inp of
                      i:is | i == x -> [(i,is)]
                      _             -> []
              )
P p <|> P q   = P (\inp ->  p inp ++ q inp)
P p <*> P q   = P (\inp ->  [(f a,inp2) 
                            | (f,inp1) <- p inp
                            , (a,inp2) <- q inp1
                            ]
                  )

infix 7 <$>

(<$>)    :: (a -> b) -> Parser a -> Parser b
f <$> p   =   succeed f <*> p

many      ::   Parser a -> Parser [a]
many p    =    (:) <$> p <*> many p 
          <|>  succeed []

token :: [Char] -> Parser [Char]
token = foldr (\x p -> (:) <$> symbol x <*> p ) 
              (succeed []) 

choice :: [Parser a] -> Parser a
choice = foldr1 (<|>)
