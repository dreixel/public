
-- Using code from http://hackage.haskell.org/package/benchpress

module Main where

import System.CPUTime (getCPUTime)
import Data.Time.Clock (getCurrentTime, diffUTCTime, NominalDiffTime)
import System.Environment (getArgs)
import System.Cmd (system)

main :: IO ()
main = do args <- getArgs
          case args of
            [] -> putStrLn "Usage: htime program"
            l  -> run (system (unwords l)) >>= printStats
          return ()

run :: IO a -> IO (Double, Double)
run t = do startWall <- getCurrentTime
           startCpu  <- getCPUTime
           _         <- t
           endCpu    <- getCPUTime
           endWall   <- getCurrentTime
           return ( picosToMillis $! endCpu - startCpu
                  , secsToMillis $! endWall `diffUTCTime` startWall)

printStats :: (Double, Double) -> IO ()
printStats = putStrLn . showStats

showStats :: (Double, Double) -> String
showStats (cpu, wall) = "\nCPU\t" ++ show cpu ++ "\nWall\t" ++ show wall

-- | Converts picoseconds to milliseconds.
picosToMillis :: Integer -> Double
picosToMillis t = realToFrac t / (10^(9 :: Int))

-- | Converts seconds to milliseconds.
secsToMillis :: NominalDiffTime -> Double
secsToMillis t = realToFrac t * (10^(3 :: Int))
