{-# OPTIONS_GHC -fenable-rewrite-rules #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DefaultSignatures #-}

module GEnum where

import Base
import Data.Char (chr)



{-# RULES "map/map1" forall f g l.
      map f (map g l) = map (f . g) l
  #-}
{-# RULES "map/map2" forall f x.
      map f (x:[]) = (f x):[]
  #-}
{-# RULES "diag/sing" forall l.
      diag (l:[]) = l
  #-}

  
-----------------------------------------------------------------------------
-- Utility functions for GEnum'
-----------------------------------------------------------------------------

infixr 5 |||

-- | Interleave elements from two lists. Similar to (++), but swap left and
-- right arguments on every recursive application.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE (|||) #-}
{-# RULES "ft/|||" forall f a b. map f (a ||| b) = map f a ||| map f b #-}
(|||) :: [a] -> [a] -> [a]
[]     ||| ys = ys
(x:xs) ||| ys = x : ys ||| xs


-- | Diagonalization of nested lists. Ensure that some elements from every
-- sublist will be included. Handles infinite sublists.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE diag #-}
{-# RULES "ft/diag" forall f l. map f (diag l) = diag (map (map f) l) #-}
diag :: [[a]] -> [a]
diag = concat . foldr skew [] . map (map (\x -> [x]))

skew :: [[a]] -> [[a]] -> [[a]]
skew []     ys = ys
skew (x:xs) ys = x : combine (++) xs ys

combine :: (a -> a -> a) -> [a] -> [a] -> [a]
combine _ xs     []     = xs
combine _ []     ys     = ys
combine f (x:xs) (y:ys) = f x y : combine f xs ys

--------------------------------------------------------------------------------
-- Generic enum
--------------------------------------------------------------------------------

class GEnum' a where
  genum' :: [a]

instance GEnum' U where
  {-# INLINE genum' #-}
  genum' = [U]

instance (GEnum a) => GEnum' (Rec a) where
  {-# INLINE genum' #-}
  genum' = map Rec genum

instance (GEnum a) => GEnum' (Var a) where
  {-# INLINE genum' #-}
  genum' = map Var genum

instance (GEnum' a) => GEnum' (C c a) where
  {-# INLINE genum' #-}
  genum' = map C genum'

instance (GEnum' f, GEnum' g) => GEnum' (f :+: g) where
  {-# INLINE genum' #-}
  genum' = map L genum' ||| map R genum'

instance (GEnum' f, GEnum' g) => GEnum' (f :*: g) where
  {-# INLINE genum' #-}
  --genum' = diag [ [ x :*: y | y <- genum' ] | x <- genum' ]
  --genum' = diag (map (flip map genum' . (:*:)) genum')
  genum' = diag (map (\x -> map (\y -> x :*: y) genum') genum')
  

class GEnum a where
  genum :: [a]
  {-# INLINE genum #-}
  default genum :: (Representable a, GEnum' (Rep a)) => [a]
  genum = map to genum' --defaultgenum
{-
{-# INLINABLE defaultgenum #-}
defaultgenum :: (Representable a, GEnum' (Rep a)) => [a]
defaultgenum = map to genum'
-}
-- Special cases for our test
instance GEnum Int    where
  {-# INLINE genum #-}
  genum = [0]

instance GEnum [Char] where
  {-# INLINE genum #-}
  genum = ["x"]
