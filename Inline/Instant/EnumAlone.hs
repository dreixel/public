module EnumAlone where


{-# RULES "map/map1" forall f g l.
      map f (map g l) = map (f . g) l
  #-}
{-# RULES "map/map2" forall f x.
      map f (x:[]) = (f x):[]
  #-}
  
--------------------------------------------------------------------------------
-- Utility functions for enumeration
--------------------------------------------------------------------------------

infixr 5 |||

-- | Interleave elements from two lists. Similar to (++), but swap left and
-- right arguments on every recursive application.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE (|||) #-}
{-# RULES "ft |||" forall f a b. map f (a ||| b) = map f a ||| map f b #-}
(|||) :: [a] -> [a] -> [a]
[]     ||| ys = ys
(x:xs) ||| ys = x : ys ||| xs


-- | Diagonalization of nested lists. Ensure that some elements from every
-- sublist will be included. Handles infinite sublists.
--
-- From Mark Jones' talk at AFP2008
{-# NOINLINE diag #-}
{-# RULES "ft diag" forall f l. map f (diag l) = diag (map (map f) l) #-}
diag :: [[a]] -> [a]
diag = concat . foldr skew [] . map (map (\x -> [x]))

skew :: [[a]] -> [[a]] -> [[a]]
skew []     ys = ys
skew (x:xs) ys = x : combine (++) xs ys

combine :: (a -> a -> a) -> [a] -> [a] -> [a]
combine _ xs     []     = xs
combine _ []     ys     = ys
combine f (x:xs) (y:ys) = f x y : combine f xs ys


--------------------------------------------------------------------------------
-- Enumeration on naturals
--------------------------------------------------------------------------------

-- Peano Naturals
data Nat = Ze | Su Nat

-- Convenience only
niceNat :: Nat -> Int
niceNat Ze     = 0
niceNat (Su n) = 1 + niceNat n

instance Show Nat where
  show = show . niceNat
  
instance Eq Nat where
  m == n = niceNat m == niceNat n

-- Enumeration on Nat
enumNat :: [Nat]
enumNat = [Ze] ||| map Su enumNat

-- Peano Naturals as shallow sum-of-products representation
type RepNat = Either () Nat

-- One-layer conversion between RepNat and Nat
{-# INLINE toNat #-}
toNat :: RepNat -> Nat
toNat (Left ()) = Ze
toNat (Right n) = Su n

-- Not used in this example, but anyway
fromNat :: Nat -> RepNat
fromNat Ze = Left ()
fromNat (Su n) = Right n


-- Enumeration of units
{-# INLINE enumU #-}
enumU :: [()]
enumU = [()]

-- Enumeration of sums
{-# INLINE enumEither #-}
enumEither :: [a] -> [b] -> [Either a b]
enumEither ea eb = map Left ea ||| map Right eb

-- Enumeration of products
{-# INLINE enumProd #-}
enumProd :: [a] -> [b] -> [(a,b)]
--enumProd ea eb = diag [ [ (a,b) | a <- ea ] | b <- eb ]
enumProd ea eb = diag (map (\a -> map (\b -> (a,b)) eb) ea)


-- Enumeration of RepNat
{-# INLINE enumRepNat #-}
enumRepNat :: [RepNat]
enumRepNat = enumEither enumU enumNatFromRep

enumNatFromRep :: [Nat]
-- This doesn't work, no matter how much I try to get GHC to inline enumRepNat
--enumNatFromRep = map toNat enumRepNat
-- Whereas this (when I inline it myself) works perfectly
enumNatFromRep = map toNat (enumEither enumU enumNatFromRep)

{-
map toNat enumRepNat 

map toNat (enumEither enumU enumNatFromRep) 

map toNat (map Left enumU ||| map Right enumNatFromRep) 

map toNat (map Left [()] ||| map Right enumNatFromRep)

map toNat ([Left ()] ||| map Right enumNatFromRep)

map toNat [Left ()] ||| map toNat (map Right enumNatFromRep)

[toNat (Left ())] ||| map toNat (map Right enumNatFromRep)

[Ze] ||| map toNat (map Right enumNatFromRep)

[Ze] ||| map (toNat . Right) enumNatFromRep

[Ze] ||| map Su enumNatFromRep
-}


--------------------------------------------------------------------------------
-- Enumeration on lists
--------------------------------------------------------------------------------

-- List as shallow sum-of-products representation
type RepList a = Either () (a,[a])

enumList :: [a] -> [[a]]
--enumList ea = [[]] ||| diag [ [ h : t | h <- ea ] | t <- enumList ea ]
enumList ea = [[]] ||| diag (map (\h -> map (\t -> h:t) (enumList ea)) ea)

-- One-layer conversion between `RepList a` and [a]
{-# INLINE [1] toList #-}
toList :: RepList a -> [a]
toList (Left ())     = []
toList (Right (h,t)) = h:t

enumRepList :: [a] -> [RepList a]
enumRepList ea = enumEither enumU (enumProd ea (enumListFromRep ea))

enumListFromRep :: [a] -> [[a]]
--enumListFromRep ea = map toList (enumRepList ea)
enumListFromRep ea = map toList (enumEither enumU (enumProd ea (enumListFromRep ea)))
