{-# LANGUAGE TypeOperators      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE FlexibleContexts   #-}

module Decode where

import Base

type Bit = Int
type Decoder a = [Bit] -> (a :*: [Bit])

{-# INLINE decodeInt #-}
decodeInt :: Decoder Int
decodeInt []    = error "cannot decode Int"
decodeInt (_:t) = (53 :*: t)

{-# INLINE decodeString #-}
decodeString :: Decoder String
decodeString []    = error "cannot decode String"
decodeString (h:t) = ("x" :*: t)

class Decode a where
  decode' :: [Bit] -> a :*: [Bit]

instance Decode U where
  decode' = decodeU

-- Special case for our test
instance Decode (Var Int) where
  decode' = decodeVarInt

-- Special case for our test
instance Decode (Var String) where
  decode' = decodeVarString

instance (Decode x) => Decode (Rec x) where
  decode' = decodeRec

instance (Decode f, Decode g) => Decode (f :+: g) where
  decode' = decodePlus

instance (Decode f, Decode g) => Decode (f :*: g) where
  decode' = decodeTimes

instance (Decode f) => Decode (C c f) where
  decode' = decodeC


{-# INLINE decodeU #-}
decodeU bs = U :*: bs

{-# INLINE decodeVarInt #-}
decodeVarInt bs = let (i :*: bs') = decodeInt bs in (Var i :*: bs')

{-# INLINE decodeVarString #-}
decodeVarString bs = let (s :*: bs') = decodeString bs in (Var s :*: bs')

{-# INLINE decodeRec #-}
decodeRec bs = let (x :*: bs') = decode' bs in (Rec x :*: bs')

{-# INLINE decodePlus #-}
decodePlus (0:bs) = let (x :*: bs') = decode' bs in (L x :*: bs')
decodePlus (1:bs) = let (x :*: bs') = decode' bs in (R x :*: bs')
decodePlus []     = error "decode': cannot decode"

{-# INLINE decodeTimes #-}
decodeTimes bs0 = let (x :*: bs1) = decode' bs0
                      (y :*: bs2) = decode' bs1
                  in ((x :*: y) :*: bs2)

{-# INLINE decodeC #-}
decodeC bs = let (x :*: bs') = decode' bs in (C x :*: bs')


-- {-# INLINE decode #-}
decode :: (Representable a, Decode (Rep a)) => [Bit] -> (a :*: [Bit])
decode bs = let (x :*: bs') = decode' bs
            in (to x :*: bs')
