Generates eq specialized to the generic representation.

Of interest:

TestEq.t1 = TestEq.eqTree_eq' TestEq.t11 TestEq.t11

Rec {
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.eqTree =
  __inline_me (\ (x_agA :: TestEq.Tree GHC.Types.Int)
                 (y_agB :: TestEq.Tree GHC.Types.Int) ->
                 TestEq.eqTree_eq'
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x_agA)
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any y_agB))
TestEq.eqTree_eq' :: (TestEq.C GHC.Prim.Any TestEq.U
                      TestEq.:+: TestEq.C
                                   GHC.Prim.Any
                                   (TestEq.Var GHC.Types.Int
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
                     -> (TestEq.C GHC.Prim.Any TestEq.U
                         TestEq.:+: TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Var GHC.Types.Int
                                       TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                                   TestEq.:*: TestEq.Rec
                                                                (TestEq.Tree GHC.Types.Int))))
                     -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.eqTree_eq' =
  \ (ds_d180 :: TestEq.C GHC.Prim.Any TestEq.U
                TestEq.:+: TestEq.C
                             GHC.Prim.Any
                             (TestEq.Var GHC.Types.Int
                              TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    (ds1_d181 :: TestEq.C GHC.Prim.Any TestEq.U
                 TestEq.:+: TestEq.C
                              GHC.Prim.Any
                              (TestEq.Var GHC.Types.Int
                               TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                           TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))) ->
    case ds_d180 of _ {
      TestEq.L x_age ->
        case ds1_d181 of _ {
          TestEq.L x'_agf ->
            case x_age of _ { TestEq.C _ ->
            case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds1_d181 of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh ->
            case x_agg of _ { TestEq.C a1_agq ->
            case x'_agh of _ { TestEq.C a'_agr ->
            case a1_agq of _ { TestEq.:*: a2_agi b_agj ->
            case a'_agr of _ { TestEq.:*: a'1_agk b'_agl ->
            case a2_agi of _ { TestEq.Var x1_ags ->
            case a'1_agk of _ { TestEq.Var x'1_agt ->
            case x1_ags of _ { GHC.Types.I# x2_a1ya ->
            case x'1_agt of _ { GHC.Types.I# y_a1ye ->
            case GHC.Prim.==# x2_a1ya y_a1ye of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True ->
                case b_agj of _ { TestEq.:*: a3_Xp9 b1_Xpb ->
                case b'_agl of _ { TestEq.:*: a'2_Xpf b'1_Xph ->
                case a3_Xp9 of _ { TestEq.Rec x3_agu ->
                case a'2_Xpf of _ { TestEq.Rec x'2_agv ->
                case TestEq.eqTree_eq'
                       (TestEq.fromTree
                          @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x3_agu)
                       (TestEq.fromTree
                          @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x'2_agv)
                of _ {
                  GHC.Bool.False -> GHC.Bool.False;
                  GHC.Bool.True ->
                    case b1_Xpb of _ { TestEq.Rec x4_XrS ->
                    case b'1_Xph of _ { TestEq.Rec x'3_XrW ->
                    TestEq.eqTree x4_XrS x'3_XrW
                    }
                    }
                }
                }
                }
                }
                }
            }
            }
            }
            }
            }
            }
            }
            }
            }
        }
    }
end Rec }


==================== Specialise ====================
Rec {
TestEq.$fConstructorList_Cons_ :: TestEq.Constructor
                                    TestEq.List_Cons_
LclIdX[DFunId]
[]
TestEq.$fConstructorList_Cons_ =
  TestEq.D:Constructor
    @ TestEq.List_Cons_ conName_a144 conFixity_a14a a_s1ej
a_s1ej [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1ej =
  TestEq.$dmconIsRecord
    @ TestEq.List_Cons_ TestEq.$fConstructorList_Cons_
conName_a144 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Cons_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a144 =
  \ (@ t_a141::* -> * -> *) (@ a_a142) _ ->
    GHC.Types.:
      @ GHC.Types.Char (GHC.Types.C# ':') (GHC.Types.[] @ GHC.Types.Char)
conFixity_a14a :: forall (t_afB :: * -> * -> *) a_afC.
                  t_afB TestEq.List_Cons_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
conFixity_a14a =
  \ (@ t_a147::* -> * -> *) (@ a_a148) _ ->
    TestEq.Infix TestEq.RightAssociative (GHC.Types.I# 5)
TestEq.$fConstructorLogic_F_ :: TestEq.Constructor TestEq.Logic_F_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_F_ =
  TestEq.D:Constructor @ TestEq.Logic_F_ conName_a123 a_s1eX a_s1eZ
a_s1eZ [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eZ =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_F_ TestEq.$fConstructorLogic_F_
a_s1eX [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eX =
  TestEq.$dmconFixity @ TestEq.Logic_F_ TestEq.$fConstructorLogic_F_
conName_a123 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_F_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a123 =
  \ (@ t_a120::* -> * -> *) (@ a_a121) _ ->
    GHC.Types.:
      @ GHC.Types.Char (GHC.Types.C# 'F') (GHC.Types.[] @ GHC.Types.Char)
TestEq.$fConstructorLogic_T_ :: TestEq.Constructor TestEq.Logic_T_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_T_ =
  TestEq.D:Constructor @ TestEq.Logic_T_ conName_a12e a_s1eT a_s1eV
a_s1eV [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eV =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_T_ TestEq.$fConstructorLogic_T_
a_s1eT [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eT =
  TestEq.$dmconFixity @ TestEq.Logic_T_ TestEq.$fConstructorLogic_T_
conName_a12e :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_T_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12e =
  \ (@ t_a12b::* -> * -> *) (@ a_a12c) _ ->
    GHC.Types.:
      @ GHC.Types.Char (GHC.Types.C# 'T') (GHC.Types.[] @ GHC.Types.Char)
TestEq.$fConstructorLogic_Not_ :: TestEq.Constructor
                                    TestEq.Logic_Not_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Not_ =
  TestEq.D:Constructor @ TestEq.Logic_Not_ conName_a12p a_s1eP a_s1eR
a_s1eR [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eR =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Not_ TestEq.$fConstructorLogic_Not_
a_s1eP [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eP =
  TestEq.$dmconFixity
    @ TestEq.Logic_Not_ TestEq.$fConstructorLogic_Not_
conName_a12p :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Not_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12p =
  \ (@ t_a12m::* -> * -> *) (@ a_a12n) _ ->
    GHC.Base.unpackCString# "Not"
TestEq.$fConstructorLogic_Disj_ :: TestEq.Constructor
                                     TestEq.Logic_Disj_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Disj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Disj_ conName_a12A a_s1eL a_s1eN
a_s1eN [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eN =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Disj_ TestEq.$fConstructorLogic_Disj_
a_s1eL [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eL =
  TestEq.$dmconFixity
    @ TestEq.Logic_Disj_ TestEq.$fConstructorLogic_Disj_
conName_a12A :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Disj_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12A =
  \ (@ t_a12x::* -> * -> *) (@ a_a12y) _ ->
    GHC.Base.unpackCString# "Disj"
TestEq.$fConstructorLogic_Conj_ :: TestEq.Constructor
                                     TestEq.Logic_Conj_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Conj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Conj_ conName_a12L a_s1eH a_s1eJ
a_s1eJ [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eJ =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Conj_ TestEq.$fConstructorLogic_Conj_
a_s1eH [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eH =
  TestEq.$dmconFixity
    @ TestEq.Logic_Conj_ TestEq.$fConstructorLogic_Conj_
conName_a12L :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Conj_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12L =
  \ (@ t_a12I::* -> * -> *) (@ a_a12J) _ ->
    GHC.Base.unpackCString# "Conj"
TestEq.$fConstructorLogic_Equiv_ :: TestEq.Constructor
                                      TestEq.Logic_Equiv_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Equiv_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Equiv_ conName_a12W a_s1eD a_s1eF
a_s1eF [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eF =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Equiv_ TestEq.$fConstructorLogic_Equiv_
a_s1eD [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eD =
  TestEq.$dmconFixity
    @ TestEq.Logic_Equiv_ TestEq.$fConstructorLogic_Equiv_
conName_a12W :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Equiv_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12W =
  \ (@ t_a12T::* -> * -> *) (@ a_a12U) _ ->
    GHC.Base.unpackCString# "Equiv"
TestEq.$fConstructorLogic_Impl_ :: TestEq.Constructor
                                     TestEq.Logic_Impl_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Impl_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Impl_ conName_a137 a_s1ez a_s1eB
a_s1eB [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eB =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Impl_ TestEq.$fConstructorLogic_Impl_
a_s1ez [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ez =
  TestEq.$dmconFixity
    @ TestEq.Logic_Impl_ TestEq.$fConstructorLogic_Impl_
conName_a137 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Impl_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a137 =
  \ (@ t_a134::* -> * -> *) (@ a_a135) _ ->
    GHC.Base.unpackCString# "Impl"
TestEq.$fConstructorLogic_Var_ :: TestEq.Constructor
                                    TestEq.Logic_Var_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Var_ =
  TestEq.D:Constructor @ TestEq.Logic_Var_ conName_a13i a_s1ev a_s1ex
a_s1ex [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1ex =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Var_ TestEq.$fConstructorLogic_Var_
a_s1ev [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ev =
  TestEq.$dmconFixity
    @ TestEq.Logic_Var_ TestEq.$fConstructorLogic_Var_
conName_a13i :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Var_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a13i =
  \ (@ t_a13f::* -> * -> *) (@ a_a13g) _ ->
    GHC.Base.unpackCString# "VarL"
TestEq.$fConstructorLeaf :: TestEq.Constructor TestEq.Leaf
LclIdX[DFunId]
[]
TestEq.$fConstructorLeaf =
  TestEq.D:Constructor @ TestEq.Leaf conName_a13I a_s1ep a_s1er
a_s1er [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1er =
  TestEq.$dmconIsRecord @ TestEq.Leaf TestEq.$fConstructorLeaf
a_s1ep [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ep = TestEq.$dmconFixity @ TestEq.Leaf TestEq.$fConstructorLeaf
conName_a13I :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Leaf a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a13I =
  \ (@ t_a13F::* -> * -> *) (@ a_a13G) _ ->
    GHC.Base.unpackCString# "Leaf"
TestEq.$fConstructorBin :: TestEq.Constructor TestEq.Bin
LclIdX[DFunId]
[]
TestEq.$fConstructorBin =
  TestEq.D:Constructor @ TestEq.Bin conName_a13T a_s1el a_s1en
a_s1en [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1en = TestEq.$dmconIsRecord @ TestEq.Bin TestEq.$fConstructorBin
a_s1el [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1el = TestEq.$dmconFixity @ TestEq.Bin TestEq.$fConstructorBin
conName_a13T :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Bin a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a13T =
  \ (@ t_a13Q::* -> * -> *) (@ a_a13R) _ ->
    GHC.Base.unpackCString# "Bin"
TestEq.$fConstructorList_Nil_ :: TestEq.Constructor
                                   TestEq.List_Nil_
LclIdX[DFunId]
[]
TestEq.$fConstructorList_Nil_ =
  TestEq.D:Constructor @ TestEq.List_Nil_ conName_a14j a_s1ef a_s1eh
a_s1eh [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eh =
  TestEq.$dmconIsRecord
    @ TestEq.List_Nil_ TestEq.$fConstructorList_Nil_
a_s1ef [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ef =
  TestEq.$dmconFixity
    @ TestEq.List_Nil_ TestEq.$fConstructorList_Nil_
conName_a14j :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Nil_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a14j =
  \ (@ t_a14g::* -> * -> *) (@ a_a14h) _ ->
    GHC.Base.unpackCString# "[]"
$s$dmconFixity_s1po :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1po =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_F_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_F_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pn :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pn =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_T_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_T_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pm :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pm =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Not_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Not_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pl :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pl =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Disj_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Disj_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pk :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pk =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Conj_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Conj_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pj :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pj =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Equiv_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Equiv_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pi :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pi =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Impl_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Impl_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1ph :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1ph =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Var_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Var_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pg :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pg =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Leaf a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity @ (t_aFL TestEq.Leaf a_aFM) TestEq.Prefix eta_B1
$s$dmconFixity_s1pf :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pf =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Bin a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity @ (t_aFL TestEq.Bin a_aFM) TestEq.Prefix eta_B1
$s$dmconFixity_s1pe :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pe =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.List_Nil_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.List_Nil_ a_aFM)
      TestEq.Prefix
      eta_B1
TestEq.$dmconFixity :: forall c_afy.
                       (TestEq.Constructor c_afy) =>
                       forall (t_afB :: * -> * -> *) a_afC.
                       t_afB c_afy a_afC -> TestEq.Fixity
LclIdX
[Arity 2
 RULES: "SPEC TestEq.$dmconFixity [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1o8 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconFixity @ TestEq.Logic_F_ $dConstructor_s1o8
              = $s$dmconFixity_s1po
        "SPEC TestEq.$dmconFixity [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1ob [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconFixity @ TestEq.Logic_T_ $dConstructor_s1ob
              = $s$dmconFixity_s1pn
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oe [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconFixity @ TestEq.Logic_Not_ $dConstructor_s1oe
              = $s$dmconFixity_s1pm
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oh [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconFixity @ TestEq.Logic_Disj_ $dConstructor_s1oh
              = $s$dmconFixity_s1pl
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1ok [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconFixity @ TestEq.Logic_Conj_ $dConstructor_s1ok
              = $s$dmconFixity_s1pk
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1on [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconFixity @ TestEq.Logic_Equiv_ $dConstructor_s1on
              = $s$dmconFixity_s1pj
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oq [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconFixity @ TestEq.Logic_Impl_ $dConstructor_s1oq
              = $s$dmconFixity_s1pi
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1ot [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconFixity @ TestEq.Logic_Var_ $dConstructor_s1ot
              = $s$dmconFixity_s1ph
        "SPEC TestEq.$dmconFixity [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1ow [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconFixity @ TestEq.Leaf $dConstructor_s1ow
              = $s$dmconFixity_s1pg
        "SPEC TestEq.$dmconFixity [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1oz [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconFixity @ TestEq.Bin $dConstructor_s1oz
              = $s$dmconFixity_s1pf
        "SPEC TestEq.$dmconFixity [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1oC [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconFixity @ TestEq.List_Nil_ $dConstructor_s1oC
              = $s$dmconFixity_s1pe]
TestEq.$dmconFixity =
  \ (@ c_afy)
    _
    (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL c_afy a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity @ (t_aFL c_afy a_aFM) TestEq.Prefix eta_B1
$s$dmconIsRecord_s1pp :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pp =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_F_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_F_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pq :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pq =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_T_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_T_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pr :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pr =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Not_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Not_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1ps :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1ps =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Disj_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Disj_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pt :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pt =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Conj_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Conj_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pu :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pu =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Equiv_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Equiv_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pv :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pv =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Impl_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Impl_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pw :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pw =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Var_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Var_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1px :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1px =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Leaf a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool @ (t_aFS TestEq.Leaf a_aFT) GHC.Bool.False eta_B1
$s$dmconIsRecord_s1py :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1py =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Bin a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool @ (t_aFS TestEq.Bin a_aFT) GHC.Bool.False eta_B1
$s$dmconIsRecord_s1pz :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pz =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.List_Cons_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.List_Cons_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pA :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pA =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.List_Nil_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.List_Nil_ a_aFT)
      GHC.Bool.False
      eta_B1
TestEq.$dmconIsRecord :: forall c_afy.
                         (TestEq.Constructor c_afy) =>
                         forall (t_afD :: * -> * -> *) a_afE.
                         t_afD c_afy a_afE -> GHC.Bool.Bool
LclIdX
[Arity 2
 RULES: "SPEC TestEq.$dmconIsRecord [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1oF [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconIsRecord @ TestEq.Logic_F_ $dConstructor_s1oF
              = $s$dmconIsRecord_s1pp
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1oI [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconIsRecord @ TestEq.Logic_T_ $dConstructor_s1oI
              = $s$dmconIsRecord_s1pq
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oL [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Not_ $dConstructor_s1oL
              = $s$dmconIsRecord_s1pr
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oO [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Disj_ $dConstructor_s1oO
              = $s$dmconIsRecord_s1ps
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1oR [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Conj_ $dConstructor_s1oR
              = $s$dmconIsRecord_s1pt
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1oU [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Equiv_ $dConstructor_s1oU
              = $s$dmconIsRecord_s1pu
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oX [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Impl_ $dConstructor_s1oX
              = $s$dmconIsRecord_s1pv
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1p0 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Var_ $dConstructor_s1p0
              = $s$dmconIsRecord_s1pw
        "SPEC TestEq.$dmconIsRecord [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1p3 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconIsRecord @ TestEq.Leaf $dConstructor_s1p3
              = $s$dmconIsRecord_s1px
        "SPEC TestEq.$dmconIsRecord [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1p6 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconIsRecord @ TestEq.Bin $dConstructor_s1p6
              = $s$dmconIsRecord_s1py
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Cons_]" ALWAYS
            forall {$dConstructor_s1p9 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Cons_}
              TestEq.$dmconIsRecord @ TestEq.List_Cons_ $dConstructor_s1p9
              = $s$dmconIsRecord_s1pz
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1pc [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconIsRecord @ TestEq.List_Nil_ $dConstructor_s1pc
              = $s$dmconIsRecord_s1pA]
TestEq.$dmconIsRecord =
  \ (@ c_afy)
    _
    (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS c_afy a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool @ (t_aFS c_afy a_aFT) GHC.Bool.False eta_B1
end Rec }

TestEq.$con2tag_Tree :: forall t_az6.
                        TestEq.Tree t_az6 -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Tree =
  \ (@ t_az6) (ds_d19M :: TestEq.Tree t_az6) ->
    case ds_d19M of _ { TestEq.Leaf -> 0; TestEq.Bin _ _ _ -> 1 }

TestEq.$con2tag_Logic :: TestEq.Logic -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Logic =
  \ (ds_d19S :: TestEq.Logic) ->
    case ds_d19S of _ {
      TestEq.VarL _ -> 0;
      TestEq.T -> 1;
      TestEq.F -> 2;
      TestEq.Not _ -> 3;
      TestEq.Impl _ _ -> 4;
      TestEq.Equiv _ _ -> 5;
      TestEq.Conj _ _ -> 6;
      TestEq.Disj _ _ -> 7
    }

TestEq.$con2tag_Associativity :: TestEq.Associativity
                                 -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Associativity =
  \ (ds_d1ab :: TestEq.Associativity) ->
    case ds_d1ab of _ {
      TestEq.LeftAssociative -> 0;
      TestEq.RightAssociative -> 1;
      TestEq.NotAssociative -> 2
    }

TestEq.$con2tag_Fixity :: TestEq.Fixity -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Fixity =
  \ (ds_d1af :: TestEq.Fixity) ->
    case ds_d1af of _ { TestEq.Prefix -> 0; TestEq.Infix _ _ -> 1 }

TestEq.eq :: forall a_afN.
             (TestEq.Representable a_afN, TestEq.Eq (TestEq.Rep a_afN)) =>
             a_afN -> a_afN -> GHC.Bool.Bool
LclIdX
[Arity 4]
TestEq.eq =
  __inline_me (\ (@ a_azl)
                 ($dRepresentable_azt :: TestEq.Representable a_azl)
                 ($dEq_azu :: TestEq.Eq (TestEq.Rep a_azl))
                 (eta_B2 :: a_azl)
                 (eta_B1 :: a_azl) ->
                 let {
                   from_s1o6 :: a_azl -> TestEq.Rep a_azl
                   LclId
                   []
                   from_s1o6 = TestEq.from @ a_azl $dRepresentable_azt } in
                 TestEq.eq'
                   @ (TestEq.Rep a_azl)
                   $dEq_azu
                   (from_s1o6 eta_B2)
                   (from_s1o6 eta_B1))

TestEq.eqRec :: forall t_azF.
                (TestEq.Eq t_azF) =>
                TestEq.Rec t_azF -> TestEq.Rec t_azF -> GHC.Bool.Bool
LclIdX
[Arity 3]
TestEq.eqRec =
  __inline_me (\ (@ t_azF)
                 ($dEq_azL :: TestEq.Eq t_azF)
                 (ds_d17K :: TestEq.Rec t_azF)
                 (ds_d17L :: TestEq.Rec t_azF) ->
                 case ds_d17K of _ { TestEq.Rec x_agu ->
                 case ds_d17L of _ { TestEq.Rec x'_agv ->
                 TestEq.eq' @ t_azF $dEq_azL x_agu x'_agv
                 }
                 })

TestEq.eqVar :: forall t_azR.
                (TestEq.Eq t_azR) =>
                TestEq.Var t_azR -> TestEq.Var t_azR -> GHC.Bool.Bool
LclIdX
[Arity 3]
TestEq.eqVar =
  __inline_me (\ (@ t_azR)
                 ($dEq_azX :: TestEq.Eq t_azR)
                 (ds_d17O :: TestEq.Var t_azR)
                 (ds_d17P :: TestEq.Var t_azR) ->
                 case ds_d17O of _ { TestEq.Var x_ags ->
                 case ds_d17P of _ { TestEq.Var x'_agt ->
                 TestEq.eq' @ t_azR $dEq_azX x_ags x'_agt
                 }
                 })

TestEq.eqC :: forall t_aA2 t_aA4 t_aA5.
              (TestEq.Eq t_aA4) =>
              TestEq.C t_aA2 t_aA4 -> TestEq.C t_aA5 t_aA4 -> GHC.Bool.Bool
LclIdX
[Arity 3]
TestEq.eqC =
  __inline_me (\ (@ t_aA2)
                 (@ t_aA4)
                 (@ t_aA5)
                 ($dEq_aAb :: TestEq.Eq t_aA4)
                 (ds_d17S :: TestEq.C t_aA2 t_aA4)
                 (ds_d17T :: TestEq.C t_aA5 t_aA4) ->
                 case ds_d17S of _ { TestEq.C a_agq ->
                 case ds_d17T of _ { TestEq.C a'_agr ->
                 TestEq.eq' @ t_aA4 $dEq_aAb a_agq a'_agr
                 }
                 })

TestEq.eqTimes :: forall t_aAi t_aAj.
                  (TestEq.Eq t_aAi, TestEq.Eq t_aAj) =>
                  (t_aAi TestEq.:*: t_aAj)
                  -> (t_aAi TestEq.:*: t_aAj)
                  -> GHC.Bool.Bool
LclIdX
[Arity 4]
TestEq.eqTimes =
  __inline_me (\ (@ t_aAi)
                 (@ t_aAj)
                 ($dEq_aAt :: TestEq.Eq t_aAi)
                 ($dEq_aAu :: TestEq.Eq t_aAj)
                 (ds_d17W :: t_aAi TestEq.:*: t_aAj)
                 (ds_d17X :: t_aAi TestEq.:*: t_aAj) ->
                 case ds_d17W of _ { TestEq.:*: a_agi b_agj ->
                 case ds_d17X of _ { TestEq.:*: a'_agk b'_agl ->
                 GHC.Classes.&&
                   (TestEq.eq' @ t_aAi $dEq_aAt a_agi a'_agk)
                   (TestEq.eq' @ t_aAj $dEq_aAu b_agj b'_agl)
                 }
                 })

TestEq.eqPlus :: forall t_aAB t_aAH.
                 (TestEq.Eq t_aAB, TestEq.Eq t_aAH) =>
                 (t_aAB TestEq.:+: t_aAH)
                 -> (t_aAB TestEq.:+: t_aAH)
                 -> GHC.Bool.Bool
LclIdX
[Arity 4]
TestEq.eqPlus =
  __inline_me (\ (@ t_aAB)
                 (@ t_aAH)
                 ($dEq_aAM :: TestEq.Eq t_aAB)
                 ($dEq_aAN :: TestEq.Eq t_aAH)
                 (ds_d180 :: t_aAB TestEq.:+: t_aAH)
                 (ds_d181 :: t_aAB TestEq.:+: t_aAH) ->
                 case ds_d180 of _ {
                   TestEq.L x_age ->
                     case ds_d181 of _ {
                       TestEq.L x'_agf -> TestEq.eq' @ t_aAB $dEq_aAM x_age x'_agf;
                       TestEq.R _ -> GHC.Bool.False
                     };
                   TestEq.R x_agg ->
                     case ds_d181 of _ {
                       TestEq.L _ -> GHC.Bool.False;
                       TestEq.R x'_agh -> TestEq.eq' @ t_aAH $dEq_aAN x_agg x'_agh
                     }
                 })

TestEq.eqU :: forall t_aAS t_aAT. t_aAS -> t_aAT -> GHC.Bool.Bool
LclIdX
[Arity 2]
TestEq.eqU =
  __inline_me (\ (@ t_aAS) (@ t_aAT) _ _ -> GHC.Bool.True)

TestEq.toLogic :: forall t_aB0
                         t_aB6
                         t_aBa
                         t_aBe
                         t_aBk
                         t_aBu
                         t_aBE
                         t_aBM.
                  (TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                   TestEq.:+: (TestEq.C t_aB6 TestEq.U
                               TestEq.:+: (TestEq.C t_aBa TestEq.U
                                           TestEq.:+: (TestEq.C t_aBe (TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     t_aBk
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 t_aBu
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: (TestEq.C
                                                                                             t_aBE
                                                                                             (TestEq.Rec
                                                                                                TestEq.Logic
                                                                                              TestEq.:*: TestEq.Rec
                                                                                                           TestEq.Logic)
                                                                                           TestEq.:+: TestEq.C
                                                                                                        t_aBM
                                                                                                        (TestEq.Rec
                                                                                                           TestEq.Logic
                                                                                                         TestEq.:*: TestEq.Rec
                                                                                                                      TestEq.Logic))))))))
                  -> TestEq.Logic
LclIdX
[Arity 1]
TestEq.toLogic =
  __inline_me (\ (@ t_aB0)
                 (@ t_aB6)
                 (@ t_aBa)
                 (@ t_aBe)
                 (@ t_aBk)
                 (@ t_aBu)
                 (@ t_aBE)
                 (@ t_aBM)
                 (ds_d18a :: TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                             TestEq.:+: (TestEq.C t_aB6 TestEq.U
                                         TestEq.:+: (TestEq.C t_aBa TestEq.U
                                                     TestEq.:+: (TestEq.C
                                                                   t_aBe (TestEq.Rec TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               t_aBk
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: (TestEq.C
                                                                                           t_aBu
                                                                                           (TestEq.Rec
                                                                                              TestEq.Logic
                                                                                            TestEq.:*: TestEq.Rec
                                                                                                         TestEq.Logic)
                                                                                         TestEq.:+: (TestEq.C
                                                                                                       t_aBE
                                                                                                       (TestEq.Rec
                                                                                                          TestEq.Logic
                                                                                                        TestEq.:*: TestEq.Rec
                                                                                                                     TestEq.Logic)
                                                                                                     TestEq.:+: TestEq.C
                                                                                                                  t_aBM
                                                                                                                  (TestEq.Rec
                                                                                                                     TestEq.Logic
                                                                                                                   TestEq.:*: TestEq.Rec
                                                                                                                                TestEq.Logic)))))))) ->
                 case ds_d18a of _ {
                   TestEq.L ds_d18D ->
                     case ds_d18D of _ { TestEq.C ds_d18E ->
                     case ds_d18E of _ { TestEq.Var f0_ag4 -> TestEq.VarL f0_ag4 }
                     };
                   TestEq.R ds_d18b ->
                     case ds_d18b of _ {
                       TestEq.L ds_d18B ->
                         case ds_d18B of _ { TestEq.C ds_d18C ->
                         case ds_d18C of _ { TestEq.U -> TestEq.T }
                         };
                       TestEq.R ds_d18c ->
                         case ds_d18c of _ {
                           TestEq.L ds_d18z ->
                             case ds_d18z of _ { TestEq.C ds_d18A ->
                             case ds_d18A of _ { TestEq.U -> TestEq.F }
                             };
                           TestEq.R ds_d18d ->
                             case ds_d18d of _ {
                               TestEq.L ds_d18x ->
                                 case ds_d18x of _ { TestEq.C ds_d18y ->
                                 case ds_d18y of _ { TestEq.Rec f0_ag5 -> TestEq.Not f0_ag5 }
                                 };
                               TestEq.R ds_d18e ->
                                 case ds_d18e of _ {
                                   TestEq.L ds_d18t ->
                                     case ds_d18t of _ { TestEq.C ds_d18u ->
                                     case ds_d18u of _ { TestEq.:*: ds_d18v ds_d18w ->
                                     case ds_d18v of _ { TestEq.Rec f0_ag6 ->
                                     case ds_d18w of _ { TestEq.Rec f1_ag7 ->
                                     TestEq.Impl f0_ag6 f1_ag7
                                     }
                                     }
                                     }
                                     };
                                   TestEq.R ds_d18f ->
                                     case ds_d18f of _ {
                                       TestEq.L ds_d18p ->
                                         case ds_d18p of _ { TestEq.C ds_d18q ->
                                         case ds_d18q of _ { TestEq.:*: ds_d18r ds_d18s ->
                                         case ds_d18r of _ { TestEq.Rec f0_ag8 ->
                                         case ds_d18s of _ { TestEq.Rec f1_ag9 ->
                                         TestEq.Equiv f0_ag8 f1_ag9
                                         }
                                         }
                                         }
                                         };
                                       TestEq.R ds_d18g ->
                                         case ds_d18g of _ {
                                           TestEq.L ds_d18l ->
                                             case ds_d18l of _ { TestEq.C ds_d18m ->
                                             case ds_d18m of _ { TestEq.:*: ds_d18n ds_d18o ->
                                             case ds_d18n of _ { TestEq.Rec f0_aga ->
                                             case ds_d18o of _ { TestEq.Rec f1_agb ->
                                             TestEq.Conj f0_aga f1_agb
                                             }
                                             }
                                             }
                                             };
                                           TestEq.R ds_d18h ->
                                             case ds_d18h of _ { TestEq.C ds_d18i ->
                                             case ds_d18i of _ { TestEq.:*: ds_d18j ds_d18k ->
                                             case ds_d18j of _ { TestEq.Rec f0_agc ->
                                             case ds_d18k of _ { TestEq.Rec f1_agd ->
                                             TestEq.Disj f0_agc f1_agd
                                             }
                                             }
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 })

TestEq.fromLogic :: forall c_aC3
                           c_aCb
                           c_aCl
                           c_aCz
                           c_aCT
                           c_aDg
                           c_aDG
                           c_aE7.
                    TestEq.Logic
                    -> TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C c_aCb TestEq.U
                                   TestEq.:+: (TestEq.C c_aCl TestEq.U
                                               TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         c_aCT
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     c_aDg
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 c_aDG
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            c_aE7
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic)))))))
LclIdX
[Arity 1]
TestEq.fromLogic =
  __inline_me (\ (@ c_aC3)
                 (@ c_aCb)
                 (@ c_aCl)
                 (@ c_aCz)
                 (@ c_aCT)
                 (@ c_aDg)
                 (@ c_aDG)
                 (@ c_aE7)
                 (ds_d19h :: TestEq.Logic) ->
                 case ds_d19h of _ {
                   TestEq.VarL f0_afU ->
                     TestEq.L
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.C
                          @ c_aC3
                          @ (TestEq.Var GHC.Base.String)
                          (TestEq.Var @ GHC.Base.String f0_afU));
                   TestEq.T ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.L
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.C @ c_aCb @ TestEq.U TestEq.U));
                   TestEq.F ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.L
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.C @ c_aCl @ TestEq.U TestEq.U)));
                   TestEq.Not f0_afV ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.L
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.C
                                   @ c_aCz
                                   @ (TestEq.Rec TestEq.Logic)
                                   (TestEq.Rec @ TestEq.Logic f0_afV)))));
                   TestEq.Impl f0_afW f1_afX ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.L
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.C
                                      @ c_aCT
                                      @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      (TestEq.:*:
                                         @ (TestEq.Rec TestEq.Logic)
                                         @ (TestEq.Rec TestEq.Logic)
                                         (TestEq.Rec @ TestEq.Logic f0_afW)
                                         (TestEq.Rec @ TestEq.Logic f1_afX)))))));
                   TestEq.Equiv f0_afY f1_afZ ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.L
                                      @ (TestEq.C
                                           c_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.C
                                         @ c_aDg
                                         @ (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         (TestEq.:*:
                                            @ (TestEq.Rec TestEq.Logic)
                                            @ (TestEq.Rec TestEq.Logic)
                                            (TestEq.Rec @ TestEq.Logic f0_afY)
                                            (TestEq.Rec @ TestEq.Logic f1_afZ))))))));
                   TestEq.Conj f0_ag0 f1_ag1 ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.R
                                      @ (TestEq.C
                                           c_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.L
                                         @ (TestEq.C
                                              c_aDG
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         @ (TestEq.C
                                              c_aE7
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         (TestEq.C
                                            @ c_aDG
                                            @ (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            (TestEq.:*:
                                               @ (TestEq.Rec TestEq.Logic)
                                               @ (TestEq.Rec TestEq.Logic)
                                               (TestEq.Rec @ TestEq.Logic f0_ag0)
                                               (TestEq.Rec @ TestEq.Logic f1_ag1)))))))));
                   TestEq.Disj f0_ag2 f1_ag3 ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.R
                                      @ (TestEq.C
                                           c_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.R
                                         @ (TestEq.C
                                              c_aDG
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         @ (TestEq.C
                                              c_aE7
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         (TestEq.C
                                            @ c_aE7
                                            @ (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            (TestEq.:*:
                                               @ (TestEq.Rec TestEq.Logic)
                                               @ (TestEq.Rec TestEq.Logic)
                                               (TestEq.Rec @ TestEq.Logic f0_ag2)
                                               (TestEq.Rec @ TestEq.Logic f1_ag3)))))))))
                 })

TestEq.toTree :: forall t_aEI t_aEN t_aEV.
                 (TestEq.C t_aEV TestEq.U
                  TestEq.:+: TestEq.C
                               t_aEI
                               (TestEq.Var t_aEN
                                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aEN))))
                 -> TestEq.Tree t_aEN
LclIdX
[Arity 1]
TestEq.toTree =
  __inline_me (\ (@ t_aEI)
                 (@ t_aEN)
                 (@ t_aEV)
                 (ds_d19q :: TestEq.C t_aEV TestEq.U
                             TestEq.:+: TestEq.C
                                          t_aEI
                                          (TestEq.Var t_aEN
                                           TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                                       TestEq.:*: TestEq.Rec
                                                                    (TestEq.Tree t_aEN)))) ->
                 case ds_d19q of _ {
                   TestEq.L ds_d19x ->
                     case ds_d19x of _ { TestEq.C ds_d19y ->
                     case ds_d19y of _ { TestEq.U -> TestEq.Leaf @ t_aEN }
                     };
                   TestEq.R ds_d19r ->
                     case ds_d19r of _ { TestEq.C ds_d19s ->
                     case ds_d19s of _ { TestEq.:*: ds_d19t ds_d19u ->
                     case ds_d19t of _ { TestEq.Var x_afR ->
                     case ds_d19u of _ { TestEq.:*: ds_d19v ds_d19w ->
                     case ds_d19v of _ { TestEq.Rec l_afS ->
                     case ds_d19w of _ { TestEq.Rec r_afT ->
                     TestEq.Bin @ t_aEN x_afR l_afS r_afT
                     }
                     }
                     }
                     }
                     }
                     }
                 })

TestEq.fromTree :: forall t_aF3 c_aFf c_aFl.
                   TestEq.Tree t_aF3
                   -> TestEq.C c_aFl TestEq.U
                      TestEq.:+: TestEq.C
                                   c_aFf
                                   (TestEq.Var t_aF3
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
LclIdX
[Arity 1]
TestEq.fromTree =
  __inline_me (\ (@ t_aF3)
                 (@ c_aFf)
                 (@ c_aFl)
                 (ds_d19J :: TestEq.Tree t_aF3) ->
                 case ds_d19J of _ {
                   TestEq.Leaf ->
                     TestEq.L
                       @ (TestEq.C c_aFl TestEq.U)
                       @ (TestEq.C
                            c_aFf
                            (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
                       (TestEq.C @ c_aFl @ TestEq.U TestEq.U);
                   TestEq.Bin x_afO l_afP r_afQ ->
                     TestEq.R
                       @ (TestEq.C c_aFl TestEq.U)
                       @ (TestEq.C
                            c_aFf
                            (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
                       (TestEq.C
                          @ c_aFf
                          @ (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
                          (TestEq.:*:
                             @ (TestEq.Var t_aF3)
                             @ (TestEq.Rec (TestEq.Tree t_aF3)
                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))
                             (TestEq.Var @ t_aF3 x_afO)
                             (TestEq.:*:
                                @ (TestEq.Rec (TestEq.Tree t_aF3))
                                @ (TestEq.Rec (TestEq.Tree t_aF3))
                                (TestEq.Rec @ (TestEq.Tree t_aF3) l_afP)
                                (TestEq.Rec @ (TestEq.Tree t_aF3) r_afQ))))
                 })

a_s1dD :: GHC.Types.Int
LclId
[]
a_s1dD = GHC.Types.I# 3

TestEq.tree0 :: TestEq.Tree GHC.Types.Int
LclIdX
[]
TestEq.tree0 =
  TestEq.Bin
    @ GHC.Types.Int
    a_s1dD
    (TestEq.Leaf @ GHC.Types.Int)
    (TestEq.Leaf @ GHC.Types.Int)

a_s1dF :: GHC.Types.Int
LclId
[]
a_s1dF = GHC.Types.I# 5

TestEq.tree1 :: TestEq.Tree GHC.Types.Int
LclIdX
[]
TestEq.tree1 =
  TestEq.Bin @ GHC.Types.Int a_s1dF TestEq.tree0 TestEq.tree0

a_s1dK :: GHC.Types.Char
LclId
[]
a_s1dK = GHC.Types.C# 'x'

a_s1dL :: [GHC.Types.Char]
LclId
[]
a_s1dL =
  GHC.Types.: @ GHC.Types.Char a_s1dK (GHC.Types.[] @ GHC.Types.Char)

a_s1dM :: TestEq.Logic
LclId
[]
a_s1dM = TestEq.VarL a_s1dL

a_s1dO :: TestEq.Logic
LclId
[]
a_s1dO = TestEq.Not TestEq.T

a_s1dP :: TestEq.Logic
LclId
[]
a_s1dP = TestEq.Equiv a_s1dM a_s1dO

a_s1dR :: TestEq.Logic
LclId
[]
a_s1dR = TestEq.Not TestEq.F

TestEq.logic1 :: TestEq.Logic
LclIdX
[]
TestEq.logic1 = TestEq.Impl a_s1dP a_s1dR

TestEq.$fRepresentableInt :: TestEq.Representable GHC.Types.Int
LclIdX[DFunId]
[]
TestEq.$fRepresentableInt =
  TestEq.D:Representable
    @ GHC.Types.Int
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Int))
     `cast` (TestEq.Rep GHC.Types.Int -> TestEq.TFCo:R:RepInt
             :: (TestEq.Rep GHC.Types.Int -> TestEq.Rep GHC.Types.Int)
                  ~
                (TestEq.Rep GHC.Types.Int -> TestEq.R:RepInt)))
    ((GHC.Base.id @ GHC.Types.Int)
     `cast` (GHC.Types.Int -> sym TestEq.TFCo:R:RepInt
             :: (GHC.Types.Int -> TestEq.R:RepInt)
                  ~
                (GHC.Types.Int -> TestEq.Rep GHC.Types.Int)))

TestEq.$fRepresentableChar :: TestEq.Representable GHC.Types.Char
LclIdX[DFunId]
[]
TestEq.$fRepresentableChar =
  TestEq.D:Representable
    @ GHC.Types.Char
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Char))
     `cast` (TestEq.Rep GHC.Types.Char -> TestEq.TFCo:R:RepChar
             :: (TestEq.Rep GHC.Types.Char -> TestEq.Rep GHC.Types.Char)
                  ~
                (TestEq.Rep GHC.Types.Char -> TestEq.R:RepChar)))
    ((GHC.Base.id @ GHC.Types.Char)
     `cast` (GHC.Types.Char -> sym TestEq.TFCo:R:RepChar
             :: (GHC.Types.Char -> TestEq.R:RepChar)
                  ~
                (GHC.Types.Char -> TestEq.Rep GHC.Types.Char)))

TestEq.$fRepresentableU :: TestEq.Representable TestEq.U
LclIdX[DFunId]
[]
TestEq.$fRepresentableU =
  TestEq.D:Representable
    @ TestEq.U
    ((GHC.Base.id @ (TestEq.Rep TestEq.U))
     `cast` (TestEq.Rep TestEq.U -> TestEq.TFCo:R:RepU
             :: (TestEq.Rep TestEq.U -> TestEq.Rep TestEq.U)
                  ~
                (TestEq.Rep TestEq.U -> TestEq.R:RepU)))
    ((GHC.Base.id @ TestEq.U)
     `cast` (TestEq.U -> sym TestEq.TFCo:R:RepU
             :: (TestEq.U -> TestEq.R:RepU)
                  ~
                (TestEq.U -> TestEq.Rep TestEq.U)))

a_s1dV :: forall a_ahe b_ahf.
          (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
          (a_ahe TestEq.:*: b_ahf) -> a_ahe TestEq.:*: b_ahf
LclId
[Arity 3]
a_s1dV =
  \ (@ a_ahe) (@ b_ahf) _ _ (eta_B1 :: a_ahe TestEq.:*: b_ahf) ->
    GHC.Base.id @ (a_ahe TestEq.:*: b_ahf) eta_B1

a_s1dX :: forall a_Xns b_Xnu.
          (TestEq.Representable a_Xns, TestEq.Representable b_Xnu) =>
          TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
          -> TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
LclId
[Arity 3]
a_s1dX =
  \ (@ a_Xns)
    (@ b_Xnu)
    _
    _
    (eta_B1 :: TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) ->
    GHC.Base.id @ (TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) eta_B1

TestEq.$fRepresentable:*: :: forall a_ahe b_ahf.
                             (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                             TestEq.Representable (a_ahe TestEq.:*: b_ahf)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRepresentable:*: =
  __inline_me (\ (@ a_Xnx)
                 (@ b_Xnz)
                 ($dRepresentable_X1bW :: TestEq.Representable a_Xnx)
                 ($dRepresentable_X1bY :: TestEq.Representable b_Xnz) ->
                 TestEq.D:Representable
                   @ (a_Xnx TestEq.:*: b_Xnz)
                   ((a_s1dX @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                            -> TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz
                            :: (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz))
                                 ~
                               (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.R:Rep:*: a_Xnx b_Xnz)))
                   ((a_s1dV @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` ((a_Xnx TestEq.:*: b_Xnz)
                            -> sym (TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz)
                            :: ((a_Xnx TestEq.:*: b_Xnz) -> TestEq.R:Rep:*: a_Xnx b_Xnz)
                                 ~
                               ((a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)))))

a_s1dZ :: forall a_aha b_ahb.
          (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
          (a_aha TestEq.:+: b_ahb) -> a_aha TestEq.:+: b_ahb
LclId
[Arity 3]
a_s1dZ =
  \ (@ a_aha) (@ b_ahb) _ _ (eta_B1 :: a_aha TestEq.:+: b_ahb) ->
    GHC.Base.id @ (a_aha TestEq.:+: b_ahb) eta_B1

a_s1e1 :: forall a_Xnz b_XnB.
          (TestEq.Representable a_Xnz, TestEq.Representable b_XnB) =>
          TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
          -> TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
LclId
[Arity 3]
a_s1e1 =
  \ (@ a_Xnz)
    (@ b_XnB)
    _
    _
    (eta_B1 :: TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) ->
    GHC.Base.id @ (TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) eta_B1

TestEq.$fRepresentable:+: :: forall a_aha b_ahb.
                             (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                             TestEq.Representable (a_aha TestEq.:+: b_ahb)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRepresentable:+: =
  __inline_me (\ (@ a_XnE)
                 (@ b_XnG)
                 ($dRepresentable_X1bU :: TestEq.Representable a_XnE)
                 ($dRepresentable_X1bW :: TestEq.Representable b_XnG) ->
                 TestEq.D:Representable
                   @ (a_XnE TestEq.:+: b_XnG)
                   ((a_s1e1 @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                            -> TestEq.TFCo:R:Rep:+: a_XnE b_XnG
                            :: (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG))
                                 ~
                               (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.R:Rep:+: a_XnE b_XnG)))
                   ((a_s1dZ @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` ((a_XnE TestEq.:+: b_XnG)
                            -> sym (TestEq.TFCo:R:Rep:+: a_XnE b_XnG)
                            :: ((a_XnE TestEq.:+: b_XnG) -> TestEq.R:Rep:+: a_XnE b_XnG)
                                 ~
                               ((a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG)))))

a_s1e3 :: forall a_ah6 c_ah7.
          (TestEq.Representable a_ah6) =>
          TestEq.C c_ah7 a_ah6 -> TestEq.C c_ah7 a_ah6
LclId
[Arity 2]
a_s1e3 =
  \ (@ a_ah6) (@ c_ah7) _ (eta_B1 :: TestEq.C c_ah7 a_ah6) ->
    GHC.Base.id @ (TestEq.C c_ah7 a_ah6) eta_B1

a_s1e5 :: forall a_XnF c_XnH.
          (TestEq.Representable a_XnF) =>
          TestEq.Rep (TestEq.C c_XnH a_XnF)
          -> TestEq.Rep (TestEq.C c_XnH a_XnF)
LclId
[Arity 2]
a_s1e5 =
  \ (@ a_XnF)
    (@ c_XnH)
    _
    (eta_B1 :: TestEq.Rep (TestEq.C c_XnH a_XnF)) ->
    GHC.Base.id @ (TestEq.Rep (TestEq.C c_XnH a_XnF)) eta_B1

TestEq.$fRepresentableC :: forall a_ah6 c_ah7.
                           (TestEq.Representable a_ah6) =>
                           TestEq.Representable (TestEq.C c_ah7 a_ah6)
LclIdX[DFunId]
[Arity 1]
TestEq.$fRepresentableC =
  __inline_me (\ (@ a_XnJ)
                 (@ c_XnL)
                 ($dRepresentable_X1iw :: TestEq.Representable a_XnJ) ->
                 TestEq.D:Representable
                   @ (TestEq.C c_XnL a_XnJ)
                   ((a_s1e5 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                            -> TestEq.TFCo:R:RepC c_XnL a_XnJ
                            :: (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                                -> TestEq.Rep (TestEq.C c_XnL a_XnJ))
                                 ~
                               (TestEq.Rep (TestEq.C c_XnL a_XnJ) -> TestEq.R:RepC c_XnL a_XnJ)))
                   ((a_s1e3 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.C c_XnL a_XnJ
                            -> sym (TestEq.TFCo:R:RepC c_XnL a_XnJ)
                            :: (TestEq.C c_XnL a_XnJ -> TestEq.R:RepC c_XnL a_XnJ)
                                 ~
                               (TestEq.C c_XnL a_XnJ -> TestEq.Rep (TestEq.C c_XnL a_XnJ)))))

a_s1e7 :: forall a_ah4.
          (TestEq.Representable a_ah4) =>
          TestEq.Var a_ah4 -> TestEq.Var a_ah4
LclId
[Arity 2]
a_s1e7 =
  \ (@ a_ah4) _ (eta_B1 :: TestEq.Var a_ah4) ->
    GHC.Base.id @ (TestEq.Var a_ah4) eta_B1

a_s1e9 :: forall a_XnL.
          (TestEq.Representable a_XnL) =>
          TestEq.Rep (TestEq.Var a_XnL) -> TestEq.Rep (TestEq.Var a_XnL)
LclId
[Arity 2]
a_s1e9 =
  \ (@ a_XnL) _ (eta_B1 :: TestEq.Rep (TestEq.Var a_XnL)) ->
    GHC.Base.id @ (TestEq.Rep (TestEq.Var a_XnL)) eta_B1

TestEq.$fRepresentableVar :: forall a_ah4.
                             (TestEq.Representable a_ah4) =>
                             TestEq.Representable (TestEq.Var a_ah4)
LclIdX[DFunId]
[Arity 1]
TestEq.$fRepresentableVar =
  __inline_me (\ (@ a_XnO)
                 ($dRepresentable_X1bL :: TestEq.Representable a_XnO) ->
                 TestEq.D:Representable
                   @ (TestEq.Var a_XnO)
                   ((a_s1e9 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.TFCo:R:RepVar a_XnO
                            :: (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.Rep (TestEq.Var a_XnO))
                                 ~
                               (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.R:RepVar a_XnO)))
                   ((a_s1e7 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Var a_XnO -> sym (TestEq.TFCo:R:RepVar a_XnO)
                            :: (TestEq.Var a_XnO -> TestEq.R:RepVar a_XnO)
                                 ~
                               (TestEq.Var a_XnO -> TestEq.Rep (TestEq.Var a_XnO)))))

a_s1eb :: forall a_ah2.
          (TestEq.Representable a_ah2) =>
          TestEq.Rec a_ah2 -> TestEq.Rec a_ah2
LclId
[Arity 2]
a_s1eb =
  \ (@ a_ah2) _ (eta_B1 :: TestEq.Rec a_ah2) ->
    GHC.Base.id @ (TestEq.Rec a_ah2) eta_B1

a_s1ed :: forall a_XnQ.
          (TestEq.Representable a_XnQ) =>
          TestEq.Rep (TestEq.Rec a_XnQ) -> TestEq.Rep (TestEq.Rec a_XnQ)
LclId
[Arity 2]
a_s1ed =
  \ (@ a_XnQ) _ (eta_B1 :: TestEq.Rep (TestEq.Rec a_XnQ)) ->
    GHC.Base.id @ (TestEq.Rep (TestEq.Rec a_XnQ)) eta_B1

TestEq.$fRepresentableRec :: forall a_ah2.
                             (TestEq.Representable a_ah2) =>
                             TestEq.Representable (TestEq.Rec a_ah2)
LclIdX[DFunId]
[Arity 1]
TestEq.$fRepresentableRec =
  __inline_me (\ (@ a_XnT)
                 ($dRepresentable_X1bF :: TestEq.Representable a_XnT) ->
                 TestEq.D:Representable
                   @ (TestEq.Rec a_XnT)
                   ((a_s1ed @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.TFCo:R:RepRec a_XnT
                            :: (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.Rep (TestEq.Rec a_XnT))
                                 ~
                               (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.R:RepRec a_XnT)))
                   ((a_s1eb @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rec a_XnT -> sym (TestEq.TFCo:R:RepRec a_XnT)
                            :: (TestEq.Rec a_XnT -> TestEq.R:RepRec a_XnT)
                                 ~
                               (TestEq.Rec a_XnT -> TestEq.Rep (TestEq.Rec a_XnT)))))

from_a14M :: forall a_agN. [a_agN] -> TestEq.Rep [a_agN]
LclId
[Arity 1]
from_a14M =
  \ (@ a_agN) (ds_d1dc :: [a_agN]) ->
    case ds_d1dc of _ {
      [] ->
        (TestEq.L
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C @ TestEq.List_Nil_ @ TestEq.U TestEq.U))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN]);
      : a_agO as_agP ->
        (TestEq.R
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C
              @ TestEq.List_Cons_
              @ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
              (TestEq.:*:
                 @ (TestEq.Var a_agN)
                 @ (TestEq.Rec [a_agN])
                 (TestEq.Var @ a_agN a_agO)
                 (TestEq.Rec @ [a_agN] as_agP))))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN])
    }

to_a14s :: forall a_agN. TestEq.Rep [a_agN] -> [a_agN]
LclId
[Arity 1]
to_a14s =
  \ (@ a_agN) (ds_d1cW :: TestEq.Rep [a_agN]) ->
    case ds_d1cW
         `cast` (sym (sym (TestEq.TFCo:R:Rep[] a_agN))
                 :: TestEq.Rep [a_agN] ~ TestEq.R:Rep[] a_agN)
    of _ {
      TestEq.L ds_d1d2 ->
        case ds_d1d2 of _ { TestEq.C ds_d1d3 ->
        case ds_d1d3 of _ { TestEq.U -> GHC.Types.[] @ a_agN }
        };
      TestEq.R ds_d1cY ->
        case ds_d1cY of _ { TestEq.C ds_d1cZ ->
        case ds_d1cZ of _ { TestEq.:*: ds_d1d0 ds_d1d1 ->
        case ds_d1d0 of _ { TestEq.Var a_agZ ->
        case ds_d1d1 of _ { TestEq.Rec as_ah0 ->
        GHC.Types.: @ a_agN a_agZ as_ah0
        }
        }
        }
        }
    }

TestEq.$fRepresentable[] :: forall a_agN.
                            TestEq.Representable [a_agN]
LclIdX[DFunId]
[]
TestEq.$fRepresentable[] =
  \ (@ a_agN) ->
    TestEq.D:Representable
      @ [a_agN] (to_a14s @ a_agN) (from_a14M @ a_agN)

a_s1et :: forall a_agL.
          TestEq.Tree a_agL
          -> TestEq.C TestEq.Leaf TestEq.U
             TestEq.:+: TestEq.C
                          TestEq.Bin
                          (TestEq.Var a_agL
                           TestEq.:*: (TestEq.Rec (TestEq.Tree a_agL)
                                       TestEq.:*: TestEq.Rec (TestEq.Tree a_agL)))
LclId
[Arity 1]
a_s1et =
  \ (@ a_agL) (sub_a178 :: TestEq.Tree a_agL) ->
    TestEq.fromTree @ a_agL @ TestEq.Bin @ TestEq.Leaf sub_a178

TestEq.$fRepresentableTree :: forall a_agL.
                              TestEq.Representable (TestEq.Tree a_agL)
LclIdX[DFunId]
[]
TestEq.$fRepresentableTree =
  \ (@ a_XnZ) ->
    TestEq.D:Representable
      @ (TestEq.Tree a_XnZ)
      ((TestEq.toTree @ TestEq.Bin @ a_XnZ @ TestEq.Leaf)
       `cast` (sym (TestEq.TFCo:R:RepTree a_XnZ) -> TestEq.Tree a_XnZ
               :: (TestEq.R:RepTree a_XnZ -> TestEq.Tree a_XnZ)
                    ~
                  (TestEq.Rep (TestEq.Tree a_XnZ) -> TestEq.Tree a_XnZ)))
      ((a_s1et @ a_XnZ)
       `cast` (TestEq.Tree a_XnZ -> sym (TestEq.TFCo:R:RepTree a_XnZ)
               :: (TestEq.Tree a_XnZ -> TestEq.R:RepTree a_XnZ)
                    ~
                  (TestEq.Tree a_XnZ -> TestEq.Rep (TestEq.Tree a_XnZ))))

TestEq.$fRepresentableLogic :: TestEq.Representable TestEq.Logic
LclIdX[DFunId]
[]
TestEq.$fRepresentableLogic =
  TestEq.D:Representable
    @ TestEq.Logic
    ((TestEq.toLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (sym TestEq.TFCo:R:RepLogic -> TestEq.Logic
             :: (TestEq.R:RepLogic -> TestEq.Logic)
                  ~
                (TestEq.Rep TestEq.Logic -> TestEq.Logic)))
    ((TestEq.fromLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (TestEq.Logic -> sym TestEq.TFCo:R:RepLogic
             :: (TestEq.Logic -> TestEq.R:RepLogic)
                  ~
                (TestEq.Logic -> TestEq.Rep TestEq.Logic)))

TestEq.$fEqU :: TestEq.Eq TestEq.U
LclIdX[DFunId]
[Arity 2]
TestEq.$fEqU =
  (TestEq.eqU @ TestEq.U @ TestEq.U)
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.U)
          :: (TestEq.U -> TestEq.U -> GHC.Bool.Bool) ~ TestEq.T:Eq TestEq.U)

Rec {
TestEq.eqLogic :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclIdX
[Arity 2]
TestEq.eqLogic =
  __inline_me (\ (x_agy :: TestEq.Logic) (y_agz :: TestEq.Logic) ->
                 eq'_aEx
                   (TestEq.fromLogic
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      x_agy)
                   (TestEq.fromLogic
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      y_agz))
TestEq.$fEqLogic :: TestEq.Eq TestEq.Logic
LclIdX[DFunId]
[Arity 2]
TestEq.$fEqLogic =
  TestEq.eqLogic
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.Logic)
          :: (TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq TestEq.Logic)
$dEq_a16K :: TestEq.Eq (TestEq.Rec TestEq.Logic)
LclId
[Arity 2]
$dEq_a16K = TestEq.$fEqRec @ TestEq.Logic TestEq.$fEqLogic
$dEq_a16O :: TestEq.Eq
               (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
LclId
[Arity 2]
$dEq_a16O =
  TestEq.$fEq:*:
    @ (TestEq.Rec TestEq.Logic)
    @ (TestEq.Rec TestEq.Logic)
    $dEq_a16K
    $dEq_a16K
eq'_aEx [ALWAYS LoopBreaker Nothing] :: (TestEq.C
                                           GHC.Prim.Any (TestEq.Var GHC.Base.String)
                                         TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                     TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                                 TestEq.:+: (TestEq.C
                                                                               GHC.Prim.Any
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic)
                                                                             TestEq.:+: (TestEq.C
                                                                                           GHC.Prim.Any
                                                                                           (TestEq.Rec
                                                                                              TestEq.Logic
                                                                                            TestEq.:*: TestEq.Rec
                                                                                                         TestEq.Logic)
                                                                                         TestEq.:+: (TestEq.C
                                                                                                       GHC.Prim.Any
                                                                                                       (TestEq.Rec
                                                                                                          TestEq.Logic
                                                                                                        TestEq.:*: TestEq.Rec
                                                                                                                     TestEq.Logic)
                                                                                                     TestEq.:+: (TestEq.C
                                                                                                                   GHC.Prim.Any
                                                                                                                   (TestEq.Rec
                                                                                                                      TestEq.Logic
                                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                                 TestEq.Logic)
                                                                                                                 TestEq.:+: TestEq.C
                                                                                                                              GHC.Prim.Any
                                                                                                                              (TestEq.Rec
                                                                                                                                 TestEq.Logic
                                                                                                                               TestEq.:*: TestEq.Rec
                                                                                                                                            TestEq.Logic))))))))
                                        -> (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                                            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                        TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                                    TestEq.:+: (TestEq.C
                                                                                  GHC.Prim.Any
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic)
                                                                                TestEq.:+: (TestEq.C
                                                                                              GHC.Prim.Any
                                                                                              (TestEq.Rec
                                                                                                 TestEq.Logic
                                                                                               TestEq.:*: TestEq.Rec
                                                                                                            TestEq.Logic)
                                                                                            TestEq.:+: (TestEq.C
                                                                                                          GHC.Prim.Any
                                                                                                          (TestEq.Rec
                                                                                                             TestEq.Logic
                                                                                                           TestEq.:*: TestEq.Rec
                                                                                                                        TestEq.Logic)
                                                                                                        TestEq.:+: (TestEq.C
                                                                                                                      GHC.Prim.Any
                                                                                                                      (TestEq.Rec
                                                                                                                         TestEq.Logic
                                                                                                                       TestEq.:*: TestEq.Rec
                                                                                                                                    TestEq.Logic)
                                                                                                                    TestEq.:+: TestEq.C
                                                                                                                                 GHC.Prim.Any
                                                                                                                                 (TestEq.Rec
                                                                                                                                    TestEq.Logic
                                                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                                                               TestEq.Logic))))))))
                                        -> GHC.Bool.Bool
LclId
[]
eq'_aEx =
  TestEq.eq'
    @ (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
       TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                   TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                               TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     GHC.Prim.Any
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 GHC.Prim.Any
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: TestEq.C
                                                                                            GHC.Prim.Any
                                                                                            (TestEq.Rec
                                                                                               TestEq.Logic
                                                                                             TestEq.:*: TestEq.Rec
                                                                                                          TestEq.Logic))))))))
    (TestEq.$fEq:+:
       @ (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String))
       @ (TestEq.C GHC.Prim.Any TestEq.U
          TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                      TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                GHC.Prim.Any
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: (TestEq.C
                                                            GHC.Prim.Any
                                                            (TestEq.Rec TestEq.Logic
                                                             TestEq.:*: TestEq.Rec TestEq.Logic)
                                                          TestEq.:+: (TestEq.C
                                                                        GHC.Prim.Any
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic)
                                                                      TestEq.:+: TestEq.C
                                                                                   GHC.Prim.Any
                                                                                   (TestEq.Rec
                                                                                      TestEq.Logic
                                                                                    TestEq.:*: TestEq.Rec
                                                                                                 TestEq.Logic)))))))
       (TestEq.$fEqC
          @ (TestEq.Var GHC.Base.String)
          @ GHC.Prim.Any
          (TestEq.$fEqVar
             @ [GHC.Types.Char]
             (TestEq.$fEq[] @ GHC.Types.Char TestEq.$fEqChar)))
       (TestEq.$fEq:+:
          @ (TestEq.C GHC.Prim.Any TestEq.U)
          @ (TestEq.C GHC.Prim.Any TestEq.U
             TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       GHC.Prim.Any
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   GHC.Prim.Any
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               GHC.Prim.Any
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: TestEq.C
                                                                          GHC.Prim.Any
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic))))))
          (TestEq.$fEqC @ TestEq.U @ GHC.Prim.Any TestEq.$fEqU)
          (TestEq.$fEq:+:
             @ (TestEq.C GHC.Prim.Any TestEq.U)
             @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                TestEq.:+: (TestEq.C
                              GHC.Prim.Any
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          GHC.Prim.Any
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      GHC.Prim.Any
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: TestEq.C
                                                                 GHC.Prim.Any
                                                                 (TestEq.Rec TestEq.Logic
                                                                  TestEq.:*: TestEq.Rec
                                                                               TestEq.Logic)))))
             (TestEq.$fEqC @ TestEq.U @ GHC.Prim.Any TestEq.$fEqU)
             (TestEq.$fEq:+:
                @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic))
                @ (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: TestEq.C
                                                        GHC.Prim.Any
                                                        (TestEq.Rec TestEq.Logic
                                                         TestEq.:*: TestEq.Rec TestEq.Logic))))
                (TestEq.$fEqC @ (TestEq.Rec TestEq.Logic) @ GHC.Prim.Any $dEq_a16K)
                (TestEq.$fEq:+:
                   @ (TestEq.C
                        GHC.Prim.Any
                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        GHC.Prim.Any
                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    GHC.Prim.Any
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: TestEq.C
                                               GHC.Prim.Any
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)))
                   (TestEq.$fEqC
                      @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      @ GHC.Prim.Any
                      $dEq_a16O)
                   (TestEq.$fEq:+:
                      @ (TestEq.C
                           GHC.Prim.Any
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           GHC.Prim.Any
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      (TestEq.$fEqC
                         @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         @ GHC.Prim.Any
                         $dEq_a16O)
                      (TestEq.$fEq:+:
                         @ (TestEq.C
                              GHC.Prim.Any
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              GHC.Prim.Any
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.$fEqC
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            @ GHC.Prim.Any
                            $dEq_a16O)
                         (TestEq.$fEqC
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            @ GHC.Prim.Any
                            $dEq_a16O))))))))
TestEq.$fEq[] :: forall a_agC.
                 (TestEq.Eq a_agC) =>
                 TestEq.Eq [a_agC]
LclIdX[DFunId]
[Arity 1]
TestEq.$fEq[] =
  __inline_me (eq'_a10s
               `cast` (forall a_agC.
                       (TestEq.Eq a_agC) =>
                       sym (TestEq.NTCo:T:Eq [a_agC])
                       :: (forall a_agC.
                           (TestEq.Eq a_agC) =>
                           [a_agC] -> [a_agC] -> GHC.Bool.Bool)
                            ~
                          (forall a_agC. (TestEq.Eq a_agC) => TestEq.T:Eq [a_agC])))
eq'_a10s [ALWAYS LoopBreaker Nothing] :: forall a_agC.
                                         (TestEq.Eq a_agC) =>
                                         [a_agC] -> [a_agC] -> GHC.Bool.Bool
LclId
[Arity 1]
eq'_a10s =
  \ (@ a_agC) ($dEq_a10b :: TestEq.Eq a_agC) ->
    TestEq.eq
      @ [a_agC]
      (TestEq.$fRepresentable[] @ a_agC)
      ((TestEq.$fEq:+:
          @ (TestEq.C TestEq.List_Nil_ TestEq.U)
          @ (TestEq.C
               TestEq.List_Cons_ (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))
          (TestEq.$fEqC @ TestEq.U @ TestEq.List_Nil_ TestEq.$fEqU)
          (TestEq.$fEqC
             @ (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC])
             @ TestEq.List_Cons_
             (TestEq.$fEq:*:
                @ (TestEq.Var a_agC)
                @ (TestEq.Rec [a_agC])
                (TestEq.$fEqVar @ a_agC $dEq_a10b)
                (TestEq.$fEqRec @ [a_agC] (TestEq.$fEq[] @ a_agC $dEq_a10b)))))
       `cast` (TestEq.T:Eq
                 (trans
                    (sym
                       (trans
                          (TestEq.TFCo:R:Rep[] a_agC)
                          (sym
                             (trans
                                (trans
                                   (TestEq.C TestEq.List_Nil_ TestEq.U
                                    TestEq.:+: TestEq.C
                                                 TestEq.List_Cons_
                                                 (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))
                                   (TestEq.C TestEq.List_Nil_ TestEq.U
                                    TestEq.:+: TestEq.C
                                                 TestEq.List_Cons_
                                                 (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC])))
                                (TestEq.C TestEq.List_Nil_ TestEq.U
                                 TestEq.:+: TestEq.C
                                              TestEq.List_Cons_
                                              (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))))))
                    (TestEq.Rep [a_agC]))
               :: TestEq.T:Eq
                    (TestEq.C TestEq.List_Nil_ TestEq.U
                     TestEq.:+: TestEq.C
                                  TestEq.List_Cons_
                                  (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))
                    ~
                  TestEq.T:Eq (TestEq.Rep [a_agC])))
TestEq.$fEqChar :: TestEq.Eq GHC.Types.Char
LclIdX[DFunId]
[]
TestEq.$fEqChar =
  a_s1f3
  `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
          :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq GHC.Types.Char)
a_s1f3 :: GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool
LclId
[]
a_s1f3 = GHC.Classes.== @ GHC.Types.Char GHC.Base.$fEqChar
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
LclIdX
[Arity 2]
TestEq.eqTree =
  __inline_me (\ (x_agA :: TestEq.Tree GHC.Types.Int)
                 (y_agB :: TestEq.Tree GHC.Types.Int) ->
                 eq'_aFu
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x_agA)
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any y_agB))
TestEq.$fEqTree :: TestEq.Eq (TestEq.Tree GHC.Types.Int)
LclIdX[DFunId]
[Arity 2]
TestEq.$fEqTree =
  TestEq.eqTree
  `cast` (sym (TestEq.NTCo:T:Eq (TestEq.Tree GHC.Types.Int))
          :: (TestEq.Tree GHC.Types.Int
              -> TestEq.Tree GHC.Types.Int
              -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq (TestEq.Tree GHC.Types.Int))
$dEq_a16u :: TestEq.Eq (TestEq.Rec (TestEq.Tree GHC.Types.Int))
LclId
[Arity 2]
$dEq_a16u =
  TestEq.$fEqRec @ (TestEq.Tree GHC.Types.Int) TestEq.$fEqTree
eq'_aFu [ALWAYS LoopBreaker Nothing] :: (TestEq.C
                                           GHC.Prim.Any TestEq.U
                                         TestEq.:+: TestEq.C
                                                      GHC.Prim.Any
                                                      (TestEq.Var GHC.Types.Int
                                                       TestEq.:*: (TestEq.Rec
                                                                     (TestEq.Tree GHC.Types.Int)
                                                                   TestEq.:*: TestEq.Rec
                                                                                (TestEq.Tree
                                                                                   GHC.Types.Int))))
                                        -> (TestEq.C GHC.Prim.Any TestEq.U
                                            TestEq.:+: TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Var GHC.Types.Int
                                                          TestEq.:*: (TestEq.Rec
                                                                        (TestEq.Tree GHC.Types.Int)
                                                                      TestEq.:*: TestEq.Rec
                                                                                   (TestEq.Tree
                                                                                      GHC.Types.Int))))
                                        -> GHC.Bool.Bool
LclId
[]
eq'_aFu =
  TestEq.eq'
    @ (TestEq.C GHC.Prim.Any TestEq.U
       TestEq.:+: TestEq.C
                    GHC.Prim.Any
                    (TestEq.Var GHC.Types.Int
                     TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                 TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    (TestEq.$fEq:+:
       @ (TestEq.C GHC.Prim.Any TestEq.U)
       @ (TestEq.C
            GHC.Prim.Any
            (TestEq.Var GHC.Types.Int
             TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                         TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
       (TestEq.$fEqC @ TestEq.U @ GHC.Prim.Any TestEq.$fEqU)
       (TestEq.$fEqC
          @ (TestEq.Var GHC.Types.Int
             TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                         TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
          @ GHC.Prim.Any
          (TestEq.$fEq:*:
             @ (TestEq.Var GHC.Types.Int)
             @ (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))
             (TestEq.$fEqVar @ GHC.Types.Int TestEq.$fEqInt)
             (TestEq.$fEq:*:
                @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
                @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
                $dEq_a16u
                $dEq_a16u))))
TestEq.$fEqInt :: TestEq.Eq GHC.Types.Int
LclIdX[DFunId]
[]
TestEq.$fEqInt =
  a_s1f1
  `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Int)
          :: (GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq GHC.Types.Int)
a_s1f1 :: GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool
LclId
[]
a_s1f1 = GHC.Classes.== @ GHC.Types.Int GHC.Base.$fEqInt
TestEq.$fEqRec :: forall a_agD.
                  (TestEq.Eq a_agD) =>
                  TestEq.Eq (TestEq.Rec a_agD)
LclIdX[DFunId]
[Arity 3]
TestEq.$fEqRec =
  __inline_me (TestEq.eqRec
               `cast` (forall a_agD.
                       (TestEq.Eq a_agD) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Rec a_agD))
                       :: (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.Rec a_agD -> TestEq.Rec a_agD -> GHC.Bool.Bool)
                            ~
                          (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.T:Eq (TestEq.Rec a_agD))))
TestEq.$fEqVar :: forall a_agE.
                  (TestEq.Eq a_agE) =>
                  TestEq.Eq (TestEq.Var a_agE)
LclIdX[DFunId]
[Arity 3]
TestEq.$fEqVar =
  __inline_me (TestEq.eqVar
               `cast` (forall a_agE.
                       (TestEq.Eq a_agE) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Var a_agE))
                       :: (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.Var a_agE -> TestEq.Var a_agE -> GHC.Bool.Bool)
                            ~
                          (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.T:Eq (TestEq.Var a_agE))))
TestEq.$fEqC :: forall a_agF c_agG.
                (TestEq.Eq a_agF) =>
                TestEq.Eq (TestEq.C c_agG a_agF)
LclIdX[DFunId]
[Arity 3]
TestEq.$fEqC =
  __inline_me (eq'_a117
               `cast` (forall a_agF c_agG.
                       (TestEq.Eq a_agF) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.C c_agG a_agF))
                       :: (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool)
                            ~
                          (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.T:Eq (TestEq.C c_agG a_agF))))
eq'_a117 :: forall a_agF c_agG.
            (TestEq.Eq a_agF) =>
            TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool
LclId
[Arity 3]
eq'_a117 =
  \ (@ a_agF)
    (@ c_agG)
    ($dEq_a110 :: TestEq.Eq a_agF)
    (eta_B2 :: TestEq.C c_agG a_agF)
    (eta_B1 :: TestEq.C c_agG a_agF) ->
    TestEq.eqC @ c_agG @ a_agF @ c_agG $dEq_a110 eta_B2 eta_B1
TestEq.$fEq:*: :: forall a_agH b_agI.
                  (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                  TestEq.Eq (a_agH TestEq.:*: b_agI)
LclIdX[DFunId]
[Arity 4]
TestEq.$fEq:*: =
  __inline_me (TestEq.eqTimes
               `cast` (forall a_agH b_agI.
                       (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                       sym (TestEq.NTCo:T:Eq (a_agH TestEq.:*: b_agI))
                       :: (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           (a_agH TestEq.:*: b_agI)
                           -> (a_agH TestEq.:*: b_agI)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           TestEq.T:Eq (a_agH TestEq.:*: b_agI))))
TestEq.$fEq:+: :: forall a_agJ b_agK.
                  (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                  TestEq.Eq (a_agJ TestEq.:+: b_agK)
LclIdX[DFunId]
[Arity 4]
TestEq.$fEq:+: =
  __inline_me (TestEq.eqPlus
               `cast` (forall a_agJ b_agK.
                       (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                       sym (TestEq.NTCo:T:Eq (a_agJ TestEq.:+: b_agK))
                       :: (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           (a_agJ TestEq.:+: b_agK)
                           -> (a_agJ TestEq.:+: b_agK)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           TestEq.T:Eq (a_agJ TestEq.:+: b_agK))))
end Rec }

a_s1fK :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U
LclId
[]
a_s1fK =
  GHC.Base.>>=
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec
    @ Text.Read.Lex.Lexeme
    @ TestEq.U
    GHC.Read.lexP
    (\ (ds_d1cy :: Text.Read.Lex.Lexeme) ->
       let {
         fail_s1o4 :: GHC.Prim.State# GHC.Prim.RealWorld
                      -> Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U
         LclId
         [Arity 1]
         fail_s1o4 =
           \ _ ->
             GHC.Base.fail
               @ Text.ParserCombinators.ReadPrec.ReadPrec
               Text.ParserCombinators.ReadPrec.$fMonadReadPrec
               @ TestEq.U
               (GHC.Base.unpackCString#
                  "Pattern match failure in do expression at <no location info>") } in
       case ds_d1cy of _ {
         __DEFAULT -> fail_s1o4 GHC.Prim.realWorld#;
         Text.Read.Lex.Ident ds_d1cz ->
           case ds_d1cz of _ {
             [] -> fail_s1o4 GHC.Prim.realWorld#;
             : ds_d1cA ds_d1cB ->
               case ds_d1cA of _ { GHC.Types.C# ds_d1cC ->
               case ds_d1cC of _ {
                 __DEFAULT -> fail_s1o4 GHC.Prim.realWorld#;
                 'U' ->
                   case ds_d1cB of _ {
                     [] ->
                       GHC.Base.return
                         @ Text.ParserCombinators.ReadPrec.ReadPrec
                         Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                         @ TestEq.U
                         TestEq.U;
                     : _ _ -> fail_s1o4 GHC.Prim.realWorld#
                   }
               }
               }
           }
       })

readPrec_aZU :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U
LclId
[Arity 1]
readPrec_aZU = GHC.Read.parens @ TestEq.U a_s1fK

Rec {
TestEq.$fReadU :: GHC.Read.Read TestEq.U
LclIdX[DFunId]
[]
TestEq.$fReadU =
  GHC.Read.D:Read
    @ TestEq.U
    a_s1fY
    readListDefault_aZF
    readPrec_aZU
    readListPrecDefault_aZY
a_s1fY [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                       -> Text.ParserCombinators.ReadP.ReadS TestEq.U
LclId
[Arity 1]
a_s1fY = GHC.Read.$dmreadsPrec @ TestEq.U TestEq.$fReadU
readListPrecDefault_aZY [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.ReadPrec
                                                          [TestEq.U]
LclId
[]
readListPrecDefault_aZY =
  GHC.Read.readListPrecDefault @ TestEq.U TestEq.$fReadU
readListDefault_aZF [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadP.ReadS
                                                      [TestEq.U]
LclId
[]
readListDefault_aZF =
  GHC.Read.readListDefault @ TestEq.U TestEq.$fReadU
end Rec }

showsPrec_aZq :: GHC.Types.Int -> TestEq.U -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aZq =
  \ _ (ds_d1cw :: TestEq.U) ->
    case ds_d1cw of _ { TestEq.U ->
    GHC.Show.showString
      (GHC.Types.:
         @ GHC.Types.Char
         (GHC.Types.C# 'U')
         (GHC.Types.[] @ GHC.Types.Char))
    }

Rec {
TestEq.$fShowU :: GHC.Show.Show TestEq.U
LclIdX[DFunId]
[]
TestEq.$fShowU =
  GHC.Show.D:Show @ TestEq.U showsPrec_aZq a_s1gh showList_aZy
a_s1gh [ALWAYS LoopBreaker Nothing] :: TestEq.U -> GHC.Base.String
LclId
[]
a_s1gh = GHC.Show.$dmshow @ TestEq.U TestEq.$fShowU
a_s1gE :: TestEq.U -> GHC.Show.ShowS
LclId
[]
a_s1gE =
  GHC.Show.showsPrec @ TestEq.U TestEq.$fShowU (GHC.Types.I# 0)
showList_aZy [ALWAYS LoopBreaker Nothing] :: [TestEq.U]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aZy = GHC.Show.showList__ @ TestEq.U a_s1gE
end Rec }

readPrec_aZb :: forall a_afL b_afM.
                (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (a_afL TestEq.:+: b_afM)
LclId
[Arity 2]
readPrec_aZb =
  \ (@ a_afL)
    (@ b_afM)
    ($dRead_aYo :: GHC.Read.Read a_afL)
    ($dRead_aYp :: GHC.Read.Read b_afM) ->
    let {
      readPrec_s1o0 :: Text.ParserCombinators.ReadPrec.ReadPrec b_afM
      LclId
      []
      readPrec_s1o0 = GHC.Read.readPrec @ b_afM $dRead_aYp } in
    let {
      return_s1nZ :: forall a_aPP.
                     a_aPP -> Text.ParserCombinators.ReadPrec.ReadPrec a_aPP
      LclId
      []
      return_s1nZ =
        GHC.Base.return
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    let {
      readPrec_s1nY :: Text.ParserCombinators.ReadPrec.ReadPrec a_afL
      LclId
      []
      readPrec_s1nY = GHC.Read.readPrec @ a_afL $dRead_aYo } in
    let {
      fail_s1nX :: forall a_aSD.
                   GHC.Base.String -> Text.ParserCombinators.ReadPrec.ReadPrec a_aSD
      LclId
      []
      fail_s1nX =
        GHC.Base.fail
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    let {
      >>=_s1nW :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1nW =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (a_afL TestEq.:+: b_afM)
      (Text.ParserCombinators.ReadPrec.+++
         @ (a_afL TestEq.:+: b_afM)
         (Text.ParserCombinators.ReadPrec.prec
            @ (a_afL TestEq.:+: b_afM)
            (GHC.Types.I# 10)
            (>>=_s1nW
               @ Text.Read.Lex.Lexeme
               @ (a_afL TestEq.:+: b_afM)
               GHC.Read.lexP
               (\ (ds_d1c9 :: Text.Read.Lex.Lexeme) ->
                  let {
                    fail_s1o2 :: GHC.Prim.State# GHC.Prim.RealWorld
                                 -> Text.ParserCombinators.ReadPrec.ReadPrec
                                      (a_afL TestEq.:+: b_afM)
                    LclId
                    [Arity 1]
                    fail_s1o2 =
                      \ _ ->
                        fail_s1nX
                          @ (a_afL TestEq.:+: b_afM)
                          (GHC.Base.unpackCString#
                             "Pattern match failure in do expression at <no location info>") } in
                  case ds_d1c9 of _ {
                    __DEFAULT -> fail_s1o2 GHC.Prim.realWorld#;
                    Text.Read.Lex.Ident ds_d1ca ->
                      case ds_d1ca of _ {
                        [] -> fail_s1o2 GHC.Prim.realWorld#;
                        : ds_d1cb ds_d1cc ->
                          case ds_d1cb of _ { GHC.Types.C# ds_d1cd ->
                          case ds_d1cd of _ {
                            __DEFAULT -> fail_s1o2 GHC.Prim.realWorld#;
                            'L' ->
                              case ds_d1cc of _ {
                                [] ->
                                  >>=_s1nW
                                    @ a_afL
                                    @ (a_afL TestEq.:+: b_afM)
                                    (Text.ParserCombinators.ReadPrec.step @ a_afL readPrec_s1nY)
                                    (\ (a1_ayI :: a_afL) ->
                                       return_s1nZ
                                         @ (a_afL TestEq.:+: b_afM)
                                         (TestEq.L @ a_afL @ b_afM a1_ayI));
                                : _ _ -> fail_s1o2 GHC.Prim.realWorld#
                              }
                          }
                          }
                      }
                  })))
         (Text.ParserCombinators.ReadPrec.prec
            @ (a_afL TestEq.:+: b_afM)
            (GHC.Types.I# 10)
            (>>=_s1nW
               @ Text.Read.Lex.Lexeme
               @ (a_afL TestEq.:+: b_afM)
               GHC.Read.lexP
               (\ (ds_d1ck :: Text.Read.Lex.Lexeme) ->
                  let {
                    fail_s1nV :: GHC.Prim.State# GHC.Prim.RealWorld
                                 -> Text.ParserCombinators.ReadPrec.ReadPrec
                                      (a_afL TestEq.:+: b_afM)
                    LclId
                    [Arity 1]
                    fail_s1nV =
                      \ _ ->
                        fail_s1nX
                          @ (a_afL TestEq.:+: b_afM)
                          (GHC.Base.unpackCString#
                             "Pattern match failure in do expression at <no location info>") } in
                  case ds_d1ck of _ {
                    __DEFAULT -> fail_s1nV GHC.Prim.realWorld#;
                    Text.Read.Lex.Ident ds_d1cl ->
                      case ds_d1cl of _ {
                        [] -> fail_s1nV GHC.Prim.realWorld#;
                        : ds_d1cm ds_d1cn ->
                          case ds_d1cm of _ { GHC.Types.C# ds_d1co ->
                          case ds_d1co of _ {
                            __DEFAULT -> fail_s1nV GHC.Prim.realWorld#;
                            'R' ->
                              case ds_d1cn of _ {
                                [] ->
                                  >>=_s1nW
                                    @ b_afM
                                    @ (a_afL TestEq.:+: b_afM)
                                    (Text.ParserCombinators.ReadPrec.step @ b_afM readPrec_s1o0)
                                    (\ (a1_ayJ :: b_afM) ->
                                       return_s1nZ
                                         @ (a_afL TestEq.:+: b_afM)
                                         (TestEq.R @ a_afL @ b_afM a1_ayJ));
                                : _ _ -> fail_s1nV GHC.Prim.realWorld#
                              }
                          }
                          }
                      }
                  }))))

Rec {
TestEq.$fRead:+: :: forall a_afL b_afM.
                    (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                    GHC.Read.Read (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRead:+: =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 ($dRead_aYo :: GHC.Read.Read a_afL)
                 ($dRead_aYp :: GHC.Read.Read b_afM) ->
                 let {
                   a_s1nN :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [a_afL TestEq.:+: b_afM]
                   LclId
                   []
                   a_s1nN =
                     readListPrec_aZl @ a_afL @ b_afM $dRead_aYo $dRead_aYp } in
                 let {
                   a_s1nM :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (a_afL TestEq.:+: b_afM)
                   LclId
                   []
                   a_s1nM = readPrec_aZb @ a_afL @ b_afM $dRead_aYo $dRead_aYp } in
                 let {
                   a_s1nO :: Text.ParserCombinators.ReadP.ReadS
                               [a_afL TestEq.:+: b_afM]
                   LclId
                   []
                   a_s1nO = readList_aYC @ a_afL @ b_afM $dRead_aYo $dRead_aYp } in
                 letrec {
                   $dRead_s1nK :: GHC.Read.Read (a_afL TestEq.:+: b_afM)
                   LclId
                   []
                   $dRead_s1nK =
                     GHC.Read.D:Read
                       @ (a_afL TestEq.:+: b_afM) a_s1nL a_s1nO a_s1nM a_s1nN;
                   a_s1nL [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_afL TestEq.:+: b_afM)
                   LclId
                   [Arity 1]
                   a_s1nL =
                     GHC.Read.$dmreadsPrec @ (a_afL TestEq.:+: b_afM) $dRead_s1nK; } in
                 $dRead_s1nK)
readListPrec_aZl [ALWAYS LoopBreaker Nothing] :: forall a_afL
                                                        b_afM.
                                                 (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [a_afL TestEq.:+: b_afM]
LclId
[Arity 2]
readListPrec_aZl =
  \ (@ a_afL)
    (@ b_afM)
    ($dRead_aYo :: GHC.Read.Read a_afL)
    ($dRead_aYp :: GHC.Read.Read b_afM) ->
    GHC.Read.readListPrecDefault
      @ (a_afL TestEq.:+: b_afM)
      (TestEq.$fRead:+: @ a_afL @ b_afM $dRead_aYo $dRead_aYp)
readList_aYC [ALWAYS LoopBreaker Nothing] :: forall a_afL b_afM.
                                             (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                                             Text.ParserCombinators.ReadP.ReadS
                                               [a_afL TestEq.:+: b_afM]
LclId
[Arity 2]
readList_aYC =
  \ (@ a_afL)
    (@ b_afM)
    ($dRead_aYo :: GHC.Read.Read a_afL)
    ($dRead_aYp :: GHC.Read.Read b_afM) ->
    GHC.Read.readListDefault
      @ (a_afL TestEq.:+: b_afM)
      (TestEq.$fRead:+: @ a_afL @ b_afM $dRead_aYo $dRead_aYp)
end Rec }

showsPrec_aYb :: forall a_afL b_afM.
                 (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                 GHC.Types.Int -> (a_afL TestEq.:+: b_afM) -> GHC.Show.ShowS
LclId
[Arity 4]
showsPrec_aYb =
  \ (@ a_afL)
    (@ b_afM)
    ($dShow_aXP :: GHC.Show.Show a_afL)
    ($dShow_aXQ :: GHC.Show.Show b_afM)
    (eta_B2 :: GHC.Types.Int)
    (eta_B1 :: a_afL TestEq.:+: b_afM) ->
    case eta_B1 of _ {
      TestEq.L b1_ayF ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt eta_B2 (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "L "))
             (GHC.Show.showsPrec @ a_afL $dShow_aXP (GHC.Types.I# 11) b1_ayF));
      TestEq.R b1_ayH ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt eta_B2 (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "R "))
             (GHC.Show.showsPrec @ b_afM $dShow_aXQ (GHC.Types.I# 11) b1_ayH))
    }

Rec {
TestEq.$fShow:+: :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Show.Show (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2]
TestEq.$fShow:+: =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 ($dShow_aXP :: GHC.Show.Show a_afL)
                 ($dShow_aXQ :: GHC.Show.Show b_afM) ->
                 let {
                   a_s1nE :: [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1nE = showList_aYn @ a_afL @ b_afM $dShow_aXP $dShow_aXQ } in
                 let {
                   a_s1nF :: GHC.Types.Int
                             -> (a_afL TestEq.:+: b_afM)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1nF = showsPrec_aYb @ a_afL @ b_afM $dShow_aXP $dShow_aXQ } in
                 letrec {
                   $dShow_s1nC :: GHC.Show.Show (a_afL TestEq.:+: b_afM)
                   LclId
                   []
                   $dShow_s1nC =
                     GHC.Show.D:Show @ (a_afL TestEq.:+: b_afM) a_s1nF a_s1nD a_s1nE;
                   a_s1nD [ALWAYS LoopBreaker Nothing] :: (a_afL TestEq.:+: b_afM)
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1nD =
                     GHC.Show.$dmshow @ (a_afL TestEq.:+: b_afM) $dShow_s1nC; } in
                 $dShow_s1nC)
showList_aYn [ALWAYS LoopBreaker Nothing] :: forall a_afL b_afM.
                                             (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                                             [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aYn =
  \ (@ a_afL)
    (@ b_afM)
    ($dShow_aXP :: GHC.Show.Show a_afL)
    ($dShow_aXQ :: GHC.Show.Show b_afM) ->
    GHC.Show.showList__
      @ (a_afL TestEq.:+: b_afM)
      (GHC.Show.showsPrec
         @ (a_afL TestEq.:+: b_afM)
         (TestEq.$fShow:+: @ a_afL @ b_afM $dShow_aXP $dShow_aXQ)
         (GHC.Types.I# 0))
end Rec }

readPrec_aXE :: forall a_afJ b_afK.
                (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (a_afJ TestEq.:*: b_afK)
LclId
[Arity 2]
readPrec_aXE =
  \ (@ a_afJ)
    (@ b_afK)
    ($dRead_aWX :: GHC.Read.Read a_afJ)
    ($dRead_aWY :: GHC.Read.Read b_afK) ->
    let {
      readPrec_s1ny :: Text.ParserCombinators.ReadPrec.ReadPrec b_afK
      LclId
      []
      readPrec_s1ny = GHC.Read.readPrec @ b_afK $dRead_aWY } in
    let {
      >>=_s1nx :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1nx =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (a_afJ TestEq.:*: b_afK)
      (Text.ParserCombinators.ReadPrec.prec
         @ (a_afJ TestEq.:*: b_afK)
         (GHC.Types.I# 6)
         (>>=_s1nx
            @ a_afJ
            @ (a_afJ TestEq.:*: b_afK)
            (Text.ParserCombinators.ReadPrec.step
               @ a_afJ (GHC.Read.readPrec @ a_afJ $dRead_aWX))
            (\ (a1_ayC :: a_afJ) ->
               >>=_s1nx
                 @ Text.Read.Lex.Lexeme
                 @ (a_afJ TestEq.:*: b_afK)
                 GHC.Read.lexP
                 (\ (ds_d1c1 :: Text.Read.Lex.Lexeme) ->
                    let {
                      fail_s1nw :: GHC.Prim.State# GHC.Prim.RealWorld
                                   -> Text.ParserCombinators.ReadPrec.ReadPrec
                                        (a_afJ TestEq.:*: b_afK)
                      LclId
                      [Arity 1]
                      fail_s1nw =
                        \ _ ->
                          GHC.Base.fail
                            @ Text.ParserCombinators.ReadPrec.ReadPrec
                            Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                            @ (a_afJ TestEq.:*: b_afK)
                            (GHC.Base.unpackCString#
                               "Pattern match failure in do expression at <no location info>") } in
                    case ds_d1c1 of _ {
                      __DEFAULT -> fail_s1nw GHC.Prim.realWorld#;
                      Text.Read.Lex.Symbol ds_d1c2 ->
                        case GHC.Base.eqString ds_d1c2 (GHC.Base.unpackCString# ":*:")
                        of _ {
                          GHC.Bool.False -> fail_s1nw GHC.Prim.realWorld#;
                          GHC.Bool.True ->
                            >>=_s1nx
                              @ b_afK
                              @ (a_afJ TestEq.:*: b_afK)
                              (Text.ParserCombinators.ReadPrec.step @ b_afK readPrec_s1ny)
                              (\ (a2_ayD :: b_afK) ->
                                 GHC.Base.return
                                   @ Text.ParserCombinators.ReadPrec.ReadPrec
                                   Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                                   @ (a_afJ TestEq.:*: b_afK)
                                   (TestEq.:*: @ a_afJ @ b_afK a1_ayC a2_ayD))
                        }
                    }))))

Rec {
TestEq.$fRead:*: :: forall a_afJ b_afK.
                    (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                    GHC.Read.Read (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRead:*: =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 ($dRead_aWX :: GHC.Read.Read a_afJ)
                 ($dRead_aWY :: GHC.Read.Read b_afK) ->
                 let {
                   a_s1nr :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [a_afJ TestEq.:*: b_afK]
                   LclId
                   []
                   a_s1nr =
                     readListPrec_aXO @ a_afJ @ b_afK $dRead_aWX $dRead_aWY } in
                 let {
                   a_s1nq :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (a_afJ TestEq.:*: b_afK)
                   LclId
                   []
                   a_s1nq = readPrec_aXE @ a_afJ @ b_afK $dRead_aWX $dRead_aWY } in
                 let {
                   a_s1ns :: Text.ParserCombinators.ReadP.ReadS
                               [a_afJ TestEq.:*: b_afK]
                   LclId
                   []
                   a_s1ns = readList_aXb @ a_afJ @ b_afK $dRead_aWX $dRead_aWY } in
                 letrec {
                   $dRead_s1no :: GHC.Read.Read (a_afJ TestEq.:*: b_afK)
                   LclId
                   []
                   $dRead_s1no =
                     GHC.Read.D:Read
                       @ (a_afJ TestEq.:*: b_afK) a_s1np a_s1ns a_s1nq a_s1nr;
                   a_s1np [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_afJ TestEq.:*: b_afK)
                   LclId
                   [Arity 1]
                   a_s1np =
                     GHC.Read.$dmreadsPrec @ (a_afJ TestEq.:*: b_afK) $dRead_s1no; } in
                 $dRead_s1no)
readListPrec_aXO [ALWAYS LoopBreaker Nothing] :: forall a_afJ
                                                        b_afK.
                                                 (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [a_afJ TestEq.:*: b_afK]
LclId
[Arity 2]
readListPrec_aXO =
  \ (@ a_afJ)
    (@ b_afK)
    ($dRead_aWX :: GHC.Read.Read a_afJ)
    ($dRead_aWY :: GHC.Read.Read b_afK) ->
    GHC.Read.readListPrecDefault
      @ (a_afJ TestEq.:*: b_afK)
      (TestEq.$fRead:*: @ a_afJ @ b_afK $dRead_aWX $dRead_aWY)
readList_aXb [ALWAYS LoopBreaker Nothing] :: forall a_afJ b_afK.
                                             (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                                             Text.ParserCombinators.ReadP.ReadS
                                               [a_afJ TestEq.:*: b_afK]
LclId
[Arity 2]
readList_aXb =
  \ (@ a_afJ)
    (@ b_afK)
    ($dRead_aWX :: GHC.Read.Read a_afJ)
    ($dRead_aWY :: GHC.Read.Read b_afK) ->
    GHC.Read.readListDefault
      @ (a_afJ TestEq.:*: b_afK)
      (TestEq.$fRead:*: @ a_afJ @ b_afK $dRead_aWX $dRead_aWY)
end Rec }

showsPrec_aWK :: forall a_afJ b_afK.
                 (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                 GHC.Types.Int -> (a_afJ TestEq.:*: b_afK) -> GHC.Show.ShowS
LclId
[Arity 4]
showsPrec_aWK =
  \ (@ a_afJ)
    (@ b_afK)
    ($dShow_aWq :: GHC.Show.Show a_afJ)
    ($dShow_aWr :: GHC.Show.Show b_afK)
    (a_ayz :: GHC.Types.Int)
    (ds_d1bZ :: a_afJ TestEq.:*: b_afK) ->
    case ds_d1bZ of _ { TestEq.:*: b1_ayA b2_ayB ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayz (GHC.Types.I# 7))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showsPrec @ a_afJ $dShow_aWq (GHC.Types.I# 7) b1_ayA)
         (GHC.Base..
            @ GHC.Base.String
            @ GHC.Base.String
            @ GHC.Base.String
            (GHC.Show.showString (GHC.Base.unpackCString# " :*: "))
            (GHC.Show.showsPrec @ b_afK $dShow_aWr (GHC.Types.I# 7) b2_ayB)))
    }

Rec {
TestEq.$fShow:*: :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Show.Show (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2]
TestEq.$fShow:*: =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 ($dShow_aWq :: GHC.Show.Show a_afJ)
                 ($dShow_aWr :: GHC.Show.Show b_afK) ->
                 let {
                   a_s1ni :: [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1ni = showList_aWW @ a_afJ @ b_afK $dShow_aWq $dShow_aWr } in
                 let {
                   a_s1nj :: GHC.Types.Int
                             -> (a_afJ TestEq.:*: b_afK)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1nj = showsPrec_aWK @ a_afJ @ b_afK $dShow_aWq $dShow_aWr } in
                 letrec {
                   $dShow_s1ng :: GHC.Show.Show (a_afJ TestEq.:*: b_afK)
                   LclId
                   []
                   $dShow_s1ng =
                     GHC.Show.D:Show @ (a_afJ TestEq.:*: b_afK) a_s1nj a_s1nh a_s1ni;
                   a_s1nh [ALWAYS LoopBreaker Nothing] :: (a_afJ TestEq.:*: b_afK)
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1nh =
                     GHC.Show.$dmshow @ (a_afJ TestEq.:*: b_afK) $dShow_s1ng; } in
                 $dShow_s1ng)
showList_aWW [ALWAYS LoopBreaker Nothing] :: forall a_afJ b_afK.
                                             (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                                             [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aWW =
  \ (@ a_afJ)
    (@ b_afK)
    ($dShow_aWq :: GHC.Show.Show a_afJ)
    ($dShow_aWr :: GHC.Show.Show b_afK) ->
    GHC.Show.showList__
      @ (a_afJ TestEq.:*: b_afK)
      (GHC.Show.showsPrec
         @ (a_afJ TestEq.:*: b_afK)
         (TestEq.$fShow:*: @ a_afJ @ b_afK $dShow_aWq $dShow_aWr)
         (GHC.Types.I# 0))
end Rec }

readPrec_aWh :: forall c_afH a_afI.
                (GHC.Read.Read a_afI) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_afH a_afI)
LclId
[Arity 1]
readPrec_aWh =
  \ (@ c_afH) (@ a_afI) ($dRead_aVL :: GHC.Read.Read a_afI) ->
    let {
      readPrec_s1nc :: Text.ParserCombinators.ReadPrec.ReadPrec a_afI
      LclId
      []
      readPrec_s1nc = GHC.Read.readPrec @ a_afI $dRead_aVL } in
    let {
      >>=_s1nb :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1nb =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (TestEq.C c_afH a_afI)
      (Text.ParserCombinators.ReadPrec.prec
         @ (TestEq.C c_afH a_afI)
         (GHC.Types.I# 10)
         (>>=_s1nb
            @ Text.Read.Lex.Lexeme
            @ (TestEq.C c_afH a_afI)
            GHC.Read.lexP
            (\ (ds_d1bO :: Text.Read.Lex.Lexeme) ->
               let {
                 fail_s1na :: GHC.Prim.State# GHC.Prim.RealWorld
                              -> Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_afH a_afI)
                 LclId
                 [Arity 1]
                 fail_s1na =
                   \ _ ->
                     GHC.Base.fail
                       @ Text.ParserCombinators.ReadPrec.ReadPrec
                       Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                       @ (TestEq.C c_afH a_afI)
                       (GHC.Base.unpackCString#
                          "Pattern match failure in do expression at <no location info>") } in
               case ds_d1bO of _ {
                 __DEFAULT -> fail_s1na GHC.Prim.realWorld#;
                 Text.Read.Lex.Ident ds_d1bP ->
                   case ds_d1bP of _ {
                     [] -> fail_s1na GHC.Prim.realWorld#;
                     : ds_d1bQ ds_d1bR ->
                       case ds_d1bQ of _ { GHC.Types.C# ds_d1bS ->
                       case ds_d1bS of _ {
                         __DEFAULT -> fail_s1na GHC.Prim.realWorld#;
                         'C' ->
                           case ds_d1bR of _ {
                             [] ->
                               >>=_s1nb
                                 @ a_afI
                                 @ (TestEq.C c_afH a_afI)
                                 (Text.ParserCombinators.ReadPrec.step @ a_afI readPrec_s1nc)
                                 (\ (a1_ayy :: a_afI) ->
                                    GHC.Base.return
                                      @ Text.ParserCombinators.ReadPrec.ReadPrec
                                      Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                                      @ (TestEq.C c_afH a_afI)
                                      (TestEq.C @ c_afH @ a_afI a1_ayy));
                             : _ _ -> fail_s1na GHC.Prim.realWorld#
                           }
                       }
                       }
                   }
               })))

Rec {
TestEq.$fReadC :: forall c_afH a_afI.
                  (GHC.Read.Read a_afI) =>
                  GHC.Read.Read (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1]
TestEq.$fReadC =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 ($dRead_aVL :: GHC.Read.Read a_afI) ->
                 let {
                   a_s1n5 :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [TestEq.C c_afH a_afI]
                   LclId
                   []
                   a_s1n5 = readListPrec_aWp @ c_afH @ a_afI $dRead_aVL } in
                 let {
                   a_s1n4 :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (TestEq.C c_afH a_afI)
                   LclId
                   []
                   a_s1n4 = readPrec_aWh @ c_afH @ a_afI $dRead_aVL } in
                 let {
                   a_s1n6 :: Text.ParserCombinators.ReadP.ReadS [TestEq.C c_afH a_afI]
                   LclId
                   []
                   a_s1n6 = readList_aVW @ c_afH @ a_afI $dRead_aVL } in
                 letrec {
                   $dRead_s1n2 :: GHC.Read.Read (TestEq.C c_afH a_afI)
                   LclId
                   []
                   $dRead_s1n2 =
                     GHC.Read.D:Read
                       @ (TestEq.C c_afH a_afI) a_s1n3 a_s1n6 a_s1n4 a_s1n5;
                   a_s1n3 [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.C c_afH a_afI)
                   LclId
                   [Arity 1]
                   a_s1n3 =
                     GHC.Read.$dmreadsPrec @ (TestEq.C c_afH a_afI) $dRead_s1n2; } in
                 $dRead_s1n2)
readListPrec_aWp [ALWAYS LoopBreaker Nothing] :: forall c_afH
                                                        a_afI.
                                                 (GHC.Read.Read a_afI) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [TestEq.C c_afH a_afI]
LclId
[Arity 1]
readListPrec_aWp =
  \ (@ c_afH) (@ a_afI) ($dRead_aVL :: GHC.Read.Read a_afI) ->
    GHC.Read.readListPrecDefault
      @ (TestEq.C c_afH a_afI)
      (TestEq.$fReadC @ c_afH @ a_afI $dRead_aVL)
readList_aVW [ALWAYS LoopBreaker Nothing] :: forall c_afH a_afI.
                                             (GHC.Read.Read a_afI) =>
                                             Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.C c_afH a_afI]
LclId
[Arity 1]
readList_aVW =
  \ (@ c_afH) (@ a_afI) ($dRead_aVL :: GHC.Read.Read a_afI) ->
    GHC.Read.readListDefault
      @ (TestEq.C c_afH a_afI)
      (TestEq.$fReadC @ c_afH @ a_afI $dRead_aVL)
end Rec }

showsPrec_aVA :: forall c_afH a_afI.
                 (GHC.Show.Show a_afI) =>
                 GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
LclId
[Arity 3]
showsPrec_aVA =
  \ (@ c_afH)
    (@ a_afI)
    ($dShow_aVm :: GHC.Show.Show a_afI)
    (a_ayw :: GHC.Types.Int)
    (ds_d1bM :: TestEq.C c_afH a_afI) ->
    case ds_d1bM of _ { TestEq.C b1_ayx ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayw (GHC.Types.I# 11))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showString (GHC.Base.unpackCString# "C "))
         (GHC.Show.showsPrec @ a_afI $dShow_aVm (GHC.Types.I# 11) b1_ayx))
    }

Rec {
TestEq.$fShowC :: forall c_afH a_afI.
                  (GHC.Show.Show a_afI) =>
                  GHC.Show.Show (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowC =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 ($dShow_aVm :: GHC.Show.Show a_afI) ->
                 let {
                   a_s1mW :: [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1mW = showList_aVK @ c_afH @ a_afI $dShow_aVm } in
                 let {
                   a_s1mX :: GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1mX = showsPrec_aVA @ c_afH @ a_afI $dShow_aVm } in
                 letrec {
                   $dShow_s1mU :: GHC.Show.Show (TestEq.C c_afH a_afI)
                   LclId
                   []
                   $dShow_s1mU =
                     GHC.Show.D:Show @ (TestEq.C c_afH a_afI) a_s1mX a_s1mV a_s1mW;
                   a_s1mV [ALWAYS LoopBreaker Nothing] :: TestEq.C c_afH a_afI
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1mV =
                     GHC.Show.$dmshow @ (TestEq.C c_afH a_afI) $dShow_s1mU; } in
                 $dShow_s1mU)
showList_aVK [ALWAYS LoopBreaker Nothing] :: forall c_afH a_afI.
                                             (GHC.Show.Show a_afI) =>
                                             [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aVK =
  \ (@ c_afH) (@ a_afI) ($dShow_aVm :: GHC.Show.Show a_afI) ->
    GHC.Show.showList__
      @ (TestEq.C c_afH a_afI)
      (GHC.Show.showsPrec
         @ (TestEq.C c_afH a_afI)
         (TestEq.$fShowC @ c_afH @ a_afI $dShow_aVm)
         (GHC.Types.I# 0))
end Rec }

readPrec_aVd :: forall a_afG.
                (GHC.Read.Read a_afG) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_afG)
LclId
[Arity 1]
readPrec_aVd =
  \ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
    let {
      readPrec_s1mQ :: Text.ParserCombinators.ReadPrec.ReadPrec a_afG
      LclId
      []
      readPrec_s1mQ = GHC.Read.readPrec @ a_afG $dRead_aUH } in
    let {
      >>=_s1mP :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1mP =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (TestEq.Var a_afG)
      (Text.ParserCombinators.ReadPrec.prec
         @ (TestEq.Var a_afG)
         (GHC.Types.I# 10)
         (>>=_s1mP
            @ Text.Read.Lex.Lexeme
            @ (TestEq.Var a_afG)
            GHC.Read.lexP
            (\ (ds_d1bH :: Text.Read.Lex.Lexeme) ->
               let {
                 fail_s1mO :: GHC.Prim.State# GHC.Prim.RealWorld
                              -> Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_afG)
                 LclId
                 [Arity 1]
                 fail_s1mO =
                   \ _ ->
                     GHC.Base.fail
                       @ Text.ParserCombinators.ReadPrec.ReadPrec
                       Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                       @ (TestEq.Var a_afG)
                       (GHC.Base.unpackCString#
                          "Pattern match failure in do expression at <no location info>") } in
               case ds_d1bH of _ {
                 __DEFAULT -> fail_s1mO GHC.Prim.realWorld#;
                 Text.Read.Lex.Ident ds_d1bI ->
                   case GHC.Base.eqString ds_d1bI (GHC.Base.unpackCString# "Var")
                   of _ {
                     GHC.Bool.False -> fail_s1mO GHC.Prim.realWorld#;
                     GHC.Bool.True ->
                       >>=_s1mP
                         @ a_afG
                         @ (TestEq.Var a_afG)
                         (Text.ParserCombinators.ReadPrec.step @ a_afG readPrec_s1mQ)
                         (\ (a1_ayv :: a_afG) ->
                            GHC.Base.return
                              @ Text.ParserCombinators.ReadPrec.ReadPrec
                              Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                              @ (TestEq.Var a_afG)
                              (TestEq.Var @ a_afG a1_ayv))
                   }
               })))

Rec {
TestEq.$fReadVar :: forall a_afG.
                    (GHC.Read.Read a_afG) =>
                    GHC.Read.Read (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1]
TestEq.$fReadVar =
  __inline_me (\ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
                 let {
                   a_s1mJ :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [TestEq.Var a_afG]
                   LclId
                   []
                   a_s1mJ = readListPrec_aVl @ a_afG $dRead_aUH } in
                 let {
                   a_s1mI :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (TestEq.Var a_afG)
                   LclId
                   []
                   a_s1mI = readPrec_aVd @ a_afG $dRead_aUH } in
                 let {
                   a_s1mK :: Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
                   LclId
                   []
                   a_s1mK = readList_aUS @ a_afG $dRead_aUH } in
                 letrec {
                   $dRead_s1mG :: GHC.Read.Read (TestEq.Var a_afG)
                   LclId
                   []
                   $dRead_s1mG =
                     GHC.Read.D:Read @ (TestEq.Var a_afG) a_s1mH a_s1mK a_s1mI a_s1mJ;
                   a_s1mH [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Var a_afG)
                   LclId
                   [Arity 1]
                   a_s1mH =
                     GHC.Read.$dmreadsPrec @ (TestEq.Var a_afG) $dRead_s1mG; } in
                 $dRead_s1mG)
readListPrec_aVl [ALWAYS LoopBreaker Nothing] :: forall a_afG.
                                                 (GHC.Read.Read a_afG) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [TestEq.Var a_afG]
LclId
[Arity 1]
readListPrec_aVl =
  \ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
    GHC.Read.readListPrecDefault
      @ (TestEq.Var a_afG) (TestEq.$fReadVar @ a_afG $dRead_aUH)
readList_aUS [ALWAYS LoopBreaker Nothing] :: forall a_afG.
                                             (GHC.Read.Read a_afG) =>
                                             Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
LclId
[Arity 1]
readList_aUS =
  \ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
    GHC.Read.readListDefault
      @ (TestEq.Var a_afG) (TestEq.$fReadVar @ a_afG $dRead_aUH)
end Rec }

showsPrec_aUw :: forall a_afG.
                 (GHC.Show.Show a_afG) =>
                 GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
LclId
[Arity 3]
showsPrec_aUw =
  \ (@ a_afG)
    ($dShow_aUi :: GHC.Show.Show a_afG)
    (a_ayt :: GHC.Types.Int)
    (ds_d1bF :: TestEq.Var a_afG) ->
    case ds_d1bF of _ { TestEq.Var b1_ayu ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayt (GHC.Types.I# 11))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showString (GHC.Base.unpackCString# "Var "))
         (GHC.Show.showsPrec @ a_afG $dShow_aUi (GHC.Types.I# 11) b1_ayu))
    }

Rec {
TestEq.$fShowVar :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Show.Show (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowVar =
  __inline_me (\ (@ a_afG) ($dShow_aUi :: GHC.Show.Show a_afG) ->
                 let {
                   a_s1mA :: [TestEq.Var a_afG] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1mA = showList_aUG @ a_afG $dShow_aUi } in
                 let {
                   a_s1mB :: GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1mB = showsPrec_aUw @ a_afG $dShow_aUi } in
                 letrec {
                   $dShow_s1my :: GHC.Show.Show (TestEq.Var a_afG)
                   LclId
                   []
                   $dShow_s1my =
                     GHC.Show.D:Show @ (TestEq.Var a_afG) a_s1mB a_s1mz a_s1mA;
                   a_s1mz [ALWAYS LoopBreaker Nothing] :: TestEq.Var a_afG
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1mz = GHC.Show.$dmshow @ (TestEq.Var a_afG) $dShow_s1my; } in
                 $dShow_s1my)
showList_aUG [ALWAYS LoopBreaker Nothing] :: forall a_afG.
                                             (GHC.Show.Show a_afG) =>
                                             [TestEq.Var a_afG] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aUG =
  \ (@ a_afG) ($dShow_aUi :: GHC.Show.Show a_afG) ->
    GHC.Show.showList__
      @ (TestEq.Var a_afG)
      (GHC.Show.showsPrec
         @ (TestEq.Var a_afG)
         (TestEq.$fShowVar @ a_afG $dShow_aUi)
         (GHC.Types.I# 0))
end Rec }

readPrec_aU9 :: forall a_afF.
                (GHC.Read.Read a_afF) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_afF)
LclId
[Arity 1]
readPrec_aU9 =
  \ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
    let {
      readPrec_s1mu :: Text.ParserCombinators.ReadPrec.ReadPrec a_afF
      LclId
      []
      readPrec_s1mu = GHC.Read.readPrec @ a_afF $dRead_aTD } in
    let {
      >>=_s1mt :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1mt =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (TestEq.Rec a_afF)
      (Text.ParserCombinators.ReadPrec.prec
         @ (TestEq.Rec a_afF)
         (GHC.Types.I# 10)
         (>>=_s1mt
            @ Text.Read.Lex.Lexeme
            @ (TestEq.Rec a_afF)
            GHC.Read.lexP
            (\ (ds_d1bA :: Text.Read.Lex.Lexeme) ->
               let {
                 fail_s1ms :: GHC.Prim.State# GHC.Prim.RealWorld
                              -> Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_afF)
                 LclId
                 [Arity 1]
                 fail_s1ms =
                   \ _ ->
                     GHC.Base.fail
                       @ Text.ParserCombinators.ReadPrec.ReadPrec
                       Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                       @ (TestEq.Rec a_afF)
                       (GHC.Base.unpackCString#
                          "Pattern match failure in do expression at <no location info>") } in
               case ds_d1bA of _ {
                 __DEFAULT -> fail_s1ms GHC.Prim.realWorld#;
                 Text.Read.Lex.Ident ds_d1bB ->
                   case GHC.Base.eqString ds_d1bB (GHC.Base.unpackCString# "Rec")
                   of _ {
                     GHC.Bool.False -> fail_s1ms GHC.Prim.realWorld#;
                     GHC.Bool.True ->
                       >>=_s1mt
                         @ a_afF
                         @ (TestEq.Rec a_afF)
                         (Text.ParserCombinators.ReadPrec.step @ a_afF readPrec_s1mu)
                         (\ (a1_ays :: a_afF) ->
                            GHC.Base.return
                              @ Text.ParserCombinators.ReadPrec.ReadPrec
                              Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                              @ (TestEq.Rec a_afF)
                              (TestEq.Rec @ a_afF a1_ays))
                   }
               })))

Rec {
TestEq.$fReadRec :: forall a_afF.
                    (GHC.Read.Read a_afF) =>
                    GHC.Read.Read (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1]
TestEq.$fReadRec =
  __inline_me (\ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
                 let {
                   a_s1mn :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [TestEq.Rec a_afF]
                   LclId
                   []
                   a_s1mn = readListPrec_aUh @ a_afF $dRead_aTD } in
                 let {
                   a_s1mm :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (TestEq.Rec a_afF)
                   LclId
                   []
                   a_s1mm = readPrec_aU9 @ a_afF $dRead_aTD } in
                 let {
                   a_s1mo :: Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
                   LclId
                   []
                   a_s1mo = readList_aTO @ a_afF $dRead_aTD } in
                 letrec {
                   $dRead_s1mk :: GHC.Read.Read (TestEq.Rec a_afF)
                   LclId
                   []
                   $dRead_s1mk =
                     GHC.Read.D:Read @ (TestEq.Rec a_afF) a_s1ml a_s1mo a_s1mm a_s1mn;
                   a_s1ml [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Rec a_afF)
                   LclId
                   [Arity 1]
                   a_s1ml =
                     GHC.Read.$dmreadsPrec @ (TestEq.Rec a_afF) $dRead_s1mk; } in
                 $dRead_s1mk)
readListPrec_aUh [ALWAYS LoopBreaker Nothing] :: forall a_afF.
                                                 (GHC.Read.Read a_afF) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [TestEq.Rec a_afF]
LclId
[Arity 1]
readListPrec_aUh =
  \ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
    GHC.Read.readListPrecDefault
      @ (TestEq.Rec a_afF) (TestEq.$fReadRec @ a_afF $dRead_aTD)
readList_aTO [ALWAYS LoopBreaker Nothing] :: forall a_afF.
                                             (GHC.Read.Read a_afF) =>
                                             Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
LclId
[Arity 1]
readList_aTO =
  \ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
    GHC.Read.readListDefault
      @ (TestEq.Rec a_afF) (TestEq.$fReadRec @ a_afF $dRead_aTD)
end Rec }

showsPrec_aTs :: forall a_afF.
                 (GHC.Show.Show a_afF) =>
                 GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
LclId
[Arity 3]
showsPrec_aTs =
  \ (@ a_afF)
    ($dShow_aTe :: GHC.Show.Show a_afF)
    (a_ayq :: GHC.Types.Int)
    (ds_d1by :: TestEq.Rec a_afF) ->
    case ds_d1by of _ { TestEq.Rec b1_ayr ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayq (GHC.Types.I# 11))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showString (GHC.Base.unpackCString# "Rec "))
         (GHC.Show.showsPrec @ a_afF $dShow_aTe (GHC.Types.I# 11) b1_ayr))
    }

Rec {
TestEq.$fShowRec :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Show.Show (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowRec =
  __inline_me (\ (@ a_afF) ($dShow_aTe :: GHC.Show.Show a_afF) ->
                 let {
                   a_s1me :: [TestEq.Rec a_afF] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1me = showList_aTC @ a_afF $dShow_aTe } in
                 let {
                   a_s1mf :: GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1mf = showsPrec_aTs @ a_afF $dShow_aTe } in
                 letrec {
                   $dShow_s1mc :: GHC.Show.Show (TestEq.Rec a_afF)
                   LclId
                   []
                   $dShow_s1mc =
                     GHC.Show.D:Show @ (TestEq.Rec a_afF) a_s1mf a_s1md a_s1me;
                   a_s1md [ALWAYS LoopBreaker Nothing] :: TestEq.Rec a_afF
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1md = GHC.Show.$dmshow @ (TestEq.Rec a_afF) $dShow_s1mc; } in
                 $dShow_s1mc)
showList_aTC [ALWAYS LoopBreaker Nothing] :: forall a_afF.
                                             (GHC.Show.Show a_afF) =>
                                             [TestEq.Rec a_afF] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aTC =
  \ (@ a_afF) ($dShow_aTe :: GHC.Show.Show a_afF) ->
    GHC.Show.showList__
      @ (TestEq.Rec a_afF)
      (GHC.Show.showsPrec
         @ (TestEq.Rec a_afF)
         (TestEq.$fShowRec @ a_afF $dShow_aTe)
         (GHC.Types.I# 0))
end Rec }

readPrec_aSX :: Text.ParserCombinators.ReadPrec.ReadPrec
                  GHC.Types.Int
LclId
[]
readPrec_aSX = GHC.Read.readPrec @ GHC.Types.Int GHC.Read.$fReadInt

return_aSF :: forall a_aPP.
              a_aPP -> Text.ParserCombinators.ReadPrec.ReadPrec a_aPP
LclId
[]
return_aSF =
  GHC.Base.return
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

fail_aSE :: forall a_aSD.
            GHC.Base.String -> Text.ParserCombinators.ReadPrec.ReadPrec a_aSD
LclId
[]
fail_aSE =
  GHC.Base.fail
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

>>=_aSC :: forall a_aSA b_aSB.
           Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
           -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
           -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
LclId
[]
>>=_aSC =
  GHC.Base.>>=
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

return_aPQ :: forall a_aPP.
              a_aPP -> Text.ParserCombinators.ReadPrec.ReadPrec a_aPP
LclId
[]
return_aPQ =
  GHC.Base.return
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

a_s1iD :: Text.ParserCombinators.ReadPrec.ReadPrec
            TestEq.Associativity
LclId
[]
a_s1iD =
  GHC.Read.choose
    @ TestEq.Associativity
    (GHC.Base.build
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
       (\ (@ a_d1b1)
          (c_d1b2 :: (GHC.Base.String,
                      Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                     -> a_d1b1
                     -> a_d1b1)
          (n_d1b3 :: a_d1b1) ->
          c_d1b2
            (GHC.Base.unpackCString# "LeftAssociative",
             return_aPQ @ TestEq.Associativity TestEq.LeftAssociative)
            (c_d1b2
               (GHC.Base.unpackCString# "RightAssociative",
                return_aPQ @ TestEq.Associativity TestEq.RightAssociative)
               (c_d1b2
                  (GHC.Base.unpackCString# "NotAssociative",
                   return_aPQ @ TestEq.Associativity TestEq.NotAssociative)
                  n_d1b3))))

readPrec_aPY :: Text.ParserCombinators.ReadPrec.ReadPrec
                  TestEq.Associativity
LclId
[Arity 1]
readPrec_aPY = GHC.Read.parens @ TestEq.Associativity a_s1iD

Rec {
TestEq.$fReadAssociativity :: GHC.Read.Read TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fReadAssociativity =
  GHC.Read.D:Read
    @ TestEq.Associativity
    a_s1iF
    readListDefault_aPz
    readPrec_aPY
    readListPrecDefault_aQ3
a_s1iF [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                       -> Text.ParserCombinators.ReadP.ReadS TestEq.Associativity
LclId
[Arity 1]
a_s1iF =
  GHC.Read.$dmreadsPrec
    @ TestEq.Associativity TestEq.$fReadAssociativity
readListPrecDefault_aQ3 [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.ReadPrec
                                                          [TestEq.Associativity]
LclId
[]
readListPrecDefault_aQ3 =
  GHC.Read.readListPrecDefault
    @ TestEq.Associativity TestEq.$fReadAssociativity
readListDefault_aPz [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadP.ReadS
                                                      [TestEq.Associativity]
LclId
[]
readListDefault_aPz =
  GHC.Read.readListDefault
    @ TestEq.Associativity TestEq.$fReadAssociativity
end Rec }

readPrec_aSQ :: Text.ParserCombinators.ReadPrec.ReadPrec
                  TestEq.Associativity
LclId
[]
readPrec_aSQ =
  GHC.Read.readPrec @ TestEq.Associativity TestEq.$fReadAssociativity

a_s1iI :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[]
a_s1iI =
  >>=_aSC
    @ Text.Read.Lex.Lexeme
    @ TestEq.Fixity
    GHC.Read.lexP
    (\ (ds_d1bo :: Text.Read.Lex.Lexeme) ->
       let {
         fail_s1m8 :: GHC.Prim.State# GHC.Prim.RealWorld
                      -> Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
         LclId
         [Arity 1]
         fail_s1m8 =
           \ _ ->
             fail_aSE
               @ TestEq.Fixity
               (GHC.Base.unpackCString#
                  "Pattern match failure in do expression at <no location info>") } in
       case ds_d1bo of _ {
         __DEFAULT -> fail_s1m8 GHC.Prim.realWorld#;
         Text.Read.Lex.Ident ds_d1bp ->
           case GHC.Base.eqString ds_d1bp (GHC.Base.unpackCString# "Prefix")
           of _ {
             GHC.Bool.False -> fail_s1m8 GHC.Prim.realWorld#;
             GHC.Bool.True -> return_aSF @ TestEq.Fixity TestEq.Prefix
           }
       })

a_s1iL :: GHC.Types.Int
LclId
[]
a_s1iL = GHC.Types.I# 10

a_s1iN :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[]
a_s1iN =
  >>=_aSC
    @ Text.Read.Lex.Lexeme
    @ TestEq.Fixity
    GHC.Read.lexP
    (\ (ds_d1bt :: Text.Read.Lex.Lexeme) ->
       let {
         fail_s1m6 :: GHC.Prim.State# GHC.Prim.RealWorld
                      -> Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
         LclId
         [Arity 1]
         fail_s1m6 =
           \ _ ->
             fail_aSE
               @ TestEq.Fixity
               (GHC.Base.unpackCString#
                  "Pattern match failure in do expression at <no location info>") } in
       case ds_d1bt of _ {
         __DEFAULT -> fail_s1m6 GHC.Prim.realWorld#;
         Text.Read.Lex.Ident ds_d1bu ->
           case GHC.Base.eqString ds_d1bu (GHC.Base.unpackCString# "Infix")
           of _ {
             GHC.Bool.False -> fail_s1m6 GHC.Prim.realWorld#;
             GHC.Bool.True ->
               >>=_aSC
                 @ TestEq.Associativity
                 @ TestEq.Fixity
                 (Text.ParserCombinators.ReadPrec.step
                    @ TestEq.Associativity readPrec_aSQ)
                 (\ (a1_axJ :: TestEq.Associativity) ->
                    >>=_aSC
                      @ GHC.Types.Int
                      @ TestEq.Fixity
                      (Text.ParserCombinators.ReadPrec.step @ GHC.Types.Int readPrec_aSX)
                      (\ (a2_axK :: GHC.Types.Int) ->
                         return_aSF @ TestEq.Fixity (TestEq.Infix a1_axJ a2_axK)))
           }
       })

a_s1iO :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[Arity 1]
a_s1iO =
  Text.ParserCombinators.ReadPrec.prec @ TestEq.Fixity a_s1iL a_s1iN

a_s1iP :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[Arity 1]
a_s1iP =
  Text.ParserCombinators.ReadPrec.+++ @ TestEq.Fixity a_s1iI a_s1iO

readPrec_aT7 :: Text.ParserCombinators.ReadPrec.ReadPrec
                  TestEq.Fixity
LclId
[Arity 1]
readPrec_aT7 = GHC.Read.parens @ TestEq.Fixity a_s1iP

Rec {
TestEq.$fReadFixity :: GHC.Read.Read TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fReadFixity =
  GHC.Read.D:Read
    @ TestEq.Fixity
    a_s1iR
    readListDefault_aRx
    readPrec_aT7
    readListPrecDefault_aTb
a_s1iR [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                       -> Text.ParserCombinators.ReadP.ReadS TestEq.Fixity
LclId
[Arity 1]
a_s1iR = GHC.Read.$dmreadsPrec @ TestEq.Fixity TestEq.$fReadFixity
readListPrecDefault_aTb [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.ReadPrec
                                                          [TestEq.Fixity]
LclId
[]
readListPrecDefault_aTb =
  GHC.Read.readListPrecDefault @ TestEq.Fixity TestEq.$fReadFixity
readListDefault_aRx [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadP.ReadS
                                                      [TestEq.Fixity]
LclId
[]
readListDefault_aRx =
  GHC.Read.readListDefault @ TestEq.Fixity TestEq.$fReadFixity
end Rec }

compare_aK1 :: TestEq.Associativity
               -> TestEq.Associativity
               -> GHC.Ordering.Ordering
LclId
[Arity 2]
compare_aK1 =
  \ (a_axg :: TestEq.Associativity)
    (b_axh :: TestEq.Associativity) ->
    case TestEq.$con2tag_Associativity a_axg of a#_axj { __DEFAULT ->
    case TestEq.$con2tag_Associativity b_axh of b#_axk { __DEFAULT ->
    case GHC.Prim.==# a#_axj b#_axk of _ {
      GHC.Bool.False ->
        case GHC.Prim.<# a#_axj b#_axk of _ {
          GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
        };
      GHC.Bool.True -> GHC.Ordering.EQ
    }
    }
    }

showsPrec_aJp :: GHC.Types.Int
                 -> TestEq.Associativity
                 -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aJp =
  \ _ (ds_d1aU :: TestEq.Associativity) ->
    case ds_d1aU of _ {
      TestEq.LeftAssociative ->
        GHC.Show.showString (GHC.Base.unpackCString# "LeftAssociative");
      TestEq.RightAssociative ->
        GHC.Show.showString (GHC.Base.unpackCString# "RightAssociative");
      TestEq.NotAssociative ->
        GHC.Show.showString (GHC.Base.unpackCString# "NotAssociative")
    }

Rec {
TestEq.$fShowAssociativity :: GHC.Show.Show TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fShowAssociativity =
  GHC.Show.D:Show
    @ TestEq.Associativity showsPrec_aJp a_s1iV showList_aJx
a_s1iV [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> GHC.Base.String
LclId
[]
a_s1iV =
  GHC.Show.$dmshow @ TestEq.Associativity TestEq.$fShowAssociativity
a_s1iX :: TestEq.Associativity -> GHC.Show.ShowS
LclId
[]
a_s1iX =
  GHC.Show.showsPrec
    @ TestEq.Associativity TestEq.$fShowAssociativity (GHC.Types.I# 0)
showList_aJx [ALWAYS LoopBreaker Nothing] :: [TestEq.Associativity]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aJx = GHC.Show.showList__ @ TestEq.Associativity a_s1iX
end Rec }

showsPrec_aQK :: GHC.Types.Int -> TestEq.Fixity -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aQK =
  \ (ds_d1ba :: GHC.Types.Int) (ds_d1bb :: TestEq.Fixity) ->
    case ds_d1bb of _ {
      TestEq.Prefix ->
        GHC.Show.showString (GHC.Base.unpackCString# "Prefix");
      TestEq.Infix b1_axw b2_axx ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt ds_d1ba (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Infix "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (GHC.Show.showsPrec
                   @ TestEq.Associativity
                   TestEq.$fShowAssociativity
                   (GHC.Types.I# 11)
                   b1_axw)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (GHC.Show.showsPrec
                      @ GHC.Types.Int GHC.Show.$fShowInt (GHC.Types.I# 11) b2_axx))))
    }

Rec {
TestEq.$fShowFixity :: GHC.Show.Show TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fShowFixity =
  GHC.Show.D:Show @ TestEq.Fixity showsPrec_aQK a_s1j0 showList_aQS
a_s1j0 [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> GHC.Base.String
LclId
[]
a_s1j0 = GHC.Show.$dmshow @ TestEq.Fixity TestEq.$fShowFixity
a_s1j2 :: TestEq.Fixity -> GHC.Show.ShowS
LclId
[]
a_s1j2 =
  GHC.Show.showsPrec
    @ TestEq.Fixity TestEq.$fShowFixity (GHC.Types.I# 0)
showList_aQS [ALWAYS LoopBreaker Nothing] :: [TestEq.Fixity]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aQS = GHC.Show.showList__ @ TestEq.Fixity a_s1j2
end Rec }

==_aJd :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2]
==_aJd =
  \ (a_axa :: TestEq.Associativity)
    (b_axb :: TestEq.Associativity) ->
    case TestEq.$con2tag_Associativity a_axa of a#_axc { __DEFAULT ->
    case TestEq.$con2tag_Associativity b_axb of b#_axd { __DEFAULT ->
    GHC.Prim.==# a#_axc b#_axd
    }
    }

Rec {
TestEq.$fEqAssociativity :: GHC.Classes.Eq TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fEqAssociativity =
  GHC.Classes.D:Eq @ TestEq.Associativity ==_aJd /=_aJk
/=_aJk [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
/=_aJk =
  \ (a_axe :: TestEq.Associativity)
    (b_axf :: TestEq.Associativity) ->
    GHC.Classes.not
      (GHC.Classes.==
         @ TestEq.Associativity TestEq.$fEqAssociativity a_axe b_axf)
end Rec }

Rec {
TestEq.$fOrdAssociativity :: GHC.Classes.Ord TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ TestEq.Associativity
    TestEq.$fEqAssociativity
    compare_aK1
    a_s1kS
    a_s1kU
    a_s1kW
    a_s1kY
    a_s1l0
    a_s1l2
a_s1l2 [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> TestEq.Associativity
LclId
[Arity 2]
a_s1l2 =
  GHC.Classes.$dmmin @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1l0 [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> TestEq.Associativity
LclId
[Arity 2]
a_s1l0 =
  GHC.Classes.$dmmax @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kY [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kY =
  GHC.Classes.$dm<= @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kW [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kW =
  GHC.Classes.$dm> @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kU [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kU =
  GHC.Classes.$dm>= @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kS [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kS =
  GHC.Classes.$dm< @ TestEq.Associativity TestEq.$fOrdAssociativity
end Rec }

compare_aRe :: TestEq.Fixity
               -> TestEq.Fixity
               -> GHC.Ordering.Ordering
LclId
[Arity 2]
compare_aRe =
  \ (a_axy :: TestEq.Fixity) (b_axz :: TestEq.Fixity) ->
    case TestEq.$con2tag_Fixity a_axy of a#_axF { __DEFAULT ->
    case TestEq.$con2tag_Fixity b_axz of b#_axG { __DEFAULT ->
    case GHC.Prim.==# a#_axF b#_axG of _ {
      GHC.Bool.False ->
        case GHC.Prim.<# a#_axF b#_axG of _ {
          GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
        };
      GHC.Bool.True ->
        case a_axy of _ {
          TestEq.Prefix -> GHC.Ordering.EQ;
          TestEq.Infix a1_axB a2_axC ->
            case b_axz of _ {
              TestEq.Prefix -> GHC.Ordering.EQ;
              TestEq.Infix b1_axD b2_axE ->
                case GHC.Classes.compare
                       @ TestEq.Associativity TestEq.$fOrdAssociativity a1_axB b1_axD
                of _ {
                  GHC.Ordering.LT -> GHC.Ordering.LT;
                  GHC.Ordering.EQ ->
                    GHC.Classes.compare
                      @ GHC.Types.Int GHC.Base.$fOrdInt a2_axC b2_axE;
                  GHC.Ordering.GT -> GHC.Ordering.GT
                }
            }
        }
    }
    }
    }

==_aQi :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2]
==_aQi =
  \ (ds_d1b4 :: TestEq.Fixity) (ds_d1b5 :: TestEq.Fixity) ->
    let {
      fail_s1m4 :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1]
      fail_s1m4 =
        \ _ ->
          case TestEq.$con2tag_Fixity ds_d1b4 of a#_axr { __DEFAULT ->
          case TestEq.$con2tag_Fixity ds_d1b5 of b#_axs { __DEFAULT ->
          GHC.Prim.==# a#_axr b#_axs
          }
          } } in
    case ds_d1b4 of _ {
      TestEq.Prefix -> fail_s1m4 GHC.Prim.realWorld#;
      TestEq.Infix a1_axl a2_axm ->
        case ds_d1b5 of _ {
          TestEq.Prefix -> fail_s1m4 GHC.Prim.realWorld#;
          TestEq.Infix b1_axn b2_axo ->
            GHC.Classes.&&
              (GHC.Classes.==
                 @ TestEq.Associativity TestEq.$fEqAssociativity a1_axl b1_axn)
              (GHC.Classes.== @ GHC.Types.Int GHC.Base.$fEqInt a2_axm b2_axo)
        }
    }

Rec {
TestEq.$fEqFixity :: GHC.Classes.Eq TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fEqFixity = GHC.Classes.D:Eq @ TestEq.Fixity ==_aQi /=_aQp
/=_aQp [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
/=_aQp =
  \ (a_axt :: TestEq.Fixity) (b_axu :: TestEq.Fixity) ->
    GHC.Classes.not
      (GHC.Classes.== @ TestEq.Fixity TestEq.$fEqFixity a_axt b_axu)
end Rec }

Rec {
TestEq.$fOrdFixity :: GHC.Classes.Ord TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fOrdFixity =
  GHC.Classes.D:Ord
    @ TestEq.Fixity
    TestEq.$fEqFixity
    compare_aRe
    a_s1lc
    a_s1le
    a_s1lg
    a_s1li
    a_s1lk
    a_s1lm
a_s1lm [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> TestEq.Fixity
LclId
[Arity 2]
a_s1lm = GHC.Classes.$dmmin @ TestEq.Fixity TestEq.$fOrdFixity
a_s1lk [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> TestEq.Fixity
LclId
[Arity 2]
a_s1lk = GHC.Classes.$dmmax @ TestEq.Fixity TestEq.$fOrdFixity
a_s1li [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1li = GHC.Classes.$dm<= @ TestEq.Fixity TestEq.$fOrdFixity
a_s1lg [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1lg = GHC.Classes.$dm> @ TestEq.Fixity TestEq.$fOrdFixity
a_s1le [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1le = GHC.Classes.$dm>= @ TestEq.Fixity TestEq.$fOrdFixity
a_s1lc [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1lc = GHC.Classes.$dm< @ TestEq.Fixity TestEq.$fOrdFixity
end Rec }

Rec {
TestEq.$fEqLogic0 :: GHC.Classes.Eq TestEq.Logic
LclIdX[DFunId]
[]
TestEq.$fEqLogic0 = GHC.Classes.D:Eq @ TestEq.Logic ==_aIY /=_aJ6
/=_aJ6 [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> TestEq.Logic
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
/=_aJ6 =
  \ (a_ax8 :: TestEq.Logic) (b_ax9 :: TestEq.Logic) ->
    GHC.Classes.not
      (GHC.Classes.== @ TestEq.Logic TestEq.$fEqLogic0 a_ax8 b_ax9)
==_aIB :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclId
[]
==_aIB = GHC.Classes.== @ TestEq.Logic TestEq.$fEqLogic0
==_aIY [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> TestEq.Logic
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
==_aIY =
  \ (ds_d1aD :: TestEq.Logic) (ds_d1aE :: TestEq.Logic) ->
    let {
      fail_s1m2 :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1]
      fail_s1m2 =
        \ _ ->
          case TestEq.$con2tag_Logic ds_d1aD of a#_ax6 { __DEFAULT ->
          case TestEq.$con2tag_Logic ds_d1aE of b#_ax7 { __DEFAULT ->
          GHC.Prim.==# a#_ax6 b#_ax7
          }
          } } in
    case ds_d1aD of _ {
      __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
      TestEq.VarL a1_awK ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.VarL b1_awL -> GHC.Base.eqString a1_awK b1_awL
        };
      TestEq.Not a1_awM ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Not b1_awN -> ==_aIB a1_awM b1_awN
        };
      TestEq.Impl a1_awO a2_awP ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Impl b1_awQ b2_awR ->
            GHC.Classes.&& (==_aIB a1_awO b1_awQ) (==_aIB a2_awP b2_awR)
        };
      TestEq.Equiv a1_awS a2_awT ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Equiv b1_awU b2_awV ->
            GHC.Classes.&& (==_aIB a1_awS b1_awU) (==_aIB a2_awT b2_awV)
        };
      TestEq.Conj a1_awW a2_awX ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Conj b1_awY b2_awZ ->
            GHC.Classes.&& (==_aIB a1_awW b1_awY) (==_aIB a2_awX b2_awZ)
        };
      TestEq.Disj a1_ax0 a2_ax1 ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Disj b1_ax2 b2_ax3 ->
            GHC.Classes.&& (==_aIB a1_ax0 b1_ax2) (==_aIB a2_ax1 b2_ax3)
        }
    }
end Rec }

showsPrec_aHl :: GHC.Types.Int -> GHC.Base.String -> GHC.Show.ShowS
LclId
[]
showsPrec_aHl =
  GHC.Show.showsPrec
    @ GHC.Base.String
    (GHC.Show.$fShow[] @ GHC.Types.Char GHC.Show.$fShowChar)

Rec {
TestEq.$fShowLogic :: GHC.Show.Show TestEq.Logic
LclIdX[DFunId]
[]
TestEq.$fShowLogic =
  GHC.Show.D:Show @ TestEq.Logic showsPrec_aIk a_s1ly showList_aIt
a_s1ly [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> GHC.Base.String
LclId
[]
a_s1ly = GHC.Show.$dmshow @ TestEq.Logic TestEq.$fShowLogic
a_s1lA :: TestEq.Logic -> GHC.Show.ShowS
LclId
[]
a_s1lA =
  GHC.Show.showsPrec
    @ TestEq.Logic TestEq.$fShowLogic (GHC.Types.I# 0)
showList_aIt [ALWAYS LoopBreaker Nothing] :: [TestEq.Logic]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aIt = GHC.Show.showList__ @ TestEq.Logic a_s1lA
showsPrec_aHr :: GHC.Types.Int -> TestEq.Logic -> GHC.Show.ShowS
LclId
[]
showsPrec_aHr =
  GHC.Show.showsPrec @ TestEq.Logic TestEq.$fShowLogic
showsPrec_aIk [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                              -> TestEq.Logic
                                              -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aIk =
  \ (a_awu :: GHC.Types.Int) (ds_d1au :: TestEq.Logic) ->
    case ds_d1au of _ {
      TestEq.VarL b1_awv ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "VarL "))
             (showsPrec_aHl (GHC.Types.I# 11) b1_awv));
      TestEq.T ->
        GHC.Show.showString
          (GHC.Types.:
             @ GHC.Types.Char
             (GHC.Types.C# 'T')
             (GHC.Types.[] @ GHC.Types.Char));
      TestEq.F ->
        GHC.Show.showString
          (GHC.Types.:
             @ GHC.Types.Char
             (GHC.Types.C# 'F')
             (GHC.Types.[] @ GHC.Types.Char));
      TestEq.Not b1_awx ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Not "))
             (showsPrec_aHr (GHC.Types.I# 11) b1_awx));
      TestEq.Impl b1_awz b2_awA ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Impl "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awz)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awA))));
      TestEq.Equiv b1_awC b2_awD ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Equiv "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awC)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awD))));
      TestEq.Conj b1_awF b2_awG ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Conj "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awF)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awG))));
      TestEq.Disj b1_awI b2_awJ ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Disj "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awI)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awJ))))
    }
end Rec }

Rec {
TestEq.$fEqTree0 :: forall a_afw.
                    (GHC.Classes.Eq a_afw) =>
                    GHC.Classes.Eq (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1]
TestEq.$fEqTree0 =
  __inline_me (\ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
                 GHC.Classes.D:Eq
                   @ (TestEq.Tree a_afw)
                   (==_aH2 @ a_afw $dEq_aGJ)
                   (/=_aHb @ a_afw $dEq_aGJ))
/=_aHb [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                       (GHC.Classes.Eq a_afw) =>
                                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1]
/=_aHb =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1lW :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      []
      ==_s1lW =
        GHC.Classes.==
          @ (TestEq.Tree a_afw) (TestEq.$fEqTree0 @ a_afw $dEq_aGJ) } in
    \ (a_aws :: TestEq.Tree a_afw) (b_awt :: TestEq.Tree a_afw) ->
      GHC.Classes.not (==_s1lW a_aws b_awt)
==_aH2 [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                       (GHC.Classes.Eq a_afw) =>
                                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1]
==_aH2 =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1m0 :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      []
      ==_s1m0 =
        GHC.Classes.==
          @ (TestEq.Tree a_afw) (TestEq.$fEqTree0 @ a_afw $dEq_aGJ) } in
    \ (ds_d1ao :: TestEq.Tree a_afw) (ds_d1ap :: TestEq.Tree a_afw) ->
      let {
        fail_s1lZ :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
        LclId
        [Arity 1]
        fail_s1lZ =
          \ _ ->
            case TestEq.$con2tag_Tree @ a_afw ds_d1ao of a#_awq { __DEFAULT ->
            case TestEq.$con2tag_Tree @ a_afw ds_d1ap of b#_awr { __DEFAULT ->
            GHC.Prim.==# a#_awq b#_awr
            }
            } } in
      case ds_d1ao of _ {
        TestEq.Leaf -> fail_s1lZ GHC.Prim.realWorld#;
        TestEq.Bin a1_awi a2_awj a3_awk ->
          case ds_d1ap of _ {
            TestEq.Leaf -> fail_s1lZ GHC.Prim.realWorld#;
            TestEq.Bin b1_awl b2_awm b3_awn ->
              GHC.Classes.&&
                (GHC.Classes.&&
                   (GHC.Classes.== @ a_afw $dEq_aGJ a1_awi b1_awl)
                   (==_s1m0 a2_awj b2_awm))
                (==_s1m0 a3_awk b3_awn)
          }
      }
end Rec }

Rec {
TestEq.$fShowTree :: forall a_afw.
                     (GHC.Show.Show a_afw) =>
                     GHC.Show.Show (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowTree =
  __inline_me (\ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
                 let {
                   a_s1lR :: [TestEq.Tree a_afw] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1lR = showList_aGI @ a_afw $dShow_aFW } in
                 let {
                   a_s1lS :: GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1lS = showsPrec_aGw @ a_afw $dShow_aFW } in
                 letrec {
                   $dShow_s1lP :: GHC.Show.Show (TestEq.Tree a_afw)
                   LclId
                   []
                   $dShow_s1lP =
                     GHC.Show.D:Show @ (TestEq.Tree a_afw) a_s1lS a_s1lQ a_s1lR;
                   a_s1lQ [ALWAYS LoopBreaker Nothing] :: TestEq.Tree a_afw
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1lQ = GHC.Show.$dmshow @ (TestEq.Tree a_afw) $dShow_s1lP; } in
                 $dShow_s1lP)
showList_aGI [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                             (GHC.Show.Show a_afw) =>
                                             [TestEq.Tree a_afw] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aGI =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    GHC.Show.showList__
      @ (TestEq.Tree a_afw)
      (GHC.Show.showsPrec
         @ (TestEq.Tree a_afw)
         (TestEq.$fShowTree @ a_afw $dShow_aFW)
         (GHC.Types.I# 0))
showsPrec_aGw [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                              (GHC.Show.Show a_afw) =>
                                              GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
LclId
[Arity 1]
showsPrec_aGw =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    let {
      showsPrec_s1lU :: GHC.Types.Int
                        -> TestEq.Tree a_afw
                        -> GHC.Show.ShowS
      LclId
      []
      showsPrec_s1lU =
        GHC.Show.showsPrec
          @ (TestEq.Tree a_afw) (TestEq.$fShowTree @ a_afw $dShow_aFW) } in
    \ (ds_d1ak :: GHC.Types.Int) (ds_d1al :: TestEq.Tree a_afw) ->
      case ds_d1al of _ {
        TestEq.Leaf ->
          GHC.Show.showString (GHC.Base.unpackCString# "Leaf");
        TestEq.Bin b1_awf b2_awg b3_awh ->
          GHC.Show.showParen
            (GHC.Classes.>=
               @ GHC.Types.Int GHC.Base.$fOrdInt ds_d1ak (GHC.Types.I# 11))
            (GHC.Base..
               @ GHC.Base.String
               @ GHC.Base.String
               @ GHC.Base.String
               (GHC.Show.showString (GHC.Base.unpackCString# "Bin "))
               (GHC.Base..
                  @ GHC.Base.String
                  @ GHC.Base.String
                  @ GHC.Base.String
                  (GHC.Show.showsPrec @ a_afw $dShow_aFW (GHC.Types.I# 11) b1_awf)
                  (GHC.Base..
                     @ GHC.Base.String
                     @ GHC.Base.String
                     @ GHC.Base.String
                     GHC.Show.showSpace
                     (GHC.Base..
                        @ GHC.Base.String
                        @ GHC.Base.String
                        @ GHC.Base.String
                        (showsPrec_s1lU (GHC.Types.I# 11) b2_awg)
                        (GHC.Base..
                           @ GHC.Base.String
                           @ GHC.Base.String
                           @ GHC.Base.String
                           GHC.Show.showSpace
                           (showsPrec_s1lU (GHC.Types.I# 11) b3_awh))))))
      }
end Rec }

TestEq.t1 :: GHC.Bool.Bool
LclIdX
[]
TestEq.t1 = TestEq.eqTree TestEq.tree1 TestEq.tree1

TestEq.t2 :: GHC.Bool.Bool
LclIdX
[]
TestEq.t2 = TestEq.eqLogic TestEq.logic1 TestEq.logic1




==================== SpecConstr ====================
$wa_s1LG :: forall b_a1zG.
            (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
            -> Text.ParserCombinators.ReadP.P b_a1zG
LclId
[Arity 1
 Str: DmdType L]
$wa_s1LG =
  \ (@ b_a1zG)
    (w_s1Ki :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG) ->
    Text.Read.Lex.lex1
      @ b_a1zG
      (let {
         lvl_s1Ow :: Text.ParserCombinators.ReadP.P b_a1zG
         LclId
         []
         lvl_s1Ow = w_s1Ki TestEq.U } in
       \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
         case a3_a1A6 of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
           Text.Read.Lex.Ident ds_d1cz [ALWAYS Just S] ->
             case ds_d1cz of _ {
               [] -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
               : ds_d1cA [ALWAYS Just U(L)] ds_d1cB ->
                 case ds_d1cA of _ { GHC.Types.C# ds_d1cC [ALWAYS Just L] ->
                 case ds_d1cC of _ {
                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                   'U' ->
                     case ds_d1cB of _ {
                       [] -> lvl_s1Ow; : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1zG
                     }
                 }
                 }
             }
         })

a_s1JA :: Text.ParserCombinators.ReadPrec.Prec
          -> forall b_a1zG.
             (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
             -> Text.ParserCombinators.ReadP.P b_a1zG
LclId
[Arity 2
 Worker $wa_s1LG
 Str: DmdType AL]
a_s1JA =
  __inline_me (\ _
                 (@ b_a1zG)
                 (w_s1Ki :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG) ->
                 $wa_s1LG @ b_a1zG w_s1Ki)

a_s1HU :: GHC.Types.Int
LclId
[Str: DmdType m]
a_s1HU = GHC.Types.I# 0

lvl_s1Ox :: GHC.Types.Int
LclId
[]
lvl_s1Ox = GHC.Types.I# 11

a_s1Ei :: forall c_Xu8 a_Xua.
          (GHC.Read.Read a_Xua) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xu8 a_Xua)
LclId
[Arity 2
 Str: DmdType LL]
a_s1Ei =
  \ (@ c_Xu8)
    (@ a_Xua)
    ($dRead_aVL [ALWAYS Just L] :: GHC.Read.Read a_Xua)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.C c_Xu8 a_Xua)
      ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta_X7U [ALWAYS Just L] :: TestEq.C c_Xu8 a_Xua
                                      -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1CC 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.C c_Xu8 a_Xua) @ b_a1zG eta_X7U);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1zG
                (let {
                   lvl_s1Oy :: a_Xua -> Text.ParserCombinators.ReadP.P b_a1zG
                   LclId
                   [Arity 1]
                   lvl_s1Oy =
                     \ (a3_X1Pe :: a_Xua) ->
                       eta_X7U (TestEq.C @ c_Xu8 @ a_Xua a3_X1Pe) } in
                 \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                   case a3_a1A6 of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                     Text.Read.Lex.Ident ds_d1bP [ALWAYS Just S] ->
                       case ds_d1bP of _ {
                         [] -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         : ds_d1bQ [ALWAYS Just U(L)] ds_d1bR ->
                           case ds_d1bQ of _ { GHC.Types.C# ds_d1bS [ALWAYS Just L] ->
                           case ds_d1bS of _ {
                             __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                             'C' ->
                               case ds_d1bR of _ {
                                 [] ->
                                   case $dRead_aVL
                                   of _ { GHC.Read.D:Read _ _ tpl_XeI [ALWAYS Just C(C(S))] _ ->
                                   (((tpl_XeI
                                      `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xua
                                              :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xua
                                                   ~
                                                 (Text.ParserCombinators.ReadPrec.Prec
                                                  -> Text.ParserCombinators.ReadP.ReadP a_Xua)))
                                       lvl_s1Ox)
                                    `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xua
                                            :: Text.ParserCombinators.ReadP.ReadP a_Xua
                                                 ~
                                               (forall b_a1zG.
                                                (a_Xua -> Text.ParserCombinators.ReadP.P b_a1zG)
                                                -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                     @ b_a1zG lvl_s1Oy
                                   };
                                 : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1zG
                               }
                           }
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.C c_Xu8 a_Xua)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (TestEq.C c_Xu8 a_Xua)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.C c_Xu8 a_Xua -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xu8 a_Xua)))
      eta_B1

a_s1Em :: forall c_Xud a_Xuf.
          (GHC.Read.Read a_Xuf) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xud a_Xuf]
LclId
[Arity 1
 Str: DmdType L]
a_s1Em =
  \ (@ c_Xud) (@ a_Xuf) ($dRead_X1aj :: GHC.Read.Read a_Xuf) ->
    GHC.Read.$dmreadList2
      @ (TestEq.C c_Xud a_Xuf)
      ((a_s1Ei @ c_Xud @ a_Xuf $dRead_X1aj)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (TestEq.C c_Xud a_Xuf))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xud a_Xuf))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xud a_Xuf)))

a_s1Df :: forall a_Xt9 b_Xtb.
          (GHC.Read.Read a_Xt9, GHC.Read.Read b_Xtb) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (a_Xt9 TestEq.:+: b_Xtb)
LclId
[Arity 2
 Str: DmdType LL]
a_s1Df =
  \ (@ a_Xt9)
    (@ b_Xtb)
    ($dRead_aYo [ALWAYS Just L] :: GHC.Read.Read a_Xt9)
    ($dRead_aYp [ALWAYS Just L] :: GHC.Read.Read b_Xtb) ->
    GHC.Read.$fRead()5
      @ (a_Xt9 TestEq.:+: b_Xtb)
      ((Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
          @ (a_Xt9 TestEq.:+: b_Xtb)
          ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b_a1zG)
              (eta_B1 [ALWAYS Just L] :: (a_Xt9 TestEq.:+: b_Xtb)
                                         -> Text.ParserCombinators.ReadP.P b_a1zG) ->
              case c_a1Cz of _ { GHC.Types.I# x_a1CC [ALWAYS Just L] ->
              case GHC.Prim.<=# x_a1CC 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xt9 TestEq.:+: b_Xtb) @ b_a1zG eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b_a1zG
                    (let {
                       lvl_s1OA :: a_Xt9 -> Text.ParserCombinators.ReadP.P b_a1zG
                       LclId
                       [Arity 1]
                       lvl_s1OA =
                         \ (a3_X1Oc :: a_Xt9) ->
                           eta_B1 (TestEq.L @ a_Xt9 @ b_Xtb a3_X1Oc) } in
                     \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                       case a3_a1A6 of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         Text.Read.Lex.Ident ds_d1ca [ALWAYS Just S] ->
                           case ds_d1ca of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                             : ds_d1cb [ALWAYS Just U(L)] ds_d1cc ->
                               case ds_d1cb of _ { GHC.Types.C# ds_d1cd [ALWAYS Just L] ->
                               case ds_d1cd of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                                 'L' ->
                                   case ds_d1cc of _ {
                                     [] ->
                                       case $dRead_aYo
                                       of _ { GHC.Read.D:Read _ _ tpl_XdG [ALWAYS Just C(C(S))] _ ->
                                       (((tpl_XdG
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    a_Xt9
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xt9
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP a_Xt9)))
                                           lvl_s1Ox)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xt9
                                                :: Text.ParserCombinators.ReadP.ReadP a_Xt9
                                                     ~
                                                   (forall b_a1zG.
                                                    (a_Xt9 -> Text.ParserCombinators.ReadP.P b_a1zG)
                                                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                         @ b_a1zG lvl_s1OA
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1zG
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xt9 TestEq.:+: b_Xtb)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xt9 TestEq.:+: b_Xtb)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1zG.
                          ((a_Xt9 TestEq.:+: b_Xtb) -> Text.ParserCombinators.ReadP.P b_a1zG)
                          -> Text.ParserCombinators.ReadP.P b_a1zG)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec (a_Xt9 TestEq.:+: b_Xtb)))
          ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b_a1zG)
              (eta_B1 [ALWAYS Just L] :: (a_Xt9 TestEq.:+: b_Xtb)
                                         -> Text.ParserCombinators.ReadP.P b_a1zG) ->
              case c_a1Cz of _ { GHC.Types.I# x_a1CC [ALWAYS Just L] ->
              case GHC.Prim.<=# x_a1CC 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xt9 TestEq.:+: b_Xtb) @ b_a1zG eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b_a1zG
                    (let {
                       lvl_s1OC :: b_Xtb -> Text.ParserCombinators.ReadP.P b_a1zG
                       LclId
                       [Arity 1]
                       lvl_s1OC =
                         \ (a3_X1Oc :: b_Xtb) ->
                           eta_B1 (TestEq.R @ a_Xt9 @ b_Xtb a3_X1Oc) } in
                     \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                       case a3_a1A6 of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         Text.Read.Lex.Ident ds_d1cl [ALWAYS Just S] ->
                           case ds_d1cl of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                             : ds_d1cm [ALWAYS Just U(L)] ds_d1cn ->
                               case ds_d1cm of _ { GHC.Types.C# ds_d1co [ALWAYS Just L] ->
                               case ds_d1co of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                                 'R' ->
                                   case ds_d1cn of _ {
                                     [] ->
                                       case $dRead_aYp
                                       of _ { GHC.Read.D:Read _ _ tpl_XdG [ALWAYS Just C(C(S))] _ ->
                                       (((tpl_XdG
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    b_Xtb
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec b_Xtb
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP b_Xtb)))
                                           lvl_s1Ox)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_Xtb
                                                :: Text.ParserCombinators.ReadP.ReadP b_Xtb
                                                     ~
                                                   (forall b_a1zG.
                                                    (b_Xtb -> Text.ParserCombinators.ReadP.P b_a1zG)
                                                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                         @ b_a1zG lvl_s1OC
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1zG
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xt9 TestEq.:+: b_Xtb)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xt9 TestEq.:+: b_Xtb)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1zG.
                          ((a_Xt9 TestEq.:+: b_Xtb) -> Text.ParserCombinators.ReadP.P b_a1zG)
                          -> Text.ParserCombinators.ReadP.P b_a1zG)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec
                        (a_Xt9 TestEq.:+: b_Xtb))))
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xt9 TestEq.:+: b_Xtb))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xt9 TestEq.:+: b_Xtb))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xt9 TestEq.:+: b_Xtb)))

a_s1Dj :: forall a_Xtf b_Xth.
          (GHC.Read.Read a_Xtf, GHC.Read.Read b_Xth) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [a_Xtf TestEq.:+: b_Xth]
LclId
[Arity 2
 Str: DmdType LL]
a_s1Dj =
  \ (@ a_Xtf)
    (@ b_Xth)
    ($dRead_X1bU :: GHC.Read.Read a_Xtf)
    ($dRead_X1bW :: GHC.Read.Read b_Xth) ->
    GHC.Read.$dmreadList2
      @ (a_Xtf TestEq.:+: b_Xth)
      ((a_s1Df @ a_Xtf @ b_Xth $dRead_X1bU $dRead_X1bW)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xtf TestEq.:+: b_Xth))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xtf TestEq.:+: b_Xth))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtf TestEq.:+: b_Xth)))

a_s1At :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.U
LclId
[Arity 1
 Str: DmdType L]
a_s1At =
  GHC.Read.$fRead()5
    @ TestEq.U
    (a_s1JA
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

a_s1AH :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.U]
LclId
[Str: DmdType]
a_s1AH =
  GHC.Read.$dmreadList2
    @ TestEq.U
    (a_s1At
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

a_s1AJ :: Text.ParserCombinators.ReadP.P [TestEq.U]
LclId
[Str: DmdType]
a_s1AJ =
  ((a_s1AH GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.U]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.U]
                ~
              (forall b_a1zG.
               ([TestEq.U] -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG)))
    @ [TestEq.U]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.U])

lvl_s1xG :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xG = GHC.Base.unpackCString# "Bin "

lvl_s1xD :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xD = GHC.Base.unpackCString# "Leaf"

lvl_s1xC :: GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
lvl_s1xC = GHC.Base.++ @ GHC.Types.Char lvl_s1xD

lvl_s1xy :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xy = GHC.Base.unpackCString# "Disj "

lvl_s1xt :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xt = GHC.Base.unpackCString# "Conj "

lvl_s1xo :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xo = GHC.Base.unpackCString# "Equiv "

lvl_s1xj :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xj = GHC.Base.unpackCString# "Impl "

lvl_s1xf :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xf = GHC.Base.unpackCString# "Not "

lvl_s1xc :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1xc = GHC.Types.C# 'F'

lvl_s1xb :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xb =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1xc (GHC.Types.[] @ GHC.Types.Char)

lvl_s1xa :: GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
lvl_s1xa = GHC.Base.++ @ GHC.Types.Char lvl_s1xb

lvl_s1x9 :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1x9 = GHC.Types.C# 'T'

lvl_s1x8 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1x8 =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1x9 (GHC.Types.[] @ GHC.Types.Char)

lvl_s1x7 :: GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
lvl_s1x7 = GHC.Base.++ @ GHC.Types.Char lvl_s1x8

lvl_s1x5 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1x5 = GHC.Base.unpackCString# "VarL "

lvl_s1wX :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wX = GHC.Base.unpackCString# "Infix "

lvl_s1wU :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wU = GHC.Base.unpackCString# "Prefix"

lvl_s1wR :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wR = GHC.Base.unpackCString# "NotAssociative"

lvl_s1wP :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wP = GHC.Base.unpackCString# "RightAssociative"

lvl_s1wN :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wN = GHC.Base.unpackCString# "LeftAssociative"

lvl_s1wG :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wG = GHC.Base.unpackCString# "Infix"

$wa_s1LM :: forall b_a1zG.
            (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
            -> Text.ParserCombinators.ReadP.P b_a1zG
LclId
[Arity 1
 Str: DmdType L]
$wa_s1LM =
  \ (@ b_a1zG)
    (w_s1Ls :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1zG) ->
    Text.Read.Lex.lex1
      @ b_a1zG
      (let {
         lvl_s1OD :: Text.ParserCombinators.ReadP.P b_a1zG
         LclId
         []
         lvl_s1OD = w_s1Ls TestEq.Prefix } in
       \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
         case a3_a1A6 of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
           Text.Read.Lex.Ident ds_d1bp [ALWAYS Just S] ->
             case GHC.Base.eqString ds_d1bp lvl_s1wU of _ {
               GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
               GHC.Bool.True -> lvl_s1OD
             }
         })

a_s1JC :: Text.ParserCombinators.ReadPrec.Prec
          -> forall b_a1zG.
             (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
             -> Text.ParserCombinators.ReadP.P b_a1zG
LclId
[Arity 2
 Worker $wa_s1LM
 Str: DmdType AL]
a_s1JC =
  __inline_me (\ _
                 (@ b_a1zG)
                 (w_s1Ls :: TestEq.Fixity
                            -> Text.ParserCombinators.ReadP.P b_a1zG) ->
                 $wa_s1LM @ b_a1zG w_s1Ls)

lvl_s1wh :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wh = GHC.Base.unpackCString# "Rec "

$wshowsPrec_s1LL :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Prim.Int# -> a_afF -> GHC.Base.String -> [GHC.Types.Char]
LclId
[Arity 3
 Str: DmdType LLL]
$wshowsPrec_s1LL =
  \ (@ a_afF)
    (w_s1Lf :: GHC.Show.Show a_afF)
    (ww_s1Li :: GHC.Prim.Int#)
    (ww_s1Lm :: a_afF) ->
    let {
      g_s1M1 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1M1 =
        case w_s1Lf
        of _ { GHC.Show.D:Show tpl_XfH [ALWAYS Just C(C(S))] _ _ ->
        tpl_XfH lvl_s1Ox ww_s1Lm
        } } in
    case GHC.Prim.>=# ww_s1Li 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char lvl_s1wh (g_s1M1 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               lvl_s1wh
               (g_s1M1
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

lvl_s1wb :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wb = GHC.Base.unpackCString# "Rec"

a_s1F0 :: forall a_XuW.
          (GHC.Read.Read a_XuW) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_XuW)
LclId
[Arity 2
 Str: DmdType LL]
a_s1F0 =
  \ (@ a_XuW)
    ($dRead_aTD [ALWAYS Just L] :: GHC.Read.Read a_XuW)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Rec a_XuW)
      ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta_X8f [ALWAYS Just L] :: TestEq.Rec a_XuW
                                      -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1CC 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Rec a_XuW) @ b_a1zG eta_X8f);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1zG
                (let {
                   lvl_s1OF :: a_XuW -> Text.ParserCombinators.ReadP.P b_a1zG
                   LclId
                   [Arity 1]
                   lvl_s1OF =
                     \ (a3_X1PS :: a_XuW) -> eta_X8f (TestEq.Rec @ a_XuW a3_X1PS) } in
                 \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                   case a3_a1A6 of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                     Text.Read.Lex.Ident ds_d1bB [ALWAYS Just S] ->
                       case GHC.Base.eqString ds_d1bB lvl_s1wb of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         GHC.Bool.True ->
                           case $dRead_aTD
                           of _ { GHC.Read.D:Read _ _ tpl_Xfx [ALWAYS Just C(C(S))] _ ->
                           (((tpl_Xfx
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XuW
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_XuW
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_XuW)))
                               lvl_s1Ox)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XuW
                                    :: Text.ParserCombinators.ReadP.ReadP a_XuW
                                         ~
                                       (forall b_a1zG.
                                        (a_XuW -> Text.ParserCombinators.ReadP.P b_a1zG)
                                        -> Text.ParserCombinators.ReadP.P b_a1zG)))
                             @ b_a1zG lvl_s1OF
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Rec a_XuW)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_XuW)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.Rec a_XuW -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_XuW)))
      eta_B1

a_s1F4 :: forall a_Xv0.
          (GHC.Read.Read a_Xv0) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xv0]
LclId
[Arity 1
 Str: DmdType L]
a_s1F4 =
  \ (@ a_Xv0) ($dRead_X18Z :: GHC.Read.Read a_Xv0) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Rec a_Xv0)
      ((a_s1F0 @ a_Xv0 $dRead_X18Z)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xv0))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xv0))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xv0)))

lvl_s1w4 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1w4 = GHC.Base.unpackCString# "Var "

$wshowsPrec_s1LK :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Prim.Int# -> a_afG -> GHC.Base.String -> [GHC.Types.Char]
LclId
[Arity 3
 Str: DmdType LLL]
$wshowsPrec_s1LK =
  \ (@ a_afG)
    (w_s1L2 :: GHC.Show.Show a_afG)
    (ww_s1L5 :: GHC.Prim.Int#)
    (ww_s1L9 :: a_afG) ->
    let {
      g_s1M5 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1M5 =
        case w_s1L2
        of _ { GHC.Show.D:Show tpl_Xfi [ALWAYS Just C(C(S))] _ _ ->
        tpl_Xfi lvl_s1Ox ww_s1L9
        } } in
    case GHC.Prim.>=# ww_s1L5 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char lvl_s1w4 (g_s1M5 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               lvl_s1w4
               (g_s1M5
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

lvl_s1vY :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vY = GHC.Base.unpackCString# "Var"

a_s1EE :: forall a_Xuy.
          (GHC.Read.Read a_Xuy) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_Xuy)
LclId
[Arity 2
 Str: DmdType LL]
a_s1EE =
  \ (@ a_Xuy)
    ($dRead_aUH [ALWAYS Just L] :: GHC.Read.Read a_Xuy)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Var a_Xuy)
      ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta_X84 [ALWAYS Just L] :: TestEq.Var a_Xuy
                                      -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1CC 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Var a_Xuy) @ b_a1zG eta_X84);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1zG
                (let {
                   lvl_s1OH :: a_Xuy -> Text.ParserCombinators.ReadP.P b_a1zG
                   LclId
                   [Arity 1]
                   lvl_s1OH =
                     \ (a3_X1Pt :: a_Xuy) -> eta_X84 (TestEq.Var @ a_Xuy a3_X1Pt) } in
                 \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                   case a3_a1A6 of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                     Text.Read.Lex.Ident ds_d1bI [ALWAYS Just S] ->
                       case GHC.Base.eqString ds_d1bI lvl_s1vY of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         GHC.Bool.True ->
                           case $dRead_aUH
                           of _ { GHC.Read.D:Read _ _ tpl_Xf8 [ALWAYS Just C(C(S))] _ ->
                           (((tpl_Xf8
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xuy
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xuy
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_Xuy)))
                               lvl_s1Ox)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xuy
                                    :: Text.ParserCombinators.ReadP.ReadP a_Xuy
                                         ~
                                       (forall b_a1zG.
                                        (a_Xuy -> Text.ParserCombinators.ReadP.P b_a1zG)
                                        -> Text.ParserCombinators.ReadP.P b_a1zG)))
                             @ b_a1zG lvl_s1OH
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Var a_Xuy)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_Xuy)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.Var a_Xuy -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_Xuy)))
      eta_B1

a_s1EI :: forall a_XuC.
          (GHC.Read.Read a_XuC) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuC]
LclId
[Arity 1
 Str: DmdType L]
a_s1EI =
  \ (@ a_XuC) ($dRead_X19E :: GHC.Read.Read a_XuC) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Var a_XuC)
      ((a_s1EE @ a_XuC $dRead_X19E)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuC))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuC))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuC)))

lvl_s1vR :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vR = GHC.Base.unpackCString# "C "

$wshowsPrec_s1LJ :: forall c_afH a_afI.
                    (GHC.Show.Show a_afI) =>
                    GHC.Prim.Int# -> a_afI -> GHC.Base.String -> [GHC.Types.Char]
LclId
[Arity 3
 Str: DmdType LLL]
$wshowsPrec_s1LJ =
  \ (@ c_afH)
    (@ a_afI)
    (w_s1KP :: GHC.Show.Show a_afI)
    (ww_s1KS :: GHC.Prim.Int#)
    (ww_s1KW :: a_afI) ->
    let {
      g_s1M9 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1M9 =
        case w_s1KP
        of _ { GHC.Show.D:Show tpl_XeU [ALWAYS Just C(C(S))] _ _ ->
        tpl_XeU lvl_s1Ox ww_s1KW
        } } in
    case GHC.Prim.>=# ww_s1KS 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char lvl_s1vR (g_s1M9 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               lvl_s1vR
               (g_s1M9
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

lvl_s1vE :: GHC.Types.Int
LclId
[Str: DmdType m]
lvl_s1vE = GHC.Types.I# 7

lvl_s1vD :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vD = GHC.Base.unpackCString# " :*: "

$wshowsPrec_s1LI :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Prim.Int#
                    -> a_afJ
                    -> b_afK
                    -> GHC.Base.String
                    -> GHC.Base.String
LclId
[Arity 5
 Str: DmdType LLLLL]
$wshowsPrec_s1LI =
  \ (@ a_afJ)
    (@ b_afK)
    (w_s1KA :: GHC.Show.Show a_afJ)
    (w_s1KB :: GHC.Show.Show b_afK)
    (ww_s1KE :: GHC.Prim.Int#)
    (ww_s1KI :: a_afJ)
    (ww_s1KJ :: b_afK) ->
    let {
      g_s1Md [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1Md =
        case w_s1KB
        of _ { GHC.Show.D:Show tpl_Xew [ALWAYS Just C(C(S))] _ _ ->
        tpl_Xew lvl_s1vE ww_s1KJ
        } } in
    let {
      f_s1Mb [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      f_s1Mb =
        case w_s1KA
        of _ { GHC.Show.D:Show tpl_Xes [ALWAYS Just C(C(S))] _ _ ->
        tpl_Xes lvl_s1vE ww_s1KI
        } } in
    case GHC.Prim.>=# ww_s1KE 7 of _ {
      GHC.Bool.False ->
        \ (x_X1vO :: GHC.Base.String) ->
          f_s1Mb (GHC.Base.++ @ GHC.Types.Char lvl_s1vD (g_s1Md x_X1vO));
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (f_s1Mb
               (GHC.Base.++
                  @ GHC.Types.Char
                  lvl_s1vD
                  (g_s1Md
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
    }

lvl_s1vw :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vw = GHC.Base.unpackCString# ":*:"

a_s1DO :: forall a_XtE b_XtG.
          (GHC.Read.Read a_XtE, GHC.Read.Read b_XtG) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (a_XtE TestEq.:*: b_XtG)
LclId
[Arity 3
 Str: DmdType LLL]
a_s1DO =
  \ (@ a_XtE)
    (@ b_XtG)
    ($dRead_aWX :: GHC.Read.Read a_XtE)
    ($dRead_aWY [ALWAYS Just L] :: GHC.Read.Read b_XtG)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (a_XtE TestEq.:*: b_XtG)
      (let {
         ds1_s1Mg [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadP
                                       a_XtE
         LclId
         [Str: DmdType]
         ds1_s1Mg =
           case $dRead_aWX
           of _ { GHC.Read.D:Read _ _ tpl_Xea [ALWAYS Just C(S)] _ ->
           (tpl_Xea
            `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XtE
                    :: Text.ParserCombinators.ReadPrec.ReadPrec a_XtE
                         ~
                       (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP a_XtE)))
             lvl_s1vE
           } } in
       (\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta_X5T :: (a_XtE TestEq.:*: b_XtG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1CC 6 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (a_XtE TestEq.:*: b_XtG) @ b_a1zG eta_X5T);
            GHC.Bool.True ->
              (ds1_s1Mg
               `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XtE
                       :: Text.ParserCombinators.ReadP.ReadP a_XtE
                            ~
                          (forall b_a1zG.
                           (a_XtE -> Text.ParserCombinators.ReadP.P b_a1zG)
                           -> Text.ParserCombinators.ReadP.P b_a1zG)))
                @ b_a1zG
                (\ (a3_a1A6 [ALWAYS Just L] :: a_XtE) ->
                   Text.Read.Lex.lex1
                     @ b_a1zG
                     (let {
                        lvl_s1OL :: b_XtG -> Text.ParserCombinators.ReadP.P b_a1zG
                        LclId
                        [Arity 1]
                        lvl_s1OL =
                          \ (a3_X1OG :: b_XtG) ->
                            eta_X5T (TestEq.:*: @ a_XtE @ b_XtG a3_a1A6 a3_X1OG) } in
                      \ (a3_X1Jo :: Text.Read.Lex.Lexeme) ->
                        case a3_X1Jo of _ {
                          __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                          Text.Read.Lex.Symbol ds_d1c2 [ALWAYS Just S] ->
                            case GHC.Base.eqString ds_d1c2 lvl_s1vw of _ {
                              GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                              GHC.Bool.True ->
                                case $dRead_aWY
                                of _ { GHC.Read.D:Read _ _ tpl_Xef [ALWAYS Just C(C(S))] _ ->
                                (((tpl_Xef
                                   `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec b_XtG
                                           :: Text.ParserCombinators.ReadPrec.ReadPrec b_XtG
                                                ~
                                              (Text.ParserCombinators.ReadPrec.Prec
                                               -> Text.ParserCombinators.ReadP.ReadP b_XtG)))
                                    lvl_s1vE)
                                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_XtG
                                         :: Text.ParserCombinators.ReadP.ReadP b_XtG
                                              ~
                                            (forall b_a1zG.
                                             (b_XtG -> Text.ParserCombinators.ReadP.P b_a1zG)
                                             -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                  @ b_a1zG lvl_s1OL
                                }
                            }
                        }))
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (a_XtE TestEq.:*: b_XtG)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (a_XtE TestEq.:*: b_XtG)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      ((a_XtE TestEq.:*: b_XtG) -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_XtE TestEq.:*: b_XtG)))
      eta_B1

a_s1DS :: forall a_XtK b_XtM.
          (GHC.Read.Read a_XtK, GHC.Read.Read b_XtM) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [a_XtK TestEq.:*: b_XtM]
LclId
[Arity 2
 Str: DmdType LL]
a_s1DS =
  \ (@ a_XtK)
    (@ b_XtM)
    ($dRead_X1b0 :: GHC.Read.Read a_XtK)
    ($dRead_X1b2 :: GHC.Read.Read b_XtM) ->
    GHC.Read.$dmreadList2
      @ (a_XtK TestEq.:*: b_XtM)
      ((a_s1DO @ a_XtK @ b_XtM $dRead_X1b0 $dRead_X1b2)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_XtK TestEq.:*: b_XtM))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_XtK TestEq.:*: b_XtM))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_XtK TestEq.:*: b_XtM)))

lvl_s1vp :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vp = GHC.Base.unpackCString# "R "

lvl_s1vl :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vl = GHC.Base.unpackCString# "L "

$wshowsPrec_s1LH :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Prim.Int#
                    -> (a_afL TestEq.:+: b_afM)
                    -> GHC.Base.String
                    -> [GHC.Types.Char]
LclId
[Arity 4
 Str: DmdType LLLS]
$wshowsPrec_s1LH =
  \ (@ a_afL)
    (@ b_afM)
    (w_s1Ko :: GHC.Show.Show a_afL)
    (w_s1Kp :: GHC.Show.Show b_afM)
    (ww_s1Ks :: GHC.Prim.Int#)
    (w_s1Ku :: a_afL TestEq.:+: b_afM) ->
    case w_s1Ku of _ {
      TestEq.L b1_ayF [ALWAYS Just L] ->
        let {
          g_s1Ml [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ml =
            case w_s1Ko
            of _ { GHC.Show.D:Show tpl_XdY [ALWAYS Just C(C(S))] _ _ ->
            tpl_XdY lvl_s1Ox b1_ayF
            } } in
        case GHC.Prim.>=# ww_s1Ks 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1vl (g_s1Ml x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1vl
                   (g_s1Ml
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.R b1_ayH [ALWAYS Just L] ->
        let {
          g_s1Mn [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Mn =
            case w_s1Kp
            of _ { GHC.Show.D:Show tpl_XdY [ALWAYS Just C(C(S))] _ _ ->
            tpl_XdY lvl_s1Ox b1_ayH
            } } in
        case GHC.Prim.>=# ww_s1Ks 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1vp (g_s1Mn x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1vp
                   (g_s1Mn
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        }
    }

lvl_s1uZ :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1uZ = GHC.Types.C# 'U'

lvl_s1uY :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uY =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1uZ (GHC.Types.[] @ GHC.Types.Char)

lvl_s1uq :: TestEq.C TestEq.List_Nil_ TestEq.U
LclId
[Str: DmdType m]
lvl_s1uq = TestEq.C @ TestEq.List_Nil_ @ TestEq.U TestEq.U

lvl_s1up :: forall a_agN.
            TestEq.C TestEq.List_Nil_ TestEq.U
            TestEq.:+: TestEq.C
                         TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
LclId
[Str: DmdType]
lvl_s1up =
  \ (@ a_agN) ->
    TestEq.L
      @ (TestEq.C TestEq.List_Nil_ TestEq.U)
      @ (TestEq.C
           TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
      lvl_s1uq

lvl_s1uo :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uo = GHC.Base.unpackCString# "[]"

lvl_s1un :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1un = GHC.Base.unpackCString# "Bin"

lvl_s1ul :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1ul = GHC.Base.unpackCString# "VarL"

lvl_s1uk :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uk = GHC.Base.unpackCString# "Impl"

lvl_s1uj :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uj = GHC.Base.unpackCString# "Equiv"

lvl_s1ui :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1ui = GHC.Base.unpackCString# "Conj"

lvl_s1uh :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uh = GHC.Base.unpackCString# "Disj"

lvl_s1ug :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1ug = GHC.Base.unpackCString# "Not"

lvl_s1ub :: GHC.Types.Int
LclId
[Str: DmdType m]
lvl_s1ub = GHC.Types.I# 5

lvl_s1ua :: TestEq.Fixity
LclId
[Str: DmdType]
lvl_s1ua = TestEq.Infix TestEq.RightAssociative lvl_s1ub

lvl_s1u9 :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1u9 = GHC.Types.C# ':'

lvl_s1u8 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1u8 =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1u9 (GHC.Types.[] @ GHC.Types.Char)

$s$dmconIsRecord_s1pA :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pA =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pz :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pz =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1py :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1py =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1px :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1px =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pw :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pw =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pv :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pv =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pu :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pu =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pt :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pt =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1ps :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1ps =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pr :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pr =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pq :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pq =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pp :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pp =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconFixity_s1po :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1po =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pn :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pn =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pm :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pm =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pl :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pl =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pk :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pk =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pj :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pj =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pi :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pi =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1ph :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1ph =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pg :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pg =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pf :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pf =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pe :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pe =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

a_s1l2 :: TestEq.Associativity
          -> TestEq.Associativity
          -> TestEq.Associativity
LclId
[Arity 2
 Str: DmdType SS]
a_s1l2 =
  \ (x_a1kz :: TestEq.Associativity)
    (y_a1kA :: TestEq.Associativity) ->
    case x_a1kz of _ {
      TestEq.LeftAssociative ->
        case y_a1kA of _ { __DEFAULT -> TestEq.LeftAssociative };
      TestEq.RightAssociative ->
        case y_a1kA of _ {
          TestEq.LeftAssociative -> TestEq.LeftAssociative;
          TestEq.RightAssociative -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.RightAssociative
        };
      TestEq.NotAssociative -> y_a1kA
    }

a_s1l0 :: TestEq.Associativity
          -> TestEq.Associativity
          -> TestEq.Associativity
LclId
[Arity 2
 Str: DmdType SS]
a_s1l0 =
  \ (x_a1kf :: TestEq.Associativity)
    (y_a1kg :: TestEq.Associativity) ->
    case x_a1kf of _ {
      TestEq.LeftAssociative -> y_a1kg;
      TestEq.RightAssociative ->
        case y_a1kg of _ {
          __DEFAULT -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.NotAssociative
        };
      TestEq.NotAssociative ->
        case y_a1kg of _ { __DEFAULT -> TestEq.NotAssociative }
    }

a_s1kY :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kY =
  \ (x_a1jY :: TestEq.Associativity)
    (y_a1jZ :: TestEq.Associativity) ->
    case x_a1jY of _ {
      TestEq.LeftAssociative ->
        case y_a1jZ of _ { __DEFAULT -> GHC.Bool.True };
      TestEq.RightAssociative ->
        case y_a1jZ of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1jZ of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

a_s1kW :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kW =
  \ (x_a1jH :: TestEq.Associativity)
    (y_a1jI :: TestEq.Associativity) ->
    case x_a1jH of _ {
      TestEq.LeftAssociative ->
        case y_a1jI of _ { __DEFAULT -> GHC.Bool.False };
      TestEq.RightAssociative ->
        case y_a1jI of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jI of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

a_s1kU :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kU =
  \ (x_a1jq :: TestEq.Associativity)
    (y_a1jr :: TestEq.Associativity) ->
    case x_a1jq of _ {
      TestEq.LeftAssociative ->
        case y_a1jr of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case y_a1jr of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jr of _ { __DEFAULT -> GHC.Bool.True }
    }

a_s1kS :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kS =
  \ (x_a1j9 :: TestEq.Associativity)
    (y_a1ja :: TestEq.Associativity) ->
    case x_a1j9 of _ {
      TestEq.LeftAssociative ->
        case y_a1ja of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case y_a1ja of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1ja of _ { __DEFAULT -> GHC.Bool.False }
    }

a_s1iV :: TestEq.Associativity -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType S]
a_s1iV =
  \ (x_X1xm :: TestEq.Associativity) ->
    case x_X1xm of _ {
      TestEq.LeftAssociative -> lvl_s1wN;
      TestEq.RightAssociative -> lvl_s1wP;
      TestEq.NotAssociative -> lvl_s1wR
    }

lvl_s1OP :: Text.ParserCombinators.ReadPrec.Prec
            -> forall b_X1Pt.
               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1Pt)
               -> Text.ParserCombinators.ReadP.P b_X1Pt
LclId
[Arity 2]
lvl_s1OP =
  \ _
    (@ b_X1Pt)
    (eta_X1Pv :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1Pt) ->
    eta_X1Pv TestEq.LeftAssociative

lvl_s1ON :: ([GHC.Types.Char],
             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
LclId
[]
lvl_s1ON =
  (lvl_s1wN,
   lvl_s1OP
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1zG.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                  -> Text.ParserCombinators.ReadP.P b_a1zG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1OT :: Text.ParserCombinators.ReadPrec.Prec
            -> forall b_X1PA.
               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1PA)
               -> Text.ParserCombinators.ReadP.P b_X1PA
LclId
[Arity 2]
lvl_s1OT =
  \ _
    (@ b_X1PA)
    (eta_X1PC :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1PA) ->
    eta_X1PC TestEq.RightAssociative

lvl_s1OR :: ([GHC.Types.Char],
             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
LclId
[]
lvl_s1OR =
  (lvl_s1wP,
   lvl_s1OT
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1zG.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                  -> Text.ParserCombinators.ReadP.P b_a1zG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1OX :: Text.ParserCombinators.ReadPrec.Prec
            -> forall b_X1PH.
               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1PH)
               -> Text.ParserCombinators.ReadP.P b_X1PH
LclId
[Arity 2]
lvl_s1OX =
  \ _
    (@ b_X1PH)
    (eta_X1PJ :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1PH) ->
    eta_X1PJ TestEq.NotAssociative

lvl_s1OV :: ([GHC.Types.Char],
             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
LclId
[]
lvl_s1OV =
  (lvl_s1wR,
   lvl_s1OX
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1zG.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                  -> Text.ParserCombinators.ReadP.P b_a1zG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1OU :: [(GHC.Base.String,
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
LclId
[]
lvl_s1OU =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    lvl_s1OV
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1OQ :: [(GHC.Base.String,
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
LclId
[]
lvl_s1OQ =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    lvl_s1OR
    lvl_s1OU

lvl_s1OM :: [(GHC.Base.String,
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
LclId
[]
lvl_s1OM =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    lvl_s1ON
    lvl_s1OQ

a_s1iD :: Text.ParserCombinators.ReadPrec.ReadPrec
            TestEq.Associativity
LclId
[Str: DmdType]
a_s1iD = GHC.Read.choose1 @ TestEq.Associativity lvl_s1OM

a_s1iF :: GHC.Types.Int
          -> Text.ParserCombinators.ReadP.ReadS TestEq.Associativity
LclId
[Arity 1
 Str: DmdType L]
a_s1iF =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Associativity
      (((GHC.Read.$fRead()5 @ TestEq.Associativity a_s1iD eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  TestEq.Associativity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                     ~
                   (forall b_a1zG.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ TestEq.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ TestEq.Associativity))

a_s1FP :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
LclId
[Arity 1
 Str: DmdType L]
a_s1FP = GHC.Read.$fRead()5 @ TestEq.Associativity a_s1iD

a_s1FR :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
LclId
[Str: DmdType]
a_s1FR =
  GHC.Read.$dmreadList2
    @ TestEq.Associativity
    (a_s1FP
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

a_s1FT :: Text.ParserCombinators.ReadP.P [TestEq.Associativity]
LclId
[Str: DmdType]
a_s1FT =
  ((a_s1FR GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [TestEq.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
                ~
              (forall b_a1zG.
               ([TestEq.Associativity] -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG)))
    @ [TestEq.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_return
       @ [TestEq.Associativity])

Rec {
a7_s1MB :: Text.ParserCombinators.ReadPrec.Prec
           -> forall b_a1Ai.
              (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1Ai)
              -> Text.ParserCombinators.ReadP.P b_a1Ai
LclId
[Arity 2
 Str: DmdType AL]
a7_s1MB =
  __inline_me (GHC.Read.$fRead()4
                 @ TestEq.Associativity
                 (a8_s1MC
                  `cast` (trans
                            (trans
                               (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                            (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                          :: (Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                               ~
                             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)))
a8_s1MC [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
LclId
[Arity 1
 Str: DmdType L]
a8_s1MC =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Associativity
    a_s1iD
    (a7_s1MB
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym
                     (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Associativity))
               (sym
                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                     TestEq.Associativity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
end Rec }

$wa_s1LN :: GHC.Prim.Int#
            -> forall b_a1zG.
               (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG
LclId
[Arity 2
 Str: DmdType LL]
$wa_s1LN =
  \ (ww_s1Ly :: GHC.Prim.Int#)
    (@ b_a1zG)
    (w_s1LA :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1zG) ->
    case GHC.Prim.<=# ww_s1Ly 10 of _ {
      GHC.Bool.False ->
        __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                       @ TestEq.Fixity @ b_a1zG w_s1LA);
      GHC.Bool.True ->
        Text.Read.Lex.lex1
          @ b_a1zG
          (let {
             lvl_s1OY :: Text.ParserCombinators.ReadP.P b_a1zG
             LclId
             []
             lvl_s1OY =
               let {
                 k_s1MF :: TestEq.Associativity
                           -> Text.ParserCombinators.ReadP.P b_a1zG
                 LclId
                 [Arity 1
                  Str: DmdType L]
                 k_s1MF =
                   \ (a3_X1R4 [ALWAYS Just L] :: TestEq.Associativity) ->
                     ((GHC.Read.$dmreadsPrec14 lvl_s1Ox)
                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                              :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                   ~
                                 (forall b_a1zG.
                                  (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_a1zG)
                                  -> Text.ParserCombinators.ReadP.P b_a1zG)))
                       @ b_a1zG
                       (\ (a3_X1KF :: GHC.Types.Int) ->
                          w_s1LA (TestEq.Infix a3_X1R4 a3_X1KF)) } in
               Text.ParserCombinators.ReadP.$fMonadPlusP_mplus
                 @ b_a1zG
                 ((((a_s1iD
                     `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                               TestEq.Associativity
                             :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity
                                  ~
                                (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)))
                      lvl_s1Ox)
                   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                             TestEq.Associativity
                           :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                                ~
                              (forall b_a1zG.
                               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                               -> Text.ParserCombinators.ReadP.P b_a1zG)))
                    @ b_a1zG k_s1MF)
                 (GHC.Read.$wa
                    @ TestEq.Associativity
                    (a8_s1MC
                     `cast` (trans
                               (trans
                                  (sym
                                     (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                        TestEq.Associativity))
                                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                             :: (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                                  ~
                                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                    @ b_a1zG
                    k_s1MF) } in
           \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
             case a3_a1A6 of _ {
               __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
               Text.Read.Lex.Ident ds_d1bu [ALWAYS Just S] ->
                 case GHC.Base.eqString ds_d1bu lvl_s1wG of _ {
                   GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                   GHC.Bool.True -> lvl_s1OY
                 }
             })
    }

a_s1JE :: Text.ParserCombinators.ReadPrec.Prec
          -> forall b_a1zG.
             (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
             -> Text.ParserCombinators.ReadP.P b_a1zG
LclId
[Arity 2
 Worker $wa_s1LN
 Str: DmdType U(L)L]
a_s1JE =
  __inline_me (\ (w_s1Lw :: Text.ParserCombinators.ReadPrec.Prec)
                 (@ b_a1zG)
                 (w_s1LA :: TestEq.Fixity
                            -> Text.ParserCombinators.ReadP.P b_a1zG) ->
                 case w_s1Lw of _ { GHC.Types.I# ww_s1Ly ->
                 $wa_s1LN ww_s1Ly @ b_a1zG w_s1LA
                 })

a_s1Gl :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
LclId
[Arity 1
 Str: DmdType L]
a_s1Gl =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Fixity
    (a_s1JC
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (a_s1JE
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

a_s1iR :: GHC.Types.Int
          -> Text.ParserCombinators.ReadP.ReadS TestEq.Fixity
LclId
[Arity 1
 Str: DmdType L]
a_s1iR =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Fixity
      (((GHC.Read.$fRead()5
           @ TestEq.Fixity
           (a_s1Gl
            `cast` (trans
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                      (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
                     ~
                   (forall b_a1zG.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ TestEq.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.Fixity))

a_s1Gn :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
LclId
[Arity 1
 Str: DmdType L]
a_s1Gn =
  GHC.Read.$fRead()5
    @ TestEq.Fixity
    (a_s1Gl
     `cast` (trans
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

a_s1Gp :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
LclId
[Str: DmdType]
a_s1Gp =
  GHC.Read.$dmreadList2
    @ TestEq.Fixity
    (a_s1Gn
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

a_s1Gr :: Text.ParserCombinators.ReadP.P [TestEq.Fixity]
LclId
[Str: DmdType]
a_s1Gr =
  ((a_s1Gp GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
                ~
              (forall b_a1zG.
               ([TestEq.Fixity] -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG)))
    @ [TestEq.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.Fixity])

a_s1gh :: TestEq.U -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType U()]
a_s1gh =
  __inline_me (\ (x_a1gf :: TestEq.U) ->
                 case x_a1gf of _ { TestEq.U -> lvl_s1uY })

a_s1fY :: GHC.Types.Int
          -> Text.ParserCombinators.ReadP.ReadS TestEq.U
LclId
[Arity 1
 Str: DmdType L]
a_s1fY =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.U
      (((GHC.Read.$fRead()5
           @ TestEq.U
           (a_s1JA
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
                      (trans
                         (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                         (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1zG.
                           (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                           -> Text.ParserCombinators.ReadP.P b_a1zG)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U
                :: Text.ParserCombinators.ReadP.ReadP TestEq.U
                     ~
                   (forall b_a1zG.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ TestEq.U
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.U))

a_s1ed :: forall a_XnQ.
          (TestEq.Representable a_XnQ) =>
          TestEq.Rep (TestEq.Rec a_XnQ) -> TestEq.Rep (TestEq.Rec a_XnQ)
LclId
[Arity 2
 Str: DmdType AS]
a_s1ed =
  __inline_me (\ (@ a_XnQ)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Rec a_XnQ)) ->
                 eta_B1)

a_s1eb :: forall a_ah2.
          (TestEq.Representable a_ah2) =>
          TestEq.Rec a_ah2 -> TestEq.Rec a_ah2
LclId
[Arity 2
 Str: DmdType AS]
a_s1eb =
  __inline_me (\ (@ a_ah2) _ (eta_B1 :: TestEq.Rec a_ah2) -> eta_B1)

a_s1e9 :: forall a_XnL.
          (TestEq.Representable a_XnL) =>
          TestEq.Rep (TestEq.Var a_XnL) -> TestEq.Rep (TestEq.Var a_XnL)
LclId
[Arity 2
 Str: DmdType AS]
a_s1e9 =
  __inline_me (\ (@ a_XnL)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Var a_XnL)) ->
                 eta_B1)

a_s1e7 :: forall a_ah4.
          (TestEq.Representable a_ah4) =>
          TestEq.Var a_ah4 -> TestEq.Var a_ah4
LclId
[Arity 2
 Str: DmdType AS]
a_s1e7 =
  __inline_me (\ (@ a_ah4) _ (eta_B1 :: TestEq.Var a_ah4) -> eta_B1)

a_s1e5 :: forall a_XnF c_XnH.
          (TestEq.Representable a_XnF) =>
          TestEq.Rep (TestEq.C c_XnH a_XnF)
          -> TestEq.Rep (TestEq.C c_XnH a_XnF)
LclId
[Arity 2
 Str: DmdType AS]
a_s1e5 =
  __inline_me (\ (@ a_XnF)
                 (@ c_XnH)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.C c_XnH a_XnF)) ->
                 eta_B1)

a_s1e3 :: forall a_ah6 c_ah7.
          (TestEq.Representable a_ah6) =>
          TestEq.C c_ah7 a_ah6 -> TestEq.C c_ah7 a_ah6
LclId
[Arity 2
 Str: DmdType AS]
a_s1e3 =
  __inline_me (\ (@ a_ah6)
                 (@ c_ah7)
                 _
                 (eta_B1 :: TestEq.C c_ah7 a_ah6) ->
                 eta_B1)

a_s1e1 :: forall a_Xnz b_XnB.
          (TestEq.Representable a_Xnz, TestEq.Representable b_XnB) =>
          TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
          -> TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
LclId
[Arity 3
 Str: DmdType AAS]
a_s1e1 =
  __inline_me (\ (@ a_Xnz)
                 (@ b_XnB)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) ->
                 eta_B1)

a_s1dZ :: forall a_aha b_ahb.
          (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
          (a_aha TestEq.:+: b_ahb) -> a_aha TestEq.:+: b_ahb
LclId
[Arity 3
 Str: DmdType AAS]
a_s1dZ =
  __inline_me (\ (@ a_aha)
                 (@ b_ahb)
                 _
                 _
                 (eta_B1 :: a_aha TestEq.:+: b_ahb) ->
                 eta_B1)

a_s1dX :: forall a_Xns b_Xnu.
          (TestEq.Representable a_Xns, TestEq.Representable b_Xnu) =>
          TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
          -> TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
LclId
[Arity 3
 Str: DmdType AAS]
a_s1dX =
  __inline_me (\ (@ a_Xns)
                 (@ b_Xnu)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) ->
                 eta_B1)

a_s1dV :: forall a_ahe b_ahf.
          (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
          (a_ahe TestEq.:*: b_ahf) -> a_ahe TestEq.:*: b_ahf
LclId
[Arity 3
 Str: DmdType AAS]
a_s1dV =
  __inline_me (\ (@ a_ahe)
                 (@ b_ahf)
                 _
                 _
                 (eta_B1 :: a_ahe TestEq.:*: b_ahf) ->
                 eta_B1)

a_s1dR :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dR = TestEq.Not TestEq.F

a_s1dO :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dO = TestEq.Not TestEq.T

a_s1dK :: GHC.Types.Char
LclId
[Str: DmdType m]
a_s1dK = GHC.Types.C# 'x'

a_s1dL :: [GHC.Types.Char]
LclId
[Str: DmdType]
a_s1dL =
  GHC.Types.: @ GHC.Types.Char a_s1dK (GHC.Types.[] @ GHC.Types.Char)

a_s1dM :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dM = TestEq.VarL a_s1dL

a_s1dP :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dP = TestEq.Equiv a_s1dM a_s1dO

a_s1dD :: GHC.Types.Int
LclId
[Str: DmdType m]
a_s1dD = GHC.Types.I# 3

TestEq.$dmconIsRecord :: forall c_afy.
                         (TestEq.Constructor c_afy) =>
                         forall (t_afD :: * -> * -> *) a_afE.
                         t_afD c_afy a_afE -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType AA
 RULES: "SPEC TestEq.$dmconIsRecord [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1oF [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconIsRecord @ TestEq.Logic_F_ $dConstructor_s1oF
              = $s$dmconIsRecord_s1pp
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1oI [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconIsRecord @ TestEq.Logic_T_ $dConstructor_s1oI
              = $s$dmconIsRecord_s1pq
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oL [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Not_ $dConstructor_s1oL
              = $s$dmconIsRecord_s1pr
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oO [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Disj_ $dConstructor_s1oO
              = $s$dmconIsRecord_s1ps
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1oR [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Conj_ $dConstructor_s1oR
              = $s$dmconIsRecord_s1pt
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1oU [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Equiv_ $dConstructor_s1oU
              = $s$dmconIsRecord_s1pu
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oX [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Impl_ $dConstructor_s1oX
              = $s$dmconIsRecord_s1pv
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1p0 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Var_ $dConstructor_s1p0
              = $s$dmconIsRecord_s1pw
        "SPEC TestEq.$dmconIsRecord [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1p3 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconIsRecord @ TestEq.Leaf $dConstructor_s1p3
              = $s$dmconIsRecord_s1px
        "SPEC TestEq.$dmconIsRecord [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1p6 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconIsRecord @ TestEq.Bin $dConstructor_s1p6
              = $s$dmconIsRecord_s1py
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Cons_]" ALWAYS
            forall {$dConstructor_s1p9 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Cons_}
              TestEq.$dmconIsRecord @ TestEq.List_Cons_ $dConstructor_s1p9
              = $s$dmconIsRecord_s1pz
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1pc [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconIsRecord @ TestEq.List_Nil_ $dConstructor_s1pc
              = $s$dmconIsRecord_s1pA]
TestEq.$dmconIsRecord =
  __inline_me (\ (@ c_afy) _ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconFixity :: forall c_afy.
                       (TestEq.Constructor c_afy) =>
                       forall (t_afB :: * -> * -> *) a_afC.
                       t_afB c_afy a_afC -> TestEq.Fixity
LclIdX
[Arity 2
 Str: DmdType AA
 RULES: "SPEC TestEq.$dmconFixity [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1o8 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconFixity @ TestEq.Logic_F_ $dConstructor_s1o8
              = $s$dmconFixity_s1po
        "SPEC TestEq.$dmconFixity [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1ob [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconFixity @ TestEq.Logic_T_ $dConstructor_s1ob
              = $s$dmconFixity_s1pn
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oe [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconFixity @ TestEq.Logic_Not_ $dConstructor_s1oe
              = $s$dmconFixity_s1pm
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oh [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconFixity @ TestEq.Logic_Disj_ $dConstructor_s1oh
              = $s$dmconFixity_s1pl
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1ok [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconFixity @ TestEq.Logic_Conj_ $dConstructor_s1ok
              = $s$dmconFixity_s1pk
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1on [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconFixity @ TestEq.Logic_Equiv_ $dConstructor_s1on
              = $s$dmconFixity_s1pj
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oq [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconFixity @ TestEq.Logic_Impl_ $dConstructor_s1oq
              = $s$dmconFixity_s1pi
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1ot [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconFixity @ TestEq.Logic_Var_ $dConstructor_s1ot
              = $s$dmconFixity_s1ph
        "SPEC TestEq.$dmconFixity [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1ow [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconFixity @ TestEq.Leaf $dConstructor_s1ow
              = $s$dmconFixity_s1pg
        "SPEC TestEq.$dmconFixity [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1oz [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconFixity @ TestEq.Bin $dConstructor_s1oz
              = $s$dmconFixity_s1pf
        "SPEC TestEq.$dmconFixity [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1oC [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconFixity @ TestEq.List_Nil_ $dConstructor_s1oC
              = $s$dmconFixity_s1pe]
TestEq.$dmconFixity =
  __inline_me (\ (@ c_afy) _ (@ t_aFL::* -> * -> *) (@ a_aFM) _ ->
                 TestEq.Prefix)

TestEq.$fRepresentableInt :: TestEq.Representable GHC.Types.Int
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableInt =
  TestEq.D:Representable
    @ GHC.Types.Int
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Int))
     `cast` (TestEq.Rep GHC.Types.Int -> TestEq.TFCo:R:RepInt
             :: (TestEq.Rep GHC.Types.Int -> TestEq.Rep GHC.Types.Int)
                  ~
                (TestEq.Rep GHC.Types.Int -> TestEq.R:RepInt)))
    ((GHC.Base.id @ GHC.Types.Int)
     `cast` (GHC.Types.Int -> sym TestEq.TFCo:R:RepInt
             :: (GHC.Types.Int -> TestEq.R:RepInt)
                  ~
                (GHC.Types.Int -> TestEq.Rep GHC.Types.Int)))

TestEq.$fRepresentableChar :: TestEq.Representable GHC.Types.Char
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableChar =
  TestEq.D:Representable
    @ GHC.Types.Char
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Char))
     `cast` (TestEq.Rep GHC.Types.Char -> TestEq.TFCo:R:RepChar
             :: (TestEq.Rep GHC.Types.Char -> TestEq.Rep GHC.Types.Char)
                  ~
                (TestEq.Rep GHC.Types.Char -> TestEq.R:RepChar)))
    ((GHC.Base.id @ GHC.Types.Char)
     `cast` (GHC.Types.Char -> sym TestEq.TFCo:R:RepChar
             :: (GHC.Types.Char -> TestEq.R:RepChar)
                  ~
                (GHC.Types.Char -> TestEq.Rep GHC.Types.Char)))

TestEq.$fRepresentableU :: TestEq.Representable TestEq.U
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableU =
  TestEq.D:Representable
    @ TestEq.U
    ((GHC.Base.id @ (TestEq.Rep TestEq.U))
     `cast` (TestEq.Rep TestEq.U -> TestEq.TFCo:R:RepU
             :: (TestEq.Rep TestEq.U -> TestEq.Rep TestEq.U)
                  ~
                (TestEq.Rep TestEq.U -> TestEq.R:RepU)))
    ((GHC.Base.id @ TestEq.U)
     `cast` (TestEq.U -> sym TestEq.TFCo:R:RepU
             :: (TestEq.U -> TestEq.R:RepU)
                  ~
                (TestEq.U -> TestEq.Rep TestEq.U)))

TestEq.$fRepresentable:*: :: forall a_ahe b_ahf.
                             (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                             TestEq.Representable (a_ahe TestEq.:*: b_ahf)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRepresentable:*: =
  __inline_me (\ (@ a_Xnx)
                 (@ b_Xnz)
                 ($dRepresentable_X1bW [ALWAYS Just L] :: TestEq.Representable
                                                            a_Xnx)
                 ($dRepresentable_X1bY [ALWAYS Just L] :: TestEq.Representable
                                                            b_Xnz) ->
                 TestEq.D:Representable
                   @ (a_Xnx TestEq.:*: b_Xnz)
                   ((a_s1dX @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                            -> TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz
                            :: (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz))
                                 ~
                               (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.R:Rep:*: a_Xnx b_Xnz)))
                   ((a_s1dV @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` ((a_Xnx TestEq.:*: b_Xnz)
                            -> sym (TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz)
                            :: ((a_Xnx TestEq.:*: b_Xnz) -> TestEq.R:Rep:*: a_Xnx b_Xnz)
                                 ~
                               ((a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)))))

TestEq.$fRepresentable:+: :: forall a_aha b_ahb.
                             (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                             TestEq.Representable (a_aha TestEq.:+: b_ahb)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRepresentable:+: =
  __inline_me (\ (@ a_XnE)
                 (@ b_XnG)
                 ($dRepresentable_X1bU [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnE)
                 ($dRepresentable_X1bW [ALWAYS Just L] :: TestEq.Representable
                                                            b_XnG) ->
                 TestEq.D:Representable
                   @ (a_XnE TestEq.:+: b_XnG)
                   ((a_s1e1 @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                            -> TestEq.TFCo:R:Rep:+: a_XnE b_XnG
                            :: (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG))
                                 ~
                               (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.R:Rep:+: a_XnE b_XnG)))
                   ((a_s1dZ @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` ((a_XnE TestEq.:+: b_XnG)
                            -> sym (TestEq.TFCo:R:Rep:+: a_XnE b_XnG)
                            :: ((a_XnE TestEq.:+: b_XnG) -> TestEq.R:Rep:+: a_XnE b_XnG)
                                 ~
                               ((a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG)))))

TestEq.$fRepresentableC :: forall a_ah6 c_ah7.
                           (TestEq.Representable a_ah6) =>
                           TestEq.Representable (TestEq.C c_ah7 a_ah6)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fRepresentableC =
  __inline_me (\ (@ a_XnJ)
                 (@ c_XnL)
                 ($dRepresentable_X1iw [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnJ) ->
                 TestEq.D:Representable
                   @ (TestEq.C c_XnL a_XnJ)
                   ((a_s1e5 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                            -> TestEq.TFCo:R:RepC c_XnL a_XnJ
                            :: (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                                -> TestEq.Rep (TestEq.C c_XnL a_XnJ))
                                 ~
                               (TestEq.Rep (TestEq.C c_XnL a_XnJ) -> TestEq.R:RepC c_XnL a_XnJ)))
                   ((a_s1e3 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.C c_XnL a_XnJ
                            -> sym (TestEq.TFCo:R:RepC c_XnL a_XnJ)
                            :: (TestEq.C c_XnL a_XnJ -> TestEq.R:RepC c_XnL a_XnJ)
                                 ~
                               (TestEq.C c_XnL a_XnJ -> TestEq.Rep (TestEq.C c_XnL a_XnJ)))))

TestEq.$fRepresentableVar :: forall a_ah4.
                             (TestEq.Representable a_ah4) =>
                             TestEq.Representable (TestEq.Var a_ah4)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fRepresentableVar =
  __inline_me (\ (@ a_XnO)
                 ($dRepresentable_X1bL [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnO) ->
                 TestEq.D:Representable
                   @ (TestEq.Var a_XnO)
                   ((a_s1e9 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.TFCo:R:RepVar a_XnO
                            :: (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.Rep (TestEq.Var a_XnO))
                                 ~
                               (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.R:RepVar a_XnO)))
                   ((a_s1e7 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Var a_XnO -> sym (TestEq.TFCo:R:RepVar a_XnO)
                            :: (TestEq.Var a_XnO -> TestEq.R:RepVar a_XnO)
                                 ~
                               (TestEq.Var a_XnO -> TestEq.Rep (TestEq.Var a_XnO)))))

TestEq.$fRepresentableRec :: forall a_ah2.
                             (TestEq.Representable a_ah2) =>
                             TestEq.Representable (TestEq.Rec a_ah2)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fRepresentableRec =
  __inline_me (\ (@ a_XnT)
                 ($dRepresentable_X1bF [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnT) ->
                 TestEq.D:Representable
                   @ (TestEq.Rec a_XnT)
                   ((a_s1ed @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.TFCo:R:RepRec a_XnT
                            :: (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.Rep (TestEq.Rec a_XnT))
                                 ~
                               (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.R:RepRec a_XnT)))
                   ((a_s1eb @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rec a_XnT -> sym (TestEq.TFCo:R:RepRec a_XnT)
                            :: (TestEq.Rec a_XnT -> TestEq.R:RepRec a_XnT)
                                 ~
                               (TestEq.Rec a_XnT -> TestEq.Rep (TestEq.Rec a_XnT)))))

TestEq.$fEqInt :: TestEq.Eq GHC.Types.Int
LclIdX[DFunId]
[Arity 2
 Str: DmdType U(L)U(L)]
TestEq.$fEqInt =
  __inline_me (GHC.Base.eqInt
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Int)
                       :: (GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Int))

TestEq.$fEqChar :: TestEq.Eq GHC.Types.Char
LclIdX[DFunId]
[Arity 2
 Str: DmdType U(L)U(L)]
TestEq.$fEqChar =
  __inline_me (GHC.Base.$fEqChar_==
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
                       :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Char))

TestEq.eq :: forall a_afN.
             (TestEq.Representable a_afN, TestEq.Eq (TestEq.Rep a_afN)) =>
             a_afN -> a_afN -> GHC.Bool.Bool
LclIdX
[Arity 4
 Str: DmdType LSLL]
TestEq.eq =
  __inline_me (\ (@ a_azl)
                 ($dRepresentable_azt :: TestEq.Representable a_azl)
                 ($dEq_azu :: TestEq.Eq (TestEq.Rep a_azl))
                 (eta_B2 :: a_azl)
                 (eta_B1 :: a_azl) ->
                 let {
                   from_s1o6 [ALWAYS Just L] :: a_azl -> TestEq.Rep a_azl
                   LclId
                   [Str: DmdType {azt->U(AS)}]
                   from_s1o6 = TestEq.from @ a_azl $dRepresentable_azt } in
                 TestEq.eq'
                   @ (TestEq.Rep a_azl)
                   $dEq_azu
                   (from_s1o6 eta_B2)
                   (from_s1o6 eta_B1))

TestEq.eqRec :: forall t_azF.
                (TestEq.Eq t_azF) =>
                TestEq.Rec t_azF -> TestEq.Rec t_azF -> GHC.Bool.Bool
LclIdX
[Arity 3
 Str: DmdType SU(L)U(L)]
TestEq.eqRec =
  __inline_me (\ (@ t_azF)
                 ($dEq_azL :: TestEq.Eq t_azF)
                 (ds_d17K :: TestEq.Rec t_azF)
                 (ds_d17L :: TestEq.Rec t_azF) ->
                 case ds_d17K of _ { TestEq.Rec x_agu [ALWAYS Just L] ->
                 case ds_d17L of _ { TestEq.Rec x'_agv [ALWAYS Just L] ->
                 TestEq.eq' @ t_azF $dEq_azL x_agu x'_agv
                 }
                 })

TestEq.$fEqRec :: forall a_agD.
                  (TestEq.Eq a_agD) =>
                  TestEq.Eq (TestEq.Rec a_agD)
LclIdX[DFunId]
[Arity 3
 Str: DmdType SU(L)U(L)]
TestEq.$fEqRec =
  __inline_me (TestEq.eqRec
               `cast` (forall a_agD.
                       (TestEq.Eq a_agD) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Rec a_agD))
                       :: (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.Rec a_agD -> TestEq.Rec a_agD -> GHC.Bool.Bool)
                            ~
                          (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.T:Eq (TestEq.Rec a_agD))))

TestEq.eqVar :: forall t_azR.
                (TestEq.Eq t_azR) =>
                TestEq.Var t_azR -> TestEq.Var t_azR -> GHC.Bool.Bool
LclIdX
[Arity 3
 Str: DmdType SU(L)U(L)]
TestEq.eqVar =
  __inline_me (\ (@ t_azR)
                 ($dEq_azX :: TestEq.Eq t_azR)
                 (ds_d17O :: TestEq.Var t_azR)
                 (ds_d17P :: TestEq.Var t_azR) ->
                 case ds_d17O of _ { TestEq.Var x_ags [ALWAYS Just L] ->
                 case ds_d17P of _ { TestEq.Var x'_agt [ALWAYS Just L] ->
                 TestEq.eq' @ t_azR $dEq_azX x_ags x'_agt
                 }
                 })

TestEq.$fEqVar :: forall a_agE.
                  (TestEq.Eq a_agE) =>
                  TestEq.Eq (TestEq.Var a_agE)
LclIdX[DFunId]
[Arity 3
 Str: DmdType SU(L)U(L)]
TestEq.$fEqVar =
  __inline_me (TestEq.eqVar
               `cast` (forall a_agE.
                       (TestEq.Eq a_agE) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Var a_agE))
                       :: (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.Var a_agE -> TestEq.Var a_agE -> GHC.Bool.Bool)
                            ~
                          (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.T:Eq (TestEq.Var a_agE))))

TestEq.eqC :: forall t_aA2 t_aA4 t_aA5.
              (TestEq.Eq t_aA4) =>
              TestEq.C t_aA2 t_aA4 -> TestEq.C t_aA5 t_aA4 -> GHC.Bool.Bool
LclIdX
[Arity 3
 Str: DmdType SU(L)U(L)]
TestEq.eqC =
  __inline_me (\ (@ t_aA2)
                 (@ t_aA4)
                 (@ t_aA5)
                 ($dEq_aAb :: TestEq.Eq t_aA4)
                 (ds_d17S :: TestEq.C t_aA2 t_aA4)
                 (ds_d17T :: TestEq.C t_aA5 t_aA4) ->
                 case ds_d17S of _ { TestEq.C a_agq [ALWAYS Just L] ->
                 case ds_d17T of _ { TestEq.C a'_agr [ALWAYS Just L] ->
                 TestEq.eq' @ t_aA4 $dEq_aAb a_agq a'_agr
                 }
                 })

TestEq.eqTimes :: forall t_aAi t_aAj.
                  (TestEq.Eq t_aAi, TestEq.Eq t_aAj) =>
                  (t_aAi TestEq.:*: t_aAj)
                  -> (t_aAi TestEq.:*: t_aAj)
                  -> GHC.Bool.Bool
LclIdX
[Arity 4
 Str: DmdType SLU(LL)U(LL)]
TestEq.eqTimes =
  __inline_me (\ (@ t_aAi)
                 (@ t_aAj)
                 ($dEq_aAt :: TestEq.Eq t_aAi)
                 ($dEq_aAu :: TestEq.Eq t_aAj)
                 (ds_d17W :: t_aAi TestEq.:*: t_aAj)
                 (ds_d17X :: t_aAi TestEq.:*: t_aAj) ->
                 case ds_d17W
                 of _ { TestEq.:*: a_agi [ALWAYS Just L] b_agj [ALWAYS Just L] ->
                 case ds_d17X
                 of _ { TestEq.:*: a'_agk [ALWAYS Just L] b'_agl [ALWAYS Just L] ->
                 GHC.Classes.&&
                   (TestEq.eq' @ t_aAi $dEq_aAt a_agi a'_agk)
                   (TestEq.eq' @ t_aAj $dEq_aAu b_agj b'_agl)
                 }
                 })

TestEq.$fEq:*: :: forall a_agH b_agI.
                  (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                  TestEq.Eq (a_agH TestEq.:*: b_agI)
LclIdX[DFunId]
[Arity 4
 Str: DmdType SLU(LL)U(LL)]
TestEq.$fEq:*: =
  __inline_me (TestEq.eqTimes
               `cast` (forall a_agH b_agI.
                       (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                       sym (TestEq.NTCo:T:Eq (a_agH TestEq.:*: b_agI))
                       :: (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           (a_agH TestEq.:*: b_agI)
                           -> (a_agH TestEq.:*: b_agI)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           TestEq.T:Eq (a_agH TestEq.:*: b_agI))))

TestEq.eqPlus :: forall t_aAB t_aAH.
                 (TestEq.Eq t_aAB, TestEq.Eq t_aAH) =>
                 (t_aAB TestEq.:+: t_aAH)
                 -> (t_aAB TestEq.:+: t_aAH)
                 -> GHC.Bool.Bool
LclIdX
[Arity 4
 Str: DmdType LLSS]
TestEq.eqPlus =
  __inline_me (\ (@ t_aAB)
                 (@ t_aAH)
                 ($dEq_aAM :: TestEq.Eq t_aAB)
                 ($dEq_aAN :: TestEq.Eq t_aAH)
                 (ds_d180 :: t_aAB TestEq.:+: t_aAH)
                 (ds_d181 :: t_aAB TestEq.:+: t_aAH) ->
                 case ds_d180 of _ {
                   TestEq.L x_age ->
                     case ds_d181 of _ {
                       TestEq.L x'_agf [ALWAYS Just L] ->
                         TestEq.eq' @ t_aAB $dEq_aAM x_age x'_agf;
                       TestEq.R _ -> GHC.Bool.False
                     };
                   TestEq.R x_agg ->
                     case ds_d181 of _ {
                       TestEq.L _ -> GHC.Bool.False;
                       TestEq.R x'_agh [ALWAYS Just L] ->
                         TestEq.eq' @ t_aAH $dEq_aAN x_agg x'_agh
                     }
                 })

TestEq.$fEq:+: :: forall a_agJ b_agK.
                  (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                  TestEq.Eq (a_agJ TestEq.:+: b_agK)
LclIdX[DFunId]
[Arity 4
 Str: DmdType LLSS]
TestEq.$fEq:+: =
  __inline_me (TestEq.eqPlus
               `cast` (forall a_agJ b_agK.
                       (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                       sym (TestEq.NTCo:T:Eq (a_agJ TestEq.:+: b_agK))
                       :: (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           (a_agJ TestEq.:+: b_agK)
                           -> (a_agJ TestEq.:+: b_agK)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           TestEq.T:Eq (a_agJ TestEq.:+: b_agK))))

TestEq.eqU :: forall t_aAS t_aAT. t_aAS -> t_aAT -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType AA]
TestEq.eqU =
  __inline_me (\ (@ t_aAS) (@ t_aAT) _ _ -> GHC.Bool.True)

TestEq.$fEqU :: TestEq.Eq TestEq.U
LclIdX[DFunId]
[Arity 2
 Str: DmdType AA]
TestEq.$fEqU =
  __inline_me ((TestEq.eqU @ TestEq.U @ TestEq.U)
               `cast` (sym (TestEq.NTCo:T:Eq TestEq.U)
                       :: (TestEq.U -> TestEq.U -> GHC.Bool.Bool) ~ TestEq.T:Eq TestEq.U))

TestEq.toLogic :: forall t_aB0
                         t_aB6
                         t_aBa
                         t_aBe
                         t_aBk
                         t_aBu
                         t_aBE
                         t_aBM.
                  (TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                   TestEq.:+: (TestEq.C t_aB6 TestEq.U
                               TestEq.:+: (TestEq.C t_aBa TestEq.U
                                           TestEq.:+: (TestEq.C t_aBe (TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     t_aBk
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 t_aBu
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: (TestEq.C
                                                                                             t_aBE
                                                                                             (TestEq.Rec
                                                                                                TestEq.Logic
                                                                                              TestEq.:*: TestEq.Rec
                                                                                                           TestEq.Logic)
                                                                                           TestEq.:+: TestEq.C
                                                                                                        t_aBM
                                                                                                        (TestEq.Rec
                                                                                                           TestEq.Logic
                                                                                                         TestEq.:*: TestEq.Rec
                                                                                                                      TestEq.Logic))))))))
                  -> TestEq.Logic
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.toLogic =
  __inline_me (\ (@ t_aB0)
                 (@ t_aB6)
                 (@ t_aBa)
                 (@ t_aBe)
                 (@ t_aBk)
                 (@ t_aBu)
                 (@ t_aBE)
                 (@ t_aBM)
                 (ds_d18a :: TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                             TestEq.:+: (TestEq.C t_aB6 TestEq.U
                                         TestEq.:+: (TestEq.C t_aBa TestEq.U
                                                     TestEq.:+: (TestEq.C
                                                                   t_aBe (TestEq.Rec TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               t_aBk
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: (TestEq.C
                                                                                           t_aBu
                                                                                           (TestEq.Rec
                                                                                              TestEq.Logic
                                                                                            TestEq.:*: TestEq.Rec
                                                                                                         TestEq.Logic)
                                                                                         TestEq.:+: (TestEq.C
                                                                                                       t_aBE
                                                                                                       (TestEq.Rec
                                                                                                          TestEq.Logic
                                                                                                        TestEq.:*: TestEq.Rec
                                                                                                                     TestEq.Logic)
                                                                                                     TestEq.:+: TestEq.C
                                                                                                                  t_aBM
                                                                                                                  (TestEq.Rec
                                                                                                                     TestEq.Logic
                                                                                                                   TestEq.:*: TestEq.Rec
                                                                                                                                TestEq.Logic)))))))) ->
                 case ds_d18a of _ {
                   TestEq.L ds_d18D [ALWAYS Just U(U(L))] ->
                     case ds_d18D of _ { TestEq.C ds_d18E [ALWAYS Just U(L)] ->
                     case ds_d18E of _ { TestEq.Var f0_ag4 [ALWAYS Just L] ->
                     TestEq.VarL f0_ag4
                     }
                     };
                   TestEq.R ds_d18b [ALWAYS Just S] ->
                     case ds_d18b of _ {
                       TestEq.L ds_d18B [ALWAYS Just U(U())] ->
                         case ds_d18B of _ { TestEq.C ds_d18C [ALWAYS Just U()] ->
                         case ds_d18C of _ { TestEq.U -> TestEq.T }
                         };
                       TestEq.R ds_d18c [ALWAYS Just S] ->
                         case ds_d18c of _ {
                           TestEq.L ds_d18z [ALWAYS Just U(U())] ->
                             case ds_d18z of _ { TestEq.C ds_d18A [ALWAYS Just U()] ->
                             case ds_d18A of _ { TestEq.U -> TestEq.F }
                             };
                           TestEq.R ds_d18d [ALWAYS Just S] ->
                             case ds_d18d of _ {
                               TestEq.L ds_d18x [ALWAYS Just U(U(L))] ->
                                 case ds_d18x of _ { TestEq.C ds_d18y [ALWAYS Just U(L)] ->
                                 case ds_d18y of _ { TestEq.Rec f0_ag5 [ALWAYS Just L] ->
                                 TestEq.Not f0_ag5
                                 }
                                 };
                               TestEq.R ds_d18e [ALWAYS Just S] ->
                                 case ds_d18e of _ {
                                   TestEq.L ds_d18t [ALWAYS Just U(U(U(L)U(L)))] ->
                                     case ds_d18t
                                     of _ { TestEq.C ds_d18u [ALWAYS Just U(U(L)U(L))] ->
                                     case ds_d18u
                                     of _
                                     { TestEq.:*: ds_d18v [ALWAYS Just U(L)]
                                                  ds_d18w [ALWAYS Just U(L)] ->
                                     case ds_d18v of _ { TestEq.Rec f0_ag6 [ALWAYS Just L] ->
                                     case ds_d18w of _ { TestEq.Rec f1_ag7 [ALWAYS Just L] ->
                                     TestEq.Impl f0_ag6 f1_ag7
                                     }
                                     }
                                     }
                                     };
                                   TestEq.R ds_d18f [ALWAYS Just S] ->
                                     case ds_d18f of _ {
                                       TestEq.L ds_d18p [ALWAYS Just U(U(U(L)U(L)))] ->
                                         case ds_d18p
                                         of _ { TestEq.C ds_d18q [ALWAYS Just U(U(L)U(L))] ->
                                         case ds_d18q
                                         of _
                                         { TestEq.:*: ds_d18r [ALWAYS Just U(L)]
                                                      ds_d18s [ALWAYS Just U(L)] ->
                                         case ds_d18r of _ { TestEq.Rec f0_ag8 [ALWAYS Just L] ->
                                         case ds_d18s of _ { TestEq.Rec f1_ag9 [ALWAYS Just L] ->
                                         TestEq.Equiv f0_ag8 f1_ag9
                                         }
                                         }
                                         }
                                         };
                                       TestEq.R ds_d18g [ALWAYS Just S] ->
                                         case ds_d18g of _ {
                                           TestEq.L ds_d18l [ALWAYS Just U(U(U(L)U(L)))] ->
                                             case ds_d18l
                                             of _ { TestEq.C ds_d18m [ALWAYS Just U(U(L)U(L))] ->
                                             case ds_d18m
                                             of _
                                             { TestEq.:*: ds_d18n [ALWAYS Just U(L)]
                                                          ds_d18o [ALWAYS Just U(L)] ->
                                             case ds_d18n
                                             of _ { TestEq.Rec f0_aga [ALWAYS Just L] ->
                                             case ds_d18o
                                             of _ { TestEq.Rec f1_agb [ALWAYS Just L] ->
                                             TestEq.Conj f0_aga f1_agb
                                             }
                                             }
                                             }
                                             };
                                           TestEq.R ds_d18h [ALWAYS Just U(U(U(L)U(L)))] ->
                                             case ds_d18h
                                             of _ { TestEq.C ds_d18i [ALWAYS Just U(U(L)U(L))] ->
                                             case ds_d18i
                                             of _
                                             { TestEq.:*: ds_d18j [ALWAYS Just U(L)]
                                                          ds_d18k [ALWAYS Just U(L)] ->
                                             case ds_d18j
                                             of _ { TestEq.Rec f0_agc [ALWAYS Just L] ->
                                             case ds_d18k
                                             of _ { TestEq.Rec f1_agd [ALWAYS Just L] ->
                                             TestEq.Disj f0_agc f1_agd
                                             }
                                             }
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 })

TestEq.fromLogic :: forall c_aC3
                           c_aCb
                           c_aCl
                           c_aCz
                           c_aCT
                           c_aDg
                           c_aDG
                           c_aE7.
                    TestEq.Logic
                    -> TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C c_aCb TestEq.U
                                   TestEq.:+: (TestEq.C c_aCl TestEq.U
                                               TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         c_aCT
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     c_aDg
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 c_aDG
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            c_aE7
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic)))))))
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.fromLogic =
  __inline_me (\ (@ c_aC3)
                 (@ c_aCb)
                 (@ c_aCl)
                 (@ c_aCz)
                 (@ c_aCT)
                 (@ c_aDg)
                 (@ c_aDG)
                 (@ c_aE7)
                 (ds_d19h :: TestEq.Logic) ->
                 case ds_d19h of _ {
                   TestEq.VarL f0_afU [ALWAYS Just L] ->
                     TestEq.L
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.C
                          @ c_aC3
                          @ (TestEq.Var GHC.Base.String)
                          (TestEq.Var @ GHC.Base.String f0_afU));
                   TestEq.T ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.L
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.C @ c_aCb @ TestEq.U TestEq.U));
                   TestEq.F ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.L
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.C @ c_aCl @ TestEq.U TestEq.U)));
                   TestEq.Not f0_afV [ALWAYS Just L] ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.L
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.C
                                   @ c_aCz
                                   @ (TestEq.Rec TestEq.Logic)
                                   (TestEq.Rec @ TestEq.Logic f0_afV)))));
                   TestEq.Impl f0_afW [ALWAYS Just L] f1_afX [ALWAYS Just L] ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.L
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.C
                                      @ c_aCT
                                      @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      (TestEq.:*:
                                         @ (TestEq.Rec TestEq.Logic)
                                         @ (TestEq.Rec TestEq.Logic)
                                         (TestEq.Rec @ TestEq.Logic f0_afW)
                                         (TestEq.Rec @ TestEq.Logic f1_afX)))))));
                   TestEq.Equiv f0_afY [ALWAYS Just L] f1_afZ [ALWAYS Just L] ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.L
                                      @ (TestEq.C
                                           c_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.C
                                         @ c_aDg
                                         @ (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         (TestEq.:*:
                                            @ (TestEq.Rec TestEq.Logic)
                                            @ (TestEq.Rec TestEq.Logic)
                                            (TestEq.Rec @ TestEq.Logic f0_afY)
                                            (TestEq.Rec @ TestEq.Logic f1_afZ))))))));
                   TestEq.Conj f0_ag0 [ALWAYS Just L] f1_ag1 [ALWAYS Just L] ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.R
                                      @ (TestEq.C
                                           c_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.L
                                         @ (TestEq.C
                                              c_aDG
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         @ (TestEq.C
                                              c_aE7
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         (TestEq.C
                                            @ c_aDG
                                            @ (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            (TestEq.:*:
                                               @ (TestEq.Rec TestEq.Logic)
                                               @ (TestEq.Rec TestEq.Logic)
                                               (TestEq.Rec @ TestEq.Logic f0_ag0)
                                               (TestEq.Rec @ TestEq.Logic f1_ag1)))))))));
                   TestEq.Disj f0_ag2 [ALWAYS Just L] f1_ag3 [ALWAYS Just L] ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c_aCb TestEq.U
                          TestEq.:+: (TestEq.C c_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c_aCb TestEq.U)
                          @ (TestEq.C c_aCl TestEq.U
                             TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c_aCl TestEq.U)
                             @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.R
                                      @ (TestEq.C
                                           c_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.R
                                         @ (TestEq.C
                                              c_aDG
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         @ (TestEq.C
                                              c_aE7
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         (TestEq.C
                                            @ c_aE7
                                            @ (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            (TestEq.:*:
                                               @ (TestEq.Rec TestEq.Logic)
                                               @ (TestEq.Rec TestEq.Logic)
                                               (TestEq.Rec @ TestEq.Logic f0_ag2)
                                               (TestEq.Rec @ TestEq.Logic f1_ag3)))))))))
                 })

TestEq.$fRepresentableLogic :: TestEq.Representable TestEq.Logic
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableLogic =
  TestEq.D:Representable
    @ TestEq.Logic
    ((TestEq.toLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (sym TestEq.TFCo:R:RepLogic -> TestEq.Logic
             :: (TestEq.R:RepLogic -> TestEq.Logic)
                  ~
                (TestEq.Rep TestEq.Logic -> TestEq.Logic)))
    ((TestEq.fromLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (TestEq.Logic -> sym TestEq.TFCo:R:RepLogic
             :: (TestEq.Logic -> TestEq.R:RepLogic)
                  ~
                (TestEq.Logic -> TestEq.Rep TestEq.Logic)))

TestEq.toTree :: forall t_aEI t_aEN t_aEV.
                 (TestEq.C t_aEV TestEq.U
                  TestEq.:+: TestEq.C
                               t_aEI
                               (TestEq.Var t_aEN
                                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aEN))))
                 -> TestEq.Tree t_aEN
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.toTree =
  __inline_me (\ (@ t_aEI)
                 (@ t_aEN)
                 (@ t_aEV)
                 (ds_d19q :: TestEq.C t_aEV TestEq.U
                             TestEq.:+: TestEq.C
                                          t_aEI
                                          (TestEq.Var t_aEN
                                           TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                                       TestEq.:*: TestEq.Rec
                                                                    (TestEq.Tree t_aEN)))) ->
                 case ds_d19q of _ {
                   TestEq.L ds_d19x [ALWAYS Just U(U())] ->
                     case ds_d19x of _ { TestEq.C ds_d19y [ALWAYS Just U()] ->
                     case ds_d19y of _ { TestEq.U -> TestEq.Leaf @ t_aEN }
                     };
                   TestEq.R ds_d19r [ALWAYS Just U(U(U(L)U(U(L)U(L))))] ->
                     case ds_d19r
                     of _ { TestEq.C ds_d19s [ALWAYS Just U(U(L)U(U(L)U(L)))] ->
                     case ds_d19s
                     of _
                     { TestEq.:*: ds_d19t [ALWAYS Just U(L)]
                                  ds_d19u [ALWAYS Just U(U(L)U(L))] ->
                     case ds_d19t of _ { TestEq.Var x_afR [ALWAYS Just L] ->
                     case ds_d19u
                     of _
                     { TestEq.:*: ds_d19v [ALWAYS Just U(L)]
                                  ds_d19w [ALWAYS Just U(L)] ->
                     case ds_d19v of _ { TestEq.Rec l_afS [ALWAYS Just L] ->
                     case ds_d19w of _ { TestEq.Rec r_afT [ALWAYS Just L] ->
                     TestEq.Bin @ t_aEN x_afR l_afS r_afT
                     }
                     }
                     }
                     }
                     }
                     }
                 })

TestEq.fromTree :: forall t_aF3 c_aFf c_aFl.
                   TestEq.Tree t_aF3
                   -> TestEq.C c_aFl TestEq.U
                      TestEq.:+: TestEq.C
                                   c_aFf
                                   (TestEq.Var t_aF3
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.fromTree =
  __inline_me (\ (@ t_aF3)
                 (@ c_aFf)
                 (@ c_aFl)
                 (ds_d19J :: TestEq.Tree t_aF3) ->
                 case ds_d19J of _ {
                   TestEq.Leaf ->
                     TestEq.L
                       @ (TestEq.C c_aFl TestEq.U)
                       @ (TestEq.C
                            c_aFf
                            (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
                       (TestEq.C @ c_aFl @ TestEq.U TestEq.U);
                   TestEq.Bin x_afO [ALWAYS Just L]
                              l_afP [ALWAYS Just L]
                              r_afQ [ALWAYS Just L] ->
                     TestEq.R
                       @ (TestEq.C c_aFl TestEq.U)
                       @ (TestEq.C
                            c_aFf
                            (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
                       (TestEq.C
                          @ c_aFf
                          @ (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
                          (TestEq.:*:
                             @ (TestEq.Var t_aF3)
                             @ (TestEq.Rec (TestEq.Tree t_aF3)
                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))
                             (TestEq.Var @ t_aF3 x_afO)
                             (TestEq.:*:
                                @ (TestEq.Rec (TestEq.Tree t_aF3))
                                @ (TestEq.Rec (TestEq.Tree t_aF3))
                                (TestEq.Rec @ (TestEq.Tree t_aF3) l_afP)
                                (TestEq.Rec @ (TestEq.Tree t_aF3) r_afQ))))
                 })

a_s1et :: forall a_agL.
          TestEq.Tree a_agL
          -> TestEq.C TestEq.Leaf TestEq.U
             TestEq.:+: TestEq.C
                          TestEq.Bin
                          (TestEq.Var a_agL
                           TestEq.:*: (TestEq.Rec (TestEq.Tree a_agL)
                                       TestEq.:*: TestEq.Rec (TestEq.Tree a_agL)))
LclId
[Arity 1
 Str: DmdType S]
a_s1et =
  \ (@ a_agL) (sub_a178 :: TestEq.Tree a_agL) ->
    TestEq.fromTree @ a_agL @ TestEq.Bin @ TestEq.Leaf sub_a178

TestEq.$fRepresentableTree :: forall a_agL.
                              TestEq.Representable (TestEq.Tree a_agL)
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableTree =
  \ (@ a_XnZ) ->
    TestEq.D:Representable
      @ (TestEq.Tree a_XnZ)
      ((TestEq.toTree @ TestEq.Bin @ a_XnZ @ TestEq.Leaf)
       `cast` (sym (TestEq.TFCo:R:RepTree a_XnZ) -> TestEq.Tree a_XnZ
               :: (TestEq.R:RepTree a_XnZ -> TestEq.Tree a_XnZ)
                    ~
                  (TestEq.Rep (TestEq.Tree a_XnZ) -> TestEq.Tree a_XnZ)))
      ((a_s1et @ a_XnZ)
       `cast` (TestEq.Tree a_XnZ -> sym (TestEq.TFCo:R:RepTree a_XnZ)
               :: (TestEq.Tree a_XnZ -> TestEq.R:RepTree a_XnZ)
                    ~
                  (TestEq.Tree a_XnZ -> TestEq.Rep (TestEq.Tree a_XnZ))))

TestEq.tree0 :: TestEq.Tree GHC.Types.Int
LclIdX
[Str: DmdType]
TestEq.tree0 =
  TestEq.Bin
    @ GHC.Types.Int
    a_s1dD
    (TestEq.Leaf @ GHC.Types.Int)
    (TestEq.Leaf @ GHC.Types.Int)

TestEq.tree1 :: TestEq.Tree GHC.Types.Int
LclIdX
[Str: DmdType]
TestEq.tree1 =
  TestEq.Bin @ GHC.Types.Int lvl_s1ub TestEq.tree0 TestEq.tree0

TestEq.logic1 :: TestEq.Logic
LclIdX
[Str: DmdType]
TestEq.logic1 = TestEq.Impl a_s1dP a_s1dR

from_a14M :: forall a_agN. [a_agN] -> TestEq.Rep [a_agN]
LclId
[Arity 1
 Str: DmdType S]
from_a14M =
  \ (@ a_agN) (ds_d1dc :: [a_agN]) ->
    case ds_d1dc of _ {
      [] ->
        (lvl_s1up @ a_agN)
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN]);
      : a_agO [ALWAYS Just L] as_agP [ALWAYS Just L] ->
        (TestEq.R
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C
              @ TestEq.List_Cons_
              @ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
              (TestEq.:*:
                 @ (TestEq.Var a_agN)
                 @ (TestEq.Rec [a_agN])
                 (TestEq.Var @ a_agN a_agO)
                 (TestEq.Rec @ [a_agN] as_agP))))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN])
    }

to_a14s :: forall a_agN. TestEq.Rep [a_agN] -> [a_agN]
LclId
[Arity 1
 Str: DmdType S]
to_a14s =
  \ (@ a_agN) (ds_d1cW :: TestEq.Rep [a_agN]) ->
    case ds_d1cW
         `cast` (sym (sym (TestEq.TFCo:R:Rep[] a_agN))
                 :: TestEq.Rep [a_agN] ~ TestEq.R:Rep[] a_agN)
    of _ {
      TestEq.L ds_d1d2 [ALWAYS Just U(U())] ->
        case ds_d1d2 of _ { TestEq.C ds_d1d3 [ALWAYS Just U()] ->
        case ds_d1d3 of _ { TestEq.U -> GHC.Types.[] @ a_agN }
        };
      TestEq.R ds_d1cY [ALWAYS Just U(U(U(L)U(L)))] ->
        case ds_d1cY of _ { TestEq.C ds_d1cZ [ALWAYS Just U(U(L)U(L))] ->
        case ds_d1cZ
        of _
        { TestEq.:*: ds_d1d0 [ALWAYS Just U(L)]
                     ds_d1d1 [ALWAYS Just U(L)] ->
        case ds_d1d0 of _ { TestEq.Var a_agZ [ALWAYS Just L] ->
        case ds_d1d1 of _ { TestEq.Rec as_ah0 [ALWAYS Just L] ->
        GHC.Types.: @ a_agN a_agZ as_ah0
        }
        }
        }
        }
    }

TestEq.$fRepresentable[] :: forall a_agN.
                            TestEq.Representable [a_agN]
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentable[] =
  \ (@ a_agN) ->
    TestEq.D:Representable
      @ [a_agN] (to_a14s @ a_agN) (from_a14M @ a_agN)

conName_a14j :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Nil_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a14j =
  __inline_me (\ (@ t_a14g::* -> * -> *) (@ a_a14h) _ -> lvl_s1uo)

TestEq.$fConstructorList_Nil_ :: TestEq.Constructor
                                   TestEq.List_Nil_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorList_Nil_ =
  TestEq.D:Constructor
    @ TestEq.List_Nil_
    conName_a14j
    $s$dmconFixity_s1pe
    $s$dmconIsRecord_s1pA

conFixity_a14a :: forall (t_afB :: * -> * -> *) a_afC.
                  t_afB TestEq.List_Cons_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
conFixity_a14a =
  __inline_me (\ (@ t_a147::* -> * -> *) (@ a_a148) _ -> lvl_s1ua)

conName_a144 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Cons_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a144 =
  __inline_me (\ (@ t_a141::* -> * -> *) (@ a_a142) _ -> lvl_s1u8)

TestEq.$fConstructorList_Cons_ :: TestEq.Constructor
                                    TestEq.List_Cons_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorList_Cons_ =
  TestEq.D:Constructor
    @ TestEq.List_Cons_
    conName_a144
    conFixity_a14a
    $s$dmconIsRecord_s1pz

conName_a13T :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Bin a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a13T =
  __inline_me (\ (@ t_a13Q::* -> * -> *) (@ a_a13R) _ -> lvl_s1un)

TestEq.$fConstructorBin :: TestEq.Constructor TestEq.Bin
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorBin =
  TestEq.D:Constructor
    @ TestEq.Bin conName_a13T $s$dmconFixity_s1pf $s$dmconIsRecord_s1py

conName_a13I :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Leaf a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a13I =
  __inline_me (\ (@ t_a13F::* -> * -> *) (@ a_a13G) _ -> lvl_s1xD)

TestEq.$fConstructorLeaf :: TestEq.Constructor TestEq.Leaf
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLeaf =
  TestEq.D:Constructor
    @ TestEq.Leaf
    conName_a13I
    $s$dmconFixity_s1pg
    $s$dmconIsRecord_s1px

conName_a13i :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Var_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a13i =
  __inline_me (\ (@ t_a13f::* -> * -> *) (@ a_a13g) _ -> lvl_s1ul)

TestEq.$fConstructorLogic_Var_ :: TestEq.Constructor
                                    TestEq.Logic_Var_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Var_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Var_
    conName_a13i
    $s$dmconFixity_s1ph
    $s$dmconIsRecord_s1pw

conName_a137 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Impl_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a137 =
  __inline_me (\ (@ t_a134::* -> * -> *) (@ a_a135) _ -> lvl_s1uk)

TestEq.$fConstructorLogic_Impl_ :: TestEq.Constructor
                                     TestEq.Logic_Impl_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Impl_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Impl_
    conName_a137
    $s$dmconFixity_s1pi
    $s$dmconIsRecord_s1pv

conName_a12W :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Equiv_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12W =
  __inline_me (\ (@ t_a12T::* -> * -> *) (@ a_a12U) _ -> lvl_s1uj)

TestEq.$fConstructorLogic_Equiv_ :: TestEq.Constructor
                                      TestEq.Logic_Equiv_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Equiv_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Equiv_
    conName_a12W
    $s$dmconFixity_s1pj
    $s$dmconIsRecord_s1pu

conName_a12L :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Conj_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12L =
  __inline_me (\ (@ t_a12I::* -> * -> *) (@ a_a12J) _ -> lvl_s1ui)

TestEq.$fConstructorLogic_Conj_ :: TestEq.Constructor
                                     TestEq.Logic_Conj_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Conj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Conj_
    conName_a12L
    $s$dmconFixity_s1pk
    $s$dmconIsRecord_s1pt

conName_a12A :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Disj_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12A =
  __inline_me (\ (@ t_a12x::* -> * -> *) (@ a_a12y) _ -> lvl_s1uh)

TestEq.$fConstructorLogic_Disj_ :: TestEq.Constructor
                                     TestEq.Logic_Disj_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Disj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Disj_
    conName_a12A
    $s$dmconFixity_s1pl
    $s$dmconIsRecord_s1ps

conName_a12p :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Not_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12p =
  __inline_me (\ (@ t_a12m::* -> * -> *) (@ a_a12n) _ -> lvl_s1ug)

TestEq.$fConstructorLogic_Not_ :: TestEq.Constructor
                                    TestEq.Logic_Not_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Not_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Not_
    conName_a12p
    $s$dmconFixity_s1pm
    $s$dmconIsRecord_s1pr

conName_a12e :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_T_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12e =
  __inline_me (\ (@ t_a12b::* -> * -> *) (@ a_a12c) _ -> lvl_s1x8)

TestEq.$fConstructorLogic_T_ :: TestEq.Constructor TestEq.Logic_T_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_T_ =
  TestEq.D:Constructor
    @ TestEq.Logic_T_
    conName_a12e
    $s$dmconFixity_s1pn
    $s$dmconIsRecord_s1pq

conName_a123 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_F_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a123 =
  __inline_me (\ (@ t_a120::* -> * -> *) (@ a_a121) _ -> lvl_s1xb)

TestEq.$fConstructorLogic_F_ :: TestEq.Constructor TestEq.Logic_F_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_F_ =
  TestEq.D:Constructor
    @ TestEq.Logic_F_
    conName_a123
    $s$dmconFixity_s1po
    $s$dmconIsRecord_s1pp

eq'_a117 :: forall a_agF c_agG.
            (TestEq.Eq a_agF) =>
            TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool
LclId
[Arity 3
 Str: DmdType SU(L)U(L)]
eq'_a117 =
  __inline_me (\ (@ a_agF)
                 (@ c_agG)
                 ($dEq_a110 :: TestEq.Eq a_agF)
                 (eta_B2 :: TestEq.C c_agG a_agF)
                 (eta_B1 :: TestEq.C c_agG a_agF) ->
                 TestEq.eqC @ c_agG @ a_agF @ c_agG $dEq_a110 eta_B2 eta_B1)

TestEq.$fEqC :: forall a_agF c_agG.
                (TestEq.Eq a_agF) =>
                TestEq.Eq (TestEq.C c_agG a_agF)
LclIdX[DFunId]
[Arity 3
 Str: DmdType SU(L)U(L)]
TestEq.$fEqC =
  __inline_me (eq'_a117
               `cast` (forall a_agF c_agG.
                       (TestEq.Eq a_agF) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.C c_agG a_agF))
                       :: (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool)
                            ~
                          (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.T:Eq (TestEq.C c_agG a_agF))))

Rec {
eq'_a10s [ALWAYS LoopBreaker Nothing] :: forall a_agC.
                                         (TestEq.Eq a_agC) =>
                                         [a_agC] -> [a_agC] -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType L]
eq'_a10s =
  \ (@ a_agC) ($dEq_a10b [ALWAYS Just L] :: TestEq.Eq a_agC) ->
    let {
      a_s1MJ :: [a_agC] -> [a_agC] -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      a_s1MJ = eq'_a10s @ a_agC $dEq_a10b } in
    \ (eta_Xbg :: [a_agC]) (eta_Xmv :: [a_agC]) ->
      case eta_Xbg of _ {
        [] ->
          case eta_Xmv of _ { [] -> GHC.Bool.True; : _ _ -> GHC.Bool.False };
        : a_agO as_agP ->
          case eta_Xmv of _ {
            [] -> GHC.Bool.False;
            : a_XnU [ALWAYS Just L] as_XnW ->
              case ($dEq_a10b
                    `cast` (TestEq.NTCo:T:Eq a_agC
                            :: TestEq.T:Eq a_agC ~ (a_agC -> a_agC -> GHC.Bool.Bool)))
                     a_agO a_XnU
              of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True -> a_s1MJ as_agP as_XnW
              }
          }
      }
end Rec }

TestEq.$fEq[] :: forall a_agC.
                 (TestEq.Eq a_agC) =>
                 TestEq.Eq [a_agC]
LclIdX[DFunId]
[Arity 1
 Str: DmdType L]
TestEq.$fEq[] =
  __inline_me (eq'_a10s
               `cast` (forall a_agC.
                       (TestEq.Eq a_agC) =>
                       sym (TestEq.NTCo:T:Eq [a_agC])
                       :: (forall a_agC.
                           (TestEq.Eq a_agC) =>
                           [a_agC] -> [a_agC] -> GHC.Bool.Bool)
                            ~
                          (forall a_agC. (TestEq.Eq a_agC) => TestEq.T:Eq [a_agC])))

a_s1yG :: [GHC.Types.Char] -> [GHC.Types.Char] -> GHC.Bool.Bool
LclId
[Str: DmdType]
a_s1yG =
  eq'_a10s
    @ GHC.Types.Char
    (GHC.Base.$fEqChar_==
     `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
             :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                  ~
                TestEq.T:Eq GHC.Types.Char))

readListDefault_aZF :: Text.ParserCombinators.ReadP.ReadS
                         [TestEq.U]
LclId
[Arity 1
 Str: DmdType L]
readListDefault_aZF =
  Text.ParserCombinators.ReadP.run @ [TestEq.U] a_s1AJ

TestEq.$fReadU :: GHC.Read.Read TestEq.U
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fReadU =
  GHC.Read.D:Read
    @ TestEq.U
    a_s1fY
    readListDefault_aZF
    (a_s1At
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
    (a_s1AH
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.U])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.U])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.U]))

showList_aZy :: [TestEq.U] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aZy =
  \ (ds1_a1gk :: [TestEq.U])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.U ->
           GHC.Base.++
             @ GHC.Types.Char
             lvl_s1uY
             (let {
                lvl12_s1ML :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1ML =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1MN [ALWAYS LoopBreaker Nothing] :: [TestEq.U]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MN =
                  \ (ds2_a1gv :: [TestEq.U]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1ML;
                      : y_a1gA ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.U ->
                           GHC.Base.++ @ GHC.Types.Char lvl_s1uY (showl_s1MN ys_a1gB)
                           })
                    }; } in
              showl_s1MN xs_a1gr)
           })
    }

showsPrec_aZq :: GHC.Types.Int -> TestEq.U -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType AU()L]
showsPrec_aZq =
  __inline_me (\ _
                 (ds_d1cw :: TestEq.U)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds_d1cw of _ { TestEq.U ->
                 GHC.Base.++ @ GHC.Types.Char lvl_s1uY eta_B1
                 })

TestEq.$fShowU :: GHC.Show.Show TestEq.U
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowU =
  GHC.Show.D:Show @ TestEq.U showsPrec_aZq a_s1gh showList_aZy

readList_aYC :: forall a_afL b_afM.
                (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                Text.ParserCombinators.ReadP.ReadS [a_afL TestEq.:+: b_afM]
LclId
[Arity 2
 Str: DmdType LL]
readList_aYC =
  \ (@ a_Xtl)
    (@ b_Xtn)
    ($dRead_X1c0 :: GHC.Read.Read a_Xtl)
    ($dRead_X1c2 :: GHC.Read.Read b_Xtn) ->
    Text.ParserCombinators.ReadP.run
      @ [a_Xtl TestEq.:+: b_Xtn]
      (((GHC.Read.$dmreadList2
           @ (a_Xtl TestEq.:+: b_Xtn)
           ((a_s1Df @ a_Xtl @ b_Xtn $dRead_X1c0 $dRead_X1c2)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_Xtl TestEq.:+: b_Xtn))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_Xtl TestEq.:+: b_Xtn))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtl TestEq.:+: b_Xtn)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_Xtl TestEq.:+: b_Xtn]
                :: Text.ParserCombinators.ReadP.ReadP [a_Xtl TestEq.:+: b_Xtn]
                     ~
                   (forall b_a1zG.
                    ([a_Xtl TestEq.:+: b_Xtn] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [a_Xtl TestEq.:+: b_Xtn]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_Xtl TestEq.:+: b_Xtn]))

TestEq.$fRead:+: :: forall a_afL b_afM.
                    (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                    GHC.Read.Read (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:+: =
  __inline_me (\ (@ a_Xte)
                 (@ b_Xtg)
                 ($dRead_X1po [ALWAYS Just L] :: GHC.Read.Read a_Xte)
                 ($dRead_X1bV [ALWAYS Just L] :: GHC.Read.Read b_Xtg) ->
                 let {
                   a_s1Ia [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [a_Xte TestEq.:+: b_Xtg]
                   LclId
                   [Str: DmdType]
                   a_s1Ia = a_s1Dj @ a_Xte @ b_Xtg $dRead_X1po $dRead_X1bV } in
                 let {
                   a_s1Dh [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  (a_Xte TestEq.:+: b_Xtg)
                   LclId
                   [Str: DmdType]
                   a_s1Dh = a_s1Df @ a_Xte @ b_Xtg $dRead_X1po $dRead_X1bV } in
                 let {
                   a_s1nO [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [a_Xte TestEq.:+: b_Xtg]
                   LclId
                   [Str: DmdType]
                   a_s1nO = readList_aYC @ a_Xte @ b_Xtg $dRead_X1po $dRead_X1bV } in
                 letrec {
                   $dRead_s1nK :: GHC.Read.Read (a_Xte TestEq.:+: b_Xtg)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1nK =
                     GHC.Read.D:Read
                       @ (a_Xte TestEq.:+: b_Xtg)
                       a_s1nL
                       a_s1nO
                       (a_s1Dh
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_Xte TestEq.:+: b_Xtg))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_Xte TestEq.:+: b_Xtg))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_Xte TestEq.:+: b_Xtg)))
                       (a_s1Ia
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_Xte TestEq.:+: b_Xtg])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_Xte TestEq.:+: b_Xtg])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_Xte TestEq.:+: b_Xtg]));
                   a_s1nL [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_Xte TestEq.:+: b_Xtg)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1nL =
                     GHC.Read.$dmreadsPrec @ (a_Xte TestEq.:+: b_Xtg) $dRead_s1nK; } in
                 $dRead_s1nK)

showsPrec_aYb :: forall a_afL b_afM.
                 (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                 GHC.Types.Int -> (a_afL TestEq.:+: b_afM) -> GHC.Show.ShowS
LclId
[Arity 4
 Worker $wshowsPrec_s1LH
 Str: DmdType LLU(L)S]
showsPrec_aYb =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 (w_s1Ko :: GHC.Show.Show a_afL)
                 (w_s1Kp :: GHC.Show.Show b_afM)
                 (w_s1Kq :: GHC.Types.Int)
                 (w_s1Ku :: a_afL TestEq.:+: b_afM) ->
                 case w_s1Kq of _ { GHC.Types.I# ww_s1Ks ->
                 $wshowsPrec_s1LH @ a_afL @ b_afM w_s1Ko w_s1Kp ww_s1Ks w_s1Ku
                 })

showList_aYn :: forall a_afL b_afM.
                (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
LclId
[Arity 4
 Str: DmdType LLSL]
showList_aYn =
  \ (@ a_Xtx)
    (@ b_Xtz)
    ($dShow_aXP [ALWAYS Just L] :: GHC.Show.Show a_Xtx)
    ($dShow_aXQ [ALWAYS Just L] :: GHC.Show.Show b_Xtz)
    (eta_B2 :: [a_Xtx TestEq.:+: b_Xtz])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          ($wshowsPrec_s1LH
             @ a_Xtx
             @ b_Xtz
             $dShow_aXP
             $dShow_aXQ
             0
             x_a1gq
             (let {
                lvl12_s1MP :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1MP =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1MR [ALWAYS LoopBreaker Nothing] :: [a_Xtx TestEq.:+: b_Xtz]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MR =
                  \ (ds2_a1gv :: [a_Xtx TestEq.:+: b_Xtz]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1MP;
                      : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          ($wshowsPrec_s1LH
                             @ a_Xtx
                             @ b_Xtz
                             $dShow_aXP
                             $dShow_aXQ
                             0
                             y_a1gA
                             (showl_s1MR ys_a1gB))
                    }; } in
              showl_s1MR xs_a1gr))
    }

TestEq.$fShow:+: :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Show.Show (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:+: =
  __inline_me (\ (@ a_Xtw)
                 (@ b_Xty)
                 ($dShow_aXP [ALWAYS Just L] :: GHC.Show.Show a_Xtw)
                 ($dShow_aXQ [ALWAYS Just L] :: GHC.Show.Show b_Xty) ->
                 let {
                   a_s1nE :: [a_Xtw TestEq.:+: b_Xty] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1nE = showList_aYn @ a_Xtw @ b_Xty $dShow_aXP $dShow_aXQ } in
                 let {
                   a_s1nF :: GHC.Types.Int
                             -> (a_Xtw TestEq.:+: b_Xty)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)S]
                   a_s1nF = showsPrec_aYb @ a_Xtw @ b_Xty $dShow_aXP $dShow_aXQ } in
                 letrec {
                   $dShow_s1nC :: GHC.Show.Show (a_Xtw TestEq.:+: b_Xty)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1nC =
                     GHC.Show.D:Show @ (a_Xtw TestEq.:+: b_Xty) a_s1nF a_s1nD a_s1nE;
                   a_s1nD [ALWAYS LoopBreaker Nothing] :: (a_Xtw TestEq.:+: b_Xty)
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1nD =
                     GHC.Show.$dmshow @ (a_Xtw TestEq.:+: b_Xty) $dShow_s1nC; } in
                 $dShow_s1nC)

readList_aXb :: forall a_afJ b_afK.
                (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                Text.ParserCombinators.ReadP.ReadS [a_afJ TestEq.:*: b_afK]
LclId
[Arity 2
 Str: DmdType LL]
readList_aXb =
  \ (@ a_XtQ)
    (@ b_XtS)
    ($dRead_X1b6 :: GHC.Read.Read a_XtQ)
    ($dRead_X1b8 :: GHC.Read.Read b_XtS) ->
    Text.ParserCombinators.ReadP.run
      @ [a_XtQ TestEq.:*: b_XtS]
      (((GHC.Read.$dmreadList2
           @ (a_XtQ TestEq.:*: b_XtS)
           ((a_s1DO @ a_XtQ @ b_XtS $dRead_X1b6 $dRead_X1b8)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_XtQ TestEq.:*: b_XtS))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_XtQ TestEq.:*: b_XtS))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_XtQ TestEq.:*: b_XtS)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_XtQ TestEq.:*: b_XtS]
                :: Text.ParserCombinators.ReadP.ReadP [a_XtQ TestEq.:*: b_XtS]
                     ~
                   (forall b_a1zG.
                    ([a_XtQ TestEq.:*: b_XtS] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [a_XtQ TestEq.:*: b_XtS]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_XtQ TestEq.:*: b_XtS]))

TestEq.$fRead:*: :: forall a_afJ b_afK.
                    (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                    GHC.Read.Read (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:*: =
  __inline_me (\ (@ a_XtJ)
                 (@ b_XtL)
                 ($dRead_X1aZ [ALWAYS Just L] :: GHC.Read.Read a_XtJ)
                 ($dRead_X1b1 [ALWAYS Just L] :: GHC.Read.Read b_XtL) ->
                 let {
                   a_s1Ic [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [a_XtJ TestEq.:*: b_XtL]
                   LclId
                   [Str: DmdType]
                   a_s1Ic = a_s1DS @ a_XtJ @ b_XtL $dRead_X1aZ $dRead_X1b1 } in
                 let {
                   a_s1DQ :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (a_XtJ TestEq.:*: b_XtL)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1DQ = a_s1DO @ a_XtJ @ b_XtL $dRead_X1aZ $dRead_X1b1 } in
                 let {
                   a_s1ns [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [a_XtJ TestEq.:*: b_XtL]
                   LclId
                   [Str: DmdType]
                   a_s1ns = readList_aXb @ a_XtJ @ b_XtL $dRead_X1aZ $dRead_X1b1 } in
                 letrec {
                   $dRead_s1no :: GHC.Read.Read (a_XtJ TestEq.:*: b_XtL)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1no =
                     GHC.Read.D:Read
                       @ (a_XtJ TestEq.:*: b_XtL)
                       a_s1np
                       a_s1ns
                       (a_s1DQ
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_XtJ TestEq.:*: b_XtL))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_XtJ TestEq.:*: b_XtL))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_XtJ TestEq.:*: b_XtL)))
                       (a_s1Ic
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_XtJ TestEq.:*: b_XtL])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_XtJ TestEq.:*: b_XtL])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_XtJ TestEq.:*: b_XtL]));
                   a_s1np [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_XtJ TestEq.:*: b_XtL)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1np =
                     GHC.Read.$dmreadsPrec @ (a_XtJ TestEq.:*: b_XtL) $dRead_s1no; } in
                 $dRead_s1no)

showsPrec_aWK :: forall a_afJ b_afK.
                 (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                 GHC.Types.Int -> (a_afJ TestEq.:*: b_afK) -> GHC.Show.ShowS
LclId
[Arity 4
 Worker $wshowsPrec_s1LI
 Str: DmdType LLU(L)U(LL)]
showsPrec_aWK =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 (w_s1KA :: GHC.Show.Show a_afJ)
                 (w_s1KB :: GHC.Show.Show b_afK)
                 (w_s1KC :: GHC.Types.Int)
                 (w_s1KG :: a_afJ TestEq.:*: b_afK) ->
                 case w_s1KC of _ { GHC.Types.I# ww_s1KE ->
                 case w_s1KG of _ { TestEq.:*: ww_s1KI ww_s1KJ ->
                 $wshowsPrec_s1LI
                   @ a_afJ @ b_afK w_s1KA w_s1KB ww_s1KE ww_s1KI ww_s1KJ
                 }
                 })

showList_aWW :: forall a_afJ b_afK.
                (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
LclId
[Arity 4
 Str: DmdType LLSL]
showList_aWW =
  \ (@ a_XtZ)
    (@ b_Xu1)
    ($dShow_aWq [ALWAYS Just L] :: GHC.Show.Show a_XtZ)
    ($dShow_aWr [ALWAYS Just L] :: GHC.Show.Show b_Xu1)
    (eta_B2 :: [a_XtZ TestEq.:*: b_Xu1])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(LL)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.:*: ww_s1KI ww_s1KJ ->
           $wshowsPrec_s1LI
             @ a_XtZ
             @ b_Xu1
             $dShow_aWq
             $dShow_aWr
             0
             ww_s1KI
             ww_s1KJ
             (let {
                lvl12_s1MT :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1MT =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1MV [ALWAYS LoopBreaker Nothing] :: [a_XtZ TestEq.:*: b_Xu1]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MV =
                  \ (ds2_a1gv :: [a_XtZ TestEq.:*: b_Xu1]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1MT;
                      : y_a1gA [ALWAYS Just D(LL)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.:*: ww_X1U5 ww_X1U7 ->
                           $wshowsPrec_s1LI
                             @ a_XtZ
                             @ b_Xu1
                             $dShow_aWq
                             $dShow_aWr
                             0
                             ww_X1U5
                             ww_X1U7
                             (showl_s1MV ys_a1gB)
                           })
                    }; } in
              showl_s1MV xs_a1gr)
           })
    }

TestEq.$fShow:*: :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Show.Show (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:*: =
  __inline_me (\ (@ a_XtY)
                 (@ b_Xu0)
                 ($dShow_aWq [ALWAYS Just L] :: GHC.Show.Show a_XtY)
                 ($dShow_aWr [ALWAYS Just L] :: GHC.Show.Show b_Xu0) ->
                 let {
                   a_s1ni :: [a_XtY TestEq.:*: b_Xu0] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1ni = showList_aWW @ a_XtY @ b_Xu0 $dShow_aWq $dShow_aWr } in
                 let {
                   a_s1nj :: GHC.Types.Int
                             -> (a_XtY TestEq.:*: b_Xu0)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(LL)]
                   a_s1nj = showsPrec_aWK @ a_XtY @ b_Xu0 $dShow_aWq $dShow_aWr } in
                 letrec {
                   $dShow_s1ng :: GHC.Show.Show (a_XtY TestEq.:*: b_Xu0)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1ng =
                     GHC.Show.D:Show @ (a_XtY TestEq.:*: b_Xu0) a_s1nj a_s1nh a_s1ni;
                   a_s1nh [ALWAYS LoopBreaker Nothing] :: (a_XtY TestEq.:*: b_Xu0)
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1nh =
                     GHC.Show.$dmshow @ (a_XtY TestEq.:*: b_Xu0) $dShow_s1ng; } in
                 $dShow_s1ng)

readList_aVW :: forall c_afH a_afI.
                (GHC.Read.Read a_afI) =>
                Text.ParserCombinators.ReadP.ReadS [TestEq.C c_afH a_afI]
LclId
[Arity 1
 Str: DmdType L]
readList_aVW =
  \ (@ c_Xui) (@ a_Xuk) ($dRead_X1ao :: GHC.Read.Read a_Xuk) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.C c_Xui a_Xuk]
      (((GHC.Read.$dmreadList2
           @ (TestEq.C c_Xui a_Xuk)
           ((a_s1Ei @ c_Xui @ a_Xuk $dRead_X1ao)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (TestEq.C c_Xui a_Xuk))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xui a_Xuk))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xui a_Xuk)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [TestEq.C c_Xui a_Xuk]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xui a_Xuk]
                     ~
                   (forall b_a1zG.
                    ([TestEq.C c_Xui a_Xuk] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [TestEq.C c_Xui a_Xuk]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.C c_Xui a_Xuk]))

TestEq.$fReadC :: forall c_afH a_afI.
                  (GHC.Read.Read a_afI) =>
                  GHC.Read.Read (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadC =
  __inline_me (\ (@ c_Xuc)
                 (@ a_Xue)
                 ($dRead_X1ai [ALWAYS Just L] :: GHC.Read.Read a_Xue) ->
                 let {
                   a_s1Ie [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [TestEq.C c_Xuc a_Xue]
                   LclId
                   [Str: DmdType]
                   a_s1Ie = a_s1Em @ c_Xuc @ a_Xue $dRead_X1ai } in
                 let {
                   a_s1Ek :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuc a_Xue)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1Ek = a_s1Ei @ c_Xuc @ a_Xue $dRead_X1ai } in
                 let {
                   a_s1n6 [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.C c_Xuc a_Xue]
                   LclId
                   [Str: DmdType]
                   a_s1n6 = readList_aVW @ c_Xuc @ a_Xue $dRead_X1ai } in
                 letrec {
                   $dRead_s1n2 :: GHC.Read.Read (TestEq.C c_Xuc a_Xue)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1n2 =
                     GHC.Read.D:Read
                       @ (TestEq.C c_Xuc a_Xue)
                       a_s1n3
                       a_s1n6
                       (a_s1Ek
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (TestEq.C c_Xuc a_Xue))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuc a_Xue))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xuc a_Xue)))
                       (a_s1Ie
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [TestEq.C c_Xuc a_Xue])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xuc a_Xue])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [TestEq.C c_Xuc a_Xue]));
                   a_s1n3 [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.C c_Xuc a_Xue)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1n3 =
                     GHC.Read.$dmreadsPrec @ (TestEq.C c_Xuc a_Xue) $dRead_s1n2; } in
                 $dRead_s1n2)

showsPrec_aVA :: forall c_afH a_afI.
                 (GHC.Show.Show a_afI) =>
                 GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
LclId
[Arity 3
 Worker $wshowsPrec_s1LJ
 Str: DmdType LU(L)U(L)]
showsPrec_aVA =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 (w_s1KP :: GHC.Show.Show a_afI)
                 (w_s1KQ :: GHC.Types.Int)
                 (w_s1KU :: TestEq.C c_afH a_afI) ->
                 case w_s1KQ of _ { GHC.Types.I# ww_s1KS ->
                 case w_s1KU of _ { TestEq.C ww_s1KW ->
                 $wshowsPrec_s1LJ @ c_afH @ a_afI w_s1KP ww_s1KS ww_s1KW
                 }
                 })

showList_aVK :: forall c_afH a_afI.
                (GHC.Show.Show a_afI) =>
                [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showList_aVK =
  \ (@ c_Xuq)
    (@ a_Xus)
    ($dShow_aVm [ALWAYS Just L] :: GHC.Show.Show a_Xus)
    (eta_B2 :: [TestEq.C c_Xuq a_Xus])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(L)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.C ww_s1KW ->
           $wshowsPrec_s1LJ
             @ c_Xuq
             @ a_Xus
             $dShow_aVm
             0
             ww_s1KW
             (let {
                lvl12_s1MX :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1MX =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1MZ [ALWAYS LoopBreaker Nothing] :: [TestEq.C c_Xuq a_Xus]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MZ =
                  \ (ds2_a1gv :: [TestEq.C c_Xuq a_Xus]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1MX;
                      : y_a1gA [ALWAYS Just D(L)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.C ww_X1Um ->
                           $wshowsPrec_s1LJ
                             @ c_Xuq @ a_Xus $dShow_aVm 0 ww_X1Um (showl_s1MZ ys_a1gB)
                           })
                    }; } in
              showl_s1MZ xs_a1gr)
           })
    }

TestEq.$fShowC :: forall c_afH a_afI.
                  (GHC.Show.Show a_afI) =>
                  GHC.Show.Show (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowC =
  __inline_me (\ (@ c_Xup)
                 (@ a_Xur)
                 ($dShow_aVm [ALWAYS Just L] :: GHC.Show.Show a_Xur) ->
                 let {
                   a_s1mW :: [TestEq.C c_Xup a_Xur] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1mW = showList_aVK @ c_Xup @ a_Xur $dShow_aVm } in
                 let {
                   a_s1mX :: GHC.Types.Int -> TestEq.C c_Xup a_Xur -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a_s1mX = showsPrec_aVA @ c_Xup @ a_Xur $dShow_aVm } in
                 letrec {
                   $dShow_s1mU :: GHC.Show.Show (TestEq.C c_Xup a_Xur)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1mU =
                     GHC.Show.D:Show @ (TestEq.C c_Xup a_Xur) a_s1mX a_s1mV a_s1mW;
                   a_s1mV [ALWAYS LoopBreaker Nothing] :: TestEq.C c_Xup a_Xur
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1mV =
                     GHC.Show.$dmshow @ (TestEq.C c_Xup a_Xur) $dShow_s1mU; } in
                 $dShow_s1mU)

readList_aUS :: forall a_afG.
                (GHC.Read.Read a_afG) =>
                Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
LclId
[Arity 1
 Str: DmdType L]
readList_aUS =
  \ (@ a_XuG) ($dRead_X19I :: GHC.Read.Read a_XuG) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Var a_XuG]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Var a_XuG)
           ((a_s1EE @ a_XuG $dRead_X19I)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuG))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuG))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuG)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Var a_XuG]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuG]
                     ~
                   (forall b_a1zG.
                    ([TestEq.Var a_XuG] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [TestEq.Var a_XuG]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Var a_XuG]))

TestEq.$fReadVar :: forall a_afG.
                    (GHC.Read.Read a_afG) =>
                    GHC.Read.Read (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadVar =
  __inline_me (\ (@ a_XuB)
                 ($dRead_X19D [ALWAYS Just L] :: GHC.Read.Read a_XuB) ->
                 let {
                   a_s1Ig [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [TestEq.Var a_XuB]
                   LclId
                   [Str: DmdType]
                   a_s1Ig = a_s1EI @ a_XuB $dRead_X19D } in
                 let {
                   a_s1EG :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuB)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1EG = a_s1EE @ a_XuB $dRead_X19D } in
                 let {
                   a_s1mK [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.Var a_XuB]
                   LclId
                   [Str: DmdType]
                   a_s1mK = readList_aUS @ a_XuB $dRead_X19D } in
                 letrec {
                   $dRead_s1mG :: GHC.Read.Read (TestEq.Var a_XuB)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1mG =
                     GHC.Read.D:Read
                       @ (TestEq.Var a_XuB)
                       a_s1mH
                       a_s1mK
                       (a_s1EG
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuB))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuB))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuB)))
                       (a_s1Ig
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Var a_XuB])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuB])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Var a_XuB]));
                   a_s1mH [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Var a_XuB)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1mH =
                     GHC.Read.$dmreadsPrec @ (TestEq.Var a_XuB) $dRead_s1mG; } in
                 $dRead_s1mG)

showsPrec_aUw :: forall a_afG.
                 (GHC.Show.Show a_afG) =>
                 GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
LclId
[Arity 3
 Worker $wshowsPrec_s1LK
 Str: DmdType LU(L)U(L)]
showsPrec_aUw =
  __inline_me (\ (@ a_afG)
                 (w_s1L2 :: GHC.Show.Show a_afG)
                 (w_s1L3 :: GHC.Types.Int)
                 (w_s1L7 :: TestEq.Var a_afG) ->
                 case w_s1L3 of _ { GHC.Types.I# ww_s1L5 ->
                 case w_s1L7 of _ { TestEq.Var ww_s1L9 ->
                 $wshowsPrec_s1LK @ a_afG w_s1L2 ww_s1L5 ww_s1L9
                 }
                 })

showList_aUG :: forall a_afG.
                (GHC.Show.Show a_afG) =>
                [TestEq.Var a_afG] -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showList_aUG =
  \ (@ a_XuO)
    ($dShow_aUi [ALWAYS Just L] :: GHC.Show.Show a_XuO)
    (eta_B2 :: [TestEq.Var a_XuO])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(L)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Var ww_s1L9 ->
           $wshowsPrec_s1LK
             @ a_XuO
             $dShow_aUi
             0
             ww_s1L9
             (let {
                lvl12_s1N1 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1N1 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1N3 [ALWAYS LoopBreaker Nothing] :: [TestEq.Var a_XuO]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1N3 =
                  \ (ds2_a1gv :: [TestEq.Var a_XuO]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1N1;
                      : y_a1gA [ALWAYS Just D(L)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Var ww_X1UD ->
                           $wshowsPrec_s1LK @ a_XuO $dShow_aUi 0 ww_X1UD (showl_s1N3 ys_a1gB)
                           })
                    }; } in
              showl_s1N3 xs_a1gr)
           })
    }

TestEq.$fShowVar :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Show.Show (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowVar =
  __inline_me (\ (@ a_XuN)
                 ($dShow_aUi [ALWAYS Just L] :: GHC.Show.Show a_XuN) ->
                 let {
                   a_s1mA :: [TestEq.Var a_XuN] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1mA = showList_aUG @ a_XuN $dShow_aUi } in
                 let {
                   a_s1mB :: GHC.Types.Int -> TestEq.Var a_XuN -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a_s1mB = showsPrec_aUw @ a_XuN $dShow_aUi } in
                 letrec {
                   $dShow_s1my :: GHC.Show.Show (TestEq.Var a_XuN)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1my =
                     GHC.Show.D:Show @ (TestEq.Var a_XuN) a_s1mB a_s1mz a_s1mA;
                   a_s1mz [ALWAYS LoopBreaker Nothing] :: TestEq.Var a_XuN
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1mz = GHC.Show.$dmshow @ (TestEq.Var a_XuN) $dShow_s1my; } in
                 $dShow_s1my)

readList_aTO :: forall a_afF.
                (GHC.Read.Read a_afF) =>
                Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
LclId
[Arity 1
 Str: DmdType L]
readList_aTO =
  \ (@ a_Xv4) ($dRead_X193 :: GHC.Read.Read a_Xv4) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Rec a_Xv4]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Rec a_Xv4)
           ((a_s1F0 @ a_Xv4 $dRead_X193)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xv4))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xv4))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xv4)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Rec a_Xv4]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xv4]
                     ~
                   (forall b_a1zG.
                    ([TestEq.Rec a_Xv4] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [TestEq.Rec a_Xv4]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Rec a_Xv4]))

TestEq.$fReadRec :: forall a_afF.
                    (GHC.Read.Read a_afF) =>
                    GHC.Read.Read (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadRec =
  __inline_me (\ (@ a_XuZ)
                 ($dRead_X18Y [ALWAYS Just L] :: GHC.Read.Read a_XuZ) ->
                 let {
                   a_s1Ii [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [TestEq.Rec a_XuZ]
                   LclId
                   [Str: DmdType]
                   a_s1Ii = a_s1F4 @ a_XuZ $dRead_X18Y } in
                 let {
                   a_s1F2 :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_XuZ)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1F2 = a_s1F0 @ a_XuZ $dRead_X18Y } in
                 let {
                   a_s1mo [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.Rec a_XuZ]
                   LclId
                   [Str: DmdType]
                   a_s1mo = readList_aTO @ a_XuZ $dRead_X18Y } in
                 letrec {
                   $dRead_s1mk :: GHC.Read.Read (TestEq.Rec a_XuZ)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1mk =
                     GHC.Read.D:Read
                       @ (TestEq.Rec a_XuZ)
                       a_s1ml
                       a_s1mo
                       (a_s1F2
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_XuZ))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_XuZ))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_XuZ)))
                       (a_s1Ii
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Rec a_XuZ])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_XuZ])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Rec a_XuZ]));
                   a_s1ml [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Rec a_XuZ)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1ml =
                     GHC.Read.$dmreadsPrec @ (TestEq.Rec a_XuZ) $dRead_s1mk; } in
                 $dRead_s1mk)

showsPrec_aTs :: forall a_afF.
                 (GHC.Show.Show a_afF) =>
                 GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
LclId
[Arity 3
 Worker $wshowsPrec_s1LL
 Str: DmdType LU(L)U(L)]
showsPrec_aTs =
  __inline_me (\ (@ a_afF)
                 (w_s1Lf :: GHC.Show.Show a_afF)
                 (w_s1Lg :: GHC.Types.Int)
                 (w_s1Lk :: TestEq.Rec a_afF) ->
                 case w_s1Lg of _ { GHC.Types.I# ww_s1Li ->
                 case w_s1Lk of _ { TestEq.Rec ww_s1Lm ->
                 $wshowsPrec_s1LL @ a_afF w_s1Lf ww_s1Li ww_s1Lm
                 }
                 })

showList_aTC :: forall a_afF.
                (GHC.Show.Show a_afF) =>
                [TestEq.Rec a_afF] -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showList_aTC =
  \ (@ a_Xvc)
    ($dShow_aTe [ALWAYS Just L] :: GHC.Show.Show a_Xvc)
    (eta_B2 :: [TestEq.Rec a_Xvc])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(L)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Rec ww_s1Lm ->
           $wshowsPrec_s1LL
             @ a_Xvc
             $dShow_aTe
             0
             ww_s1Lm
             (let {
                lvl12_s1N5 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1N5 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1N7 [ALWAYS LoopBreaker Nothing] :: [TestEq.Rec a_Xvc]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1N7 =
                  \ (ds2_a1gv :: [TestEq.Rec a_Xvc]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1N5;
                      : y_a1gA [ALWAYS Just D(L)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Rec ww_X1UV ->
                           $wshowsPrec_s1LL @ a_Xvc $dShow_aTe 0 ww_X1UV (showl_s1N7 ys_a1gB)
                           })
                    }; } in
              showl_s1N7 xs_a1gr)
           })
    }

TestEq.$fShowRec :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Show.Show (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowRec =
  __inline_me (\ (@ a_Xvb)
                 ($dShow_aTe [ALWAYS Just L] :: GHC.Show.Show a_Xvb) ->
                 let {
                   a_s1me :: [TestEq.Rec a_Xvb] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1me = showList_aTC @ a_Xvb $dShow_aTe } in
                 let {
                   a_s1mf :: GHC.Types.Int -> TestEq.Rec a_Xvb -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a_s1mf = showsPrec_aTs @ a_Xvb $dShow_aTe } in
                 letrec {
                   $dShow_s1mc :: GHC.Show.Show (TestEq.Rec a_Xvb)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1mc =
                     GHC.Show.D:Show @ (TestEq.Rec a_Xvb) a_s1mf a_s1md a_s1me;
                   a_s1md [ALWAYS LoopBreaker Nothing] :: TestEq.Rec a_Xvb
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1md = GHC.Show.$dmshow @ (TestEq.Rec a_Xvb) $dShow_s1mc; } in
                 $dShow_s1mc)

readListDefault_aRx :: Text.ParserCombinators.ReadP.ReadS
                         [TestEq.Fixity]
LclId
[Arity 1
 Str: DmdType L]
readListDefault_aRx =
  Text.ParserCombinators.ReadP.run @ [TestEq.Fixity] a_s1Gr

TestEq.$fReadFixity :: GHC.Read.Read TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fReadFixity =
  GHC.Read.D:Read
    @ TestEq.Fixity
    a_s1iR
    readListDefault_aRx
    (a_s1Gn
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (a_s1Gp
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Fixity]))

compare_aRe :: TestEq.Fixity
               -> TestEq.Fixity
               -> GHC.Ordering.Ordering
LclId
[Arity 2
 Str: DmdType SS]
compare_aRe =
  \ (a_axy :: TestEq.Fixity) (b_axz :: TestEq.Fixity) ->
    let {
      $j_s1Nb :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      LclId
      [Arity 1
       Str: DmdType L]
      $j_s1Nb =
        \ (a#_axF [ALWAYS Just L OneShot] :: GHC.Prim.Int#) ->
          let {
            $j_s1N9 :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            LclId
            [Arity 1
             Str: DmdType L]
            $j_s1N9 =
              \ (b#_axG [ALWAYS Just L OneShot] :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_axF b#_axG of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_axF b#_axG of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a_axy of _ {
                      TestEq.Prefix -> GHC.Ordering.EQ;
                      TestEq.Infix a1_axB a2_axC ->
                        case b_axz of _ {
                          TestEq.Prefix -> GHC.Ordering.EQ;
                          TestEq.Infix b1_axD [ALWAYS Just S] b2_axE ->
                            case a1_axB of _ {
                              TestEq.LeftAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.RightAssociative -> GHC.Ordering.LT;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.RightAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Ordering.GT;
                                  TestEq.RightAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.NotAssociative ->
                                case b1_axD of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  TestEq.NotAssociative -> GHC.Base.compareInt a2_axC b2_axE
                                }
                            }
                        }
                    }
                } } in
          case b_axz of _ {
            TestEq.Prefix -> $j_s1N9 0; TestEq.Infix _ _ -> $j_s1N9 1
          } } in
    case a_axy of _ {
      TestEq.Prefix -> $j_s1Nb 0; TestEq.Infix _ _ -> $j_s1Nb 1
    }

a_s1lc :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1lc =
  \ (x_a1j9 :: TestEq.Fixity) (y_a1ja :: TestEq.Fixity) ->
    case compare_aRe x_a1j9 y_a1ja of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

a_s1le :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1le =
  \ (x_a1jq :: TestEq.Fixity) (y_a1jr :: TestEq.Fixity) ->
    case compare_aRe x_a1jq y_a1jr of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

a_s1lg :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1lg =
  \ (x_a1jH :: TestEq.Fixity) (y_a1jI :: TestEq.Fixity) ->
    case compare_aRe x_a1jH y_a1jI of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

a_s1li :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1li =
  \ (x_a1jY :: TestEq.Fixity) (y_a1jZ :: TestEq.Fixity) ->
    case compare_aRe x_a1jY y_a1jZ of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

a_s1lk :: TestEq.Fixity -> TestEq.Fixity -> TestEq.Fixity
LclId
[Arity 2
 Str: DmdType SS]
a_s1lk =
  \ (x_a1kf :: TestEq.Fixity) (y_a1kg :: TestEq.Fixity) ->
    case compare_aRe x_a1kf y_a1kg of _ {
      __DEFAULT -> y_a1kg; GHC.Ordering.GT -> x_a1kf
    }

a_s1lm :: TestEq.Fixity -> TestEq.Fixity -> TestEq.Fixity
LclId
[Arity 2
 Str: DmdType SS]
a_s1lm =
  \ (x_a1kz :: TestEq.Fixity) (y_a1kA :: TestEq.Fixity) ->
    case compare_aRe x_a1kz y_a1kA of _ {
      __DEFAULT -> x_a1kz; GHC.Ordering.GT -> y_a1kA
    }

showsPrec_aQK :: GHC.Types.Int -> TestEq.Fixity -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showsPrec_aQK =
  \ (ds_d1ba :: GHC.Types.Int)
    (ds_d1bb :: TestEq.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds_d1bb of _ {
      TestEq.Prefix -> GHC.Base.++ @ GHC.Types.Char lvl_s1wU eta_B1;
      TestEq.Infix b1_axw [ALWAYS Just L] b2_axx [ALWAYS Just D(L)] ->
        case ds_d1ba of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        let {
          p_s1Nd :: GHC.Show.ShowS
          LclId
          [Arity 1
           Str: DmdType L]
          p_s1Nd =
            \ (x_X1yO :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1wX
                (case b1_axw of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       lvl_s1wN
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1GR [ALWAYS Just L] ->
                           GHC.Show.$wshowSignedInt 11 ww_a1GR x_X1yO
                           }));
                   TestEq.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       lvl_s1wP
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1GR [ALWAYS Just L] ->
                           GHC.Show.$wshowSignedInt 11 ww_a1GR x_X1yO
                           }));
                   TestEq.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       lvl_s1wR
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1GR [ALWAYS Just L] ->
                           GHC.Show.$wshowSignedInt 11 ww_a1GR x_X1yO
                           }))
                 }) } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False -> p_s1Nd eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.showSignedInt2
              (p_s1Nd
                 (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 eta_B1))
        }
        }
    }

showList_aQS :: [TestEq.Fixity] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aQS =
  \ (ds1_a1gk :: [TestEq.Fixity])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (showsPrec_aQK
             a_s1HU
             x_a1gq
             (let {
                lvl12_s1Nf :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1Nf =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1Nh [ALWAYS LoopBreaker Nothing] :: [TestEq.Fixity]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1Nh =
                  \ (ds2_a1gv :: [TestEq.Fixity]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1Nf;
                      : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (showsPrec_aQK a_s1HU y_a1gA (showl_s1Nh ys_a1gB))
                    }; } in
              showl_s1Nh xs_a1gr))
    }

a_s1j0 :: TestEq.Fixity -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType S]
a_s1j0 =
  \ (x_X1xA :: TestEq.Fixity) ->
    showsPrec_aQK
      GHC.Base.zeroInt x_X1xA (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowFixity :: GHC.Show.Show TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowFixity =
  GHC.Show.D:Show @ TestEq.Fixity showsPrec_aQK a_s1j0 showList_aQS

==_aQi :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
==_aQi =
  \ (ds_d1b4 :: TestEq.Fixity) (ds_d1b5 :: TestEq.Fixity) ->
    case ds_d1b4 of _ {
      TestEq.Prefix ->
        case ds_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.True; TestEq.Infix _ _ -> GHC.Bool.False
        };
      TestEq.Infix a1_axl a2_axm ->
        case ds_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.False;
          TestEq.Infix b1_axn [ALWAYS Just S] b2_axo ->
            case a1_axl of _ {
              TestEq.LeftAssociative ->
                case b1_axn of _ {
                  TestEq.LeftAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1ya [ALWAYS Just L] ->
                    case b2_axo of _ { GHC.Types.I# y_a1ye [ALWAYS Just L] ->
                    GHC.Prim.==# x_a1ya y_a1ye
                    }
                    };
                  TestEq.RightAssociative -> GHC.Bool.False;
                  TestEq.NotAssociative -> GHC.Bool.False
                };
              TestEq.RightAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.RightAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1ya [ALWAYS Just L] ->
                    case b2_axo of _ { GHC.Types.I# y_a1ye [ALWAYS Just L] ->
                    GHC.Prim.==# x_a1ya y_a1ye
                    }
                    }
                };
              TestEq.NotAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.NotAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1ya [ALWAYS Just L] ->
                    case b2_axo of _ { GHC.Types.I# y_a1ye [ALWAYS Just L] ->
                    GHC.Prim.==# x_a1ya y_a1ye
                    }
                    }
                }
            }
        }
    }

/=_aQp :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
/=_aQp =
  \ (a_axt :: TestEq.Fixity) (b_axu :: TestEq.Fixity) ->
    case ==_aQi a_axt b_axu of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqFixity :: GHC.Classes.Eq TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fEqFixity = GHC.Classes.D:Eq @ TestEq.Fixity ==_aQi /=_aQp

TestEq.$fOrdFixity :: GHC.Classes.Ord TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fOrdFixity =
  GHC.Classes.D:Ord
    @ TestEq.Fixity
    TestEq.$fEqFixity
    compare_aRe
    a_s1lc
    a_s1le
    a_s1lg
    a_s1li
    a_s1lk
    a_s1lm

readListDefault_aPz :: Text.ParserCombinators.ReadP.ReadS
                         [TestEq.Associativity]
LclId
[Arity 1
 Str: DmdType L]
readListDefault_aPz =
  Text.ParserCombinators.ReadP.run @ [TestEq.Associativity] a_s1FT

TestEq.$fReadAssociativity :: GHC.Read.Read TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fReadAssociativity =
  GHC.Read.D:Read
    @ TestEq.Associativity
    a_s1iF
    readListDefault_aPz
    (a_s1FP
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
    (a_s1FR
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [TestEq.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Associativity]))

compare_aK1 :: TestEq.Associativity
               -> TestEq.Associativity
               -> GHC.Ordering.Ordering
LclId
[Arity 2
 Str: DmdType SS]
compare_aK1 =
  \ (a_axg :: TestEq.Associativity)
    (b_axh :: TestEq.Associativity) ->
    case a_axg of _ {
      TestEq.LeftAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.EQ;
          TestEq.RightAssociative -> GHC.Ordering.LT;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.RightAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.GT;
          TestEq.RightAssociative -> GHC.Ordering.EQ;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.NotAssociative ->
        case b_axh of _ {
          __DEFAULT -> GHC.Ordering.GT;
          TestEq.NotAssociative -> GHC.Ordering.EQ
        }
    }

showList_aJx :: [TestEq.Associativity] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aJx =
  \ (ds1_a1gk :: [TestEq.Associativity])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1Nn [ALWAYS Just L] :: GHC.Base.String
             LclId
             [Str: DmdType]
             eta_s1Nn =
               let {
                 lvl12_s1Nj :: [GHC.Types.Char]
                 LclId
                 [Str: DmdType]
                 lvl12_s1Nj =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
               letrec {
                 showl_s1Nl [ALWAYS LoopBreaker Nothing] :: [TestEq.Associativity]
                                                            -> [GHC.Types.Char]
                 LclId
                 [Arity 1
                  Str: DmdType S]
                 showl_s1Nl =
                   \ (ds2_a1gv :: [TestEq.Associativity]) ->
                     case ds2_a1gv of _ {
                       [] -> lvl12_s1Nj;
                       : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_a1gA of _ {
                              TestEq.LeftAssociative ->
                                GHC.Base.++ @ GHC.Types.Char lvl_s1wN (showl_s1Nl ys_a1gB);
                              TestEq.RightAssociative ->
                                GHC.Base.++ @ GHC.Types.Char lvl_s1wP (showl_s1Nl ys_a1gB);
                              TestEq.NotAssociative ->
                                GHC.Base.++ @ GHC.Types.Char lvl_s1wR (showl_s1Nl ys_a1gB)
                            })
                     }; } in
               showl_s1Nl xs_a1gr } in
           case x_a1gq of _ {
             TestEq.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char lvl_s1wN eta_s1Nn;
             TestEq.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char lvl_s1wP eta_s1Nn;
             TestEq.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char lvl_s1wR eta_s1Nn
           })
    }

showsPrec_aJp :: GHC.Types.Int
                 -> TestEq.Associativity
                 -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType ASL]
showsPrec_aJp =
  __inline_me (\ _
                 (ds_d1aU :: TestEq.Associativity)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds_d1aU of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++ @ GHC.Types.Char lvl_s1wN eta_B1;
                   TestEq.RightAssociative ->
                     GHC.Base.++ @ GHC.Types.Char lvl_s1wP eta_B1;
                   TestEq.NotAssociative ->
                     GHC.Base.++ @ GHC.Types.Char lvl_s1wR eta_B1
                 })

TestEq.$fShowAssociativity :: GHC.Show.Show TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowAssociativity =
  GHC.Show.D:Show
    @ TestEq.Associativity showsPrec_aJp a_s1iV showList_aJx

/=_aJk :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
/=_aJk =
  \ (a_axe :: TestEq.Associativity)
    (b_axf :: TestEq.Associativity) ->
    case a_axe of _ {
      TestEq.LeftAssociative ->
        case b_axf of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

==_aJd :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
==_aJd =
  \ (a_axa :: TestEq.Associativity)
    (b_axb :: TestEq.Associativity) ->
    case a_axa of _ {
      TestEq.LeftAssociative ->
        case b_axb of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

TestEq.$fEqAssociativity :: GHC.Classes.Eq TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fEqAssociativity =
  GHC.Classes.D:Eq @ TestEq.Associativity ==_aJd /=_aJk

TestEq.$fOrdAssociativity :: GHC.Classes.Ord TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ TestEq.Associativity
    TestEq.$fEqAssociativity
    compare_aK1
    a_s1kS
    a_s1kU
    a_s1kW
    a_s1kY
    a_s1l0
    a_s1l2

Rec {
==_aIY [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> TestEq.Logic
                                       -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
==_aIY =
  \ (ds_d1aD :: TestEq.Logic) (ds_d1aE :: TestEq.Logic) ->
    let {
      $wfail_s1Np :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1
       Str: DmdType A]
      $wfail_s1Np =
        \ _ ->
          case ds_d1aD of _ {
            TestEq.VarL _ ->
              case ds_d1aE of _ {
                TestEq.VarL _ -> GHC.Bool.True;
                TestEq.T -> GHC.Bool.False;
                TestEq.F -> GHC.Bool.False;
                TestEq.Not _ -> GHC.Bool.False;
                TestEq.Impl _ _ -> GHC.Bool.False;
                TestEq.Equiv _ _ -> GHC.Bool.False;
                TestEq.Conj _ _ -> GHC.Bool.False;
                TestEq.Disj _ _ -> GHC.Bool.False
              };
            TestEq.T ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.T -> GHC.Bool.True
              };
            TestEq.F ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.F -> GHC.Bool.True
              };
            TestEq.Not _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Not _ -> GHC.Bool.True
              };
            TestEq.Impl _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Impl _ _ -> GHC.Bool.True
              };
            TestEq.Equiv _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Equiv _ _ -> GHC.Bool.True
              };
            TestEq.Conj _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Conj _ _ -> GHC.Bool.True
              };
            TestEq.Disj _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Disj _ _ -> GHC.Bool.True
              }
          } } in
    case ds_d1aD of _ {
      __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
      TestEq.VarL a1_awK ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.VarL b1_awL [ALWAYS Just S] ->
            GHC.Base.eqString a1_awK b1_awL
        };
      TestEq.Not a1_awM ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Not b1_awN [ALWAYS Just S] -> ==_aIY a1_awM b1_awN
        };
      TestEq.Impl a1_awO a2_awP ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Impl b1_awQ [ALWAYS Just S] b2_awR ->
            case ==_aIY a1_awO b1_awQ of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_awP b2_awR
            }
        };
      TestEq.Equiv a1_awS a2_awT ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Equiv b1_awU [ALWAYS Just S] b2_awV ->
            case ==_aIY a1_awS b1_awU of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_awT b2_awV
            }
        };
      TestEq.Conj a1_awW a2_awX ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Conj b1_awY [ALWAYS Just S] b2_awZ ->
            case ==_aIY a1_awW b1_awY of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_awX b2_awZ
            }
        };
      TestEq.Disj a1_ax0 a2_ax1 ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Disj b1_ax2 [ALWAYS Just S] b2_ax3 ->
            case ==_aIY a1_ax0 b1_ax2 of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_ax1 b2_ax3
            }
        }
    }
end Rec }

/=_aJ6 :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
/=_aJ6 =
  \ (a_ax8 :: TestEq.Logic) (b_ax9 :: TestEq.Logic) ->
    case ==_aIY a_ax8 b_ax9 of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqLogic0 :: GHC.Classes.Eq TestEq.Logic
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fEqLogic0 = GHC.Classes.D:Eq @ TestEq.Logic ==_aIY /=_aJ6

Rec {
$sshowsPrec_s1RR :: TestEq.Logic
                    -> GHC.Prim.Int#
                    -> GHC.Base.String
                    -> [GHC.Types.Char]
LclId
[Arity 2
 Str: DmdType SL]
$sshowsPrec_s1RR =
  \ (sc_s1Qy :: TestEq.Logic) (sc_s1Qz :: GHC.Prim.Int#) ->
    case sc_s1Qy of _ {
      TestEq.VarL b1_awv [ALWAYS Just L] ->
        case GHC.Prim.>=# sc_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1x5
                (GHC.Show.$fShowChar_showList b1_awv x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1x5
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
        };
      TestEq.T -> lvl_s1x7;
      TestEq.F -> lvl_s1xa;
      TestEq.Not b1_awx [ALWAYS Just L] ->
        let {
          g_s1Nr [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nr = showsPrec_aIk lvl_s1Ox b1_awx } in
        case GHC.Prim.>=# sc_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1xf (g_s1Nr x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xf
                   (g_s1Nr
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.Impl b1_awz [ALWAYS Just L] b2_awA [ALWAYS Just L] ->
        let {
          g_s1Nv [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nv = showsPrec_aIk lvl_s1Ox b2_awA } in
        let {
          f_s1Nt [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nt = showsPrec_aIk lvl_s1Ox b1_awz } in
        case GHC.Prim.>=# sc_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xj
                (f_s1Nt
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nv x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xj
                   (f_s1Nt
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nv
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Equiv b1_awC [ALWAYS Just L] b2_awD [ALWAYS Just L] ->
        let {
          g_s1Nz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nz = showsPrec_aIk lvl_s1Ox b2_awD } in
        let {
          f_s1Nx [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nx = showsPrec_aIk lvl_s1Ox b1_awC } in
        case GHC.Prim.>=# sc_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xo
                (f_s1Nx
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nz x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xo
                   (f_s1Nx
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nz
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Conj b1_awF [ALWAYS Just L] b2_awG [ALWAYS Just L] ->
        let {
          g_s1ND [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1ND = showsPrec_aIk lvl_s1Ox b2_awG } in
        let {
          f_s1NB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NB = showsPrec_aIk lvl_s1Ox b1_awF } in
        case GHC.Prim.>=# sc_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xt
                (f_s1NB
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1ND x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xt
                   (f_s1NB
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1ND
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Disj b1_awI [ALWAYS Just L] b2_awJ [ALWAYS Just L] ->
        let {
          g_s1NH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1NH = showsPrec_aIk lvl_s1Ox b2_awJ } in
        let {
          f_s1NF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NF = showsPrec_aIk lvl_s1Ox b1_awI } in
        case GHC.Prim.>=# sc_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xy
                (f_s1NF
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1NH x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xy
                   (f_s1NF
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1NH
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
    }
showsPrec_aIk [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                              -> TestEq.Logic
                                              -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType LS
 RULES: "SC:showsPrec0" [0]
            forall {sc_s1Qy :: TestEq.Logic sc_s1Qz :: GHC.Prim.Int#}
              showsPrec_aIk (GHC.Types.I# sc_s1Qz) sc_s1Qy
              = $sshowsPrec_s1RR sc_s1Qy sc_s1Qz]
showsPrec_aIk =
  \ (a_awu :: GHC.Types.Int) (ds_d1au :: TestEq.Logic) ->
    case ds_d1au of _ {
      TestEq.VarL b1_awv [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1x5
                (GHC.Show.$fShowChar_showList b1_awv x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1x5
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
        }
        };
      TestEq.T -> lvl_s1x7;
      TestEq.F -> lvl_s1xa;
      TestEq.Not b1_awx [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        let {
          g_s1Nr [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nr = showsPrec_aIk lvl_s1Ox b1_awx } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1xf (g_s1Nr x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xf
                   (g_s1Nr
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        }
        };
      TestEq.Impl b1_awz [ALWAYS Just L] b2_awA [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        let {
          g_s1Nv [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nv = showsPrec_aIk lvl_s1Ox b2_awA } in
        let {
          f_s1Nt [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nt = showsPrec_aIk lvl_s1Ox b1_awz } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xj
                (f_s1Nt
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nv x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xj
                   (f_s1Nt
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nv
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        };
      TestEq.Equiv b1_awC [ALWAYS Just L] b2_awD [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        let {
          g_s1Nz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nz = showsPrec_aIk lvl_s1Ox b2_awD } in
        let {
          f_s1Nx [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nx = showsPrec_aIk lvl_s1Ox b1_awC } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xo
                (f_s1Nx
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nz x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xo
                   (f_s1Nx
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nz
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        };
      TestEq.Conj b1_awF [ALWAYS Just L] b2_awG [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        let {
          g_s1ND [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1ND = showsPrec_aIk lvl_s1Ox b2_awG } in
        let {
          f_s1NB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NB = showsPrec_aIk lvl_s1Ox b1_awF } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xt
                (f_s1NB
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1ND x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xt
                   (f_s1NB
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1ND
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        };
      TestEq.Disj b1_awI [ALWAYS Just L] b2_awJ [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
        let {
          g_s1NH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1NH = showsPrec_aIk lvl_s1Ox b2_awJ } in
        let {
          f_s1NF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NF = showsPrec_aIk lvl_s1Ox b1_awI } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xy
                (f_s1NF
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1NH x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xy
                   (f_s1NF
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1NH
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        }
    }
end Rec }

showList_aIt :: [TestEq.Logic] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aIt =
  \ (ds1_a1gk :: [TestEq.Logic])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (showsPrec_aIk
             a_s1HU
             x_a1gq
             (let {
                lvl12_s1NJ :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NJ =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1NL [ALWAYS LoopBreaker Nothing] :: [TestEq.Logic]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NL =
                  \ (ds2_a1gv :: [TestEq.Logic]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NJ;
                      : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (showsPrec_aIk a_s1HU y_a1gA (showl_s1NL ys_a1gB))
                    }; } in
              showl_s1NL xs_a1gr))
    }

a_s1ly :: TestEq.Logic -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType S]
a_s1ly =
  \ (x_X1yM :: TestEq.Logic) ->
    showsPrec_aIk
      GHC.Base.zeroInt x_X1yM (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowLogic :: GHC.Show.Show TestEq.Logic
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowLogic =
  GHC.Show.D:Show @ TestEq.Logic showsPrec_aIk a_s1ly showList_aIt

Rec {
==_aH2 [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                       (GHC.Classes.Eq a_afw) =>
                                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType L]
==_aH2 =
  \ (@ a_afw) ($dEq_aGJ [ALWAYS Just L] :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1NN :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1NN = ==_aH2 @ a_afw $dEq_aGJ } in
    \ (ds_d1ao :: TestEq.Tree a_afw) (ds_d1ap :: TestEq.Tree a_afw) ->
      case ds_d1ao of _ {
        TestEq.Leaf ->
          case ds_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
          };
        TestEq.Bin a1_awi a2_awj a3_awk ->
          case ds_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.False;
            TestEq.Bin b1_awl [ALWAYS Just L] b2_awm b3_awn ->
              case $dEq_aGJ
              of _ { GHC.Classes.D:Eq tpl_XiZ [ALWAYS Just C(C(S))] _ ->
              case tpl_XiZ a1_awi b1_awl of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True ->
                  case ==_s1NN a2_awj b2_awm of _ {
                    GHC.Bool.False -> GHC.Bool.False;
                    GHC.Bool.True -> ==_s1NN a3_awk b3_awn
                  }
              }
              }
          }
      }
end Rec }

/=_aHb :: forall a_afw.
          (GHC.Classes.Eq a_afw) =>
          TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType L]
/=_aHb =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1NP [ALWAYS Just L] :: TestEq.Tree a_afw
                                 -> TestEq.Tree a_afw
                                 -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1NP = ==_aH2 @ a_afw $dEq_aGJ } in
    \ (a_aws :: TestEq.Tree a_afw) (b_awt :: TestEq.Tree a_afw) ->
      case ==_s1NP a_aws b_awt of _ {
        GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
      }

TestEq.$fEqTree0 :: forall a_afw.
                    (GHC.Classes.Eq a_afw) =>
                    GHC.Classes.Eq (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fEqTree0 =
  __inline_me (\ (@ a_afw)
                 ($dEq_aGJ [ALWAYS Just L] :: GHC.Classes.Eq a_afw) ->
                 GHC.Classes.D:Eq
                   @ (TestEq.Tree a_afw)
                   (==_aH2 @ a_afw $dEq_aGJ)
                   (/=_aHb @ a_afw $dEq_aGJ))

Rec {
showsPrec_aGw [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                              (GHC.Show.Show a_afw) =>
                                              GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
showsPrec_aGw =
  \ (@ a_afw) ($dShow_aFW [ALWAYS Just L] :: GHC.Show.Show a_afw) ->
    let {
      a_s1NR :: GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
      LclId
      [Str: DmdType]
      a_s1NR = showsPrec_aGw @ a_afw $dShow_aFW } in
    \ (ds_d1ak :: GHC.Types.Int) (ds_d1al :: TestEq.Tree a_afw) ->
      case ds_d1al of _ {
        TestEq.Leaf -> lvl_s1xC;
        TestEq.Bin b1_awf [ALWAYS Just L]
                   b2_awg [ALWAYS Just L]
                   b3_awh [ALWAYS Just L] ->
          case ds_d1ak of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
          let {
            g_s1NX [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            g_s1NX = a_s1NR lvl_s1Ox b3_awh } in
          let {
            f_s1NV [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f_s1NV = a_s1NR lvl_s1Ox b2_awg } in
          let {
            f_s1NT [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f_s1NT =
              case $dShow_aFW
              of _ { GHC.Show.D:Show tpl_Xjk [ALWAYS Just C(C(S))] _ _ ->
              tpl_Xjk lvl_s1Ox b1_awf
              } } in
          let {
            p_s1NZ :: GHC.Show.ShowS
            LclId
            [Arity 1
             Str: DmdType L]
            p_s1NZ =
              \ (x_X1AO :: GHC.Base.String) ->
                GHC.Base.++
                  @ GHC.Types.Char
                  lvl_s1xG
                  (f_s1NT
                     (GHC.Types.:
                        @ GHC.Types.Char
                        GHC.Show.showSpace1
                        (f_s1NV
                           (GHC.Types.:
                              @ GHC.Types.Char GHC.Show.showSpace1 (g_s1NX x_X1AO))))) } in
          case GHC.Prim.>=# x_a1Do 11 of _ {
            GHC.Bool.False -> p_s1NZ;
            GHC.Bool.True ->
              \ (x_a1hb :: GHC.Base.String) ->
                GHC.Types.:
                  @ GHC.Types.Char
                  GHC.Show.showSignedInt2
                  (p_s1NZ
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))
          }
          }
      }
end Rec }

showList_aGI :: forall a_afw.
                (GHC.Show.Show a_afw) =>
                [TestEq.Tree a_afw] -> GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
showList_aGI =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    GHC.Show.showList__
      @ (TestEq.Tree a_afw) (showsPrec_aGw @ a_afw $dShow_aFW a_s1HU)

TestEq.$fShowTree :: forall a_afw.
                     (GHC.Show.Show a_afw) =>
                     GHC.Show.Show (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowTree =
  __inline_me (\ (@ a_afw)
                 ($dShow_aFW [ALWAYS Just L] :: GHC.Show.Show a_afw) ->
                 let {
                   a_s1lR [ALWAYS Just L] :: [TestEq.Tree a_afw] -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a_s1lR = showList_aGI @ a_afw $dShow_aFW } in
                 let {
                   a_s1lS [ALWAYS Just L] :: GHC.Types.Int
                                             -> TestEq.Tree a_afw
                                             -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a_s1lS = showsPrec_aGw @ a_afw $dShow_aFW } in
                 letrec {
                   $dShow_s1lP :: GHC.Show.Show (TestEq.Tree a_afw)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1lP =
                     GHC.Show.D:Show @ (TestEq.Tree a_afw) a_s1lS a_s1lQ a_s1lR;
                   a_s1lQ [ALWAYS LoopBreaker Nothing] :: TestEq.Tree a_afw
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1lQ = GHC.Show.$dmshow @ (TestEq.Tree a_afw) $dShow_s1lP; } in
                 $dShow_s1lP)

Rec {
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType SS]
TestEq.eqTree =
  __inline_me (\ (x_agA :: TestEq.Tree GHC.Types.Int)
                 (y_agB :: TestEq.Tree GHC.Types.Int) ->
                 eq'_aFu
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x_agA)
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any y_agB))
eq'_aFu [ALWAYS LoopBreaker Nothing] :: (TestEq.C
                                           GHC.Prim.Any TestEq.U
                                         TestEq.:+: TestEq.C
                                                      GHC.Prim.Any
                                                      (TestEq.Var GHC.Types.Int
                                                       TestEq.:*: (TestEq.Rec
                                                                     (TestEq.Tree GHC.Types.Int)
                                                                   TestEq.:*: TestEq.Rec
                                                                                (TestEq.Tree
                                                                                   GHC.Types.Int))))
                                        -> (TestEq.C GHC.Prim.Any TestEq.U
                                            TestEq.:+: TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Var GHC.Types.Int
                                                          TestEq.:*: (TestEq.Rec
                                                                        (TestEq.Tree GHC.Types.Int)
                                                                      TestEq.:*: TestEq.Rec
                                                                                   (TestEq.Tree
                                                                                      GHC.Types.Int))))
                                        -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
eq'_aFu =
  \ (ds_d180 :: TestEq.C GHC.Prim.Any TestEq.U
                TestEq.:+: TestEq.C
                             GHC.Prim.Any
                             (TestEq.Var GHC.Types.Int
                              TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    (ds_d181 :: TestEq.C GHC.Prim.Any TestEq.U
                TestEq.:+: TestEq.C
                             GHC.Prim.Any
                             (TestEq.Var GHC.Types.Int
                              TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))) ->
    case ds_d180 of _ {
      TestEq.L x_age [ALWAYS Just D(A)] ->
        case ds_d181 of _ {
          TestEq.L x'_agf [ALWAYS Just U(A)] ->
            case x_age of _ { TestEq.C _ ->
            case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg [ALWAYS Just D(D(D(D(T))D(D(T)D(T))))] ->
        case ds_d181 of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh [ALWAYS Just U(U(U(U(L))D(D(T)D(T))))] ->
            case x_agg
            of _ { TestEq.C a_agq [ALWAYS Just U(U(U(L))D(D(T)D(T)))] ->
            case x'_agh
            of _ { TestEq.C a'_agr [ALWAYS Just U(U(U(L))D(D(T)D(T)))] ->
            case a_agq
            of _
            { TestEq.:*: a_agi [ALWAYS Just U(U(L))]
                         b_agj [ALWAYS Just D(D(T)D(T))] ->
            case a'_agr
            of _
            { TestEq.:*: a'_agk [ALWAYS Just U(U(L))]
                         b'_agl [ALWAYS Just D(D(T)D(T))] ->
            case a_agi of _ { TestEq.Var x_ags [ALWAYS Just U(L)] ->
            case a'_agk of _ { TestEq.Var x'_agt [ALWAYS Just U(L)] ->
            case x_ags of _ { GHC.Types.I# x_a1ya [ALWAYS Just L] ->
            case x'_agt of _ { GHC.Types.I# y_a1ye [ALWAYS Just L] ->
            case GHC.Prim.==# x_a1ya y_a1ye of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True ->
                case b_agj
                of _
                { TestEq.:*: a_Xp9 [ALWAYS Just U(S)] b_Xpb [ALWAYS Just D(T)] ->
                case b'_agl
                of _
                { TestEq.:*: a'_Xpf [ALWAYS Just U(S)] b'_Xph [ALWAYS Just D(T)] ->
                case a_Xp9 of _ { TestEq.Rec x_agu [ALWAYS Just S] ->
                case a'_Xpf of _ { TestEq.Rec x'_agv [ALWAYS Just S] ->
                case eq'_aFu
                       (TestEq.fromTree
                          @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x_agu)
                       (TestEq.fromTree
                          @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x'_agv)
                of _ {
                  GHC.Bool.False -> GHC.Bool.False;
                  GHC.Bool.True ->
                    case b_Xpb of _ { TestEq.Rec x_XrS [ALWAYS Just S] ->
                    case b'_Xph of _ { TestEq.Rec x'_XrW [ALWAYS Just S] ->
                    TestEq.eqTree x_XrS x'_XrW
                    }
                    }
                }
                }
                }
                }
                }
            }
            }
            }
            }
            }
            }
            }
            }
            }
        }
    }
end Rec }

TestEq.$fEqTree :: TestEq.Eq (TestEq.Tree GHC.Types.Int)
LclIdX[DFunId]
[Arity 2
 Str: DmdType SS]
TestEq.$fEqTree =
  TestEq.eqTree
  `cast` (sym (TestEq.NTCo:T:Eq (TestEq.Tree GHC.Types.Int))
          :: (TestEq.Tree GHC.Types.Int
              -> TestEq.Tree GHC.Types.Int
              -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq (TestEq.Tree GHC.Types.Int))

lvl_s1P4 :: TestEq.Var GHC.Types.Int
LclId
[]
lvl_s1P4 = TestEq.Var @ GHC.Types.Int lvl_s1ub

lvl_s1P6 :: TestEq.Rec (TestEq.Tree GHC.Types.Int)
LclId
[]
lvl_s1P6 = TestEq.Rec @ (TestEq.Tree GHC.Types.Int) TestEq.tree0

lvl_s1P5 :: TestEq.Rec (TestEq.Tree GHC.Types.Int)
            TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)
LclId
[]
lvl_s1P5 =
  TestEq.:*:
    @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
    @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
    lvl_s1P6
    lvl_s1P6

lvl_s1P3 :: TestEq.Var GHC.Types.Int
            TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                        TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))
LclId
[]
lvl_s1P3 =
  TestEq.:*:
    @ (TestEq.Var GHC.Types.Int)
    @ (TestEq.Rec (TestEq.Tree GHC.Types.Int)
       TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))
    lvl_s1P4
    lvl_s1P5

lvl_s1P2 :: TestEq.C
              GHC.Prim.Any
              (TestEq.Var GHC.Types.Int
               TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                           TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
LclId
[]
lvl_s1P2 =
  TestEq.C
    @ GHC.Prim.Any
    @ (TestEq.Var GHC.Types.Int
       TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                   TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
    lvl_s1P3

lvl_s1P1 :: TestEq.C GHC.Prim.Any TestEq.U
            TestEq.:+: TestEq.C
                         GHC.Prim.Any
                         (TestEq.Var GHC.Types.Int
                          TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                      TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
LclId
[]
lvl_s1P1 =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any TestEq.U)
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Var GHC.Types.Int
          TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                      TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    lvl_s1P2

TestEq.t1 :: GHC.Bool.Bool
LclIdX
[Str: DmdType]
TestEq.t1 = eq'_aFu lvl_s1P1 lvl_s1P1

Rec {
TestEq.eqLogic :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType SS]
TestEq.eqLogic =
  __inline_me (\ (x_agy :: TestEq.Logic) (y_agz :: TestEq.Logic) ->
                 eq'_aEx
                   (TestEq.fromLogic
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      x_agy)
                   (TestEq.fromLogic
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      y_agz))
$wa_s1LF :: TestEq.Logic
            -> TestEq.Rec TestEq.Logic
            -> TestEq.Logic
            -> TestEq.Rec TestEq.Logic
            -> GHC.Bool.Bool
LclId
[Arity 4
 Str: DmdType SLSL]
$wa_s1LF =
  \ (ww_s1K2 :: TestEq.Logic)
    (ww_s1K4 :: TestEq.Rec TestEq.Logic)
    (ww_s1Ka :: TestEq.Logic)
    (ww_s1Kc :: TestEq.Rec TestEq.Logic) ->
    case eq'_aEx
           (TestEq.fromLogic
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              ww_s1K2)
           (TestEq.fromLogic
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              ww_s1Ka)
    of _ {
      GHC.Bool.False -> GHC.Bool.False;
      GHC.Bool.True ->
        case ww_s1K4 of _ { TestEq.Rec x_Xsd [ALWAYS Just S] ->
        case ww_s1Kc of _ { TestEq.Rec x'_Xsh [ALWAYS Just S] ->
        TestEq.eqLogic x_Xsd x'_Xsh
        }
        }
    }
eq'_aEx [ALWAYS LoopBreaker Nothing] :: (TestEq.C
                                           GHC.Prim.Any (TestEq.Var GHC.Base.String)
                                         TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                     TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                                 TestEq.:+: (TestEq.C
                                                                               GHC.Prim.Any
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic)
                                                                             TestEq.:+: (TestEq.C
                                                                                           GHC.Prim.Any
                                                                                           (TestEq.Rec
                                                                                              TestEq.Logic
                                                                                            TestEq.:*: TestEq.Rec
                                                                                                         TestEq.Logic)
                                                                                         TestEq.:+: (TestEq.C
                                                                                                       GHC.Prim.Any
                                                                                                       (TestEq.Rec
                                                                                                          TestEq.Logic
                                                                                                        TestEq.:*: TestEq.Rec
                                                                                                                     TestEq.Logic)
                                                                                                     TestEq.:+: (TestEq.C
                                                                                                                   GHC.Prim.Any
                                                                                                                   (TestEq.Rec
                                                                                                                      TestEq.Logic
                                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                                 TestEq.Logic)
                                                                                                                 TestEq.:+: TestEq.C
                                                                                                                              GHC.Prim.Any
                                                                                                                              (TestEq.Rec
                                                                                                                                 TestEq.Logic
                                                                                                                               TestEq.:*: TestEq.Rec
                                                                                                                                            TestEq.Logic))))))))
                                        -> (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                                            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                        TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                                    TestEq.:+: (TestEq.C
                                                                                  GHC.Prim.Any
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic)
                                                                                TestEq.:+: (TestEq.C
                                                                                              GHC.Prim.Any
                                                                                              (TestEq.Rec
                                                                                                 TestEq.Logic
                                                                                               TestEq.:*: TestEq.Rec
                                                                                                            TestEq.Logic)
                                                                                            TestEq.:+: (TestEq.C
                                                                                                          GHC.Prim.Any
                                                                                                          (TestEq.Rec
                                                                                                             TestEq.Logic
                                                                                                           TestEq.:*: TestEq.Rec
                                                                                                                        TestEq.Logic)
                                                                                                        TestEq.:+: (TestEq.C
                                                                                                                      GHC.Prim.Any
                                                                                                                      (TestEq.Rec
                                                                                                                         TestEq.Logic
                                                                                                                       TestEq.:*: TestEq.Rec
                                                                                                                                    TestEq.Logic)
                                                                                                                    TestEq.:+: TestEq.C
                                                                                                                                 GHC.Prim.Any
                                                                                                                                 (TestEq.Rec
                                                                                                                                    TestEq.Logic
                                                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                                                               TestEq.Logic))))))))
                                        -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
eq'_aEx =
  \ (ds_X1kj :: TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                        TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  GHC.Prim.Any
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: (TestEq.C
                                                                              GHC.Prim.Any
                                                                              (TestEq.Rec
                                                                                 TestEq.Logic
                                                                               TestEq.:*: TestEq.Rec
                                                                                            TestEq.Logic)
                                                                            TestEq.:+: (TestEq.C
                                                                                          GHC.Prim.Any
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic)
                                                                                        TestEq.:+: TestEq.C
                                                                                                     GHC.Prim.Any
                                                                                                     (TestEq.Rec
                                                                                                        TestEq.Logic
                                                                                                      TestEq.:*: TestEq.Rec
                                                                                                                   TestEq.Logic))))))))
    (ds_X1kl :: TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                        TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  GHC.Prim.Any
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: (TestEq.C
                                                                              GHC.Prim.Any
                                                                              (TestEq.Rec
                                                                                 TestEq.Logic
                                                                               TestEq.:*: TestEq.Rec
                                                                                            TestEq.Logic)
                                                                            TestEq.:+: (TestEq.C
                                                                                          GHC.Prim.Any
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic)
                                                                                        TestEq.:+: TestEq.C
                                                                                                     GHC.Prim.Any
                                                                                                     (TestEq.Rec
                                                                                                        TestEq.Logic
                                                                                                      TestEq.:*: TestEq.Rec
                                                                                                                   TestEq.Logic)))))))) ->
    case ds_X1kj of _ {
      TestEq.L x_age [ALWAYS Just D(D(T))] ->
        case ds_X1kl of _ {
          TestEq.L x'_agf [ALWAYS Just U(U(L))] ->
            case x_age of _ { TestEq.C a_agq [ALWAYS Just U(L)] ->
            case x'_agf of _ { TestEq.C a'_agr [ALWAYS Just U(L)] ->
            TestEq.eqVar
              @ [GHC.Types.Char]
              (a_s1yG
               `cast` (trans
                         (sym (TestEq.NTCo:T:Eq [GHC.Types.Char]))
                         (TestEq.T:Eq [GHC.Types.Char])
                       :: ([GHC.Types.Char] -> [GHC.Types.Char] -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq [GHC.Types.Char]))
              a_agq
              a'_agr
            }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds_X1kl of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh [ALWAYS Just S] ->
            case x_agg of _ {
              TestEq.L x_age [ALWAYS Just D(A)] ->
                case x'_agh of _ {
                  TestEq.L x'_agf [ALWAYS Just U(A)] ->
                    case x_age of _ { TestEq.C _ ->
                    case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
                    };
                  TestEq.R _ -> GHC.Bool.False
                };
              TestEq.R x_XoK ->
                case x'_agh of _ {
                  TestEq.L _ -> GHC.Bool.False;
                  TestEq.R x'_XoO [ALWAYS Just S] ->
                    case x_XoK of _ {
                      TestEq.L x_age [ALWAYS Just D(A)] ->
                        case x'_XoO of _ {
                          TestEq.L x'_agf [ALWAYS Just U(A)] ->
                            case x_age of _ { TestEq.C _ ->
                            case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
                            };
                          TestEq.R _ -> GHC.Bool.False
                        };
                      TestEq.R x_XsE ->
                        case x'_XoO of _ {
                          TestEq.L _ -> GHC.Bool.False;
                          TestEq.R x'_XsI [ALWAYS Just S] ->
                            case x_XsE of _ {
                              TestEq.L x_age [ALWAYS Just D(D(T))] ->
                                case x'_XsI of _ {
                                  TestEq.L x'_agf [ALWAYS Just U(U(S))] ->
                                    case x_age of _ { TestEq.C a_agq [ALWAYS Just U(S)] ->
                                    case x'_agf of _ { TestEq.C a'_agr [ALWAYS Just U(S)] ->
                                    case a_agq of _ { TestEq.Rec x_agu [ALWAYS Just S] ->
                                    case a'_agr of _ { TestEq.Rec x'_agv [ALWAYS Just S] ->
                                    TestEq.eqLogic x_agu x'_agv
                                    }
                                    }
                                    }
                                    };
                                  TestEq.R _ -> GHC.Bool.False
                                };
                              TestEq.R x_XoX ->
                                case x'_XsI of _ {
                                  TestEq.L _ -> GHC.Bool.False;
                                  TestEq.R x'_Xp1 [ALWAYS Just S] ->
                                    case x_XoX of _ {
                                      TestEq.L x_age [ALWAYS Just D(D(D(T)T))] ->
                                        case x'_Xp1 of _ {
                                          TestEq.L x'_agf [ALWAYS Just U(U(U(S)L))] ->
                                            case x_age
                                            of _ { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                            case x'_agf
                                            of _ { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                            case a_agq of _ { TestEq.:*: ww_s1K0 ww_s1K4 ->
                                            case ww_s1K0 of _ { TestEq.Rec ww_s1K2 ->
                                            case a'_agr of _ { TestEq.:*: ww_s1K8 ww_s1Kc ->
                                            case ww_s1K8 of _ { TestEq.Rec ww_s1Ka ->
                                            $wa_s1LF ww_s1K2 ww_s1K4 ww_s1Ka ww_s1Kc
                                            }
                                            }
                                            }
                                            }
                                            }
                                            };
                                          TestEq.R _ -> GHC.Bool.False
                                        };
                                      TestEq.R x_Xp4 ->
                                        case x'_Xp1 of _ {
                                          TestEq.L _ -> GHC.Bool.False;
                                          TestEq.R x'_Xp8 [ALWAYS Just S] ->
                                            case x_Xp4 of _ {
                                              TestEq.L x_age [ALWAYS Just D(D(D(T)T))] ->
                                                case x'_Xp8 of _ {
                                                  TestEq.L x'_agf [ALWAYS Just U(U(U(S)L))] ->
                                                    case x_age
                                                    of _ { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                                    case x'_agf
                                                    of _ { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                                    case a_agq of _ { TestEq.:*: ww_s1K0 ww_s1K4 ->
                                                    case ww_s1K0 of _ { TestEq.Rec ww_s1K2 ->
                                                    case a'_agr of _ { TestEq.:*: ww_s1K8 ww_s1Kc ->
                                                    case ww_s1K8 of _ { TestEq.Rec ww_s1Ka ->
                                                    $wa_s1LF ww_s1K2 ww_s1K4 ww_s1Ka ww_s1Kc
                                                    }
                                                    }
                                                    }
                                                    }
                                                    }
                                                    };
                                                  TestEq.R _ -> GHC.Bool.False
                                                };
                                              TestEq.R x_Xpb ->
                                                case x'_Xp8 of _ {
                                                  TestEq.L _ -> GHC.Bool.False;
                                                  TestEq.R x'_Xpf [ALWAYS Just S] ->
                                                    case x_Xpb of _ {
                                                      TestEq.L x_age [ALWAYS Just D(D(D(T)T))] ->
                                                        case x'_Xpf of _ {
                                                          TestEq.L x'_agf [ALWAYS Just U(U(U(S)L))] ->
                                                            case x_age
                                                            of _
                                                            { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                                            case x'_agf
                                                            of _
                                                            { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                                            case a_agq
                                                            of _ { TestEq.:*: ww_s1K0 ww_s1K4 ->
                                                            case ww_s1K0
                                                            of _ { TestEq.Rec ww_s1K2 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww_s1K8 ww_s1Kc ->
                                                            case ww_s1K8
                                                            of _ { TestEq.Rec ww_s1Ka ->
                                                            $wa_s1LF ww_s1K2 ww_s1K4 ww_s1Ka ww_s1Kc
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            };
                                                          TestEq.R _ -> GHC.Bool.False
                                                        };
                                                      TestEq.R x_Xpi [ALWAYS Just D(D(D(T)T))] ->
                                                        case x'_Xpf of _ {
                                                          TestEq.L _ -> GHC.Bool.False;
                                                          TestEq.R x'_Xpm [ALWAYS Just U(U(U(S)L))] ->
                                                            case x_Xpi
                                                            of _
                                                            { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                                            case x'_Xpm
                                                            of _
                                                            { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                                            case a_agq
                                                            of _ { TestEq.:*: ww_s1K0 ww_s1K4 ->
                                                            case ww_s1K0
                                                            of _ { TestEq.Rec ww_s1K2 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww_s1K8 ww_s1Kc ->
                                                            case ww_s1K8
                                                            of _ { TestEq.Rec ww_s1Ka ->
                                                            $wa_s1LF ww_s1K2 ww_s1K4 ww_s1Ka ww_s1Kc
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
end Rec }

TestEq.$fEqLogic :: TestEq.Eq TestEq.Logic
LclIdX[DFunId]
[Arity 2
 Str: DmdType SS]
TestEq.$fEqLogic =
  TestEq.eqLogic
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.Logic)
          :: (TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq TestEq.Logic)

lvl_s1Pm :: TestEq.Rec TestEq.Logic
LclId
[]
lvl_s1Pm = TestEq.Rec @ TestEq.Logic a_s1dP

lvl_s1Pn :: TestEq.Rec TestEq.Logic
LclId
[]
lvl_s1Pn = TestEq.Rec @ TestEq.Logic a_s1dR

lvl_s1Pl :: TestEq.Rec TestEq.Logic
            TestEq.:*: TestEq.Rec TestEq.Logic
LclId
[]
lvl_s1Pl =
  TestEq.:*:
    @ (TestEq.Rec TestEq.Logic)
    @ (TestEq.Rec TestEq.Logic)
    lvl_s1Pm
    lvl_s1Pn

lvl_s1Pk :: TestEq.C
              GHC.Prim.Any
              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
LclId
[]
lvl_s1Pk =
  TestEq.C
    @ GHC.Prim.Any
    @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
    lvl_s1Pl

lvl_s1Pj :: TestEq.C
              GHC.Prim.Any
              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
            TestEq.:+: (TestEq.C
                          GHC.Prim.Any
                          (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                        TestEq.:+: (TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                    TestEq.:+: TestEq.C
                                                 GHC.Prim.Any
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)))
LclId
[]
lvl_s1Pj =
  TestEq.L
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
       TestEq.:+: (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: TestEq.C
                                GHC.Prim.Any
                                (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)))
    lvl_s1Pk

lvl_s1Pi :: TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
            TestEq.:+: (TestEq.C
                          GHC.Prim.Any
                          (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                        TestEq.:+: (TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                    TestEq.:+: (TestEq.C
                                                  GHC.Prim.Any
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)
                                                TestEq.:+: TestEq.C
                                                             GHC.Prim.Any
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic))))
LclId
[]
lvl_s1Pi =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic))
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
       TestEq.:+: (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: TestEq.C
                                            GHC.Prim.Any
                                            (TestEq.Rec TestEq.Logic
                                             TestEq.:*: TestEq.Rec TestEq.Logic))))
    lvl_s1Pj

lvl_s1Ph :: TestEq.C GHC.Prim.Any TestEq.U
            TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                        TestEq.:+: (TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                    TestEq.:+: (TestEq.C
                                                  GHC.Prim.Any
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              GHC.Prim.Any
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: TestEq.C
                                                                         GHC.Prim.Any
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)))))
LclId
[]
lvl_s1Ph =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any TestEq.U)
    @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
       TestEq.:+: (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: TestEq.C
                                                        GHC.Prim.Any
                                                        (TestEq.Rec TestEq.Logic
                                                         TestEq.:*: TestEq.Rec TestEq.Logic)))))
    lvl_s1Pi

lvl_s1Pg :: TestEq.C GHC.Prim.Any TestEq.U
            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                        TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                    TestEq.:+: (TestEq.C
                                                  GHC.Prim.Any
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              GHC.Prim.Any
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          GHC.Prim.Any
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: TestEq.C
                                                                                     GHC.Prim.Any
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic))))))
LclId
[]
lvl_s1Pg =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any TestEq.U)
    @ (TestEq.C GHC.Prim.Any TestEq.U
       TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    GHC.Prim.Any
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic))))))
    lvl_s1Ph

lvl_s1Pf :: TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                        TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                    TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              GHC.Prim.Any
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          GHC.Prim.Any
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: (TestEq.C
                                                                                      GHC.Prim.Any
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)
                                                                                    TestEq.:+: TestEq.C
                                                                                                 GHC.Prim.Any
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)))))))
LclId
[]
lvl_s1Pf =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String))
    @ (TestEq.C GHC.Prim.Any TestEq.U
       TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                   TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     GHC.Prim.Any
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: TestEq.C
                                                                                GHC.Prim.Any
                                                                                (TestEq.Rec
                                                                                   TestEq.Logic
                                                                                 TestEq.:*: TestEq.Rec
                                                                                              TestEq.Logic)))))))
    lvl_s1Pg

TestEq.t2 :: GHC.Bool.Bool
LclIdX
[Str: DmdType]
TestEq.t2 = eq'_aEx lvl_s1Pf lvl_s1Pf




==================== Tidy Core ====================
TestEq.eq' :: forall a_afv.
              (TestEq.Eq a_afv) =>
              a_afv -> a_afv -> GHC.Bool.Bool
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.eq' =
  \ (@ a_afv) (tpl_B1 [ALWAYS Once Nothing] :: TestEq.Eq a_afv) ->
    tpl_B1
    `cast` (TestEq.NTCo:T:Eq a_afv
            :: TestEq.T:Eq a_afv ~ (a_afv -> a_afv -> GHC.Bool.Bool))

TestEq.to :: forall a_afx.
             (TestEq.Representable a_afx) =>
             TestEq.Rep a_afx -> a_afx
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(SA)]
TestEq.to =
  \ (@ a_afx)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Representable a_afx) ->
    case tpl_B1
    of _ { TestEq.D:Representable tpl_B2 [ALWAYS Once Nothing] _ ->
    tpl_B2
    }

TestEq.from :: forall a_afx.
               (TestEq.Representable a_afx) =>
               a_afx -> TestEq.Rep a_afx
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(AS)]
TestEq.from =
  \ (@ a_afx)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Representable a_afx) ->
    case tpl_B1
    of _ { TestEq.D:Representable _ tpl_B3 [ALWAYS Once Nothing] ->
    tpl_B3
    }

TestEq.conName :: forall c_afy.
                  (TestEq.Constructor c_afy) =>
                  forall (t_afz :: * -> * -> *) a_afA.
                  t_afz c_afy a_afA -> GHC.Base.String
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(SAA)]
TestEq.conName =
  \ (@ c_afy)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Constructor c_afy) ->
    case tpl_B1
    of _ { TestEq.D:Constructor tpl_B2 [ALWAYS Once Nothing] _ _ ->
    tpl_B2
    }

TestEq.conFixity :: forall c_afy.
                    (TestEq.Constructor c_afy) =>
                    forall (t_afB :: * -> * -> *) a_afC.
                    t_afB c_afy a_afC -> TestEq.Fixity
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(ASA)]
TestEq.conFixity =
  \ (@ c_afy)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Constructor c_afy) ->
    case tpl_B1
    of _ { TestEq.D:Constructor _ tpl_B3 [ALWAYS Once Nothing] _ ->
    tpl_B3
    }

TestEq.conIsRecord :: forall c_afy.
                      (TestEq.Constructor c_afy) =>
                      forall (t_afD :: * -> * -> *) a_afE.
                      t_afD c_afy a_afE -> GHC.Bool.Bool
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(AAS)]
TestEq.conIsRecord =
  \ (@ c_afy)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Constructor c_afy) ->
    case tpl_B1
    of _ { TestEq.D:Constructor _ _ tpl_B4 [ALWAYS Once Nothing] ->
    tpl_B4
    }

TestEq.$wa2 :: forall b_a1zG.
               (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG
GblId
[Arity 1
 Str: DmdType L]
TestEq.$wa2 =
  \ (@ b_a1zG)
    (w_s1Ki :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG) ->
    Text.Read.Lex.lex1
      @ b_a1zG
      (let {
         lvl10_s1Ow :: Text.ParserCombinators.ReadP.P b_a1zG
         LclId
         []
         lvl10_s1Ow = w_s1Ki TestEq.U } in
       \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
         case a3_a1A6 of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
           Text.Read.Lex.Ident ds_d1cz ->
             case ds_d1cz of _ {
               [] -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
               : ds1_d1cA ds2_d1cB ->
                 case ds1_d1cA of _ { GHC.Types.C# ds3_d1cC ->
                 case ds3_d1cC of _ {
                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                   'U' ->
                     case ds2_d1cB of _ {
                       [] -> lvl10_s1Ow;
                       : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1zG
                     }
                 }
                 }
             }
         })

TestEq.$fReadU3 :: Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG
GblId
[Arity 2
 Worker TestEq.$wa2
 Str: DmdType AL]
TestEq.$fReadU3 =
  __inline_me (\ _
                 (@ b_a1zG)
                 (w1_s1Ki :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG) ->
                 TestEq.$wa2 @ b_a1zG w1_s1Ki)

TestEq.$fShowFixity1 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fShowFixity1 = GHC.Types.I# 0

TestEq.$fRead:+:2 :: GHC.Types.Int
GblId
[NoCafRefs]
TestEq.$fRead:+:2 = GHC.Types.I# 11

TestEq.$fReadC1 :: forall c_Xu8 a_Xua.
                   (GHC.Read.Read a_Xua) =>
                   Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xu8 a_Xua)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fReadC1 =
  \ (@ c_Xu8)
    (@ a_Xua)
    ($dRead_aVL :: GHC.Read.Read a_Xua)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.C c_Xu8 a_Xua)
      ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta1_X7U :: TestEq.C c_Xu8 a_Xua
                       -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC ->
          case GHC.Prim.<=# x_a1CC 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.C c_Xu8 a_Xua) @ b_a1zG eta1_X7U);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1zG
                (let {
                   lvl10_s1Oy :: a_Xua -> Text.ParserCombinators.ReadP.P b_a1zG
                   LclId
                   [Arity 1]
                   lvl10_s1Oy =
                     \ (a3_X1Pe :: a_Xua) ->
                       eta1_X7U (TestEq.C @ c_Xu8 @ a_Xua a3_X1Pe) } in
                 \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                   case a3_a1A6 of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                     Text.Read.Lex.Ident ds_d1bP ->
                       case ds_d1bP of _ {
                         [] -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         : ds1_d1bQ ds2_d1bR ->
                           case ds1_d1bQ of _ { GHC.Types.C# ds3_d1bS ->
                           case ds3_d1bS of _ {
                             __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                             'C' ->
                               case ds2_d1bR of _ {
                                 [] ->
                                   case $dRead_aVL of _ { GHC.Read.D:Read _ _ tpl3_XeI _ ->
                                   (((tpl3_XeI
                                      `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xua
                                              :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xua
                                                   ~
                                                 (Text.ParserCombinators.ReadPrec.Prec
                                                  -> Text.ParserCombinators.ReadP.ReadP a_Xua)))
                                       TestEq.$fRead:+:2)
                                    `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xua
                                            :: Text.ParserCombinators.ReadP.ReadP a_Xua
                                                 ~
                                               (forall b_a1zG.
                                                (a_Xua -> Text.ParserCombinators.ReadP.P b_a1zG)
                                                -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                     @ b_a1zG lvl10_s1Oy
                                   };
                                 : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1zG
                               }
                           }
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.C c_Xu8 a_Xua)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (TestEq.C c_Xu8 a_Xua)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.C c_Xu8 a_Xua -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xu8 a_Xua)))
      eta_B1

TestEq.$fReadC2 :: forall c_Xud a_Xuf.
                   (GHC.Read.Read a_Xuf) =>
                   Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xud a_Xuf]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadC2 =
  \ (@ c_Xud) (@ a_Xuf) ($dRead_X1aj :: GHC.Read.Read a_Xuf) ->
    GHC.Read.$dmreadList2
      @ (TestEq.C c_Xud a_Xuf)
      ((TestEq.$fReadC1 @ c_Xud @ a_Xuf $dRead_X1aj)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (TestEq.C c_Xud a_Xuf))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xud a_Xuf))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xud a_Xuf)))

TestEq.$fRead:+:1 :: forall a_Xt9 b_Xtb.
                     (GHC.Read.Read a_Xt9, GHC.Read.Read b_Xtb) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (a_Xt9 TestEq.:+: b_Xtb)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:+:1 =
  \ (@ a_Xt9)
    (@ b_Xtb)
    ($dRead_aYo :: GHC.Read.Read a_Xt9)
    ($dRead1_aYp :: GHC.Read.Read b_Xtb) ->
    GHC.Read.$fRead()5
      @ (a_Xt9 TestEq.:+: b_Xtb)
      ((Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
          @ (a_Xt9 TestEq.:+: b_Xtb)
          ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b1_a1zG)
              (eta_B1 :: (a_Xt9 TestEq.:+: b_Xtb)
                         -> Text.ParserCombinators.ReadP.P b1_a1zG) ->
              case c_a1Cz of _ { GHC.Types.I# x_a1CC ->
              case GHC.Prim.<=# x_a1CC 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xt9 TestEq.:+: b_Xtb) @ b1_a1zG eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b1_a1zG
                    (let {
                       lvl10_s1OA :: a_Xt9 -> Text.ParserCombinators.ReadP.P b1_a1zG
                       LclId
                       [Arity 1]
                       lvl10_s1OA =
                         \ (a3_X1Oc :: a_Xt9) ->
                           eta_B1 (TestEq.L @ a_Xt9 @ b_Xtb a3_X1Oc) } in
                     \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                       case a3_a1A6 of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                         Text.Read.Lex.Ident ds_d1ca ->
                           case ds_d1ca of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                             : ds1_d1cb ds2_d1cc ->
                               case ds1_d1cb of _ { GHC.Types.C# ds3_d1cd ->
                               case ds3_d1cd of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                                 'L' ->
                                   case ds2_d1cc of _ {
                                     [] ->
                                       case $dRead_aYo of _ { GHC.Read.D:Read _ _ tpl3_XdG _ ->
                                       (((tpl3_XdG
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    a_Xt9
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xt9
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP a_Xt9)))
                                           TestEq.$fRead:+:2)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xt9
                                                :: Text.ParserCombinators.ReadP.ReadP a_Xt9
                                                     ~
                                                   (forall b_a1zG.
                                                    (a_Xt9 -> Text.ParserCombinators.ReadP.P b_a1zG)
                                                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                         @ b1_a1zG lvl10_s1OA
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xt9 TestEq.:+: b_Xtb)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xt9 TestEq.:+: b_Xtb)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1zG.
                          ((a_Xt9 TestEq.:+: b_Xtb) -> Text.ParserCombinators.ReadP.P b_a1zG)
                          -> Text.ParserCombinators.ReadP.P b_a1zG)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec (a_Xt9 TestEq.:+: b_Xtb)))
          ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b1_a1zG)
              (eta_B1 :: (a_Xt9 TestEq.:+: b_Xtb)
                         -> Text.ParserCombinators.ReadP.P b1_a1zG) ->
              case c_a1Cz of _ { GHC.Types.I# x_a1CC ->
              case GHC.Prim.<=# x_a1CC 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xt9 TestEq.:+: b_Xtb) @ b1_a1zG eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b1_a1zG
                    (let {
                       lvl10_s1OC :: b_Xtb -> Text.ParserCombinators.ReadP.P b1_a1zG
                       LclId
                       [Arity 1]
                       lvl10_s1OC =
                         \ (a3_X1Oc :: b_Xtb) ->
                           eta_B1 (TestEq.R @ a_Xt9 @ b_Xtb a3_X1Oc) } in
                     \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                       case a3_a1A6 of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                         Text.Read.Lex.Ident ds_d1cl ->
                           case ds_d1cl of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                             : ds1_d1cm ds2_d1cn ->
                               case ds1_d1cm of _ { GHC.Types.C# ds3_d1co ->
                               case ds3_d1co of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                                 'R' ->
                                   case ds2_d1cn of _ {
                                     [] ->
                                       case $dRead1_aYp of _ { GHC.Read.D:Read _ _ tpl3_XdG _ ->
                                       (((tpl3_XdG
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    b_Xtb
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec b_Xtb
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP b_Xtb)))
                                           TestEq.$fRead:+:2)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_Xtb
                                                :: Text.ParserCombinators.ReadP.ReadP b_Xtb
                                                     ~
                                                   (forall b_a1zG.
                                                    (b_Xtb -> Text.ParserCombinators.ReadP.P b_a1zG)
                                                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                         @ b1_a1zG lvl10_s1OC
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xt9 TestEq.:+: b_Xtb)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xt9 TestEq.:+: b_Xtb)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1zG.
                          ((a_Xt9 TestEq.:+: b_Xtb) -> Text.ParserCombinators.ReadP.P b_a1zG)
                          -> Text.ParserCombinators.ReadP.P b_a1zG)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec
                        (a_Xt9 TestEq.:+: b_Xtb))))
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xt9 TestEq.:+: b_Xtb))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xt9 TestEq.:+: b_Xtb))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xt9 TestEq.:+: b_Xtb)))

TestEq.$fRead:+:3 :: forall a_Xtf b_Xth.
                     (GHC.Read.Read a_Xtf, GHC.Read.Read b_Xth) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [a_Xtf TestEq.:+: b_Xth]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:+:3 =
  \ (@ a_Xtf)
    (@ b_Xth)
    ($dRead_X1bU :: GHC.Read.Read a_Xtf)
    ($dRead1_X1bW :: GHC.Read.Read b_Xth) ->
    GHC.Read.$dmreadList2
      @ (a_Xtf TestEq.:+: b_Xth)
      ((TestEq.$fRead:+:1 @ a_Xtf @ b_Xth $dRead_X1bU $dRead1_X1bW)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xtf TestEq.:+: b_Xth))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xtf TestEq.:+: b_Xth))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtf TestEq.:+: b_Xth)))

TestEq.$fReadU2 :: Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP TestEq.U
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadU2 =
  GHC.Read.$fRead()5
    @ TestEq.U
    (TestEq.$fReadU3
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

TestEq.$fReadU1 :: Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP [TestEq.U]
GblId
[Str: DmdType]
TestEq.$fReadU1 =
  GHC.Read.$dmreadList2
    @ TestEq.U
    (TestEq.$fReadU2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

TestEq.$fReadU4 :: Text.ParserCombinators.ReadP.P [TestEq.U]
GblId
[Str: DmdType]
TestEq.$fReadU4 =
  ((TestEq.$fReadU1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.U]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.U]
                ~
              (forall b_a1zG.
               ([TestEq.U] -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG)))
    @ [TestEq.U]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.U])

lvl_r1Vw :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl_r1Vw = GHC.Base.unpackCString# "Bin "

TestEq.$fConstructorLeaf1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLeaf1 = GHC.Base.unpackCString# "Leaf"

lvl1_r1Vz :: GHC.Show.ShowS
GblId
[Arity 1
 Str: DmdType L]
lvl1_r1Vz = GHC.Base.++ @ GHC.Types.Char TestEq.$fConstructorLeaf1

lvl2_r1VB :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl2_r1VB = GHC.Base.unpackCString# "Disj "

lvl3_r1VD :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl3_r1VD = GHC.Base.unpackCString# "Conj "

lvl4_r1VF :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl4_r1VF = GHC.Base.unpackCString# "Equiv "

lvl5_r1VH :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl5_r1VH = GHC.Base.unpackCString# "Impl "

lvl6_r1VJ :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl6_r1VJ = GHC.Base.unpackCString# "Not "

TestEq.$fConstructorLogic_F_2 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_F_2 = GHC.Types.C# 'F'

TestEq.$fConstructorLogic_F_1 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorLogic_F_1 =
  GHC.Types.:
    @ GHC.Types.Char
    TestEq.$fConstructorLogic_F_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl7_r1VN :: GHC.Show.ShowS
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
lvl7_r1VN =
  GHC.Base.++ @ GHC.Types.Char TestEq.$fConstructorLogic_F_1

TestEq.$fConstructorLogic_T_2 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_T_2 = GHC.Types.C# 'T'

TestEq.$fConstructorLogic_T_1 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorLogic_T_1 =
  GHC.Types.:
    @ GHC.Types.Char
    TestEq.$fConstructorLogic_T_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl8_r1VR :: GHC.Show.ShowS
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
lvl8_r1VR =
  GHC.Base.++ @ GHC.Types.Char TestEq.$fConstructorLogic_T_1

lvl9_r1VT :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl9_r1VT = GHC.Base.unpackCString# "VarL "

TestEq.$fShowFixity2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowFixity2 = GHC.Base.unpackCString# "Infix "

TestEq.$fReadFixity7 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadFixity7 = GHC.Base.unpackCString# "Prefix"

TestEq.$fReadAssociativity9 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

TestEq.$fReadAssociativity12 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

TestEq.$fReadAssociativity15 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

TestEq.$fReadFixity5 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadFixity5 = GHC.Base.unpackCString# "Infix"

TestEq.$wa1 :: forall b_a1zG.
               (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG
GblId
[Arity 1
 Str: DmdType L]
TestEq.$wa1 =
  \ (@ b_a1zG)
    (w_s1Ls :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1zG) ->
    Text.Read.Lex.lex1
      @ b_a1zG
      (let {
         lvl10_s1OD :: Text.ParserCombinators.ReadP.P b_a1zG
         LclId
         []
         lvl10_s1OD = w_s1Ls TestEq.Prefix } in
       \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
         case a3_a1A6 of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
           Text.Read.Lex.Ident ds_d1bp ->
             case GHC.Base.eqString ds_d1bp TestEq.$fReadFixity7 of _ {
               GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
               GHC.Bool.True -> lvl10_s1OD
             }
         })

TestEq.$fReadFixity6 :: Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1zG.
                           (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                           -> Text.ParserCombinators.ReadP.P b_a1zG
GblId
[Arity 2
 Worker TestEq.$wa1
 Str: DmdType AL]
TestEq.$fReadFixity6 =
  __inline_me (\ _
                 (@ b_a1zG)
                 (w1_s1Ls :: TestEq.Fixity
                             -> Text.ParserCombinators.ReadP.P b_a1zG) ->
                 TestEq.$wa1 @ b_a1zG w1_s1Ls)

TestEq.$fShowRec1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowRec1 = GHC.Base.unpackCString# "Rec "

TestEq.$wshowsPrec3 :: forall a_afF.
                       (GHC.Show.Show a_afF) =>
                       GHC.Prim.Int# -> a_afF -> GHC.Base.String -> [GHC.Types.Char]
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$wshowsPrec3 =
  \ (@ a_afF)
    (w_s1Lf :: GHC.Show.Show a_afF)
    (ww_s1Li :: GHC.Prim.Int#)
    (ww1_s1Lm :: a_afF) ->
    let {
      g_s1M1 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1M1 =
        case w_s1Lf of _ { GHC.Show.D:Show tpl1_XfH _ _ ->
        tpl1_XfH TestEq.$fRead:+:2 ww1_s1Lm
        } } in
    case GHC.Prim.>=# ww_s1Li 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char TestEq.$fShowRec1 (g_s1M1 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               TestEq.$fShowRec1
               (g_s1M1
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

TestEq.$fReadRec2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadRec2 = GHC.Base.unpackCString# "Rec"

TestEq.$fReadRec1 :: forall a_XuW.
                     (GHC.Read.Read a_XuW) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_XuW)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fReadRec1 =
  \ (@ a_XuW)
    ($dRead_aTD :: GHC.Read.Read a_XuW)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Rec a_XuW)
      ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta1_X8f :: TestEq.Rec a_XuW
                       -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC ->
          case GHC.Prim.<=# x_a1CC 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Rec a_XuW) @ b_a1zG eta1_X8f);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1zG
                (let {
                   lvl10_s1OF :: a_XuW -> Text.ParserCombinators.ReadP.P b_a1zG
                   LclId
                   [Arity 1]
                   lvl10_s1OF =
                     \ (a3_X1PS :: a_XuW) -> eta1_X8f (TestEq.Rec @ a_XuW a3_X1PS) } in
                 \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                   case a3_a1A6 of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                     Text.Read.Lex.Ident ds_d1bB ->
                       case GHC.Base.eqString ds_d1bB TestEq.$fReadRec2 of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         GHC.Bool.True ->
                           case $dRead_aTD of _ { GHC.Read.D:Read _ _ tpl3_Xfx _ ->
                           (((tpl3_Xfx
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XuW
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_XuW
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_XuW)))
                               TestEq.$fRead:+:2)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XuW
                                    :: Text.ParserCombinators.ReadP.ReadP a_XuW
                                         ~
                                       (forall b_a1zG.
                                        (a_XuW -> Text.ParserCombinators.ReadP.P b_a1zG)
                                        -> Text.ParserCombinators.ReadP.P b_a1zG)))
                             @ b_a1zG lvl10_s1OF
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Rec a_XuW)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_XuW)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.Rec a_XuW -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_XuW)))
      eta_B1

TestEq.$fReadRec3 :: forall a_Xv0.
                     (GHC.Read.Read a_Xv0) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xv0]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadRec3 =
  \ (@ a_Xv0) ($dRead_X18Z :: GHC.Read.Read a_Xv0) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Rec a_Xv0)
      ((TestEq.$fReadRec1 @ a_Xv0 $dRead_X18Z)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xv0))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xv0))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xv0)))

TestEq.$fShowVar1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowVar1 = GHC.Base.unpackCString# "Var "

TestEq.$wshowsPrec4 :: forall a_afG.
                       (GHC.Show.Show a_afG) =>
                       GHC.Prim.Int# -> a_afG -> GHC.Base.String -> [GHC.Types.Char]
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$wshowsPrec4 =
  \ (@ a_afG)
    (w_s1L2 :: GHC.Show.Show a_afG)
    (ww_s1L5 :: GHC.Prim.Int#)
    (ww1_s1L9 :: a_afG) ->
    let {
      g_s1M5 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1M5 =
        case w_s1L2 of _ { GHC.Show.D:Show tpl1_Xfi _ _ ->
        tpl1_Xfi TestEq.$fRead:+:2 ww1_s1L9
        } } in
    case GHC.Prim.>=# ww_s1L5 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char TestEq.$fShowVar1 (g_s1M5 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               TestEq.$fShowVar1
               (g_s1M5
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

TestEq.$fReadVar2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadVar2 = GHC.Base.unpackCString# "Var"

TestEq.$fReadVar1 :: forall a_Xuy.
                     (GHC.Read.Read a_Xuy) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_Xuy)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fReadVar1 =
  \ (@ a_Xuy)
    ($dRead_aUH :: GHC.Read.Read a_Xuy)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Var a_Xuy)
      ((\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1zG)
          (eta1_X84 :: TestEq.Var a_Xuy
                       -> Text.ParserCombinators.ReadP.P b_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC ->
          case GHC.Prim.<=# x_a1CC 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Var a_Xuy) @ b_a1zG eta1_X84);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1zG
                (let {
                   lvl10_s1OH :: a_Xuy -> Text.ParserCombinators.ReadP.P b_a1zG
                   LclId
                   [Arity 1]
                   lvl10_s1OH =
                     \ (a3_X1Pt :: a_Xuy) -> eta1_X84 (TestEq.Var @ a_Xuy a3_X1Pt) } in
                 \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
                   case a3_a1A6 of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                     Text.Read.Lex.Ident ds_d1bI ->
                       case GHC.Base.eqString ds_d1bI TestEq.$fReadVar2 of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                         GHC.Bool.True ->
                           case $dRead_aUH of _ { GHC.Read.D:Read _ _ tpl3_Xf8 _ ->
                           (((tpl3_Xf8
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xuy
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xuy
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_Xuy)))
                               TestEq.$fRead:+:2)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xuy
                                    :: Text.ParserCombinators.ReadP.ReadP a_Xuy
                                         ~
                                       (forall b_a1zG.
                                        (a_Xuy -> Text.ParserCombinators.ReadP.P b_a1zG)
                                        -> Text.ParserCombinators.ReadP.P b_a1zG)))
                             @ b_a1zG lvl10_s1OH
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Var a_Xuy)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_Xuy)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      (TestEq.Var a_Xuy -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_Xuy)))
      eta_B1

TestEq.$fReadVar3 :: forall a_XuC.
                     (GHC.Read.Read a_XuC) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuC]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadVar3 =
  \ (@ a_XuC) ($dRead_X19E :: GHC.Read.Read a_XuC) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Var a_XuC)
      ((TestEq.$fReadVar1 @ a_XuC $dRead_X19E)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuC))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuC))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuC)))

TestEq.$fShowC1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowC1 = GHC.Base.unpackCString# "C "

TestEq.$wshowsPrec2 :: forall c_afH a_afI.
                       (GHC.Show.Show a_afI) =>
                       GHC.Prim.Int# -> a_afI -> GHC.Base.String -> [GHC.Types.Char]
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$wshowsPrec2 =
  \ (@ c_afH)
    (@ a_afI)
    (w_s1KP :: GHC.Show.Show a_afI)
    (ww_s1KS :: GHC.Prim.Int#)
    (ww1_s1KW :: a_afI) ->
    let {
      g_s1M9 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1M9 =
        case w_s1KP of _ { GHC.Show.D:Show tpl1_XeU _ _ ->
        tpl1_XeU TestEq.$fRead:+:2 ww1_s1KW
        } } in
    case GHC.Prim.>=# ww_s1KS 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char TestEq.$fShowC1 (g_s1M9 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               TestEq.$fShowC1
               (g_s1M9
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

TestEq.$fRead:*:3 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fRead:*:3 = GHC.Types.I# 7

TestEq.$fShow:*:1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShow:*:1 = GHC.Base.unpackCString# " :*: "

TestEq.$wshowsPrec :: forall a_afJ b_afK.
                      (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                      GHC.Prim.Int#
                      -> a_afJ
                      -> b_afK
                      -> GHC.Base.String
                      -> GHC.Base.String
GblId
[Arity 5
 Str: DmdType LLLLL]
TestEq.$wshowsPrec =
  \ (@ a_afJ)
    (@ b_afK)
    (w_s1KA :: GHC.Show.Show a_afJ)
    (w1_s1KB :: GHC.Show.Show b_afK)
    (ww_s1KE :: GHC.Prim.Int#)
    (ww1_s1KI :: a_afJ)
    (ww2_s1KJ :: b_afK) ->
    let {
      g_s1Md [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1Md =
        case w1_s1KB of _ { GHC.Show.D:Show tpl1_Xew _ _ ->
        tpl1_Xew TestEq.$fRead:*:3 ww2_s1KJ
        } } in
    let {
      f_s1Mb [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      f_s1Mb =
        case w_s1KA of _ { GHC.Show.D:Show tpl1_Xes _ _ ->
        tpl1_Xes TestEq.$fRead:*:3 ww1_s1KI
        } } in
    case GHC.Prim.>=# ww_s1KE 7 of _ {
      GHC.Bool.False ->
        \ (x_X1vO :: GHC.Base.String) ->
          f_s1Mb
            (GHC.Base.++ @ GHC.Types.Char TestEq.$fShow:*:1 (g_s1Md x_X1vO));
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (f_s1Mb
               (GHC.Base.++
                  @ GHC.Types.Char
                  TestEq.$fShow:*:1
                  (g_s1Md
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
    }

TestEq.$fRead:*:2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fRead:*:2 = GHC.Base.unpackCString# ":*:"

TestEq.$fRead:*:1 :: forall a_XtE b_XtG.
                     (GHC.Read.Read a_XtE, GHC.Read.Read b_XtG) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (a_XtE TestEq.:*: b_XtG)
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$fRead:*:1 =
  \ (@ a_XtE)
    (@ b_XtG)
    ($dRead_aWX :: GHC.Read.Read a_XtE)
    ($dRead1_aWY :: GHC.Read.Read b_XtG)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (a_XtE TestEq.:*: b_XtG)
      (let {
         ds1_s1Mg [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadP
                                       a_XtE
         LclId
         [Str: DmdType]
         ds1_s1Mg =
           case $dRead_aWX of _ { GHC.Read.D:Read _ _ tpl3_Xea _ ->
           (tpl3_Xea
            `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XtE
                    :: Text.ParserCombinators.ReadPrec.ReadPrec a_XtE
                         ~
                       (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP a_XtE)))
             TestEq.$fRead:*:3
           } } in
       (\ (c_a1Cz :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b1_a1zG)
          (eta1_X5T :: (a_XtE TestEq.:*: b_XtG)
                       -> Text.ParserCombinators.ReadP.P b1_a1zG) ->
          case c_a1Cz of _ { GHC.Types.I# x_a1CC ->
          case GHC.Prim.<=# x_a1CC 6 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (a_XtE TestEq.:*: b_XtG) @ b1_a1zG eta1_X5T);
            GHC.Bool.True ->
              (ds1_s1Mg
               `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XtE
                       :: Text.ParserCombinators.ReadP.ReadP a_XtE
                            ~
                          (forall b_a1zG.
                           (a_XtE -> Text.ParserCombinators.ReadP.P b_a1zG)
                           -> Text.ParserCombinators.ReadP.P b_a1zG)))
                @ b1_a1zG
                (\ (a3_a1A6 :: a_XtE) ->
                   Text.Read.Lex.lex1
                     @ b1_a1zG
                     (let {
                        lvl10_s1OL :: b_XtG -> Text.ParserCombinators.ReadP.P b1_a1zG
                        LclId
                        [Arity 1]
                        lvl10_s1OL =
                          \ (a31_X1OG :: b_XtG) ->
                            eta1_X5T (TestEq.:*: @ a_XtE @ b_XtG a3_a1A6 a31_X1OG) } in
                      \ (a31_X1Jo :: Text.Read.Lex.Lexeme) ->
                        case a31_X1Jo of _ {
                          __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                          Text.Read.Lex.Symbol ds_d1c2 ->
                            case GHC.Base.eqString ds_d1c2 TestEq.$fRead:*:2 of _ {
                              GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b1_a1zG;
                              GHC.Bool.True ->
                                case $dRead1_aWY of _ { GHC.Read.D:Read _ _ tpl3_Xef _ ->
                                (((tpl3_Xef
                                   `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec b_XtG
                                           :: Text.ParserCombinators.ReadPrec.ReadPrec b_XtG
                                                ~
                                              (Text.ParserCombinators.ReadPrec.Prec
                                               -> Text.ParserCombinators.ReadP.ReadP b_XtG)))
                                    TestEq.$fRead:*:3)
                                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_XtG
                                         :: Text.ParserCombinators.ReadP.ReadP b_XtG
                                              ~
                                            (forall b_a1zG.
                                             (b_XtG -> Text.ParserCombinators.ReadP.P b_a1zG)
                                             -> Text.ParserCombinators.ReadP.P b_a1zG)))
                                  @ b1_a1zG lvl10_s1OL
                                }
                            }
                        }))
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (a_XtE TestEq.:*: b_XtG)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (a_XtE TestEq.:*: b_XtG)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1zG.
                      ((a_XtE TestEq.:*: b_XtG) -> Text.ParserCombinators.ReadP.P b_a1zG)
                      -> Text.ParserCombinators.ReadP.P b_a1zG)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_XtE TestEq.:*: b_XtG)))
      eta_B1

TestEq.$fRead:*:4 :: forall a_XtK b_XtM.
                     (GHC.Read.Read a_XtK, GHC.Read.Read b_XtM) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [a_XtK TestEq.:*: b_XtM]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:*:4 =
  \ (@ a_XtK)
    (@ b_XtM)
    ($dRead_X1b0 :: GHC.Read.Read a_XtK)
    ($dRead1_X1b2 :: GHC.Read.Read b_XtM) ->
    GHC.Read.$dmreadList2
      @ (a_XtK TestEq.:*: b_XtM)
      ((TestEq.$fRead:*:1 @ a_XtK @ b_XtM $dRead_X1b0 $dRead1_X1b2)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_XtK TestEq.:*: b_XtM))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_XtK TestEq.:*: b_XtM))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_XtK TestEq.:*: b_XtM)))

TestEq.$fShow:+:1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShow:+:1 = GHC.Base.unpackCString# "R "

TestEq.$fShow:+:2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShow:+:2 = GHC.Base.unpackCString# "L "

TestEq.$wshowsPrec1 :: forall a_afL b_afM.
                       (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                       GHC.Prim.Int#
                       -> (a_afL TestEq.:+: b_afM)
                       -> GHC.Base.String
                       -> [GHC.Types.Char]
GblId
[Arity 4
 Str: DmdType LLLS]
TestEq.$wshowsPrec1 =
  \ (@ a_afL)
    (@ b_afM)
    (w_s1Ko :: GHC.Show.Show a_afL)
    (w1_s1Kp :: GHC.Show.Show b_afM)
    (ww_s1Ks :: GHC.Prim.Int#)
    (w2_s1Ku :: a_afL TestEq.:+: b_afM) ->
    case w2_s1Ku of _ {
      TestEq.L b1_ayF ->
        let {
          g_s1Ml [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ml =
            case w_s1Ko of _ { GHC.Show.D:Show tpl1_XdY _ _ ->
            tpl1_XdY TestEq.$fRead:+:2 b1_ayF
            } } in
        case GHC.Prim.>=# ww_s1Ks 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char TestEq.$fShow:+:2 (g_s1Ml x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   TestEq.$fShow:+:2
                   (g_s1Ml
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.R b1_ayH ->
        let {
          g_s1Mn [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Mn =
            case w1_s1Kp of _ { GHC.Show.D:Show tpl1_XdY _ _ ->
            tpl1_XdY TestEq.$fRead:+:2 b1_ayH
            } } in
        case GHC.Prim.>=# ww_s1Ks 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char TestEq.$fShow:+:1 (g_s1Mn x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   TestEq.$fShow:+:1
                   (g_s1Mn
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        }
    }

TestEq.$fShowU2 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fShowU2 = GHC.Types.C# 'U'

TestEq.$fShowU1 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fShowU1 =
  GHC.Types.:
    @ GHC.Types.Char TestEq.$fShowU2 (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fRepresentable[]2 :: TestEq.C TestEq.List_Nil_ TestEq.U
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentable[]2 =
  TestEq.C @ TestEq.List_Nil_ @ TestEq.U TestEq.U

TestEq.$fRepresentable[]1 :: forall a_agN.
                             TestEq.C TestEq.List_Nil_ TestEq.U
                             TestEq.:+: TestEq.C
                                          TestEq.List_Cons_
                                          (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fRepresentable[]1 =
  \ (@ a_agN) ->
    TestEq.L
      @ (TestEq.C TestEq.List_Nil_ TestEq.U)
      @ (TestEq.C
           TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
      TestEq.$fRepresentable[]2

TestEq.$fConstructorList_Nil_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorList_Nil_1 = GHC.Base.unpackCString# "[]"

TestEq.$fConstructorBin1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorBin1 = GHC.Base.unpackCString# "Bin"

TestEq.$fConstructorLogic_Var_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Var_1 = GHC.Base.unpackCString# "VarL"

TestEq.$fConstructorLogic_Impl_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Impl_1 = GHC.Base.unpackCString# "Impl"

TestEq.$fConstructorLogic_Equiv_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Equiv_1 = GHC.Base.unpackCString# "Equiv"

TestEq.$fConstructorLogic_Conj_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Conj_1 = GHC.Base.unpackCString# "Conj"

TestEq.$fConstructorLogic_Disj_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Disj_1 = GHC.Base.unpackCString# "Disj"

TestEq.$fConstructorLogic_Not_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Not_1 = GHC.Base.unpackCString# "Not"

TestEq.$fConstructorList_Cons_2 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorList_Cons_2 = GHC.Types.I# 5

TestEq.$fConstructorList_Cons_1 :: TestEq.Fixity
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorList_Cons_1 =
  TestEq.Infix
    TestEq.RightAssociative TestEq.$fConstructorList_Cons_2

TestEq.$fConstructorList_Cons_4 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorList_Cons_4 = GHC.Types.C# ':'

TestEq.$fConstructorList_Cons_3 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorList_Cons_3 =
  GHC.Types.:
    @ GHC.Types.Char
    TestEq.$fConstructorList_Cons_4
    (GHC.Types.[] @ GHC.Types.Char)

TestEq.$dmconIsRecord_$s$dmconIsRecord11 :: forall (t_afD :: *
                                                             -> *
                                                             -> *)
                                                   a_afE.
                                            t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord11 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord10 :: forall (t_afD :: *
                                                             -> *
                                                             -> *)
                                                   a_afE.
                                            t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord10 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord9 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord9 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord8 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord8 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord7 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord7 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord6 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord6 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord5 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord5 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord4 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord4 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord3 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord3 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord2 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord2 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord1 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord1 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord :: forall (t_afD :: *
                                                           -> *
                                                           -> *)
                                                 a_afE.
                                          t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconFixity_$s$dmconFixity10 :: forall (t_afB :: *
                                                         -> *
                                                         -> *)
                                               a_afC.
                                        t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity10 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity9 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity9 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity8 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity8 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity7 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity7 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity6 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity6 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity5 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity5 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity4 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity4 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity3 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity3 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity2 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity2 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity1 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity1 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity :: forall (t_afB :: * -> * -> *)
                                             a_afC.
                                      t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$fOrdAssociativity1 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> TestEq.Associativity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity1 =
  \ (x_a1kz :: TestEq.Associativity)
    (y_a1kA :: TestEq.Associativity) ->
    case x_a1kz of _ {
      TestEq.LeftAssociative ->
        case y_a1kA of _ { __DEFAULT -> TestEq.LeftAssociative };
      TestEq.RightAssociative ->
        case y_a1kA of _ {
          TestEq.LeftAssociative -> TestEq.LeftAssociative;
          TestEq.RightAssociative -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.RightAssociative
        };
      TestEq.NotAssociative -> y_a1kA
    }

TestEq.$fOrdAssociativity2 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> TestEq.Associativity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity2 =
  \ (x_a1kf :: TestEq.Associativity)
    (y_a1kg :: TestEq.Associativity) ->
    case x_a1kf of _ {
      TestEq.LeftAssociative -> y_a1kg;
      TestEq.RightAssociative ->
        case y_a1kg of _ {
          __DEFAULT -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.NotAssociative
        };
      TestEq.NotAssociative ->
        case y_a1kg of _ { __DEFAULT -> TestEq.NotAssociative }
    }

TestEq.$fOrdAssociativity3 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity3 =
  \ (x_a1jY :: TestEq.Associativity)
    (y_a1jZ :: TestEq.Associativity) ->
    case x_a1jY of _ {
      TestEq.LeftAssociative ->
        case y_a1jZ of _ { __DEFAULT -> GHC.Bool.True };
      TestEq.RightAssociative ->
        case y_a1jZ of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1jZ of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

TestEq.$fOrdAssociativity4 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity4 =
  \ (x_a1jH :: TestEq.Associativity)
    (y_a1jI :: TestEq.Associativity) ->
    case x_a1jH of _ {
      TestEq.LeftAssociative ->
        case y_a1jI of _ { __DEFAULT -> GHC.Bool.False };
      TestEq.RightAssociative ->
        case y_a1jI of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jI of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

TestEq.$fOrdAssociativity5 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity5 =
  \ (x_a1jq :: TestEq.Associativity)
    (y_a1jr :: TestEq.Associativity) ->
    case x_a1jq of _ {
      TestEq.LeftAssociative ->
        case y_a1jr of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case y_a1jr of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jr of _ { __DEFAULT -> GHC.Bool.True }
    }

TestEq.$fOrdAssociativity6 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity6 =
  \ (x_a1j9 :: TestEq.Associativity)
    (y_a1ja :: TestEq.Associativity) ->
    case x_a1j9 of _ {
      TestEq.LeftAssociative ->
        case y_a1ja of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case y_a1ja of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1ja of _ { __DEFAULT -> GHC.Bool.False }
    }

TestEq.$fShowAssociativity1 :: TestEq.Associativity
                               -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType S]
TestEq.$fShowAssociativity1 =
  \ (x_X1xm :: TestEq.Associativity) ->
    case x_X1xm of _ {
      TestEq.LeftAssociative -> TestEq.$fReadAssociativity15;
      TestEq.RightAssociative -> TestEq.$fReadAssociativity12;
      TestEq.NotAssociative -> TestEq.$fReadAssociativity9
    }

TestEq.$fReadAssociativity14 :: Text.ParserCombinators.ReadPrec.Prec
                                -> forall b_X1Pt.
                                   (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1Pt)
                                   -> Text.ParserCombinators.ReadP.P b_X1Pt
GblId
[Arity 2
 NoCafRefs]
TestEq.$fReadAssociativity14 =
  \ _
    (@ b_X1Pt)
    (eta_X1Pv :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1Pt) ->
    eta_X1Pv TestEq.LeftAssociative

TestEq.$fReadAssociativity13 :: ([GHC.Types.Char],
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
GblId
[]
TestEq.$fReadAssociativity13 =
  (TestEq.$fReadAssociativity15,
   TestEq.$fReadAssociativity14
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1zG.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                  -> Text.ParserCombinators.ReadP.P b_a1zG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity11 :: Text.ParserCombinators.ReadPrec.Prec
                                -> forall b_X1PA.
                                   (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1PA)
                                   -> Text.ParserCombinators.ReadP.P b_X1PA
GblId
[Arity 2
 NoCafRefs]
TestEq.$fReadAssociativity11 =
  \ _
    (@ b_X1PA)
    (eta_X1PC :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1PA) ->
    eta_X1PC TestEq.RightAssociative

TestEq.$fReadAssociativity10 :: ([GHC.Types.Char],
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
GblId
[]
TestEq.$fReadAssociativity10 =
  (TestEq.$fReadAssociativity12,
   TestEq.$fReadAssociativity11
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1zG.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                  -> Text.ParserCombinators.ReadP.P b_a1zG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity8 :: Text.ParserCombinators.ReadPrec.Prec
                               -> forall b_X1PH.
                                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1PH)
                                  -> Text.ParserCombinators.ReadP.P b_X1PH
GblId
[Arity 2
 NoCafRefs]
TestEq.$fReadAssociativity8 =
  \ _
    (@ b_X1PH)
    (eta_X1PJ :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1PH) ->
    eta_X1PJ TestEq.NotAssociative

TestEq.$fReadAssociativity7 :: ([GHC.Types.Char],
                                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
GblId
[]
TestEq.$fReadAssociativity7 =
  (TestEq.$fReadAssociativity9,
   TestEq.$fReadAssociativity8
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1zG.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                  -> Text.ParserCombinators.ReadP.P b_a1zG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity6 :: [(GHC.Base.String,
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
GblId
[]
TestEq.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    TestEq.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity5 :: [(GHC.Base.String,
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
GblId
[]
TestEq.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    TestEq.$fReadAssociativity10
    TestEq.$fReadAssociativity6

TestEq.$fReadAssociativity4 :: [(GHC.Base.String,
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
GblId
[]
TestEq.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    TestEq.$fReadAssociativity13
    TestEq.$fReadAssociativity5

TestEq.$fReadAssociativity3 :: Text.ParserCombinators.ReadPrec.ReadPrec
                                 TestEq.Associativity
GblId
[Str: DmdType]
TestEq.$fReadAssociativity3 =
  GHC.Read.choose1 @ TestEq.Associativity TestEq.$fReadAssociativity4

TestEq.$fReadAssociativity17 :: GHC.Types.Int
                                -> Text.ParserCombinators.ReadP.ReadS TestEq.Associativity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadAssociativity17 =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Associativity
      (((GHC.Read.$fRead()5
           @ TestEq.Associativity TestEq.$fReadAssociativity3 eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  TestEq.Associativity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                     ~
                   (forall b_a1zG.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ TestEq.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ TestEq.Associativity))

TestEq.$fReadAssociativity2 :: Text.ParserCombinators.ReadPrec.Prec
                               -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadAssociativity2 =
  GHC.Read.$fRead()5
    @ TestEq.Associativity TestEq.$fReadAssociativity3

TestEq.$fReadAssociativity1 :: Text.ParserCombinators.ReadPrec.Prec
                               -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ TestEq.Associativity
    (TestEq.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity16 :: Text.ParserCombinators.ReadP.P
                                  [TestEq.Associativity]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity16 =
  ((TestEq.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [TestEq.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
                ~
              (forall b_a1zG.
               ([TestEq.Associativity] -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG)))
    @ [TestEq.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_return
       @ [TestEq.Associativity])

Rec {
a7_r1Xm :: Text.ParserCombinators.ReadPrec.Prec
           -> forall b_a1Ai.
              (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1Ai)
              -> Text.ParserCombinators.ReadP.P b_a1Ai
GblId
[Arity 2
 Str: DmdType AL]
a7_r1Xm =
  __inline_me (GHC.Read.$fRead()4
                 @ TestEq.Associativity
                 (TestEq.$fReadFixity_a8
                  `cast` (trans
                            (trans
                               (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                            (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                          :: (Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                               ~
                             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)))
TestEq.$fReadFixity_a8 :: Text.ParserCombinators.ReadPrec.Prec
                          -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity_a8 =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Associativity
    TestEq.$fReadAssociativity3
    (a7_r1Xm
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym
                     (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Associativity))
               (sym
                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                     TestEq.Associativity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
end Rec }

TestEq.$wa :: GHC.Prim.Int#
              -> forall b_a1zG.
                 (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                 -> Text.ParserCombinators.ReadP.P b_a1zG
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$wa =
  \ (ww_s1Ly :: GHC.Prim.Int#)
    (@ b_a1zG)
    (w_s1LA :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1zG) ->
    case GHC.Prim.<=# ww_s1Ly 10 of _ {
      GHC.Bool.False ->
        __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                       @ TestEq.Fixity @ b_a1zG w_s1LA);
      GHC.Bool.True ->
        Text.Read.Lex.lex1
          @ b_a1zG
          (let {
             lvl10_s1OY :: Text.ParserCombinators.ReadP.P b_a1zG
             LclId
             []
             lvl10_s1OY =
               let {
                 k_s1MF :: TestEq.Associativity
                           -> Text.ParserCombinators.ReadP.P b_a1zG
                 LclId
                 [Arity 1
                  Str: DmdType L]
                 k_s1MF =
                   \ (a3_X1R4 :: TestEq.Associativity) ->
                     ((GHC.Read.$dmreadsPrec14 TestEq.$fRead:+:2)
                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                              :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                   ~
                                 (forall b_a1zG.
                                  (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_a1zG)
                                  -> Text.ParserCombinators.ReadP.P b_a1zG)))
                       @ b_a1zG
                       (\ (a31_X1KF :: GHC.Types.Int) ->
                          w_s1LA (TestEq.Infix a3_X1R4 a31_X1KF)) } in
               Text.ParserCombinators.ReadP.$fMonadPlusP_mplus
                 @ b_a1zG
                 ((((TestEq.$fReadAssociativity3
                     `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                               TestEq.Associativity
                             :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity
                                  ~
                                (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)))
                      TestEq.$fRead:+:2)
                   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                             TestEq.Associativity
                           :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                                ~
                              (forall b_a1zG.
                               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1zG)
                               -> Text.ParserCombinators.ReadP.P b_a1zG)))
                    @ b_a1zG k_s1MF)
                 (GHC.Read.$wa
                    @ TestEq.Associativity
                    (TestEq.$fReadFixity_a8
                     `cast` (trans
                               (trans
                                  (sym
                                     (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                        TestEq.Associativity))
                                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                             :: (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                                  ~
                                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                    @ b_a1zG
                    k_s1MF) } in
           \ (a3_a1A6 :: Text.Read.Lex.Lexeme) ->
             case a3_a1A6 of _ {
               __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
               Text.Read.Lex.Ident ds_d1bu ->
                 case GHC.Base.eqString ds_d1bu TestEq.$fReadFixity5 of _ {
                   GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1zG;
                   GHC.Bool.True -> lvl10_s1OY
                 }
             })
    }

TestEq.$fReadFixity4 :: Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1zG.
                           (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                           -> Text.ParserCombinators.ReadP.P b_a1zG
GblId
[Arity 2
 Worker TestEq.$wa
 Str: DmdType U(L)L]
TestEq.$fReadFixity4 =
  __inline_me (\ (w_s1Lw :: Text.ParserCombinators.ReadPrec.Prec)
                 (@ b_a1zG)
                 (w1_s1LA :: TestEq.Fixity
                             -> Text.ParserCombinators.ReadP.P b_a1zG) ->
                 case w_s1Lw of _ { GHC.Types.I# ww_s1Ly ->
                 TestEq.$wa ww_s1Ly @ b_a1zG w1_s1LA
                 })

TestEq.$fReadFixity3 :: Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity3 =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Fixity
    (TestEq.$fReadFixity6
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (TestEq.$fReadFixity4
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1zG.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

TestEq.$fReadFixity9 :: GHC.Types.Int
                        -> Text.ParserCombinators.ReadP.ReadS TestEq.Fixity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity9 =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Fixity
      (((GHC.Read.$fRead()5
           @ TestEq.Fixity
           (TestEq.$fReadFixity3
            `cast` (trans
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                      (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
                     ~
                   (forall b_a1zG.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ TestEq.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.Fixity))

TestEq.$fReadFixity2 :: Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity2 =
  GHC.Read.$fRead()5
    @ TestEq.Fixity
    (TestEq.$fReadFixity3
     `cast` (trans
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

TestEq.$fReadFixity1 :: Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
GblId
[Str: DmdType]
TestEq.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ TestEq.Fixity
    (TestEq.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

TestEq.$fReadFixity8 :: Text.ParserCombinators.ReadP.P
                          [TestEq.Fixity]
GblId
[Str: DmdType]
TestEq.$fReadFixity8 =
  ((TestEq.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
                ~
              (forall b_a1zG.
               ([TestEq.Fixity] -> Text.ParserCombinators.ReadP.P b_a1zG)
               -> Text.ParserCombinators.ReadP.P b_a1zG)))
    @ [TestEq.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.Fixity])

TestEq.$fShowU3 :: TestEq.U -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType U()]
TestEq.$fShowU3 =
  __inline_me (\ (x_a1gf :: TestEq.U) ->
                 case x_a1gf of _ { TestEq.U -> TestEq.$fShowU1 })

TestEq.$fReadU5 :: GHC.Types.Int
                   -> Text.ParserCombinators.ReadP.ReadS TestEq.U
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadU5 =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.U
      (((GHC.Read.$fRead()5
           @ TestEq.U
           (TestEq.$fReadU3
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
                      (trans
                         (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                         (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1zG.
                           (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                           -> Text.ParserCombinators.ReadP.P b_a1zG)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U
                :: Text.ParserCombinators.ReadP.ReadP TestEq.U
                     ~
                   (forall b_a1zG.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ TestEq.U
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.U))

TestEq.$fRepresentableRec2 :: forall a_XnQ.
                              (TestEq.Representable a_XnQ) =>
                              TestEq.Rep (TestEq.Rec a_XnQ) -> TestEq.Rep (TestEq.Rec a_XnQ)
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableRec2 =
  __inline_me (\ (@ a_XnQ)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Rec a_XnQ)) ->
                 eta_B1)

TestEq.$fRepresentableRec1 :: forall a_ah2.
                              (TestEq.Representable a_ah2) =>
                              TestEq.Rec a_ah2 -> TestEq.Rec a_ah2
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableRec1 =
  __inline_me (\ (@ a_ah2) _ (eta_B1 :: TestEq.Rec a_ah2) -> eta_B1)

TestEq.$fRepresentableVar2 :: forall a_XnL.
                              (TestEq.Representable a_XnL) =>
                              TestEq.Rep (TestEq.Var a_XnL) -> TestEq.Rep (TestEq.Var a_XnL)
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableVar2 =
  __inline_me (\ (@ a_XnL)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Var a_XnL)) ->
                 eta_B1)

TestEq.$fRepresentableVar1 :: forall a_ah4.
                              (TestEq.Representable a_ah4) =>
                              TestEq.Var a_ah4 -> TestEq.Var a_ah4
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableVar1 =
  __inline_me (\ (@ a_ah4) _ (eta_B1 :: TestEq.Var a_ah4) -> eta_B1)

TestEq.$fRepresentableC2 :: forall a_XnF c_XnH.
                            (TestEq.Representable a_XnF) =>
                            TestEq.Rep (TestEq.C c_XnH a_XnF)
                            -> TestEq.Rep (TestEq.C c_XnH a_XnF)
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableC2 =
  __inline_me (\ (@ a_XnF)
                 (@ c_XnH)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.C c_XnH a_XnF)) ->
                 eta_B1)

TestEq.$fRepresentableC1 :: forall a_ah6 c_ah7.
                            (TestEq.Representable a_ah6) =>
                            TestEq.C c_ah7 a_ah6 -> TestEq.C c_ah7 a_ah6
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableC1 =
  __inline_me (\ (@ a_ah6)
                 (@ c_ah7)
                 _
                 (eta_B1 :: TestEq.C c_ah7 a_ah6) ->
                 eta_B1)

TestEq.$fRepresentable:+:2 :: forall a_Xnz b_XnB.
                              (TestEq.Representable a_Xnz, TestEq.Representable b_XnB) =>
                              TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
                              -> TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:+:2 =
  __inline_me (\ (@ a_Xnz)
                 (@ b_XnB)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) ->
                 eta_B1)

TestEq.$fRepresentable:+:1 :: forall a_aha b_ahb.
                              (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                              (a_aha TestEq.:+: b_ahb) -> a_aha TestEq.:+: b_ahb
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:+:1 =
  __inline_me (\ (@ a_aha)
                 (@ b_ahb)
                 _
                 _
                 (eta_B1 :: a_aha TestEq.:+: b_ahb) ->
                 eta_B1)

TestEq.$fRepresentable:*:2 :: forall a_Xns b_Xnu.
                              (TestEq.Representable a_Xns, TestEq.Representable b_Xnu) =>
                              TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
                              -> TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:*:2 =
  __inline_me (\ (@ a_Xns)
                 (@ b_Xnu)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) ->
                 eta_B1)

TestEq.$fRepresentable:*:1 :: forall a_ahe b_ahf.
                              (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                              (a_ahe TestEq.:*: b_ahf) -> a_ahe TestEq.:*: b_ahf
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:*:1 =
  __inline_me (\ (@ a_ahe)
                 (@ b_ahf)
                 _
                 _
                 (eta_B1 :: a_ahe TestEq.:*: b_ahf) ->
                 eta_B1)

TestEq.logic11 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic11 = TestEq.Not TestEq.F

TestEq.logic13 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic13 = TestEq.Not TestEq.T

TestEq.logic16 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.logic16 = GHC.Types.C# 'x'

TestEq.logic15 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic15 =
  GHC.Types.:
    @ GHC.Types.Char TestEq.logic16 (GHC.Types.[] @ GHC.Types.Char)

TestEq.logic14 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic14 = TestEq.VarL TestEq.logic15

TestEq.logic12 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic12 = TestEq.Equiv TestEq.logic14 TestEq.logic13

TestEq.tree01 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.tree01 = GHC.Types.I# 3

TestEq.$dmconIsRecord :: forall c_afy.
                         (TestEq.Constructor c_afy) =>
                         forall (t_afD :: * -> * -> *) a_afE.
                         t_afD c_afy a_afE -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.$dmconIsRecord =
  __inline_me (\ (@ c_afy) _ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconFixity :: forall c_afy.
                       (TestEq.Constructor c_afy) =>
                       forall (t_afB :: * -> * -> *) a_afC.
                       t_afB c_afy a_afC -> TestEq.Fixity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.$dmconFixity =
  __inline_me (\ (@ c_afy) _ (@ t_aFL::* -> * -> *) (@ a_aFM) _ ->
                 TestEq.Prefix)

TestEq.$fRepresentableInt :: TestEq.Representable GHC.Types.Int
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableInt =
  TestEq.D:Representable
    @ GHC.Types.Int
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Int))
     `cast` (TestEq.Rep GHC.Types.Int -> TestEq.TFCo:R:RepInt
             :: (TestEq.Rep GHC.Types.Int -> TestEq.Rep GHC.Types.Int)
                  ~
                (TestEq.Rep GHC.Types.Int -> TestEq.R:RepInt)))
    ((GHC.Base.id @ GHC.Types.Int)
     `cast` (GHC.Types.Int -> sym TestEq.TFCo:R:RepInt
             :: (GHC.Types.Int -> TestEq.R:RepInt)
                  ~
                (GHC.Types.Int -> TestEq.Rep GHC.Types.Int)))

TestEq.$fRepresentableChar :: TestEq.Representable GHC.Types.Char
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableChar =
  TestEq.D:Representable
    @ GHC.Types.Char
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Char))
     `cast` (TestEq.Rep GHC.Types.Char -> TestEq.TFCo:R:RepChar
             :: (TestEq.Rep GHC.Types.Char -> TestEq.Rep GHC.Types.Char)
                  ~
                (TestEq.Rep GHC.Types.Char -> TestEq.R:RepChar)))
    ((GHC.Base.id @ GHC.Types.Char)
     `cast` (GHC.Types.Char -> sym TestEq.TFCo:R:RepChar
             :: (GHC.Types.Char -> TestEq.R:RepChar)
                  ~
                (GHC.Types.Char -> TestEq.Rep GHC.Types.Char)))

TestEq.$fRepresentableU :: TestEq.Representable TestEq.U
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableU =
  TestEq.D:Representable
    @ TestEq.U
    ((GHC.Base.id @ (TestEq.Rep TestEq.U))
     `cast` (TestEq.Rep TestEq.U -> TestEq.TFCo:R:RepU
             :: (TestEq.Rep TestEq.U -> TestEq.Rep TestEq.U)
                  ~
                (TestEq.Rep TestEq.U -> TestEq.R:RepU)))
    ((GHC.Base.id @ TestEq.U)
     `cast` (TestEq.U -> sym TestEq.TFCo:R:RepU
             :: (TestEq.U -> TestEq.R:RepU)
                  ~
                (TestEq.U -> TestEq.Rep TestEq.U)))

TestEq.$fRepresentable:*: :: forall a_ahe b_ahf.
                             (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                             TestEq.Representable (a_ahe TestEq.:*: b_ahf)
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType LLm]
TestEq.$fRepresentable:*: =
  __inline_me (\ (@ a_Xnx)
                 (@ b_Xnz)
                 ($dRepresentable_X1bW :: TestEq.Representable a_Xnx)
                 ($dRepresentable1_X1bY :: TestEq.Representable b_Xnz) ->
                 TestEq.D:Representable
                   @ (a_Xnx TestEq.:*: b_Xnz)
                   ((TestEq.$fRepresentable:*:2
                       @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable1_X1bY)
                    `cast` (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                            -> TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz
                            :: (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz))
                                 ~
                               (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.R:Rep:*: a_Xnx b_Xnz)))
                   ((TestEq.$fRepresentable:*:1
                       @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable1_X1bY)
                    `cast` ((a_Xnx TestEq.:*: b_Xnz)
                            -> sym (TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz)
                            :: ((a_Xnx TestEq.:*: b_Xnz) -> TestEq.R:Rep:*: a_Xnx b_Xnz)
                                 ~
                               ((a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)))))

TestEq.$fRepresentable:+: :: forall a_aha b_ahb.
                             (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                             TestEq.Representable (a_aha TestEq.:+: b_ahb)
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType LLm]
TestEq.$fRepresentable:+: =
  __inline_me (\ (@ a_XnE)
                 (@ b_XnG)
                 ($dRepresentable_X1bU :: TestEq.Representable a_XnE)
                 ($dRepresentable1_X1bW :: TestEq.Representable b_XnG) ->
                 TestEq.D:Representable
                   @ (a_XnE TestEq.:+: b_XnG)
                   ((TestEq.$fRepresentable:+:2
                       @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable1_X1bW)
                    `cast` (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                            -> TestEq.TFCo:R:Rep:+: a_XnE b_XnG
                            :: (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG))
                                 ~
                               (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.R:Rep:+: a_XnE b_XnG)))
                   ((TestEq.$fRepresentable:+:1
                       @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable1_X1bW)
                    `cast` ((a_XnE TestEq.:+: b_XnG)
                            -> sym (TestEq.TFCo:R:Rep:+: a_XnE b_XnG)
                            :: ((a_XnE TestEq.:+: b_XnG) -> TestEq.R:Rep:+: a_XnE b_XnG)
                                 ~
                               ((a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG)))))

TestEq.$fRepresentableC :: forall a_ah6 c_ah7.
                           (TestEq.Representable a_ah6) =>
                           TestEq.Representable (TestEq.C c_ah7 a_ah6)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fRepresentableC =
  __inline_me (\ (@ a_XnJ)
                 (@ c_XnL)
                 ($dRepresentable_X1iw :: TestEq.Representable a_XnJ) ->
                 TestEq.D:Representable
                   @ (TestEq.C c_XnL a_XnJ)
                   ((TestEq.$fRepresentableC2 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                            -> TestEq.TFCo:R:RepC c_XnL a_XnJ
                            :: (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                                -> TestEq.Rep (TestEq.C c_XnL a_XnJ))
                                 ~
                               (TestEq.Rep (TestEq.C c_XnL a_XnJ) -> TestEq.R:RepC c_XnL a_XnJ)))
                   ((TestEq.$fRepresentableC1 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.C c_XnL a_XnJ
                            -> sym (TestEq.TFCo:R:RepC c_XnL a_XnJ)
                            :: (TestEq.C c_XnL a_XnJ -> TestEq.R:RepC c_XnL a_XnJ)
                                 ~
                               (TestEq.C c_XnL a_XnJ -> TestEq.Rep (TestEq.C c_XnL a_XnJ)))))

TestEq.$fRepresentableVar :: forall a_ah4.
                             (TestEq.Representable a_ah4) =>
                             TestEq.Representable (TestEq.Var a_ah4)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fRepresentableVar =
  __inline_me (\ (@ a_XnO)
                 ($dRepresentable_X1bL :: TestEq.Representable a_XnO) ->
                 TestEq.D:Representable
                   @ (TestEq.Var a_XnO)
                   ((TestEq.$fRepresentableVar2 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.TFCo:R:RepVar a_XnO
                            :: (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.Rep (TestEq.Var a_XnO))
                                 ~
                               (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.R:RepVar a_XnO)))
                   ((TestEq.$fRepresentableVar1 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Var a_XnO -> sym (TestEq.TFCo:R:RepVar a_XnO)
                            :: (TestEq.Var a_XnO -> TestEq.R:RepVar a_XnO)
                                 ~
                               (TestEq.Var a_XnO -> TestEq.Rep (TestEq.Var a_XnO)))))

TestEq.$fRepresentableRec :: forall a_ah2.
                             (TestEq.Representable a_ah2) =>
                             TestEq.Representable (TestEq.Rec a_ah2)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fRepresentableRec =
  __inline_me (\ (@ a_XnT)
                 ($dRepresentable_X1bF :: TestEq.Representable a_XnT) ->
                 TestEq.D:Representable
                   @ (TestEq.Rec a_XnT)
                   ((TestEq.$fRepresentableRec2 @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.TFCo:R:RepRec a_XnT
                            :: (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.Rep (TestEq.Rec a_XnT))
                                 ~
                               (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.R:RepRec a_XnT)))
                   ((TestEq.$fRepresentableRec1 @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rec a_XnT -> sym (TestEq.TFCo:R:RepRec a_XnT)
                            :: (TestEq.Rec a_XnT -> TestEq.R:RepRec a_XnT)
                                 ~
                               (TestEq.Rec a_XnT -> TestEq.Rep (TestEq.Rec a_XnT)))))

TestEq.$fEqInt :: TestEq.Eq GHC.Types.Int
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType U(L)U(L)]
TestEq.$fEqInt =
  __inline_me (GHC.Base.eqInt
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Int)
                       :: (GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Int))

TestEq.$fEqChar :: TestEq.Eq GHC.Types.Char
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType U(L)U(L)]
TestEq.$fEqChar =
  __inline_me (GHC.Base.$fEqChar_==
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
                       :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Char))

TestEq.eq :: forall a_afN.
             (TestEq.Representable a_afN, TestEq.Eq (TestEq.Rep a_afN)) =>
             a_afN -> a_afN -> GHC.Bool.Bool
GblId
[Arity 4
 NoCafRefs
 Str: DmdType LSLL]
TestEq.eq =
  __inline_me (\ (@ a_azl)
                 ($dRepresentable_azt :: TestEq.Representable a_azl)
                 ($dEq_azu :: TestEq.Eq (TestEq.Rep a_azl))
                 (eta_B2 :: a_azl)
                 (eta1_B1 :: a_azl) ->
                 let {
                   from1_s1o6 [ALWAYS Just L] :: a_azl -> TestEq.Rep a_azl
                   LclId
                   [Str: DmdType {azt->U(AS)}]
                   from1_s1o6 = TestEq.from @ a_azl $dRepresentable_azt } in
                 TestEq.eq'
                   @ (TestEq.Rep a_azl)
                   $dEq_azu
                   (from1_s1o6 eta_B2)
                   (from1_s1o6 eta1_B1))

TestEq.eqRec :: forall t_azF.
                (TestEq.Eq t_azF) =>
                TestEq.Rec t_azF -> TestEq.Rec t_azF -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.eqRec =
  __inline_me (\ (@ t_azF)
                 ($dEq_azL :: TestEq.Eq t_azF)
                 (ds_d17K :: TestEq.Rec t_azF)
                 (ds1_d17L :: TestEq.Rec t_azF) ->
                 case ds_d17K of _ { TestEq.Rec x_agu ->
                 case ds1_d17L of _ { TestEq.Rec x'_agv ->
                 TestEq.eq' @ t_azF $dEq_azL x_agu x'_agv
                 }
                 })

TestEq.$fEqRec :: forall a_agD.
                  (TestEq.Eq a_agD) =>
                  TestEq.Eq (TestEq.Rec a_agD)
GblId[DFunId]
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.$fEqRec =
  __inline_me (TestEq.eqRec
               `cast` (forall a_agD.
                       (TestEq.Eq a_agD) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Rec a_agD))
                       :: (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.Rec a_agD -> TestEq.Rec a_agD -> GHC.Bool.Bool)
                            ~
                          (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.T:Eq (TestEq.Rec a_agD))))

TestEq.eqVar :: forall t_azR.
                (TestEq.Eq t_azR) =>
                TestEq.Var t_azR -> TestEq.Var t_azR -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.eqVar =
  __inline_me (\ (@ t_azR)
                 ($dEq_azX :: TestEq.Eq t_azR)
                 (ds_d17O :: TestEq.Var t_azR)
                 (ds1_d17P :: TestEq.Var t_azR) ->
                 case ds_d17O of _ { TestEq.Var x_ags ->
                 case ds1_d17P of _ { TestEq.Var x'_agt ->
                 TestEq.eq' @ t_azR $dEq_azX x_ags x'_agt
                 }
                 })

TestEq.$fEqVar :: forall a_agE.
                  (TestEq.Eq a_agE) =>
                  TestEq.Eq (TestEq.Var a_agE)
GblId[DFunId]
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.$fEqVar =
  __inline_me (TestEq.eqVar
               `cast` (forall a_agE.
                       (TestEq.Eq a_agE) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Var a_agE))
                       :: (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.Var a_agE -> TestEq.Var a_agE -> GHC.Bool.Bool)
                            ~
                          (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.T:Eq (TestEq.Var a_agE))))

TestEq.eqC :: forall t_aA2 t1_aA4 t2_aA5.
              (TestEq.Eq t1_aA4) =>
              TestEq.C t_aA2 t1_aA4 -> TestEq.C t2_aA5 t1_aA4 -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.eqC =
  __inline_me (\ (@ t_aA2)
                 (@ t1_aA4)
                 (@ t2_aA5)
                 ($dEq_aAb :: TestEq.Eq t1_aA4)
                 (ds_d17S :: TestEq.C t_aA2 t1_aA4)
                 (ds1_d17T :: TestEq.C t2_aA5 t1_aA4) ->
                 case ds_d17S of _ { TestEq.C a1_agq ->
                 case ds1_d17T of _ { TestEq.C a'_agr ->
                 TestEq.eq' @ t1_aA4 $dEq_aAb a1_agq a'_agr
                 }
                 })

TestEq.eqTimes :: forall t_aAi t1_aAj.
                  (TestEq.Eq t_aAi, TestEq.Eq t1_aAj) =>
                  (t_aAi TestEq.:*: t1_aAj)
                  -> (t_aAi TestEq.:*: t1_aAj)
                  -> GHC.Bool.Bool
GblId
[Arity 4
 NoCafRefs
 Str: DmdType SLU(LL)U(LL)]
TestEq.eqTimes =
  __inline_me (\ (@ t_aAi)
                 (@ t1_aAj)
                 ($dEq_aAt :: TestEq.Eq t_aAi)
                 ($dEq1_aAu :: TestEq.Eq t1_aAj)
                 (ds_d17W :: t_aAi TestEq.:*: t1_aAj)
                 (ds1_d17X :: t_aAi TestEq.:*: t1_aAj) ->
                 case ds_d17W of _ { TestEq.:*: a1_agi b_agj ->
                 case ds1_d17X of _ { TestEq.:*: a'_agk b'_agl ->
                 GHC.Classes.&&
                   (TestEq.eq' @ t_aAi $dEq_aAt a1_agi a'_agk)
                   (TestEq.eq' @ t1_aAj $dEq1_aAu b_agj b'_agl)
                 }
                 })

TestEq.$fEq:*: :: forall a_agH b_agI.
                  (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                  TestEq.Eq (a_agH TestEq.:*: b_agI)
GblId[DFunId]
[Arity 4
 NoCafRefs
 Str: DmdType SLU(LL)U(LL)]
TestEq.$fEq:*: =
  __inline_me (TestEq.eqTimes
               `cast` (forall a_agH b_agI.
                       (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                       sym (TestEq.NTCo:T:Eq (a_agH TestEq.:*: b_agI))
                       :: (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           (a_agH TestEq.:*: b_agI)
                           -> (a_agH TestEq.:*: b_agI)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           TestEq.T:Eq (a_agH TestEq.:*: b_agI))))

TestEq.eqPlus :: forall t_aAB t1_aAH.
                 (TestEq.Eq t_aAB, TestEq.Eq t1_aAH) =>
                 (t_aAB TestEq.:+: t1_aAH)
                 -> (t_aAB TestEq.:+: t1_aAH)
                 -> GHC.Bool.Bool
GblId
[Arity 4
 NoCafRefs
 Str: DmdType LLSS]
TestEq.eqPlus =
  __inline_me (\ (@ t_aAB)
                 (@ t1_aAH)
                 ($dEq_aAM :: TestEq.Eq t_aAB)
                 ($dEq1_aAN :: TestEq.Eq t1_aAH)
                 (ds_d180 :: t_aAB TestEq.:+: t1_aAH)
                 (ds1_d181 :: t_aAB TestEq.:+: t1_aAH) ->
                 case ds_d180 of _ {
                   TestEq.L x_age ->
                     case ds1_d181 of _ {
                       TestEq.L x'_agf -> TestEq.eq' @ t_aAB $dEq_aAM x_age x'_agf;
                       TestEq.R _ -> GHC.Bool.False
                     };
                   TestEq.R x_agg ->
                     case ds1_d181 of _ {
                       TestEq.L _ -> GHC.Bool.False;
                       TestEq.R x'_agh -> TestEq.eq' @ t1_aAH $dEq1_aAN x_agg x'_agh
                     }
                 })

TestEq.$fEq:+: :: forall a_agJ b_agK.
                  (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                  TestEq.Eq (a_agJ TestEq.:+: b_agK)
GblId[DFunId]
[Arity 4
 NoCafRefs
 Str: DmdType LLSS]
TestEq.$fEq:+: =
  __inline_me (TestEq.eqPlus
               `cast` (forall a_agJ b_agK.
                       (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                       sym (TestEq.NTCo:T:Eq (a_agJ TestEq.:+: b_agK))
                       :: (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           (a_agJ TestEq.:+: b_agK)
                           -> (a_agJ TestEq.:+: b_agK)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           TestEq.T:Eq (a_agJ TestEq.:+: b_agK))))

TestEq.eqU :: forall t_aAS t1_aAT. t_aAS -> t1_aAT -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.eqU =
  __inline_me (\ (@ t_aAS) (@ t1_aAT) _ _ -> GHC.Bool.True)

TestEq.$fEqU :: TestEq.Eq TestEq.U
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.$fEqU =
  __inline_me ((TestEq.eqU @ TestEq.U @ TestEq.U)
               `cast` (sym (TestEq.NTCo:T:Eq TestEq.U)
                       :: (TestEq.U -> TestEq.U -> GHC.Bool.Bool) ~ TestEq.T:Eq TestEq.U))

TestEq.toLogic :: forall t_aB0
                         t1_aB6
                         t2_aBa
                         t3_aBe
                         t4_aBk
                         t5_aBu
                         t6_aBE
                         t7_aBM.
                  (TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                   TestEq.:+: (TestEq.C t1_aB6 TestEq.U
                               TestEq.:+: (TestEq.C t2_aBa TestEq.U
                                           TestEq.:+: (TestEq.C t3_aBe (TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     t4_aBk
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 t5_aBu
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: (TestEq.C
                                                                                             t6_aBE
                                                                                             (TestEq.Rec
                                                                                                TestEq.Logic
                                                                                              TestEq.:*: TestEq.Rec
                                                                                                           TestEq.Logic)
                                                                                           TestEq.:+: TestEq.C
                                                                                                        t7_aBM
                                                                                                        (TestEq.Rec
                                                                                                           TestEq.Logic
                                                                                                         TestEq.:*: TestEq.Rec
                                                                                                                      TestEq.Logic))))))))
                  -> TestEq.Logic
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.toLogic =
  __inline_me (\ (@ t_aB0)
                 (@ t1_aB6)
                 (@ t2_aBa)
                 (@ t3_aBe)
                 (@ t4_aBk)
                 (@ t5_aBu)
                 (@ t6_aBE)
                 (@ t7_aBM)
                 (ds_d18a :: TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                             TestEq.:+: (TestEq.C t1_aB6 TestEq.U
                                         TestEq.:+: (TestEq.C t2_aBa TestEq.U
                                                     TestEq.:+: (TestEq.C
                                                                   t3_aBe (TestEq.Rec TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               t4_aBk
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: (TestEq.C
                                                                                           t5_aBu
                                                                                           (TestEq.Rec
                                                                                              TestEq.Logic
                                                                                            TestEq.:*: TestEq.Rec
                                                                                                         TestEq.Logic)
                                                                                         TestEq.:+: (TestEq.C
                                                                                                       t6_aBE
                                                                                                       (TestEq.Rec
                                                                                                          TestEq.Logic
                                                                                                        TestEq.:*: TestEq.Rec
                                                                                                                     TestEq.Logic)
                                                                                                     TestEq.:+: TestEq.C
                                                                                                                  t7_aBM
                                                                                                                  (TestEq.Rec
                                                                                                                     TestEq.Logic
                                                                                                                   TestEq.:*: TestEq.Rec
                                                                                                                                TestEq.Logic)))))))) ->
                 case ds_d18a of _ {
                   TestEq.L ds1_d18D ->
                     case ds1_d18D of _ { TestEq.C ds2_d18E ->
                     case ds2_d18E of _ { TestEq.Var f0_ag4 -> TestEq.VarL f0_ag4 }
                     };
                   TestEq.R ds1_d18b ->
                     case ds1_d18b of _ {
                       TestEq.L ds2_d18B ->
                         case ds2_d18B of _ { TestEq.C ds3_d18C ->
                         case ds3_d18C of _ { TestEq.U -> TestEq.T }
                         };
                       TestEq.R ds2_d18c ->
                         case ds2_d18c of _ {
                           TestEq.L ds3_d18z ->
                             case ds3_d18z of _ { TestEq.C ds4_d18A ->
                             case ds4_d18A of _ { TestEq.U -> TestEq.F }
                             };
                           TestEq.R ds3_d18d ->
                             case ds3_d18d of _ {
                               TestEq.L ds4_d18x ->
                                 case ds4_d18x of _ { TestEq.C ds5_d18y ->
                                 case ds5_d18y of _ { TestEq.Rec f0_ag5 -> TestEq.Not f0_ag5 }
                                 };
                               TestEq.R ds4_d18e ->
                                 case ds4_d18e of _ {
                                   TestEq.L ds5_d18t ->
                                     case ds5_d18t of _ { TestEq.C ds6_d18u ->
                                     case ds6_d18u of _ { TestEq.:*: ds7_d18v ds8_d18w ->
                                     case ds7_d18v of _ { TestEq.Rec f0_ag6 ->
                                     case ds8_d18w of _ { TestEq.Rec f1_ag7 ->
                                     TestEq.Impl f0_ag6 f1_ag7
                                     }
                                     }
                                     }
                                     };
                                   TestEq.R ds5_d18f ->
                                     case ds5_d18f of _ {
                                       TestEq.L ds6_d18p ->
                                         case ds6_d18p of _ { TestEq.C ds7_d18q ->
                                         case ds7_d18q of _ { TestEq.:*: ds8_d18r ds9_d18s ->
                                         case ds8_d18r of _ { TestEq.Rec f0_ag8 ->
                                         case ds9_d18s of _ { TestEq.Rec f1_ag9 ->
                                         TestEq.Equiv f0_ag8 f1_ag9
                                         }
                                         }
                                         }
                                         };
                                       TestEq.R ds6_d18g ->
                                         case ds6_d18g of _ {
                                           TestEq.L ds7_d18l ->
                                             case ds7_d18l of _ { TestEq.C ds8_d18m ->
                                             case ds8_d18m of _ { TestEq.:*: ds9_d18n ds10_d18o ->
                                             case ds9_d18n of _ { TestEq.Rec f0_aga ->
                                             case ds10_d18o of _ { TestEq.Rec f1_agb ->
                                             TestEq.Conj f0_aga f1_agb
                                             }
                                             }
                                             }
                                             };
                                           TestEq.R ds7_d18h ->
                                             case ds7_d18h of _ { TestEq.C ds8_d18i ->
                                             case ds8_d18i of _ { TestEq.:*: ds9_d18j ds10_d18k ->
                                             case ds9_d18j of _ { TestEq.Rec f0_agc ->
                                             case ds10_d18k of _ { TestEq.Rec f1_agd ->
                                             TestEq.Disj f0_agc f1_agd
                                             }
                                             }
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 })

TestEq.fromLogic :: forall c_aC3
                           c1_aCb
                           c2_aCl
                           c3_aCz
                           c4_aCT
                           c5_aDg
                           c6_aDG
                           c7_aE7.
                    TestEq.Logic
                    -> TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C c1_aCb TestEq.U
                                   TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                               TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         c4_aCT
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     c5_aDg
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 c6_aDG
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            c7_aE7
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic)))))))
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.fromLogic =
  __inline_me (\ (@ c_aC3)
                 (@ c1_aCb)
                 (@ c2_aCl)
                 (@ c3_aCz)
                 (@ c4_aCT)
                 (@ c5_aDg)
                 (@ c6_aDG)
                 (@ c7_aE7)
                 (ds_d19h :: TestEq.Logic) ->
                 case ds_d19h of _ {
                   TestEq.VarL f0_afU ->
                     TestEq.L
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.C
                          @ c_aC3
                          @ (TestEq.Var GHC.Base.String)
                          (TestEq.Var @ GHC.Base.String f0_afU));
                   TestEq.T ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.L
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.C @ c1_aCb @ TestEq.U TestEq.U));
                   TestEq.F ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.L
                             @ (TestEq.C c2_aCl TestEq.U)
                             @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c4_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c5_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c6_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c7_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.C @ c2_aCl @ TestEq.U TestEq.U)));
                   TestEq.Not f0_afV ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c2_aCl TestEq.U)
                             @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c4_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c5_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c6_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c7_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.L
                                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c4_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c5_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c6_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c7_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.C
                                   @ c3_aCz
                                   @ (TestEq.Rec TestEq.Logic)
                                   (TestEq.Rec @ TestEq.Logic f0_afV)))));
                   TestEq.Impl f0_afW f1_afX ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c2_aCl TestEq.U)
                             @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c4_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c5_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c6_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c7_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c4_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c5_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c6_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c7_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.L
                                   @ (TestEq.C
                                        c4_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c5_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c6_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c7_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.C
                                      @ c4_aCT
                                      @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      (TestEq.:*:
                                         @ (TestEq.Rec TestEq.Logic)
                                         @ (TestEq.Rec TestEq.Logic)
                                         (TestEq.Rec @ TestEq.Logic f0_afW)
                                         (TestEq.Rec @ TestEq.Logic f1_afX)))))));
                   TestEq.Equiv f0_afY f1_afZ ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c2_aCl TestEq.U)
                             @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c4_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c5_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c6_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c7_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c4_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c5_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c6_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c7_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c4_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c5_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c6_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c7_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.L
                                      @ (TestEq.C
                                           c5_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c6_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c7_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.C
                                         @ c5_aDg
                                         @ (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         (TestEq.:*:
                                            @ (TestEq.Rec TestEq.Logic)
                                            @ (TestEq.Rec TestEq.Logic)
                                            (TestEq.Rec @ TestEq.Logic f0_afY)
                                            (TestEq.Rec @ TestEq.Logic f1_afZ))))))));
                   TestEq.Conj f0_ag0 f1_ag1 ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c2_aCl TestEq.U)
                             @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c4_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c5_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c6_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c7_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c4_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c5_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c6_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c7_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c4_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c5_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c6_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c7_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.R
                                      @ (TestEq.C
                                           c5_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c6_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c7_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.L
                                         @ (TestEq.C
                                              c6_aDG
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         @ (TestEq.C
                                              c7_aE7
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         (TestEq.C
                                            @ c6_aDG
                                            @ (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            (TestEq.:*:
                                               @ (TestEq.Rec TestEq.Logic)
                                               @ (TestEq.Rec TestEq.Logic)
                                               (TestEq.Rec @ TestEq.Logic f0_ag0)
                                               (TestEq.Rec @ TestEq.Logic f1_ag1)))))))));
                   TestEq.Disj f0_ag2 f1_ag3 ->
                     TestEq.R
                       @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
                       @ (TestEq.C c1_aCb TestEq.U
                          TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                      TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                c4_aCT
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            c5_aDg
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        c6_aDG
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   c7_aE7
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
                       (TestEq.R
                          @ (TestEq.C c1_aCb TestEq.U)
                          @ (TestEq.C c2_aCl TestEq.U
                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                         TestEq.:+: (TestEq.C
                                                       c4_aCT
                                                       (TestEq.Rec TestEq.Logic
                                                        TestEq.:*: TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   c5_aDg
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               c6_aDG
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: TestEq.C
                                                                                          c7_aE7
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic))))))
                          (TestEq.R
                             @ (TestEq.C c2_aCl TestEq.U)
                             @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                TestEq.:+: (TestEq.C
                                              c4_aCT
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            TestEq.:+: (TestEq.C
                                                          c5_aDg
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                                        TestEq.:+: (TestEq.C
                                                                      c6_aDG
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic)
                                                                    TestEq.:+: TestEq.C
                                                                                 c7_aE7
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)))))
                             (TestEq.R
                                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                                @ (TestEq.C
                                     c4_aCT
                                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                   TestEq.:+: (TestEq.C
                                                 c5_aDg
                                                 (TestEq.Rec TestEq.Logic
                                                  TestEq.:*: TestEq.Rec TestEq.Logic)
                                               TestEq.:+: (TestEq.C
                                                             c6_aDG
                                                             (TestEq.Rec TestEq.Logic
                                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: TestEq.C
                                                                        c7_aE7
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic))))
                                (TestEq.R
                                   @ (TestEq.C
                                        c4_aCT
                                        (TestEq.Rec TestEq.Logic
                                         TestEq.:*: TestEq.Rec TestEq.Logic))
                                   @ (TestEq.C
                                        c5_aDg
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    c6_aDG
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               c7_aE7
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic)))
                                   (TestEq.R
                                      @ (TestEq.C
                                           c5_aDg
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic))
                                      @ (TestEq.C
                                           c6_aDG
                                           (TestEq.Rec TestEq.Logic
                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                         TestEq.:+: TestEq.C
                                                      c7_aE7
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic))
                                      (TestEq.R
                                         @ (TestEq.C
                                              c6_aDG
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         @ (TestEq.C
                                              c7_aE7
                                              (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic))
                                         (TestEq.C
                                            @ c7_aE7
                                            @ (TestEq.Rec TestEq.Logic
                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                            (TestEq.:*:
                                               @ (TestEq.Rec TestEq.Logic)
                                               @ (TestEq.Rec TestEq.Logic)
                                               (TestEq.Rec @ TestEq.Logic f0_ag2)
                                               (TestEq.Rec @ TestEq.Logic f1_ag3)))))))))
                 })

TestEq.$fRepresentableLogic :: TestEq.Representable TestEq.Logic
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableLogic =
  TestEq.D:Representable
    @ TestEq.Logic
    ((TestEq.toLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (sym TestEq.TFCo:R:RepLogic -> TestEq.Logic
             :: (TestEq.R:RepLogic -> TestEq.Logic)
                  ~
                (TestEq.Rep TestEq.Logic -> TestEq.Logic)))
    ((TestEq.fromLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (TestEq.Logic -> sym TestEq.TFCo:R:RepLogic
             :: (TestEq.Logic -> TestEq.R:RepLogic)
                  ~
                (TestEq.Logic -> TestEq.Rep TestEq.Logic)))

TestEq.toTree :: forall t_aEI t1_aEN t2_aEV.
                 (TestEq.C t2_aEV TestEq.U
                  TestEq.:+: TestEq.C
                               t_aEI
                               (TestEq.Var t1_aEN
                                TestEq.:*: (TestEq.Rec (TestEq.Tree t1_aEN)
                                            TestEq.:*: TestEq.Rec (TestEq.Tree t1_aEN))))
                 -> TestEq.Tree t1_aEN
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.toTree =
  __inline_me (\ (@ t_aEI)
                 (@ t1_aEN)
                 (@ t2_aEV)
                 (ds_d19q :: TestEq.C t2_aEV TestEq.U
                             TestEq.:+: TestEq.C
                                          t_aEI
                                          (TestEq.Var t1_aEN
                                           TestEq.:*: (TestEq.Rec (TestEq.Tree t1_aEN)
                                                       TestEq.:*: TestEq.Rec
                                                                    (TestEq.Tree t1_aEN)))) ->
                 case ds_d19q of _ {
                   TestEq.L ds1_d19x ->
                     case ds1_d19x of _ { TestEq.C ds2_d19y ->
                     case ds2_d19y of _ { TestEq.U -> TestEq.Leaf @ t1_aEN }
                     };
                   TestEq.R ds1_d19r ->
                     case ds1_d19r of _ { TestEq.C ds2_d19s ->
                     case ds2_d19s of _ { TestEq.:*: ds3_d19t ds4_d19u ->
                     case ds3_d19t of _ { TestEq.Var x_afR ->
                     case ds4_d19u of _ { TestEq.:*: ds5_d19v ds6_d19w ->
                     case ds5_d19v of _ { TestEq.Rec l_afS ->
                     case ds6_d19w of _ { TestEq.Rec r_afT ->
                     TestEq.Bin @ t1_aEN x_afR l_afS r_afT
                     }
                     }
                     }
                     }
                     }
                     }
                 })

TestEq.fromTree :: forall t_aF3 c_aFf c1_aFl.
                   TestEq.Tree t_aF3
                   -> TestEq.C c1_aFl TestEq.U
                      TestEq.:+: TestEq.C
                                   c_aFf
                                   (TestEq.Var t_aF3
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.fromTree =
  __inline_me (\ (@ t_aF3)
                 (@ c_aFf)
                 (@ c1_aFl)
                 (ds_d19J :: TestEq.Tree t_aF3) ->
                 case ds_d19J of _ {
                   TestEq.Leaf ->
                     TestEq.L
                       @ (TestEq.C c1_aFl TestEq.U)
                       @ (TestEq.C
                            c_aFf
                            (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
                       (TestEq.C @ c1_aFl @ TestEq.U TestEq.U);
                   TestEq.Bin x_afO l_afP r_afQ ->
                     TestEq.R
                       @ (TestEq.C c1_aFl TestEq.U)
                       @ (TestEq.C
                            c_aFf
                            (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
                       (TestEq.C
                          @ c_aFf
                          @ (TestEq.Var t_aF3
                             TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
                          (TestEq.:*:
                             @ (TestEq.Var t_aF3)
                             @ (TestEq.Rec (TestEq.Tree t_aF3)
                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))
                             (TestEq.Var @ t_aF3 x_afO)
                             (TestEq.:*:
                                @ (TestEq.Rec (TestEq.Tree t_aF3))
                                @ (TestEq.Rec (TestEq.Tree t_aF3))
                                (TestEq.Rec @ (TestEq.Tree t_aF3) l_afP)
                                (TestEq.Rec @ (TestEq.Tree t_aF3) r_afQ))))
                 })

TestEq.$fRepresentableTree1 :: forall a_agL.
                               TestEq.Tree a_agL
                               -> TestEq.C TestEq.Leaf TestEq.U
                                  TestEq.:+: TestEq.C
                                               TestEq.Bin
                                               (TestEq.Var a_agL
                                                TestEq.:*: (TestEq.Rec (TestEq.Tree a_agL)
                                                            TestEq.:*: TestEq.Rec
                                                                         (TestEq.Tree a_agL)))
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.$fRepresentableTree1 =
  \ (@ a_agL) (sub_a178 :: TestEq.Tree a_agL) ->
    TestEq.fromTree @ a_agL @ TestEq.Bin @ TestEq.Leaf sub_a178

TestEq.$fRepresentableTree :: forall a_agL.
                              TestEq.Representable (TestEq.Tree a_agL)
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableTree =
  \ (@ a_XnZ) ->
    TestEq.D:Representable
      @ (TestEq.Tree a_XnZ)
      ((TestEq.toTree @ TestEq.Bin @ a_XnZ @ TestEq.Leaf)
       `cast` (sym (TestEq.TFCo:R:RepTree a_XnZ) -> TestEq.Tree a_XnZ
               :: (TestEq.R:RepTree a_XnZ -> TestEq.Tree a_XnZ)
                    ~
                  (TestEq.Rep (TestEq.Tree a_XnZ) -> TestEq.Tree a_XnZ)))
      ((TestEq.$fRepresentableTree1 @ a_XnZ)
       `cast` (TestEq.Tree a_XnZ -> sym (TestEq.TFCo:R:RepTree a_XnZ)
               :: (TestEq.Tree a_XnZ -> TestEq.R:RepTree a_XnZ)
                    ~
                  (TestEq.Tree a_XnZ -> TestEq.Rep (TestEq.Tree a_XnZ))))

TestEq.tree0 :: TestEq.Tree GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType]
TestEq.tree0 =
  TestEq.Bin
    @ GHC.Types.Int
    TestEq.tree01
    (TestEq.Leaf @ GHC.Types.Int)
    (TestEq.Leaf @ GHC.Types.Int)

TestEq.tree1 :: TestEq.Tree GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType]
TestEq.tree1 =
  TestEq.Bin
    @ GHC.Types.Int
    TestEq.$fConstructorList_Cons_2
    TestEq.tree0
    TestEq.tree0

TestEq.logic1 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic1 = TestEq.Impl TestEq.logic12 TestEq.logic11

TestEq.$fRepresentable[]_from :: forall a_agN.
                                 [a_agN] -> TestEq.Rep [a_agN]
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.$fRepresentable[]_from =
  \ (@ a_agN) (ds_d1dc :: [a_agN]) ->
    case ds_d1dc of _ {
      [] ->
        (TestEq.$fRepresentable[]1 @ a_agN)
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN]);
      : a1_agO as_agP ->
        (TestEq.R
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C
              @ TestEq.List_Cons_
              @ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
              (TestEq.:*:
                 @ (TestEq.Var a_agN)
                 @ (TestEq.Rec [a_agN])
                 (TestEq.Var @ a_agN a1_agO)
                 (TestEq.Rec @ [a_agN] as_agP))))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN])
    }

TestEq.$fRepresentable[]_to :: forall a_agN.
                               TestEq.Rep [a_agN] -> [a_agN]
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.$fRepresentable[]_to =
  \ (@ a_agN) (ds_d1cW :: TestEq.Rep [a_agN]) ->
    case ds_d1cW
         `cast` (sym (sym (TestEq.TFCo:R:Rep[] a_agN))
                 :: TestEq.Rep [a_agN] ~ TestEq.R:Rep[] a_agN)
    of _ {
      TestEq.L ds1_d1d2 ->
        case ds1_d1d2 of _ { TestEq.C ds2_d1d3 ->
        case ds2_d1d3 of _ { TestEq.U -> GHC.Types.[] @ a_agN }
        };
      TestEq.R ds1_d1cY ->
        case ds1_d1cY of _ { TestEq.C ds2_d1cZ ->
        case ds2_d1cZ of _ { TestEq.:*: ds3_d1d0 ds4_d1d1 ->
        case ds3_d1d0 of _ { TestEq.Var a1_agZ ->
        case ds4_d1d1 of _ { TestEq.Rec as_ah0 ->
        GHC.Types.: @ a_agN a1_agZ as_ah0
        }
        }
        }
        }
    }

TestEq.$fRepresentable[] :: forall a_agN.
                            TestEq.Representable [a_agN]
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentable[] =
  \ (@ a_agN) ->
    TestEq.D:Representable
      @ [a_agN]
      (TestEq.$fRepresentable[]_to @ a_agN)
      (TestEq.$fRepresentable[]_from @ a_agN)

TestEq.$fConstructorList_Nil__conName :: forall (t_afz :: *
                                                          -> *
                                                          -> *)
                                                a_afA.
                                         t_afz TestEq.List_Nil_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorList_Nil__conName =
  __inline_me (\ (@ t_a14g::* -> * -> *) (@ a_a14h) _ ->
                 TestEq.$fConstructorList_Nil_1)

TestEq.$fConstructorList_Nil_ :: TestEq.Constructor
                                   TestEq.List_Nil_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorList_Nil_ =
  TestEq.D:Constructor
    @ TestEq.List_Nil_
    TestEq.$fConstructorList_Nil__conName
    TestEq.$dmconFixity_$s$dmconFixity
    TestEq.$dmconIsRecord_$s$dmconIsRecord11

TestEq.$fConstructorList_Cons__conFixity :: forall (t_afB :: *
                                                             -> *
                                                             -> *)
                                                   a_afC.
                                            t_afB TestEq.List_Cons_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorList_Cons__conFixity =
  __inline_me (\ (@ t_a147::* -> * -> *) (@ a_a148) _ ->
                 TestEq.$fConstructorList_Cons_1)

TestEq.$fConstructorList_Cons__conName :: forall (t_afz :: *
                                                           -> *
                                                           -> *)
                                                 a_afA.
                                          t_afz TestEq.List_Cons_ a_afA -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorList_Cons__conName =
  __inline_me (\ (@ t_a141::* -> * -> *) (@ a_a142) _ ->
                 TestEq.$fConstructorList_Cons_3)

TestEq.$fConstructorList_Cons_ :: TestEq.Constructor
                                    TestEq.List_Cons_
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorList_Cons_ =
  TestEq.D:Constructor
    @ TestEq.List_Cons_
    TestEq.$fConstructorList_Cons__conName
    TestEq.$fConstructorList_Cons__conFixity
    TestEq.$dmconIsRecord_$s$dmconIsRecord10

TestEq.$fConstructorBin_conName :: forall (t_afz :: * -> * -> *)
                                          a_afA.
                                   t_afz TestEq.Bin a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorBin_conName =
  __inline_me (\ (@ t_a13Q::* -> * -> *) (@ a_a13R) _ ->
                 TestEq.$fConstructorBin1)

TestEq.$fConstructorBin :: TestEq.Constructor TestEq.Bin
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorBin =
  TestEq.D:Constructor
    @ TestEq.Bin
    TestEq.$fConstructorBin_conName
    TestEq.$dmconFixity_$s$dmconFixity1
    TestEq.$dmconIsRecord_$s$dmconIsRecord9

TestEq.$fConstructorLeaf_conName :: forall (t_afz :: * -> * -> *)
                                           a_afA.
                                    t_afz TestEq.Leaf a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLeaf_conName =
  __inline_me (\ (@ t_a13F::* -> * -> *) (@ a_a13G) _ ->
                 TestEq.$fConstructorLeaf1)

TestEq.$fConstructorLeaf :: TestEq.Constructor TestEq.Leaf
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLeaf =
  TestEq.D:Constructor
    @ TestEq.Leaf
    TestEq.$fConstructorLeaf_conName
    TestEq.$dmconFixity_$s$dmconFixity2
    TestEq.$dmconIsRecord_$s$dmconIsRecord8

TestEq.$fConstructorLogic_Var__conName :: forall (t_afz :: *
                                                           -> *
                                                           -> *)
                                                 a_afA.
                                          t_afz TestEq.Logic_Var_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Var__conName =
  __inline_me (\ (@ t_a13f::* -> * -> *) (@ a_a13g) _ ->
                 TestEq.$fConstructorLogic_Var_1)

TestEq.$fConstructorLogic_Var_ :: TestEq.Constructor
                                    TestEq.Logic_Var_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Var_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Var_
    TestEq.$fConstructorLogic_Var__conName
    TestEq.$dmconFixity_$s$dmconFixity3
    TestEq.$dmconIsRecord_$s$dmconIsRecord7

TestEq.$fConstructorLogic_Impl__conName :: forall (t_afz :: *
                                                            -> *
                                                            -> *)
                                                  a_afA.
                                           t_afz TestEq.Logic_Impl_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Impl__conName =
  __inline_me (\ (@ t_a134::* -> * -> *) (@ a_a135) _ ->
                 TestEq.$fConstructorLogic_Impl_1)

TestEq.$fConstructorLogic_Impl_ :: TestEq.Constructor
                                     TestEq.Logic_Impl_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Impl_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Impl_
    TestEq.$fConstructorLogic_Impl__conName
    TestEq.$dmconFixity_$s$dmconFixity4
    TestEq.$dmconIsRecord_$s$dmconIsRecord6

TestEq.$fConstructorLogic_Equiv__conName :: forall (t_afz :: *
                                                             -> *
                                                             -> *)
                                                   a_afA.
                                            t_afz TestEq.Logic_Equiv_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Equiv__conName =
  __inline_me (\ (@ t_a12T::* -> * -> *) (@ a_a12U) _ ->
                 TestEq.$fConstructorLogic_Equiv_1)

TestEq.$fConstructorLogic_Equiv_ :: TestEq.Constructor
                                      TestEq.Logic_Equiv_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Equiv_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Equiv_
    TestEq.$fConstructorLogic_Equiv__conName
    TestEq.$dmconFixity_$s$dmconFixity5
    TestEq.$dmconIsRecord_$s$dmconIsRecord5

TestEq.$fConstructorLogic_Conj__conName :: forall (t_afz :: *
                                                            -> *
                                                            -> *)
                                                  a_afA.
                                           t_afz TestEq.Logic_Conj_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Conj__conName =
  __inline_me (\ (@ t_a12I::* -> * -> *) (@ a_a12J) _ ->
                 TestEq.$fConstructorLogic_Conj_1)

TestEq.$fConstructorLogic_Conj_ :: TestEq.Constructor
                                     TestEq.Logic_Conj_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Conj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Conj_
    TestEq.$fConstructorLogic_Conj__conName
    TestEq.$dmconFixity_$s$dmconFixity6
    TestEq.$dmconIsRecord_$s$dmconIsRecord4

TestEq.$fConstructorLogic_Disj__conName :: forall (t_afz :: *
                                                            -> *
                                                            -> *)
                                                  a_afA.
                                           t_afz TestEq.Logic_Disj_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Disj__conName =
  __inline_me (\ (@ t_a12x::* -> * -> *) (@ a_a12y) _ ->
                 TestEq.$fConstructorLogic_Disj_1)

TestEq.$fConstructorLogic_Disj_ :: TestEq.Constructor
                                     TestEq.Logic_Disj_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Disj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Disj_
    TestEq.$fConstructorLogic_Disj__conName
    TestEq.$dmconFixity_$s$dmconFixity7
    TestEq.$dmconIsRecord_$s$dmconIsRecord3

TestEq.$fConstructorLogic_Not__conName :: forall (t_afz :: *
                                                           -> *
                                                           -> *)
                                                 a_afA.
                                          t_afz TestEq.Logic_Not_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Not__conName =
  __inline_me (\ (@ t_a12m::* -> * -> *) (@ a_a12n) _ ->
                 TestEq.$fConstructorLogic_Not_1)

TestEq.$fConstructorLogic_Not_ :: TestEq.Constructor
                                    TestEq.Logic_Not_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Not_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Not_
    TestEq.$fConstructorLogic_Not__conName
    TestEq.$dmconFixity_$s$dmconFixity8
    TestEq.$dmconIsRecord_$s$dmconIsRecord2

TestEq.$fConstructorLogic_T__conName :: forall (t_afz :: *
                                                         -> *
                                                         -> *)
                                               a_afA.
                                        t_afz TestEq.Logic_T_ a_afA -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorLogic_T__conName =
  __inline_me (\ (@ t_a12b::* -> * -> *) (@ a_a12c) _ ->
                 TestEq.$fConstructorLogic_T_1)

TestEq.$fConstructorLogic_T_ :: TestEq.Constructor TestEq.Logic_T_
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_T_ =
  TestEq.D:Constructor
    @ TestEq.Logic_T_
    TestEq.$fConstructorLogic_T__conName
    TestEq.$dmconFixity_$s$dmconFixity9
    TestEq.$dmconIsRecord_$s$dmconIsRecord1

TestEq.$fConstructorLogic_F__conName :: forall (t_afz :: *
                                                         -> *
                                                         -> *)
                                               a_afA.
                                        t_afz TestEq.Logic_F_ a_afA -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorLogic_F__conName =
  __inline_me (\ (@ t_a120::* -> * -> *) (@ a_a121) _ ->
                 TestEq.$fConstructorLogic_F_1)

TestEq.$fConstructorLogic_F_ :: TestEq.Constructor TestEq.Logic_F_
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_F_ =
  TestEq.D:Constructor
    @ TestEq.Logic_F_
    TestEq.$fConstructorLogic_F__conName
    TestEq.$dmconFixity_$s$dmconFixity10
    TestEq.$dmconIsRecord_$s$dmconIsRecord

TestEq.$fEqC_eq' :: forall a_agF c_agG.
                    (TestEq.Eq a_agF) =>
                    TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.$fEqC_eq' =
  __inline_me (\ (@ a_agF)
                 (@ c_agG)
                 ($dEq_a110 :: TestEq.Eq a_agF)
                 (eta_B2 :: TestEq.C c_agG a_agF)
                 (eta1_B1 :: TestEq.C c_agG a_agF) ->
                 TestEq.eqC @ c_agG @ a_agF @ c_agG $dEq_a110 eta_B2 eta1_B1)

TestEq.$fEqC :: forall a_agF c_agG.
                (TestEq.Eq a_agF) =>
                TestEq.Eq (TestEq.C c_agG a_agF)
GblId[DFunId]
[Arity 3
 NoCafRefs
 Str: DmdType SU(L)U(L)]
TestEq.$fEqC =
  __inline_me (TestEq.$fEqC_eq'
               `cast` (forall a_agF c_agG.
                       (TestEq.Eq a_agF) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.C c_agG a_agF))
                       :: (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool)
                            ~
                          (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.T:Eq (TestEq.C c_agG a_agF))))

Rec {
TestEq.$fEq[]_eq' :: forall a_agC.
                     (TestEq.Eq a_agC) =>
                     [a_agC] -> [a_agC] -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEq[]_eq' =
  \ (@ a_agC) ($dEq_a10b :: TestEq.Eq a_agC) ->
    let {
      a1_s1MJ :: [a_agC] -> [a_agC] -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      a1_s1MJ = TestEq.$fEq[]_eq' @ a_agC $dEq_a10b } in
    \ (eta_Xbg :: [a_agC]) (eta1_Xmv :: [a_agC]) ->
      case eta_Xbg of _ {
        [] ->
          case eta1_Xmv of _ {
            [] -> GHC.Bool.True; : _ _ -> GHC.Bool.False
          };
        : a2_agO as_agP ->
          case eta1_Xmv of _ {
            [] -> GHC.Bool.False;
            : a3_XnU as1_XnW ->
              case ($dEq_a10b
                    `cast` (TestEq.NTCo:T:Eq a_agC
                            :: TestEq.T:Eq a_agC ~ (a_agC -> a_agC -> GHC.Bool.Bool)))
                     a2_agO a3_XnU
              of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True -> a1_s1MJ as_agP as1_XnW
              }
          }
      }
end Rec }

TestEq.$fEq[] :: forall a_agC.
                 (TestEq.Eq a_agC) =>
                 TestEq.Eq [a_agC]
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEq[] =
  __inline_me (TestEq.$fEq[]_eq'
               `cast` (forall a_agC.
                       (TestEq.Eq a_agC) =>
                       sym (TestEq.NTCo:T:Eq [a_agC])
                       :: (forall a_agC.
                           (TestEq.Eq a_agC) =>
                           [a_agC] -> [a_agC] -> GHC.Bool.Bool)
                            ~
                          (forall a_agC. (TestEq.Eq a_agC) => TestEq.T:Eq [a_agC])))

a_r1Y7 :: [GHC.Types.Char] -> [GHC.Types.Char] -> GHC.Bool.Bool
GblId
[Str: DmdType]
a_r1Y7 =
  TestEq.$fEq[]_eq'
    @ GHC.Types.Char
    (GHC.Base.$fEqChar_==
     `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
             :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                  ~
                TestEq.T:Eq GHC.Types.Char))

TestEq.$fReadU_readListDefault :: Text.ParserCombinators.ReadP.ReadS
                                    [TestEq.U]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadU_readListDefault =
  Text.ParserCombinators.ReadP.run @ [TestEq.U] TestEq.$fReadU4

TestEq.$fReadU :: GHC.Read.Read TestEq.U
GblId[DFunId]
[Str: DmdType m]
TestEq.$fReadU =
  GHC.Read.D:Read
    @ TestEq.U
    TestEq.$fReadU5
    TestEq.$fReadU_readListDefault
    (TestEq.$fReadU2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
    (TestEq.$fReadU1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.U])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.U])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.U]))

TestEq.$fShowU_showList :: [TestEq.U] -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowU_showList =
  \ (ds1_a1gk :: [TestEq.U]) (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.U ->
           GHC.Base.++
             @ GHC.Types.Char
             TestEq.$fShowU1
             (let {
                lvl12_s1ML :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1ML =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1MN :: [TestEq.U] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MN =
                  \ (ds2_a1gv :: [TestEq.U]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1ML;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.U ->
                           GHC.Base.++ @ GHC.Types.Char TestEq.$fShowU1 (showl_s1MN ys_a1gB)
                           })
                    }; } in
              showl_s1MN xs_a1gr)
           })
    }

TestEq.$fShowU_showsPrec :: GHC.Types.Int
                            -> TestEq.U
                            -> GHC.Show.ShowS
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AU()L]
TestEq.$fShowU_showsPrec =
  __inline_me (\ _
                 (ds1_d1cw :: TestEq.U)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds1_d1cw of _ { TestEq.U ->
                 GHC.Base.++ @ GHC.Types.Char TestEq.$fShowU1 eta_B1
                 })

TestEq.$fShowU :: GHC.Show.Show TestEq.U
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowU =
  GHC.Show.D:Show
    @ TestEq.U
    TestEq.$fShowU_showsPrec
    TestEq.$fShowU3
    TestEq.$fShowU_showList

TestEq.$fRead:+:_readList :: forall a_afL b_afM.
                             (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                             Text.ParserCombinators.ReadP.ReadS [a_afL TestEq.:+: b_afM]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:+:_readList =
  \ (@ a_Xtl)
    (@ b_Xtn)
    ($dRead_X1c0 :: GHC.Read.Read a_Xtl)
    ($dRead1_X1c2 :: GHC.Read.Read b_Xtn) ->
    Text.ParserCombinators.ReadP.run
      @ [a_Xtl TestEq.:+: b_Xtn]
      (((GHC.Read.$dmreadList2
           @ (a_Xtl TestEq.:+: b_Xtn)
           ((TestEq.$fRead:+:1 @ a_Xtl @ b_Xtn $dRead_X1c0 $dRead1_X1c2)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_Xtl TestEq.:+: b_Xtn))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_Xtl TestEq.:+: b_Xtn))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtl TestEq.:+: b_Xtn)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_Xtl TestEq.:+: b_Xtn]
                :: Text.ParserCombinators.ReadP.ReadP [a_Xtl TestEq.:+: b_Xtn]
                     ~
                   (forall b_a1zG.
                    ([a_Xtl TestEq.:+: b_Xtn] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [a_Xtl TestEq.:+: b_Xtn]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_Xtl TestEq.:+: b_Xtn]))

TestEq.$fRead:+: :: forall a_afL b_afM.
                    (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                    GHC.Read.Read (a_afL TestEq.:+: b_afM)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:+: =
  __inline_me (\ (@ a_Xte)
                 (@ b_Xtg)
                 ($dRead_X1po :: GHC.Read.Read a_Xte)
                 ($dRead1_X1bV :: GHC.Read.Read b_Xtg) ->
                 let {
                   a1_s1Ia [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [a_Xte TestEq.:+: b_Xtg]
                   LclId
                   [Str: DmdType]
                   a1_s1Ia =
                     TestEq.$fRead:+:3 @ a_Xte @ b_Xtg $dRead_X1po $dRead1_X1bV } in
                 let {
                   a2_s1Dh [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   (a_Xte TestEq.:+: b_Xtg)
                   LclId
                   [Str: DmdType]
                   a2_s1Dh =
                     TestEq.$fRead:+:1 @ a_Xte @ b_Xtg $dRead_X1po $dRead1_X1bV } in
                 let {
                   a3_s1nO [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [a_Xte TestEq.:+: b_Xtg]
                   LclId
                   [Str: DmdType]
                   a3_s1nO =
                     TestEq.$fRead:+:_readList
                       @ a_Xte @ b_Xtg $dRead_X1po $dRead1_X1bV } in
                 letrec {
                   $dRead2_s1nK :: GHC.Read.Read (a_Xte TestEq.:+: b_Xtg)
                   LclId
                   [Str: DmdType m]
                   $dRead2_s1nK =
                     GHC.Read.D:Read
                       @ (a_Xte TestEq.:+: b_Xtg)
                       a4_s1nL
                       a3_s1nO
                       (a2_s1Dh
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_Xte TestEq.:+: b_Xtg))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_Xte TestEq.:+: b_Xtg))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_Xte TestEq.:+: b_Xtg)))
                       (a1_s1Ia
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_Xte TestEq.:+: b_Xtg])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_Xte TestEq.:+: b_Xtg])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_Xte TestEq.:+: b_Xtg]));
                   a4_s1nL :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (a_Xte TestEq.:+: b_Xtg)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1nL =
                     GHC.Read.$dmreadsPrec @ (a_Xte TestEq.:+: b_Xtg) $dRead2_s1nK; } in
                 $dRead2_s1nK)

TestEq.$fShow:+:_showsPrec :: forall a_afL b_afM.
                              (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                              GHC.Types.Int -> (a_afL TestEq.:+: b_afM) -> GHC.Show.ShowS
GblId
[Arity 4
 Worker TestEq.$wshowsPrec1
 Str: DmdType LLU(L)S]
TestEq.$fShow:+:_showsPrec =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 (w_s1Ko :: GHC.Show.Show a_afL)
                 (w1_s1Kp :: GHC.Show.Show b_afM)
                 (w2_s1Kq :: GHC.Types.Int)
                 (w3_s1Ku :: a_afL TestEq.:+: b_afM) ->
                 case w2_s1Kq of _ { GHC.Types.I# ww_s1Ks ->
                 TestEq.$wshowsPrec1 @ a_afL @ b_afM w_s1Ko w1_s1Kp ww_s1Ks w3_s1Ku
                 })

TestEq.$fShow:+:_showList :: forall a_afL b_afM.
                             (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                             [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
GblId
[Arity 4
 Str: DmdType LLSL]
TestEq.$fShow:+:_showList =
  \ (@ a_Xtx)
    (@ b_Xtz)
    ($dShow_aXP :: GHC.Show.Show a_Xtx)
    ($dShow1_aXQ :: GHC.Show.Show b_Xtz)
    (eta_B2 :: [a_Xtx TestEq.:+: b_Xtz])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (TestEq.$wshowsPrec1
             @ a_Xtx
             @ b_Xtz
             $dShow_aXP
             $dShow1_aXQ
             0
             x_a1gq
             (let {
                lvl12_s1MP :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1MP =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1MR :: [a_Xtx TestEq.:+: b_Xtz] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MR =
                  \ (ds2_a1gv :: [a_Xtx TestEq.:+: b_Xtz]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1MP;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (TestEq.$wshowsPrec1
                             @ a_Xtx
                             @ b_Xtz
                             $dShow_aXP
                             $dShow1_aXQ
                             0
                             y_a1gA
                             (showl_s1MR ys_a1gB))
                    }; } in
              showl_s1MR xs_a1gr))
    }

TestEq.$fShow:+: :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Show.Show (a_afL TestEq.:+: b_afM)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:+: =
  __inline_me (\ (@ a_Xtw)
                 (@ b_Xty)
                 ($dShow_aXP :: GHC.Show.Show a_Xtw)
                 ($dShow1_aXQ :: GHC.Show.Show b_Xty) ->
                 let {
                   a1_s1nE :: [a_Xtw TestEq.:+: b_Xty] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1nE =
                     TestEq.$fShow:+:_showList
                       @ a_Xtw @ b_Xty $dShow_aXP $dShow1_aXQ } in
                 let {
                   a2_s1nF :: GHC.Types.Int
                              -> (a_Xtw TestEq.:+: b_Xty)
                              -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)S]
                   a2_s1nF =
                     TestEq.$fShow:+:_showsPrec
                       @ a_Xtw @ b_Xty $dShow_aXP $dShow1_aXQ } in
                 letrec {
                   $dShow2_s1nC :: GHC.Show.Show (a_Xtw TestEq.:+: b_Xty)
                   LclId
                   [Str: DmdType m]
                   $dShow2_s1nC =
                     GHC.Show.D:Show @ (a_Xtw TestEq.:+: b_Xty) a2_s1nF a3_s1nD a1_s1nE;
                   a3_s1nD :: (a_Xtw TestEq.:+: b_Xty) -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1nD =
                     GHC.Show.$dmshow @ (a_Xtw TestEq.:+: b_Xty) $dShow2_s1nC; } in
                 $dShow2_s1nC)

TestEq.$fRead:*:_readList :: forall a_afJ b_afK.
                             (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                             Text.ParserCombinators.ReadP.ReadS [a_afJ TestEq.:*: b_afK]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:*:_readList =
  \ (@ a_XtQ)
    (@ b_XtS)
    ($dRead_X1b6 :: GHC.Read.Read a_XtQ)
    ($dRead1_X1b8 :: GHC.Read.Read b_XtS) ->
    Text.ParserCombinators.ReadP.run
      @ [a_XtQ TestEq.:*: b_XtS]
      (((GHC.Read.$dmreadList2
           @ (a_XtQ TestEq.:*: b_XtS)
           ((TestEq.$fRead:*:1 @ a_XtQ @ b_XtS $dRead_X1b6 $dRead1_X1b8)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_XtQ TestEq.:*: b_XtS))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_XtQ TestEq.:*: b_XtS))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_XtQ TestEq.:*: b_XtS)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_XtQ TestEq.:*: b_XtS]
                :: Text.ParserCombinators.ReadP.ReadP [a_XtQ TestEq.:*: b_XtS]
                     ~
                   (forall b_a1zG.
                    ([a_XtQ TestEq.:*: b_XtS] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [a_XtQ TestEq.:*: b_XtS]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_XtQ TestEq.:*: b_XtS]))

TestEq.$fRead:*: :: forall a_afJ b_afK.
                    (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                    GHC.Read.Read (a_afJ TestEq.:*: b_afK)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:*: =
  __inline_me (\ (@ a_XtJ)
                 (@ b_XtL)
                 ($dRead_X1aZ :: GHC.Read.Read a_XtJ)
                 ($dRead1_X1b1 :: GHC.Read.Read b_XtL) ->
                 let {
                   a1_s1Ic [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [a_XtJ TestEq.:*: b_XtL]
                   LclId
                   [Str: DmdType]
                   a1_s1Ic =
                     TestEq.$fRead:*:4 @ a_XtJ @ b_XtL $dRead_X1aZ $dRead1_X1b1 } in
                 let {
                   a2_s1DQ :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (a_XtJ TestEq.:*: b_XtL)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1DQ =
                     TestEq.$fRead:*:1 @ a_XtJ @ b_XtL $dRead_X1aZ $dRead1_X1b1 } in
                 let {
                   a3_s1ns [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [a_XtJ TestEq.:*: b_XtL]
                   LclId
                   [Str: DmdType]
                   a3_s1ns =
                     TestEq.$fRead:*:_readList
                       @ a_XtJ @ b_XtL $dRead_X1aZ $dRead1_X1b1 } in
                 letrec {
                   $dRead2_s1no :: GHC.Read.Read (a_XtJ TestEq.:*: b_XtL)
                   LclId
                   [Str: DmdType m]
                   $dRead2_s1no =
                     GHC.Read.D:Read
                       @ (a_XtJ TestEq.:*: b_XtL)
                       a4_s1np
                       a3_s1ns
                       (a2_s1DQ
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_XtJ TestEq.:*: b_XtL))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_XtJ TestEq.:*: b_XtL))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_XtJ TestEq.:*: b_XtL)))
                       (a1_s1Ic
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_XtJ TestEq.:*: b_XtL])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_XtJ TestEq.:*: b_XtL])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_XtJ TestEq.:*: b_XtL]));
                   a4_s1np :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (a_XtJ TestEq.:*: b_XtL)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1np =
                     GHC.Read.$dmreadsPrec @ (a_XtJ TestEq.:*: b_XtL) $dRead2_s1no; } in
                 $dRead2_s1no)

TestEq.$fShow:*:_showsPrec :: forall a_afJ b_afK.
                              (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                              GHC.Types.Int -> (a_afJ TestEq.:*: b_afK) -> GHC.Show.ShowS
GblId
[Arity 4
 Worker TestEq.$wshowsPrec
 Str: DmdType LLU(L)U(LL)]
TestEq.$fShow:*:_showsPrec =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 (w_s1KA :: GHC.Show.Show a_afJ)
                 (w1_s1KB :: GHC.Show.Show b_afK)
                 (w2_s1KC :: GHC.Types.Int)
                 (w3_s1KG :: a_afJ TestEq.:*: b_afK) ->
                 case w2_s1KC of _ { GHC.Types.I# ww_s1KE ->
                 case w3_s1KG of _ { TestEq.:*: ww1_s1KI ww2_s1KJ ->
                 TestEq.$wshowsPrec
                   @ a_afJ @ b_afK w_s1KA w1_s1KB ww_s1KE ww1_s1KI ww2_s1KJ
                 }
                 })

TestEq.$fShow:*:_showList :: forall a_afJ b_afK.
                             (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                             [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
GblId
[Arity 4
 Str: DmdType LLSL]
TestEq.$fShow:*:_showList =
  \ (@ a_XtZ)
    (@ b_Xu1)
    ($dShow_aWq :: GHC.Show.Show a_XtZ)
    ($dShow1_aWr :: GHC.Show.Show b_Xu1)
    (eta_B2 :: [a_XtZ TestEq.:*: b_Xu1])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.:*: ww_s1KI ww1_s1KJ ->
           TestEq.$wshowsPrec
             @ a_XtZ
             @ b_Xu1
             $dShow_aWq
             $dShow1_aWr
             0
             ww_s1KI
             ww1_s1KJ
             (let {
                lvl12_s1MT :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1MT =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1MV :: [a_XtZ TestEq.:*: b_Xu1] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MV =
                  \ (ds2_a1gv :: [a_XtZ TestEq.:*: b_Xu1]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1MT;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.:*: ww2_X1U5 ww3_X1U7 ->
                           TestEq.$wshowsPrec
                             @ a_XtZ
                             @ b_Xu1
                             $dShow_aWq
                             $dShow1_aWr
                             0
                             ww2_X1U5
                             ww3_X1U7
                             (showl_s1MV ys_a1gB)
                           })
                    }; } in
              showl_s1MV xs_a1gr)
           })
    }

TestEq.$fShow:*: :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Show.Show (a_afJ TestEq.:*: b_afK)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:*: =
  __inline_me (\ (@ a_XtY)
                 (@ b_Xu0)
                 ($dShow_aWq :: GHC.Show.Show a_XtY)
                 ($dShow1_aWr :: GHC.Show.Show b_Xu0) ->
                 let {
                   a1_s1ni :: [a_XtY TestEq.:*: b_Xu0] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1ni =
                     TestEq.$fShow:*:_showList
                       @ a_XtY @ b_Xu0 $dShow_aWq $dShow1_aWr } in
                 let {
                   a2_s1nj :: GHC.Types.Int
                              -> (a_XtY TestEq.:*: b_Xu0)
                              -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(LL)]
                   a2_s1nj =
                     TestEq.$fShow:*:_showsPrec
                       @ a_XtY @ b_Xu0 $dShow_aWq $dShow1_aWr } in
                 letrec {
                   $dShow2_s1ng :: GHC.Show.Show (a_XtY TestEq.:*: b_Xu0)
                   LclId
                   [Str: DmdType m]
                   $dShow2_s1ng =
                     GHC.Show.D:Show @ (a_XtY TestEq.:*: b_Xu0) a2_s1nj a3_s1nh a1_s1ni;
                   a3_s1nh :: (a_XtY TestEq.:*: b_Xu0) -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1nh =
                     GHC.Show.$dmshow @ (a_XtY TestEq.:*: b_Xu0) $dShow2_s1ng; } in
                 $dShow2_s1ng)

TestEq.$fReadC_readList :: forall c_afH a_afI.
                           (GHC.Read.Read a_afI) =>
                           Text.ParserCombinators.ReadP.ReadS [TestEq.C c_afH a_afI]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadC_readList =
  \ (@ c_Xui) (@ a_Xuk) ($dRead_X1ao :: GHC.Read.Read a_Xuk) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.C c_Xui a_Xuk]
      (((GHC.Read.$dmreadList2
           @ (TestEq.C c_Xui a_Xuk)
           ((TestEq.$fReadC1 @ c_Xui @ a_Xuk $dRead_X1ao)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (TestEq.C c_Xui a_Xuk))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xui a_Xuk))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xui a_Xuk)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [TestEq.C c_Xui a_Xuk]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xui a_Xuk]
                     ~
                   (forall b_a1zG.
                    ([TestEq.C c_Xui a_Xuk] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [TestEq.C c_Xui a_Xuk]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.C c_Xui a_Xuk]))

TestEq.$fReadC :: forall c_afH a_afI.
                  (GHC.Read.Read a_afI) =>
                  GHC.Read.Read (TestEq.C c_afH a_afI)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadC =
  __inline_me (\ (@ c_Xuc)
                 (@ a_Xue)
                 ($dRead_X1ai :: GHC.Read.Read a_Xue) ->
                 let {
                   a1_s1Ie [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [TestEq.C c_Xuc a_Xue]
                   LclId
                   [Str: DmdType]
                   a1_s1Ie = TestEq.$fReadC2 @ c_Xuc @ a_Xue $dRead_X1ai } in
                 let {
                   a2_s1Ek :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuc a_Xue)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1Ek = TestEq.$fReadC1 @ c_Xuc @ a_Xue $dRead_X1ai } in
                 let {
                   a3_s1n6 [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.C c_Xuc a_Xue]
                   LclId
                   [Str: DmdType]
                   a3_s1n6 = TestEq.$fReadC_readList @ c_Xuc @ a_Xue $dRead_X1ai } in
                 letrec {
                   $dRead1_s1n2 :: GHC.Read.Read (TestEq.C c_Xuc a_Xue)
                   LclId
                   [Str: DmdType m]
                   $dRead1_s1n2 =
                     GHC.Read.D:Read
                       @ (TestEq.C c_Xuc a_Xue)
                       a4_s1n3
                       a3_s1n6
                       (a2_s1Ek
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (TestEq.C c_Xuc a_Xue))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuc a_Xue))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xuc a_Xue)))
                       (a1_s1Ie
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [TestEq.C c_Xuc a_Xue])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xuc a_Xue])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [TestEq.C c_Xuc a_Xue]));
                   a4_s1n3 :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (TestEq.C c_Xuc a_Xue)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1n3 =
                     GHC.Read.$dmreadsPrec @ (TestEq.C c_Xuc a_Xue) $dRead1_s1n2; } in
                 $dRead1_s1n2)

TestEq.$fShowC_showsPrec :: forall c_afH a_afI.
                            (GHC.Show.Show a_afI) =>
                            GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
GblId
[Arity 3
 Worker TestEq.$wshowsPrec2
 Str: DmdType LU(L)U(L)]
TestEq.$fShowC_showsPrec =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 (w_s1KP :: GHC.Show.Show a_afI)
                 (w1_s1KQ :: GHC.Types.Int)
                 (w2_s1KU :: TestEq.C c_afH a_afI) ->
                 case w1_s1KQ of _ { GHC.Types.I# ww_s1KS ->
                 case w2_s1KU of _ { TestEq.C ww1_s1KW ->
                 TestEq.$wshowsPrec2 @ c_afH @ a_afI w_s1KP ww_s1KS ww1_s1KW
                 }
                 })

TestEq.$fShowC_showList :: forall c_afH a_afI.
                           (GHC.Show.Show a_afI) =>
                           [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowC_showList =
  \ (@ c_Xuq)
    (@ a_Xus)
    ($dShow_aVm :: GHC.Show.Show a_Xus)
    (eta_B2 :: [TestEq.C c_Xuq a_Xus])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.C ww_s1KW ->
           TestEq.$wshowsPrec2
             @ c_Xuq
             @ a_Xus
             $dShow_aVm
             0
             ww_s1KW
             (let {
                lvl12_s1MX :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1MX =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1MZ :: [TestEq.C c_Xuq a_Xus] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1MZ =
                  \ (ds2_a1gv :: [TestEq.C c_Xuq a_Xus]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1MX;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.C ww1_X1Um ->
                           TestEq.$wshowsPrec2
                             @ c_Xuq @ a_Xus $dShow_aVm 0 ww1_X1Um (showl_s1MZ ys_a1gB)
                           })
                    }; } in
              showl_s1MZ xs_a1gr)
           })
    }

TestEq.$fShowC :: forall c_afH a_afI.
                  (GHC.Show.Show a_afI) =>
                  GHC.Show.Show (TestEq.C c_afH a_afI)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowC =
  __inline_me (\ (@ c_Xup)
                 (@ a_Xur)
                 ($dShow_aVm :: GHC.Show.Show a_Xur) ->
                 let {
                   a1_s1mW :: [TestEq.C c_Xup a_Xur] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1mW = TestEq.$fShowC_showList @ c_Xup @ a_Xur $dShow_aVm } in
                 let {
                   a2_s1mX :: GHC.Types.Int -> TestEq.C c_Xup a_Xur -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a2_s1mX = TestEq.$fShowC_showsPrec @ c_Xup @ a_Xur $dShow_aVm } in
                 letrec {
                   $dShow1_s1mU :: GHC.Show.Show (TestEq.C c_Xup a_Xur)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1mU =
                     GHC.Show.D:Show @ (TestEq.C c_Xup a_Xur) a2_s1mX a3_s1mV a1_s1mW;
                   a3_s1mV :: TestEq.C c_Xup a_Xur -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1mV =
                     GHC.Show.$dmshow @ (TestEq.C c_Xup a_Xur) $dShow1_s1mU; } in
                 $dShow1_s1mU)

TestEq.$fReadVar_readList :: forall a_afG.
                             (GHC.Read.Read a_afG) =>
                             Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadVar_readList =
  \ (@ a_XuG) ($dRead_X19I :: GHC.Read.Read a_XuG) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Var a_XuG]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Var a_XuG)
           ((TestEq.$fReadVar1 @ a_XuG $dRead_X19I)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuG))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuG))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuG)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Var a_XuG]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuG]
                     ~
                   (forall b_a1zG.
                    ([TestEq.Var a_XuG] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [TestEq.Var a_XuG]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Var a_XuG]))

TestEq.$fReadVar :: forall a_afG.
                    (GHC.Read.Read a_afG) =>
                    GHC.Read.Read (TestEq.Var a_afG)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadVar =
  __inline_me (\ (@ a_XuB) ($dRead_X19D :: GHC.Read.Read a_XuB) ->
                 let {
                   a1_s1Ig [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [TestEq.Var a_XuB]
                   LclId
                   [Str: DmdType]
                   a1_s1Ig = TestEq.$fReadVar3 @ a_XuB $dRead_X19D } in
                 let {
                   a2_s1EG :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuB)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1EG = TestEq.$fReadVar1 @ a_XuB $dRead_X19D } in
                 let {
                   a3_s1mK [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.Var a_XuB]
                   LclId
                   [Str: DmdType]
                   a3_s1mK = TestEq.$fReadVar_readList @ a_XuB $dRead_X19D } in
                 letrec {
                   $dRead1_s1mG :: GHC.Read.Read (TestEq.Var a_XuB)
                   LclId
                   [Str: DmdType m]
                   $dRead1_s1mG =
                     GHC.Read.D:Read
                       @ (TestEq.Var a_XuB)
                       a4_s1mH
                       a3_s1mK
                       (a2_s1EG
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuB))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuB))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuB)))
                       (a1_s1Ig
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Var a_XuB])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuB])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Var a_XuB]));
                   a4_s1mH :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (TestEq.Var a_XuB)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1mH =
                     GHC.Read.$dmreadsPrec @ (TestEq.Var a_XuB) $dRead1_s1mG; } in
                 $dRead1_s1mG)

TestEq.$fShowVar_showsPrec :: forall a_afG.
                              (GHC.Show.Show a_afG) =>
                              GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
GblId
[Arity 3
 Worker TestEq.$wshowsPrec4
 Str: DmdType LU(L)U(L)]
TestEq.$fShowVar_showsPrec =
  __inline_me (\ (@ a_afG)
                 (w_s1L2 :: GHC.Show.Show a_afG)
                 (w1_s1L3 :: GHC.Types.Int)
                 (w2_s1L7 :: TestEq.Var a_afG) ->
                 case w1_s1L3 of _ { GHC.Types.I# ww_s1L5 ->
                 case w2_s1L7 of _ { TestEq.Var ww1_s1L9 ->
                 TestEq.$wshowsPrec4 @ a_afG w_s1L2 ww_s1L5 ww1_s1L9
                 }
                 })

TestEq.$fShowVar_showList :: forall a_afG.
                             (GHC.Show.Show a_afG) =>
                             [TestEq.Var a_afG] -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowVar_showList =
  \ (@ a_XuO)
    ($dShow_aUi :: GHC.Show.Show a_XuO)
    (eta_B2 :: [TestEq.Var a_XuO])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Var ww_s1L9 ->
           TestEq.$wshowsPrec4
             @ a_XuO
             $dShow_aUi
             0
             ww_s1L9
             (let {
                lvl12_s1N1 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1N1 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1N3 :: [TestEq.Var a_XuO] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1N3 =
                  \ (ds2_a1gv :: [TestEq.Var a_XuO]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1N1;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Var ww1_X1UD ->
                           TestEq.$wshowsPrec4
                             @ a_XuO $dShow_aUi 0 ww1_X1UD (showl_s1N3 ys_a1gB)
                           })
                    }; } in
              showl_s1N3 xs_a1gr)
           })
    }

TestEq.$fShowVar :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Show.Show (TestEq.Var a_afG)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowVar =
  __inline_me (\ (@ a_XuN) ($dShow_aUi :: GHC.Show.Show a_XuN) ->
                 let {
                   a1_s1mA :: [TestEq.Var a_XuN] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1mA = TestEq.$fShowVar_showList @ a_XuN $dShow_aUi } in
                 let {
                   a2_s1mB :: GHC.Types.Int -> TestEq.Var a_XuN -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a2_s1mB = TestEq.$fShowVar_showsPrec @ a_XuN $dShow_aUi } in
                 letrec {
                   $dShow1_s1my :: GHC.Show.Show (TestEq.Var a_XuN)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1my =
                     GHC.Show.D:Show @ (TestEq.Var a_XuN) a2_s1mB a3_s1mz a1_s1mA;
                   a3_s1mz :: TestEq.Var a_XuN -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1mz = GHC.Show.$dmshow @ (TestEq.Var a_XuN) $dShow1_s1my; } in
                 $dShow1_s1my)

TestEq.$fReadRec_readList :: forall a_afF.
                             (GHC.Read.Read a_afF) =>
                             Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadRec_readList =
  \ (@ a_Xv4) ($dRead_X193 :: GHC.Read.Read a_Xv4) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Rec a_Xv4]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Rec a_Xv4)
           ((TestEq.$fReadRec1 @ a_Xv4 $dRead_X193)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xv4))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xv4))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xv4)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Rec a_Xv4]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xv4]
                     ~
                   (forall b_a1zG.
                    ([TestEq.Rec a_Xv4] -> Text.ParserCombinators.ReadP.P b_a1zG)
                    -> Text.ParserCombinators.ReadP.P b_a1zG)))
         @ [TestEq.Rec a_Xv4]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Rec a_Xv4]))

TestEq.$fReadRec :: forall a_afF.
                    (GHC.Read.Read a_afF) =>
                    GHC.Read.Read (TestEq.Rec a_afF)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadRec =
  __inline_me (\ (@ a_XuZ) ($dRead_X18Y :: GHC.Read.Read a_XuZ) ->
                 let {
                   a1_s1Ii [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [TestEq.Rec a_XuZ]
                   LclId
                   [Str: DmdType]
                   a1_s1Ii = TestEq.$fReadRec3 @ a_XuZ $dRead_X18Y } in
                 let {
                   a2_s1F2 :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_XuZ)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1F2 = TestEq.$fReadRec1 @ a_XuZ $dRead_X18Y } in
                 let {
                   a3_s1mo [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.Rec a_XuZ]
                   LclId
                   [Str: DmdType]
                   a3_s1mo = TestEq.$fReadRec_readList @ a_XuZ $dRead_X18Y } in
                 letrec {
                   $dRead1_s1mk :: GHC.Read.Read (TestEq.Rec a_XuZ)
                   LclId
                   [Str: DmdType m]
                   $dRead1_s1mk =
                     GHC.Read.D:Read
                       @ (TestEq.Rec a_XuZ)
                       a4_s1ml
                       a3_s1mo
                       (a2_s1F2
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_XuZ))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_XuZ))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_XuZ)))
                       (a1_s1Ii
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Rec a_XuZ])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_XuZ])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Rec a_XuZ]));
                   a4_s1ml :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (TestEq.Rec a_XuZ)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1ml =
                     GHC.Read.$dmreadsPrec @ (TestEq.Rec a_XuZ) $dRead1_s1mk; } in
                 $dRead1_s1mk)

TestEq.$fShowRec_showsPrec :: forall a_afF.
                              (GHC.Show.Show a_afF) =>
                              GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
GblId
[Arity 3
 Worker TestEq.$wshowsPrec3
 Str: DmdType LU(L)U(L)]
TestEq.$fShowRec_showsPrec =
  __inline_me (\ (@ a_afF)
                 (w_s1Lf :: GHC.Show.Show a_afF)
                 (w1_s1Lg :: GHC.Types.Int)
                 (w2_s1Lk :: TestEq.Rec a_afF) ->
                 case w1_s1Lg of _ { GHC.Types.I# ww_s1Li ->
                 case w2_s1Lk of _ { TestEq.Rec ww1_s1Lm ->
                 TestEq.$wshowsPrec3 @ a_afF w_s1Lf ww_s1Li ww1_s1Lm
                 }
                 })

TestEq.$fShowRec_showList :: forall a_afF.
                             (GHC.Show.Show a_afF) =>
                             [TestEq.Rec a_afF] -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowRec_showList =
  \ (@ a_Xvc)
    ($dShow_aTe :: GHC.Show.Show a_Xvc)
    (eta_B2 :: [TestEq.Rec a_Xvc])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Rec ww_s1Lm ->
           TestEq.$wshowsPrec3
             @ a_Xvc
             $dShow_aTe
             0
             ww_s1Lm
             (let {
                lvl12_s1N5 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1N5 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1N7 :: [TestEq.Rec a_Xvc] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1N7 =
                  \ (ds2_a1gv :: [TestEq.Rec a_Xvc]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1N5;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Rec ww1_X1UV ->
                           TestEq.$wshowsPrec3
                             @ a_Xvc $dShow_aTe 0 ww1_X1UV (showl_s1N7 ys_a1gB)
                           })
                    }; } in
              showl_s1N7 xs_a1gr)
           })
    }

TestEq.$fShowRec :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Show.Show (TestEq.Rec a_afF)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowRec =
  __inline_me (\ (@ a_Xvb) ($dShow_aTe :: GHC.Show.Show a_Xvb) ->
                 let {
                   a1_s1me :: [TestEq.Rec a_Xvb] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1me = TestEq.$fShowRec_showList @ a_Xvb $dShow_aTe } in
                 let {
                   a2_s1mf :: GHC.Types.Int -> TestEq.Rec a_Xvb -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a2_s1mf = TestEq.$fShowRec_showsPrec @ a_Xvb $dShow_aTe } in
                 letrec {
                   $dShow1_s1mc :: GHC.Show.Show (TestEq.Rec a_Xvb)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1mc =
                     GHC.Show.D:Show @ (TestEq.Rec a_Xvb) a2_s1mf a3_s1md a1_s1me;
                   a3_s1md :: TestEq.Rec a_Xvb -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1md = GHC.Show.$dmshow @ (TestEq.Rec a_Xvb) $dShow1_s1mc; } in
                 $dShow1_s1mc)

TestEq.$fReadFixity_readListDefault :: Text.ParserCombinators.ReadP.ReadS
                                         [TestEq.Fixity]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [TestEq.Fixity] TestEq.$fReadFixity8

TestEq.$fReadFixity :: GHC.Read.Read TestEq.Fixity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fReadFixity =
  GHC.Read.D:Read
    @ TestEq.Fixity
    TestEq.$fReadFixity9
    TestEq.$fReadFixity_readListDefault
    (TestEq.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (TestEq.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Fixity]))

TestEq.$fOrdFixity_compare :: TestEq.Fixity
                              -> TestEq.Fixity
                              -> GHC.Ordering.Ordering
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity_compare =
  \ (a1_axy :: TestEq.Fixity) (b_axz :: TestEq.Fixity) ->
    let {
      $j_s1Nb :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      LclId
      [Arity 1
       Str: DmdType L]
      $j_s1Nb =
        \ (a#_axF :: GHC.Prim.Int#) ->
          let {
            $j1_s1N9 :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            LclId
            [Arity 1
             Str: DmdType L]
            $j1_s1N9 =
              \ (b#_axG :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_axF b#_axG of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_axF b#_axG of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a1_axy of _ {
                      TestEq.Prefix -> GHC.Ordering.EQ;
                      TestEq.Infix a11_axB a2_axC ->
                        case b_axz of _ {
                          TestEq.Prefix -> GHC.Ordering.EQ;
                          TestEq.Infix b1_axD b2_axE ->
                            case a11_axB of _ {
                              TestEq.LeftAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.RightAssociative -> GHC.Ordering.LT;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.RightAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Ordering.GT;
                                  TestEq.RightAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.NotAssociative ->
                                case b1_axD of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  TestEq.NotAssociative -> GHC.Base.compareInt a2_axC b2_axE
                                }
                            }
                        }
                    }
                } } in
          case b_axz of _ {
            TestEq.Prefix -> $j1_s1N9 0; TestEq.Infix _ _ -> $j1_s1N9 1
          } } in
    case a1_axy of _ {
      TestEq.Prefix -> $j_s1Nb 0; TestEq.Infix _ _ -> $j_s1Nb 1
    }

TestEq.$fOrdFixity6 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity6 =
  \ (x_a1j9 :: TestEq.Fixity) (y_a1ja :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1j9 y_a1ja of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

TestEq.$fOrdFixity5 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity5 =
  \ (x_a1jq :: TestEq.Fixity) (y_a1jr :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1jq y_a1jr of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

TestEq.$fOrdFixity4 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity4 =
  \ (x_a1jH :: TestEq.Fixity) (y_a1jI :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1jH y_a1jI of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

TestEq.$fOrdFixity3 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity3 =
  \ (x_a1jY :: TestEq.Fixity) (y_a1jZ :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1jY y_a1jZ of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

TestEq.$fOrdFixity2 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> TestEq.Fixity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity2 =
  \ (x_a1kf :: TestEq.Fixity) (y_a1kg :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1kf y_a1kg of _ {
      __DEFAULT -> y_a1kg; GHC.Ordering.GT -> x_a1kf
    }

TestEq.$fOrdFixity1 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> TestEq.Fixity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity1 =
  \ (x_a1kz :: TestEq.Fixity) (y_a1kA :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1kz y_a1kA of _ {
      __DEFAULT -> x_a1kz; GHC.Ordering.GT -> y_a1kA
    }

TestEq.$fShowFixity_showsPrec :: GHC.Types.Int
                                 -> TestEq.Fixity
                                 -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowFixity_showsPrec =
  \ (ds_d1ba :: GHC.Types.Int)
    (ds1_d1bb :: TestEq.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_d1bb of _ {
      TestEq.Prefix ->
        GHC.Base.++ @ GHC.Types.Char TestEq.$fReadFixity7 eta_B1;
      TestEq.Infix b1_axw b2_axx ->
        case ds_d1ba of _ { GHC.Types.I# x_a1Do ->
        let {
          p_s1Nd :: GHC.Show.ShowS
          LclId
          [Arity 1
           Str: DmdType L]
          p_s1Nd =
            \ (x1_X1yO :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                TestEq.$fShowFixity2
                (case b1_axw of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       TestEq.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1GR ->
                           GHC.Show.$wshowSignedInt 11 ww_a1GR x1_X1yO
                           }));
                   TestEq.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       TestEq.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1GR ->
                           GHC.Show.$wshowSignedInt 11 ww_a1GR x1_X1yO
                           }));
                   TestEq.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       TestEq.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1GR ->
                           GHC.Show.$wshowSignedInt 11 ww_a1GR x1_X1yO
                           }))
                 }) } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False -> p_s1Nd eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.showSignedInt2
              (p_s1Nd
                 (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 eta_B1))
        }
        }
    }

TestEq.$fShowFixity_showList :: [TestEq.Fixity] -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowFixity_showList =
  \ (ds1_a1gk :: [TestEq.Fixity]) (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (TestEq.$fShowFixity_showsPrec
             TestEq.$fShowFixity1
             x_a1gq
             (let {
                lvl12_s1Nf :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1Nf =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1Nh :: [TestEq.Fixity] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1Nh =
                  \ (ds2_a1gv :: [TestEq.Fixity]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1Nf;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (TestEq.$fShowFixity_showsPrec
                             TestEq.$fShowFixity1 y_a1gA (showl_s1Nh ys_a1gB))
                    }; } in
              showl_s1Nh xs_a1gr))
    }

TestEq.$fShowFixity3 :: TestEq.Fixity -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType S]
TestEq.$fShowFixity3 =
  \ (x_X1xA :: TestEq.Fixity) ->
    TestEq.$fShowFixity_showsPrec
      GHC.Base.zeroInt x_X1xA (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowFixity :: GHC.Show.Show TestEq.Fixity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowFixity =
  GHC.Show.D:Show
    @ TestEq.Fixity
    TestEq.$fShowFixity_showsPrec
    TestEq.$fShowFixity3
    TestEq.$fShowFixity_showList

TestEq.$fEqFixity_== :: TestEq.Fixity
                        -> TestEq.Fixity
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqFixity_== =
  \ (ds_d1b4 :: TestEq.Fixity) (ds1_d1b5 :: TestEq.Fixity) ->
    case ds_d1b4 of _ {
      TestEq.Prefix ->
        case ds1_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.True; TestEq.Infix _ _ -> GHC.Bool.False
        };
      TestEq.Infix a1_axl a2_axm ->
        case ds1_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.False;
          TestEq.Infix b1_axn b2_axo ->
            case a1_axl of _ {
              TestEq.LeftAssociative ->
                case b1_axn of _ {
                  TestEq.LeftAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1ya ->
                    case b2_axo of _ { GHC.Types.I# y_a1ye ->
                    GHC.Prim.==# x_a1ya y_a1ye
                    }
                    };
                  TestEq.RightAssociative -> GHC.Bool.False;
                  TestEq.NotAssociative -> GHC.Bool.False
                };
              TestEq.RightAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.RightAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1ya ->
                    case b2_axo of _ { GHC.Types.I# y_a1ye ->
                    GHC.Prim.==# x_a1ya y_a1ye
                    }
                    }
                };
              TestEq.NotAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.NotAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1ya ->
                    case b2_axo of _ { GHC.Types.I# y_a1ye ->
                    GHC.Prim.==# x_a1ya y_a1ye
                    }
                    }
                }
            }
        }
    }

TestEq.$fEqFixity_/= :: TestEq.Fixity
                        -> TestEq.Fixity
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqFixity_/= =
  \ (a1_axt :: TestEq.Fixity) (b_axu :: TestEq.Fixity) ->
    case TestEq.$fEqFixity_== a1_axt b_axu of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqFixity :: GHC.Classes.Eq TestEq.Fixity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fEqFixity =
  GHC.Classes.D:Eq
    @ TestEq.Fixity TestEq.$fEqFixity_== TestEq.$fEqFixity_/=

TestEq.$fOrdFixity :: GHC.Classes.Ord TestEq.Fixity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fOrdFixity =
  GHC.Classes.D:Ord
    @ TestEq.Fixity
    TestEq.$fEqFixity
    TestEq.$fOrdFixity_compare
    TestEq.$fOrdFixity6
    TestEq.$fOrdFixity5
    TestEq.$fOrdFixity4
    TestEq.$fOrdFixity3
    TestEq.$fOrdFixity2
    TestEq.$fOrdFixity1

TestEq.$fReadAssociativity_readListDefault :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.Associativity]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [TestEq.Associativity] TestEq.$fReadAssociativity16

TestEq.$fReadAssociativity :: GHC.Read.Read TestEq.Associativity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fReadAssociativity =
  GHC.Read.D:Read
    @ TestEq.Associativity
    TestEq.$fReadAssociativity17
    TestEq.$fReadAssociativity_readListDefault
    (TestEq.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
    (TestEq.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [TestEq.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Associativity]))

TestEq.$fOrdAssociativity_compare :: TestEq.Associativity
                                     -> TestEq.Associativity
                                     -> GHC.Ordering.Ordering
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity_compare =
  \ (a1_axg :: TestEq.Associativity)
    (b_axh :: TestEq.Associativity) ->
    case a1_axg of _ {
      TestEq.LeftAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.EQ;
          TestEq.RightAssociative -> GHC.Ordering.LT;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.RightAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.GT;
          TestEq.RightAssociative -> GHC.Ordering.EQ;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.NotAssociative ->
        case b_axh of _ {
          __DEFAULT -> GHC.Ordering.GT;
          TestEq.NotAssociative -> GHC.Ordering.EQ
        }
    }

TestEq.$fShowAssociativity_showList :: [TestEq.Associativity]
                                       -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowAssociativity_showList =
  \ (ds1_a1gk :: [TestEq.Associativity])
    (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1Nn [ALWAYS Just L] :: GHC.Base.String
             LclId
             [Str: DmdType]
             eta_s1Nn =
               let {
                 lvl12_s1Nj :: [GHC.Types.Char]
                 LclId
                 [Str: DmdType]
                 lvl12_s1Nj =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
               letrec {
                 showl_s1Nl :: [TestEq.Associativity] -> [GHC.Types.Char]
                 LclId
                 [Arity 1
                  Str: DmdType S]
                 showl_s1Nl =
                   \ (ds2_a1gv :: [TestEq.Associativity]) ->
                     case ds2_a1gv of _ {
                       [] -> lvl12_s1Nj;
                       : y_a1gA ys_a1gB ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_a1gA of _ {
                              TestEq.LeftAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char
                                  TestEq.$fReadAssociativity15
                                  (showl_s1Nl ys_a1gB);
                              TestEq.RightAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char
                                  TestEq.$fReadAssociativity12
                                  (showl_s1Nl ys_a1gB);
                              TestEq.NotAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char TestEq.$fReadAssociativity9 (showl_s1Nl ys_a1gB)
                            })
                     }; } in
               showl_s1Nl xs_a1gr } in
           case x_a1gq of _ {
             TestEq.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity15 eta_s1Nn;
             TestEq.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity12 eta_s1Nn;
             TestEq.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity9 eta_s1Nn
           })
    }

TestEq.$fShowAssociativity_showsPrec :: GHC.Types.Int
                                        -> TestEq.Associativity
                                        -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType ASL]
TestEq.$fShowAssociativity_showsPrec =
  __inline_me (\ _
                 (ds1_d1aU :: TestEq.Associativity)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds1_d1aU of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity15 eta_B1;
                   TestEq.RightAssociative ->
                     GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity12 eta_B1;
                   TestEq.NotAssociative ->
                     GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity9 eta_B1
                 })

TestEq.$fShowAssociativity :: GHC.Show.Show TestEq.Associativity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowAssociativity =
  GHC.Show.D:Show
    @ TestEq.Associativity
    TestEq.$fShowAssociativity_showsPrec
    TestEq.$fShowAssociativity1
    TestEq.$fShowAssociativity_showList

TestEq.$fEqAssociativity_/= :: TestEq.Associativity
                               -> TestEq.Associativity
                               -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqAssociativity_/= =
  \ (a1_axe :: TestEq.Associativity)
    (b_axf :: TestEq.Associativity) ->
    case a1_axe of _ {
      TestEq.LeftAssociative ->
        case b_axf of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

TestEq.$fEqAssociativity_== :: TestEq.Associativity
                               -> TestEq.Associativity
                               -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqAssociativity_== =
  \ (a1_axa :: TestEq.Associativity)
    (b_axb :: TestEq.Associativity) ->
    case a1_axa of _ {
      TestEq.LeftAssociative ->
        case b_axb of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

TestEq.$fEqAssociativity :: GHC.Classes.Eq TestEq.Associativity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ TestEq.Associativity
    TestEq.$fEqAssociativity_==
    TestEq.$fEqAssociativity_/=

TestEq.$fOrdAssociativity :: GHC.Classes.Ord TestEq.Associativity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ TestEq.Associativity
    TestEq.$fEqAssociativity
    TestEq.$fOrdAssociativity_compare
    TestEq.$fOrdAssociativity6
    TestEq.$fOrdAssociativity5
    TestEq.$fOrdAssociativity4
    TestEq.$fOrdAssociativity3
    TestEq.$fOrdAssociativity2
    TestEq.$fOrdAssociativity1

Rec {
TestEq.$fEqLogic0_== :: TestEq.Logic
                        -> TestEq.Logic
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqLogic0_== =
  \ (ds_d1aD :: TestEq.Logic) (ds1_d1aE :: TestEq.Logic) ->
    let {
      $wfail_s1Np :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1
       Str: DmdType A]
      $wfail_s1Np =
        \ _ ->
          case ds_d1aD of _ {
            TestEq.VarL _ ->
              case ds1_d1aE of _ {
                TestEq.VarL _ -> GHC.Bool.True;
                TestEq.T -> GHC.Bool.False;
                TestEq.F -> GHC.Bool.False;
                TestEq.Not _ -> GHC.Bool.False;
                TestEq.Impl _ _ -> GHC.Bool.False;
                TestEq.Equiv _ _ -> GHC.Bool.False;
                TestEq.Conj _ _ -> GHC.Bool.False;
                TestEq.Disj _ _ -> GHC.Bool.False
              };
            TestEq.T ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.T -> GHC.Bool.True
              };
            TestEq.F ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.F -> GHC.Bool.True
              };
            TestEq.Not _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Not _ -> GHC.Bool.True
              };
            TestEq.Impl _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Impl _ _ -> GHC.Bool.True
              };
            TestEq.Equiv _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Equiv _ _ -> GHC.Bool.True
              };
            TestEq.Conj _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Conj _ _ -> GHC.Bool.True
              };
            TestEq.Disj _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Disj _ _ -> GHC.Bool.True
              }
          } } in
    case ds_d1aD of _ {
      __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
      TestEq.VarL a1_awK ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.VarL b1_awL -> GHC.Base.eqString a1_awK b1_awL
        };
      TestEq.Not a1_awM ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Not b1_awN -> TestEq.$fEqLogic0_== a1_awM b1_awN
        };
      TestEq.Impl a1_awO a2_awP ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Impl b1_awQ b2_awR ->
            case TestEq.$fEqLogic0_== a1_awO b1_awQ of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_awP b2_awR
            }
        };
      TestEq.Equiv a1_awS a2_awT ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Equiv b1_awU b2_awV ->
            case TestEq.$fEqLogic0_== a1_awS b1_awU of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_awT b2_awV
            }
        };
      TestEq.Conj a1_awW a2_awX ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Conj b1_awY b2_awZ ->
            case TestEq.$fEqLogic0_== a1_awW b1_awY of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_awX b2_awZ
            }
        };
      TestEq.Disj a1_ax0 a2_ax1 ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Np GHC.Prim.realWorld#;
          TestEq.Disj b1_ax2 b2_ax3 ->
            case TestEq.$fEqLogic0_== a1_ax0 b1_ax2 of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_ax1 b2_ax3
            }
        }
    }
end Rec }

TestEq.$fEqLogic0_/= :: TestEq.Logic
                        -> TestEq.Logic
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqLogic0_/= =
  \ (a1_ax8 :: TestEq.Logic) (b_ax9 :: TestEq.Logic) ->
    case TestEq.$fEqLogic0_== a1_ax8 b_ax9 of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqLogic0 :: GHC.Classes.Eq TestEq.Logic
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fEqLogic0 =
  GHC.Classes.D:Eq
    @ TestEq.Logic TestEq.$fEqLogic0_== TestEq.$fEqLogic0_/=

Rec {
TestEq.$fShowLogic_$sshowsPrec :: TestEq.Logic
                                  -> GHC.Prim.Int#
                                  -> GHC.Base.String
                                  -> [GHC.Types.Char]
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowLogic_$sshowsPrec =
  \ (sc_s1Qy :: TestEq.Logic) (sc1_s1Qz :: GHC.Prim.Int#) ->
    case sc_s1Qy of _ {
      TestEq.VarL b1_awv ->
        case GHC.Prim.>=# sc1_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl9_r1VT
                (GHC.Show.$fShowChar_showList b1_awv x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl9_r1VT
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
        };
      TestEq.T -> lvl8_r1VR;
      TestEq.F -> lvl7_r1VN;
      TestEq.Not b1_awx ->
        let {
          g_s1Nr [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nr = TestEq.$fShowLogic_$sshowsPrec b1_awx 11 } in
        case GHC.Prim.>=# sc1_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl6_r1VJ (g_s1Nr x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl6_r1VJ
                   (g_s1Nr
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.Impl b1_awz b2_awA ->
        let {
          g_s1Nv [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nv = TestEq.$fShowLogic_$sshowsPrec b2_awA 11 } in
        let {
          f_s1Nt [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nt = TestEq.$fShowLogic_$sshowsPrec b1_awz 11 } in
        case GHC.Prim.>=# sc1_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl5_r1VH
                (f_s1Nt
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nv x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl5_r1VH
                   (f_s1Nt
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nv
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Equiv b1_awC b2_awD ->
        let {
          g_s1Nz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nz = TestEq.$fShowLogic_$sshowsPrec b2_awD 11 } in
        let {
          f_s1Nx [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nx = TestEq.$fShowLogic_$sshowsPrec b1_awC 11 } in
        case GHC.Prim.>=# sc1_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl4_r1VF
                (f_s1Nx
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nz x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl4_r1VF
                   (f_s1Nx
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nz
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Conj b1_awF b2_awG ->
        let {
          g_s1ND [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1ND = TestEq.$fShowLogic_$sshowsPrec b2_awG 11 } in
        let {
          f_s1NB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NB = TestEq.$fShowLogic_$sshowsPrec b1_awF 11 } in
        case GHC.Prim.>=# sc1_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl3_r1VD
                (f_s1NB
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1ND x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl3_r1VD
                   (f_s1NB
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1ND
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Disj b1_awI b2_awJ ->
        let {
          g_s1NH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1NH = TestEq.$fShowLogic_$sshowsPrec b2_awJ 11 } in
        let {
          f_s1NF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NF = TestEq.$fShowLogic_$sshowsPrec b1_awI 11 } in
        case GHC.Prim.>=# sc1_s1Qz 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl2_r1VB
                (f_s1NF
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1NH x_X1Aa)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl2_r1VB
                   (f_s1NF
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1NH
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
    }
end Rec }

TestEq.$fShowLogic_showsPrec :: GHC.Types.Int
                                -> TestEq.Logic
                                -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType LS]
TestEq.$fShowLogic_showsPrec =
  \ (a1_awu :: GHC.Types.Int) (ds_d1au :: TestEq.Logic) ->
    case ds_d1au of _ {
      TestEq.VarL b1_awv ->
        case a1_awu of _ { GHC.Types.I# x_a1Do ->
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x1_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl9_r1VT
                (GHC.Show.$fShowChar_showList b1_awv x1_a1hi);
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl9_r1VT
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb))))
        }
        };
      TestEq.T -> lvl8_r1VR;
      TestEq.F -> lvl7_r1VN;
      TestEq.Not b1_awx ->
        case a1_awu of _ { GHC.Types.I# x_a1Do ->
        let {
          g_s1Nr [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nr = TestEq.$fShowLogic_$sshowsPrec b1_awx 11 } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x1_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl6_r1VJ (g_s1Nr x1_a1hi);
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl6_r1VJ
                   (g_s1Nr
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))
        }
        };
      TestEq.Impl b1_awz b2_awA ->
        case a1_awu of _ { GHC.Types.I# x_a1Do ->
        let {
          g_s1Nv [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nv = TestEq.$fShowLogic_$sshowsPrec b2_awA 11 } in
        let {
          f_s1Nt [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nt = TestEq.$fShowLogic_$sshowsPrec b1_awz 11 } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl5_r1VH
                (f_s1Nt
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nv x1_X1Aa)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl5_r1VH
                   (f_s1Nt
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nv
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        };
      TestEq.Equiv b1_awC b2_awD ->
        case a1_awu of _ { GHC.Types.I# x_a1Do ->
        let {
          g_s1Nz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nz = TestEq.$fShowLogic_$sshowsPrec b2_awD 11 } in
        let {
          f_s1Nx [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Nx = TestEq.$fShowLogic_$sshowsPrec b1_awC 11 } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl4_r1VF
                (f_s1Nx
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Nz x1_X1Aa)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl4_r1VF
                   (f_s1Nx
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Nz
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        };
      TestEq.Conj b1_awF b2_awG ->
        case a1_awu of _ { GHC.Types.I# x_a1Do ->
        let {
          g_s1ND [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1ND = TestEq.$fShowLogic_$sshowsPrec b2_awG 11 } in
        let {
          f_s1NB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NB = TestEq.$fShowLogic_$sshowsPrec b1_awF 11 } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl3_r1VD
                (f_s1NB
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1ND x1_X1Aa)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl3_r1VD
                   (f_s1NB
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1ND
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        };
      TestEq.Disj b1_awI b2_awJ ->
        case a1_awu of _ { GHC.Types.I# x_a1Do ->
        let {
          g_s1NH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1NH = TestEq.$fShowLogic_$sshowsPrec b2_awJ 11 } in
        let {
          f_s1NF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1NF = TestEq.$fShowLogic_$sshowsPrec b1_awI 11 } in
        case GHC.Prim.>=# x_a1Do 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Aa :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl2_r1VB
                (f_s1NF
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1NH x1_X1Aa)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl2_r1VB
                   (f_s1NF
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1NH
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        }
    }

TestEq.$fShowLogic_showList :: [TestEq.Logic] -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowLogic_showList =
  \ (ds1_a1gk :: [TestEq.Logic]) (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (TestEq.$fShowLogic_$sshowsPrec
             x_a1gq
             0
             (let {
                lvl12_s1NJ :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NJ =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1NL :: [TestEq.Logic] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NL =
                  \ (ds2_a1gv :: [TestEq.Logic]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NJ;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (TestEq.$fShowLogic_$sshowsPrec y_a1gA 0 (showl_s1NL ys_a1gB))
                    }; } in
              showl_s1NL xs_a1gr))
    }

TestEq.$fShowLogic1 :: TestEq.Logic -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType S]
TestEq.$fShowLogic1 =
  \ (x_X1yM :: TestEq.Logic) ->
    TestEq.$fShowLogic_showsPrec
      GHC.Base.zeroInt x_X1yM (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowLogic :: GHC.Show.Show TestEq.Logic
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowLogic =
  GHC.Show.D:Show
    @ TestEq.Logic
    TestEq.$fShowLogic_showsPrec
    TestEq.$fShowLogic1
    TestEq.$fShowLogic_showList

Rec {
TestEq.$fEqTree0_== :: forall a_afw.
                       (GHC.Classes.Eq a_afw) =>
                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEqTree0_== =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1NN :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1NN = TestEq.$fEqTree0_== @ a_afw $dEq_aGJ } in
    \ (ds_d1ao :: TestEq.Tree a_afw) (ds1_d1ap :: TestEq.Tree a_afw) ->
      case ds_d1ao of _ {
        TestEq.Leaf ->
          case ds1_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
          };
        TestEq.Bin a1_awi a2_awj a3_awk ->
          case ds1_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.False;
            TestEq.Bin b1_awl b2_awm b3_awn ->
              case $dEq_aGJ of _ { GHC.Classes.D:Eq tpl1_XiZ _ ->
              case tpl1_XiZ a1_awi b1_awl of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True ->
                  case ==_s1NN a2_awj b2_awm of _ {
                    GHC.Bool.False -> GHC.Bool.False;
                    GHC.Bool.True -> ==_s1NN a3_awk b3_awn
                  }
              }
              }
          }
      }
end Rec }

TestEq.$fEqTree0_/= :: forall a_afw.
                       (GHC.Classes.Eq a_afw) =>
                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEqTree0_/= =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1NP [ALWAYS Just L] :: TestEq.Tree a_afw
                                 -> TestEq.Tree a_afw
                                 -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1NP = TestEq.$fEqTree0_== @ a_afw $dEq_aGJ } in
    \ (a1_aws :: TestEq.Tree a_afw) (b_awt :: TestEq.Tree a_afw) ->
      case ==_s1NP a1_aws b_awt of _ {
        GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
      }

TestEq.$fEqTree0 :: forall a_afw.
                    (GHC.Classes.Eq a_afw) =>
                    GHC.Classes.Eq (TestEq.Tree a_afw)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fEqTree0 =
  __inline_me (\ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
                 GHC.Classes.D:Eq
                   @ (TestEq.Tree a_afw)
                   (TestEq.$fEqTree0_== @ a_afw $dEq_aGJ)
                   (TestEq.$fEqTree0_/= @ a_afw $dEq_aGJ))

Rec {
TestEq.$fShowTree_showsPrec :: forall a_afw.
                               (GHC.Show.Show a_afw) =>
                               GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fShowTree_showsPrec =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    let {
      a1_s1NR :: GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
      LclId
      [Str: DmdType]
      a1_s1NR = TestEq.$fShowTree_showsPrec @ a_afw $dShow_aFW } in
    \ (ds_d1ak :: GHC.Types.Int) (ds1_d1al :: TestEq.Tree a_afw) ->
      case ds1_d1al of _ {
        TestEq.Leaf -> lvl1_r1Vz;
        TestEq.Bin b1_awf b2_awg b3_awh ->
          case ds_d1ak of _ { GHC.Types.I# x_a1Do ->
          let {
            g_s1NX [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            g_s1NX = a1_s1NR TestEq.$fRead:+:2 b3_awh } in
          let {
            f_s1NV [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f_s1NV = a1_s1NR TestEq.$fRead:+:2 b2_awg } in
          let {
            f1_s1NT [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f1_s1NT =
              case $dShow_aFW of _ { GHC.Show.D:Show tpl1_Xjk _ _ ->
              tpl1_Xjk TestEq.$fRead:+:2 b1_awf
              } } in
          let {
            p_s1NZ :: GHC.Show.ShowS
            LclId
            [Arity 1
             Str: DmdType L]
            p_s1NZ =
              \ (x1_X1AO :: GHC.Base.String) ->
                GHC.Base.++
                  @ GHC.Types.Char
                  lvl_r1Vw
                  (f1_s1NT
                     (GHC.Types.:
                        @ GHC.Types.Char
                        GHC.Show.showSpace1
                        (f_s1NV
                           (GHC.Types.:
                              @ GHC.Types.Char GHC.Show.showSpace1 (g_s1NX x1_X1AO))))) } in
          case GHC.Prim.>=# x_a1Do 11 of _ {
            GHC.Bool.False -> p_s1NZ;
            GHC.Bool.True ->
              \ (x1_a1hb :: GHC.Base.String) ->
                GHC.Types.:
                  @ GHC.Types.Char
                  GHC.Show.showSignedInt2
                  (p_s1NZ
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb))
          }
          }
      }
end Rec }

TestEq.$fShowTree_showList :: forall a_afw.
                              (GHC.Show.Show a_afw) =>
                              [TestEq.Tree a_afw] -> GHC.Show.ShowS
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fShowTree_showList =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    GHC.Show.showList__
      @ (TestEq.Tree a_afw)
      (TestEq.$fShowTree_showsPrec
         @ a_afw $dShow_aFW TestEq.$fShowFixity1)

TestEq.$fShowTree :: forall a_afw.
                     (GHC.Show.Show a_afw) =>
                     GHC.Show.Show (TestEq.Tree a_afw)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowTree =
  __inline_me (\ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
                 let {
                   a1_s1lR [ALWAYS Just L] :: [TestEq.Tree a_afw] -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a1_s1lR = TestEq.$fShowTree_showList @ a_afw $dShow_aFW } in
                 let {
                   a2_s1lS [ALWAYS Just L] :: GHC.Types.Int
                                              -> TestEq.Tree a_afw
                                              -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a2_s1lS = TestEq.$fShowTree_showsPrec @ a_afw $dShow_aFW } in
                 letrec {
                   $dShow1_s1lP :: GHC.Show.Show (TestEq.Tree a_afw)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1lP =
                     GHC.Show.D:Show @ (TestEq.Tree a_afw) a2_s1lS a3_s1lQ a1_s1lR;
                   a3_s1lQ :: TestEq.Tree a_afw -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1lQ = GHC.Show.$dmshow @ (TestEq.Tree a_afw) $dShow1_s1lP; } in
                 $dShow1_s1lP)

Rec {
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.eqTree =
  __inline_me (\ (x_agA :: TestEq.Tree GHC.Types.Int)
                 (y_agB :: TestEq.Tree GHC.Types.Int) ->
                 TestEq.eqTree_eq'
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x_agA)
                   (TestEq.fromTree
                      @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any y_agB))
TestEq.eqTree_eq' :: (TestEq.C GHC.Prim.Any TestEq.U
                      TestEq.:+: TestEq.C
                                   GHC.Prim.Any
                                   (TestEq.Var GHC.Types.Int
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
                     -> (TestEq.C GHC.Prim.Any TestEq.U
                         TestEq.:+: TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Var GHC.Types.Int
                                       TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                                   TestEq.:*: TestEq.Rec
                                                                (TestEq.Tree GHC.Types.Int))))
                     -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.eqTree_eq' =
  \ (ds_d180 :: TestEq.C GHC.Prim.Any TestEq.U
                TestEq.:+: TestEq.C
                             GHC.Prim.Any
                             (TestEq.Var GHC.Types.Int
                              TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    (ds1_d181 :: TestEq.C GHC.Prim.Any TestEq.U
                 TestEq.:+: TestEq.C
                              GHC.Prim.Any
                              (TestEq.Var GHC.Types.Int
                               TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                           TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))) ->
    case ds_d180 of _ {
      TestEq.L x_age ->
        case ds1_d181 of _ {
          TestEq.L x'_agf ->
            case x_age of _ { TestEq.C _ ->
            case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds1_d181 of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh ->
            case x_agg of _ { TestEq.C a1_agq ->
            case x'_agh of _ { TestEq.C a'_agr ->
            case a1_agq of _ { TestEq.:*: a2_agi b_agj ->
            case a'_agr of _ { TestEq.:*: a'1_agk b'_agl ->
            case a2_agi of _ { TestEq.Var x1_ags ->
            case a'1_agk of _ { TestEq.Var x'1_agt ->
            case x1_ags of _ { GHC.Types.I# x2_a1ya ->
            case x'1_agt of _ { GHC.Types.I# y_a1ye ->
            case GHC.Prim.==# x2_a1ya y_a1ye of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True ->
                case b_agj of _ { TestEq.:*: a3_Xp9 b1_Xpb ->
                case b'_agl of _ { TestEq.:*: a'2_Xpf b'1_Xph ->
                case a3_Xp9 of _ { TestEq.Rec x3_agu ->
                case a'2_Xpf of _ { TestEq.Rec x'2_agv ->
                case TestEq.eqTree_eq'
                       (TestEq.fromTree
                          @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x3_agu)
                       (TestEq.fromTree
                          @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x'2_agv)
                of _ {
                  GHC.Bool.False -> GHC.Bool.False;
                  GHC.Bool.True ->
                    case b1_Xpb of _ { TestEq.Rec x4_XrS ->
                    case b'1_Xph of _ { TestEq.Rec x'3_XrW ->
                    TestEq.eqTree x4_XrS x'3_XrW
                    }
                    }
                }
                }
                }
                }
                }
            }
            }
            }
            }
            }
            }
            }
            }
            }
        }
    }
end Rec }

TestEq.$fEqTree :: TestEq.Eq (TestEq.Tree GHC.Types.Int)
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqTree =
  TestEq.eqTree
  `cast` (sym (TestEq.NTCo:T:Eq (TestEq.Tree GHC.Types.Int))
          :: (TestEq.Tree GHC.Types.Int
              -> TestEq.Tree GHC.Types.Int
              -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq (TestEq.Tree GHC.Types.Int))

TestEq.t16 :: TestEq.Var GHC.Types.Int
GblId
[NoCafRefs]
TestEq.t16 =
  TestEq.Var @ GHC.Types.Int TestEq.$fConstructorList_Cons_2

TestEq.t15 :: TestEq.Rec (TestEq.Tree GHC.Types.Int)
GblId
[NoCafRefs]
TestEq.t15 = TestEq.Rec @ (TestEq.Tree GHC.Types.Int) TestEq.tree0

TestEq.t14 :: TestEq.Rec (TestEq.Tree GHC.Types.Int)
              TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)
GblId
[NoCafRefs]
TestEq.t14 =
  TestEq.:*:
    @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
    @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
    TestEq.t15
    TestEq.t15

TestEq.t13 :: TestEq.Var GHC.Types.Int
              TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                          TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))
GblId
[NoCafRefs]
TestEq.t13 =
  TestEq.:*:
    @ (TestEq.Var GHC.Types.Int)
    @ (TestEq.Rec (TestEq.Tree GHC.Types.Int)
       TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))
    TestEq.t16
    TestEq.t14

TestEq.t12 :: TestEq.C
                GHC.Prim.Any
                (TestEq.Var GHC.Types.Int
                 TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                             TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
GblId
[NoCafRefs]
TestEq.t12 =
  TestEq.C
    @ GHC.Prim.Any
    @ (TestEq.Var GHC.Types.Int
       TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                   TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
    TestEq.t13

TestEq.t11 :: TestEq.C GHC.Prim.Any TestEq.U
              TestEq.:+: TestEq.C
                           GHC.Prim.Any
                           (TestEq.Var GHC.Types.Int
                            TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                        TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
GblId
[NoCafRefs]
TestEq.t11 =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any TestEq.U)
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Var GHC.Types.Int
          TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                      TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    TestEq.t12

TestEq.t1 :: GHC.Bool.Bool
GblId
[Str: DmdType]
TestEq.t1 = TestEq.eqTree_eq' TestEq.t11 TestEq.t11

Rec {
TestEq.eqLogic :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
GblId
[Arity 2
 Str: DmdType SS]
TestEq.eqLogic =
  __inline_me (\ (x_agy :: TestEq.Logic) (y_agz :: TestEq.Logic) ->
                 TestEq.eqLogic_eq'
                   (TestEq.fromLogic
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      x_agy)
                   (TestEq.fromLogic
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      @ GHC.Prim.Any
                      y_agz))
$wa3_r1Z2 :: TestEq.Logic
             -> TestEq.Rec TestEq.Logic
             -> TestEq.Logic
             -> TestEq.Rec TestEq.Logic
             -> GHC.Bool.Bool
GblId
[Arity 4
 Str: DmdType SLSL]
$wa3_r1Z2 =
  \ (ww_s1K2 :: TestEq.Logic)
    (ww1_s1K4 :: TestEq.Rec TestEq.Logic)
    (ww2_s1Ka :: TestEq.Logic)
    (ww3_s1Kc :: TestEq.Rec TestEq.Logic) ->
    case TestEq.eqLogic_eq'
           (TestEq.fromLogic
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              ww_s1K2)
           (TestEq.fromLogic
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              @ GHC.Prim.Any
              ww2_s1Ka)
    of _ {
      GHC.Bool.False -> GHC.Bool.False;
      GHC.Bool.True ->
        case ww1_s1K4 of _ { TestEq.Rec x_Xsd ->
        case ww3_s1Kc of _ { TestEq.Rec x'_Xsh ->
        TestEq.eqLogic x_Xsd x'_Xsh
        }
        }
    }
TestEq.eqLogic_eq' :: (TestEq.C
                         GHC.Prim.Any (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                   TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                               TestEq.:+: (TestEq.C
                                                             GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         GHC.Prim.Any
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     GHC.Prim.Any
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 GHC.Prim.Any
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            GHC.Prim.Any
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic))))))))
                      -> (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                          TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                      TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                                  TestEq.:+: (TestEq.C
                                                                GHC.Prim.Any
                                                                (TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            GHC.Prim.Any
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        GHC.Prim.Any
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: (TestEq.C
                                                                                                    GHC.Prim.Any
                                                                                                    (TestEq.Rec
                                                                                                       TestEq.Logic
                                                                                                     TestEq.:*: TestEq.Rec
                                                                                                                  TestEq.Logic)
                                                                                                  TestEq.:+: TestEq.C
                                                                                                               GHC.Prim.Any
                                                                                                               (TestEq.Rec
                                                                                                                  TestEq.Logic
                                                                                                                TestEq.:*: TestEq.Rec
                                                                                                                             TestEq.Logic))))))))
                      -> GHC.Bool.Bool
GblId
[Arity 2
 Str: DmdType SS]
TestEq.eqLogic_eq' =
  \ (ds_X1kj :: TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                        TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  GHC.Prim.Any
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: (TestEq.C
                                                                              GHC.Prim.Any
                                                                              (TestEq.Rec
                                                                                 TestEq.Logic
                                                                               TestEq.:*: TestEq.Rec
                                                                                            TestEq.Logic)
                                                                            TestEq.:+: (TestEq.C
                                                                                          GHC.Prim.Any
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic)
                                                                                        TestEq.:+: TestEq.C
                                                                                                     GHC.Prim.Any
                                                                                                     (TestEq.Rec
                                                                                                        TestEq.Logic
                                                                                                      TestEq.:*: TestEq.Rec
                                                                                                                   TestEq.Logic))))))))
    (ds1_X1kl :: TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
                 TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                             TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                         TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                     TestEq.:+: (TestEq.C
                                                                   GHC.Prim.Any
                                                                   (TestEq.Rec TestEq.Logic
                                                                    TestEq.:*: TestEq.Rec
                                                                                 TestEq.Logic)
                                                                 TestEq.:+: (TestEq.C
                                                                               GHC.Prim.Any
                                                                               (TestEq.Rec
                                                                                  TestEq.Logic
                                                                                TestEq.:*: TestEq.Rec
                                                                                             TestEq.Logic)
                                                                             TestEq.:+: (TestEq.C
                                                                                           GHC.Prim.Any
                                                                                           (TestEq.Rec
                                                                                              TestEq.Logic
                                                                                            TestEq.:*: TestEq.Rec
                                                                                                         TestEq.Logic)
                                                                                         TestEq.:+: TestEq.C
                                                                                                      GHC.Prim.Any
                                                                                                      (TestEq.Rec
                                                                                                         TestEq.Logic
                                                                                                       TestEq.:*: TestEq.Rec
                                                                                                                    TestEq.Logic)))))))) ->
    case ds_X1kj of _ {
      TestEq.L x_age ->
        case ds1_X1kl of _ {
          TestEq.L x'_agf ->
            case x_age of _ { TestEq.C a1_agq ->
            case x'_agf of _ { TestEq.C a'_agr ->
            TestEq.eqVar
              @ [GHC.Types.Char]
              (a_r1Y7
               `cast` (trans
                         (sym (TestEq.NTCo:T:Eq [GHC.Types.Char]))
                         (TestEq.T:Eq [GHC.Types.Char])
                       :: ([GHC.Types.Char] -> [GHC.Types.Char] -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq [GHC.Types.Char]))
              a1_agq
              a'_agr
            }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds1_X1kl of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh ->
            case x_agg of _ {
              TestEq.L x1_age ->
                case x'_agh of _ {
                  TestEq.L x'1_agf ->
                    case x1_age of _ { TestEq.C _ ->
                    case x'1_agf of _ { TestEq.C _ -> GHC.Bool.True }
                    };
                  TestEq.R _ -> GHC.Bool.False
                };
              TestEq.R x1_XoK ->
                case x'_agh of _ {
                  TestEq.L _ -> GHC.Bool.False;
                  TestEq.R x'1_XoO ->
                    case x1_XoK of _ {
                      TestEq.L x2_age ->
                        case x'1_XoO of _ {
                          TestEq.L x'2_agf ->
                            case x2_age of _ { TestEq.C _ ->
                            case x'2_agf of _ { TestEq.C _ -> GHC.Bool.True }
                            };
                          TestEq.R _ -> GHC.Bool.False
                        };
                      TestEq.R x2_XsE ->
                        case x'1_XoO of _ {
                          TestEq.L _ -> GHC.Bool.False;
                          TestEq.R x'2_XsI ->
                            case x2_XsE of _ {
                              TestEq.L x3_age ->
                                case x'2_XsI of _ {
                                  TestEq.L x'3_agf ->
                                    case x3_age of _ { TestEq.C a1_agq ->
                                    case x'3_agf of _ { TestEq.C a'_agr ->
                                    case a1_agq of _ { TestEq.Rec x4_agu ->
                                    case a'_agr of _ { TestEq.Rec x'4_agv ->
                                    TestEq.eqLogic x4_agu x'4_agv
                                    }
                                    }
                                    }
                                    };
                                  TestEq.R _ -> GHC.Bool.False
                                };
                              TestEq.R x3_XoX ->
                                case x'2_XsI of _ {
                                  TestEq.L _ -> GHC.Bool.False;
                                  TestEq.R x'3_Xp1 ->
                                    case x3_XoX of _ {
                                      TestEq.L x4_age ->
                                        case x'3_Xp1 of _ {
                                          TestEq.L x'4_agf ->
                                            case x4_age of _ { TestEq.C a1_agq ->
                                            case x'4_agf of _ { TestEq.C a'_agr ->
                                            case a1_agq of _ { TestEq.:*: ww_s1K0 ww1_s1K4 ->
                                            case ww_s1K0 of _ { TestEq.Rec ww3_s1K2 ->
                                            case a'_agr of _ { TestEq.:*: ww4_s1K8 ww5_s1Kc ->
                                            case ww4_s1K8 of _ { TestEq.Rec ww7_s1Ka ->
                                            $wa3_r1Z2 ww3_s1K2 ww1_s1K4 ww7_s1Ka ww5_s1Kc
                                            }
                                            }
                                            }
                                            }
                                            }
                                            };
                                          TestEq.R _ -> GHC.Bool.False
                                        };
                                      TestEq.R x4_Xp4 ->
                                        case x'3_Xp1 of _ {
                                          TestEq.L _ -> GHC.Bool.False;
                                          TestEq.R x'4_Xp8 ->
                                            case x4_Xp4 of _ {
                                              TestEq.L x5_age ->
                                                case x'4_Xp8 of _ {
                                                  TestEq.L x'5_agf ->
                                                    case x5_age of _ { TestEq.C a1_agq ->
                                                    case x'5_agf of _ { TestEq.C a'_agr ->
                                                    case a1_agq
                                                    of _ { TestEq.:*: ww_s1K0 ww1_s1K4 ->
                                                    case ww_s1K0 of _ { TestEq.Rec ww3_s1K2 ->
                                                    case a'_agr
                                                    of _ { TestEq.:*: ww4_s1K8 ww5_s1Kc ->
                                                    case ww4_s1K8 of _ { TestEq.Rec ww7_s1Ka ->
                                                    $wa3_r1Z2 ww3_s1K2 ww1_s1K4 ww7_s1Ka ww5_s1Kc
                                                    }
                                                    }
                                                    }
                                                    }
                                                    }
                                                    };
                                                  TestEq.R _ -> GHC.Bool.False
                                                };
                                              TestEq.R x5_Xpb ->
                                                case x'4_Xp8 of _ {
                                                  TestEq.L _ -> GHC.Bool.False;
                                                  TestEq.R x'5_Xpf ->
                                                    case x5_Xpb of _ {
                                                      TestEq.L x6_age ->
                                                        case x'5_Xpf of _ {
                                                          TestEq.L x'6_agf ->
                                                            case x6_age of _ { TestEq.C a1_agq ->
                                                            case x'6_agf of _ { TestEq.C a'_agr ->
                                                            case a1_agq
                                                            of _ { TestEq.:*: ww_s1K0 ww1_s1K4 ->
                                                            case ww_s1K0
                                                            of _ { TestEq.Rec ww3_s1K2 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww4_s1K8 ww5_s1Kc ->
                                                            case ww4_s1K8
                                                            of _ { TestEq.Rec ww7_s1Ka ->
                                                            $wa3_r1Z2
                                                              ww3_s1K2 ww1_s1K4 ww7_s1Ka ww5_s1Kc
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            };
                                                          TestEq.R _ -> GHC.Bool.False
                                                        };
                                                      TestEq.R x6_Xpi ->
                                                        case x'5_Xpf of _ {
                                                          TestEq.L _ -> GHC.Bool.False;
                                                          TestEq.R x'6_Xpm ->
                                                            case x6_Xpi of _ { TestEq.C a1_agq ->
                                                            case x'6_Xpm of _ { TestEq.C a'_agr ->
                                                            case a1_agq
                                                            of _ { TestEq.:*: ww_s1K0 ww1_s1K4 ->
                                                            case ww_s1K0
                                                            of _ { TestEq.Rec ww3_s1K2 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww4_s1K8 ww5_s1Kc ->
                                                            case ww4_s1K8
                                                            of _ { TestEq.Rec ww7_s1Ka ->
                                                            $wa3_r1Z2
                                                              ww3_s1K2 ww1_s1K4 ww7_s1Ka ww5_s1Kc
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
end Rec }

TestEq.$fEqLogic :: TestEq.Eq TestEq.Logic
GblId[DFunId]
[Arity 2
 Str: DmdType SS]
TestEq.$fEqLogic =
  TestEq.eqLogic
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.Logic)
          :: (TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq TestEq.Logic)

TestEq.t29 :: TestEq.Rec TestEq.Logic
GblId
[NoCafRefs]
TestEq.t29 = TestEq.Rec @ TestEq.Logic TestEq.logic12

TestEq.t28 :: TestEq.Rec TestEq.Logic
GblId
[NoCafRefs]
TestEq.t28 = TestEq.Rec @ TestEq.Logic TestEq.logic11

TestEq.t27 :: TestEq.Rec TestEq.Logic
              TestEq.:*: TestEq.Rec TestEq.Logic
GblId
[NoCafRefs]
TestEq.t27 =
  TestEq.:*:
    @ (TestEq.Rec TestEq.Logic)
    @ (TestEq.Rec TestEq.Logic)
    TestEq.t29
    TestEq.t28

TestEq.t26 :: TestEq.C
                GHC.Prim.Any
                (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
GblId
[NoCafRefs]
TestEq.t26 =
  TestEq.C
    @ GHC.Prim.Any
    @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
    TestEq.t27

TestEq.t25 :: TestEq.C
                GHC.Prim.Any
                (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
              TestEq.:+: (TestEq.C
                            GHC.Prim.Any
                            (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                          TestEq.:+: (TestEq.C
                                        GHC.Prim.Any
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: TestEq.C
                                                   GHC.Prim.Any
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)))
GblId
[NoCafRefs]
TestEq.t25 =
  TestEq.L
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
       TestEq.:+: (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: TestEq.C
                                GHC.Prim.Any
                                (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)))
    TestEq.t26

TestEq.t24 :: TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
              TestEq.:+: (TestEq.C
                            GHC.Prim.Any
                            (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                          TestEq.:+: (TestEq.C
                                        GHC.Prim.Any
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    GHC.Prim.Any
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: TestEq.C
                                                               GHC.Prim.Any
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec
                                                                             TestEq.Logic))))
GblId
[NoCafRefs]
TestEq.t24 =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic))
    @ (TestEq.C
         GHC.Prim.Any
         (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
       TestEq.:+: (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: TestEq.C
                                            GHC.Prim.Any
                                            (TestEq.Rec TestEq.Logic
                                             TestEq.:*: TestEq.Rec TestEq.Logic))))
    TestEq.t25

TestEq.t23 :: TestEq.C GHC.Prim.Any TestEq.U
              TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                          TestEq.:+: (TestEq.C
                                        GHC.Prim.Any
                                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    GHC.Prim.Any
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                GHC.Prim.Any
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: TestEq.C
                                                                           GHC.Prim.Any
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)))))
GblId
[NoCafRefs]
TestEq.t23 =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any TestEq.U)
    @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
       TestEq.:+: (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: TestEq.C
                                                        GHC.Prim.Any
                                                        (TestEq.Rec TestEq.Logic
                                                         TestEq.:*: TestEq.Rec TestEq.Logic)))))
    TestEq.t24

TestEq.t22 :: TestEq.C GHC.Prim.Any TestEq.U
              TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                          TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                      TestEq.:+: (TestEq.C
                                                    GHC.Prim.Any
                                                    (TestEq.Rec TestEq.Logic
                                                     TestEq.:*: TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                GHC.Prim.Any
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            GHC.Prim.Any
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: TestEq.C
                                                                                       GHC.Prim.Any
                                                                                       (TestEq.Rec
                                                                                          TestEq.Logic
                                                                                        TestEq.:*: TestEq.Rec
                                                                                                     TestEq.Logic))))))
GblId
[NoCafRefs]
TestEq.t22 =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any TestEq.U)
    @ (TestEq.C GHC.Prim.Any TestEq.U
       TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    GHC.Prim.Any
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic))))))
    TestEq.t23

TestEq.t21 :: TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
              TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                          TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                      TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                  TestEq.:+: (TestEq.C
                                                                GHC.Prim.Any
                                                                (TestEq.Rec TestEq.Logic
                                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                                              TestEq.:+: (TestEq.C
                                                                            GHC.Prim.Any
                                                                            (TestEq.Rec TestEq.Logic
                                                                             TestEq.:*: TestEq.Rec
                                                                                          TestEq.Logic)
                                                                          TestEq.:+: (TestEq.C
                                                                                        GHC.Prim.Any
                                                                                        (TestEq.Rec
                                                                                           TestEq.Logic
                                                                                         TestEq.:*: TestEq.Rec
                                                                                                      TestEq.Logic)
                                                                                      TestEq.:+: TestEq.C
                                                                                                   GHC.Prim.Any
                                                                                                   (TestEq.Rec
                                                                                                      TestEq.Logic
                                                                                                    TestEq.:*: TestEq.Rec
                                                                                                                 TestEq.Logic)))))))
GblId
[NoCafRefs]
TestEq.t21 =
  TestEq.R
    @ (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String))
    @ (TestEq.C GHC.Prim.Any TestEq.U
       TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                   TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     GHC.Prim.Any
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: TestEq.C
                                                                                GHC.Prim.Any
                                                                                (TestEq.Rec
                                                                                   TestEq.Logic
                                                                                 TestEq.:*: TestEq.Rec
                                                                                              TestEq.Logic)))))))
    TestEq.t22

TestEq.t2 :: GHC.Bool.Bool
GblId
[Str: DmdType]
TestEq.t2 = TestEq.eqLogic_eq' TestEq.t21 TestEq.t21




==================== Tidy Core Rules ====================
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_F_]" ALWAYS
    forall {$dConstructor_s1oF [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_F_}
      TestEq.$dmconIsRecord @ TestEq.Logic_F_ $dConstructor_s1oF
      = TestEq.$dmconIsRecord_$s$dmconIsRecord
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_T_]" ALWAYS
    forall {$dConstructor_s1oI [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_T_}
      TestEq.$dmconIsRecord @ TestEq.Logic_T_ $dConstructor_s1oI
      = TestEq.$dmconIsRecord_$s$dmconIsRecord1
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Not_]" ALWAYS
    forall {$dConstructor_s1oL [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Not_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Not_ $dConstructor_s1oL
      = TestEq.$dmconIsRecord_$s$dmconIsRecord2
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s1oO [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Disj_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Disj_ $dConstructor_s1oO
      = TestEq.$dmconIsRecord_$s$dmconIsRecord3
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s1oR [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Conj_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Conj_ $dConstructor_s1oR
      = TestEq.$dmconIsRecord_$s$dmconIsRecord4
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s1oU [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Equiv_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Equiv_ $dConstructor_s1oU
      = TestEq.$dmconIsRecord_$s$dmconIsRecord5
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s1oX [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Impl_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Impl_ $dConstructor_s1oX
      = TestEq.$dmconIsRecord_$s$dmconIsRecord6
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Var_]" ALWAYS
    forall {$dConstructor_s1p0 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Var_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Var_ $dConstructor_s1p0
      = TestEq.$dmconIsRecord_$s$dmconIsRecord7
"SPEC TestEq.$dmconIsRecord [TestEq.Leaf]" ALWAYS
    forall {$dConstructor_s1p3 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Leaf}
      TestEq.$dmconIsRecord @ TestEq.Leaf $dConstructor_s1p3
      = TestEq.$dmconIsRecord_$s$dmconIsRecord8
"SPEC TestEq.$dmconIsRecord [TestEq.Bin]" ALWAYS
    forall {$dConstructor_s1p6 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Bin}
      TestEq.$dmconIsRecord @ TestEq.Bin $dConstructor_s1p6
      = TestEq.$dmconIsRecord_$s$dmconIsRecord9
"SPEC TestEq.$dmconIsRecord [TestEq.List_Cons_]" ALWAYS
    forall {$dConstructor_s1p9 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.List_Cons_}
      TestEq.$dmconIsRecord @ TestEq.List_Cons_ $dConstructor_s1p9
      = TestEq.$dmconIsRecord_$s$dmconIsRecord10
"SPEC TestEq.$dmconIsRecord [TestEq.List_Nil_]" ALWAYS
    forall {$dConstructor_s1pc [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.List_Nil_}
      TestEq.$dmconIsRecord @ TestEq.List_Nil_ $dConstructor_s1pc
      = TestEq.$dmconIsRecord_$s$dmconIsRecord11
"SPEC TestEq.$dmconFixity [TestEq.Logic_F_]" ALWAYS
    forall {$dConstructor_s1o8 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_F_}
      TestEq.$dmconFixity @ TestEq.Logic_F_ $dConstructor_s1o8
      = TestEq.$dmconFixity_$s$dmconFixity10
"SPEC TestEq.$dmconFixity [TestEq.Logic_T_]" ALWAYS
    forall {$dConstructor_s1ob [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_T_}
      TestEq.$dmconFixity @ TestEq.Logic_T_ $dConstructor_s1ob
      = TestEq.$dmconFixity_$s$dmconFixity9
"SPEC TestEq.$dmconFixity [TestEq.Logic_Not_]" ALWAYS
    forall {$dConstructor_s1oe [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Not_}
      TestEq.$dmconFixity @ TestEq.Logic_Not_ $dConstructor_s1oe
      = TestEq.$dmconFixity_$s$dmconFixity8
"SPEC TestEq.$dmconFixity [TestEq.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s1oh [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Disj_}
      TestEq.$dmconFixity @ TestEq.Logic_Disj_ $dConstructor_s1oh
      = TestEq.$dmconFixity_$s$dmconFixity7
"SPEC TestEq.$dmconFixity [TestEq.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s1ok [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Conj_}
      TestEq.$dmconFixity @ TestEq.Logic_Conj_ $dConstructor_s1ok
      = TestEq.$dmconFixity_$s$dmconFixity6
"SPEC TestEq.$dmconFixity [TestEq.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s1on [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Equiv_}
      TestEq.$dmconFixity @ TestEq.Logic_Equiv_ $dConstructor_s1on
      = TestEq.$dmconFixity_$s$dmconFixity5
"SPEC TestEq.$dmconFixity [TestEq.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s1oq [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Impl_}
      TestEq.$dmconFixity @ TestEq.Logic_Impl_ $dConstructor_s1oq
      = TestEq.$dmconFixity_$s$dmconFixity4
"SPEC TestEq.$dmconFixity [TestEq.Logic_Var_]" ALWAYS
    forall {$dConstructor_s1ot [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Var_}
      TestEq.$dmconFixity @ TestEq.Logic_Var_ $dConstructor_s1ot
      = TestEq.$dmconFixity_$s$dmconFixity3
"SPEC TestEq.$dmconFixity [TestEq.Leaf]" ALWAYS
    forall {$dConstructor_s1ow [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Leaf}
      TestEq.$dmconFixity @ TestEq.Leaf $dConstructor_s1ow
      = TestEq.$dmconFixity_$s$dmconFixity2
"SPEC TestEq.$dmconFixity [TestEq.Bin]" ALWAYS
    forall {$dConstructor_s1oz [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Bin}
      TestEq.$dmconFixity @ TestEq.Bin $dConstructor_s1oz
      = TestEq.$dmconFixity_$s$dmconFixity1
"SPEC TestEq.$dmconFixity [TestEq.List_Nil_]" ALWAYS
    forall {$dConstructor_s1oC [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.List_Nil_}
      TestEq.$dmconFixity @ TestEq.List_Nil_ $dConstructor_s1oC
      = TestEq.$dmconFixity_$s$dmconFixity
"SC:showsPrec0" [0]
    forall {sc_s1Qy :: TestEq.Logic sc1_s1Qz :: GHC.Prim.Int#}
      TestEq.$fShowLogic_showsPrec (GHC.Types.I# sc1_s1Qz) sc_s1Qy
      = TestEq.$fShowLogic_$sshowsPrec sc_s1Qy sc1_s1Qz


