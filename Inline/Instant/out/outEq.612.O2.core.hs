Best version, generates eq specialized to concrete datatype.

Of interest:

TestEq.t1 = TestEq.eqTree TestEq.tree1 TestEq.tree1

Rec {
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.eqTree =
  \ (x_agA :: TestEq.Tree GHC.Types.Int)
    (y_agB :: TestEq.Tree GHC.Types.Int) ->
    case x_agA of _ {
      TestEq.Leaf ->
        case y_agB of _ {
          TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
        };
      TestEq.Bin x1_afO l_afP r_afQ ->
        case y_agB of _ {
          TestEq.Leaf -> GHC.Bool.False;
          TestEq.Bin x2_Xov l1_Xox r1_Xoz ->
            case x1_afO of _ { GHC.Types.I# x3_a1yv ->
            case x2_Xov of _ { GHC.Types.I# y1_a1yz ->
            case GHC.Prim.==# x3_a1yv y1_a1yz of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True ->
                case TestEq.eqTree l_afP l1_Xox of _ {
                  GHC.Bool.False -> GHC.Bool.False;
                  GHC.Bool.True -> TestEq.eqTree r_afQ r1_Xoz
                }
            }
            }
            }
        }
    }
end Rec }

==================== Specialise ====================
Rec {
TestEq.$fConstructorList_Cons_ :: TestEq.Constructor
                                    TestEq.List_Cons_
LclIdX[DFunId]
[]
TestEq.$fConstructorList_Cons_ =
  TestEq.D:Constructor
    @ TestEq.List_Cons_ conName_a144 conFixity_a14a a_s1ej
a_s1ej [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1ej =
  TestEq.$dmconIsRecord
    @ TestEq.List_Cons_ TestEq.$fConstructorList_Cons_
conName_a144 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Cons_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a144 =
  \ (@ t_a141::* -> * -> *) (@ a_a142) _ ->
    GHC.Types.:
      @ GHC.Types.Char (GHC.Types.C# ':') (GHC.Types.[] @ GHC.Types.Char)
conFixity_a14a :: forall (t_afB :: * -> * -> *) a_afC.
                  t_afB TestEq.List_Cons_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
conFixity_a14a =
  \ (@ t_a147::* -> * -> *) (@ a_a148) _ ->
    TestEq.Infix TestEq.RightAssociative (GHC.Types.I# 5)
TestEq.$fConstructorLogic_F_ :: TestEq.Constructor TestEq.Logic_F_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_F_ =
  TestEq.D:Constructor @ TestEq.Logic_F_ conName_a123 a_s1eX a_s1eZ
a_s1eZ [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eZ =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_F_ TestEq.$fConstructorLogic_F_
a_s1eX [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eX =
  TestEq.$dmconFixity @ TestEq.Logic_F_ TestEq.$fConstructorLogic_F_
conName_a123 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_F_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a123 =
  \ (@ t_a120::* -> * -> *) (@ a_a121) _ ->
    GHC.Types.:
      @ GHC.Types.Char (GHC.Types.C# 'F') (GHC.Types.[] @ GHC.Types.Char)
TestEq.$fConstructorLogic_T_ :: TestEq.Constructor TestEq.Logic_T_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_T_ =
  TestEq.D:Constructor @ TestEq.Logic_T_ conName_a12e a_s1eT a_s1eV
a_s1eV [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eV =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_T_ TestEq.$fConstructorLogic_T_
a_s1eT [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eT =
  TestEq.$dmconFixity @ TestEq.Logic_T_ TestEq.$fConstructorLogic_T_
conName_a12e :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_T_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12e =
  \ (@ t_a12b::* -> * -> *) (@ a_a12c) _ ->
    GHC.Types.:
      @ GHC.Types.Char (GHC.Types.C# 'T') (GHC.Types.[] @ GHC.Types.Char)
TestEq.$fConstructorLogic_Not_ :: TestEq.Constructor
                                    TestEq.Logic_Not_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Not_ =
  TestEq.D:Constructor @ TestEq.Logic_Not_ conName_a12p a_s1eP a_s1eR
a_s1eR [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eR =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Not_ TestEq.$fConstructorLogic_Not_
a_s1eP [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eP =
  TestEq.$dmconFixity
    @ TestEq.Logic_Not_ TestEq.$fConstructorLogic_Not_
conName_a12p :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Not_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12p =
  \ (@ t_a12m::* -> * -> *) (@ a_a12n) _ ->
    GHC.Base.unpackCString# "Not"
TestEq.$fConstructorLogic_Disj_ :: TestEq.Constructor
                                     TestEq.Logic_Disj_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Disj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Disj_ conName_a12A a_s1eL a_s1eN
a_s1eN [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eN =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Disj_ TestEq.$fConstructorLogic_Disj_
a_s1eL [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eL =
  TestEq.$dmconFixity
    @ TestEq.Logic_Disj_ TestEq.$fConstructorLogic_Disj_
conName_a12A :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Disj_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12A =
  \ (@ t_a12x::* -> * -> *) (@ a_a12y) _ ->
    GHC.Base.unpackCString# "Disj"
TestEq.$fConstructorLogic_Conj_ :: TestEq.Constructor
                                     TestEq.Logic_Conj_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Conj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Conj_ conName_a12L a_s1eH a_s1eJ
a_s1eJ [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eJ =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Conj_ TestEq.$fConstructorLogic_Conj_
a_s1eH [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eH =
  TestEq.$dmconFixity
    @ TestEq.Logic_Conj_ TestEq.$fConstructorLogic_Conj_
conName_a12L :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Conj_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12L =
  \ (@ t_a12I::* -> * -> *) (@ a_a12J) _ ->
    GHC.Base.unpackCString# "Conj"
TestEq.$fConstructorLogic_Equiv_ :: TestEq.Constructor
                                      TestEq.Logic_Equiv_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Equiv_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Equiv_ conName_a12W a_s1eD a_s1eF
a_s1eF [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eF =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Equiv_ TestEq.$fConstructorLogic_Equiv_
a_s1eD [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1eD =
  TestEq.$dmconFixity
    @ TestEq.Logic_Equiv_ TestEq.$fConstructorLogic_Equiv_
conName_a12W :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Equiv_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a12W =
  \ (@ t_a12T::* -> * -> *) (@ a_a12U) _ ->
    GHC.Base.unpackCString# "Equiv"
TestEq.$fConstructorLogic_Impl_ :: TestEq.Constructor
                                     TestEq.Logic_Impl_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Impl_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Impl_ conName_a137 a_s1ez a_s1eB
a_s1eB [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eB =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Impl_ TestEq.$fConstructorLogic_Impl_
a_s1ez [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ez =
  TestEq.$dmconFixity
    @ TestEq.Logic_Impl_ TestEq.$fConstructorLogic_Impl_
conName_a137 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Impl_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a137 =
  \ (@ t_a134::* -> * -> *) (@ a_a135) _ ->
    GHC.Base.unpackCString# "Impl"
TestEq.$fConstructorLogic_Var_ :: TestEq.Constructor
                                    TestEq.Logic_Var_
LclIdX[DFunId]
[]
TestEq.$fConstructorLogic_Var_ =
  TestEq.D:Constructor @ TestEq.Logic_Var_ conName_a13i a_s1ev a_s1ex
a_s1ex [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1ex =
  TestEq.$dmconIsRecord
    @ TestEq.Logic_Var_ TestEq.$fConstructorLogic_Var_
a_s1ev [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ev =
  TestEq.$dmconFixity
    @ TestEq.Logic_Var_ TestEq.$fConstructorLogic_Var_
conName_a13i :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Var_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a13i =
  \ (@ t_a13f::* -> * -> *) (@ a_a13g) _ ->
    GHC.Base.unpackCString# "VarL"
TestEq.$fConstructorLeaf :: TestEq.Constructor TestEq.Leaf
LclIdX[DFunId]
[]
TestEq.$fConstructorLeaf =
  TestEq.D:Constructor @ TestEq.Leaf conName_a13I a_s1ep a_s1er
a_s1er [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1er =
  TestEq.$dmconIsRecord @ TestEq.Leaf TestEq.$fConstructorLeaf
a_s1ep [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ep = TestEq.$dmconFixity @ TestEq.Leaf TestEq.$fConstructorLeaf
conName_a13I :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Leaf a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a13I =
  \ (@ t_a13F::* -> * -> *) (@ a_a13G) _ ->
    GHC.Base.unpackCString# "Leaf"
TestEq.$fConstructorBin :: TestEq.Constructor TestEq.Bin
LclIdX[DFunId]
[]
TestEq.$fConstructorBin =
  TestEq.D:Constructor @ TestEq.Bin conName_a13T a_s1el a_s1en
a_s1en [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1en = TestEq.$dmconIsRecord @ TestEq.Bin TestEq.$fConstructorBin
a_s1el [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1el = TestEq.$dmconFixity @ TestEq.Bin TestEq.$fConstructorBin
conName_a13T :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Bin a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a13T =
  \ (@ t_a13Q::* -> * -> *) (@ a_a13R) _ ->
    GHC.Base.unpackCString# "Bin"
TestEq.$fConstructorList_Nil_ :: TestEq.Constructor
                                   TestEq.List_Nil_
LclIdX[DFunId]
[]
TestEq.$fConstructorList_Nil_ =
  TestEq.D:Constructor @ TestEq.List_Nil_ conName_a14j a_s1ef a_s1eh
a_s1eh [ALWAYS LoopBreaker Nothing] :: forall (t_afD :: *
                                                        -> *
                                                        -> *)
                                              a_afE.
                                       t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
a_s1eh =
  TestEq.$dmconIsRecord
    @ TestEq.List_Nil_ TestEq.$fConstructorList_Nil_
a_s1ef [ALWAYS LoopBreaker Nothing] :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
a_s1ef =
  TestEq.$dmconFixity
    @ TestEq.List_Nil_ TestEq.$fConstructorList_Nil_
conName_a14j :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Nil_ a_afA -> GHC.Base.String
LclId
[Arity 1]
conName_a14j =
  \ (@ t_a14g::* -> * -> *) (@ a_a14h) _ ->
    GHC.Base.unpackCString# "[]"
$s$dmconFixity_s1po :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1po =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_F_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_F_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pn :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pn =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_T_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_T_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pm :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pm =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Not_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Not_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pl :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pl =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Disj_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Disj_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pk :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pk =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Conj_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Conj_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pj :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pj =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Equiv_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Equiv_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pi :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pi =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Impl_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Impl_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1ph :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1ph =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Logic_Var_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.Logic_Var_ a_aFM)
      TestEq.Prefix
      eta_B1
$s$dmconFixity_s1pg :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pg =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Leaf a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity @ (t_aFL TestEq.Leaf a_aFM) TestEq.Prefix eta_B1
$s$dmconFixity_s1pf :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pf =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.Bin a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity @ (t_aFL TestEq.Bin a_aFM) TestEq.Prefix eta_B1
$s$dmconFixity_s1pe :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
LclId
[Arity 1]
$s$dmconFixity_s1pe =
  \ (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL TestEq.List_Nil_ a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity
      @ (t_aFL TestEq.List_Nil_ a_aFM)
      TestEq.Prefix
      eta_B1
TestEq.$dmconFixity :: forall c_afy.
                       (TestEq.Constructor c_afy) =>
                       forall (t_afB :: * -> * -> *) a_afC.
                       t_afB c_afy a_afC -> TestEq.Fixity
LclIdX
[Arity 2
 RULES: "SPEC TestEq.$dmconFixity [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1o8 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconFixity @ TestEq.Logic_F_ $dConstructor_s1o8
              = $s$dmconFixity_s1po
        "SPEC TestEq.$dmconFixity [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1ob [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconFixity @ TestEq.Logic_T_ $dConstructor_s1ob
              = $s$dmconFixity_s1pn
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oe [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconFixity @ TestEq.Logic_Not_ $dConstructor_s1oe
              = $s$dmconFixity_s1pm
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oh [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconFixity @ TestEq.Logic_Disj_ $dConstructor_s1oh
              = $s$dmconFixity_s1pl
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1ok [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconFixity @ TestEq.Logic_Conj_ $dConstructor_s1ok
              = $s$dmconFixity_s1pk
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1on [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconFixity @ TestEq.Logic_Equiv_ $dConstructor_s1on
              = $s$dmconFixity_s1pj
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oq [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconFixity @ TestEq.Logic_Impl_ $dConstructor_s1oq
              = $s$dmconFixity_s1pi
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1ot [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconFixity @ TestEq.Logic_Var_ $dConstructor_s1ot
              = $s$dmconFixity_s1ph
        "SPEC TestEq.$dmconFixity [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1ow [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconFixity @ TestEq.Leaf $dConstructor_s1ow
              = $s$dmconFixity_s1pg
        "SPEC TestEq.$dmconFixity [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1oz [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconFixity @ TestEq.Bin $dConstructor_s1oz
              = $s$dmconFixity_s1pf
        "SPEC TestEq.$dmconFixity [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1oC [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconFixity @ TestEq.List_Nil_ $dConstructor_s1oC
              = $s$dmconFixity_s1pe]
TestEq.$dmconFixity =
  \ (@ c_afy)
    _
    (@ t_aFL::* -> * -> *)
    (@ a_aFM)
    (eta_B1 :: t_aFL c_afy a_aFM) ->
    GHC.Base.const
      @ TestEq.Fixity @ (t_aFL c_afy a_aFM) TestEq.Prefix eta_B1
$s$dmconIsRecord_s1pp :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pp =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_F_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_F_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pq :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pq =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_T_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_T_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pr :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pr =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Not_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Not_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1ps :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1ps =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Disj_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Disj_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pt :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pt =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Conj_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Conj_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pu :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pu =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Equiv_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Equiv_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pv :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pv =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Impl_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Impl_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pw :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pw =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Logic_Var_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.Logic_Var_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1px :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1px =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Leaf a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool @ (t_aFS TestEq.Leaf a_aFT) GHC.Bool.False eta_B1
$s$dmconIsRecord_s1py :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1py =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.Bin a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool @ (t_aFS TestEq.Bin a_aFT) GHC.Bool.False eta_B1
$s$dmconIsRecord_s1pz :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pz =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.List_Cons_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.List_Cons_ a_aFT)
      GHC.Bool.False
      eta_B1
$s$dmconIsRecord_s1pA :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1]
$s$dmconIsRecord_s1pA =
  \ (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS TestEq.List_Nil_ a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool
      @ (t_aFS TestEq.List_Nil_ a_aFT)
      GHC.Bool.False
      eta_B1
TestEq.$dmconIsRecord :: forall c_afy.
                         (TestEq.Constructor c_afy) =>
                         forall (t_afD :: * -> * -> *) a_afE.
                         t_afD c_afy a_afE -> GHC.Bool.Bool
LclIdX
[Arity 2
 RULES: "SPEC TestEq.$dmconIsRecord [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1oF [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconIsRecord @ TestEq.Logic_F_ $dConstructor_s1oF
              = $s$dmconIsRecord_s1pp
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1oI [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconIsRecord @ TestEq.Logic_T_ $dConstructor_s1oI
              = $s$dmconIsRecord_s1pq
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oL [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Not_ $dConstructor_s1oL
              = $s$dmconIsRecord_s1pr
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oO [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Disj_ $dConstructor_s1oO
              = $s$dmconIsRecord_s1ps
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1oR [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Conj_ $dConstructor_s1oR
              = $s$dmconIsRecord_s1pt
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1oU [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Equiv_ $dConstructor_s1oU
              = $s$dmconIsRecord_s1pu
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oX [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Impl_ $dConstructor_s1oX
              = $s$dmconIsRecord_s1pv
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1p0 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Var_ $dConstructor_s1p0
              = $s$dmconIsRecord_s1pw
        "SPEC TestEq.$dmconIsRecord [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1p3 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconIsRecord @ TestEq.Leaf $dConstructor_s1p3
              = $s$dmconIsRecord_s1px
        "SPEC TestEq.$dmconIsRecord [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1p6 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconIsRecord @ TestEq.Bin $dConstructor_s1p6
              = $s$dmconIsRecord_s1py
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Cons_]" ALWAYS
            forall {$dConstructor_s1p9 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Cons_}
              TestEq.$dmconIsRecord @ TestEq.List_Cons_ $dConstructor_s1p9
              = $s$dmconIsRecord_s1pz
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1pc [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconIsRecord @ TestEq.List_Nil_ $dConstructor_s1pc
              = $s$dmconIsRecord_s1pA]
TestEq.$dmconIsRecord =
  \ (@ c_afy)
    _
    (@ t_aFS::* -> * -> *)
    (@ a_aFT)
    (eta_B1 :: t_aFS c_afy a_aFT) ->
    GHC.Base.const
      @ GHC.Bool.Bool @ (t_aFS c_afy a_aFT) GHC.Bool.False eta_B1
end Rec }

TestEq.$con2tag_Tree :: forall t_az6.
                        TestEq.Tree t_az6 -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Tree =
  \ (@ t_az6) (ds_d19M :: TestEq.Tree t_az6) ->
    case ds_d19M of _ { TestEq.Leaf -> 0; TestEq.Bin _ _ _ -> 1 }

TestEq.$con2tag_Logic :: TestEq.Logic -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Logic =
  \ (ds_d19S :: TestEq.Logic) ->
    case ds_d19S of _ {
      TestEq.VarL _ -> 0;
      TestEq.T -> 1;
      TestEq.F -> 2;
      TestEq.Not _ -> 3;
      TestEq.Impl _ _ -> 4;
      TestEq.Equiv _ _ -> 5;
      TestEq.Conj _ _ -> 6;
      TestEq.Disj _ _ -> 7
    }

TestEq.$con2tag_Associativity :: TestEq.Associativity
                                 -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Associativity =
  \ (ds_d1ab :: TestEq.Associativity) ->
    case ds_d1ab of _ {
      TestEq.LeftAssociative -> 0;
      TestEq.RightAssociative -> 1;
      TestEq.NotAssociative -> 2
    }

TestEq.$con2tag_Fixity :: TestEq.Fixity -> GHC.Prim.Int#
LclId
[Arity 1]
TestEq.$con2tag_Fixity =
  \ (ds_d1af :: TestEq.Fixity) ->
    case ds_d1af of _ { TestEq.Prefix -> 0; TestEq.Infix _ _ -> 1 }

TestEq.eq :: forall a_afN.
             (TestEq.Representable a_afN, TestEq.Eq (TestEq.Rep a_afN)) =>
             a_afN -> a_afN -> GHC.Bool.Bool
LclIdX
[Arity 4]
TestEq.eq =
  \ (@ a_azl)
    ($dRepresentable_azt :: TestEq.Representable a_azl)
    ($dEq_azu :: TestEq.Eq (TestEq.Rep a_azl))
    (eta_B2 :: a_azl)
    (eta_B1 :: a_azl) ->
    let {
      from_s1o6 :: a_azl -> TestEq.Rep a_azl
      LclId
      []
      from_s1o6 = TestEq.from @ a_azl $dRepresentable_azt } in
    TestEq.eq'
      @ (TestEq.Rep a_azl) $dEq_azu (from_s1o6 eta_B2) (from_s1o6 eta_B1)

TestEq.eqRec :: forall t_azF.
                (TestEq.Eq t_azF) =>
                TestEq.Rec t_azF -> TestEq.Rec t_azF -> GHC.Bool.Bool
LclIdX
[Arity 3]
TestEq.eqRec =
  \ (@ t_azF)
    ($dEq_azL :: TestEq.Eq t_azF)
    (ds_d17K :: TestEq.Rec t_azF)
    (ds_d17L :: TestEq.Rec t_azF) ->
    case ds_d17K of _ { TestEq.Rec x_agu ->
    case ds_d17L of _ { TestEq.Rec x'_agv ->
    TestEq.eq' @ t_azF $dEq_azL x_agu x'_agv
    }
    }

TestEq.eqVar :: forall t_azR.
                (TestEq.Eq t_azR) =>
                TestEq.Var t_azR -> TestEq.Var t_azR -> GHC.Bool.Bool
LclIdX
[Arity 3]
TestEq.eqVar =
  \ (@ t_azR)
    ($dEq_azX :: TestEq.Eq t_azR)
    (ds_d17O :: TestEq.Var t_azR)
    (ds_d17P :: TestEq.Var t_azR) ->
    case ds_d17O of _ { TestEq.Var x_ags ->
    case ds_d17P of _ { TestEq.Var x'_agt ->
    TestEq.eq' @ t_azR $dEq_azX x_ags x'_agt
    }
    }

TestEq.eqC :: forall t_aA2 t_aA4 t_aA5.
              (TestEq.Eq t_aA4) =>
              TestEq.C t_aA2 t_aA4 -> TestEq.C t_aA5 t_aA4 -> GHC.Bool.Bool
LclIdX
[Arity 3]
TestEq.eqC =
  \ (@ t_aA2)
    (@ t_aA4)
    (@ t_aA5)
    ($dEq_aAb :: TestEq.Eq t_aA4)
    (ds_d17S :: TestEq.C t_aA2 t_aA4)
    (ds_d17T :: TestEq.C t_aA5 t_aA4) ->
    case ds_d17S of _ { TestEq.C a_agq ->
    case ds_d17T of _ { TestEq.C a'_agr ->
    TestEq.eq' @ t_aA4 $dEq_aAb a_agq a'_agr
    }
    }

TestEq.eqTimes :: forall t_aAi t_aAj.
                  (TestEq.Eq t_aAi, TestEq.Eq t_aAj) =>
                  (t_aAi TestEq.:*: t_aAj)
                  -> (t_aAi TestEq.:*: t_aAj)
                  -> GHC.Bool.Bool
LclIdX
[Arity 4]
TestEq.eqTimes =
  \ (@ t_aAi)
    (@ t_aAj)
    ($dEq_aAt :: TestEq.Eq t_aAi)
    ($dEq_aAu :: TestEq.Eq t_aAj)
    (ds_d17W :: t_aAi TestEq.:*: t_aAj)
    (ds_d17X :: t_aAi TestEq.:*: t_aAj) ->
    case ds_d17W of _ { TestEq.:*: a_agi b_agj ->
    case ds_d17X of _ { TestEq.:*: a'_agk b'_agl ->
    GHC.Classes.&&
      (TestEq.eq' @ t_aAi $dEq_aAt a_agi a'_agk)
      (TestEq.eq' @ t_aAj $dEq_aAu b_agj b'_agl)
    }
    }

TestEq.eqPlus :: forall t_aAB t_aAH.
                 (TestEq.Eq t_aAB, TestEq.Eq t_aAH) =>
                 (t_aAB TestEq.:+: t_aAH)
                 -> (t_aAB TestEq.:+: t_aAH)
                 -> GHC.Bool.Bool
LclIdX
[Arity 4]
TestEq.eqPlus =
  \ (@ t_aAB)
    (@ t_aAH)
    ($dEq_aAM :: TestEq.Eq t_aAB)
    ($dEq_aAN :: TestEq.Eq t_aAH)
    (ds_d180 :: t_aAB TestEq.:+: t_aAH)
    (ds_d181 :: t_aAB TestEq.:+: t_aAH) ->
    case ds_d180 of _ {
      TestEq.L x_age ->
        case ds_d181 of _ {
          TestEq.L x'_agf -> TestEq.eq' @ t_aAB $dEq_aAM x_age x'_agf;
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds_d181 of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh -> TestEq.eq' @ t_aAH $dEq_aAN x_agg x'_agh
        }
    }

TestEq.eqU :: forall t_aAS t_aAT. t_aAS -> t_aAT -> GHC.Bool.Bool
LclIdX
[Arity 2]
TestEq.eqU = \ (@ t_aAS) (@ t_aAT) _ _ -> GHC.Bool.True

TestEq.toLogic :: forall t_aB0
                         t_aB6
                         t_aBa
                         t_aBe
                         t_aBk
                         t_aBu
                         t_aBE
                         t_aBM.
                  (TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                   TestEq.:+: (TestEq.C t_aB6 TestEq.U
                               TestEq.:+: (TestEq.C t_aBa TestEq.U
                                           TestEq.:+: (TestEq.C t_aBe (TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     t_aBk
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 t_aBu
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: (TestEq.C
                                                                                             t_aBE
                                                                                             (TestEq.Rec
                                                                                                TestEq.Logic
                                                                                              TestEq.:*: TestEq.Rec
                                                                                                           TestEq.Logic)
                                                                                           TestEq.:+: TestEq.C
                                                                                                        t_aBM
                                                                                                        (TestEq.Rec
                                                                                                           TestEq.Logic
                                                                                                         TestEq.:*: TestEq.Rec
                                                                                                                      TestEq.Logic))))))))
                  -> TestEq.Logic
LclIdX
[Arity 1]
TestEq.toLogic =
  \ (@ t_aB0)
    (@ t_aB6)
    (@ t_aBa)
    (@ t_aBe)
    (@ t_aBk)
    (@ t_aBu)
    (@ t_aBE)
    (@ t_aBM)
    (ds_d18a :: TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                TestEq.:+: (TestEq.C t_aB6 TestEq.U
                            TestEq.:+: (TestEq.C t_aBa TestEq.U
                                        TestEq.:+: (TestEq.C t_aBe (TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  t_aBk
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: (TestEq.C
                                                                              t_aBu
                                                                              (TestEq.Rec
                                                                                 TestEq.Logic
                                                                               TestEq.:*: TestEq.Rec
                                                                                            TestEq.Logic)
                                                                            TestEq.:+: (TestEq.C
                                                                                          t_aBE
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic)
                                                                                        TestEq.:+: TestEq.C
                                                                                                     t_aBM
                                                                                                     (TestEq.Rec
                                                                                                        TestEq.Logic
                                                                                                      TestEq.:*: TestEq.Rec
                                                                                                                   TestEq.Logic)))))))) ->
    case ds_d18a of _ {
      TestEq.L ds_d18D ->
        case ds_d18D of _ { TestEq.C ds_d18E ->
        case ds_d18E of _ { TestEq.Var f0_ag4 -> TestEq.VarL f0_ag4 }
        };
      TestEq.R ds_d18b ->
        case ds_d18b of _ {
          TestEq.L ds_d18B ->
            case ds_d18B of _ { TestEq.C ds_d18C ->
            case ds_d18C of _ { TestEq.U -> TestEq.T }
            };
          TestEq.R ds_d18c ->
            case ds_d18c of _ {
              TestEq.L ds_d18z ->
                case ds_d18z of _ { TestEq.C ds_d18A ->
                case ds_d18A of _ { TestEq.U -> TestEq.F }
                };
              TestEq.R ds_d18d ->
                case ds_d18d of _ {
                  TestEq.L ds_d18x ->
                    case ds_d18x of _ { TestEq.C ds_d18y ->
                    case ds_d18y of _ { TestEq.Rec f0_ag5 -> TestEq.Not f0_ag5 }
                    };
                  TestEq.R ds_d18e ->
                    case ds_d18e of _ {
                      TestEq.L ds_d18t ->
                        case ds_d18t of _ { TestEq.C ds_d18u ->
                        case ds_d18u of _ { TestEq.:*: ds_d18v ds_d18w ->
                        case ds_d18v of _ { TestEq.Rec f0_ag6 ->
                        case ds_d18w of _ { TestEq.Rec f1_ag7 ->
                        TestEq.Impl f0_ag6 f1_ag7
                        }
                        }
                        }
                        };
                      TestEq.R ds_d18f ->
                        case ds_d18f of _ {
                          TestEq.L ds_d18p ->
                            case ds_d18p of _ { TestEq.C ds_d18q ->
                            case ds_d18q of _ { TestEq.:*: ds_d18r ds_d18s ->
                            case ds_d18r of _ { TestEq.Rec f0_ag8 ->
                            case ds_d18s of _ { TestEq.Rec f1_ag9 ->
                            TestEq.Equiv f0_ag8 f1_ag9
                            }
                            }
                            }
                            };
                          TestEq.R ds_d18g ->
                            case ds_d18g of _ {
                              TestEq.L ds_d18l ->
                                case ds_d18l of _ { TestEq.C ds_d18m ->
                                case ds_d18m of _ { TestEq.:*: ds_d18n ds_d18o ->
                                case ds_d18n of _ { TestEq.Rec f0_aga ->
                                case ds_d18o of _ { TestEq.Rec f1_agb ->
                                TestEq.Conj f0_aga f1_agb
                                }
                                }
                                }
                                };
                              TestEq.R ds_d18h ->
                                case ds_d18h of _ { TestEq.C ds_d18i ->
                                case ds_d18i of _ { TestEq.:*: ds_d18j ds_d18k ->
                                case ds_d18j of _ { TestEq.Rec f0_agc ->
                                case ds_d18k of _ { TestEq.Rec f1_agd ->
                                TestEq.Disj f0_agc f1_agd
                                }
                                }
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

TestEq.fromLogic :: forall c_aC3
                           c_aCb
                           c_aCl
                           c_aCz
                           c_aCT
                           c_aDg
                           c_aDG
                           c_aE7.
                    TestEq.Logic
                    -> TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C c_aCb TestEq.U
                                   TestEq.:+: (TestEq.C c_aCl TestEq.U
                                               TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         c_aCT
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     c_aDg
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 c_aDG
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            c_aE7
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic)))))))
LclIdX
[Arity 1]
TestEq.fromLogic =
  \ (@ c_aC3)
    (@ c_aCb)
    (@ c_aCl)
    (@ c_aCz)
    (@ c_aCT)
    (@ c_aDg)
    (@ c_aDG)
    (@ c_aE7)
    (ds_d19h :: TestEq.Logic) ->
    case ds_d19h of _ {
      TestEq.VarL f0_afU ->
        TestEq.L
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.C
             @ c_aC3
             @ (TestEq.Var GHC.Base.String)
             (TestEq.Var @ GHC.Base.String f0_afU));
      TestEq.T ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.L
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.C @ c_aCb @ TestEq.U TestEq.U));
      TestEq.F ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.L
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.C @ c_aCl @ TestEq.U TestEq.U)));
      TestEq.Not f0_afV ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.L
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.C
                      @ c_aCz
                      @ (TestEq.Rec TestEq.Logic)
                      (TestEq.Rec @ TestEq.Logic f0_afV)))));
      TestEq.Impl f0_afW f1_afX ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.L
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.C
                         @ c_aCT
                         @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         (TestEq.:*:
                            @ (TestEq.Rec TestEq.Logic)
                            @ (TestEq.Rec TestEq.Logic)
                            (TestEq.Rec @ TestEq.Logic f0_afW)
                            (TestEq.Rec @ TestEq.Logic f1_afX)))))));
      TestEq.Equiv f0_afY f1_afZ ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.L
                         @ (TestEq.C
                              c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.C
                            @ c_aDg
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            (TestEq.:*:
                               @ (TestEq.Rec TestEq.Logic)
                               @ (TestEq.Rec TestEq.Logic)
                               (TestEq.Rec @ TestEq.Logic f0_afY)
                               (TestEq.Rec @ TestEq.Logic f1_afZ))))))));
      TestEq.Conj f0_ag0 f1_ag1 ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.R
                         @ (TestEq.C
                              c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.L
                            @ (TestEq.C
                                 c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            @ (TestEq.C
                                 c_aE7 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            (TestEq.C
                               @ c_aDG
                               @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               (TestEq.:*:
                                  @ (TestEq.Rec TestEq.Logic)
                                  @ (TestEq.Rec TestEq.Logic)
                                  (TestEq.Rec @ TestEq.Logic f0_ag0)
                                  (TestEq.Rec @ TestEq.Logic f1_ag1)))))))));
      TestEq.Disj f0_ag2 f1_ag3 ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.R
                         @ (TestEq.C
                              c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.R
                            @ (TestEq.C
                                 c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            @ (TestEq.C
                                 c_aE7 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            (TestEq.C
                               @ c_aE7
                               @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               (TestEq.:*:
                                  @ (TestEq.Rec TestEq.Logic)
                                  @ (TestEq.Rec TestEq.Logic)
                                  (TestEq.Rec @ TestEq.Logic f0_ag2)
                                  (TestEq.Rec @ TestEq.Logic f1_ag3)))))))))
    }

TestEq.toTree :: forall t_aEI t_aEN t_aEV.
                 (TestEq.C t_aEV TestEq.U
                  TestEq.:+: TestEq.C
                               t_aEI
                               (TestEq.Var t_aEN
                                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aEN))))
                 -> TestEq.Tree t_aEN
LclIdX
[Arity 1]
TestEq.toTree =
  \ (@ t_aEI)
    (@ t_aEN)
    (@ t_aEV)
    (ds_d19q :: TestEq.C t_aEV TestEq.U
                TestEq.:+: TestEq.C
                             t_aEI
                             (TestEq.Var t_aEN
                              TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree t_aEN)))) ->
    case ds_d19q of _ {
      TestEq.L ds_d19x ->
        case ds_d19x of _ { TestEq.C ds_d19y ->
        case ds_d19y of _ { TestEq.U -> TestEq.Leaf @ t_aEN }
        };
      TestEq.R ds_d19r ->
        case ds_d19r of _ { TestEq.C ds_d19s ->
        case ds_d19s of _ { TestEq.:*: ds_d19t ds_d19u ->
        case ds_d19t of _ { TestEq.Var x_afR ->
        case ds_d19u of _ { TestEq.:*: ds_d19v ds_d19w ->
        case ds_d19v of _ { TestEq.Rec l_afS ->
        case ds_d19w of _ { TestEq.Rec r_afT ->
        TestEq.Bin @ t_aEN x_afR l_afS r_afT
        }
        }
        }
        }
        }
        }
    }

TestEq.fromTree :: forall t_aF3 c_aFf c_aFl.
                   TestEq.Tree t_aF3
                   -> TestEq.C c_aFl TestEq.U
                      TestEq.:+: TestEq.C
                                   c_aFf
                                   (TestEq.Var t_aF3
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
LclIdX
[Arity 1]
TestEq.fromTree =
  \ (@ t_aF3) (@ c_aFf) (@ c_aFl) (ds_d19J :: TestEq.Tree t_aF3) ->
    case ds_d19J of _ {
      TestEq.Leaf ->
        TestEq.L
          @ (TestEq.C c_aFl TestEq.U)
          @ (TestEq.C
               c_aFf
               (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
          (TestEq.C @ c_aFl @ TestEq.U TestEq.U);
      TestEq.Bin x_afO l_afP r_afQ ->
        TestEq.R
          @ (TestEq.C c_aFl TestEq.U)
          @ (TestEq.C
               c_aFf
               (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
          (TestEq.C
             @ c_aFf
             @ (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
             (TestEq.:*:
                @ (TestEq.Var t_aF3)
                @ (TestEq.Rec (TestEq.Tree t_aF3)
                   TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))
                (TestEq.Var @ t_aF3 x_afO)
                (TestEq.:*:
                   @ (TestEq.Rec (TestEq.Tree t_aF3))
                   @ (TestEq.Rec (TestEq.Tree t_aF3))
                   (TestEq.Rec @ (TestEq.Tree t_aF3) l_afP)
                   (TestEq.Rec @ (TestEq.Tree t_aF3) r_afQ))))
    }

a_s1dD :: GHC.Types.Int
LclId
[]
a_s1dD = GHC.Types.I# 3

TestEq.tree0 :: TestEq.Tree GHC.Types.Int
LclIdX
[]
TestEq.tree0 =
  TestEq.Bin
    @ GHC.Types.Int
    a_s1dD
    (TestEq.Leaf @ GHC.Types.Int)
    (TestEq.Leaf @ GHC.Types.Int)

a_s1dF :: GHC.Types.Int
LclId
[]
a_s1dF = GHC.Types.I# 5

TestEq.tree1 :: TestEq.Tree GHC.Types.Int
LclIdX
[]
TestEq.tree1 =
  TestEq.Bin @ GHC.Types.Int a_s1dF TestEq.tree0 TestEq.tree0

a_s1dK :: GHC.Types.Char
LclId
[]
a_s1dK = GHC.Types.C# 'x'

a_s1dL :: [GHC.Types.Char]
LclId
[]
a_s1dL =
  GHC.Types.: @ GHC.Types.Char a_s1dK (GHC.Types.[] @ GHC.Types.Char)

a_s1dM :: TestEq.Logic
LclId
[]
a_s1dM = TestEq.VarL a_s1dL

a_s1dO :: TestEq.Logic
LclId
[]
a_s1dO = TestEq.Not TestEq.T

a_s1dP :: TestEq.Logic
LclId
[]
a_s1dP = TestEq.Equiv a_s1dM a_s1dO

a_s1dR :: TestEq.Logic
LclId
[]
a_s1dR = TestEq.Not TestEq.F

TestEq.logic1 :: TestEq.Logic
LclIdX
[]
TestEq.logic1 = TestEq.Impl a_s1dP a_s1dR

TestEq.$fRepresentableInt :: TestEq.Representable GHC.Types.Int
LclIdX[DFunId]
[]
TestEq.$fRepresentableInt =
  TestEq.D:Representable
    @ GHC.Types.Int
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Int))
     `cast` (TestEq.Rep GHC.Types.Int -> TestEq.TFCo:R:RepInt
             :: (TestEq.Rep GHC.Types.Int -> TestEq.Rep GHC.Types.Int)
                  ~
                (TestEq.Rep GHC.Types.Int -> TestEq.R:RepInt)))
    ((GHC.Base.id @ GHC.Types.Int)
     `cast` (GHC.Types.Int -> sym TestEq.TFCo:R:RepInt
             :: (GHC.Types.Int -> TestEq.R:RepInt)
                  ~
                (GHC.Types.Int -> TestEq.Rep GHC.Types.Int)))

TestEq.$fRepresentableChar :: TestEq.Representable GHC.Types.Char
LclIdX[DFunId]
[]
TestEq.$fRepresentableChar =
  TestEq.D:Representable
    @ GHC.Types.Char
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Char))
     `cast` (TestEq.Rep GHC.Types.Char -> TestEq.TFCo:R:RepChar
             :: (TestEq.Rep GHC.Types.Char -> TestEq.Rep GHC.Types.Char)
                  ~
                (TestEq.Rep GHC.Types.Char -> TestEq.R:RepChar)))
    ((GHC.Base.id @ GHC.Types.Char)
     `cast` (GHC.Types.Char -> sym TestEq.TFCo:R:RepChar
             :: (GHC.Types.Char -> TestEq.R:RepChar)
                  ~
                (GHC.Types.Char -> TestEq.Rep GHC.Types.Char)))

TestEq.$fRepresentableU :: TestEq.Representable TestEq.U
LclIdX[DFunId]
[]
TestEq.$fRepresentableU =
  TestEq.D:Representable
    @ TestEq.U
    ((GHC.Base.id @ (TestEq.Rep TestEq.U))
     `cast` (TestEq.Rep TestEq.U -> TestEq.TFCo:R:RepU
             :: (TestEq.Rep TestEq.U -> TestEq.Rep TestEq.U)
                  ~
                (TestEq.Rep TestEq.U -> TestEq.R:RepU)))
    ((GHC.Base.id @ TestEq.U)
     `cast` (TestEq.U -> sym TestEq.TFCo:R:RepU
             :: (TestEq.U -> TestEq.R:RepU)
                  ~
                (TestEq.U -> TestEq.Rep TestEq.U)))

a_s1dV :: forall a_ahe b_ahf.
          (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
          (a_ahe TestEq.:*: b_ahf) -> a_ahe TestEq.:*: b_ahf
LclId
[Arity 3]
a_s1dV =
  \ (@ a_ahe) (@ b_ahf) _ _ (eta_B1 :: a_ahe TestEq.:*: b_ahf) ->
    GHC.Base.id @ (a_ahe TestEq.:*: b_ahf) eta_B1

a_s1dX :: forall a_Xns b_Xnu.
          (TestEq.Representable a_Xns, TestEq.Representable b_Xnu) =>
          TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
          -> TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
LclId
[Arity 3]
a_s1dX =
  \ (@ a_Xns)
    (@ b_Xnu)
    _
    _
    (eta_B1 :: TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) ->
    GHC.Base.id @ (TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) eta_B1

TestEq.$fRepresentable:*: :: forall a_ahe b_ahf.
                             (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                             TestEq.Representable (a_ahe TestEq.:*: b_ahf)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRepresentable:*: =
  __inline_me (\ (@ a_Xnx)
                 (@ b_Xnz)
                 ($dRepresentable_X1bW :: TestEq.Representable a_Xnx)
                 ($dRepresentable_X1bY :: TestEq.Representable b_Xnz) ->
                 TestEq.D:Representable
                   @ (a_Xnx TestEq.:*: b_Xnz)
                   ((a_s1dX @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                            -> TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz
                            :: (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz))
                                 ~
                               (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.R:Rep:*: a_Xnx b_Xnz)))
                   ((a_s1dV @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` ((a_Xnx TestEq.:*: b_Xnz)
                            -> sym (TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz)
                            :: ((a_Xnx TestEq.:*: b_Xnz) -> TestEq.R:Rep:*: a_Xnx b_Xnz)
                                 ~
                               ((a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)))))

a_s1dZ :: forall a_aha b_ahb.
          (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
          (a_aha TestEq.:+: b_ahb) -> a_aha TestEq.:+: b_ahb
LclId
[Arity 3]
a_s1dZ =
  \ (@ a_aha) (@ b_ahb) _ _ (eta_B1 :: a_aha TestEq.:+: b_ahb) ->
    GHC.Base.id @ (a_aha TestEq.:+: b_ahb) eta_B1

a_s1e1 :: forall a_Xnz b_XnB.
          (TestEq.Representable a_Xnz, TestEq.Representable b_XnB) =>
          TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
          -> TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
LclId
[Arity 3]
a_s1e1 =
  \ (@ a_Xnz)
    (@ b_XnB)
    _
    _
    (eta_B1 :: TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) ->
    GHC.Base.id @ (TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) eta_B1

TestEq.$fRepresentable:+: :: forall a_aha b_ahb.
                             (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                             TestEq.Representable (a_aha TestEq.:+: b_ahb)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRepresentable:+: =
  __inline_me (\ (@ a_XnE)
                 (@ b_XnG)
                 ($dRepresentable_X1bU :: TestEq.Representable a_XnE)
                 ($dRepresentable_X1bW :: TestEq.Representable b_XnG) ->
                 TestEq.D:Representable
                   @ (a_XnE TestEq.:+: b_XnG)
                   ((a_s1e1 @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                            -> TestEq.TFCo:R:Rep:+: a_XnE b_XnG
                            :: (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG))
                                 ~
                               (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.R:Rep:+: a_XnE b_XnG)))
                   ((a_s1dZ @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` ((a_XnE TestEq.:+: b_XnG)
                            -> sym (TestEq.TFCo:R:Rep:+: a_XnE b_XnG)
                            :: ((a_XnE TestEq.:+: b_XnG) -> TestEq.R:Rep:+: a_XnE b_XnG)
                                 ~
                               ((a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG)))))

a_s1e3 :: forall a_ah6 c_ah7.
          (TestEq.Representable a_ah6) =>
          TestEq.C c_ah7 a_ah6 -> TestEq.C c_ah7 a_ah6
LclId
[Arity 2]
a_s1e3 =
  \ (@ a_ah6) (@ c_ah7) _ (eta_B1 :: TestEq.C c_ah7 a_ah6) ->
    GHC.Base.id @ (TestEq.C c_ah7 a_ah6) eta_B1

a_s1e5 :: forall a_XnF c_XnH.
          (TestEq.Representable a_XnF) =>
          TestEq.Rep (TestEq.C c_XnH a_XnF)
          -> TestEq.Rep (TestEq.C c_XnH a_XnF)
LclId
[Arity 2]
a_s1e5 =
  \ (@ a_XnF)
    (@ c_XnH)
    _
    (eta_B1 :: TestEq.Rep (TestEq.C c_XnH a_XnF)) ->
    GHC.Base.id @ (TestEq.Rep (TestEq.C c_XnH a_XnF)) eta_B1

TestEq.$fRepresentableC :: forall a_ah6 c_ah7.
                           (TestEq.Representable a_ah6) =>
                           TestEq.Representable (TestEq.C c_ah7 a_ah6)
LclIdX[DFunId]
[Arity 1]
TestEq.$fRepresentableC =
  __inline_me (\ (@ a_XnJ)
                 (@ c_XnL)
                 ($dRepresentable_X1iw :: TestEq.Representable a_XnJ) ->
                 TestEq.D:Representable
                   @ (TestEq.C c_XnL a_XnJ)
                   ((a_s1e5 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                            -> TestEq.TFCo:R:RepC c_XnL a_XnJ
                            :: (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                                -> TestEq.Rep (TestEq.C c_XnL a_XnJ))
                                 ~
                               (TestEq.Rep (TestEq.C c_XnL a_XnJ) -> TestEq.R:RepC c_XnL a_XnJ)))
                   ((a_s1e3 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.C c_XnL a_XnJ
                            -> sym (TestEq.TFCo:R:RepC c_XnL a_XnJ)
                            :: (TestEq.C c_XnL a_XnJ -> TestEq.R:RepC c_XnL a_XnJ)
                                 ~
                               (TestEq.C c_XnL a_XnJ -> TestEq.Rep (TestEq.C c_XnL a_XnJ)))))

a_s1e7 :: forall a_ah4.
          (TestEq.Representable a_ah4) =>
          TestEq.Var a_ah4 -> TestEq.Var a_ah4
LclId
[Arity 2]
a_s1e7 =
  \ (@ a_ah4) _ (eta_B1 :: TestEq.Var a_ah4) ->
    GHC.Base.id @ (TestEq.Var a_ah4) eta_B1

a_s1e9 :: forall a_XnL.
          (TestEq.Representable a_XnL) =>
          TestEq.Rep (TestEq.Var a_XnL) -> TestEq.Rep (TestEq.Var a_XnL)
LclId
[Arity 2]
a_s1e9 =
  \ (@ a_XnL) _ (eta_B1 :: TestEq.Rep (TestEq.Var a_XnL)) ->
    GHC.Base.id @ (TestEq.Rep (TestEq.Var a_XnL)) eta_B1

TestEq.$fRepresentableVar :: forall a_ah4.
                             (TestEq.Representable a_ah4) =>
                             TestEq.Representable (TestEq.Var a_ah4)
LclIdX[DFunId]
[Arity 1]
TestEq.$fRepresentableVar =
  __inline_me (\ (@ a_XnO)
                 ($dRepresentable_X1bL :: TestEq.Representable a_XnO) ->
                 TestEq.D:Representable
                   @ (TestEq.Var a_XnO)
                   ((a_s1e9 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.TFCo:R:RepVar a_XnO
                            :: (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.Rep (TestEq.Var a_XnO))
                                 ~
                               (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.R:RepVar a_XnO)))
                   ((a_s1e7 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Var a_XnO -> sym (TestEq.TFCo:R:RepVar a_XnO)
                            :: (TestEq.Var a_XnO -> TestEq.R:RepVar a_XnO)
                                 ~
                               (TestEq.Var a_XnO -> TestEq.Rep (TestEq.Var a_XnO)))))

a_s1eb :: forall a_ah2.
          (TestEq.Representable a_ah2) =>
          TestEq.Rec a_ah2 -> TestEq.Rec a_ah2
LclId
[Arity 2]
a_s1eb =
  \ (@ a_ah2) _ (eta_B1 :: TestEq.Rec a_ah2) ->
    GHC.Base.id @ (TestEq.Rec a_ah2) eta_B1

a_s1ed :: forall a_XnQ.
          (TestEq.Representable a_XnQ) =>
          TestEq.Rep (TestEq.Rec a_XnQ) -> TestEq.Rep (TestEq.Rec a_XnQ)
LclId
[Arity 2]
a_s1ed =
  \ (@ a_XnQ) _ (eta_B1 :: TestEq.Rep (TestEq.Rec a_XnQ)) ->
    GHC.Base.id @ (TestEq.Rep (TestEq.Rec a_XnQ)) eta_B1

TestEq.$fRepresentableRec :: forall a_ah2.
                             (TestEq.Representable a_ah2) =>
                             TestEq.Representable (TestEq.Rec a_ah2)
LclIdX[DFunId]
[Arity 1]
TestEq.$fRepresentableRec =
  __inline_me (\ (@ a_XnT)
                 ($dRepresentable_X1bF :: TestEq.Representable a_XnT) ->
                 TestEq.D:Representable
                   @ (TestEq.Rec a_XnT)
                   ((a_s1ed @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.TFCo:R:RepRec a_XnT
                            :: (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.Rep (TestEq.Rec a_XnT))
                                 ~
                               (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.R:RepRec a_XnT)))
                   ((a_s1eb @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rec a_XnT -> sym (TestEq.TFCo:R:RepRec a_XnT)
                            :: (TestEq.Rec a_XnT -> TestEq.R:RepRec a_XnT)
                                 ~
                               (TestEq.Rec a_XnT -> TestEq.Rep (TestEq.Rec a_XnT)))))

from_a14M :: forall a_agN. [a_agN] -> TestEq.Rep [a_agN]
LclId
[Arity 1]
from_a14M =
  \ (@ a_agN) (ds_d1dc :: [a_agN]) ->
    case ds_d1dc of _ {
      [] ->
        (TestEq.L
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C @ TestEq.List_Nil_ @ TestEq.U TestEq.U))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN]);
      : a_agO as_agP ->
        (TestEq.R
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C
              @ TestEq.List_Cons_
              @ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
              (TestEq.:*:
                 @ (TestEq.Var a_agN)
                 @ (TestEq.Rec [a_agN])
                 (TestEq.Var @ a_agN a_agO)
                 (TestEq.Rec @ [a_agN] as_agP))))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN])
    }

to_a14s :: forall a_agN. TestEq.Rep [a_agN] -> [a_agN]
LclId
[Arity 1]
to_a14s =
  \ (@ a_agN) (ds_d1cW :: TestEq.Rep [a_agN]) ->
    case ds_d1cW
         `cast` (sym (sym (TestEq.TFCo:R:Rep[] a_agN))
                 :: TestEq.Rep [a_agN] ~ TestEq.R:Rep[] a_agN)
    of _ {
      TestEq.L ds_d1d2 ->
        case ds_d1d2 of _ { TestEq.C ds_d1d3 ->
        case ds_d1d3 of _ { TestEq.U -> GHC.Types.[] @ a_agN }
        };
      TestEq.R ds_d1cY ->
        case ds_d1cY of _ { TestEq.C ds_d1cZ ->
        case ds_d1cZ of _ { TestEq.:*: ds_d1d0 ds_d1d1 ->
        case ds_d1d0 of _ { TestEq.Var a_agZ ->
        case ds_d1d1 of _ { TestEq.Rec as_ah0 ->
        GHC.Types.: @ a_agN a_agZ as_ah0
        }
        }
        }
        }
    }

TestEq.$fRepresentable[] :: forall a_agN.
                            TestEq.Representable [a_agN]
LclIdX[DFunId]
[]
TestEq.$fRepresentable[] =
  \ (@ a_agN) ->
    TestEq.D:Representable
      @ [a_agN] (to_a14s @ a_agN) (from_a14M @ a_agN)

a_s1et :: forall a_agL.
          TestEq.Tree a_agL
          -> TestEq.C TestEq.Leaf TestEq.U
             TestEq.:+: TestEq.C
                          TestEq.Bin
                          (TestEq.Var a_agL
                           TestEq.:*: (TestEq.Rec (TestEq.Tree a_agL)
                                       TestEq.:*: TestEq.Rec (TestEq.Tree a_agL)))
LclId
[Arity 1]
a_s1et =
  \ (@ a_agL) (sub_a178 :: TestEq.Tree a_agL) ->
    TestEq.fromTree @ a_agL @ TestEq.Bin @ TestEq.Leaf sub_a178

TestEq.$fRepresentableTree :: forall a_agL.
                              TestEq.Representable (TestEq.Tree a_agL)
LclIdX[DFunId]
[]
TestEq.$fRepresentableTree =
  \ (@ a_XnZ) ->
    TestEq.D:Representable
      @ (TestEq.Tree a_XnZ)
      ((TestEq.toTree @ TestEq.Bin @ a_XnZ @ TestEq.Leaf)
       `cast` (sym (TestEq.TFCo:R:RepTree a_XnZ) -> TestEq.Tree a_XnZ
               :: (TestEq.R:RepTree a_XnZ -> TestEq.Tree a_XnZ)
                    ~
                  (TestEq.Rep (TestEq.Tree a_XnZ) -> TestEq.Tree a_XnZ)))
      ((a_s1et @ a_XnZ)
       `cast` (TestEq.Tree a_XnZ -> sym (TestEq.TFCo:R:RepTree a_XnZ)
               :: (TestEq.Tree a_XnZ -> TestEq.R:RepTree a_XnZ)
                    ~
                  (TestEq.Tree a_XnZ -> TestEq.Rep (TestEq.Tree a_XnZ))))

TestEq.$fRepresentableLogic :: TestEq.Representable TestEq.Logic
LclIdX[DFunId]
[]
TestEq.$fRepresentableLogic =
  TestEq.D:Representable
    @ TestEq.Logic
    ((TestEq.toLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (sym TestEq.TFCo:R:RepLogic -> TestEq.Logic
             :: (TestEq.R:RepLogic -> TestEq.Logic)
                  ~
                (TestEq.Rep TestEq.Logic -> TestEq.Logic)))
    ((TestEq.fromLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (TestEq.Logic -> sym TestEq.TFCo:R:RepLogic
             :: (TestEq.Logic -> TestEq.R:RepLogic)
                  ~
                (TestEq.Logic -> TestEq.Rep TestEq.Logic)))

TestEq.$fEqU :: TestEq.Eq TestEq.U
LclIdX[DFunId]
[Arity 2]
TestEq.$fEqU =
  (TestEq.eqU @ TestEq.U @ TestEq.U)
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.U)
          :: (TestEq.U -> TestEq.U -> GHC.Bool.Bool) ~ TestEq.T:Eq TestEq.U)

Rec {
$dEq_a16O :: TestEq.Eq
               (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
LclId
[Arity 2]
$dEq_a16O =
  TestEq.$fEq:*:
    @ (TestEq.Rec TestEq.Logic)
    @ (TestEq.Rec TestEq.Logic)
    $dEq_a16K
    $dEq_a16K
eq'_aEx :: (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
            TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                        TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                    TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              GHC.Prim.Any
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          GHC.Prim.Any
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: (TestEq.C
                                                                                      GHC.Prim.Any
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)
                                                                                    TestEq.:+: TestEq.C
                                                                                                 GHC.Prim.Any
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic))))))))
           -> (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
               TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                           TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                                       TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                                   TestEq.:+: (TestEq.C
                                                                 GHC.Prim.Any
                                                                 (TestEq.Rec TestEq.Logic
                                                                  TestEq.:*: TestEq.Rec
                                                                               TestEq.Logic)
                                                               TestEq.:+: (TestEq.C
                                                                             GHC.Prim.Any
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic)
                                                                           TestEq.:+: (TestEq.C
                                                                                         GHC.Prim.Any
                                                                                         (TestEq.Rec
                                                                                            TestEq.Logic
                                                                                          TestEq.:*: TestEq.Rec
                                                                                                       TestEq.Logic)
                                                                                       TestEq.:+: TestEq.C
                                                                                                    GHC.Prim.Any
                                                                                                    (TestEq.Rec
                                                                                                       TestEq.Logic
                                                                                                     TestEq.:*: TestEq.Rec
                                                                                                                  TestEq.Logic))))))))
           -> GHC.Bool.Bool
LclId
[]
eq'_aEx =
  TestEq.eq'
    @ (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String)
       TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                   TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                               TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         GHC.Prim.Any
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     GHC.Prim.Any
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 GHC.Prim.Any
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: TestEq.C
                                                                                            GHC.Prim.Any
                                                                                            (TestEq.Rec
                                                                                               TestEq.Logic
                                                                                             TestEq.:*: TestEq.Rec
                                                                                                          TestEq.Logic))))))))
    (TestEq.$fEq:+:
       @ (TestEq.C GHC.Prim.Any (TestEq.Var GHC.Base.String))
       @ (TestEq.C GHC.Prim.Any TestEq.U
          TestEq.:+: (TestEq.C GHC.Prim.Any TestEq.U
                      TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                GHC.Prim.Any
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: (TestEq.C
                                                            GHC.Prim.Any
                                                            (TestEq.Rec TestEq.Logic
                                                             TestEq.:*: TestEq.Rec TestEq.Logic)
                                                          TestEq.:+: (TestEq.C
                                                                        GHC.Prim.Any
                                                                        (TestEq.Rec TestEq.Logic
                                                                         TestEq.:*: TestEq.Rec
                                                                                      TestEq.Logic)
                                                                      TestEq.:+: TestEq.C
                                                                                   GHC.Prim.Any
                                                                                   (TestEq.Rec
                                                                                      TestEq.Logic
                                                                                    TestEq.:*: TestEq.Rec
                                                                                                 TestEq.Logic)))))))
       (TestEq.$fEqC
          @ (TestEq.Var GHC.Base.String)
          @ GHC.Prim.Any
          (TestEq.$fEqVar
             @ [GHC.Types.Char]
             (TestEq.$fEq[] @ GHC.Types.Char TestEq.$fEqChar)))
       (TestEq.$fEq:+:
          @ (TestEq.C GHC.Prim.Any TestEq.U)
          @ (TestEq.C GHC.Prim.Any TestEq.U
             TestEq.:+: (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       GHC.Prim.Any
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   GHC.Prim.Any
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               GHC.Prim.Any
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: TestEq.C
                                                                          GHC.Prim.Any
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic))))))
          (TestEq.$fEqC @ TestEq.U @ GHC.Prim.Any TestEq.$fEqU)
          (TestEq.$fEq:+:
             @ (TestEq.C GHC.Prim.Any TestEq.U)
             @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic)
                TestEq.:+: (TestEq.C
                              GHC.Prim.Any
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          GHC.Prim.Any
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      GHC.Prim.Any
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: TestEq.C
                                                                 GHC.Prim.Any
                                                                 (TestEq.Rec TestEq.Logic
                                                                  TestEq.:*: TestEq.Rec
                                                                               TestEq.Logic)))))
             (TestEq.$fEqC @ TestEq.U @ GHC.Prim.Any TestEq.$fEqU)
             (TestEq.$fEq:+:
                @ (TestEq.C GHC.Prim.Any (TestEq.Rec TestEq.Logic))
                @ (TestEq.C
                     GHC.Prim.Any
                     (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 GHC.Prim.Any
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             GHC.Prim.Any
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: TestEq.C
                                                        GHC.Prim.Any
                                                        (TestEq.Rec TestEq.Logic
                                                         TestEq.:*: TestEq.Rec TestEq.Logic))))
                (TestEq.$fEqC @ (TestEq.Rec TestEq.Logic) @ GHC.Prim.Any $dEq_a16K)
                (TestEq.$fEq:+:
                   @ (TestEq.C
                        GHC.Prim.Any
                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        GHC.Prim.Any
                        (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    GHC.Prim.Any
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: TestEq.C
                                               GHC.Prim.Any
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)))
                   (TestEq.$fEqC
                      @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      @ GHC.Prim.Any
                      $dEq_a16O)
                   (TestEq.$fEq:+:
                      @ (TestEq.C
                           GHC.Prim.Any
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           GHC.Prim.Any
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: TestEq.C
                                      GHC.Prim.Any
                                      (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      (TestEq.$fEqC
                         @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         @ GHC.Prim.Any
                         $dEq_a16O)
                      (TestEq.$fEq:+:
                         @ (TestEq.C
                              GHC.Prim.Any
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              GHC.Prim.Any
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.$fEqC
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            @ GHC.Prim.Any
                            $dEq_a16O)
                         (TestEq.$fEqC
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            @ GHC.Prim.Any
                            $dEq_a16O))))))))
TestEq.eqLogic :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclIdX
[Arity 2]
TestEq.eqLogic =
  \ (x_agy :: TestEq.Logic) (y_agz :: TestEq.Logic) ->
    eq'_aEx
      (TestEq.fromLogic
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         x_agy)
      (TestEq.fromLogic
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         @ GHC.Prim.Any
         y_agz)
TestEq.$fEqLogic :: TestEq.Eq TestEq.Logic
LclIdX[DFunId]
[Arity 2]
TestEq.$fEqLogic =
  TestEq.eqLogic
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.Logic)
          :: (TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq TestEq.Logic)
$dEq_a16K [ALWAYS LoopBreaker Nothing] :: TestEq.Eq
                                            (TestEq.Rec TestEq.Logic)
LclId
[Arity 2]
$dEq_a16K = TestEq.$fEqRec @ TestEq.Logic TestEq.$fEqLogic
TestEq.$fEq[] :: forall a_agC.
                 (TestEq.Eq a_agC) =>
                 TestEq.Eq [a_agC]
LclIdX[DFunId]
[Arity 1]
TestEq.$fEq[] =
  __inline_me (eq'_a10s
               `cast` (forall a_agC.
                       (TestEq.Eq a_agC) =>
                       sym (TestEq.NTCo:T:Eq [a_agC])
                       :: (forall a_agC.
                           (TestEq.Eq a_agC) =>
                           [a_agC] -> [a_agC] -> GHC.Bool.Bool)
                            ~
                          (forall a_agC. (TestEq.Eq a_agC) => TestEq.T:Eq [a_agC])))
eq'_a10s [ALWAYS LoopBreaker Nothing] :: forall a_agC.
                                         (TestEq.Eq a_agC) =>
                                         [a_agC] -> [a_agC] -> GHC.Bool.Bool
LclId
[Arity 1]
eq'_a10s =
  \ (@ a_agC) ($dEq_a10b :: TestEq.Eq a_agC) ->
    TestEq.eq
      @ [a_agC]
      (TestEq.$fRepresentable[] @ a_agC)
      ((TestEq.$fEq:+:
          @ (TestEq.C TestEq.List_Nil_ TestEq.U)
          @ (TestEq.C
               TestEq.List_Cons_ (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))
          (TestEq.$fEqC @ TestEq.U @ TestEq.List_Nil_ TestEq.$fEqU)
          (TestEq.$fEqC
             @ (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC])
             @ TestEq.List_Cons_
             (TestEq.$fEq:*:
                @ (TestEq.Var a_agC)
                @ (TestEq.Rec [a_agC])
                (TestEq.$fEqVar @ a_agC $dEq_a10b)
                (TestEq.$fEqRec @ [a_agC] (TestEq.$fEq[] @ a_agC $dEq_a10b)))))
       `cast` (TestEq.T:Eq
                 (trans
                    (sym
                       (trans
                          (TestEq.TFCo:R:Rep[] a_agC)
                          (sym
                             (trans
                                (trans
                                   (TestEq.C TestEq.List_Nil_ TestEq.U
                                    TestEq.:+: TestEq.C
                                                 TestEq.List_Cons_
                                                 (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))
                                   (TestEq.C TestEq.List_Nil_ TestEq.U
                                    TestEq.:+: TestEq.C
                                                 TestEq.List_Cons_
                                                 (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC])))
                                (TestEq.C TestEq.List_Nil_ TestEq.U
                                 TestEq.:+: TestEq.C
                                              TestEq.List_Cons_
                                              (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))))))
                    (TestEq.Rep [a_agC]))
               :: TestEq.T:Eq
                    (TestEq.C TestEq.List_Nil_ TestEq.U
                     TestEq.:+: TestEq.C
                                  TestEq.List_Cons_
                                  (TestEq.Var a_agC TestEq.:*: TestEq.Rec [a_agC]))
                    ~
                  TestEq.T:Eq (TestEq.Rep [a_agC])))
TestEq.$fEqChar :: TestEq.Eq GHC.Types.Char
LclIdX[DFunId]
[]
TestEq.$fEqChar =
  a_s1f3
  `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
          :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq GHC.Types.Char)
a_s1f3 :: GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool
LclId
[]
a_s1f3 = GHC.Classes.== @ GHC.Types.Char GHC.Base.$fEqChar
eq'_aFu :: (TestEq.C GHC.Prim.Any TestEq.U
            TestEq.:+: TestEq.C
                         GHC.Prim.Any
                         (TestEq.Var GHC.Types.Int
                          TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                      TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
           -> (TestEq.C GHC.Prim.Any TestEq.U
               TestEq.:+: TestEq.C
                            GHC.Prim.Any
                            (TestEq.Var GHC.Types.Int
                             TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                         TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
           -> GHC.Bool.Bool
LclId
[]
eq'_aFu =
  TestEq.eq'
    @ (TestEq.C GHC.Prim.Any TestEq.U
       TestEq.:+: TestEq.C
                    GHC.Prim.Any
                    (TestEq.Var GHC.Types.Int
                     TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                                 TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
    (TestEq.$fEq:+:
       @ (TestEq.C GHC.Prim.Any TestEq.U)
       @ (TestEq.C
            GHC.Prim.Any
            (TestEq.Var GHC.Types.Int
             TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                         TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))))
       (TestEq.$fEqC @ TestEq.U @ GHC.Prim.Any TestEq.$fEqU)
       (TestEq.$fEqC
          @ (TestEq.Var GHC.Types.Int
             TestEq.:*: (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                         TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int)))
          @ GHC.Prim.Any
          (TestEq.$fEq:*:
             @ (TestEq.Var GHC.Types.Int)
             @ (TestEq.Rec (TestEq.Tree GHC.Types.Int)
                TestEq.:*: TestEq.Rec (TestEq.Tree GHC.Types.Int))
             (TestEq.$fEqVar @ GHC.Types.Int TestEq.$fEqInt)
             (TestEq.$fEq:*:
                @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
                @ (TestEq.Rec (TestEq.Tree GHC.Types.Int))
                $dEq_a16u
                $dEq_a16u))))
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
LclIdX
[Arity 2]
TestEq.eqTree =
  \ (x_agA :: TestEq.Tree GHC.Types.Int)
    (y_agB :: TestEq.Tree GHC.Types.Int) ->
    eq'_aFu
      (TestEq.fromTree
         @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any x_agA)
      (TestEq.fromTree
         @ GHC.Types.Int @ GHC.Prim.Any @ GHC.Prim.Any y_agB)
TestEq.$fEqTree :: TestEq.Eq (TestEq.Tree GHC.Types.Int)
LclIdX[DFunId]
[Arity 2]
TestEq.$fEqTree =
  TestEq.eqTree
  `cast` (sym (TestEq.NTCo:T:Eq (TestEq.Tree GHC.Types.Int))
          :: (TestEq.Tree GHC.Types.Int
              -> TestEq.Tree GHC.Types.Int
              -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq (TestEq.Tree GHC.Types.Int))
$dEq_a16u [ALWAYS LoopBreaker Nothing] :: TestEq.Eq
                                            (TestEq.Rec (TestEq.Tree GHC.Types.Int))
LclId
[Arity 2]
$dEq_a16u =
  TestEq.$fEqRec @ (TestEq.Tree GHC.Types.Int) TestEq.$fEqTree
TestEq.$fEqInt :: TestEq.Eq GHC.Types.Int
LclIdX[DFunId]
[]
TestEq.$fEqInt =
  a_s1f1
  `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Int)
          :: (GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq GHC.Types.Int)
a_s1f1 :: GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool
LclId
[]
a_s1f1 = GHC.Classes.== @ GHC.Types.Int GHC.Base.$fEqInt
TestEq.$fEqRec :: forall a_agD.
                  (TestEq.Eq a_agD) =>
                  TestEq.Eq (TestEq.Rec a_agD)
LclIdX[DFunId]
[Arity 3]
TestEq.$fEqRec =
  __inline_me (TestEq.eqRec
               `cast` (forall a_agD.
                       (TestEq.Eq a_agD) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Rec a_agD))
                       :: (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.Rec a_agD -> TestEq.Rec a_agD -> GHC.Bool.Bool)
                            ~
                          (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.T:Eq (TestEq.Rec a_agD))))
TestEq.$fEqVar :: forall a_agE.
                  (TestEq.Eq a_agE) =>
                  TestEq.Eq (TestEq.Var a_agE)
LclIdX[DFunId]
[Arity 3]
TestEq.$fEqVar =
  __inline_me (TestEq.eqVar
               `cast` (forall a_agE.
                       (TestEq.Eq a_agE) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Var a_agE))
                       :: (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.Var a_agE -> TestEq.Var a_agE -> GHC.Bool.Bool)
                            ~
                          (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.T:Eq (TestEq.Var a_agE))))
TestEq.$fEqC :: forall a_agF c_agG.
                (TestEq.Eq a_agF) =>
                TestEq.Eq (TestEq.C c_agG a_agF)
LclIdX[DFunId]
[Arity 3]
TestEq.$fEqC =
  __inline_me (eq'_a117
               `cast` (forall a_agF c_agG.
                       (TestEq.Eq a_agF) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.C c_agG a_agF))
                       :: (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool)
                            ~
                          (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.T:Eq (TestEq.C c_agG a_agF))))
eq'_a117 :: forall a_agF c_agG.
            (TestEq.Eq a_agF) =>
            TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool
LclId
[Arity 3]
eq'_a117 =
  \ (@ a_agF)
    (@ c_agG)
    ($dEq_a110 :: TestEq.Eq a_agF)
    (eta_B2 :: TestEq.C c_agG a_agF)
    (eta_B1 :: TestEq.C c_agG a_agF) ->
    TestEq.eqC @ c_agG @ a_agF @ c_agG $dEq_a110 eta_B2 eta_B1
TestEq.$fEq:*: :: forall a_agH b_agI.
                  (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                  TestEq.Eq (a_agH TestEq.:*: b_agI)
LclIdX[DFunId]
[Arity 4]
TestEq.$fEq:*: =
  __inline_me (TestEq.eqTimes
               `cast` (forall a_agH b_agI.
                       (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                       sym (TestEq.NTCo:T:Eq (a_agH TestEq.:*: b_agI))
                       :: (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           (a_agH TestEq.:*: b_agI)
                           -> (a_agH TestEq.:*: b_agI)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           TestEq.T:Eq (a_agH TestEq.:*: b_agI))))
TestEq.$fEq:+: :: forall a_agJ b_agK.
                  (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                  TestEq.Eq (a_agJ TestEq.:+: b_agK)
LclIdX[DFunId]
[Arity 4]
TestEq.$fEq:+: =
  __inline_me (TestEq.eqPlus
               `cast` (forall a_agJ b_agK.
                       (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                       sym (TestEq.NTCo:T:Eq (a_agJ TestEq.:+: b_agK))
                       :: (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           (a_agJ TestEq.:+: b_agK)
                           -> (a_agJ TestEq.:+: b_agK)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           TestEq.T:Eq (a_agJ TestEq.:+: b_agK))))
end Rec }

a_s1fK :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U
LclId
[]
a_s1fK =
  GHC.Base.>>=
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec
    @ Text.Read.Lex.Lexeme
    @ TestEq.U
    GHC.Read.lexP
    (\ (ds_d1cy :: Text.Read.Lex.Lexeme) ->
       let {
         fail_s1o4 :: GHC.Prim.State# GHC.Prim.RealWorld
                      -> Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U
         LclId
         [Arity 1]
         fail_s1o4 =
           \ _ ->
             GHC.Base.fail
               @ Text.ParserCombinators.ReadPrec.ReadPrec
               Text.ParserCombinators.ReadPrec.$fMonadReadPrec
               @ TestEq.U
               (GHC.Base.unpackCString#
                  "Pattern match failure in do expression at <no location info>") } in
       case ds_d1cy of _ {
         __DEFAULT -> fail_s1o4 GHC.Prim.realWorld#;
         Text.Read.Lex.Ident ds_d1cz ->
           case ds_d1cz of _ {
             [] -> fail_s1o4 GHC.Prim.realWorld#;
             : ds_d1cA ds_d1cB ->
               case ds_d1cA of _ { GHC.Types.C# ds_d1cC ->
               case ds_d1cC of _ {
                 __DEFAULT -> fail_s1o4 GHC.Prim.realWorld#;
                 'U' ->
                   case ds_d1cB of _ {
                     [] ->
                       GHC.Base.return
                         @ Text.ParserCombinators.ReadPrec.ReadPrec
                         Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                         @ TestEq.U
                         TestEq.U;
                     : _ _ -> fail_s1o4 GHC.Prim.realWorld#
                   }
               }
               }
           }
       })

readPrec_aZU :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U
LclId
[Arity 1]
readPrec_aZU = GHC.Read.parens @ TestEq.U a_s1fK

Rec {
TestEq.$fReadU :: GHC.Read.Read TestEq.U
LclIdX[DFunId]
[]
TestEq.$fReadU =
  GHC.Read.D:Read
    @ TestEq.U
    a_s1fY
    readListDefault_aZF
    readPrec_aZU
    readListPrecDefault_aZY
a_s1fY [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                       -> Text.ParserCombinators.ReadP.ReadS TestEq.U
LclId
[Arity 1]
a_s1fY = GHC.Read.$dmreadsPrec @ TestEq.U TestEq.$fReadU
readListPrecDefault_aZY [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.ReadPrec
                                                          [TestEq.U]
LclId
[]
readListPrecDefault_aZY =
  GHC.Read.readListPrecDefault @ TestEq.U TestEq.$fReadU
readListDefault_aZF [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadP.ReadS
                                                      [TestEq.U]
LclId
[]
readListDefault_aZF =
  GHC.Read.readListDefault @ TestEq.U TestEq.$fReadU
end Rec }

showsPrec_aZq :: GHC.Types.Int -> TestEq.U -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aZq =
  \ _ (ds_d1cw :: TestEq.U) ->
    case ds_d1cw of _ { TestEq.U ->
    GHC.Show.showString
      (GHC.Types.:
         @ GHC.Types.Char
         (GHC.Types.C# 'U')
         (GHC.Types.[] @ GHC.Types.Char))
    }

Rec {
TestEq.$fShowU :: GHC.Show.Show TestEq.U
LclIdX[DFunId]
[]
TestEq.$fShowU =
  GHC.Show.D:Show @ TestEq.U showsPrec_aZq a_s1gh showList_aZy
a_s1gh [ALWAYS LoopBreaker Nothing] :: TestEq.U -> GHC.Base.String
LclId
[]
a_s1gh = GHC.Show.$dmshow @ TestEq.U TestEq.$fShowU
a_s1gE :: TestEq.U -> GHC.Show.ShowS
LclId
[]
a_s1gE =
  GHC.Show.showsPrec @ TestEq.U TestEq.$fShowU (GHC.Types.I# 0)
showList_aZy [ALWAYS LoopBreaker Nothing] :: [TestEq.U]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aZy = GHC.Show.showList__ @ TestEq.U a_s1gE
end Rec }

readPrec_aZb :: forall a_afL b_afM.
                (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (a_afL TestEq.:+: b_afM)
LclId
[Arity 2]
readPrec_aZb =
  \ (@ a_afL)
    (@ b_afM)
    ($dRead_aYo :: GHC.Read.Read a_afL)
    ($dRead_aYp :: GHC.Read.Read b_afM) ->
    let {
      readPrec_s1o0 :: Text.ParserCombinators.ReadPrec.ReadPrec b_afM
      LclId
      []
      readPrec_s1o0 = GHC.Read.readPrec @ b_afM $dRead_aYp } in
    let {
      return_s1nZ :: forall a_aPP.
                     a_aPP -> Text.ParserCombinators.ReadPrec.ReadPrec a_aPP
      LclId
      []
      return_s1nZ =
        GHC.Base.return
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    let {
      readPrec_s1nY :: Text.ParserCombinators.ReadPrec.ReadPrec a_afL
      LclId
      []
      readPrec_s1nY = GHC.Read.readPrec @ a_afL $dRead_aYo } in
    let {
      fail_s1nX :: forall a_aSD.
                   GHC.Base.String -> Text.ParserCombinators.ReadPrec.ReadPrec a_aSD
      LclId
      []
      fail_s1nX =
        GHC.Base.fail
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    let {
      >>=_s1nW :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1nW =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (a_afL TestEq.:+: b_afM)
      (Text.ParserCombinators.ReadPrec.+++
         @ (a_afL TestEq.:+: b_afM)
         (Text.ParserCombinators.ReadPrec.prec
            @ (a_afL TestEq.:+: b_afM)
            (GHC.Types.I# 10)
            (>>=_s1nW
               @ Text.Read.Lex.Lexeme
               @ (a_afL TestEq.:+: b_afM)
               GHC.Read.lexP
               (\ (ds_d1c9 :: Text.Read.Lex.Lexeme) ->
                  let {
                    fail_s1o2 :: GHC.Prim.State# GHC.Prim.RealWorld
                                 -> Text.ParserCombinators.ReadPrec.ReadPrec
                                      (a_afL TestEq.:+: b_afM)
                    LclId
                    [Arity 1]
                    fail_s1o2 =
                      \ _ ->
                        fail_s1nX
                          @ (a_afL TestEq.:+: b_afM)
                          (GHC.Base.unpackCString#
                             "Pattern match failure in do expression at <no location info>") } in
                  case ds_d1c9 of _ {
                    __DEFAULT -> fail_s1o2 GHC.Prim.realWorld#;
                    Text.Read.Lex.Ident ds_d1ca ->
                      case ds_d1ca of _ {
                        [] -> fail_s1o2 GHC.Prim.realWorld#;
                        : ds_d1cb ds_d1cc ->
                          case ds_d1cb of _ { GHC.Types.C# ds_d1cd ->
                          case ds_d1cd of _ {
                            __DEFAULT -> fail_s1o2 GHC.Prim.realWorld#;
                            'L' ->
                              case ds_d1cc of _ {
                                [] ->
                                  >>=_s1nW
                                    @ a_afL
                                    @ (a_afL TestEq.:+: b_afM)
                                    (Text.ParserCombinators.ReadPrec.step @ a_afL readPrec_s1nY)
                                    (\ (a1_ayI :: a_afL) ->
                                       return_s1nZ
                                         @ (a_afL TestEq.:+: b_afM)
                                         (TestEq.L @ a_afL @ b_afM a1_ayI));
                                : _ _ -> fail_s1o2 GHC.Prim.realWorld#
                              }
                          }
                          }
                      }
                  })))
         (Text.ParserCombinators.ReadPrec.prec
            @ (a_afL TestEq.:+: b_afM)
            (GHC.Types.I# 10)
            (>>=_s1nW
               @ Text.Read.Lex.Lexeme
               @ (a_afL TestEq.:+: b_afM)
               GHC.Read.lexP
               (\ (ds_d1ck :: Text.Read.Lex.Lexeme) ->
                  let {
                    fail_s1nV :: GHC.Prim.State# GHC.Prim.RealWorld
                                 -> Text.ParserCombinators.ReadPrec.ReadPrec
                                      (a_afL TestEq.:+: b_afM)
                    LclId
                    [Arity 1]
                    fail_s1nV =
                      \ _ ->
                        fail_s1nX
                          @ (a_afL TestEq.:+: b_afM)
                          (GHC.Base.unpackCString#
                             "Pattern match failure in do expression at <no location info>") } in
                  case ds_d1ck of _ {
                    __DEFAULT -> fail_s1nV GHC.Prim.realWorld#;
                    Text.Read.Lex.Ident ds_d1cl ->
                      case ds_d1cl of _ {
                        [] -> fail_s1nV GHC.Prim.realWorld#;
                        : ds_d1cm ds_d1cn ->
                          case ds_d1cm of _ { GHC.Types.C# ds_d1co ->
                          case ds_d1co of _ {
                            __DEFAULT -> fail_s1nV GHC.Prim.realWorld#;
                            'R' ->
                              case ds_d1cn of _ {
                                [] ->
                                  >>=_s1nW
                                    @ b_afM
                                    @ (a_afL TestEq.:+: b_afM)
                                    (Text.ParserCombinators.ReadPrec.step @ b_afM readPrec_s1o0)
                                    (\ (a1_ayJ :: b_afM) ->
                                       return_s1nZ
                                         @ (a_afL TestEq.:+: b_afM)
                                         (TestEq.R @ a_afL @ b_afM a1_ayJ));
                                : _ _ -> fail_s1nV GHC.Prim.realWorld#
                              }
                          }
                          }
                      }
                  }))))

Rec {
TestEq.$fRead:+: :: forall a_afL b_afM.
                    (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                    GHC.Read.Read (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRead:+: =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 ($dRead_aYo :: GHC.Read.Read a_afL)
                 ($dRead_aYp :: GHC.Read.Read b_afM) ->
                 let {
                   a_s1nN :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [a_afL TestEq.:+: b_afM]
                   LclId
                   []
                   a_s1nN =
                     readListPrec_aZl @ a_afL @ b_afM $dRead_aYo $dRead_aYp } in
                 let {
                   a_s1nM :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (a_afL TestEq.:+: b_afM)
                   LclId
                   []
                   a_s1nM = readPrec_aZb @ a_afL @ b_afM $dRead_aYo $dRead_aYp } in
                 let {
                   a_s1nO :: Text.ParserCombinators.ReadP.ReadS
                               [a_afL TestEq.:+: b_afM]
                   LclId
                   []
                   a_s1nO = readList_aYC @ a_afL @ b_afM $dRead_aYo $dRead_aYp } in
                 letrec {
                   $dRead_s1nK :: GHC.Read.Read (a_afL TestEq.:+: b_afM)
                   LclId
                   []
                   $dRead_s1nK =
                     GHC.Read.D:Read
                       @ (a_afL TestEq.:+: b_afM) a_s1nL a_s1nO a_s1nM a_s1nN;
                   a_s1nL [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_afL TestEq.:+: b_afM)
                   LclId
                   [Arity 1]
                   a_s1nL =
                     GHC.Read.$dmreadsPrec @ (a_afL TestEq.:+: b_afM) $dRead_s1nK; } in
                 $dRead_s1nK)
readListPrec_aZl [ALWAYS LoopBreaker Nothing] :: forall a_afL
                                                        b_afM.
                                                 (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [a_afL TestEq.:+: b_afM]
LclId
[Arity 2]
readListPrec_aZl =
  \ (@ a_afL)
    (@ b_afM)
    ($dRead_aYo :: GHC.Read.Read a_afL)
    ($dRead_aYp :: GHC.Read.Read b_afM) ->
    GHC.Read.readListPrecDefault
      @ (a_afL TestEq.:+: b_afM)
      (TestEq.$fRead:+: @ a_afL @ b_afM $dRead_aYo $dRead_aYp)
readList_aYC [ALWAYS LoopBreaker Nothing] :: forall a_afL b_afM.
                                             (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                                             Text.ParserCombinators.ReadP.ReadS
                                               [a_afL TestEq.:+: b_afM]
LclId
[Arity 2]
readList_aYC =
  \ (@ a_afL)
    (@ b_afM)
    ($dRead_aYo :: GHC.Read.Read a_afL)
    ($dRead_aYp :: GHC.Read.Read b_afM) ->
    GHC.Read.readListDefault
      @ (a_afL TestEq.:+: b_afM)
      (TestEq.$fRead:+: @ a_afL @ b_afM $dRead_aYo $dRead_aYp)
end Rec }

showsPrec_aYb :: forall a_afL b_afM.
                 (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                 GHC.Types.Int -> (a_afL TestEq.:+: b_afM) -> GHC.Show.ShowS
LclId
[Arity 4]
showsPrec_aYb =
  \ (@ a_afL)
    (@ b_afM)
    ($dShow_aXP :: GHC.Show.Show a_afL)
    ($dShow_aXQ :: GHC.Show.Show b_afM)
    (eta_B2 :: GHC.Types.Int)
    (eta_B1 :: a_afL TestEq.:+: b_afM) ->
    case eta_B1 of _ {
      TestEq.L b1_ayF ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt eta_B2 (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "L "))
             (GHC.Show.showsPrec @ a_afL $dShow_aXP (GHC.Types.I# 11) b1_ayF));
      TestEq.R b1_ayH ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt eta_B2 (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "R "))
             (GHC.Show.showsPrec @ b_afM $dShow_aXQ (GHC.Types.I# 11) b1_ayH))
    }

Rec {
TestEq.$fShow:+: :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Show.Show (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2]
TestEq.$fShow:+: =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 ($dShow_aXP :: GHC.Show.Show a_afL)
                 ($dShow_aXQ :: GHC.Show.Show b_afM) ->
                 let {
                   a_s1nE :: [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1nE = showList_aYn @ a_afL @ b_afM $dShow_aXP $dShow_aXQ } in
                 let {
                   a_s1nF :: GHC.Types.Int
                             -> (a_afL TestEq.:+: b_afM)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1nF = showsPrec_aYb @ a_afL @ b_afM $dShow_aXP $dShow_aXQ } in
                 letrec {
                   $dShow_s1nC :: GHC.Show.Show (a_afL TestEq.:+: b_afM)
                   LclId
                   []
                   $dShow_s1nC =
                     GHC.Show.D:Show @ (a_afL TestEq.:+: b_afM) a_s1nF a_s1nD a_s1nE;
                   a_s1nD [ALWAYS LoopBreaker Nothing] :: (a_afL TestEq.:+: b_afM)
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1nD =
                     GHC.Show.$dmshow @ (a_afL TestEq.:+: b_afM) $dShow_s1nC; } in
                 $dShow_s1nC)
showList_aYn [ALWAYS LoopBreaker Nothing] :: forall a_afL b_afM.
                                             (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                                             [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aYn =
  \ (@ a_afL)
    (@ b_afM)
    ($dShow_aXP :: GHC.Show.Show a_afL)
    ($dShow_aXQ :: GHC.Show.Show b_afM) ->
    GHC.Show.showList__
      @ (a_afL TestEq.:+: b_afM)
      (GHC.Show.showsPrec
         @ (a_afL TestEq.:+: b_afM)
         (TestEq.$fShow:+: @ a_afL @ b_afM $dShow_aXP $dShow_aXQ)
         (GHC.Types.I# 0))
end Rec }

readPrec_aXE :: forall a_afJ b_afK.
                (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (a_afJ TestEq.:*: b_afK)
LclId
[Arity 2]
readPrec_aXE =
  \ (@ a_afJ)
    (@ b_afK)
    ($dRead_aWX :: GHC.Read.Read a_afJ)
    ($dRead_aWY :: GHC.Read.Read b_afK) ->
    let {
      readPrec_s1ny :: Text.ParserCombinators.ReadPrec.ReadPrec b_afK
      LclId
      []
      readPrec_s1ny = GHC.Read.readPrec @ b_afK $dRead_aWY } in
    let {
      >>=_s1nx :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1nx =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (a_afJ TestEq.:*: b_afK)
      (Text.ParserCombinators.ReadPrec.prec
         @ (a_afJ TestEq.:*: b_afK)
         (GHC.Types.I# 6)
         (>>=_s1nx
            @ a_afJ
            @ (a_afJ TestEq.:*: b_afK)
            (Text.ParserCombinators.ReadPrec.step
               @ a_afJ (GHC.Read.readPrec @ a_afJ $dRead_aWX))
            (\ (a1_ayC :: a_afJ) ->
               >>=_s1nx
                 @ Text.Read.Lex.Lexeme
                 @ (a_afJ TestEq.:*: b_afK)
                 GHC.Read.lexP
                 (\ (ds_d1c1 :: Text.Read.Lex.Lexeme) ->
                    let {
                      fail_s1nw :: GHC.Prim.State# GHC.Prim.RealWorld
                                   -> Text.ParserCombinators.ReadPrec.ReadPrec
                                        (a_afJ TestEq.:*: b_afK)
                      LclId
                      [Arity 1]
                      fail_s1nw =
                        \ _ ->
                          GHC.Base.fail
                            @ Text.ParserCombinators.ReadPrec.ReadPrec
                            Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                            @ (a_afJ TestEq.:*: b_afK)
                            (GHC.Base.unpackCString#
                               "Pattern match failure in do expression at <no location info>") } in
                    case ds_d1c1 of _ {
                      __DEFAULT -> fail_s1nw GHC.Prim.realWorld#;
                      Text.Read.Lex.Symbol ds_d1c2 ->
                        case GHC.Base.eqString ds_d1c2 (GHC.Base.unpackCString# ":*:")
                        of _ {
                          GHC.Bool.False -> fail_s1nw GHC.Prim.realWorld#;
                          GHC.Bool.True ->
                            >>=_s1nx
                              @ b_afK
                              @ (a_afJ TestEq.:*: b_afK)
                              (Text.ParserCombinators.ReadPrec.step @ b_afK readPrec_s1ny)
                              (\ (a2_ayD :: b_afK) ->
                                 GHC.Base.return
                                   @ Text.ParserCombinators.ReadPrec.ReadPrec
                                   Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                                   @ (a_afJ TestEq.:*: b_afK)
                                   (TestEq.:*: @ a_afJ @ b_afK a1_ayC a2_ayD))
                        }
                    }))))

Rec {
TestEq.$fRead:*: :: forall a_afJ b_afK.
                    (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                    GHC.Read.Read (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2]
TestEq.$fRead:*: =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 ($dRead_aWX :: GHC.Read.Read a_afJ)
                 ($dRead_aWY :: GHC.Read.Read b_afK) ->
                 let {
                   a_s1nr :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [a_afJ TestEq.:*: b_afK]
                   LclId
                   []
                   a_s1nr =
                     readListPrec_aXO @ a_afJ @ b_afK $dRead_aWX $dRead_aWY } in
                 let {
                   a_s1nq :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (a_afJ TestEq.:*: b_afK)
                   LclId
                   []
                   a_s1nq = readPrec_aXE @ a_afJ @ b_afK $dRead_aWX $dRead_aWY } in
                 let {
                   a_s1ns :: Text.ParserCombinators.ReadP.ReadS
                               [a_afJ TestEq.:*: b_afK]
                   LclId
                   []
                   a_s1ns = readList_aXb @ a_afJ @ b_afK $dRead_aWX $dRead_aWY } in
                 letrec {
                   $dRead_s1no :: GHC.Read.Read (a_afJ TestEq.:*: b_afK)
                   LclId
                   []
                   $dRead_s1no =
                     GHC.Read.D:Read
                       @ (a_afJ TestEq.:*: b_afK) a_s1np a_s1ns a_s1nq a_s1nr;
                   a_s1np [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_afJ TestEq.:*: b_afK)
                   LclId
                   [Arity 1]
                   a_s1np =
                     GHC.Read.$dmreadsPrec @ (a_afJ TestEq.:*: b_afK) $dRead_s1no; } in
                 $dRead_s1no)
readListPrec_aXO [ALWAYS LoopBreaker Nothing] :: forall a_afJ
                                                        b_afK.
                                                 (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [a_afJ TestEq.:*: b_afK]
LclId
[Arity 2]
readListPrec_aXO =
  \ (@ a_afJ)
    (@ b_afK)
    ($dRead_aWX :: GHC.Read.Read a_afJ)
    ($dRead_aWY :: GHC.Read.Read b_afK) ->
    GHC.Read.readListPrecDefault
      @ (a_afJ TestEq.:*: b_afK)
      (TestEq.$fRead:*: @ a_afJ @ b_afK $dRead_aWX $dRead_aWY)
readList_aXb [ALWAYS LoopBreaker Nothing] :: forall a_afJ b_afK.
                                             (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                                             Text.ParserCombinators.ReadP.ReadS
                                               [a_afJ TestEq.:*: b_afK]
LclId
[Arity 2]
readList_aXb =
  \ (@ a_afJ)
    (@ b_afK)
    ($dRead_aWX :: GHC.Read.Read a_afJ)
    ($dRead_aWY :: GHC.Read.Read b_afK) ->
    GHC.Read.readListDefault
      @ (a_afJ TestEq.:*: b_afK)
      (TestEq.$fRead:*: @ a_afJ @ b_afK $dRead_aWX $dRead_aWY)
end Rec }

showsPrec_aWK :: forall a_afJ b_afK.
                 (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                 GHC.Types.Int -> (a_afJ TestEq.:*: b_afK) -> GHC.Show.ShowS
LclId
[Arity 4]
showsPrec_aWK =
  \ (@ a_afJ)
    (@ b_afK)
    ($dShow_aWq :: GHC.Show.Show a_afJ)
    ($dShow_aWr :: GHC.Show.Show b_afK)
    (a_ayz :: GHC.Types.Int)
    (ds_d1bZ :: a_afJ TestEq.:*: b_afK) ->
    case ds_d1bZ of _ { TestEq.:*: b1_ayA b2_ayB ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayz (GHC.Types.I# 7))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showsPrec @ a_afJ $dShow_aWq (GHC.Types.I# 7) b1_ayA)
         (GHC.Base..
            @ GHC.Base.String
            @ GHC.Base.String
            @ GHC.Base.String
            (GHC.Show.showString (GHC.Base.unpackCString# " :*: "))
            (GHC.Show.showsPrec @ b_afK $dShow_aWr (GHC.Types.I# 7) b2_ayB)))
    }

Rec {
TestEq.$fShow:*: :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Show.Show (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2]
TestEq.$fShow:*: =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 ($dShow_aWq :: GHC.Show.Show a_afJ)
                 ($dShow_aWr :: GHC.Show.Show b_afK) ->
                 let {
                   a_s1ni :: [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1ni = showList_aWW @ a_afJ @ b_afK $dShow_aWq $dShow_aWr } in
                 let {
                   a_s1nj :: GHC.Types.Int
                             -> (a_afJ TestEq.:*: b_afK)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1nj = showsPrec_aWK @ a_afJ @ b_afK $dShow_aWq $dShow_aWr } in
                 letrec {
                   $dShow_s1ng :: GHC.Show.Show (a_afJ TestEq.:*: b_afK)
                   LclId
                   []
                   $dShow_s1ng =
                     GHC.Show.D:Show @ (a_afJ TestEq.:*: b_afK) a_s1nj a_s1nh a_s1ni;
                   a_s1nh [ALWAYS LoopBreaker Nothing] :: (a_afJ TestEq.:*: b_afK)
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1nh =
                     GHC.Show.$dmshow @ (a_afJ TestEq.:*: b_afK) $dShow_s1ng; } in
                 $dShow_s1ng)
showList_aWW [ALWAYS LoopBreaker Nothing] :: forall a_afJ b_afK.
                                             (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                                             [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aWW =
  \ (@ a_afJ)
    (@ b_afK)
    ($dShow_aWq :: GHC.Show.Show a_afJ)
    ($dShow_aWr :: GHC.Show.Show b_afK) ->
    GHC.Show.showList__
      @ (a_afJ TestEq.:*: b_afK)
      (GHC.Show.showsPrec
         @ (a_afJ TestEq.:*: b_afK)
         (TestEq.$fShow:*: @ a_afJ @ b_afK $dShow_aWq $dShow_aWr)
         (GHC.Types.I# 0))
end Rec }

readPrec_aWh :: forall c_afH a_afI.
                (GHC.Read.Read a_afI) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_afH a_afI)
LclId
[Arity 1]
readPrec_aWh =
  \ (@ c_afH) (@ a_afI) ($dRead_aVL :: GHC.Read.Read a_afI) ->
    let {
      readPrec_s1nc :: Text.ParserCombinators.ReadPrec.ReadPrec a_afI
      LclId
      []
      readPrec_s1nc = GHC.Read.readPrec @ a_afI $dRead_aVL } in
    let {
      >>=_s1nb :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1nb =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (TestEq.C c_afH a_afI)
      (Text.ParserCombinators.ReadPrec.prec
         @ (TestEq.C c_afH a_afI)
         (GHC.Types.I# 10)
         (>>=_s1nb
            @ Text.Read.Lex.Lexeme
            @ (TestEq.C c_afH a_afI)
            GHC.Read.lexP
            (\ (ds_d1bO :: Text.Read.Lex.Lexeme) ->
               let {
                 fail_s1na :: GHC.Prim.State# GHC.Prim.RealWorld
                              -> Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_afH a_afI)
                 LclId
                 [Arity 1]
                 fail_s1na =
                   \ _ ->
                     GHC.Base.fail
                       @ Text.ParserCombinators.ReadPrec.ReadPrec
                       Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                       @ (TestEq.C c_afH a_afI)
                       (GHC.Base.unpackCString#
                          "Pattern match failure in do expression at <no location info>") } in
               case ds_d1bO of _ {
                 __DEFAULT -> fail_s1na GHC.Prim.realWorld#;
                 Text.Read.Lex.Ident ds_d1bP ->
                   case ds_d1bP of _ {
                     [] -> fail_s1na GHC.Prim.realWorld#;
                     : ds_d1bQ ds_d1bR ->
                       case ds_d1bQ of _ { GHC.Types.C# ds_d1bS ->
                       case ds_d1bS of _ {
                         __DEFAULT -> fail_s1na GHC.Prim.realWorld#;
                         'C' ->
                           case ds_d1bR of _ {
                             [] ->
                               >>=_s1nb
                                 @ a_afI
                                 @ (TestEq.C c_afH a_afI)
                                 (Text.ParserCombinators.ReadPrec.step @ a_afI readPrec_s1nc)
                                 (\ (a1_ayy :: a_afI) ->
                                    GHC.Base.return
                                      @ Text.ParserCombinators.ReadPrec.ReadPrec
                                      Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                                      @ (TestEq.C c_afH a_afI)
                                      (TestEq.C @ c_afH @ a_afI a1_ayy));
                             : _ _ -> fail_s1na GHC.Prim.realWorld#
                           }
                       }
                       }
                   }
               })))

Rec {
TestEq.$fReadC :: forall c_afH a_afI.
                  (GHC.Read.Read a_afI) =>
                  GHC.Read.Read (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1]
TestEq.$fReadC =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 ($dRead_aVL :: GHC.Read.Read a_afI) ->
                 let {
                   a_s1n5 :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [TestEq.C c_afH a_afI]
                   LclId
                   []
                   a_s1n5 = readListPrec_aWp @ c_afH @ a_afI $dRead_aVL } in
                 let {
                   a_s1n4 :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (TestEq.C c_afH a_afI)
                   LclId
                   []
                   a_s1n4 = readPrec_aWh @ c_afH @ a_afI $dRead_aVL } in
                 let {
                   a_s1n6 :: Text.ParserCombinators.ReadP.ReadS [TestEq.C c_afH a_afI]
                   LclId
                   []
                   a_s1n6 = readList_aVW @ c_afH @ a_afI $dRead_aVL } in
                 letrec {
                   $dRead_s1n2 :: GHC.Read.Read (TestEq.C c_afH a_afI)
                   LclId
                   []
                   $dRead_s1n2 =
                     GHC.Read.D:Read
                       @ (TestEq.C c_afH a_afI) a_s1n3 a_s1n6 a_s1n4 a_s1n5;
                   a_s1n3 [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.C c_afH a_afI)
                   LclId
                   [Arity 1]
                   a_s1n3 =
                     GHC.Read.$dmreadsPrec @ (TestEq.C c_afH a_afI) $dRead_s1n2; } in
                 $dRead_s1n2)
readListPrec_aWp [ALWAYS LoopBreaker Nothing] :: forall c_afH
                                                        a_afI.
                                                 (GHC.Read.Read a_afI) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [TestEq.C c_afH a_afI]
LclId
[Arity 1]
readListPrec_aWp =
  \ (@ c_afH) (@ a_afI) ($dRead_aVL :: GHC.Read.Read a_afI) ->
    GHC.Read.readListPrecDefault
      @ (TestEq.C c_afH a_afI)
      (TestEq.$fReadC @ c_afH @ a_afI $dRead_aVL)
readList_aVW [ALWAYS LoopBreaker Nothing] :: forall c_afH a_afI.
                                             (GHC.Read.Read a_afI) =>
                                             Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.C c_afH a_afI]
LclId
[Arity 1]
readList_aVW =
  \ (@ c_afH) (@ a_afI) ($dRead_aVL :: GHC.Read.Read a_afI) ->
    GHC.Read.readListDefault
      @ (TestEq.C c_afH a_afI)
      (TestEq.$fReadC @ c_afH @ a_afI $dRead_aVL)
end Rec }

showsPrec_aVA :: forall c_afH a_afI.
                 (GHC.Show.Show a_afI) =>
                 GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
LclId
[Arity 3]
showsPrec_aVA =
  \ (@ c_afH)
    (@ a_afI)
    ($dShow_aVm :: GHC.Show.Show a_afI)
    (a_ayw :: GHC.Types.Int)
    (ds_d1bM :: TestEq.C c_afH a_afI) ->
    case ds_d1bM of _ { TestEq.C b1_ayx ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayw (GHC.Types.I# 11))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showString (GHC.Base.unpackCString# "C "))
         (GHC.Show.showsPrec @ a_afI $dShow_aVm (GHC.Types.I# 11) b1_ayx))
    }

Rec {
TestEq.$fShowC :: forall c_afH a_afI.
                  (GHC.Show.Show a_afI) =>
                  GHC.Show.Show (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowC =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 ($dShow_aVm :: GHC.Show.Show a_afI) ->
                 let {
                   a_s1mW :: [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1mW = showList_aVK @ c_afH @ a_afI $dShow_aVm } in
                 let {
                   a_s1mX :: GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1mX = showsPrec_aVA @ c_afH @ a_afI $dShow_aVm } in
                 letrec {
                   $dShow_s1mU :: GHC.Show.Show (TestEq.C c_afH a_afI)
                   LclId
                   []
                   $dShow_s1mU =
                     GHC.Show.D:Show @ (TestEq.C c_afH a_afI) a_s1mX a_s1mV a_s1mW;
                   a_s1mV [ALWAYS LoopBreaker Nothing] :: TestEq.C c_afH a_afI
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1mV =
                     GHC.Show.$dmshow @ (TestEq.C c_afH a_afI) $dShow_s1mU; } in
                 $dShow_s1mU)
showList_aVK [ALWAYS LoopBreaker Nothing] :: forall c_afH a_afI.
                                             (GHC.Show.Show a_afI) =>
                                             [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aVK =
  \ (@ c_afH) (@ a_afI) ($dShow_aVm :: GHC.Show.Show a_afI) ->
    GHC.Show.showList__
      @ (TestEq.C c_afH a_afI)
      (GHC.Show.showsPrec
         @ (TestEq.C c_afH a_afI)
         (TestEq.$fShowC @ c_afH @ a_afI $dShow_aVm)
         (GHC.Types.I# 0))
end Rec }

readPrec_aVd :: forall a_afG.
                (GHC.Read.Read a_afG) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_afG)
LclId
[Arity 1]
readPrec_aVd =
  \ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
    let {
      readPrec_s1mQ :: Text.ParserCombinators.ReadPrec.ReadPrec a_afG
      LclId
      []
      readPrec_s1mQ = GHC.Read.readPrec @ a_afG $dRead_aUH } in
    let {
      >>=_s1mP :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1mP =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (TestEq.Var a_afG)
      (Text.ParserCombinators.ReadPrec.prec
         @ (TestEq.Var a_afG)
         (GHC.Types.I# 10)
         (>>=_s1mP
            @ Text.Read.Lex.Lexeme
            @ (TestEq.Var a_afG)
            GHC.Read.lexP
            (\ (ds_d1bH :: Text.Read.Lex.Lexeme) ->
               let {
                 fail_s1mO :: GHC.Prim.State# GHC.Prim.RealWorld
                              -> Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_afG)
                 LclId
                 [Arity 1]
                 fail_s1mO =
                   \ _ ->
                     GHC.Base.fail
                       @ Text.ParserCombinators.ReadPrec.ReadPrec
                       Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                       @ (TestEq.Var a_afG)
                       (GHC.Base.unpackCString#
                          "Pattern match failure in do expression at <no location info>") } in
               case ds_d1bH of _ {
                 __DEFAULT -> fail_s1mO GHC.Prim.realWorld#;
                 Text.Read.Lex.Ident ds_d1bI ->
                   case GHC.Base.eqString ds_d1bI (GHC.Base.unpackCString# "Var")
                   of _ {
                     GHC.Bool.False -> fail_s1mO GHC.Prim.realWorld#;
                     GHC.Bool.True ->
                       >>=_s1mP
                         @ a_afG
                         @ (TestEq.Var a_afG)
                         (Text.ParserCombinators.ReadPrec.step @ a_afG readPrec_s1mQ)
                         (\ (a1_ayv :: a_afG) ->
                            GHC.Base.return
                              @ Text.ParserCombinators.ReadPrec.ReadPrec
                              Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                              @ (TestEq.Var a_afG)
                              (TestEq.Var @ a_afG a1_ayv))
                   }
               })))

Rec {
TestEq.$fReadVar :: forall a_afG.
                    (GHC.Read.Read a_afG) =>
                    GHC.Read.Read (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1]
TestEq.$fReadVar =
  __inline_me (\ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
                 let {
                   a_s1mJ :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [TestEq.Var a_afG]
                   LclId
                   []
                   a_s1mJ = readListPrec_aVl @ a_afG $dRead_aUH } in
                 let {
                   a_s1mI :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (TestEq.Var a_afG)
                   LclId
                   []
                   a_s1mI = readPrec_aVd @ a_afG $dRead_aUH } in
                 let {
                   a_s1mK :: Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
                   LclId
                   []
                   a_s1mK = readList_aUS @ a_afG $dRead_aUH } in
                 letrec {
                   $dRead_s1mG :: GHC.Read.Read (TestEq.Var a_afG)
                   LclId
                   []
                   $dRead_s1mG =
                     GHC.Read.D:Read @ (TestEq.Var a_afG) a_s1mH a_s1mK a_s1mI a_s1mJ;
                   a_s1mH [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Var a_afG)
                   LclId
                   [Arity 1]
                   a_s1mH =
                     GHC.Read.$dmreadsPrec @ (TestEq.Var a_afG) $dRead_s1mG; } in
                 $dRead_s1mG)
readListPrec_aVl [ALWAYS LoopBreaker Nothing] :: forall a_afG.
                                                 (GHC.Read.Read a_afG) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [TestEq.Var a_afG]
LclId
[Arity 1]
readListPrec_aVl =
  \ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
    GHC.Read.readListPrecDefault
      @ (TestEq.Var a_afG) (TestEq.$fReadVar @ a_afG $dRead_aUH)
readList_aUS [ALWAYS LoopBreaker Nothing] :: forall a_afG.
                                             (GHC.Read.Read a_afG) =>
                                             Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
LclId
[Arity 1]
readList_aUS =
  \ (@ a_afG) ($dRead_aUH :: GHC.Read.Read a_afG) ->
    GHC.Read.readListDefault
      @ (TestEq.Var a_afG) (TestEq.$fReadVar @ a_afG $dRead_aUH)
end Rec }

showsPrec_aUw :: forall a_afG.
                 (GHC.Show.Show a_afG) =>
                 GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
LclId
[Arity 3]
showsPrec_aUw =
  \ (@ a_afG)
    ($dShow_aUi :: GHC.Show.Show a_afG)
    (a_ayt :: GHC.Types.Int)
    (ds_d1bF :: TestEq.Var a_afG) ->
    case ds_d1bF of _ { TestEq.Var b1_ayu ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayt (GHC.Types.I# 11))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showString (GHC.Base.unpackCString# "Var "))
         (GHC.Show.showsPrec @ a_afG $dShow_aUi (GHC.Types.I# 11) b1_ayu))
    }

Rec {
TestEq.$fShowVar :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Show.Show (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowVar =
  __inline_me (\ (@ a_afG) ($dShow_aUi :: GHC.Show.Show a_afG) ->
                 let {
                   a_s1mA :: [TestEq.Var a_afG] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1mA = showList_aUG @ a_afG $dShow_aUi } in
                 let {
                   a_s1mB :: GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1mB = showsPrec_aUw @ a_afG $dShow_aUi } in
                 letrec {
                   $dShow_s1my :: GHC.Show.Show (TestEq.Var a_afG)
                   LclId
                   []
                   $dShow_s1my =
                     GHC.Show.D:Show @ (TestEq.Var a_afG) a_s1mB a_s1mz a_s1mA;
                   a_s1mz [ALWAYS LoopBreaker Nothing] :: TestEq.Var a_afG
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1mz = GHC.Show.$dmshow @ (TestEq.Var a_afG) $dShow_s1my; } in
                 $dShow_s1my)
showList_aUG [ALWAYS LoopBreaker Nothing] :: forall a_afG.
                                             (GHC.Show.Show a_afG) =>
                                             [TestEq.Var a_afG] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aUG =
  \ (@ a_afG) ($dShow_aUi :: GHC.Show.Show a_afG) ->
    GHC.Show.showList__
      @ (TestEq.Var a_afG)
      (GHC.Show.showsPrec
         @ (TestEq.Var a_afG)
         (TestEq.$fShowVar @ a_afG $dShow_aUi)
         (GHC.Types.I# 0))
end Rec }

readPrec_aU9 :: forall a_afF.
                (GHC.Read.Read a_afF) =>
                Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_afF)
LclId
[Arity 1]
readPrec_aU9 =
  \ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
    let {
      readPrec_s1mu :: Text.ParserCombinators.ReadPrec.ReadPrec a_afF
      LclId
      []
      readPrec_s1mu = GHC.Read.readPrec @ a_afF $dRead_aTD } in
    let {
      >>=_s1mt :: forall a_aSA b_aSB.
                  Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
                  -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
                  -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
      LclId
      []
      >>=_s1mt =
        GHC.Base.>>=
          @ Text.ParserCombinators.ReadPrec.ReadPrec
          Text.ParserCombinators.ReadPrec.$fMonadReadPrec } in
    GHC.Read.parens
      @ (TestEq.Rec a_afF)
      (Text.ParserCombinators.ReadPrec.prec
         @ (TestEq.Rec a_afF)
         (GHC.Types.I# 10)
         (>>=_s1mt
            @ Text.Read.Lex.Lexeme
            @ (TestEq.Rec a_afF)
            GHC.Read.lexP
            (\ (ds_d1bA :: Text.Read.Lex.Lexeme) ->
               let {
                 fail_s1ms :: GHC.Prim.State# GHC.Prim.RealWorld
                              -> Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_afF)
                 LclId
                 [Arity 1]
                 fail_s1ms =
                   \ _ ->
                     GHC.Base.fail
                       @ Text.ParserCombinators.ReadPrec.ReadPrec
                       Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                       @ (TestEq.Rec a_afF)
                       (GHC.Base.unpackCString#
                          "Pattern match failure in do expression at <no location info>") } in
               case ds_d1bA of _ {
                 __DEFAULT -> fail_s1ms GHC.Prim.realWorld#;
                 Text.Read.Lex.Ident ds_d1bB ->
                   case GHC.Base.eqString ds_d1bB (GHC.Base.unpackCString# "Rec")
                   of _ {
                     GHC.Bool.False -> fail_s1ms GHC.Prim.realWorld#;
                     GHC.Bool.True ->
                       >>=_s1mt
                         @ a_afF
                         @ (TestEq.Rec a_afF)
                         (Text.ParserCombinators.ReadPrec.step @ a_afF readPrec_s1mu)
                         (\ (a1_ays :: a_afF) ->
                            GHC.Base.return
                              @ Text.ParserCombinators.ReadPrec.ReadPrec
                              Text.ParserCombinators.ReadPrec.$fMonadReadPrec
                              @ (TestEq.Rec a_afF)
                              (TestEq.Rec @ a_afF a1_ays))
                   }
               })))

Rec {
TestEq.$fReadRec :: forall a_afF.
                    (GHC.Read.Read a_afF) =>
                    GHC.Read.Read (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1]
TestEq.$fReadRec =
  __inline_me (\ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
                 let {
                   a_s1mn :: Text.ParserCombinators.ReadPrec.ReadPrec
                               [TestEq.Rec a_afF]
                   LclId
                   []
                   a_s1mn = readListPrec_aUh @ a_afF $dRead_aTD } in
                 let {
                   a_s1mm :: Text.ParserCombinators.ReadPrec.ReadPrec
                               (TestEq.Rec a_afF)
                   LclId
                   []
                   a_s1mm = readPrec_aU9 @ a_afF $dRead_aTD } in
                 let {
                   a_s1mo :: Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
                   LclId
                   []
                   a_s1mo = readList_aTO @ a_afF $dRead_aTD } in
                 letrec {
                   $dRead_s1mk :: GHC.Read.Read (TestEq.Rec a_afF)
                   LclId
                   []
                   $dRead_s1mk =
                     GHC.Read.D:Read @ (TestEq.Rec a_afF) a_s1ml a_s1mo a_s1mm a_s1mn;
                   a_s1ml [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Rec a_afF)
                   LclId
                   [Arity 1]
                   a_s1ml =
                     GHC.Read.$dmreadsPrec @ (TestEq.Rec a_afF) $dRead_s1mk; } in
                 $dRead_s1mk)
readListPrec_aUh [ALWAYS LoopBreaker Nothing] :: forall a_afF.
                                                 (GHC.Read.Read a_afF) =>
                                                 Text.ParserCombinators.ReadPrec.ReadPrec
                                                   [TestEq.Rec a_afF]
LclId
[Arity 1]
readListPrec_aUh =
  \ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
    GHC.Read.readListPrecDefault
      @ (TestEq.Rec a_afF) (TestEq.$fReadRec @ a_afF $dRead_aTD)
readList_aTO [ALWAYS LoopBreaker Nothing] :: forall a_afF.
                                             (GHC.Read.Read a_afF) =>
                                             Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
LclId
[Arity 1]
readList_aTO =
  \ (@ a_afF) ($dRead_aTD :: GHC.Read.Read a_afF) ->
    GHC.Read.readListDefault
      @ (TestEq.Rec a_afF) (TestEq.$fReadRec @ a_afF $dRead_aTD)
end Rec }

showsPrec_aTs :: forall a_afF.
                 (GHC.Show.Show a_afF) =>
                 GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
LclId
[Arity 3]
showsPrec_aTs =
  \ (@ a_afF)
    ($dShow_aTe :: GHC.Show.Show a_afF)
    (a_ayq :: GHC.Types.Int)
    (ds_d1by :: TestEq.Rec a_afF) ->
    case ds_d1by of _ { TestEq.Rec b1_ayr ->
    GHC.Show.showParen
      (GHC.Classes.>=
         @ GHC.Types.Int GHC.Base.$fOrdInt a_ayq (GHC.Types.I# 11))
      (GHC.Base..
         @ GHC.Base.String
         @ GHC.Base.String
         @ GHC.Base.String
         (GHC.Show.showString (GHC.Base.unpackCString# "Rec "))
         (GHC.Show.showsPrec @ a_afF $dShow_aTe (GHC.Types.I# 11) b1_ayr))
    }

Rec {
TestEq.$fShowRec :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Show.Show (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowRec =
  __inline_me (\ (@ a_afF) ($dShow_aTe :: GHC.Show.Show a_afF) ->
                 let {
                   a_s1me :: [TestEq.Rec a_afF] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1me = showList_aTC @ a_afF $dShow_aTe } in
                 let {
                   a_s1mf :: GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
                   LclId
                   [Arity 2]
                   a_s1mf = showsPrec_aTs @ a_afF $dShow_aTe } in
                 letrec {
                   $dShow_s1mc :: GHC.Show.Show (TestEq.Rec a_afF)
                   LclId
                   []
                   $dShow_s1mc =
                     GHC.Show.D:Show @ (TestEq.Rec a_afF) a_s1mf a_s1md a_s1me;
                   a_s1md [ALWAYS LoopBreaker Nothing] :: TestEq.Rec a_afF
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1md = GHC.Show.$dmshow @ (TestEq.Rec a_afF) $dShow_s1mc; } in
                 $dShow_s1mc)
showList_aTC [ALWAYS LoopBreaker Nothing] :: forall a_afF.
                                             (GHC.Show.Show a_afF) =>
                                             [TestEq.Rec a_afF] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aTC =
  \ (@ a_afF) ($dShow_aTe :: GHC.Show.Show a_afF) ->
    GHC.Show.showList__
      @ (TestEq.Rec a_afF)
      (GHC.Show.showsPrec
         @ (TestEq.Rec a_afF)
         (TestEq.$fShowRec @ a_afF $dShow_aTe)
         (GHC.Types.I# 0))
end Rec }

readPrec_aSX :: Text.ParserCombinators.ReadPrec.ReadPrec
                  GHC.Types.Int
LclId
[]
readPrec_aSX = GHC.Read.readPrec @ GHC.Types.Int GHC.Read.$fReadInt

return_aSF :: forall a_aPP.
              a_aPP -> Text.ParserCombinators.ReadPrec.ReadPrec a_aPP
LclId
[]
return_aSF =
  GHC.Base.return
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

fail_aSE :: forall a_aSD.
            GHC.Base.String -> Text.ParserCombinators.ReadPrec.ReadPrec a_aSD
LclId
[]
fail_aSE =
  GHC.Base.fail
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

>>=_aSC :: forall a_aSA b_aSB.
           Text.ParserCombinators.ReadPrec.ReadPrec a_aSA
           -> (a_aSA -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB)
           -> Text.ParserCombinators.ReadPrec.ReadPrec b_aSB
LclId
[]
>>=_aSC =
  GHC.Base.>>=
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

return_aPQ :: forall a_aPP.
              a_aPP -> Text.ParserCombinators.ReadPrec.ReadPrec a_aPP
LclId
[]
return_aPQ =
  GHC.Base.return
    @ Text.ParserCombinators.ReadPrec.ReadPrec
    Text.ParserCombinators.ReadPrec.$fMonadReadPrec

a_s1iD :: Text.ParserCombinators.ReadPrec.ReadPrec
            TestEq.Associativity
LclId
[]
a_s1iD =
  GHC.Read.choose
    @ TestEq.Associativity
    (GHC.Base.build
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
       (\ (@ a_d1b1)
          (c_d1b2 :: (GHC.Base.String,
                      Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                     -> a_d1b1
                     -> a_d1b1)
          (n_d1b3 :: a_d1b1) ->
          c_d1b2
            (GHC.Base.unpackCString# "LeftAssociative",
             return_aPQ @ TestEq.Associativity TestEq.LeftAssociative)
            (c_d1b2
               (GHC.Base.unpackCString# "RightAssociative",
                return_aPQ @ TestEq.Associativity TestEq.RightAssociative)
               (c_d1b2
                  (GHC.Base.unpackCString# "NotAssociative",
                   return_aPQ @ TestEq.Associativity TestEq.NotAssociative)
                  n_d1b3))))

readPrec_aPY :: Text.ParserCombinators.ReadPrec.ReadPrec
                  TestEq.Associativity
LclId
[Arity 1]
readPrec_aPY = GHC.Read.parens @ TestEq.Associativity a_s1iD

Rec {
TestEq.$fReadAssociativity :: GHC.Read.Read TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fReadAssociativity =
  GHC.Read.D:Read
    @ TestEq.Associativity
    a_s1iF
    readListDefault_aPz
    readPrec_aPY
    readListPrecDefault_aQ3
a_s1iF [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                       -> Text.ParserCombinators.ReadP.ReadS TestEq.Associativity
LclId
[Arity 1]
a_s1iF =
  GHC.Read.$dmreadsPrec
    @ TestEq.Associativity TestEq.$fReadAssociativity
readListPrecDefault_aQ3 [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.ReadPrec
                                                          [TestEq.Associativity]
LclId
[]
readListPrecDefault_aQ3 =
  GHC.Read.readListPrecDefault
    @ TestEq.Associativity TestEq.$fReadAssociativity
readListDefault_aPz [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadP.ReadS
                                                      [TestEq.Associativity]
LclId
[]
readListDefault_aPz =
  GHC.Read.readListDefault
    @ TestEq.Associativity TestEq.$fReadAssociativity
end Rec }

readPrec_aSQ :: Text.ParserCombinators.ReadPrec.ReadPrec
                  TestEq.Associativity
LclId
[]
readPrec_aSQ =
  GHC.Read.readPrec @ TestEq.Associativity TestEq.$fReadAssociativity

a_s1iI :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[]
a_s1iI =
  >>=_aSC
    @ Text.Read.Lex.Lexeme
    @ TestEq.Fixity
    GHC.Read.lexP
    (\ (ds_d1bo :: Text.Read.Lex.Lexeme) ->
       let {
         fail_s1m8 :: GHC.Prim.State# GHC.Prim.RealWorld
                      -> Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
         LclId
         [Arity 1]
         fail_s1m8 =
           \ _ ->
             fail_aSE
               @ TestEq.Fixity
               (GHC.Base.unpackCString#
                  "Pattern match failure in do expression at <no location info>") } in
       case ds_d1bo of _ {
         __DEFAULT -> fail_s1m8 GHC.Prim.realWorld#;
         Text.Read.Lex.Ident ds_d1bp ->
           case GHC.Base.eqString ds_d1bp (GHC.Base.unpackCString# "Prefix")
           of _ {
             GHC.Bool.False -> fail_s1m8 GHC.Prim.realWorld#;
             GHC.Bool.True -> return_aSF @ TestEq.Fixity TestEq.Prefix
           }
       })

a_s1iL :: GHC.Types.Int
LclId
[]
a_s1iL = GHC.Types.I# 10

a_s1iN :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[]
a_s1iN =
  >>=_aSC
    @ Text.Read.Lex.Lexeme
    @ TestEq.Fixity
    GHC.Read.lexP
    (\ (ds_d1bt :: Text.Read.Lex.Lexeme) ->
       let {
         fail_s1m6 :: GHC.Prim.State# GHC.Prim.RealWorld
                      -> Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
         LclId
         [Arity 1]
         fail_s1m6 =
           \ _ ->
             fail_aSE
               @ TestEq.Fixity
               (GHC.Base.unpackCString#
                  "Pattern match failure in do expression at <no location info>") } in
       case ds_d1bt of _ {
         __DEFAULT -> fail_s1m6 GHC.Prim.realWorld#;
         Text.Read.Lex.Ident ds_d1bu ->
           case GHC.Base.eqString ds_d1bu (GHC.Base.unpackCString# "Infix")
           of _ {
             GHC.Bool.False -> fail_s1m6 GHC.Prim.realWorld#;
             GHC.Bool.True ->
               >>=_aSC
                 @ TestEq.Associativity
                 @ TestEq.Fixity
                 (Text.ParserCombinators.ReadPrec.step
                    @ TestEq.Associativity readPrec_aSQ)
                 (\ (a1_axJ :: TestEq.Associativity) ->
                    >>=_aSC
                      @ GHC.Types.Int
                      @ TestEq.Fixity
                      (Text.ParserCombinators.ReadPrec.step @ GHC.Types.Int readPrec_aSX)
                      (\ (a2_axK :: GHC.Types.Int) ->
                         return_aSF @ TestEq.Fixity (TestEq.Infix a1_axJ a2_axK)))
           }
       })

a_s1iO :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[Arity 1]
a_s1iO =
  Text.ParserCombinators.ReadPrec.prec @ TestEq.Fixity a_s1iL a_s1iN

a_s1iP :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity
LclId
[Arity 1]
a_s1iP =
  Text.ParserCombinators.ReadPrec.+++ @ TestEq.Fixity a_s1iI a_s1iO

readPrec_aT7 :: Text.ParserCombinators.ReadPrec.ReadPrec
                  TestEq.Fixity
LclId
[Arity 1]
readPrec_aT7 = GHC.Read.parens @ TestEq.Fixity a_s1iP

Rec {
TestEq.$fReadFixity :: GHC.Read.Read TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fReadFixity =
  GHC.Read.D:Read
    @ TestEq.Fixity
    a_s1iR
    readListDefault_aRx
    readPrec_aT7
    readListPrecDefault_aTb
a_s1iR [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                       -> Text.ParserCombinators.ReadP.ReadS TestEq.Fixity
LclId
[Arity 1]
a_s1iR = GHC.Read.$dmreadsPrec @ TestEq.Fixity TestEq.$fReadFixity
readListPrecDefault_aTb [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.ReadPrec
                                                          [TestEq.Fixity]
LclId
[]
readListPrecDefault_aTb =
  GHC.Read.readListPrecDefault @ TestEq.Fixity TestEq.$fReadFixity
readListDefault_aRx [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadP.ReadS
                                                      [TestEq.Fixity]
LclId
[]
readListDefault_aRx =
  GHC.Read.readListDefault @ TestEq.Fixity TestEq.$fReadFixity
end Rec }

compare_aK1 :: TestEq.Associativity
               -> TestEq.Associativity
               -> GHC.Ordering.Ordering
LclId
[Arity 2]
compare_aK1 =
  \ (a_axg :: TestEq.Associativity)
    (b_axh :: TestEq.Associativity) ->
    case TestEq.$con2tag_Associativity a_axg of a#_axj { __DEFAULT ->
    case TestEq.$con2tag_Associativity b_axh of b#_axk { __DEFAULT ->
    case GHC.Prim.==# a#_axj b#_axk of _ {
      GHC.Bool.False ->
        case GHC.Prim.<# a#_axj b#_axk of _ {
          GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
        };
      GHC.Bool.True -> GHC.Ordering.EQ
    }
    }
    }

showsPrec_aJp :: GHC.Types.Int
                 -> TestEq.Associativity
                 -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aJp =
  \ _ (ds_d1aU :: TestEq.Associativity) ->
    case ds_d1aU of _ {
      TestEq.LeftAssociative ->
        GHC.Show.showString (GHC.Base.unpackCString# "LeftAssociative");
      TestEq.RightAssociative ->
        GHC.Show.showString (GHC.Base.unpackCString# "RightAssociative");
      TestEq.NotAssociative ->
        GHC.Show.showString (GHC.Base.unpackCString# "NotAssociative")
    }

Rec {
TestEq.$fShowAssociativity :: GHC.Show.Show TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fShowAssociativity =
  GHC.Show.D:Show
    @ TestEq.Associativity showsPrec_aJp a_s1iV showList_aJx
a_s1iV [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> GHC.Base.String
LclId
[]
a_s1iV =
  GHC.Show.$dmshow @ TestEq.Associativity TestEq.$fShowAssociativity
a_s1iX :: TestEq.Associativity -> GHC.Show.ShowS
LclId
[]
a_s1iX =
  GHC.Show.showsPrec
    @ TestEq.Associativity TestEq.$fShowAssociativity (GHC.Types.I# 0)
showList_aJx [ALWAYS LoopBreaker Nothing] :: [TestEq.Associativity]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aJx = GHC.Show.showList__ @ TestEq.Associativity a_s1iX
end Rec }

showsPrec_aQK :: GHC.Types.Int -> TestEq.Fixity -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aQK =
  \ (ds_d1ba :: GHC.Types.Int) (ds_d1bb :: TestEq.Fixity) ->
    case ds_d1bb of _ {
      TestEq.Prefix ->
        GHC.Show.showString (GHC.Base.unpackCString# "Prefix");
      TestEq.Infix b1_axw b2_axx ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt ds_d1ba (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Infix "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (GHC.Show.showsPrec
                   @ TestEq.Associativity
                   TestEq.$fShowAssociativity
                   (GHC.Types.I# 11)
                   b1_axw)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (GHC.Show.showsPrec
                      @ GHC.Types.Int GHC.Show.$fShowInt (GHC.Types.I# 11) b2_axx))))
    }

Rec {
TestEq.$fShowFixity :: GHC.Show.Show TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fShowFixity =
  GHC.Show.D:Show @ TestEq.Fixity showsPrec_aQK a_s1j0 showList_aQS
a_s1j0 [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> GHC.Base.String
LclId
[]
a_s1j0 = GHC.Show.$dmshow @ TestEq.Fixity TestEq.$fShowFixity
a_s1j2 :: TestEq.Fixity -> GHC.Show.ShowS
LclId
[]
a_s1j2 =
  GHC.Show.showsPrec
    @ TestEq.Fixity TestEq.$fShowFixity (GHC.Types.I# 0)
showList_aQS [ALWAYS LoopBreaker Nothing] :: [TestEq.Fixity]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aQS = GHC.Show.showList__ @ TestEq.Fixity a_s1j2
end Rec }

==_aJd :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2]
==_aJd =
  \ (a_axa :: TestEq.Associativity)
    (b_axb :: TestEq.Associativity) ->
    case TestEq.$con2tag_Associativity a_axa of a#_axc { __DEFAULT ->
    case TestEq.$con2tag_Associativity b_axb of b#_axd { __DEFAULT ->
    GHC.Prim.==# a#_axc b#_axd
    }
    }

Rec {
TestEq.$fEqAssociativity :: GHC.Classes.Eq TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fEqAssociativity =
  GHC.Classes.D:Eq @ TestEq.Associativity ==_aJd /=_aJk
/=_aJk [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
/=_aJk =
  \ (a_axe :: TestEq.Associativity)
    (b_axf :: TestEq.Associativity) ->
    GHC.Classes.not
      (GHC.Classes.==
         @ TestEq.Associativity TestEq.$fEqAssociativity a_axe b_axf)
end Rec }

Rec {
TestEq.$fOrdAssociativity :: GHC.Classes.Ord TestEq.Associativity
LclIdX[DFunId]
[]
TestEq.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ TestEq.Associativity
    TestEq.$fEqAssociativity
    compare_aK1
    a_s1kS
    a_s1kU
    a_s1kW
    a_s1kY
    a_s1l0
    a_s1l2
a_s1l2 [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> TestEq.Associativity
LclId
[Arity 2]
a_s1l2 =
  GHC.Classes.$dmmin @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1l0 [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> TestEq.Associativity
LclId
[Arity 2]
a_s1l0 =
  GHC.Classes.$dmmax @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kY [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kY =
  GHC.Classes.$dm<= @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kW [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kW =
  GHC.Classes.$dm> @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kU [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kU =
  GHC.Classes.$dm>= @ TestEq.Associativity TestEq.$fOrdAssociativity
a_s1kS [ALWAYS LoopBreaker Nothing] :: TestEq.Associativity
                                       -> TestEq.Associativity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1kS =
  GHC.Classes.$dm< @ TestEq.Associativity TestEq.$fOrdAssociativity
end Rec }

compare_aRe :: TestEq.Fixity
               -> TestEq.Fixity
               -> GHC.Ordering.Ordering
LclId
[Arity 2]
compare_aRe =
  \ (a_axy :: TestEq.Fixity) (b_axz :: TestEq.Fixity) ->
    case TestEq.$con2tag_Fixity a_axy of a#_axF { __DEFAULT ->
    case TestEq.$con2tag_Fixity b_axz of b#_axG { __DEFAULT ->
    case GHC.Prim.==# a#_axF b#_axG of _ {
      GHC.Bool.False ->
        case GHC.Prim.<# a#_axF b#_axG of _ {
          GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
        };
      GHC.Bool.True ->
        case a_axy of _ {
          TestEq.Prefix -> GHC.Ordering.EQ;
          TestEq.Infix a1_axB a2_axC ->
            case b_axz of _ {
              TestEq.Prefix -> GHC.Ordering.EQ;
              TestEq.Infix b1_axD b2_axE ->
                case GHC.Classes.compare
                       @ TestEq.Associativity TestEq.$fOrdAssociativity a1_axB b1_axD
                of _ {
                  GHC.Ordering.LT -> GHC.Ordering.LT;
                  GHC.Ordering.EQ ->
                    GHC.Classes.compare
                      @ GHC.Types.Int GHC.Base.$fOrdInt a2_axC b2_axE;
                  GHC.Ordering.GT -> GHC.Ordering.GT
                }
            }
        }
    }
    }
    }

==_aQi :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2]
==_aQi =
  \ (ds_d1b4 :: TestEq.Fixity) (ds_d1b5 :: TestEq.Fixity) ->
    let {
      fail_s1m4 :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1]
      fail_s1m4 =
        \ _ ->
          case TestEq.$con2tag_Fixity ds_d1b4 of a#_axr { __DEFAULT ->
          case TestEq.$con2tag_Fixity ds_d1b5 of b#_axs { __DEFAULT ->
          GHC.Prim.==# a#_axr b#_axs
          }
          } } in
    case ds_d1b4 of _ {
      TestEq.Prefix -> fail_s1m4 GHC.Prim.realWorld#;
      TestEq.Infix a1_axl a2_axm ->
        case ds_d1b5 of _ {
          TestEq.Prefix -> fail_s1m4 GHC.Prim.realWorld#;
          TestEq.Infix b1_axn b2_axo ->
            GHC.Classes.&&
              (GHC.Classes.==
                 @ TestEq.Associativity TestEq.$fEqAssociativity a1_axl b1_axn)
              (GHC.Classes.== @ GHC.Types.Int GHC.Base.$fEqInt a2_axm b2_axo)
        }
    }

Rec {
TestEq.$fEqFixity :: GHC.Classes.Eq TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fEqFixity = GHC.Classes.D:Eq @ TestEq.Fixity ==_aQi /=_aQp
/=_aQp [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
/=_aQp =
  \ (a_axt :: TestEq.Fixity) (b_axu :: TestEq.Fixity) ->
    GHC.Classes.not
      (GHC.Classes.== @ TestEq.Fixity TestEq.$fEqFixity a_axt b_axu)
end Rec }

Rec {
TestEq.$fOrdFixity :: GHC.Classes.Ord TestEq.Fixity
LclIdX[DFunId]
[]
TestEq.$fOrdFixity =
  GHC.Classes.D:Ord
    @ TestEq.Fixity
    TestEq.$fEqFixity
    compare_aRe
    a_s1lc
    a_s1le
    a_s1lg
    a_s1li
    a_s1lk
    a_s1lm
a_s1lm [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> TestEq.Fixity
LclId
[Arity 2]
a_s1lm = GHC.Classes.$dmmin @ TestEq.Fixity TestEq.$fOrdFixity
a_s1lk [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> TestEq.Fixity
LclId
[Arity 2]
a_s1lk = GHC.Classes.$dmmax @ TestEq.Fixity TestEq.$fOrdFixity
a_s1li [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1li = GHC.Classes.$dm<= @ TestEq.Fixity TestEq.$fOrdFixity
a_s1lg [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1lg = GHC.Classes.$dm> @ TestEq.Fixity TestEq.$fOrdFixity
a_s1le [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1le = GHC.Classes.$dm>= @ TestEq.Fixity TestEq.$fOrdFixity
a_s1lc [ALWAYS LoopBreaker Nothing] :: TestEq.Fixity
                                       -> TestEq.Fixity
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
a_s1lc = GHC.Classes.$dm< @ TestEq.Fixity TestEq.$fOrdFixity
end Rec }

Rec {
TestEq.$fEqLogic0 :: GHC.Classes.Eq TestEq.Logic
LclIdX[DFunId]
[]
TestEq.$fEqLogic0 = GHC.Classes.D:Eq @ TestEq.Logic ==_aIY /=_aJ6
/=_aJ6 [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> TestEq.Logic
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
/=_aJ6 =
  \ (a_ax8 :: TestEq.Logic) (b_ax9 :: TestEq.Logic) ->
    GHC.Classes.not
      (GHC.Classes.== @ TestEq.Logic TestEq.$fEqLogic0 a_ax8 b_ax9)
==_aIB :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclId
[]
==_aIB = GHC.Classes.== @ TestEq.Logic TestEq.$fEqLogic0
==_aIY [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> TestEq.Logic
                                       -> GHC.Bool.Bool
LclId
[Arity 2]
==_aIY =
  \ (ds_d1aD :: TestEq.Logic) (ds_d1aE :: TestEq.Logic) ->
    let {
      fail_s1m2 :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1]
      fail_s1m2 =
        \ _ ->
          case TestEq.$con2tag_Logic ds_d1aD of a#_ax6 { __DEFAULT ->
          case TestEq.$con2tag_Logic ds_d1aE of b#_ax7 { __DEFAULT ->
          GHC.Prim.==# a#_ax6 b#_ax7
          }
          } } in
    case ds_d1aD of _ {
      __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
      TestEq.VarL a1_awK ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.VarL b1_awL -> GHC.Base.eqString a1_awK b1_awL
        };
      TestEq.Not a1_awM ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Not b1_awN -> ==_aIB a1_awM b1_awN
        };
      TestEq.Impl a1_awO a2_awP ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Impl b1_awQ b2_awR ->
            GHC.Classes.&& (==_aIB a1_awO b1_awQ) (==_aIB a2_awP b2_awR)
        };
      TestEq.Equiv a1_awS a2_awT ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Equiv b1_awU b2_awV ->
            GHC.Classes.&& (==_aIB a1_awS b1_awU) (==_aIB a2_awT b2_awV)
        };
      TestEq.Conj a1_awW a2_awX ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Conj b1_awY b2_awZ ->
            GHC.Classes.&& (==_aIB a1_awW b1_awY) (==_aIB a2_awX b2_awZ)
        };
      TestEq.Disj a1_ax0 a2_ax1 ->
        case ds_d1aE of _ {
          __DEFAULT -> fail_s1m2 GHC.Prim.realWorld#;
          TestEq.Disj b1_ax2 b2_ax3 ->
            GHC.Classes.&& (==_aIB a1_ax0 b1_ax2) (==_aIB a2_ax1 b2_ax3)
        }
    }
end Rec }

showsPrec_aHl :: GHC.Types.Int -> GHC.Base.String -> GHC.Show.ShowS
LclId
[]
showsPrec_aHl =
  GHC.Show.showsPrec
    @ GHC.Base.String
    (GHC.Show.$fShow[] @ GHC.Types.Char GHC.Show.$fShowChar)

Rec {
TestEq.$fShowLogic :: GHC.Show.Show TestEq.Logic
LclIdX[DFunId]
[]
TestEq.$fShowLogic =
  GHC.Show.D:Show @ TestEq.Logic showsPrec_aIk a_s1ly showList_aIt
a_s1ly [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> GHC.Base.String
LclId
[]
a_s1ly = GHC.Show.$dmshow @ TestEq.Logic TestEq.$fShowLogic
a_s1lA :: TestEq.Logic -> GHC.Show.ShowS
LclId
[]
a_s1lA =
  GHC.Show.showsPrec
    @ TestEq.Logic TestEq.$fShowLogic (GHC.Types.I# 0)
showList_aIt [ALWAYS LoopBreaker Nothing] :: [TestEq.Logic]
                                             -> GHC.Show.ShowS
LclId
[Arity 2]
showList_aIt = GHC.Show.showList__ @ TestEq.Logic a_s1lA
showsPrec_aHr :: GHC.Types.Int -> TestEq.Logic -> GHC.Show.ShowS
LclId
[]
showsPrec_aHr =
  GHC.Show.showsPrec @ TestEq.Logic TestEq.$fShowLogic
showsPrec_aIk [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                              -> TestEq.Logic
                                              -> GHC.Show.ShowS
LclId
[Arity 2]
showsPrec_aIk =
  \ (a_awu :: GHC.Types.Int) (ds_d1au :: TestEq.Logic) ->
    case ds_d1au of _ {
      TestEq.VarL b1_awv ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "VarL "))
             (showsPrec_aHl (GHC.Types.I# 11) b1_awv));
      TestEq.T ->
        GHC.Show.showString
          (GHC.Types.:
             @ GHC.Types.Char
             (GHC.Types.C# 'T')
             (GHC.Types.[] @ GHC.Types.Char));
      TestEq.F ->
        GHC.Show.showString
          (GHC.Types.:
             @ GHC.Types.Char
             (GHC.Types.C# 'F')
             (GHC.Types.[] @ GHC.Types.Char));
      TestEq.Not b1_awx ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Not "))
             (showsPrec_aHr (GHC.Types.I# 11) b1_awx));
      TestEq.Impl b1_awz b2_awA ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Impl "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awz)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awA))));
      TestEq.Equiv b1_awC b2_awD ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Equiv "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awC)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awD))));
      TestEq.Conj b1_awF b2_awG ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Conj "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awF)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awG))));
      TestEq.Disj b1_awI b2_awJ ->
        GHC.Show.showParen
          (GHC.Classes.>=
             @ GHC.Types.Int GHC.Base.$fOrdInt a_awu (GHC.Types.I# 11))
          (GHC.Base..
             @ GHC.Base.String
             @ GHC.Base.String
             @ GHC.Base.String
             (GHC.Show.showString (GHC.Base.unpackCString# "Disj "))
             (GHC.Base..
                @ GHC.Base.String
                @ GHC.Base.String
                @ GHC.Base.String
                (showsPrec_aHr (GHC.Types.I# 11) b1_awI)
                (GHC.Base..
                   @ GHC.Base.String
                   @ GHC.Base.String
                   @ GHC.Base.String
                   GHC.Show.showSpace
                   (showsPrec_aHr (GHC.Types.I# 11) b2_awJ))))
    }
end Rec }

Rec {
TestEq.$fEqTree0 :: forall a_afw.
                    (GHC.Classes.Eq a_afw) =>
                    GHC.Classes.Eq (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1]
TestEq.$fEqTree0 =
  __inline_me (\ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
                 GHC.Classes.D:Eq
                   @ (TestEq.Tree a_afw)
                   (==_aH2 @ a_afw $dEq_aGJ)
                   (/=_aHb @ a_afw $dEq_aGJ))
/=_aHb [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                       (GHC.Classes.Eq a_afw) =>
                                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1]
/=_aHb =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1lW :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      []
      ==_s1lW =
        GHC.Classes.==
          @ (TestEq.Tree a_afw) (TestEq.$fEqTree0 @ a_afw $dEq_aGJ) } in
    \ (a_aws :: TestEq.Tree a_afw) (b_awt :: TestEq.Tree a_afw) ->
      GHC.Classes.not (==_s1lW a_aws b_awt)
==_aH2 [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                       (GHC.Classes.Eq a_afw) =>
                                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1]
==_aH2 =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1m0 :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      []
      ==_s1m0 =
        GHC.Classes.==
          @ (TestEq.Tree a_afw) (TestEq.$fEqTree0 @ a_afw $dEq_aGJ) } in
    \ (ds_d1ao :: TestEq.Tree a_afw) (ds_d1ap :: TestEq.Tree a_afw) ->
      let {
        fail_s1lZ :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
        LclId
        [Arity 1]
        fail_s1lZ =
          \ _ ->
            case TestEq.$con2tag_Tree @ a_afw ds_d1ao of a#_awq { __DEFAULT ->
            case TestEq.$con2tag_Tree @ a_afw ds_d1ap of b#_awr { __DEFAULT ->
            GHC.Prim.==# a#_awq b#_awr
            }
            } } in
      case ds_d1ao of _ {
        TestEq.Leaf -> fail_s1lZ GHC.Prim.realWorld#;
        TestEq.Bin a1_awi a2_awj a3_awk ->
          case ds_d1ap of _ {
            TestEq.Leaf -> fail_s1lZ GHC.Prim.realWorld#;
            TestEq.Bin b1_awl b2_awm b3_awn ->
              GHC.Classes.&&
                (GHC.Classes.&&
                   (GHC.Classes.== @ a_afw $dEq_aGJ a1_awi b1_awl)
                   (==_s1m0 a2_awj b2_awm))
                (==_s1m0 a3_awk b3_awn)
          }
      }
end Rec }

Rec {
TestEq.$fShowTree :: forall a_afw.
                     (GHC.Show.Show a_afw) =>
                     GHC.Show.Show (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1]
TestEq.$fShowTree =
  __inline_me (\ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
                 let {
                   a_s1lR :: [TestEq.Tree a_afw] -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1lR = showList_aGI @ a_afw $dShow_aFW } in
                 let {
                   a_s1lS :: GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
                   LclId
                   []
                   a_s1lS = showsPrec_aGw @ a_afw $dShow_aFW } in
                 letrec {
                   $dShow_s1lP :: GHC.Show.Show (TestEq.Tree a_afw)
                   LclId
                   []
                   $dShow_s1lP =
                     GHC.Show.D:Show @ (TestEq.Tree a_afw) a_s1lS a_s1lQ a_s1lR;
                   a_s1lQ [ALWAYS LoopBreaker Nothing] :: TestEq.Tree a_afw
                                                          -> GHC.Base.String
                   LclId
                   []
                   a_s1lQ = GHC.Show.$dmshow @ (TestEq.Tree a_afw) $dShow_s1lP; } in
                 $dShow_s1lP)
showList_aGI [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                             (GHC.Show.Show a_afw) =>
                                             [TestEq.Tree a_afw] -> GHC.Show.ShowS
LclId
[Arity 1]
showList_aGI =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    GHC.Show.showList__
      @ (TestEq.Tree a_afw)
      (GHC.Show.showsPrec
         @ (TestEq.Tree a_afw)
         (TestEq.$fShowTree @ a_afw $dShow_aFW)
         (GHC.Types.I# 0))
showsPrec_aGw [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                              (GHC.Show.Show a_afw) =>
                                              GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
LclId
[Arity 1]
showsPrec_aGw =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    let {
      showsPrec_s1lU :: GHC.Types.Int
                        -> TestEq.Tree a_afw
                        -> GHC.Show.ShowS
      LclId
      []
      showsPrec_s1lU =
        GHC.Show.showsPrec
          @ (TestEq.Tree a_afw) (TestEq.$fShowTree @ a_afw $dShow_aFW) } in
    \ (ds_d1ak :: GHC.Types.Int) (ds_d1al :: TestEq.Tree a_afw) ->
      case ds_d1al of _ {
        TestEq.Leaf ->
          GHC.Show.showString (GHC.Base.unpackCString# "Leaf");
        TestEq.Bin b1_awf b2_awg b3_awh ->
          GHC.Show.showParen
            (GHC.Classes.>=
               @ GHC.Types.Int GHC.Base.$fOrdInt ds_d1ak (GHC.Types.I# 11))
            (GHC.Base..
               @ GHC.Base.String
               @ GHC.Base.String
               @ GHC.Base.String
               (GHC.Show.showString (GHC.Base.unpackCString# "Bin "))
               (GHC.Base..
                  @ GHC.Base.String
                  @ GHC.Base.String
                  @ GHC.Base.String
                  (GHC.Show.showsPrec @ a_afw $dShow_aFW (GHC.Types.I# 11) b1_awf)
                  (GHC.Base..
                     @ GHC.Base.String
                     @ GHC.Base.String
                     @ GHC.Base.String
                     GHC.Show.showSpace
                     (GHC.Base..
                        @ GHC.Base.String
                        @ GHC.Base.String
                        @ GHC.Base.String
                        (showsPrec_s1lU (GHC.Types.I# 11) b2_awg)
                        (GHC.Base..
                           @ GHC.Base.String
                           @ GHC.Base.String
                           @ GHC.Base.String
                           GHC.Show.showSpace
                           (showsPrec_s1lU (GHC.Types.I# 11) b3_awh))))))
      }
end Rec }

TestEq.t1 :: GHC.Bool.Bool
LclIdX
[]
TestEq.t1 = TestEq.eqTree TestEq.tree1 TestEq.tree1

TestEq.t2 :: GHC.Bool.Bool
LclIdX
[]
TestEq.t2 = TestEq.eqLogic TestEq.logic1 TestEq.logic1




==================== SpecConstr ====================
$wa_s1MI :: forall b_a1As.
            (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
            -> Text.ParserCombinators.ReadP.P b_a1As
LclId
[Arity 1
 Str: DmdType L]
$wa_s1MI =
  \ (@ b_a1As)
    (w_s1Lk :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As) ->
    Text.Read.Lex.lex1
      @ b_a1As
      (let {
         lvl_s1P2 :: Text.ParserCombinators.ReadP.P b_a1As
         LclId
         []
         lvl_s1P2 = w_s1Lk TestEq.U } in
       \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
         case a3_a1AS of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
           Text.Read.Lex.Ident ds_d1cz [ALWAYS Just S] ->
             case ds_d1cz of _ {
               [] -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
               : ds_d1cA [ALWAYS Just U(L)] ds_d1cB ->
                 case ds_d1cA of _ { GHC.Types.C# ds_d1cC [ALWAYS Just L] ->
                 case ds_d1cC of _ {
                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                   'U' ->
                     case ds_d1cB of _ {
                       [] -> lvl_s1P2; : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1As
                     }
                 }
                 }
             }
         })

a_s1KC :: Text.ParserCombinators.ReadPrec.Prec
          -> forall b_a1As.
             (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
             -> Text.ParserCombinators.ReadP.P b_a1As
LclId
[Arity 2
 Worker $wa_s1MI
 Str: DmdType AL]
a_s1KC =
  __inline_me (\ _
                 (@ b_a1As)
                 (w_s1Lk :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As) ->
                 $wa_s1MI @ b_a1As w_s1Lk)

a_s1IG :: GHC.Types.Int
LclId
[Str: DmdType m]
a_s1IG = GHC.Types.I# 0

lvl_s1P3 :: GHC.Types.Int
LclId
[]
lvl_s1P3 = GHC.Types.I# 11

a_s1F4 :: forall c_Xus a_Xuu.
          (GHC.Read.Read a_Xuu) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xus a_Xuu)
LclId
[Arity 2
 Str: DmdType LL]
a_s1F4 =
  \ (@ c_Xus)
    (@ a_Xuu)
    ($dRead_aVL [ALWAYS Just L] :: GHC.Read.Read a_Xuu)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.C c_Xus a_Xuu)
      ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta_X8a [ALWAYS Just L] :: TestEq.C c_Xus a_Xuu
                                      -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1Do 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.C c_Xus a_Xuu) @ b_a1As eta_X8a);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1As
                (let {
                   lvl_s1P4 :: a_Xuu -> Text.ParserCombinators.ReadP.P b_a1As
                   LclId
                   [Arity 1]
                   lvl_s1P4 =
                     \ (a3_X1Qk :: a_Xuu) ->
                       eta_X8a (TestEq.C @ c_Xus @ a_Xuu a3_X1Qk) } in
                 \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                   case a3_a1AS of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                     Text.Read.Lex.Ident ds_d1bP [ALWAYS Just S] ->
                       case ds_d1bP of _ {
                         [] -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         : ds_d1bQ [ALWAYS Just U(L)] ds_d1bR ->
                           case ds_d1bQ of _ { GHC.Types.C# ds_d1bS [ALWAYS Just L] ->
                           case ds_d1bS of _ {
                             __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                             'C' ->
                               case ds_d1bR of _ {
                                 [] ->
                                   case $dRead_aVL
                                   of _ { GHC.Read.D:Read _ _ tpl_Xf2 [ALWAYS Just C(C(S))] _ ->
                                   (((tpl_Xf2
                                      `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xuu
                                              :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xuu
                                                   ~
                                                 (Text.ParserCombinators.ReadPrec.Prec
                                                  -> Text.ParserCombinators.ReadP.ReadP a_Xuu)))
                                       lvl_s1P3)
                                    `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xuu
                                            :: Text.ParserCombinators.ReadP.ReadP a_Xuu
                                                 ~
                                               (forall b_a1As.
                                                (a_Xuu -> Text.ParserCombinators.ReadP.P b_a1As)
                                                -> Text.ParserCombinators.ReadP.P b_a1As)))
                                     @ b_a1As lvl_s1P4
                                   };
                                 : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1As
                               }
                           }
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.C c_Xus a_Xuu)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (TestEq.C c_Xus a_Xuu)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.C c_Xus a_Xuu -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xus a_Xuu)))
      eta_B1

a_s1F8 :: forall c_Xux a_Xuz.
          (GHC.Read.Read a_Xuz) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xux a_Xuz]
LclId
[Arity 1
 Str: DmdType L]
a_s1F8 =
  \ (@ c_Xux) (@ a_Xuz) ($dRead_X1aD :: GHC.Read.Read a_Xuz) ->
    GHC.Read.$dmreadList2
      @ (TestEq.C c_Xux a_Xuz)
      ((a_s1F4 @ c_Xux @ a_Xuz $dRead_X1aD)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (TestEq.C c_Xux a_Xuz))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xux a_Xuz))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xux a_Xuz)))

a_s1E1 :: forall a_Xtt b_Xtv.
          (GHC.Read.Read a_Xtt, GHC.Read.Read b_Xtv) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (a_Xtt TestEq.:+: b_Xtv)
LclId
[Arity 2
 Str: DmdType LL]
a_s1E1 =
  \ (@ a_Xtt)
    (@ b_Xtv)
    ($dRead_aYo [ALWAYS Just L] :: GHC.Read.Read a_Xtt)
    ($dRead_aYp [ALWAYS Just L] :: GHC.Read.Read b_Xtv) ->
    GHC.Read.$fRead()5
      @ (a_Xtt TestEq.:+: b_Xtv)
      ((Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
          @ (a_Xtt TestEq.:+: b_Xtv)
          ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b_a1As)
              (eta_B1 [ALWAYS Just L] :: (a_Xtt TestEq.:+: b_Xtv)
                                         -> Text.ParserCombinators.ReadP.P b_a1As) ->
              case c_a1Dl of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
              case GHC.Prim.<=# x_a1Do 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xtt TestEq.:+: b_Xtv) @ b_a1As eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b_a1As
                    (let {
                       lvl_s1P6 :: a_Xtt -> Text.ParserCombinators.ReadP.P b_a1As
                       LclId
                       [Arity 1]
                       lvl_s1P6 =
                         \ (a3_X1Pi :: a_Xtt) ->
                           eta_B1 (TestEq.L @ a_Xtt @ b_Xtv a3_X1Pi) } in
                     \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                       case a3_a1AS of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         Text.Read.Lex.Ident ds_d1ca [ALWAYS Just S] ->
                           case ds_d1ca of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                             : ds_d1cb [ALWAYS Just U(L)] ds_d1cc ->
                               case ds_d1cb of _ { GHC.Types.C# ds_d1cd [ALWAYS Just L] ->
                               case ds_d1cd of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                                 'L' ->
                                   case ds_d1cc of _ {
                                     [] ->
                                       case $dRead_aYo
                                       of _ { GHC.Read.D:Read _ _ tpl_Xe0 [ALWAYS Just C(C(S))] _ ->
                                       (((tpl_Xe0
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    a_Xtt
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xtt
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP a_Xtt)))
                                           lvl_s1P3)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xtt
                                                :: Text.ParserCombinators.ReadP.ReadP a_Xtt
                                                     ~
                                                   (forall b_a1As.
                                                    (a_Xtt -> Text.ParserCombinators.ReadP.P b_a1As)
                                                    -> Text.ParserCombinators.ReadP.P b_a1As)))
                                         @ b_a1As lvl_s1P6
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1As
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xtt TestEq.:+: b_Xtv)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xtt TestEq.:+: b_Xtv)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1As.
                          ((a_Xtt TestEq.:+: b_Xtv) -> Text.ParserCombinators.ReadP.P b_a1As)
                          -> Text.ParserCombinators.ReadP.P b_a1As)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtt TestEq.:+: b_Xtv)))
          ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b_a1As)
              (eta_B1 [ALWAYS Just L] :: (a_Xtt TestEq.:+: b_Xtv)
                                         -> Text.ParserCombinators.ReadP.P b_a1As) ->
              case c_a1Dl of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
              case GHC.Prim.<=# x_a1Do 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xtt TestEq.:+: b_Xtv) @ b_a1As eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b_a1As
                    (let {
                       lvl_s1P8 :: b_Xtv -> Text.ParserCombinators.ReadP.P b_a1As
                       LclId
                       [Arity 1]
                       lvl_s1P8 =
                         \ (a3_X1Pi :: b_Xtv) ->
                           eta_B1 (TestEq.R @ a_Xtt @ b_Xtv a3_X1Pi) } in
                     \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                       case a3_a1AS of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         Text.Read.Lex.Ident ds_d1cl [ALWAYS Just S] ->
                           case ds_d1cl of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                             : ds_d1cm [ALWAYS Just U(L)] ds_d1cn ->
                               case ds_d1cm of _ { GHC.Types.C# ds_d1co [ALWAYS Just L] ->
                               case ds_d1co of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                                 'R' ->
                                   case ds_d1cn of _ {
                                     [] ->
                                       case $dRead_aYp
                                       of _ { GHC.Read.D:Read _ _ tpl_Xe0 [ALWAYS Just C(C(S))] _ ->
                                       (((tpl_Xe0
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    b_Xtv
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec b_Xtv
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP b_Xtv)))
                                           lvl_s1P3)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_Xtv
                                                :: Text.ParserCombinators.ReadP.ReadP b_Xtv
                                                     ~
                                                   (forall b_a1As.
                                                    (b_Xtv -> Text.ParserCombinators.ReadP.P b_a1As)
                                                    -> Text.ParserCombinators.ReadP.P b_a1As)))
                                         @ b_a1As lvl_s1P8
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1As
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xtt TestEq.:+: b_Xtv)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xtt TestEq.:+: b_Xtv)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1As.
                          ((a_Xtt TestEq.:+: b_Xtv) -> Text.ParserCombinators.ReadP.P b_a1As)
                          -> Text.ParserCombinators.ReadP.P b_a1As)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec
                        (a_Xtt TestEq.:+: b_Xtv))))
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xtt TestEq.:+: b_Xtv))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xtt TestEq.:+: b_Xtv))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtt TestEq.:+: b_Xtv)))

a_s1E5 :: forall a_Xtz b_XtB.
          (GHC.Read.Read a_Xtz, GHC.Read.Read b_XtB) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [a_Xtz TestEq.:+: b_XtB]
LclId
[Arity 2
 Str: DmdType LL]
a_s1E5 =
  \ (@ a_Xtz)
    (@ b_XtB)
    ($dRead_X1ce :: GHC.Read.Read a_Xtz)
    ($dRead_X1cg :: GHC.Read.Read b_XtB) ->
    GHC.Read.$dmreadList2
      @ (a_Xtz TestEq.:+: b_XtB)
      ((a_s1E1 @ a_Xtz @ b_XtB $dRead_X1ce $dRead_X1cg)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xtz TestEq.:+: b_XtB))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xtz TestEq.:+: b_XtB))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtz TestEq.:+: b_XtB)))

a_s1Bf :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.U
LclId
[Arity 1
 Str: DmdType L]
a_s1Bf =
  GHC.Read.$fRead()5
    @ TestEq.U
    (a_s1KC
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

a_s1Bt :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.U]
LclId
[Str: DmdType]
a_s1Bt =
  GHC.Read.$dmreadList2
    @ TestEq.U
    (a_s1Bf
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

a_s1Bv :: Text.ParserCombinators.ReadP.P [TestEq.U]
LclId
[Str: DmdType]
a_s1Bv =
  ((a_s1Bt GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.U]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.U]
                ~
              (forall b_a1As.
               ([TestEq.U] -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As)))
    @ [TestEq.U]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.U])

lvl_s1y0 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1y0 = GHC.Base.unpackCString# "Bin "

lvl_s1xX :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xX = GHC.Base.unpackCString# "Leaf"

lvl_s1xW :: GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
lvl_s1xW = GHC.Base.++ @ GHC.Types.Char lvl_s1xX

lvl_s1xS :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xS = GHC.Base.unpackCString# "Disj "

lvl_s1xN :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xN = GHC.Base.unpackCString# "Conj "

lvl_s1xI :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xI = GHC.Base.unpackCString# "Equiv "

lvl_s1xD :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xD = GHC.Base.unpackCString# "Impl "

lvl_s1xz :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xz = GHC.Base.unpackCString# "Not "

lvl_s1xw :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1xw = GHC.Types.C# 'F'

lvl_s1xv :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xv =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1xw (GHC.Types.[] @ GHC.Types.Char)

lvl_s1xu :: GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
lvl_s1xu = GHC.Base.++ @ GHC.Types.Char lvl_s1xv

lvl_s1xt :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1xt = GHC.Types.C# 'T'

lvl_s1xs :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xs =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1xt (GHC.Types.[] @ GHC.Types.Char)

lvl_s1xr :: GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
lvl_s1xr = GHC.Base.++ @ GHC.Types.Char lvl_s1xs

lvl_s1xp :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xp = GHC.Base.unpackCString# "VarL "

lvl_s1xh :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xh = GHC.Base.unpackCString# "Infix "

lvl_s1xe :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xe = GHC.Base.unpackCString# "Prefix"

lvl_s1xb :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1xb = GHC.Base.unpackCString# "NotAssociative"

lvl_s1x9 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1x9 = GHC.Base.unpackCString# "RightAssociative"

lvl_s1x7 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1x7 = GHC.Base.unpackCString# "LeftAssociative"

lvl_s1x0 :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1x0 = GHC.Base.unpackCString# "Infix"

$wa_s1MO :: forall b_a1As.
            (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
            -> Text.ParserCombinators.ReadP.P b_a1As
LclId
[Arity 1
 Str: DmdType L]
$wa_s1MO =
  \ (@ b_a1As)
    (w_s1Mu :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1As) ->
    Text.Read.Lex.lex1
      @ b_a1As
      (let {
         lvl_s1P9 :: Text.ParserCombinators.ReadP.P b_a1As
         LclId
         []
         lvl_s1P9 = w_s1Mu TestEq.Prefix } in
       \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
         case a3_a1AS of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
           Text.Read.Lex.Ident ds_d1bp [ALWAYS Just S] ->
             case GHC.Base.eqString ds_d1bp lvl_s1xe of _ {
               GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
               GHC.Bool.True -> lvl_s1P9
             }
         })

a_s1KE :: Text.ParserCombinators.ReadPrec.Prec
          -> forall b_a1As.
             (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
             -> Text.ParserCombinators.ReadP.P b_a1As
LclId
[Arity 2
 Worker $wa_s1MO
 Str: DmdType AL]
a_s1KE =
  __inline_me (\ _
                 (@ b_a1As)
                 (w_s1Mu :: TestEq.Fixity
                            -> Text.ParserCombinators.ReadP.P b_a1As) ->
                 $wa_s1MO @ b_a1As w_s1Mu)

lvl_s1wB :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wB = GHC.Base.unpackCString# "Rec "

$wshowsPrec_s1MN :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Prim.Int# -> a_afF -> GHC.Base.String -> [GHC.Types.Char]
LclId
[Arity 3
 Str: DmdType LLL]
$wshowsPrec_s1MN =
  \ (@ a_afF)
    (w_s1Mh :: GHC.Show.Show a_afF)
    (ww_s1Mk :: GHC.Prim.Int#)
    (ww_s1Mo :: a_afF) ->
    let {
      g_s1N3 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1N3 =
        case w_s1Mh
        of _ { GHC.Show.D:Show tpl_Xg1 [ALWAYS Just C(C(S))] _ _ ->
        tpl_Xg1 lvl_s1P3 ww_s1Mo
        } } in
    case GHC.Prim.>=# ww_s1Mk 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char lvl_s1wB (g_s1N3 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               lvl_s1wB
               (g_s1N3
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

lvl_s1wv :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wv = GHC.Base.unpackCString# "Rec"

a_s1FM :: forall a_Xvg.
          (GHC.Read.Read a_Xvg) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvg)
LclId
[Arity 2
 Str: DmdType LL]
a_s1FM =
  \ (@ a_Xvg)
    ($dRead_aTD [ALWAYS Just L] :: GHC.Read.Read a_Xvg)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Rec a_Xvg)
      ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta_X8v [ALWAYS Just L] :: TestEq.Rec a_Xvg
                                      -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1Do 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Rec a_Xvg) @ b_a1As eta_X8v);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1As
                (let {
                   lvl_s1Pb :: a_Xvg -> Text.ParserCombinators.ReadP.P b_a1As
                   LclId
                   [Arity 1]
                   lvl_s1Pb =
                     \ (a3_X1QY :: a_Xvg) -> eta_X8v (TestEq.Rec @ a_Xvg a3_X1QY) } in
                 \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                   case a3_a1AS of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                     Text.Read.Lex.Ident ds_d1bB [ALWAYS Just S] ->
                       case GHC.Base.eqString ds_d1bB lvl_s1wv of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         GHC.Bool.True ->
                           case $dRead_aTD
                           of _ { GHC.Read.D:Read _ _ tpl_XfR [ALWAYS Just C(C(S))] _ ->
                           (((tpl_XfR
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xvg
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xvg
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_Xvg)))
                               lvl_s1P3)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xvg
                                    :: Text.ParserCombinators.ReadP.ReadP a_Xvg
                                         ~
                                       (forall b_a1As.
                                        (a_Xvg -> Text.ParserCombinators.ReadP.P b_a1As)
                                        -> Text.ParserCombinators.ReadP.P b_a1As)))
                             @ b_a1As lvl_s1Pb
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Rec a_Xvg)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvg)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.Rec a_Xvg -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvg)))
      eta_B1

a_s1FQ :: forall a_Xvk.
          (GHC.Read.Read a_Xvk) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xvk]
LclId
[Arity 1
 Str: DmdType L]
a_s1FQ =
  \ (@ a_Xvk) ($dRead_X19j :: GHC.Read.Read a_Xvk) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Rec a_Xvk)
      ((a_s1FM @ a_Xvk $dRead_X19j)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvk))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvk))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvk)))

lvl_s1wo :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wo = GHC.Base.unpackCString# "Var "

$wshowsPrec_s1MM :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Prim.Int# -> a_afG -> GHC.Base.String -> [GHC.Types.Char]
LclId
[Arity 3
 Str: DmdType LLL]
$wshowsPrec_s1MM =
  \ (@ a_afG)
    (w_s1M4 :: GHC.Show.Show a_afG)
    (ww_s1M7 :: GHC.Prim.Int#)
    (ww_s1Mb :: a_afG) ->
    let {
      g_s1N7 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1N7 =
        case w_s1M4
        of _ { GHC.Show.D:Show tpl_XfC [ALWAYS Just C(C(S))] _ _ ->
        tpl_XfC lvl_s1P3 ww_s1Mb
        } } in
    case GHC.Prim.>=# ww_s1M7 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char lvl_s1wo (g_s1N7 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               lvl_s1wo
               (g_s1N7
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

lvl_s1wi :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wi = GHC.Base.unpackCString# "Var"

a_s1Fq :: forall a_XuS.
          (GHC.Read.Read a_XuS) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuS)
LclId
[Arity 2
 Str: DmdType LL]
a_s1Fq =
  \ (@ a_XuS)
    ($dRead_aUH [ALWAYS Just L] :: GHC.Read.Read a_XuS)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Var a_XuS)
      ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta_X8k [ALWAYS Just L] :: TestEq.Var a_XuS
                                      -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1Do 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Var a_XuS) @ b_a1As eta_X8k);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1As
                (let {
                   lvl_s1Pd :: a_XuS -> Text.ParserCombinators.ReadP.P b_a1As
                   LclId
                   [Arity 1]
                   lvl_s1Pd =
                     \ (a3_X1Qz :: a_XuS) -> eta_X8k (TestEq.Var @ a_XuS a3_X1Qz) } in
                 \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                   case a3_a1AS of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                     Text.Read.Lex.Ident ds_d1bI [ALWAYS Just S] ->
                       case GHC.Base.eqString ds_d1bI lvl_s1wi of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         GHC.Bool.True ->
                           case $dRead_aUH
                           of _ { GHC.Read.D:Read _ _ tpl_Xfs [ALWAYS Just C(C(S))] _ ->
                           (((tpl_Xfs
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XuS
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_XuS
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_XuS)))
                               lvl_s1P3)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XuS
                                    :: Text.ParserCombinators.ReadP.ReadP a_XuS
                                         ~
                                       (forall b_a1As.
                                        (a_XuS -> Text.ParserCombinators.ReadP.P b_a1As)
                                        -> Text.ParserCombinators.ReadP.P b_a1As)))
                             @ b_a1As lvl_s1Pd
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Var a_XuS)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuS)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.Var a_XuS -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuS)))
      eta_B1

a_s1Fu :: forall a_XuW.
          (GHC.Read.Read a_XuW) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuW]
LclId
[Arity 1
 Str: DmdType L]
a_s1Fu =
  \ (@ a_XuW) ($dRead_X19Y :: GHC.Read.Read a_XuW) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Var a_XuW)
      ((a_s1Fq @ a_XuW $dRead_X19Y)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuW))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuW))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuW)))

lvl_s1wb :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1wb = GHC.Base.unpackCString# "C "

$wshowsPrec_s1ML :: forall c_afH a_afI.
                    (GHC.Show.Show a_afI) =>
                    GHC.Prim.Int# -> a_afI -> GHC.Base.String -> [GHC.Types.Char]
LclId
[Arity 3
 Str: DmdType LLL]
$wshowsPrec_s1ML =
  \ (@ c_afH)
    (@ a_afI)
    (w_s1LR :: GHC.Show.Show a_afI)
    (ww_s1LU :: GHC.Prim.Int#)
    (ww_s1LY :: a_afI) ->
    let {
      g_s1Nb [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1Nb =
        case w_s1LR
        of _ { GHC.Show.D:Show tpl_Xfe [ALWAYS Just C(C(S))] _ _ ->
        tpl_Xfe lvl_s1P3 ww_s1LY
        } } in
    case GHC.Prim.>=# ww_s1LU 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char lvl_s1wb (g_s1Nb x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               lvl_s1wb
               (g_s1Nb
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

lvl_s1vY :: GHC.Types.Int
LclId
[Str: DmdType m]
lvl_s1vY = GHC.Types.I# 7

lvl_s1vX :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vX = GHC.Base.unpackCString# " :*: "

$wshowsPrec_s1MK :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Prim.Int#
                    -> a_afJ
                    -> b_afK
                    -> GHC.Base.String
                    -> GHC.Base.String
LclId
[Arity 5
 Str: DmdType LLLLL]
$wshowsPrec_s1MK =
  \ (@ a_afJ)
    (@ b_afK)
    (w_s1LC :: GHC.Show.Show a_afJ)
    (w_s1LD :: GHC.Show.Show b_afK)
    (ww_s1LG :: GHC.Prim.Int#)
    (ww_s1LK :: a_afJ)
    (ww_s1LL :: b_afK) ->
    let {
      g_s1Nf [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1Nf =
        case w_s1LD
        of _ { GHC.Show.D:Show tpl_XeQ [ALWAYS Just C(C(S))] _ _ ->
        tpl_XeQ lvl_s1vY ww_s1LL
        } } in
    let {
      f_s1Nd [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      f_s1Nd =
        case w_s1LC
        of _ { GHC.Show.D:Show tpl_XeM [ALWAYS Just C(C(S))] _ _ ->
        tpl_XeM lvl_s1vY ww_s1LK
        } } in
    case GHC.Prim.>=# ww_s1LG 7 of _ {
      GHC.Bool.False ->
        \ (x_X1w8 :: GHC.Base.String) ->
          f_s1Nd (GHC.Base.++ @ GHC.Types.Char lvl_s1vX (g_s1Nf x_X1w8));
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (f_s1Nd
               (GHC.Base.++
                  @ GHC.Types.Char
                  lvl_s1vX
                  (g_s1Nf
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
    }

lvl_s1vQ :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vQ = GHC.Base.unpackCString# ":*:"

a_s1EA :: forall a_XtY b_Xu0.
          (GHC.Read.Read a_XtY, GHC.Read.Read b_Xu0) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP (a_XtY TestEq.:*: b_Xu0)
LclId
[Arity 3
 Str: DmdType LLL]
a_s1EA =
  \ (@ a_XtY)
    (@ b_Xu0)
    ($dRead_aWX :: GHC.Read.Read a_XtY)
    ($dRead_aWY [ALWAYS Just L] :: GHC.Read.Read b_Xu0)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (a_XtY TestEq.:*: b_Xu0)
      (let {
         ds1_s1Ni [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadP
                                       a_XtY
         LclId
         [Str: DmdType]
         ds1_s1Ni =
           case $dRead_aWX
           of _ { GHC.Read.D:Read _ _ tpl_Xeu [ALWAYS Just C(S)] _ ->
           (tpl_Xeu
            `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XtY
                    :: Text.ParserCombinators.ReadPrec.ReadPrec a_XtY
                         ~
                       (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP a_XtY)))
             lvl_s1vY
           } } in
       (\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta_X5L :: (a_XtY TestEq.:*: b_Xu0)
                      -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do [ALWAYS Just L] ->
          case GHC.Prim.<=# x_a1Do 6 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (a_XtY TestEq.:*: b_Xu0) @ b_a1As eta_X5L);
            GHC.Bool.True ->
              (ds1_s1Ni
               `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XtY
                       :: Text.ParserCombinators.ReadP.ReadP a_XtY
                            ~
                          (forall b_a1As.
                           (a_XtY -> Text.ParserCombinators.ReadP.P b_a1As)
                           -> Text.ParserCombinators.ReadP.P b_a1As)))
                @ b_a1As
                (\ (a3_a1AS [ALWAYS Just L] :: a_XtY) ->
                   Text.Read.Lex.lex1
                     @ b_a1As
                     (let {
                        lvl_s1Ph :: b_Xu0 -> Text.ParserCombinators.ReadP.P b_a1As
                        LclId
                        [Arity 1]
                        lvl_s1Ph =
                          \ (a3_X1PM :: b_Xu0) ->
                            eta_X5L (TestEq.:*: @ a_XtY @ b_Xu0 a3_a1AS a3_X1PM) } in
                      \ (a3_X1Kt :: Text.Read.Lex.Lexeme) ->
                        case a3_X1Kt of _ {
                          __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                          Text.Read.Lex.Symbol ds_d1c2 [ALWAYS Just S] ->
                            case GHC.Base.eqString ds_d1c2 lvl_s1vQ of _ {
                              GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                              GHC.Bool.True ->
                                case $dRead_aWY
                                of _ { GHC.Read.D:Read _ _ tpl_Xez [ALWAYS Just C(C(S))] _ ->
                                (((tpl_Xez
                                   `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec b_Xu0
                                           :: Text.ParserCombinators.ReadPrec.ReadPrec b_Xu0
                                                ~
                                              (Text.ParserCombinators.ReadPrec.Prec
                                               -> Text.ParserCombinators.ReadP.ReadP b_Xu0)))
                                    lvl_s1vY)
                                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_Xu0
                                         :: Text.ParserCombinators.ReadP.ReadP b_Xu0
                                              ~
                                            (forall b_a1As.
                                             (b_Xu0 -> Text.ParserCombinators.ReadP.P b_a1As)
                                             -> Text.ParserCombinators.ReadP.P b_a1As)))
                                  @ b_a1As lvl_s1Ph
                                }
                            }
                        }))
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (a_XtY TestEq.:*: b_Xu0)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (a_XtY TestEq.:*: b_Xu0)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      ((a_XtY TestEq.:*: b_Xu0) -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_XtY TestEq.:*: b_Xu0)))
      eta_B1

a_s1EE :: forall a_Xu4 b_Xu6.
          (GHC.Read.Read a_Xu4, GHC.Read.Read b_Xu6) =>
          Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [a_Xu4 TestEq.:*: b_Xu6]
LclId
[Arity 2
 Str: DmdType LL]
a_s1EE =
  \ (@ a_Xu4)
    (@ b_Xu6)
    ($dRead_X1bk :: GHC.Read.Read a_Xu4)
    ($dRead_X1bm :: GHC.Read.Read b_Xu6) ->
    GHC.Read.$dmreadList2
      @ (a_Xu4 TestEq.:*: b_Xu6)
      ((a_s1EA @ a_Xu4 @ b_Xu6 $dRead_X1bk $dRead_X1bm)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xu4 TestEq.:*: b_Xu6))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xu4 TestEq.:*: b_Xu6))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xu4 TestEq.:*: b_Xu6)))

lvl_s1vJ :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vJ = GHC.Base.unpackCString# "R "

lvl_s1vF :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vF = GHC.Base.unpackCString# "L "

$wshowsPrec_s1MJ :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Prim.Int#
                    -> (a_afL TestEq.:+: b_afM)
                    -> GHC.Base.String
                    -> [GHC.Types.Char]
LclId
[Arity 4
 Str: DmdType LLLS]
$wshowsPrec_s1MJ =
  \ (@ a_afL)
    (@ b_afM)
    (w_s1Lq :: GHC.Show.Show a_afL)
    (w_s1Lr :: GHC.Show.Show b_afM)
    (ww_s1Lu :: GHC.Prim.Int#)
    (w_s1Lw :: a_afL TestEq.:+: b_afM) ->
    case w_s1Lw of _ {
      TestEq.L b1_ayF [ALWAYS Just L] ->
        let {
          g_s1Nn [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nn =
            case w_s1Lq
            of _ { GHC.Show.D:Show tpl_Xei [ALWAYS Just C(C(S))] _ _ ->
            tpl_Xei lvl_s1P3 b1_ayF
            } } in
        case GHC.Prim.>=# ww_s1Lu 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1vF (g_s1Nn x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1vF
                   (g_s1Nn
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.R b1_ayH [ALWAYS Just L] ->
        let {
          g_s1Np [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Np =
            case w_s1Lr
            of _ { GHC.Show.D:Show tpl_Xei [ALWAYS Just C(C(S))] _ _ ->
            tpl_Xei lvl_s1P3 b1_ayH
            } } in
        case GHC.Prim.>=# ww_s1Lu 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1vJ (g_s1Np x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1vJ
                   (g_s1Np
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        }
    }

lvl_s1vj :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1vj = GHC.Types.C# 'U'

lvl_s1vi :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1vi =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1vj (GHC.Types.[] @ GHC.Types.Char)

lvl_s1uK :: TestEq.C TestEq.List_Nil_ TestEq.U
LclId
[Str: DmdType m]
lvl_s1uK = TestEq.C @ TestEq.List_Nil_ @ TestEq.U TestEq.U

lvl_s1uJ :: forall a_agN.
            TestEq.C TestEq.List_Nil_ TestEq.U
            TestEq.:+: TestEq.C
                         TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
LclId
[Str: DmdType]
lvl_s1uJ =
  \ (@ a_agN) ->
    TestEq.L
      @ (TestEq.C TestEq.List_Nil_ TestEq.U)
      @ (TestEq.C
           TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
      lvl_s1uK

lvl_s1uI :: forall c_aFl. TestEq.C c_aFl TestEq.U
LclId
[Str: DmdType m]
lvl_s1uI = \ (@ c_aFl) -> TestEq.C @ c_aFl @ TestEq.U TestEq.U

lvl_s1uH :: forall t_aF3 c_aFf c_aFl.
            TestEq.C c_aFl TestEq.U
            TestEq.:+: TestEq.C
                         c_aFf
                         (TestEq.Var t_aF3
                          TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                      TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
LclId
[Str: DmdType]
lvl_s1uH =
  \ (@ t_aF3) (@ c_aFf) (@ c_aFl) ->
    TestEq.L
      @ (TestEq.C c_aFl TestEq.U)
      @ (TestEq.C
           c_aFf
           (TestEq.Var t_aF3
            TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                        TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
      (lvl_s1uI @ c_aFl)

lvl_s1uG :: forall c_aCl. TestEq.C c_aCl TestEq.U
LclId
[Str: DmdType m]
lvl_s1uG = \ (@ c_aCl) -> TestEq.C @ c_aCl @ TestEq.U TestEq.U

lvl_s1uF :: forall c_aCl c_aCz c_aCT c_aDg c_aDG c_aE7.
            TestEq.C c_aCl TestEq.U
            TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                        TestEq.:+: (TestEq.C
                                      c_aCT
                                      (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                    TestEq.:+: (TestEq.C
                                                  c_aDg
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              c_aDG
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: TestEq.C
                                                                         c_aE7
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)))))
LclId
[Str: DmdType]
lvl_s1uF =
  \ (@ c_aCl) (@ c_aCz) (@ c_aCT) (@ c_aDg) (@ c_aDG) (@ c_aE7) ->
    TestEq.L
      @ (TestEq.C c_aCl TestEq.U)
      @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
         TestEq.:+: (TestEq.C
                       c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                     TestEq.:+: (TestEq.C
                                   c_aDg
                                   (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c_aDG
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: TestEq.C
                                                          c_aE7
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)))))
      (lvl_s1uG @ c_aCl)

lvl_s1uE :: forall c_aCb c_aCl c_aCz c_aCT c_aDg c_aDG c_aE7.
            TestEq.C c_aCb TestEq.U
            TestEq.:+: (TestEq.C c_aCl TestEq.U
                        TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                    TestEq.:+: (TestEq.C
                                                  c_aCT
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              c_aDg
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          c_aDG
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: TestEq.C
                                                                                     c_aE7
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic))))))
LclId
[Str: DmdType]
lvl_s1uE =
  \ (@ c_aCb)
    (@ c_aCl)
    (@ c_aCz)
    (@ c_aCT)
    (@ c_aDg)
    (@ c_aDG)
    (@ c_aE7) ->
    TestEq.R
      @ (TestEq.C c_aCb TestEq.U)
      @ (TestEq.C c_aCl TestEq.U
         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                     TestEq.:+: (TestEq.C
                                   c_aCT
                                   (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c_aDg
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c_aDG
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: TestEq.C
                                                                      c_aE7
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic))))))
      (lvl_s1uF @ c_aCl @ c_aCz @ c_aCT @ c_aDg @ c_aDG @ c_aE7)

lvl_s1uD :: forall c_aC3 c_aCb c_aCl c_aCz c_aCT c_aDg c_aDG c_aE7.
            TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
            TestEq.:+: (TestEq.C c_aCb TestEq.U
                        TestEq.:+: (TestEq.C c_aCl TestEq.U
                                    TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              c_aCT
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          c_aDg
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: (TestEq.C
                                                                                      c_aDG
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)
                                                                                    TestEq.:+: TestEq.C
                                                                                                 c_aE7
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)))))))
LclId
[Str: DmdType]
lvl_s1uD =
  \ (@ c_aC3)
    (@ c_aCb)
    (@ c_aCl)
    (@ c_aCz)
    (@ c_aCT)
    (@ c_aDg)
    (@ c_aDG)
    (@ c_aE7) ->
    TestEq.R
      @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
      @ (TestEq.C c_aCb TestEq.U
         TestEq.:+: (TestEq.C c_aCl TestEq.U
                     TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c_aCT
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c_aDg
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c_aDG
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: TestEq.C
                                                                                  c_aE7
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic
                                                                                   TestEq.:*: TestEq.Rec
                                                                                                TestEq.Logic)))))))
      (lvl_s1uE @ c_aCb @ c_aCl @ c_aCz @ c_aCT @ c_aDg @ c_aDG @ c_aE7)

lvl_s1uC :: forall c_aCb. TestEq.C c_aCb TestEq.U
LclId
[Str: DmdType m]
lvl_s1uC = \ (@ c_aCb) -> TestEq.C @ c_aCb @ TestEq.U TestEq.U

lvl_s1uB :: forall c_aCb c_aCl c_aCz c_aCT c_aDg c_aDG c_aE7.
            TestEq.C c_aCb TestEq.U
            TestEq.:+: (TestEq.C c_aCl TestEq.U
                        TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                    TestEq.:+: (TestEq.C
                                                  c_aCT
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              c_aDg
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          c_aDG
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: TestEq.C
                                                                                     c_aE7
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic))))))
LclId
[Str: DmdType]
lvl_s1uB =
  \ (@ c_aCb)
    (@ c_aCl)
    (@ c_aCz)
    (@ c_aCT)
    (@ c_aDg)
    (@ c_aDG)
    (@ c_aE7) ->
    TestEq.L
      @ (TestEq.C c_aCb TestEq.U)
      @ (TestEq.C c_aCl TestEq.U
         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                     TestEq.:+: (TestEq.C
                                   c_aCT
                                   (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c_aDg
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c_aDG
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: TestEq.C
                                                                      c_aE7
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic))))))
      (lvl_s1uC @ c_aCb)

lvl_s1uA :: forall c_aC3 c_aCb c_aCl c_aCz c_aCT c_aDg c_aDG c_aE7.
            TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
            TestEq.:+: (TestEq.C c_aCb TestEq.U
                        TestEq.:+: (TestEq.C c_aCl TestEq.U
                                    TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                TestEq.:+: (TestEq.C
                                                              c_aCT
                                                              (TestEq.Rec TestEq.Logic
                                                               TestEq.:*: TestEq.Rec TestEq.Logic)
                                                            TestEq.:+: (TestEq.C
                                                                          c_aDg
                                                                          (TestEq.Rec TestEq.Logic
                                                                           TestEq.:*: TestEq.Rec
                                                                                        TestEq.Logic)
                                                                        TestEq.:+: (TestEq.C
                                                                                      c_aDG
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)
                                                                                    TestEq.:+: TestEq.C
                                                                                                 c_aE7
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)))))))
LclId
[Str: DmdType]
lvl_s1uA =
  \ (@ c_aC3)
    (@ c_aCb)
    (@ c_aCl)
    (@ c_aCz)
    (@ c_aCT)
    (@ c_aDg)
    (@ c_aDG)
    (@ c_aE7) ->
    TestEq.R
      @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
      @ (TestEq.C c_aCb TestEq.U
         TestEq.:+: (TestEq.C c_aCl TestEq.U
                     TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c_aCT
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c_aDg
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c_aDG
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: TestEq.C
                                                                                  c_aE7
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic
                                                                                   TestEq.:*: TestEq.Rec
                                                                                                TestEq.Logic)))))))
      (lvl_s1uB @ c_aCb @ c_aCl @ c_aCz @ c_aCT @ c_aDg @ c_aDG @ c_aE7)

lvl_s1uz :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uz = GHC.Base.unpackCString# "[]"

lvl_s1uy :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uy = GHC.Base.unpackCString# "Bin"

lvl_s1uw :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uw = GHC.Base.unpackCString# "VarL"

lvl_s1uv :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uv = GHC.Base.unpackCString# "Impl"

lvl_s1uu :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uu = GHC.Base.unpackCString# "Equiv"

lvl_s1ut :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1ut = GHC.Base.unpackCString# "Conj"

lvl_s1us :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1us = GHC.Base.unpackCString# "Disj"

lvl_s1ur :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1ur = GHC.Base.unpackCString# "Not"

lvl_s1um :: GHC.Types.Int
LclId
[Str: DmdType m]
lvl_s1um = GHC.Types.I# 5

lvl_s1ul :: TestEq.Fixity
LclId
[Str: DmdType]
lvl_s1ul = TestEq.Infix TestEq.RightAssociative lvl_s1um

lvl_s1uk :: GHC.Types.Char
LclId
[Str: DmdType m]
lvl_s1uk = GHC.Types.C# ':'

lvl_s1uj :: [GHC.Types.Char]
LclId
[Str: DmdType]
lvl_s1uj =
  GHC.Types.:
    @ GHC.Types.Char lvl_s1uk (GHC.Types.[] @ GHC.Types.Char)

$s$dmconIsRecord_s1pA :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pA =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pz :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pz =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1py :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1py =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1px :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1px =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pw :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pw =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pv :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pv =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pu :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pu =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pt :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pt =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1ps :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1ps =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pr :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pr =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pq :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pq =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconIsRecord_s1pp :: forall (t_afD :: * -> * -> *) a_afE.
                         t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType A]
$s$dmconIsRecord_s1pp =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

$s$dmconFixity_s1po :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1po =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pn :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pn =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pm :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pm =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pl :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pl =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pk :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pk =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pj :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pj =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pi :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pi =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1ph :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1ph =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pg :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pg =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pf :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pf =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

$s$dmconFixity_s1pe :: forall (t_afB :: * -> * -> *) a_afC.
                       t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
$s$dmconFixity_s1pe =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

a_s1l2 :: TestEq.Associativity
          -> TestEq.Associativity
          -> TestEq.Associativity
LclId
[Arity 2
 Str: DmdType SS]
a_s1l2 =
  \ (x_a1kz :: TestEq.Associativity)
    (y_a1kA :: TestEq.Associativity) ->
    case x_a1kz of _ {
      TestEq.LeftAssociative ->
        case y_a1kA of _ { __DEFAULT -> TestEq.LeftAssociative };
      TestEq.RightAssociative ->
        case y_a1kA of _ {
          TestEq.LeftAssociative -> TestEq.LeftAssociative;
          TestEq.RightAssociative -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.RightAssociative
        };
      TestEq.NotAssociative -> y_a1kA
    }

a_s1l0 :: TestEq.Associativity
          -> TestEq.Associativity
          -> TestEq.Associativity
LclId
[Arity 2
 Str: DmdType SS]
a_s1l0 =
  \ (x_a1kf :: TestEq.Associativity)
    (y_a1kg :: TestEq.Associativity) ->
    case x_a1kf of _ {
      TestEq.LeftAssociative -> y_a1kg;
      TestEq.RightAssociative ->
        case y_a1kg of _ {
          __DEFAULT -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.NotAssociative
        };
      TestEq.NotAssociative ->
        case y_a1kg of _ { __DEFAULT -> TestEq.NotAssociative }
    }

a_s1kY :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kY =
  \ (x_a1jY :: TestEq.Associativity)
    (y_a1jZ :: TestEq.Associativity) ->
    case x_a1jY of _ {
      TestEq.LeftAssociative ->
        case y_a1jZ of _ { __DEFAULT -> GHC.Bool.True };
      TestEq.RightAssociative ->
        case y_a1jZ of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1jZ of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

a_s1kW :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kW =
  \ (x_a1jH :: TestEq.Associativity)
    (y_a1jI :: TestEq.Associativity) ->
    case x_a1jH of _ {
      TestEq.LeftAssociative ->
        case y_a1jI of _ { __DEFAULT -> GHC.Bool.False };
      TestEq.RightAssociative ->
        case y_a1jI of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jI of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

a_s1kU :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kU =
  \ (x_a1jq :: TestEq.Associativity)
    (y_a1jr :: TestEq.Associativity) ->
    case x_a1jq of _ {
      TestEq.LeftAssociative ->
        case y_a1jr of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case y_a1jr of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jr of _ { __DEFAULT -> GHC.Bool.True }
    }

a_s1kS :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1kS =
  \ (x_a1j9 :: TestEq.Associativity)
    (y_a1ja :: TestEq.Associativity) ->
    case x_a1j9 of _ {
      TestEq.LeftAssociative ->
        case y_a1ja of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case y_a1ja of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1ja of _ { __DEFAULT -> GHC.Bool.False }
    }

a_s1iV :: TestEq.Associativity -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType S]
a_s1iV =
  \ (x_X1xG :: TestEq.Associativity) ->
    case x_X1xG of _ {
      TestEq.LeftAssociative -> lvl_s1x7;
      TestEq.RightAssociative -> lvl_s1x9;
      TestEq.NotAssociative -> lvl_s1xb
    }

lvl_s1Pl :: Text.ParserCombinators.ReadPrec.Prec
            -> forall b_X1Qz.
               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1Qz)
               -> Text.ParserCombinators.ReadP.P b_X1Qz
LclId
[Arity 2]
lvl_s1Pl =
  \ _
    (@ b_X1Qz)
    (eta_X1QB :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1Qz) ->
    eta_X1QB TestEq.LeftAssociative

lvl_s1Pj :: ([GHC.Types.Char],
             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
LclId
[]
lvl_s1Pj =
  (lvl_s1x7,
   lvl_s1Pl
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1As.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                  -> Text.ParserCombinators.ReadP.P b_a1As)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1Pp :: Text.ParserCombinators.ReadPrec.Prec
            -> forall b_X1QG.
               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1QG)
               -> Text.ParserCombinators.ReadP.P b_X1QG
LclId
[Arity 2]
lvl_s1Pp =
  \ _
    (@ b_X1QG)
    (eta_X1QI :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1QG) ->
    eta_X1QI TestEq.RightAssociative

lvl_s1Pn :: ([GHC.Types.Char],
             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
LclId
[]
lvl_s1Pn =
  (lvl_s1x9,
   lvl_s1Pp
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1As.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                  -> Text.ParserCombinators.ReadP.P b_a1As)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1Pt :: Text.ParserCombinators.ReadPrec.Prec
            -> forall b_X1QN.
               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1QN)
               -> Text.ParserCombinators.ReadP.P b_X1QN
LclId
[Arity 2]
lvl_s1Pt =
  \ _
    (@ b_X1QN)
    (eta_X1QP :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1QN) ->
    eta_X1QP TestEq.NotAssociative

lvl_s1Pr :: ([GHC.Types.Char],
             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
LclId
[]
lvl_s1Pr =
  (lvl_s1xb,
   lvl_s1Pt
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1As.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                  -> Text.ParserCombinators.ReadP.P b_a1As)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1Pq :: [(GHC.Base.String,
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
LclId
[]
lvl_s1Pq =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    lvl_s1Pr
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

lvl_s1Pm :: [(GHC.Base.String,
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
LclId
[]
lvl_s1Pm =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    lvl_s1Pn
    lvl_s1Pq

lvl_s1Pi :: [(GHC.Base.String,
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
LclId
[]
lvl_s1Pi =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    lvl_s1Pj
    lvl_s1Pm

a_s1iD :: Text.ParserCombinators.ReadPrec.ReadPrec
            TestEq.Associativity
LclId
[Str: DmdType]
a_s1iD = GHC.Read.choose1 @ TestEq.Associativity lvl_s1Pi

a_s1iF :: GHC.Types.Int
          -> Text.ParserCombinators.ReadP.ReadS TestEq.Associativity
LclId
[Arity 1
 Str: DmdType L]
a_s1iF =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Associativity
      (((GHC.Read.$fRead()5 @ TestEq.Associativity a_s1iD eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  TestEq.Associativity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                     ~
                   (forall b_a1As.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ TestEq.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ TestEq.Associativity))

a_s1GB :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
LclId
[Arity 1
 Str: DmdType L]
a_s1GB = GHC.Read.$fRead()5 @ TestEq.Associativity a_s1iD

a_s1GD :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
LclId
[Str: DmdType]
a_s1GD =
  GHC.Read.$dmreadList2
    @ TestEq.Associativity
    (a_s1GB
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

a_s1GF :: Text.ParserCombinators.ReadP.P [TestEq.Associativity]
LclId
[Str: DmdType]
a_s1GF =
  ((a_s1GD GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [TestEq.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
                ~
              (forall b_a1As.
               ([TestEq.Associativity] -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As)))
    @ [TestEq.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_return
       @ [TestEq.Associativity])

Rec {
a7_s1ND :: Text.ParserCombinators.ReadPrec.Prec
           -> forall b_a1B4.
              (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1B4)
              -> Text.ParserCombinators.ReadP.P b_a1B4
LclId
[Arity 2
 Str: DmdType AL]
a7_s1ND =
  __inline_me (GHC.Read.$fRead()4
                 @ TestEq.Associativity
                 (a8_s1NE
                  `cast` (trans
                            (trans
                               (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                            (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                          :: (Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                               ~
                             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)))
a8_s1NE [ALWAYS LoopBreaker Nothing] :: Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
LclId
[Arity 1
 Str: DmdType L]
a8_s1NE =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Associativity
    a_s1iD
    (a7_s1ND
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym
                     (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Associativity))
               (sym
                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                     TestEq.Associativity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
end Rec }

$wa_s1MP :: GHC.Prim.Int#
            -> forall b_a1As.
               (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As
LclId
[Arity 2
 Str: DmdType LL]
$wa_s1MP =
  \ (ww_s1MA :: GHC.Prim.Int#)
    (@ b_a1As)
    (w_s1MC :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1As) ->
    case GHC.Prim.<=# ww_s1MA 10 of _ {
      GHC.Bool.False ->
        __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                       @ TestEq.Fixity @ b_a1As w_s1MC);
      GHC.Bool.True ->
        Text.Read.Lex.lex1
          @ b_a1As
          (let {
             lvl_s1Pu :: Text.ParserCombinators.ReadP.P b_a1As
             LclId
             []
             lvl_s1Pu =
               let {
                 k_s1NH :: TestEq.Associativity
                           -> Text.ParserCombinators.ReadP.P b_a1As
                 LclId
                 [Arity 1
                  Str: DmdType L]
                 k_s1NH =
                   \ (a3_X1Sa [ALWAYS Just L] :: TestEq.Associativity) ->
                     ((GHC.Read.$dmreadsPrec14 lvl_s1P3)
                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                              :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                   ~
                                 (forall b_a1As.
                                  (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_a1As)
                                  -> Text.ParserCombinators.ReadP.P b_a1As)))
                       @ b_a1As
                       (\ (a3_X1LK :: GHC.Types.Int) ->
                          w_s1MC (TestEq.Infix a3_X1Sa a3_X1LK)) } in
               Text.ParserCombinators.ReadP.$fMonadPlusP_mplus
                 @ b_a1As
                 ((((a_s1iD
                     `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                               TestEq.Associativity
                             :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity
                                  ~
                                (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)))
                      lvl_s1P3)
                   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                             TestEq.Associativity
                           :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                                ~
                              (forall b_a1As.
                               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                               -> Text.ParserCombinators.ReadP.P b_a1As)))
                    @ b_a1As k_s1NH)
                 (GHC.Read.$wa
                    @ TestEq.Associativity
                    (a8_s1NE
                     `cast` (trans
                               (trans
                                  (sym
                                     (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                        TestEq.Associativity))
                                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                             :: (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                                  ~
                                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                    @ b_a1As
                    k_s1NH) } in
           \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
             case a3_a1AS of _ {
               __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
               Text.Read.Lex.Ident ds_d1bu [ALWAYS Just S] ->
                 case GHC.Base.eqString ds_d1bu lvl_s1x0 of _ {
                   GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                   GHC.Bool.True -> lvl_s1Pu
                 }
             })
    }

a_s1KG :: Text.ParserCombinators.ReadPrec.Prec
          -> forall b_a1As.
             (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
             -> Text.ParserCombinators.ReadP.P b_a1As
LclId
[Arity 2
 Worker $wa_s1MP
 Str: DmdType U(L)L]
a_s1KG =
  __inline_me (\ (w_s1My :: Text.ParserCombinators.ReadPrec.Prec)
                 (@ b_a1As)
                 (w_s1MC :: TestEq.Fixity
                            -> Text.ParserCombinators.ReadP.P b_a1As) ->
                 case w_s1My of _ { GHC.Types.I# ww_s1MA ->
                 $wa_s1MP ww_s1MA @ b_a1As w_s1MC
                 })

a_s1H7 :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
LclId
[Arity 1
 Str: DmdType L]
a_s1H7 =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Fixity
    (a_s1KE
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (a_s1KG
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

a_s1iR :: GHC.Types.Int
          -> Text.ParserCombinators.ReadP.ReadS TestEq.Fixity
LclId
[Arity 1
 Str: DmdType L]
a_s1iR =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Fixity
      (((GHC.Read.$fRead()5
           @ TestEq.Fixity
           (a_s1H7
            `cast` (trans
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                      (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
                     ~
                   (forall b_a1As.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ TestEq.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.Fixity))

a_s1H9 :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
LclId
[Arity 1
 Str: DmdType L]
a_s1H9 =
  GHC.Read.$fRead()5
    @ TestEq.Fixity
    (a_s1H7
     `cast` (trans
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

a_s1Hb :: Text.ParserCombinators.ReadPrec.Prec
          -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
LclId
[Str: DmdType]
a_s1Hb =
  GHC.Read.$dmreadList2
    @ TestEq.Fixity
    (a_s1H9
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

a_s1Hd :: Text.ParserCombinators.ReadP.P [TestEq.Fixity]
LclId
[Str: DmdType]
a_s1Hd =
  ((a_s1Hb GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
                ~
              (forall b_a1As.
               ([TestEq.Fixity] -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As)))
    @ [TestEq.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.Fixity])

a_s1gh :: TestEq.U -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType U()]
a_s1gh =
  __inline_me (\ (x_a1gf :: TestEq.U) ->
                 case x_a1gf of _ { TestEq.U -> lvl_s1vi })

a_s1fY :: GHC.Types.Int
          -> Text.ParserCombinators.ReadP.ReadS TestEq.U
LclId
[Arity 1
 Str: DmdType L]
a_s1fY =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.U
      (((GHC.Read.$fRead()5
           @ TestEq.U
           (a_s1KC
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
                      (trans
                         (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                         (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1As.
                           (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                           -> Text.ParserCombinators.ReadP.P b_a1As)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U
                :: Text.ParserCombinators.ReadP.ReadP TestEq.U
                     ~
                   (forall b_a1As.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ TestEq.U
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.U))

a_s1ed :: forall a_XnQ.
          (TestEq.Representable a_XnQ) =>
          TestEq.Rep (TestEq.Rec a_XnQ) -> TestEq.Rep (TestEq.Rec a_XnQ)
LclId
[Arity 2
 Str: DmdType AS]
a_s1ed =
  __inline_me (\ (@ a_XnQ)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Rec a_XnQ)) ->
                 eta_B1)

a_s1eb :: forall a_ah2.
          (TestEq.Representable a_ah2) =>
          TestEq.Rec a_ah2 -> TestEq.Rec a_ah2
LclId
[Arity 2
 Str: DmdType AS]
a_s1eb =
  __inline_me (\ (@ a_ah2) _ (eta_B1 :: TestEq.Rec a_ah2) -> eta_B1)

a_s1e9 :: forall a_XnL.
          (TestEq.Representable a_XnL) =>
          TestEq.Rep (TestEq.Var a_XnL) -> TestEq.Rep (TestEq.Var a_XnL)
LclId
[Arity 2
 Str: DmdType AS]
a_s1e9 =
  __inline_me (\ (@ a_XnL)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Var a_XnL)) ->
                 eta_B1)

a_s1e7 :: forall a_ah4.
          (TestEq.Representable a_ah4) =>
          TestEq.Var a_ah4 -> TestEq.Var a_ah4
LclId
[Arity 2
 Str: DmdType AS]
a_s1e7 =
  __inline_me (\ (@ a_ah4) _ (eta_B1 :: TestEq.Var a_ah4) -> eta_B1)

a_s1e5 :: forall a_XnF c_XnH.
          (TestEq.Representable a_XnF) =>
          TestEq.Rep (TestEq.C c_XnH a_XnF)
          -> TestEq.Rep (TestEq.C c_XnH a_XnF)
LclId
[Arity 2
 Str: DmdType AS]
a_s1e5 =
  __inline_me (\ (@ a_XnF)
                 (@ c_XnH)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.C c_XnH a_XnF)) ->
                 eta_B1)

a_s1e3 :: forall a_ah6 c_ah7.
          (TestEq.Representable a_ah6) =>
          TestEq.C c_ah7 a_ah6 -> TestEq.C c_ah7 a_ah6
LclId
[Arity 2
 Str: DmdType AS]
a_s1e3 =
  __inline_me (\ (@ a_ah6)
                 (@ c_ah7)
                 _
                 (eta_B1 :: TestEq.C c_ah7 a_ah6) ->
                 eta_B1)

a_s1e1 :: forall a_Xnz b_XnB.
          (TestEq.Representable a_Xnz, TestEq.Representable b_XnB) =>
          TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
          -> TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
LclId
[Arity 3
 Str: DmdType AAS]
a_s1e1 =
  __inline_me (\ (@ a_Xnz)
                 (@ b_XnB)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) ->
                 eta_B1)

a_s1dZ :: forall a_aha b_ahb.
          (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
          (a_aha TestEq.:+: b_ahb) -> a_aha TestEq.:+: b_ahb
LclId
[Arity 3
 Str: DmdType AAS]
a_s1dZ =
  __inline_me (\ (@ a_aha)
                 (@ b_ahb)
                 _
                 _
                 (eta_B1 :: a_aha TestEq.:+: b_ahb) ->
                 eta_B1)

a_s1dX :: forall a_Xns b_Xnu.
          (TestEq.Representable a_Xns, TestEq.Representable b_Xnu) =>
          TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
          -> TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
LclId
[Arity 3
 Str: DmdType AAS]
a_s1dX =
  __inline_me (\ (@ a_Xns)
                 (@ b_Xnu)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) ->
                 eta_B1)

a_s1dV :: forall a_ahe b_ahf.
          (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
          (a_ahe TestEq.:*: b_ahf) -> a_ahe TestEq.:*: b_ahf
LclId
[Arity 3
 Str: DmdType AAS]
a_s1dV =
  __inline_me (\ (@ a_ahe)
                 (@ b_ahf)
                 _
                 _
                 (eta_B1 :: a_ahe TestEq.:*: b_ahf) ->
                 eta_B1)

a_s1dR :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dR = TestEq.Not TestEq.F

a_s1dO :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dO = TestEq.Not TestEq.T

a_s1dK :: GHC.Types.Char
LclId
[Str: DmdType m]
a_s1dK = GHC.Types.C# 'x'

a_s1dL :: [GHC.Types.Char]
LclId
[Str: DmdType]
a_s1dL =
  GHC.Types.: @ GHC.Types.Char a_s1dK (GHC.Types.[] @ GHC.Types.Char)

a_s1dM :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dM = TestEq.VarL a_s1dL

a_s1dP :: TestEq.Logic
LclId
[Str: DmdType]
a_s1dP = TestEq.Equiv a_s1dM a_s1dO

a_s1dD :: GHC.Types.Int
LclId
[Str: DmdType m]
a_s1dD = GHC.Types.I# 3

TestEq.$dmconIsRecord :: forall c_afy.
                         (TestEq.Constructor c_afy) =>
                         forall (t_afD :: * -> * -> *) a_afE.
                         t_afD c_afy a_afE -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType AA
 RULES: "SPEC TestEq.$dmconIsRecord [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1oF [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconIsRecord @ TestEq.Logic_F_ $dConstructor_s1oF
              = $s$dmconIsRecord_s1pp
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1oI [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconIsRecord @ TestEq.Logic_T_ $dConstructor_s1oI
              = $s$dmconIsRecord_s1pq
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oL [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Not_ $dConstructor_s1oL
              = $s$dmconIsRecord_s1pr
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oO [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Disj_ $dConstructor_s1oO
              = $s$dmconIsRecord_s1ps
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1oR [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Conj_ $dConstructor_s1oR
              = $s$dmconIsRecord_s1pt
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1oU [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Equiv_ $dConstructor_s1oU
              = $s$dmconIsRecord_s1pu
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oX [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Impl_ $dConstructor_s1oX
              = $s$dmconIsRecord_s1pv
        "SPEC TestEq.$dmconIsRecord [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1p0 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconIsRecord @ TestEq.Logic_Var_ $dConstructor_s1p0
              = $s$dmconIsRecord_s1pw
        "SPEC TestEq.$dmconIsRecord [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1p3 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconIsRecord @ TestEq.Leaf $dConstructor_s1p3
              = $s$dmconIsRecord_s1px
        "SPEC TestEq.$dmconIsRecord [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1p6 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconIsRecord @ TestEq.Bin $dConstructor_s1p6
              = $s$dmconIsRecord_s1py
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Cons_]" ALWAYS
            forall {$dConstructor_s1p9 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Cons_}
              TestEq.$dmconIsRecord @ TestEq.List_Cons_ $dConstructor_s1p9
              = $s$dmconIsRecord_s1pz
        "SPEC TestEq.$dmconIsRecord [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1pc [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconIsRecord @ TestEq.List_Nil_ $dConstructor_s1pc
              = $s$dmconIsRecord_s1pA]
TestEq.$dmconIsRecord =
  __inline_me (\ (@ c_afy) _ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconFixity :: forall c_afy.
                       (TestEq.Constructor c_afy) =>
                       forall (t_afB :: * -> * -> *) a_afC.
                       t_afB c_afy a_afC -> TestEq.Fixity
LclIdX
[Arity 2
 Str: DmdType AA
 RULES: "SPEC TestEq.$dmconFixity [TestEq.Logic_F_]" ALWAYS
            forall {$dConstructor_s1o8 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_F_}
              TestEq.$dmconFixity @ TestEq.Logic_F_ $dConstructor_s1o8
              = $s$dmconFixity_s1po
        "SPEC TestEq.$dmconFixity [TestEq.Logic_T_]" ALWAYS
            forall {$dConstructor_s1ob [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_T_}
              TestEq.$dmconFixity @ TestEq.Logic_T_ $dConstructor_s1ob
              = $s$dmconFixity_s1pn
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Not_]" ALWAYS
            forall {$dConstructor_s1oe [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Not_}
              TestEq.$dmconFixity @ TestEq.Logic_Not_ $dConstructor_s1oe
              = $s$dmconFixity_s1pm
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Disj_]" ALWAYS
            forall {$dConstructor_s1oh [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Disj_}
              TestEq.$dmconFixity @ TestEq.Logic_Disj_ $dConstructor_s1oh
              = $s$dmconFixity_s1pl
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Conj_]" ALWAYS
            forall {$dConstructor_s1ok [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Conj_}
              TestEq.$dmconFixity @ TestEq.Logic_Conj_ $dConstructor_s1ok
              = $s$dmconFixity_s1pk
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Equiv_]" ALWAYS
            forall {$dConstructor_s1on [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Equiv_}
              TestEq.$dmconFixity @ TestEq.Logic_Equiv_ $dConstructor_s1on
              = $s$dmconFixity_s1pj
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Impl_]" ALWAYS
            forall {$dConstructor_s1oq [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Impl_}
              TestEq.$dmconFixity @ TestEq.Logic_Impl_ $dConstructor_s1oq
              = $s$dmconFixity_s1pi
        "SPEC TestEq.$dmconFixity [TestEq.Logic_Var_]" ALWAYS
            forall {$dConstructor_s1ot [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Logic_Var_}
              TestEq.$dmconFixity @ TestEq.Logic_Var_ $dConstructor_s1ot
              = $s$dmconFixity_s1ph
        "SPEC TestEq.$dmconFixity [TestEq.Leaf]" ALWAYS
            forall {$dConstructor_s1ow [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Leaf}
              TestEq.$dmconFixity @ TestEq.Leaf $dConstructor_s1ow
              = $s$dmconFixity_s1pg
        "SPEC TestEq.$dmconFixity [TestEq.Bin]" ALWAYS
            forall {$dConstructor_s1oz [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.Bin}
              TestEq.$dmconFixity @ TestEq.Bin $dConstructor_s1oz
              = $s$dmconFixity_s1pf
        "SPEC TestEq.$dmconFixity [TestEq.List_Nil_]" ALWAYS
            forall {$dConstructor_s1oC [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                                  TestEq.List_Nil_}
              TestEq.$dmconFixity @ TestEq.List_Nil_ $dConstructor_s1oC
              = $s$dmconFixity_s1pe]
TestEq.$dmconFixity =
  __inline_me (\ (@ c_afy) _ (@ t_aFL::* -> * -> *) (@ a_aFM) _ ->
                 TestEq.Prefix)

TestEq.$fRepresentableInt :: TestEq.Representable GHC.Types.Int
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableInt =
  TestEq.D:Representable
    @ GHC.Types.Int
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Int))
     `cast` (TestEq.Rep GHC.Types.Int -> TestEq.TFCo:R:RepInt
             :: (TestEq.Rep GHC.Types.Int -> TestEq.Rep GHC.Types.Int)
                  ~
                (TestEq.Rep GHC.Types.Int -> TestEq.R:RepInt)))
    ((GHC.Base.id @ GHC.Types.Int)
     `cast` (GHC.Types.Int -> sym TestEq.TFCo:R:RepInt
             :: (GHC.Types.Int -> TestEq.R:RepInt)
                  ~
                (GHC.Types.Int -> TestEq.Rep GHC.Types.Int)))

TestEq.$fRepresentableChar :: TestEq.Representable GHC.Types.Char
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableChar =
  TestEq.D:Representable
    @ GHC.Types.Char
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Char))
     `cast` (TestEq.Rep GHC.Types.Char -> TestEq.TFCo:R:RepChar
             :: (TestEq.Rep GHC.Types.Char -> TestEq.Rep GHC.Types.Char)
                  ~
                (TestEq.Rep GHC.Types.Char -> TestEq.R:RepChar)))
    ((GHC.Base.id @ GHC.Types.Char)
     `cast` (GHC.Types.Char -> sym TestEq.TFCo:R:RepChar
             :: (GHC.Types.Char -> TestEq.R:RepChar)
                  ~
                (GHC.Types.Char -> TestEq.Rep GHC.Types.Char)))

TestEq.$fRepresentableU :: TestEq.Representable TestEq.U
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableU =
  TestEq.D:Representable
    @ TestEq.U
    ((GHC.Base.id @ (TestEq.Rep TestEq.U))
     `cast` (TestEq.Rep TestEq.U -> TestEq.TFCo:R:RepU
             :: (TestEq.Rep TestEq.U -> TestEq.Rep TestEq.U)
                  ~
                (TestEq.Rep TestEq.U -> TestEq.R:RepU)))
    ((GHC.Base.id @ TestEq.U)
     `cast` (TestEq.U -> sym TestEq.TFCo:R:RepU
             :: (TestEq.U -> TestEq.R:RepU)
                  ~
                (TestEq.U -> TestEq.Rep TestEq.U)))

TestEq.$fRepresentable:*: :: forall a_ahe b_ahf.
                             (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                             TestEq.Representable (a_ahe TestEq.:*: b_ahf)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRepresentable:*: =
  __inline_me (\ (@ a_Xnx)
                 (@ b_Xnz)
                 ($dRepresentable_X1bW [ALWAYS Just L] :: TestEq.Representable
                                                            a_Xnx)
                 ($dRepresentable_X1bY [ALWAYS Just L] :: TestEq.Representable
                                                            b_Xnz) ->
                 TestEq.D:Representable
                   @ (a_Xnx TestEq.:*: b_Xnz)
                   ((a_s1dX @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                            -> TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz
                            :: (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz))
                                 ~
                               (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.R:Rep:*: a_Xnx b_Xnz)))
                   ((a_s1dV @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable_X1bY)
                    `cast` ((a_Xnx TestEq.:*: b_Xnz)
                            -> sym (TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz)
                            :: ((a_Xnx TestEq.:*: b_Xnz) -> TestEq.R:Rep:*: a_Xnx b_Xnz)
                                 ~
                               ((a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)))))

TestEq.$fRepresentable:+: :: forall a_aha b_ahb.
                             (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                             TestEq.Representable (a_aha TestEq.:+: b_ahb)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRepresentable:+: =
  __inline_me (\ (@ a_XnE)
                 (@ b_XnG)
                 ($dRepresentable_X1bU [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnE)
                 ($dRepresentable_X1bW [ALWAYS Just L] :: TestEq.Representable
                                                            b_XnG) ->
                 TestEq.D:Representable
                   @ (a_XnE TestEq.:+: b_XnG)
                   ((a_s1e1 @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                            -> TestEq.TFCo:R:Rep:+: a_XnE b_XnG
                            :: (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG))
                                 ~
                               (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.R:Rep:+: a_XnE b_XnG)))
                   ((a_s1dZ @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable_X1bW)
                    `cast` ((a_XnE TestEq.:+: b_XnG)
                            -> sym (TestEq.TFCo:R:Rep:+: a_XnE b_XnG)
                            :: ((a_XnE TestEq.:+: b_XnG) -> TestEq.R:Rep:+: a_XnE b_XnG)
                                 ~
                               ((a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG)))))

TestEq.$fRepresentableC :: forall a_ah6 c_ah7.
                           (TestEq.Representable a_ah6) =>
                           TestEq.Representable (TestEq.C c_ah7 a_ah6)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fRepresentableC =
  __inline_me (\ (@ a_XnJ)
                 (@ c_XnL)
                 ($dRepresentable_X1iw [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnJ) ->
                 TestEq.D:Representable
                   @ (TestEq.C c_XnL a_XnJ)
                   ((a_s1e5 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                            -> TestEq.TFCo:R:RepC c_XnL a_XnJ
                            :: (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                                -> TestEq.Rep (TestEq.C c_XnL a_XnJ))
                                 ~
                               (TestEq.Rep (TestEq.C c_XnL a_XnJ) -> TestEq.R:RepC c_XnL a_XnJ)))
                   ((a_s1e3 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.C c_XnL a_XnJ
                            -> sym (TestEq.TFCo:R:RepC c_XnL a_XnJ)
                            :: (TestEq.C c_XnL a_XnJ -> TestEq.R:RepC c_XnL a_XnJ)
                                 ~
                               (TestEq.C c_XnL a_XnJ -> TestEq.Rep (TestEq.C c_XnL a_XnJ)))))

TestEq.$fRepresentableVar :: forall a_ah4.
                             (TestEq.Representable a_ah4) =>
                             TestEq.Representable (TestEq.Var a_ah4)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fRepresentableVar =
  __inline_me (\ (@ a_XnO)
                 ($dRepresentable_X1bL [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnO) ->
                 TestEq.D:Representable
                   @ (TestEq.Var a_XnO)
                   ((a_s1e9 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.TFCo:R:RepVar a_XnO
                            :: (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.Rep (TestEq.Var a_XnO))
                                 ~
                               (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.R:RepVar a_XnO)))
                   ((a_s1e7 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Var a_XnO -> sym (TestEq.TFCo:R:RepVar a_XnO)
                            :: (TestEq.Var a_XnO -> TestEq.R:RepVar a_XnO)
                                 ~
                               (TestEq.Var a_XnO -> TestEq.Rep (TestEq.Var a_XnO)))))

TestEq.$fRepresentableRec :: forall a_ah2.
                             (TestEq.Representable a_ah2) =>
                             TestEq.Representable (TestEq.Rec a_ah2)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fRepresentableRec =
  __inline_me (\ (@ a_XnT)
                 ($dRepresentable_X1bF [ALWAYS Just L] :: TestEq.Representable
                                                            a_XnT) ->
                 TestEq.D:Representable
                   @ (TestEq.Rec a_XnT)
                   ((a_s1ed @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.TFCo:R:RepRec a_XnT
                            :: (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.Rep (TestEq.Rec a_XnT))
                                 ~
                               (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.R:RepRec a_XnT)))
                   ((a_s1eb @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rec a_XnT -> sym (TestEq.TFCo:R:RepRec a_XnT)
                            :: (TestEq.Rec a_XnT -> TestEq.R:RepRec a_XnT)
                                 ~
                               (TestEq.Rec a_XnT -> TestEq.Rep (TestEq.Rec a_XnT)))))

TestEq.$fEqInt :: TestEq.Eq GHC.Types.Int
LclIdX[DFunId]
[Arity 2
 Str: DmdType U(L)U(L)]
TestEq.$fEqInt =
  __inline_me (GHC.Base.eqInt
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Int)
                       :: (GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Int))

TestEq.$fEqChar :: TestEq.Eq GHC.Types.Char
LclIdX[DFunId]
[Arity 2
 Str: DmdType U(L)U(L)]
TestEq.$fEqChar =
  __inline_me (GHC.Base.$fEqChar_==
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
                       :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Char))

Rec {
TestEq.eqTree [ALWAYS LoopBreaker Nothing] :: TestEq.Tree
                                                GHC.Types.Int
                                              -> TestEq.Tree GHC.Types.Int
                                              -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType SS]
TestEq.eqTree =
  \ (x_agA :: TestEq.Tree GHC.Types.Int)
    (y_agB :: TestEq.Tree GHC.Types.Int) ->
    case x_agA of _ {
      TestEq.Leaf ->
        case y_agB of _ {
          TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
        };
      TestEq.Bin x_afO [ALWAYS Just D(T)] l_afP r_afQ ->
        case y_agB of _ {
          TestEq.Leaf -> GHC.Bool.False;
          TestEq.Bin x_Xov [ALWAYS Just U(L)] l_Xox r_Xoz ->
            case x_afO of _ { GHC.Types.I# x_a1yv [ALWAYS Just L] ->
            case x_Xov of _ { GHC.Types.I# y_a1yz [ALWAYS Just L] ->
            case GHC.Prim.==# x_a1yv y_a1yz of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True ->
                case TestEq.eqTree l_afP l_Xox of _ {
                  GHC.Bool.False -> GHC.Bool.False;
                  GHC.Bool.True -> TestEq.eqTree r_afQ r_Xoz
                }
            }
            }
            }
        }
    }
end Rec }

TestEq.$fEqTree :: TestEq.Eq (TestEq.Tree GHC.Types.Int)
LclIdX[DFunId]
[Arity 2
 Str: DmdType SS]
TestEq.$fEqTree =
  TestEq.eqTree
  `cast` (sym (TestEq.NTCo:T:Eq (TestEq.Tree GHC.Types.Int))
          :: (TestEq.Tree GHC.Types.Int
              -> TestEq.Tree GHC.Types.Int
              -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq (TestEq.Tree GHC.Types.Int))

TestEq.eq :: forall a_afN.
             (TestEq.Representable a_afN, TestEq.Eq (TestEq.Rep a_afN)) =>
             a_afN -> a_afN -> GHC.Bool.Bool
LclIdX
[Arity 4
 Str: DmdType LC(C(S))LL]
TestEq.eq =
  \ (@ a_azl)
    ($dRepresentable_azt [ALWAYS Just L] :: TestEq.Representable a_azl)
    ($dEq_azu :: TestEq.Eq (TestEq.Rep a_azl))
    (eta_B2 :: a_azl)
    (eta_B1 :: a_azl) ->
    ($dEq_azu
     `cast` (TestEq.NTCo:T:Eq (TestEq.Rep a_azl)
             :: TestEq.T:Eq (TestEq.Rep a_azl)
                  ~
                (TestEq.Rep a_azl -> TestEq.Rep a_azl -> GHC.Bool.Bool)))
      (case $dRepresentable_azt
       of _ { TestEq.D:Representable _ tpl_B3 [ALWAYS Just C(S)] ->
       tpl_B3 eta_B2
       })
      (case $dRepresentable_azt
       of _ { TestEq.D:Representable _ tpl_B3 [ALWAYS Just C(S)] ->
       tpl_B3 eta_B1
       })

TestEq.eqRec :: forall t_azF.
                (TestEq.Eq t_azF) =>
                TestEq.Rec t_azF -> TestEq.Rec t_azF -> GHC.Bool.Bool
LclIdX
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.eqRec =
  __inline_me (\ (@ t_azF)
                 ($dEq_azL :: TestEq.Eq t_azF)
                 (ds_d17K :: TestEq.Rec t_azF)
                 (ds_d17L :: TestEq.Rec t_azF) ->
                 case ds_d17K of _ { TestEq.Rec x_agu [ALWAYS Just L] ->
                 case ds_d17L of _ { TestEq.Rec x'_agv [ALWAYS Just L] ->
                 ($dEq_azL
                  `cast` (TestEq.NTCo:T:Eq t_azF
                          :: TestEq.T:Eq t_azF ~ (t_azF -> t_azF -> GHC.Bool.Bool)))
                   x_agu x'_agv
                 }
                 })

TestEq.$fEqRec :: forall a_agD.
                  (TestEq.Eq a_agD) =>
                  TestEq.Eq (TestEq.Rec a_agD)
LclIdX[DFunId]
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqRec =
  __inline_me (TestEq.eqRec
               `cast` (forall a_agD.
                       (TestEq.Eq a_agD) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Rec a_agD))
                       :: (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.Rec a_agD -> TestEq.Rec a_agD -> GHC.Bool.Bool)
                            ~
                          (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.T:Eq (TestEq.Rec a_agD))))

TestEq.eqVar :: forall t_azR.
                (TestEq.Eq t_azR) =>
                TestEq.Var t_azR -> TestEq.Var t_azR -> GHC.Bool.Bool
LclIdX
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.eqVar =
  __inline_me (\ (@ t_azR)
                 ($dEq_azX :: TestEq.Eq t_azR)
                 (ds_d17O :: TestEq.Var t_azR)
                 (ds_d17P :: TestEq.Var t_azR) ->
                 case ds_d17O of _ { TestEq.Var x_ags [ALWAYS Just L] ->
                 case ds_d17P of _ { TestEq.Var x'_agt [ALWAYS Just L] ->
                 ($dEq_azX
                  `cast` (TestEq.NTCo:T:Eq t_azR
                          :: TestEq.T:Eq t_azR ~ (t_azR -> t_azR -> GHC.Bool.Bool)))
                   x_ags x'_agt
                 }
                 })

TestEq.$fEqVar :: forall a_agE.
                  (TestEq.Eq a_agE) =>
                  TestEq.Eq (TestEq.Var a_agE)
LclIdX[DFunId]
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqVar =
  __inline_me (TestEq.eqVar
               `cast` (forall a_agE.
                       (TestEq.Eq a_agE) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Var a_agE))
                       :: (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.Var a_agE -> TestEq.Var a_agE -> GHC.Bool.Bool)
                            ~
                          (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.T:Eq (TestEq.Var a_agE))))

TestEq.eqC :: forall t_aA2 t_aA4 t_aA5.
              (TestEq.Eq t_aA4) =>
              TestEq.C t_aA2 t_aA4 -> TestEq.C t_aA5 t_aA4 -> GHC.Bool.Bool
LclIdX
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.eqC =
  __inline_me (\ (@ t_aA2)
                 (@ t_aA4)
                 (@ t_aA5)
                 ($dEq_aAb :: TestEq.Eq t_aA4)
                 (ds_d17S :: TestEq.C t_aA2 t_aA4)
                 (ds_d17T :: TestEq.C t_aA5 t_aA4) ->
                 case ds_d17S of _ { TestEq.C a_agq [ALWAYS Just L] ->
                 case ds_d17T of _ { TestEq.C a'_agr [ALWAYS Just L] ->
                 ($dEq_aAb
                  `cast` (TestEq.NTCo:T:Eq t_aA4
                          :: TestEq.T:Eq t_aA4 ~ (t_aA4 -> t_aA4 -> GHC.Bool.Bool)))
                   a_agq a'_agr
                 }
                 })

TestEq.eqTimes :: forall t_aAi t_aAj.
                  (TestEq.Eq t_aAi, TestEq.Eq t_aAj) =>
                  (t_aAi TestEq.:*: t_aAj)
                  -> (t_aAi TestEq.:*: t_aAj)
                  -> GHC.Bool.Bool
LclIdX
[Arity 4
 Str: DmdType C(C(S))LU(LL)U(LL)]
TestEq.eqTimes =
  __inline_me (\ (@ t_aAi)
                 (@ t_aAj)
                 ($dEq_aAt :: TestEq.Eq t_aAi)
                 ($dEq_aAu :: TestEq.Eq t_aAj)
                 (ds_d17W :: t_aAi TestEq.:*: t_aAj)
                 (ds_d17X :: t_aAi TestEq.:*: t_aAj) ->
                 case ds_d17W of _ { TestEq.:*: a_agi [ALWAYS Just L] b_agj ->
                 case ds_d17X of _ { TestEq.:*: a'_agk [ALWAYS Just L] b'_agl ->
                 case ($dEq_aAt
                       `cast` (TestEq.NTCo:T:Eq t_aAi
                               :: TestEq.T:Eq t_aAi ~ (t_aAi -> t_aAi -> GHC.Bool.Bool)))
                        a_agi a'_agk
                 of _ {
                   GHC.Bool.False -> GHC.Bool.False;
                   GHC.Bool.True ->
                     ($dEq_aAu
                      `cast` (TestEq.NTCo:T:Eq t_aAj
                              :: TestEq.T:Eq t_aAj ~ (t_aAj -> t_aAj -> GHC.Bool.Bool)))
                       b_agj b'_agl
                 }
                 }
                 })

TestEq.$fEq:*: :: forall a_agH b_agI.
                  (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                  TestEq.Eq (a_agH TestEq.:*: b_agI)
LclIdX[DFunId]
[Arity 4
 Str: DmdType C(C(S))LU(LL)U(LL)]
TestEq.$fEq:*: =
  __inline_me (TestEq.eqTimes
               `cast` (forall a_agH b_agI.
                       (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                       sym (TestEq.NTCo:T:Eq (a_agH TestEq.:*: b_agI))
                       :: (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           (a_agH TestEq.:*: b_agI)
                           -> (a_agH TestEq.:*: b_agI)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           TestEq.T:Eq (a_agH TestEq.:*: b_agI))))

TestEq.eqPlus :: forall t_aAB t_aAH.
                 (TestEq.Eq t_aAB, TestEq.Eq t_aAH) =>
                 (t_aAB TestEq.:+: t_aAH)
                 -> (t_aAB TestEq.:+: t_aAH)
                 -> GHC.Bool.Bool
LclIdX
[Arity 4
 Str: DmdType LLSS]
TestEq.eqPlus =
  \ (@ t_aAB)
    (@ t_aAH)
    ($dEq_aAM :: TestEq.Eq t_aAB)
    ($dEq_aAN :: TestEq.Eq t_aAH)
    (ds_d180 :: t_aAB TestEq.:+: t_aAH)
    (ds_d181 :: t_aAB TestEq.:+: t_aAH) ->
    case ds_d180 of _ {
      TestEq.L x_age ->
        case ds_d181 of _ {
          TestEq.L x'_agf [ALWAYS Just L] ->
            ($dEq_aAM
             `cast` (TestEq.NTCo:T:Eq t_aAB
                     :: TestEq.T:Eq t_aAB ~ (t_aAB -> t_aAB -> GHC.Bool.Bool)))
              x_age x'_agf;
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds_d181 of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh [ALWAYS Just L] ->
            ($dEq_aAN
             `cast` (TestEq.NTCo:T:Eq t_aAH
                     :: TestEq.T:Eq t_aAH ~ (t_aAH -> t_aAH -> GHC.Bool.Bool)))
              x_agg x'_agh
        }
    }

TestEq.$fEq:+: :: forall a_agJ b_agK.
                  (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                  TestEq.Eq (a_agJ TestEq.:+: b_agK)
LclIdX[DFunId]
[Arity 4
 Str: DmdType LLSS]
TestEq.$fEq:+: =
  __inline_me (TestEq.eqPlus
               `cast` (forall a_agJ b_agK.
                       (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                       sym (TestEq.NTCo:T:Eq (a_agJ TestEq.:+: b_agK))
                       :: (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           (a_agJ TestEq.:+: b_agK)
                           -> (a_agJ TestEq.:+: b_agK)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           TestEq.T:Eq (a_agJ TestEq.:+: b_agK))))

TestEq.eqU :: forall t_aAS t_aAT. t_aAS -> t_aAT -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType AA]
TestEq.eqU =
  __inline_me (\ (@ t_aAS) (@ t_aAT) _ _ -> GHC.Bool.True)

TestEq.$fEqU :: TestEq.Eq TestEq.U
LclIdX[DFunId]
[Arity 2
 Str: DmdType AA]
TestEq.$fEqU =
  __inline_me ((TestEq.eqU @ TestEq.U @ TestEq.U)
               `cast` (sym (TestEq.NTCo:T:Eq TestEq.U)
                       :: (TestEq.U -> TestEq.U -> GHC.Bool.Bool) ~ TestEq.T:Eq TestEq.U))

TestEq.toLogic :: forall t_aB0
                         t_aB6
                         t_aBa
                         t_aBe
                         t_aBk
                         t_aBu
                         t_aBE
                         t_aBM.
                  (TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                   TestEq.:+: (TestEq.C t_aB6 TestEq.U
                               TestEq.:+: (TestEq.C t_aBa TestEq.U
                                           TestEq.:+: (TestEq.C t_aBe (TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     t_aBk
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 t_aBu
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: (TestEq.C
                                                                                             t_aBE
                                                                                             (TestEq.Rec
                                                                                                TestEq.Logic
                                                                                              TestEq.:*: TestEq.Rec
                                                                                                           TestEq.Logic)
                                                                                           TestEq.:+: TestEq.C
                                                                                                        t_aBM
                                                                                                        (TestEq.Rec
                                                                                                           TestEq.Logic
                                                                                                         TestEq.:*: TestEq.Rec
                                                                                                                      TestEq.Logic))))))))
                  -> TestEq.Logic
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.toLogic =
  \ (@ t_aB0)
    (@ t_aB6)
    (@ t_aBa)
    (@ t_aBe)
    (@ t_aBk)
    (@ t_aBu)
    (@ t_aBE)
    (@ t_aBM)
    (ds_d18a :: TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                TestEq.:+: (TestEq.C t_aB6 TestEq.U
                            TestEq.:+: (TestEq.C t_aBa TestEq.U
                                        TestEq.:+: (TestEq.C t_aBe (TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  t_aBk
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: (TestEq.C
                                                                              t_aBu
                                                                              (TestEq.Rec
                                                                                 TestEq.Logic
                                                                               TestEq.:*: TestEq.Rec
                                                                                            TestEq.Logic)
                                                                            TestEq.:+: (TestEq.C
                                                                                          t_aBE
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic)
                                                                                        TestEq.:+: TestEq.C
                                                                                                     t_aBM
                                                                                                     (TestEq.Rec
                                                                                                        TestEq.Logic
                                                                                                      TestEq.:*: TestEq.Rec
                                                                                                                   TestEq.Logic)))))))) ->
    case ds_d18a of _ {
      TestEq.L ds_d18D [ALWAYS Just U(U(L))] ->
        case ds_d18D of _ { TestEq.C ds_d18E [ALWAYS Just U(L)] ->
        case ds_d18E of _ { TestEq.Var f0_ag4 [ALWAYS Just L] ->
        TestEq.VarL f0_ag4
        }
        };
      TestEq.R ds_d18b [ALWAYS Just S] ->
        case ds_d18b of _ {
          TestEq.L ds_d18B [ALWAYS Just U(U())] ->
            case ds_d18B of _ { TestEq.C ds_d18C [ALWAYS Just U()] ->
            case ds_d18C of _ { TestEq.U -> TestEq.T }
            };
          TestEq.R ds_d18c [ALWAYS Just S] ->
            case ds_d18c of _ {
              TestEq.L ds_d18z [ALWAYS Just U(U())] ->
                case ds_d18z of _ { TestEq.C ds_d18A [ALWAYS Just U()] ->
                case ds_d18A of _ { TestEq.U -> TestEq.F }
                };
              TestEq.R ds_d18d [ALWAYS Just S] ->
                case ds_d18d of _ {
                  TestEq.L ds_d18x [ALWAYS Just U(U(L))] ->
                    case ds_d18x of _ { TestEq.C ds_d18y [ALWAYS Just U(L)] ->
                    case ds_d18y of _ { TestEq.Rec f0_ag5 [ALWAYS Just L] ->
                    TestEq.Not f0_ag5
                    }
                    };
                  TestEq.R ds_d18e [ALWAYS Just S] ->
                    case ds_d18e of _ {
                      TestEq.L ds_d18t [ALWAYS Just U(U(U(L)U(L)))] ->
                        case ds_d18t of _ { TestEq.C ds_d18u [ALWAYS Just U(U(L)U(L))] ->
                        case ds_d18u
                        of _
                        { TestEq.:*: ds_d18v [ALWAYS Just U(L)]
                                     ds_d18w [ALWAYS Just U(L)] ->
                        case ds_d18v of _ { TestEq.Rec f0_ag6 [ALWAYS Just L] ->
                        case ds_d18w of _ { TestEq.Rec f1_ag7 [ALWAYS Just L] ->
                        TestEq.Impl f0_ag6 f1_ag7
                        }
                        }
                        }
                        };
                      TestEq.R ds_d18f [ALWAYS Just S] ->
                        case ds_d18f of _ {
                          TestEq.L ds_d18p [ALWAYS Just U(U(U(L)U(L)))] ->
                            case ds_d18p of _ { TestEq.C ds_d18q [ALWAYS Just U(U(L)U(L))] ->
                            case ds_d18q
                            of _
                            { TestEq.:*: ds_d18r [ALWAYS Just U(L)]
                                         ds_d18s [ALWAYS Just U(L)] ->
                            case ds_d18r of _ { TestEq.Rec f0_ag8 [ALWAYS Just L] ->
                            case ds_d18s of _ { TestEq.Rec f1_ag9 [ALWAYS Just L] ->
                            TestEq.Equiv f0_ag8 f1_ag9
                            }
                            }
                            }
                            };
                          TestEq.R ds_d18g [ALWAYS Just S] ->
                            case ds_d18g of _ {
                              TestEq.L ds_d18l [ALWAYS Just U(U(U(L)U(L)))] ->
                                case ds_d18l of _ { TestEq.C ds_d18m [ALWAYS Just U(U(L)U(L))] ->
                                case ds_d18m
                                of _
                                { TestEq.:*: ds_d18n [ALWAYS Just U(L)]
                                             ds_d18o [ALWAYS Just U(L)] ->
                                case ds_d18n of _ { TestEq.Rec f0_aga [ALWAYS Just L] ->
                                case ds_d18o of _ { TestEq.Rec f1_agb [ALWAYS Just L] ->
                                TestEq.Conj f0_aga f1_agb
                                }
                                }
                                }
                                };
                              TestEq.R ds_d18h [ALWAYS Just U(U(U(L)U(L)))] ->
                                case ds_d18h of _ { TestEq.C ds_d18i [ALWAYS Just U(U(L)U(L))] ->
                                case ds_d18i
                                of _
                                { TestEq.:*: ds_d18j [ALWAYS Just U(L)]
                                             ds_d18k [ALWAYS Just U(L)] ->
                                case ds_d18j of _ { TestEq.Rec f0_agc [ALWAYS Just L] ->
                                case ds_d18k of _ { TestEq.Rec f1_agd [ALWAYS Just L] ->
                                TestEq.Disj f0_agc f1_agd
                                }
                                }
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

TestEq.fromLogic :: forall c_aC3
                           c_aCb
                           c_aCl
                           c_aCz
                           c_aCT
                           c_aDg
                           c_aDG
                           c_aE7.
                    TestEq.Logic
                    -> TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C c_aCb TestEq.U
                                   TestEq.:+: (TestEq.C c_aCl TestEq.U
                                               TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         c_aCT
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     c_aDg
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 c_aDG
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            c_aE7
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic)))))))
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.fromLogic =
  \ (@ c_aC3)
    (@ c_aCb)
    (@ c_aCl)
    (@ c_aCz)
    (@ c_aCT)
    (@ c_aDg)
    (@ c_aDG)
    (@ c_aE7)
    (ds_d19h :: TestEq.Logic) ->
    case ds_d19h of _ {
      TestEq.VarL f0_afU [ALWAYS Just L] ->
        TestEq.L
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.C
             @ c_aC3
             @ (TestEq.Var GHC.Base.String)
             (TestEq.Var @ GHC.Base.String f0_afU));
      TestEq.T ->
        lvl_s1uA
          @ c_aC3 @ c_aCb @ c_aCl @ c_aCz @ c_aCT @ c_aDg @ c_aDG @ c_aE7;
      TestEq.F ->
        lvl_s1uD
          @ c_aC3 @ c_aCb @ c_aCl @ c_aCz @ c_aCT @ c_aDg @ c_aDG @ c_aE7;
      TestEq.Not f0_afV [ALWAYS Just L] ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.L
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.C
                      @ c_aCz
                      @ (TestEq.Rec TestEq.Logic)
                      (TestEq.Rec @ TestEq.Logic f0_afV)))));
      TestEq.Impl f0_afW [ALWAYS Just L] f1_afX [ALWAYS Just L] ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.L
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.C
                         @ c_aCT
                         @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         (TestEq.:*:
                            @ (TestEq.Rec TestEq.Logic)
                            @ (TestEq.Rec TestEq.Logic)
                            (TestEq.Rec @ TestEq.Logic f0_afW)
                            (TestEq.Rec @ TestEq.Logic f1_afX)))))));
      TestEq.Equiv f0_afY [ALWAYS Just L] f1_afZ [ALWAYS Just L] ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.L
                         @ (TestEq.C
                              c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.C
                            @ c_aDg
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            (TestEq.:*:
                               @ (TestEq.Rec TestEq.Logic)
                               @ (TestEq.Rec TestEq.Logic)
                               (TestEq.Rec @ TestEq.Logic f0_afY)
                               (TestEq.Rec @ TestEq.Logic f1_afZ))))))));
      TestEq.Conj f0_ag0 [ALWAYS Just L] f1_ag1 [ALWAYS Just L] ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.R
                         @ (TestEq.C
                              c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.L
                            @ (TestEq.C
                                 c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            @ (TestEq.C
                                 c_aE7 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            (TestEq.C
                               @ c_aDG
                               @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               (TestEq.:*:
                                  @ (TestEq.Rec TestEq.Logic)
                                  @ (TestEq.Rec TestEq.Logic)
                                  (TestEq.Rec @ TestEq.Logic f0_ag0)
                                  (TestEq.Rec @ TestEq.Logic f1_ag1)))))))));
      TestEq.Disj f0_ag2 [ALWAYS Just L] f1_ag3 [ALWAYS Just L] ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c_aCb TestEq.U
             TestEq.:+: (TestEq.C c_aCl TestEq.U
                         TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c_aCb TestEq.U)
             @ (TestEq.C c_aCl TestEq.U
                TestEq.:+: (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c_aCl TestEq.U)
                @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.R
                         @ (TestEq.C
                              c_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.R
                            @ (TestEq.C
                                 c_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            @ (TestEq.C
                                 c_aE7 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            (TestEq.C
                               @ c_aE7
                               @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               (TestEq.:*:
                                  @ (TestEq.Rec TestEq.Logic)
                                  @ (TestEq.Rec TestEq.Logic)
                                  (TestEq.Rec @ TestEq.Logic f0_ag2)
                                  (TestEq.Rec @ TestEq.Logic f1_ag3)))))))))
    }

TestEq.$fRepresentableLogic :: TestEq.Representable TestEq.Logic
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableLogic =
  TestEq.D:Representable
    @ TestEq.Logic
    ((TestEq.toLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (sym TestEq.TFCo:R:RepLogic -> TestEq.Logic
             :: (TestEq.R:RepLogic -> TestEq.Logic)
                  ~
                (TestEq.Rep TestEq.Logic -> TestEq.Logic)))
    ((TestEq.fromLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (TestEq.Logic -> sym TestEq.TFCo:R:RepLogic
             :: (TestEq.Logic -> TestEq.R:RepLogic)
                  ~
                (TestEq.Logic -> TestEq.Rep TestEq.Logic)))

TestEq.toTree :: forall t_aEI t_aEN t_aEV.
                 (TestEq.C t_aEV TestEq.U
                  TestEq.:+: TestEq.C
                               t_aEI
                               (TestEq.Var t_aEN
                                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aEN))))
                 -> TestEq.Tree t_aEN
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.toTree =
  \ (@ t_aEI)
    (@ t_aEN)
    (@ t_aEV)
    (ds_d19q :: TestEq.C t_aEV TestEq.U
                TestEq.:+: TestEq.C
                             t_aEI
                             (TestEq.Var t_aEN
                              TestEq.:*: (TestEq.Rec (TestEq.Tree t_aEN)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree t_aEN)))) ->
    case ds_d19q of _ {
      TestEq.L ds_d19x [ALWAYS Just U(U())] ->
        case ds_d19x of _ { TestEq.C ds_d19y [ALWAYS Just U()] ->
        case ds_d19y of _ { TestEq.U -> TestEq.Leaf @ t_aEN }
        };
      TestEq.R ds_d19r [ALWAYS Just U(U(U(L)U(U(L)U(L))))] ->
        case ds_d19r
        of _ { TestEq.C ds_d19s [ALWAYS Just U(U(L)U(U(L)U(L)))] ->
        case ds_d19s
        of _
        { TestEq.:*: ds_d19t [ALWAYS Just U(L)]
                     ds_d19u [ALWAYS Just U(U(L)U(L))] ->
        case ds_d19t of _ { TestEq.Var x_afR [ALWAYS Just L] ->
        case ds_d19u
        of _
        { TestEq.:*: ds_d19v [ALWAYS Just U(L)]
                     ds_d19w [ALWAYS Just U(L)] ->
        case ds_d19v of _ { TestEq.Rec l_afS [ALWAYS Just L] ->
        case ds_d19w of _ { TestEq.Rec r_afT [ALWAYS Just L] ->
        TestEq.Bin @ t_aEN x_afR l_afS r_afT
        }
        }
        }
        }
        }
        }
    }

TestEq.fromTree :: forall t_aF3 c_aFf c_aFl.
                   TestEq.Tree t_aF3
                   -> TestEq.C c_aFl TestEq.U
                      TestEq.:+: TestEq.C
                                   c_aFf
                                   (TestEq.Var t_aF3
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
LclIdX
[Arity 1
 Str: DmdType S]
TestEq.fromTree =
  \ (@ t_aF3) (@ c_aFf) (@ c_aFl) (ds_d19J :: TestEq.Tree t_aF3) ->
    case ds_d19J of _ {
      TestEq.Leaf -> lvl_s1uH @ t_aF3 @ c_aFf @ c_aFl;
      TestEq.Bin x_afO [ALWAYS Just L]
                 l_afP [ALWAYS Just L]
                 r_afQ [ALWAYS Just L] ->
        TestEq.R
          @ (TestEq.C c_aFl TestEq.U)
          @ (TestEq.C
               c_aFf
               (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
          (TestEq.C
             @ c_aFf
             @ (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
             (TestEq.:*:
                @ (TestEq.Var t_aF3)
                @ (TestEq.Rec (TestEq.Tree t_aF3)
                   TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))
                (TestEq.Var @ t_aF3 x_afO)
                (TestEq.:*:
                   @ (TestEq.Rec (TestEq.Tree t_aF3))
                   @ (TestEq.Rec (TestEq.Tree t_aF3))
                   (TestEq.Rec @ (TestEq.Tree t_aF3) l_afP)
                   (TestEq.Rec @ (TestEq.Tree t_aF3) r_afQ))))
    }

a_s1et :: forall a_agL.
          TestEq.Tree a_agL
          -> TestEq.C TestEq.Leaf TestEq.U
             TestEq.:+: TestEq.C
                          TestEq.Bin
                          (TestEq.Var a_agL
                           TestEq.:*: (TestEq.Rec (TestEq.Tree a_agL)
                                       TestEq.:*: TestEq.Rec (TestEq.Tree a_agL)))
LclId
[Arity 1
 Str: DmdType S]
a_s1et =
  \ (@ a_agL) (sub_a178 :: TestEq.Tree a_agL) ->
    TestEq.fromTree @ a_agL @ TestEq.Bin @ TestEq.Leaf sub_a178

TestEq.$fRepresentableTree :: forall a_agL.
                              TestEq.Representable (TestEq.Tree a_agL)
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentableTree =
  \ (@ a_XnZ) ->
    TestEq.D:Representable
      @ (TestEq.Tree a_XnZ)
      ((TestEq.toTree @ TestEq.Bin @ a_XnZ @ TestEq.Leaf)
       `cast` (sym (TestEq.TFCo:R:RepTree a_XnZ) -> TestEq.Tree a_XnZ
               :: (TestEq.R:RepTree a_XnZ -> TestEq.Tree a_XnZ)
                    ~
                  (TestEq.Rep (TestEq.Tree a_XnZ) -> TestEq.Tree a_XnZ)))
      ((a_s1et @ a_XnZ)
       `cast` (TestEq.Tree a_XnZ -> sym (TestEq.TFCo:R:RepTree a_XnZ)
               :: (TestEq.Tree a_XnZ -> TestEq.R:RepTree a_XnZ)
                    ~
                  (TestEq.Tree a_XnZ -> TestEq.Rep (TestEq.Tree a_XnZ))))

TestEq.tree0 :: TestEq.Tree GHC.Types.Int
LclIdX
[Str: DmdType]
TestEq.tree0 =
  TestEq.Bin
    @ GHC.Types.Int
    a_s1dD
    (TestEq.Leaf @ GHC.Types.Int)
    (TestEq.Leaf @ GHC.Types.Int)

TestEq.tree1 :: TestEq.Tree GHC.Types.Int
LclIdX
[Str: DmdType]
TestEq.tree1 =
  TestEq.Bin @ GHC.Types.Int lvl_s1um TestEq.tree0 TestEq.tree0

TestEq.t1 :: GHC.Bool.Bool
LclIdX
[Str: DmdType]
TestEq.t1 = TestEq.eqTree TestEq.tree1 TestEq.tree1

TestEq.logic1 :: TestEq.Logic
LclIdX
[Str: DmdType]
TestEq.logic1 = TestEq.Impl a_s1dP a_s1dR

from_a14M :: forall a_agN. [a_agN] -> TestEq.Rep [a_agN]
LclId
[Arity 1
 Str: DmdType S]
from_a14M =
  \ (@ a_agN) (ds_d1dc :: [a_agN]) ->
    case ds_d1dc of _ {
      [] ->
        (lvl_s1uJ @ a_agN)
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN]);
      : a_agO [ALWAYS Just L] as_agP [ALWAYS Just L] ->
        (TestEq.R
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C
              @ TestEq.List_Cons_
              @ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
              (TestEq.:*:
                 @ (TestEq.Var a_agN)
                 @ (TestEq.Rec [a_agN])
                 (TestEq.Var @ a_agN a_agO)
                 (TestEq.Rec @ [a_agN] as_agP))))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN])
    }

to_a14s :: forall a_agN. TestEq.Rep [a_agN] -> [a_agN]
LclId
[Arity 1
 Str: DmdType S]
to_a14s =
  \ (@ a_agN) (ds_d1cW :: TestEq.Rep [a_agN]) ->
    case ds_d1cW
         `cast` (sym (sym (TestEq.TFCo:R:Rep[] a_agN))
                 :: TestEq.Rep [a_agN] ~ TestEq.R:Rep[] a_agN)
    of _ {
      TestEq.L ds_d1d2 [ALWAYS Just U(U())] ->
        case ds_d1d2 of _ { TestEq.C ds_d1d3 [ALWAYS Just U()] ->
        case ds_d1d3 of _ { TestEq.U -> GHC.Types.[] @ a_agN }
        };
      TestEq.R ds_d1cY [ALWAYS Just U(U(U(L)U(L)))] ->
        case ds_d1cY of _ { TestEq.C ds_d1cZ [ALWAYS Just U(U(L)U(L))] ->
        case ds_d1cZ
        of _
        { TestEq.:*: ds_d1d0 [ALWAYS Just U(L)]
                     ds_d1d1 [ALWAYS Just U(L)] ->
        case ds_d1d0 of _ { TestEq.Var a_agZ [ALWAYS Just L] ->
        case ds_d1d1 of _ { TestEq.Rec as_ah0 [ALWAYS Just L] ->
        GHC.Types.: @ a_agN a_agZ as_ah0
        }
        }
        }
        }
    }

TestEq.$fRepresentable[] :: forall a_agN.
                            TestEq.Representable [a_agN]
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fRepresentable[] =
  \ (@ a_agN) ->
    TestEq.D:Representable
      @ [a_agN] (to_a14s @ a_agN) (from_a14M @ a_agN)

conName_a14j :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Nil_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a14j =
  __inline_me (\ (@ t_a14g::* -> * -> *) (@ a_a14h) _ -> lvl_s1uz)

TestEq.$fConstructorList_Nil_ :: TestEq.Constructor
                                   TestEq.List_Nil_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorList_Nil_ =
  TestEq.D:Constructor
    @ TestEq.List_Nil_
    conName_a14j
    $s$dmconFixity_s1pe
    $s$dmconIsRecord_s1pA

conFixity_a14a :: forall (t_afB :: * -> * -> *) a_afC.
                  t_afB TestEq.List_Cons_ a_afC -> TestEq.Fixity
LclId
[Arity 1
 Str: DmdType A]
conFixity_a14a =
  __inline_me (\ (@ t_a147::* -> * -> *) (@ a_a148) _ -> lvl_s1ul)

conName_a144 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.List_Cons_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a144 =
  __inline_me (\ (@ t_a141::* -> * -> *) (@ a_a142) _ -> lvl_s1uj)

TestEq.$fConstructorList_Cons_ :: TestEq.Constructor
                                    TestEq.List_Cons_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorList_Cons_ =
  TestEq.D:Constructor
    @ TestEq.List_Cons_
    conName_a144
    conFixity_a14a
    $s$dmconIsRecord_s1pz

conName_a13T :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Bin a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a13T =
  __inline_me (\ (@ t_a13Q::* -> * -> *) (@ a_a13R) _ -> lvl_s1uy)

TestEq.$fConstructorBin :: TestEq.Constructor TestEq.Bin
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorBin =
  TestEq.D:Constructor
    @ TestEq.Bin conName_a13T $s$dmconFixity_s1pf $s$dmconIsRecord_s1py

conName_a13I :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Leaf a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a13I =
  __inline_me (\ (@ t_a13F::* -> * -> *) (@ a_a13G) _ -> lvl_s1xX)

TestEq.$fConstructorLeaf :: TestEq.Constructor TestEq.Leaf
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLeaf =
  TestEq.D:Constructor
    @ TestEq.Leaf
    conName_a13I
    $s$dmconFixity_s1pg
    $s$dmconIsRecord_s1px

conName_a13i :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Var_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a13i =
  __inline_me (\ (@ t_a13f::* -> * -> *) (@ a_a13g) _ -> lvl_s1uw)

TestEq.$fConstructorLogic_Var_ :: TestEq.Constructor
                                    TestEq.Logic_Var_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Var_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Var_
    conName_a13i
    $s$dmconFixity_s1ph
    $s$dmconIsRecord_s1pw

conName_a137 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Impl_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a137 =
  __inline_me (\ (@ t_a134::* -> * -> *) (@ a_a135) _ -> lvl_s1uv)

TestEq.$fConstructorLogic_Impl_ :: TestEq.Constructor
                                     TestEq.Logic_Impl_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Impl_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Impl_
    conName_a137
    $s$dmconFixity_s1pi
    $s$dmconIsRecord_s1pv

conName_a12W :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Equiv_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12W =
  __inline_me (\ (@ t_a12T::* -> * -> *) (@ a_a12U) _ -> lvl_s1uu)

TestEq.$fConstructorLogic_Equiv_ :: TestEq.Constructor
                                      TestEq.Logic_Equiv_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Equiv_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Equiv_
    conName_a12W
    $s$dmconFixity_s1pj
    $s$dmconIsRecord_s1pu

conName_a12L :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Conj_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12L =
  __inline_me (\ (@ t_a12I::* -> * -> *) (@ a_a12J) _ -> lvl_s1ut)

TestEq.$fConstructorLogic_Conj_ :: TestEq.Constructor
                                     TestEq.Logic_Conj_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Conj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Conj_
    conName_a12L
    $s$dmconFixity_s1pk
    $s$dmconIsRecord_s1pt

conName_a12A :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Disj_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12A =
  __inline_me (\ (@ t_a12x::* -> * -> *) (@ a_a12y) _ -> lvl_s1us)

TestEq.$fConstructorLogic_Disj_ :: TestEq.Constructor
                                     TestEq.Logic_Disj_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Disj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Disj_
    conName_a12A
    $s$dmconFixity_s1pl
    $s$dmconIsRecord_s1ps

conName_a12p :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_Not_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12p =
  __inline_me (\ (@ t_a12m::* -> * -> *) (@ a_a12n) _ -> lvl_s1ur)

TestEq.$fConstructorLogic_Not_ :: TestEq.Constructor
                                    TestEq.Logic_Not_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Not_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Not_
    conName_a12p
    $s$dmconFixity_s1pm
    $s$dmconIsRecord_s1pr

conName_a12e :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_T_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a12e =
  __inline_me (\ (@ t_a12b::* -> * -> *) (@ a_a12c) _ -> lvl_s1xs)

TestEq.$fConstructorLogic_T_ :: TestEq.Constructor TestEq.Logic_T_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_T_ =
  TestEq.D:Constructor
    @ TestEq.Logic_T_
    conName_a12e
    $s$dmconFixity_s1pn
    $s$dmconIsRecord_s1pq

conName_a123 :: forall (t_afz :: * -> * -> *) a_afA.
                t_afz TestEq.Logic_F_ a_afA -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType A]
conName_a123 =
  __inline_me (\ (@ t_a120::* -> * -> *) (@ a_a121) _ -> lvl_s1xv)

TestEq.$fConstructorLogic_F_ :: TestEq.Constructor TestEq.Logic_F_
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_F_ =
  TestEq.D:Constructor
    @ TestEq.Logic_F_
    conName_a123
    $s$dmconFixity_s1po
    $s$dmconIsRecord_s1pp

eq'_a117 :: forall a_agF c_agG.
            (TestEq.Eq a_agF) =>
            TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool
LclId
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
eq'_a117 =
  __inline_me (\ (@ a_agF)
                 (@ c_agG)
                 ($dEq_a110 :: TestEq.Eq a_agF)
                 (eta_B2 :: TestEq.C c_agG a_agF)
                 (eta_B1 :: TestEq.C c_agG a_agF) ->
                 case eta_B2 of _ { TestEq.C a_agq [ALWAYS Just L] ->
                 case eta_B1 of _ { TestEq.C a'_agr [ALWAYS Just L] ->
                 ($dEq_a110
                  `cast` (TestEq.NTCo:T:Eq a_agF
                          :: TestEq.T:Eq a_agF ~ (a_agF -> a_agF -> GHC.Bool.Bool)))
                   a_agq a'_agr
                 }
                 })

TestEq.$fEqC :: forall a_agF c_agG.
                (TestEq.Eq a_agF) =>
                TestEq.Eq (TestEq.C c_agG a_agF)
LclIdX[DFunId]
[Arity 3
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqC =
  __inline_me (eq'_a117
               `cast` (forall a_agF c_agG.
                       (TestEq.Eq a_agF) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.C c_agG a_agF))
                       :: (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool)
                            ~
                          (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.T:Eq (TestEq.C c_agG a_agF))))

Rec {
eq'_a10s [ALWAYS LoopBreaker Nothing] :: forall a_agC.
                                         (TestEq.Eq a_agC) =>
                                         [a_agC] -> [a_agC] -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType L]
eq'_a10s =
  \ (@ a_agC) ($dEq_a10b [ALWAYS Just L] :: TestEq.Eq a_agC) ->
    let {
      a_s1NL :: [a_agC] -> [a_agC] -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      a_s1NL = eq'_a10s @ a_agC $dEq_a10b } in
    \ (eta_Xby :: [a_agC]) (eta_Xn5 :: [a_agC]) ->
      case eta_Xby of _ {
        [] ->
          case eta_Xn5 of _ { [] -> GHC.Bool.True; : _ _ -> GHC.Bool.False };
        : a_agO as_agP ->
          case eta_Xn5 of _ {
            [] -> GHC.Bool.False;
            : a_Xoa [ALWAYS Just L] as_Xoc ->
              case ($dEq_a10b
                    `cast` (TestEq.NTCo:T:Eq a_agC
                            :: TestEq.T:Eq a_agC ~ (a_agC -> a_agC -> GHC.Bool.Bool)))
                     a_agO a_Xoa
              of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True -> a_s1NL as_agP as_Xoc
              }
          }
      }
end Rec }

TestEq.$fEq[] :: forall a_agC.
                 (TestEq.Eq a_agC) =>
                 TestEq.Eq [a_agC]
LclIdX[DFunId]
[Arity 1
 Str: DmdType L]
TestEq.$fEq[] =
  __inline_me (eq'_a10s
               `cast` (forall a_agC.
                       (TestEq.Eq a_agC) =>
                       sym (TestEq.NTCo:T:Eq [a_agC])
                       :: (forall a_agC.
                           (TestEq.Eq a_agC) =>
                           [a_agC] -> [a_agC] -> GHC.Bool.Bool)
                            ~
                          (forall a_agC. (TestEq.Eq a_agC) => TestEq.T:Eq [a_agC])))

a_s1z0 :: [GHC.Types.Char] -> [GHC.Types.Char] -> GHC.Bool.Bool
LclId
[Str: DmdType]
a_s1z0 =
  eq'_a10s
    @ GHC.Types.Char
    (GHC.Base.$fEqChar_==
     `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
             :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                  ~
                TestEq.T:Eq GHC.Types.Char))

Rec {
$wa_s1MH :: TestEq.Logic
            -> TestEq.Rec TestEq.Logic
            -> TestEq.Logic
            -> TestEq.Rec TestEq.Logic
            -> GHC.Bool.Bool
LclId
[Arity 4
 Str: DmdType SLSL]
$wa_s1MH =
  \ (ww_s1L4 :: TestEq.Logic)
    (ww_s1L6 :: TestEq.Rec TestEq.Logic)
    (ww_s1Lc :: TestEq.Logic)
    (ww_s1Le :: TestEq.Rec TestEq.Logic) ->
    case TestEq.eqLogic ww_s1L4 ww_s1Lc of _ {
      GHC.Bool.False -> GHC.Bool.False;
      GHC.Bool.True ->
        case ww_s1L6 of _ { TestEq.Rec x_Xsx [ALWAYS Just S] ->
        case ww_s1Le of _ { TestEq.Rec x'_XsB [ALWAYS Just S] ->
        TestEq.eqLogic x_Xsx x'_XsB
        }
        }
    }
TestEq.eqLogic [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                               -> TestEq.Logic
                                               -> GHC.Bool.Bool
LclIdX
[Arity 2
 Str: DmdType SS]
TestEq.eqLogic =
  \ (x_agy :: TestEq.Logic) (y_agz :: TestEq.Logic) ->
    case TestEq.fromLogic
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           x_agy
    of _ {
      TestEq.L x_age [ALWAYS Just D(D(T))] ->
        case TestEq.fromLogic
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               y_agz
        of _ {
          TestEq.L x'_agf [ALWAYS Just U(U(L))] ->
            case x_age of _ { TestEq.C a_agq [ALWAYS Just U(L)] ->
            case x'_agf of _ { TestEq.C a'_agr [ALWAYS Just U(L)] ->
            case a_agq of _ { TestEq.Var x_ags [ALWAYS Just L] ->
            case a'_agr of _ { TestEq.Var x'_agt [ALWAYS Just L] ->
            a_s1z0 x_ags x'_agt
            }
            }
            }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case TestEq.fromLogic
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               y_agz
        of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh [ALWAYS Just S] ->
            case x_agg of _ {
              TestEq.L x_age [ALWAYS Just D(A)] ->
                case x'_agh of _ {
                  TestEq.L x'_agf [ALWAYS Just U(A)] ->
                    case x_age of _ { TestEq.C _ ->
                    case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
                    };
                  TestEq.R _ -> GHC.Bool.False
                };
              TestEq.R x_XnP ->
                case x'_agh of _ {
                  TestEq.L _ -> GHC.Bool.False;
                  TestEq.R x'_XnT [ALWAYS Just S] ->
                    case x_XnP of _ {
                      TestEq.L x_age [ALWAYS Just D(A)] ->
                        case x'_XnT of _ {
                          TestEq.L x'_agf [ALWAYS Just U(A)] ->
                            case x_age of _ { TestEq.C _ ->
                            case x'_agf of _ { TestEq.C _ -> GHC.Bool.True }
                            };
                          TestEq.R _ -> GHC.Bool.False
                        };
                      TestEq.R x_XsY ->
                        case x'_XnT of _ {
                          TestEq.L _ -> GHC.Bool.False;
                          TestEq.R x'_Xt2 [ALWAYS Just S] ->
                            case x_XsY of _ {
                              TestEq.L x_age [ALWAYS Just D(D(T))] ->
                                case x'_Xt2 of _ {
                                  TestEq.L x'_agf [ALWAYS Just U(U(S))] ->
                                    case x_age of _ { TestEq.C a_agq [ALWAYS Just U(S)] ->
                                    case x'_agf of _ { TestEq.C a'_agr [ALWAYS Just U(S)] ->
                                    case a_agq of _ { TestEq.Rec x_agu [ALWAYS Just S] ->
                                    case a'_agr of _ { TestEq.Rec x'_agv [ALWAYS Just S] ->
                                    TestEq.eqLogic x_agu x'_agv
                                    }
                                    }
                                    }
                                    };
                                  TestEq.R _ -> GHC.Bool.False
                                };
                              TestEq.R x_Xp8 ->
                                case x'_Xt2 of _ {
                                  TestEq.L _ -> GHC.Bool.False;
                                  TestEq.R x'_Xpc [ALWAYS Just S] ->
                                    case x_Xp8 of _ {
                                      TestEq.L x_age [ALWAYS Just D(D(D(T)T))] ->
                                        case x'_Xpc of _ {
                                          TestEq.L x'_agf [ALWAYS Just U(U(U(S)L))] ->
                                            case x_age
                                            of _ { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                            case x'_agf
                                            of _ { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                            case a_agq of _ { TestEq.:*: ww_s1L2 ww_s1L6 ->
                                            case ww_s1L2 of _ { TestEq.Rec ww_s1L4 ->
                                            case a'_agr of _ { TestEq.:*: ww_s1La ww_s1Le ->
                                            case ww_s1La of _ { TestEq.Rec ww_s1Lc ->
                                            $wa_s1MH ww_s1L4 ww_s1L6 ww_s1Lc ww_s1Le
                                            }
                                            }
                                            }
                                            }
                                            }
                                            };
                                          TestEq.R _ -> GHC.Bool.False
                                        };
                                      TestEq.R x_Xpf ->
                                        case x'_Xpc of _ {
                                          TestEq.L _ -> GHC.Bool.False;
                                          TestEq.R x'_Xpj [ALWAYS Just S] ->
                                            case x_Xpf of _ {
                                              TestEq.L x_age [ALWAYS Just D(D(D(T)T))] ->
                                                case x'_Xpj of _ {
                                                  TestEq.L x'_agf [ALWAYS Just U(U(U(S)L))] ->
                                                    case x_age
                                                    of _ { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                                    case x'_agf
                                                    of _ { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                                    case a_agq of _ { TestEq.:*: ww_s1L2 ww_s1L6 ->
                                                    case ww_s1L2 of _ { TestEq.Rec ww_s1L4 ->
                                                    case a'_agr of _ { TestEq.:*: ww_s1La ww_s1Le ->
                                                    case ww_s1La of _ { TestEq.Rec ww_s1Lc ->
                                                    $wa_s1MH ww_s1L4 ww_s1L6 ww_s1Lc ww_s1Le
                                                    }
                                                    }
                                                    }
                                                    }
                                                    }
                                                    };
                                                  TestEq.R _ -> GHC.Bool.False
                                                };
                                              TestEq.R x_Xpm ->
                                                case x'_Xpj of _ {
                                                  TestEq.L _ -> GHC.Bool.False;
                                                  TestEq.R x'_Xpq [ALWAYS Just S] ->
                                                    case x_Xpm of _ {
                                                      TestEq.L x_age [ALWAYS Just D(D(D(T)T))] ->
                                                        case x'_Xpq of _ {
                                                          TestEq.L x'_agf [ALWAYS Just U(U(U(S)L))] ->
                                                            case x_age
                                                            of _
                                                            { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                                            case x'_agf
                                                            of _
                                                            { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                                            case a_agq
                                                            of _ { TestEq.:*: ww_s1L2 ww_s1L6 ->
                                                            case ww_s1L2
                                                            of _ { TestEq.Rec ww_s1L4 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww_s1La ww_s1Le ->
                                                            case ww_s1La
                                                            of _ { TestEq.Rec ww_s1Lc ->
                                                            $wa_s1MH ww_s1L4 ww_s1L6 ww_s1Lc ww_s1Le
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            };
                                                          TestEq.R _ -> GHC.Bool.False
                                                        };
                                                      TestEq.R x_Xpt [ALWAYS Just D(D(D(T)T))] ->
                                                        case x'_Xpq of _ {
                                                          TestEq.L _ -> GHC.Bool.False;
                                                          TestEq.R x'_Xpx [ALWAYS Just U(U(U(S)L))] ->
                                                            case x_Xpt
                                                            of _
                                                            { TestEq.C a_agq [ALWAYS Just U(U(S)L)] ->
                                                            case x'_Xpx
                                                            of _
                                                            { TestEq.C a'_agr [ALWAYS Just U(U(S)L)] ->
                                                            case a_agq
                                                            of _ { TestEq.:*: ww_s1L2 ww_s1L6 ->
                                                            case ww_s1L2
                                                            of _ { TestEq.Rec ww_s1L4 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww_s1La ww_s1Le ->
                                                            case ww_s1La
                                                            of _ { TestEq.Rec ww_s1Lc ->
                                                            $wa_s1MH ww_s1L4 ww_s1L6 ww_s1Lc ww_s1Le
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
end Rec }

TestEq.t2 :: GHC.Bool.Bool
LclIdX
[Str: DmdType]
TestEq.t2 = TestEq.eqLogic TestEq.logic1 TestEq.logic1

TestEq.$fEqLogic :: TestEq.Eq TestEq.Logic
LclIdX[DFunId]
[Arity 2
 Str: DmdType SS]
TestEq.$fEqLogic =
  TestEq.eqLogic
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.Logic)
          :: (TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq TestEq.Logic)

readListDefault_aZF :: Text.ParserCombinators.ReadP.ReadS
                         [TestEq.U]
LclId
[Arity 1
 Str: DmdType L]
readListDefault_aZF =
  Text.ParserCombinators.ReadP.run @ [TestEq.U] a_s1Bv

TestEq.$fReadU :: GHC.Read.Read TestEq.U
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fReadU =
  GHC.Read.D:Read
    @ TestEq.U
    a_s1fY
    readListDefault_aZF
    (a_s1Bf
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
    (a_s1Bt
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.U])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.U])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.U]))

showList_aZy :: [TestEq.U] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aZy =
  \ (ds1_a1gk :: [TestEq.U])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.U ->
           GHC.Base.++
             @ GHC.Types.Char
             lvl_s1vi
             (let {
                lvl12_s1NN :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NN =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1NP [ALWAYS LoopBreaker Nothing] :: [TestEq.U]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NP =
                  \ (ds2_a1gv :: [TestEq.U]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NN;
                      : y_a1gA ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.U ->
                           GHC.Base.++ @ GHC.Types.Char lvl_s1vi (showl_s1NP ys_a1gB)
                           })
                    }; } in
              showl_s1NP xs_a1gr)
           })
    }

showsPrec_aZq :: GHC.Types.Int -> TestEq.U -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType AU()L]
showsPrec_aZq =
  __inline_me (\ _
                 (ds_d1cw :: TestEq.U)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds_d1cw of _ { TestEq.U ->
                 GHC.Base.++ @ GHC.Types.Char lvl_s1vi eta_B1
                 })

TestEq.$fShowU :: GHC.Show.Show TestEq.U
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowU =
  GHC.Show.D:Show @ TestEq.U showsPrec_aZq a_s1gh showList_aZy

readList_aYC :: forall a_afL b_afM.
                (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                Text.ParserCombinators.ReadP.ReadS [a_afL TestEq.:+: b_afM]
LclId
[Arity 2
 Str: DmdType LL]
readList_aYC =
  \ (@ a_XtF)
    (@ b_XtH)
    ($dRead_X1ck :: GHC.Read.Read a_XtF)
    ($dRead_X1cm :: GHC.Read.Read b_XtH) ->
    Text.ParserCombinators.ReadP.run
      @ [a_XtF TestEq.:+: b_XtH]
      (((GHC.Read.$dmreadList2
           @ (a_XtF TestEq.:+: b_XtH)
           ((a_s1E1 @ a_XtF @ b_XtH $dRead_X1ck $dRead_X1cm)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_XtF TestEq.:+: b_XtH))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_XtF TestEq.:+: b_XtH))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_XtF TestEq.:+: b_XtH)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_XtF TestEq.:+: b_XtH]
                :: Text.ParserCombinators.ReadP.ReadP [a_XtF TestEq.:+: b_XtH]
                     ~
                   (forall b_a1As.
                    ([a_XtF TestEq.:+: b_XtH] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [a_XtF TestEq.:+: b_XtH]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_XtF TestEq.:+: b_XtH]))

TestEq.$fRead:+: :: forall a_afL b_afM.
                    (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                    GHC.Read.Read (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:+: =
  __inline_me (\ (@ a_Xty)
                 (@ b_XtA)
                 ($dRead_X1cd [ALWAYS Just L] :: GHC.Read.Read a_Xty)
                 ($dRead_X1cf [ALWAYS Just L] :: GHC.Read.Read b_XtA) ->
                 let {
                   a_s1IW [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [a_Xty TestEq.:+: b_XtA]
                   LclId
                   [Str: DmdType]
                   a_s1IW = a_s1E5 @ a_Xty @ b_XtA $dRead_X1cd $dRead_X1cf } in
                 let {
                   a_s1E3 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  (a_Xty TestEq.:+: b_XtA)
                   LclId
                   [Str: DmdType]
                   a_s1E3 = a_s1E1 @ a_Xty @ b_XtA $dRead_X1cd $dRead_X1cf } in
                 let {
                   a_s1nO [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [a_Xty TestEq.:+: b_XtA]
                   LclId
                   [Str: DmdType]
                   a_s1nO = readList_aYC @ a_Xty @ b_XtA $dRead_X1cd $dRead_X1cf } in
                 letrec {
                   $dRead_s1nK :: GHC.Read.Read (a_Xty TestEq.:+: b_XtA)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1nK =
                     GHC.Read.D:Read
                       @ (a_Xty TestEq.:+: b_XtA)
                       a_s1nL
                       a_s1nO
                       (a_s1E3
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_Xty TestEq.:+: b_XtA))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_Xty TestEq.:+: b_XtA))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_Xty TestEq.:+: b_XtA)))
                       (a_s1IW
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_Xty TestEq.:+: b_XtA])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_Xty TestEq.:+: b_XtA])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_Xty TestEq.:+: b_XtA]));
                   a_s1nL [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_Xty TestEq.:+: b_XtA)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1nL =
                     GHC.Read.$dmreadsPrec @ (a_Xty TestEq.:+: b_XtA) $dRead_s1nK; } in
                 $dRead_s1nK)

showsPrec_aYb :: forall a_afL b_afM.
                 (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                 GHC.Types.Int -> (a_afL TestEq.:+: b_afM) -> GHC.Show.ShowS
LclId
[Arity 4
 Worker $wshowsPrec_s1MJ
 Str: DmdType LLU(L)S]
showsPrec_aYb =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 (w_s1Lq :: GHC.Show.Show a_afL)
                 (w_s1Lr :: GHC.Show.Show b_afM)
                 (w_s1Ls :: GHC.Types.Int)
                 (w_s1Lw :: a_afL TestEq.:+: b_afM) ->
                 case w_s1Ls of _ { GHC.Types.I# ww_s1Lu ->
                 $wshowsPrec_s1MJ @ a_afL @ b_afM w_s1Lq w_s1Lr ww_s1Lu w_s1Lw
                 })

showList_aYn :: forall a_afL b_afM.
                (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
LclId
[Arity 4
 Str: DmdType LLSL]
showList_aYn =
  \ (@ a_XtR)
    (@ b_XtT)
    ($dShow_aXP [ALWAYS Just L] :: GHC.Show.Show a_XtR)
    ($dShow_aXQ [ALWAYS Just L] :: GHC.Show.Show b_XtT)
    (eta_B2 :: [a_XtR TestEq.:+: b_XtT])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          ($wshowsPrec_s1MJ
             @ a_XtR
             @ b_XtT
             $dShow_aXP
             $dShow_aXQ
             0
             x_a1gq
             (let {
                lvl12_s1NR :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NR =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1NT [ALWAYS LoopBreaker Nothing] :: [a_XtR TestEq.:+: b_XtT]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NT =
                  \ (ds2_a1gv :: [a_XtR TestEq.:+: b_XtT]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NR;
                      : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          ($wshowsPrec_s1MJ
                             @ a_XtR
                             @ b_XtT
                             $dShow_aXP
                             $dShow_aXQ
                             0
                             y_a1gA
                             (showl_s1NT ys_a1gB))
                    }; } in
              showl_s1NT xs_a1gr))
    }

TestEq.$fShow:+: :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Show.Show (a_afL TestEq.:+: b_afM)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:+: =
  __inline_me (\ (@ a_XtQ)
                 (@ b_XtS)
                 ($dShow_aXP [ALWAYS Just L] :: GHC.Show.Show a_XtQ)
                 ($dShow_aXQ [ALWAYS Just L] :: GHC.Show.Show b_XtS) ->
                 let {
                   a_s1nE :: [a_XtQ TestEq.:+: b_XtS] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1nE = showList_aYn @ a_XtQ @ b_XtS $dShow_aXP $dShow_aXQ } in
                 let {
                   a_s1nF :: GHC.Types.Int
                             -> (a_XtQ TestEq.:+: b_XtS)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)S]
                   a_s1nF = showsPrec_aYb @ a_XtQ @ b_XtS $dShow_aXP $dShow_aXQ } in
                 letrec {
                   $dShow_s1nC :: GHC.Show.Show (a_XtQ TestEq.:+: b_XtS)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1nC =
                     GHC.Show.D:Show @ (a_XtQ TestEq.:+: b_XtS) a_s1nF a_s1nD a_s1nE;
                   a_s1nD [ALWAYS LoopBreaker Nothing] :: (a_XtQ TestEq.:+: b_XtS)
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1nD =
                     GHC.Show.$dmshow @ (a_XtQ TestEq.:+: b_XtS) $dShow_s1nC; } in
                 $dShow_s1nC)

readList_aXb :: forall a_afJ b_afK.
                (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                Text.ParserCombinators.ReadP.ReadS [a_afJ TestEq.:*: b_afK]
LclId
[Arity 2
 Str: DmdType LL]
readList_aXb =
  \ (@ a_Xua)
    (@ b_Xuc)
    ($dRead_X1bq :: GHC.Read.Read a_Xua)
    ($dRead_X1bs :: GHC.Read.Read b_Xuc) ->
    Text.ParserCombinators.ReadP.run
      @ [a_Xua TestEq.:*: b_Xuc]
      (((GHC.Read.$dmreadList2
           @ (a_Xua TestEq.:*: b_Xuc)
           ((a_s1EA @ a_Xua @ b_Xuc $dRead_X1bq $dRead_X1bs)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_Xua TestEq.:*: b_Xuc))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_Xua TestEq.:*: b_Xuc))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_Xua TestEq.:*: b_Xuc)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_Xua TestEq.:*: b_Xuc]
                :: Text.ParserCombinators.ReadP.ReadP [a_Xua TestEq.:*: b_Xuc]
                     ~
                   (forall b_a1As.
                    ([a_Xua TestEq.:*: b_Xuc] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [a_Xua TestEq.:*: b_Xuc]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_Xua TestEq.:*: b_Xuc]))

TestEq.$fRead:*: :: forall a_afJ b_afK.
                    (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                    GHC.Read.Read (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:*: =
  __inline_me (\ (@ a_Xu3)
                 (@ b_Xu5)
                 ($dRead_X1bj [ALWAYS Just L] :: GHC.Read.Read a_Xu3)
                 ($dRead_X1bl [ALWAYS Just L] :: GHC.Read.Read b_Xu5) ->
                 let {
                   a_s1IY [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [a_Xu3 TestEq.:*: b_Xu5]
                   LclId
                   [Str: DmdType]
                   a_s1IY = a_s1EE @ a_Xu3 @ b_Xu5 $dRead_X1bj $dRead_X1bl } in
                 let {
                   a_s1EC :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (a_Xu3 TestEq.:*: b_Xu5)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1EC = a_s1EA @ a_Xu3 @ b_Xu5 $dRead_X1bj $dRead_X1bl } in
                 let {
                   a_s1ns [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [a_Xu3 TestEq.:*: b_Xu5]
                   LclId
                   [Str: DmdType]
                   a_s1ns = readList_aXb @ a_Xu3 @ b_Xu5 $dRead_X1bj $dRead_X1bl } in
                 letrec {
                   $dRead_s1no :: GHC.Read.Read (a_Xu3 TestEq.:*: b_Xu5)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1no =
                     GHC.Read.D:Read
                       @ (a_Xu3 TestEq.:*: b_Xu5)
                       a_s1np
                       a_s1ns
                       (a_s1EC
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_Xu3 TestEq.:*: b_Xu5))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_Xu3 TestEq.:*: b_Xu5))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_Xu3 TestEq.:*: b_Xu5)))
                       (a_s1IY
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_Xu3 TestEq.:*: b_Xu5])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_Xu3 TestEq.:*: b_Xu5])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_Xu3 TestEq.:*: b_Xu5]));
                   a_s1np [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (a_Xu3 TestEq.:*: b_Xu5)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1np =
                     GHC.Read.$dmreadsPrec @ (a_Xu3 TestEq.:*: b_Xu5) $dRead_s1no; } in
                 $dRead_s1no)

showsPrec_aWK :: forall a_afJ b_afK.
                 (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                 GHC.Types.Int -> (a_afJ TestEq.:*: b_afK) -> GHC.Show.ShowS
LclId
[Arity 4
 Worker $wshowsPrec_s1MK
 Str: DmdType LLU(L)U(LL)]
showsPrec_aWK =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 (w_s1LC :: GHC.Show.Show a_afJ)
                 (w_s1LD :: GHC.Show.Show b_afK)
                 (w_s1LE :: GHC.Types.Int)
                 (w_s1LI :: a_afJ TestEq.:*: b_afK) ->
                 case w_s1LE of _ { GHC.Types.I# ww_s1LG ->
                 case w_s1LI of _ { TestEq.:*: ww_s1LK ww_s1LL ->
                 $wshowsPrec_s1MK
                   @ a_afJ @ b_afK w_s1LC w_s1LD ww_s1LG ww_s1LK ww_s1LL
                 }
                 })

showList_aWW :: forall a_afJ b_afK.
                (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
LclId
[Arity 4
 Str: DmdType LLSL]
showList_aWW =
  \ (@ a_Xuj)
    (@ b_Xul)
    ($dShow_aWq [ALWAYS Just L] :: GHC.Show.Show a_Xuj)
    ($dShow_aWr [ALWAYS Just L] :: GHC.Show.Show b_Xul)
    (eta_B2 :: [a_Xuj TestEq.:*: b_Xul])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(LL)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.:*: ww_s1LK ww_s1LL ->
           $wshowsPrec_s1MK
             @ a_Xuj
             @ b_Xul
             $dShow_aWq
             $dShow_aWr
             0
             ww_s1LK
             ww_s1LL
             (let {
                lvl12_s1NV :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NV =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1NX [ALWAYS LoopBreaker Nothing] :: [a_Xuj TestEq.:*: b_Xul]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NX =
                  \ (ds2_a1gv :: [a_Xuj TestEq.:*: b_Xul]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NV;
                      : y_a1gA [ALWAYS Just D(LL)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.:*: ww_X1Vu ww_X1Vw ->
                           $wshowsPrec_s1MK
                             @ a_Xuj
                             @ b_Xul
                             $dShow_aWq
                             $dShow_aWr
                             0
                             ww_X1Vu
                             ww_X1Vw
                             (showl_s1NX ys_a1gB)
                           })
                    }; } in
              showl_s1NX xs_a1gr)
           })
    }

TestEq.$fShow:*: :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Show.Show (a_afJ TestEq.:*: b_afK)
LclIdX[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:*: =
  __inline_me (\ (@ a_Xui)
                 (@ b_Xuk)
                 ($dShow_aWq [ALWAYS Just L] :: GHC.Show.Show a_Xui)
                 ($dShow_aWr [ALWAYS Just L] :: GHC.Show.Show b_Xuk) ->
                 let {
                   a_s1ni :: [a_Xui TestEq.:*: b_Xuk] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1ni = showList_aWW @ a_Xui @ b_Xuk $dShow_aWq $dShow_aWr } in
                 let {
                   a_s1nj :: GHC.Types.Int
                             -> (a_Xui TestEq.:*: b_Xuk)
                             -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(LL)]
                   a_s1nj = showsPrec_aWK @ a_Xui @ b_Xuk $dShow_aWq $dShow_aWr } in
                 letrec {
                   $dShow_s1ng :: GHC.Show.Show (a_Xui TestEq.:*: b_Xuk)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1ng =
                     GHC.Show.D:Show @ (a_Xui TestEq.:*: b_Xuk) a_s1nj a_s1nh a_s1ni;
                   a_s1nh [ALWAYS LoopBreaker Nothing] :: (a_Xui TestEq.:*: b_Xuk)
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1nh =
                     GHC.Show.$dmshow @ (a_Xui TestEq.:*: b_Xuk) $dShow_s1ng; } in
                 $dShow_s1ng)

readList_aVW :: forall c_afH a_afI.
                (GHC.Read.Read a_afI) =>
                Text.ParserCombinators.ReadP.ReadS [TestEq.C c_afH a_afI]
LclId
[Arity 1
 Str: DmdType L]
readList_aVW =
  \ (@ c_XuC) (@ a_XuE) ($dRead_X1aI :: GHC.Read.Read a_XuE) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.C c_XuC a_XuE]
      (((GHC.Read.$dmreadList2
           @ (TestEq.C c_XuC a_XuE)
           ((a_s1F4 @ c_XuC @ a_XuE $dRead_X1aI)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (TestEq.C c_XuC a_XuE))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_XuC a_XuE))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_XuC a_XuE)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [TestEq.C c_XuC a_XuE]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.C c_XuC a_XuE]
                     ~
                   (forall b_a1As.
                    ([TestEq.C c_XuC a_XuE] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [TestEq.C c_XuC a_XuE]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.C c_XuC a_XuE]))

TestEq.$fReadC :: forall c_afH a_afI.
                  (GHC.Read.Read a_afI) =>
                  GHC.Read.Read (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadC =
  __inline_me (\ (@ c_Xuw)
                 (@ a_Xuy)
                 ($dRead_X1aC [ALWAYS Just L] :: GHC.Read.Read a_Xuy) ->
                 let {
                   a_s1J0 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [TestEq.C c_Xuw a_Xuy]
                   LclId
                   [Str: DmdType]
                   a_s1J0 = a_s1F8 @ c_Xuw @ a_Xuy $dRead_X1aC } in
                 let {
                   a_s1F6 :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuw a_Xuy)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1F6 = a_s1F4 @ c_Xuw @ a_Xuy $dRead_X1aC } in
                 let {
                   a_s1n6 [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.C c_Xuw a_Xuy]
                   LclId
                   [Str: DmdType]
                   a_s1n6 = readList_aVW @ c_Xuw @ a_Xuy $dRead_X1aC } in
                 letrec {
                   $dRead_s1n2 :: GHC.Read.Read (TestEq.C c_Xuw a_Xuy)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1n2 =
                     GHC.Read.D:Read
                       @ (TestEq.C c_Xuw a_Xuy)
                       a_s1n3
                       a_s1n6
                       (a_s1F6
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (TestEq.C c_Xuw a_Xuy))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuw a_Xuy))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xuw a_Xuy)))
                       (a_s1J0
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [TestEq.C c_Xuw a_Xuy])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xuw a_Xuy])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [TestEq.C c_Xuw a_Xuy]));
                   a_s1n3 [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.C c_Xuw a_Xuy)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1n3 =
                     GHC.Read.$dmreadsPrec @ (TestEq.C c_Xuw a_Xuy) $dRead_s1n2; } in
                 $dRead_s1n2)

showsPrec_aVA :: forall c_afH a_afI.
                 (GHC.Show.Show a_afI) =>
                 GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
LclId
[Arity 3
 Worker $wshowsPrec_s1ML
 Str: DmdType LU(L)U(L)]
showsPrec_aVA =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 (w_s1LR :: GHC.Show.Show a_afI)
                 (w_s1LS :: GHC.Types.Int)
                 (w_s1LW :: TestEq.C c_afH a_afI) ->
                 case w_s1LS of _ { GHC.Types.I# ww_s1LU ->
                 case w_s1LW of _ { TestEq.C ww_s1LY ->
                 $wshowsPrec_s1ML @ c_afH @ a_afI w_s1LR ww_s1LU ww_s1LY
                 }
                 })

showList_aVK :: forall c_afH a_afI.
                (GHC.Show.Show a_afI) =>
                [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showList_aVK =
  \ (@ c_XuK)
    (@ a_XuM)
    ($dShow_aVm [ALWAYS Just L] :: GHC.Show.Show a_XuM)
    (eta_B2 :: [TestEq.C c_XuK a_XuM])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(L)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.C ww_s1LY ->
           $wshowsPrec_s1ML
             @ c_XuK
             @ a_XuM
             $dShow_aVm
             0
             ww_s1LY
             (let {
                lvl12_s1NZ :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NZ =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1O1 [ALWAYS LoopBreaker Nothing] :: [TestEq.C c_XuK a_XuM]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1O1 =
                  \ (ds2_a1gv :: [TestEq.C c_XuK a_XuM]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NZ;
                      : y_a1gA [ALWAYS Just D(L)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.C ww_X1VL ->
                           $wshowsPrec_s1ML
                             @ c_XuK @ a_XuM $dShow_aVm 0 ww_X1VL (showl_s1O1 ys_a1gB)
                           })
                    }; } in
              showl_s1O1 xs_a1gr)
           })
    }

TestEq.$fShowC :: forall c_afH a_afI.
                  (GHC.Show.Show a_afI) =>
                  GHC.Show.Show (TestEq.C c_afH a_afI)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowC =
  __inline_me (\ (@ c_XuJ)
                 (@ a_XuL)
                 ($dShow_aVm [ALWAYS Just L] :: GHC.Show.Show a_XuL) ->
                 let {
                   a_s1mW :: [TestEq.C c_XuJ a_XuL] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1mW = showList_aVK @ c_XuJ @ a_XuL $dShow_aVm } in
                 let {
                   a_s1mX :: GHC.Types.Int -> TestEq.C c_XuJ a_XuL -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a_s1mX = showsPrec_aVA @ c_XuJ @ a_XuL $dShow_aVm } in
                 letrec {
                   $dShow_s1mU :: GHC.Show.Show (TestEq.C c_XuJ a_XuL)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1mU =
                     GHC.Show.D:Show @ (TestEq.C c_XuJ a_XuL) a_s1mX a_s1mV a_s1mW;
                   a_s1mV [ALWAYS LoopBreaker Nothing] :: TestEq.C c_XuJ a_XuL
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1mV =
                     GHC.Show.$dmshow @ (TestEq.C c_XuJ a_XuL) $dShow_s1mU; } in
                 $dShow_s1mU)

readList_aUS :: forall a_afG.
                (GHC.Read.Read a_afG) =>
                Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
LclId
[Arity 1
 Str: DmdType L]
readList_aUS =
  \ (@ a_Xv0) ($dRead_X1a2 :: GHC.Read.Read a_Xv0) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Var a_Xv0]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Var a_Xv0)
           ((a_s1Fq @ a_Xv0 $dRead_X1a2)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_Xv0))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_Xv0))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_Xv0)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Var a_Xv0]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_Xv0]
                     ~
                   (forall b_a1As.
                    ([TestEq.Var a_Xv0] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [TestEq.Var a_Xv0]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Var a_Xv0]))

TestEq.$fReadVar :: forall a_afG.
                    (GHC.Read.Read a_afG) =>
                    GHC.Read.Read (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadVar =
  __inline_me (\ (@ a_XuV)
                 ($dRead_X19X [ALWAYS Just L] :: GHC.Read.Read a_XuV) ->
                 let {
                   a_s1J2 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [TestEq.Var a_XuV]
                   LclId
                   [Str: DmdType]
                   a_s1J2 = a_s1Fu @ a_XuV $dRead_X19X } in
                 let {
                   a_s1Fs :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuV)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1Fs = a_s1Fq @ a_XuV $dRead_X19X } in
                 let {
                   a_s1mK [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.Var a_XuV]
                   LclId
                   [Str: DmdType]
                   a_s1mK = readList_aUS @ a_XuV $dRead_X19X } in
                 letrec {
                   $dRead_s1mG :: GHC.Read.Read (TestEq.Var a_XuV)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1mG =
                     GHC.Read.D:Read
                       @ (TestEq.Var a_XuV)
                       a_s1mH
                       a_s1mK
                       (a_s1Fs
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuV))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuV))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuV)))
                       (a_s1J2
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Var a_XuV])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuV])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Var a_XuV]));
                   a_s1mH [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Var a_XuV)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1mH =
                     GHC.Read.$dmreadsPrec @ (TestEq.Var a_XuV) $dRead_s1mG; } in
                 $dRead_s1mG)

showsPrec_aUw :: forall a_afG.
                 (GHC.Show.Show a_afG) =>
                 GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
LclId
[Arity 3
 Worker $wshowsPrec_s1MM
 Str: DmdType LU(L)U(L)]
showsPrec_aUw =
  __inline_me (\ (@ a_afG)
                 (w_s1M4 :: GHC.Show.Show a_afG)
                 (w_s1M5 :: GHC.Types.Int)
                 (w_s1M9 :: TestEq.Var a_afG) ->
                 case w_s1M5 of _ { GHC.Types.I# ww_s1M7 ->
                 case w_s1M9 of _ { TestEq.Var ww_s1Mb ->
                 $wshowsPrec_s1MM @ a_afG w_s1M4 ww_s1M7 ww_s1Mb
                 }
                 })

showList_aUG :: forall a_afG.
                (GHC.Show.Show a_afG) =>
                [TestEq.Var a_afG] -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showList_aUG =
  \ (@ a_Xv8)
    ($dShow_aUi [ALWAYS Just L] :: GHC.Show.Show a_Xv8)
    (eta_B2 :: [TestEq.Var a_Xv8])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(L)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Var ww_s1Mb ->
           $wshowsPrec_s1MM
             @ a_Xv8
             $dShow_aUi
             0
             ww_s1Mb
             (let {
                lvl12_s1O3 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1O3 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1O5 [ALWAYS LoopBreaker Nothing] :: [TestEq.Var a_Xv8]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1O5 =
                  \ (ds2_a1gv :: [TestEq.Var a_Xv8]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1O3;
                      : y_a1gA [ALWAYS Just D(L)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Var ww_X1W2 ->
                           $wshowsPrec_s1MM @ a_Xv8 $dShow_aUi 0 ww_X1W2 (showl_s1O5 ys_a1gB)
                           })
                    }; } in
              showl_s1O5 xs_a1gr)
           })
    }

TestEq.$fShowVar :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Show.Show (TestEq.Var a_afG)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowVar =
  __inline_me (\ (@ a_Xv7)
                 ($dShow_aUi [ALWAYS Just L] :: GHC.Show.Show a_Xv7) ->
                 let {
                   a_s1mA :: [TestEq.Var a_Xv7] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1mA = showList_aUG @ a_Xv7 $dShow_aUi } in
                 let {
                   a_s1mB :: GHC.Types.Int -> TestEq.Var a_Xv7 -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a_s1mB = showsPrec_aUw @ a_Xv7 $dShow_aUi } in
                 letrec {
                   $dShow_s1my :: GHC.Show.Show (TestEq.Var a_Xv7)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1my =
                     GHC.Show.D:Show @ (TestEq.Var a_Xv7) a_s1mB a_s1mz a_s1mA;
                   a_s1mz [ALWAYS LoopBreaker Nothing] :: TestEq.Var a_Xv7
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1mz = GHC.Show.$dmshow @ (TestEq.Var a_Xv7) $dShow_s1my; } in
                 $dShow_s1my)

readList_aTO :: forall a_afF.
                (GHC.Read.Read a_afF) =>
                Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
LclId
[Arity 1
 Str: DmdType L]
readList_aTO =
  \ (@ a_Xvo) ($dRead_X19n :: GHC.Read.Read a_Xvo) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Rec a_Xvo]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Rec a_Xvo)
           ((a_s1FM @ a_Xvo $dRead_X19n)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvo))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvo))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvo)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Rec a_Xvo]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xvo]
                     ~
                   (forall b_a1As.
                    ([TestEq.Rec a_Xvo] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [TestEq.Rec a_Xvo]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Rec a_Xvo]))

TestEq.$fReadRec :: forall a_afF.
                    (GHC.Read.Read a_afF) =>
                    GHC.Read.Read (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadRec =
  __inline_me (\ (@ a_Xvj)
                 ($dRead_X19i [ALWAYS Just L] :: GHC.Read.Read a_Xvj) ->
                 let {
                   a_s1J4 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                             -> Text.ParserCombinators.ReadP.ReadP
                                                  [TestEq.Rec a_Xvj]
                   LclId
                   [Str: DmdType]
                   a_s1J4 = a_s1FQ @ a_Xvj $dRead_X19i } in
                 let {
                   a_s1FO :: Text.ParserCombinators.ReadPrec.Prec
                             -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvj)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1FO = a_s1FM @ a_Xvj $dRead_X19i } in
                 let {
                   a_s1mo [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                               [TestEq.Rec a_Xvj]
                   LclId
                   [Str: DmdType]
                   a_s1mo = readList_aTO @ a_Xvj $dRead_X19i } in
                 letrec {
                   $dRead_s1mk :: GHC.Read.Read (TestEq.Rec a_Xvj)
                   LclId
                   [Str: DmdType m]
                   $dRead_s1mk =
                     GHC.Read.D:Read
                       @ (TestEq.Rec a_Xvj)
                       a_s1ml
                       a_s1mo
                       (a_s1FO
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvj))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvj))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvj)))
                       (a_s1J4
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Rec a_Xvj])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xvj])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Rec a_Xvj]));
                   a_s1ml [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                                          -> Text.ParserCombinators.ReadP.ReadS
                                                               (TestEq.Rec a_Xvj)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a_s1ml =
                     GHC.Read.$dmreadsPrec @ (TestEq.Rec a_Xvj) $dRead_s1mk; } in
                 $dRead_s1mk)

showsPrec_aTs :: forall a_afF.
                 (GHC.Show.Show a_afF) =>
                 GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
LclId
[Arity 3
 Worker $wshowsPrec_s1MN
 Str: DmdType LU(L)U(L)]
showsPrec_aTs =
  __inline_me (\ (@ a_afF)
                 (w_s1Mh :: GHC.Show.Show a_afF)
                 (w_s1Mi :: GHC.Types.Int)
                 (w_s1Mm :: TestEq.Rec a_afF) ->
                 case w_s1Mi of _ { GHC.Types.I# ww_s1Mk ->
                 case w_s1Mm of _ { TestEq.Rec ww_s1Mo ->
                 $wshowsPrec_s1MN @ a_afF w_s1Mh ww_s1Mk ww_s1Mo
                 }
                 })

showList_aTC :: forall a_afF.
                (GHC.Show.Show a_afF) =>
                [TestEq.Rec a_afF] -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showList_aTC =
  \ (@ a_Xvw)
    ($dShow_aTe [ALWAYS Just L] :: GHC.Show.Show a_Xvw)
    (eta_B2 :: [TestEq.Rec a_Xvw])
    (eta_B1 [ALWAYS Just L] :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta_B1;
      : x_a1gq [ALWAYS Just D(L)] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Rec ww_s1Mo ->
           $wshowsPrec_s1MN
             @ a_Xvw
             $dShow_aTe
             0
             ww_s1Mo
             (let {
                lvl12_s1O7 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1O7 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta_B1 } in
              letrec {
                showl_s1O9 [ALWAYS LoopBreaker Nothing] :: [TestEq.Rec a_Xvw]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1O9 =
                  \ (ds2_a1gv :: [TestEq.Rec a_Xvw]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1O7;
                      : y_a1gA [ALWAYS Just D(L)] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Rec ww_X1Wk ->
                           $wshowsPrec_s1MN @ a_Xvw $dShow_aTe 0 ww_X1Wk (showl_s1O9 ys_a1gB)
                           })
                    }; } in
              showl_s1O9 xs_a1gr)
           })
    }

TestEq.$fShowRec :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Show.Show (TestEq.Rec a_afF)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowRec =
  __inline_me (\ (@ a_Xvv)
                 ($dShow_aTe [ALWAYS Just L] :: GHC.Show.Show a_Xvv) ->
                 let {
                   a_s1me :: [TestEq.Rec a_Xvv] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a_s1me = showList_aTC @ a_Xvv $dShow_aTe } in
                 let {
                   a_s1mf :: GHC.Types.Int -> TestEq.Rec a_Xvv -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a_s1mf = showsPrec_aTs @ a_Xvv $dShow_aTe } in
                 letrec {
                   $dShow_s1mc :: GHC.Show.Show (TestEq.Rec a_Xvv)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1mc =
                     GHC.Show.D:Show @ (TestEq.Rec a_Xvv) a_s1mf a_s1md a_s1me;
                   a_s1md [ALWAYS LoopBreaker Nothing] :: TestEq.Rec a_Xvv
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1md = GHC.Show.$dmshow @ (TestEq.Rec a_Xvv) $dShow_s1mc; } in
                 $dShow_s1mc)

readListDefault_aRx :: Text.ParserCombinators.ReadP.ReadS
                         [TestEq.Fixity]
LclId
[Arity 1
 Str: DmdType L]
readListDefault_aRx =
  Text.ParserCombinators.ReadP.run @ [TestEq.Fixity] a_s1Hd

TestEq.$fReadFixity :: GHC.Read.Read TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fReadFixity =
  GHC.Read.D:Read
    @ TestEq.Fixity
    a_s1iR
    readListDefault_aRx
    (a_s1H9
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (a_s1Hb
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Fixity]))

compare_aRe :: TestEq.Fixity
               -> TestEq.Fixity
               -> GHC.Ordering.Ordering
LclId
[Arity 2
 Str: DmdType SS]
compare_aRe =
  \ (a_axy :: TestEq.Fixity) (b_axz :: TestEq.Fixity) ->
    let {
      $j_s1Od :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      LclId
      [Arity 1
       Str: DmdType L]
      $j_s1Od =
        \ (a#_axF [ALWAYS Just L OneShot] :: GHC.Prim.Int#) ->
          let {
            $j_s1Ob :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            LclId
            [Arity 1
             Str: DmdType L]
            $j_s1Ob =
              \ (b#_axG [ALWAYS Just L OneShot] :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_axF b#_axG of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_axF b#_axG of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a_axy of _ {
                      TestEq.Prefix -> GHC.Ordering.EQ;
                      TestEq.Infix a1_axB a2_axC ->
                        case b_axz of _ {
                          TestEq.Prefix -> GHC.Ordering.EQ;
                          TestEq.Infix b1_axD [ALWAYS Just S] b2_axE ->
                            case a1_axB of _ {
                              TestEq.LeftAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.RightAssociative -> GHC.Ordering.LT;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.RightAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Ordering.GT;
                                  TestEq.RightAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.NotAssociative ->
                                case b1_axD of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  TestEq.NotAssociative -> GHC.Base.compareInt a2_axC b2_axE
                                }
                            }
                        }
                    }
                } } in
          case b_axz of _ {
            TestEq.Prefix -> $j_s1Ob 0; TestEq.Infix _ _ -> $j_s1Ob 1
          } } in
    case a_axy of _ {
      TestEq.Prefix -> $j_s1Od 0; TestEq.Infix _ _ -> $j_s1Od 1
    }

a_s1lc :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1lc =
  \ (x_a1j9 :: TestEq.Fixity) (y_a1ja :: TestEq.Fixity) ->
    case compare_aRe x_a1j9 y_a1ja of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

a_s1le :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1le =
  \ (x_a1jq :: TestEq.Fixity) (y_a1jr :: TestEq.Fixity) ->
    case compare_aRe x_a1jq y_a1jr of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

a_s1lg :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1lg =
  \ (x_a1jH :: TestEq.Fixity) (y_a1jI :: TestEq.Fixity) ->
    case compare_aRe x_a1jH y_a1jI of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

a_s1li :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
a_s1li =
  \ (x_a1jY :: TestEq.Fixity) (y_a1jZ :: TestEq.Fixity) ->
    case compare_aRe x_a1jY y_a1jZ of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

a_s1lk :: TestEq.Fixity -> TestEq.Fixity -> TestEq.Fixity
LclId
[Arity 2
 Str: DmdType SS]
a_s1lk =
  \ (x_a1kf :: TestEq.Fixity) (y_a1kg :: TestEq.Fixity) ->
    case compare_aRe x_a1kf y_a1kg of _ {
      __DEFAULT -> y_a1kg; GHC.Ordering.GT -> x_a1kf
    }

a_s1lm :: TestEq.Fixity -> TestEq.Fixity -> TestEq.Fixity
LclId
[Arity 2
 Str: DmdType SS]
a_s1lm =
  \ (x_a1kz :: TestEq.Fixity) (y_a1kA :: TestEq.Fixity) ->
    case compare_aRe x_a1kz y_a1kA of _ {
      __DEFAULT -> x_a1kz; GHC.Ordering.GT -> y_a1kA
    }

showsPrec_aQK :: GHC.Types.Int -> TestEq.Fixity -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType LSL]
showsPrec_aQK =
  \ (ds_d1ba :: GHC.Types.Int)
    (ds_d1bb :: TestEq.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds_d1bb of _ {
      TestEq.Prefix -> GHC.Base.++ @ GHC.Types.Char lvl_s1xe eta_B1;
      TestEq.Infix b1_axw [ALWAYS Just L] b2_axx [ALWAYS Just D(L)] ->
        case ds_d1ba of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        let {
          p_s1Of :: GHC.Show.ShowS
          LclId
          [Arity 1
           Str: DmdType L]
          p_s1Of =
            \ (x_X1z8 :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xh
                (case b1_axw of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       lvl_s1x7
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1HD [ALWAYS Just L] ->
                           GHC.Show.$wshowSignedInt 11 ww_a1HD x_X1z8
                           }));
                   TestEq.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       lvl_s1x9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1HD [ALWAYS Just L] ->
                           GHC.Show.$wshowSignedInt 11 ww_a1HD x_X1z8
                           }));
                   TestEq.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       lvl_s1xb
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1HD [ALWAYS Just L] ->
                           GHC.Show.$wshowSignedInt 11 ww_a1HD x_X1z8
                           }))
                 }) } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False -> p_s1Of eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.showSignedInt2
              (p_s1Of
                 (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 eta_B1))
        }
        }
    }

showList_aQS :: [TestEq.Fixity] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aQS =
  \ (ds1_a1gk :: [TestEq.Fixity])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (showsPrec_aQK
             a_s1IG
             x_a1gq
             (let {
                lvl12_s1Oh :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1Oh =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1Oj [ALWAYS LoopBreaker Nothing] :: [TestEq.Fixity]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1Oj =
                  \ (ds2_a1gv :: [TestEq.Fixity]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1Oh;
                      : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (showsPrec_aQK a_s1IG y_a1gA (showl_s1Oj ys_a1gB))
                    }; } in
              showl_s1Oj xs_a1gr))
    }

a_s1j0 :: TestEq.Fixity -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType S]
a_s1j0 =
  \ (x_X1xU :: TestEq.Fixity) ->
    showsPrec_aQK
      GHC.Base.zeroInt x_X1xU (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowFixity :: GHC.Show.Show TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowFixity =
  GHC.Show.D:Show @ TestEq.Fixity showsPrec_aQK a_s1j0 showList_aQS

==_aQi :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
==_aQi =
  \ (ds_d1b4 :: TestEq.Fixity) (ds_d1b5 :: TestEq.Fixity) ->
    case ds_d1b4 of _ {
      TestEq.Prefix ->
        case ds_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.True; TestEq.Infix _ _ -> GHC.Bool.False
        };
      TestEq.Infix a1_axl a2_axm ->
        case ds_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.False;
          TestEq.Infix b1_axn [ALWAYS Just S] b2_axo ->
            case a1_axl of _ {
              TestEq.LeftAssociative ->
                case b1_axn of _ {
                  TestEq.LeftAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1yv [ALWAYS Just L] ->
                    case b2_axo of _ { GHC.Types.I# y_a1yz [ALWAYS Just L] ->
                    GHC.Prim.==# x_a1yv y_a1yz
                    }
                    };
                  TestEq.RightAssociative -> GHC.Bool.False;
                  TestEq.NotAssociative -> GHC.Bool.False
                };
              TestEq.RightAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.RightAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1yv [ALWAYS Just L] ->
                    case b2_axo of _ { GHC.Types.I# y_a1yz [ALWAYS Just L] ->
                    GHC.Prim.==# x_a1yv y_a1yz
                    }
                    }
                };
              TestEq.NotAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.NotAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1yv [ALWAYS Just L] ->
                    case b2_axo of _ { GHC.Types.I# y_a1yz [ALWAYS Just L] ->
                    GHC.Prim.==# x_a1yv y_a1yz
                    }
                    }
                }
            }
        }
    }

/=_aQp :: TestEq.Fixity -> TestEq.Fixity -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
/=_aQp =
  \ (a_axt :: TestEq.Fixity) (b_axu :: TestEq.Fixity) ->
    case ==_aQi a_axt b_axu of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqFixity :: GHC.Classes.Eq TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fEqFixity = GHC.Classes.D:Eq @ TestEq.Fixity ==_aQi /=_aQp

TestEq.$fOrdFixity :: GHC.Classes.Ord TestEq.Fixity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fOrdFixity =
  GHC.Classes.D:Ord
    @ TestEq.Fixity
    TestEq.$fEqFixity
    compare_aRe
    a_s1lc
    a_s1le
    a_s1lg
    a_s1li
    a_s1lk
    a_s1lm

readListDefault_aPz :: Text.ParserCombinators.ReadP.ReadS
                         [TestEq.Associativity]
LclId
[Arity 1
 Str: DmdType L]
readListDefault_aPz =
  Text.ParserCombinators.ReadP.run @ [TestEq.Associativity] a_s1GF

TestEq.$fReadAssociativity :: GHC.Read.Read TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fReadAssociativity =
  GHC.Read.D:Read
    @ TestEq.Associativity
    a_s1iF
    readListDefault_aPz
    (a_s1GB
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
    (a_s1GD
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [TestEq.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Associativity]))

compare_aK1 :: TestEq.Associativity
               -> TestEq.Associativity
               -> GHC.Ordering.Ordering
LclId
[Arity 2
 Str: DmdType SS]
compare_aK1 =
  \ (a_axg :: TestEq.Associativity)
    (b_axh :: TestEq.Associativity) ->
    case a_axg of _ {
      TestEq.LeftAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.EQ;
          TestEq.RightAssociative -> GHC.Ordering.LT;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.RightAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.GT;
          TestEq.RightAssociative -> GHC.Ordering.EQ;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.NotAssociative ->
        case b_axh of _ {
          __DEFAULT -> GHC.Ordering.GT;
          TestEq.NotAssociative -> GHC.Ordering.EQ
        }
    }

showList_aJx :: [TestEq.Associativity] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aJx =
  \ (ds1_a1gk :: [TestEq.Associativity])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1Op [ALWAYS Just L] :: GHC.Base.String
             LclId
             [Str: DmdType]
             eta_s1Op =
               let {
                 lvl12_s1Ol :: [GHC.Types.Char]
                 LclId
                 [Str: DmdType]
                 lvl12_s1Ol =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
               letrec {
                 showl_s1On [ALWAYS LoopBreaker Nothing] :: [TestEq.Associativity]
                                                            -> [GHC.Types.Char]
                 LclId
                 [Arity 1
                  Str: DmdType S]
                 showl_s1On =
                   \ (ds2_a1gv :: [TestEq.Associativity]) ->
                     case ds2_a1gv of _ {
                       [] -> lvl12_s1Ol;
                       : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_a1gA of _ {
                              TestEq.LeftAssociative ->
                                GHC.Base.++ @ GHC.Types.Char lvl_s1x7 (showl_s1On ys_a1gB);
                              TestEq.RightAssociative ->
                                GHC.Base.++ @ GHC.Types.Char lvl_s1x9 (showl_s1On ys_a1gB);
                              TestEq.NotAssociative ->
                                GHC.Base.++ @ GHC.Types.Char lvl_s1xb (showl_s1On ys_a1gB)
                            })
                     }; } in
               showl_s1On xs_a1gr } in
           case x_a1gq of _ {
             TestEq.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char lvl_s1x7 eta_s1Op;
             TestEq.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char lvl_s1x9 eta_s1Op;
             TestEq.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char lvl_s1xb eta_s1Op
           })
    }

showsPrec_aJp :: GHC.Types.Int
                 -> TestEq.Associativity
                 -> GHC.Show.ShowS
LclId
[Arity 3
 Str: DmdType ASL]
showsPrec_aJp =
  __inline_me (\ _
                 (ds_d1aU :: TestEq.Associativity)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds_d1aU of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++ @ GHC.Types.Char lvl_s1x7 eta_B1;
                   TestEq.RightAssociative ->
                     GHC.Base.++ @ GHC.Types.Char lvl_s1x9 eta_B1;
                   TestEq.NotAssociative ->
                     GHC.Base.++ @ GHC.Types.Char lvl_s1xb eta_B1
                 })

TestEq.$fShowAssociativity :: GHC.Show.Show TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowAssociativity =
  GHC.Show.D:Show
    @ TestEq.Associativity showsPrec_aJp a_s1iV showList_aJx

/=_aJk :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
/=_aJk =
  \ (a_axe :: TestEq.Associativity)
    (b_axf :: TestEq.Associativity) ->
    case a_axe of _ {
      TestEq.LeftAssociative ->
        case b_axf of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

==_aJd :: TestEq.Associativity
          -> TestEq.Associativity
          -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
==_aJd =
  \ (a_axa :: TestEq.Associativity)
    (b_axb :: TestEq.Associativity) ->
    case a_axa of _ {
      TestEq.LeftAssociative ->
        case b_axb of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

TestEq.$fEqAssociativity :: GHC.Classes.Eq TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fEqAssociativity =
  GHC.Classes.D:Eq @ TestEq.Associativity ==_aJd /=_aJk

TestEq.$fOrdAssociativity :: GHC.Classes.Ord TestEq.Associativity
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ TestEq.Associativity
    TestEq.$fEqAssociativity
    compare_aK1
    a_s1kS
    a_s1kU
    a_s1kW
    a_s1kY
    a_s1l0
    a_s1l2

Rec {
==_aIY [ALWAYS LoopBreaker Nothing] :: TestEq.Logic
                                       -> TestEq.Logic
                                       -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
==_aIY =
  \ (ds_d1aD :: TestEq.Logic) (ds_d1aE :: TestEq.Logic) ->
    let {
      $wfail_s1Or :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1
       Str: DmdType A]
      $wfail_s1Or =
        \ _ ->
          case ds_d1aD of _ {
            TestEq.VarL _ ->
              case ds_d1aE of _ {
                TestEq.VarL _ -> GHC.Bool.True;
                TestEq.T -> GHC.Bool.False;
                TestEq.F -> GHC.Bool.False;
                TestEq.Not _ -> GHC.Bool.False;
                TestEq.Impl _ _ -> GHC.Bool.False;
                TestEq.Equiv _ _ -> GHC.Bool.False;
                TestEq.Conj _ _ -> GHC.Bool.False;
                TestEq.Disj _ _ -> GHC.Bool.False
              };
            TestEq.T ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.T -> GHC.Bool.True
              };
            TestEq.F ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.F -> GHC.Bool.True
              };
            TestEq.Not _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Not _ -> GHC.Bool.True
              };
            TestEq.Impl _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Impl _ _ -> GHC.Bool.True
              };
            TestEq.Equiv _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Equiv _ _ -> GHC.Bool.True
              };
            TestEq.Conj _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Conj _ _ -> GHC.Bool.True
              };
            TestEq.Disj _ _ ->
              case ds_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Disj _ _ -> GHC.Bool.True
              }
          } } in
    case ds_d1aD of _ {
      __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
      TestEq.VarL a1_awK ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.VarL b1_awL [ALWAYS Just S] ->
            GHC.Base.eqString a1_awK b1_awL
        };
      TestEq.Not a1_awM ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Not b1_awN [ALWAYS Just S] -> ==_aIY a1_awM b1_awN
        };
      TestEq.Impl a1_awO a2_awP ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Impl b1_awQ [ALWAYS Just S] b2_awR ->
            case ==_aIY a1_awO b1_awQ of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_awP b2_awR
            }
        };
      TestEq.Equiv a1_awS a2_awT ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Equiv b1_awU [ALWAYS Just S] b2_awV ->
            case ==_aIY a1_awS b1_awU of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_awT b2_awV
            }
        };
      TestEq.Conj a1_awW a2_awX ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Conj b1_awY [ALWAYS Just S] b2_awZ ->
            case ==_aIY a1_awW b1_awY of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_awX b2_awZ
            }
        };
      TestEq.Disj a1_ax0 a2_ax1 ->
        case ds_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Disj b1_ax2 [ALWAYS Just S] b2_ax3 ->
            case ==_aIY a1_ax0 b1_ax2 of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> ==_aIY a2_ax1 b2_ax3
            }
        }
    }
end Rec }

/=_aJ6 :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
LclId
[Arity 2
 Str: DmdType SS]
/=_aJ6 =
  \ (a_ax8 :: TestEq.Logic) (b_ax9 :: TestEq.Logic) ->
    case ==_aIY a_ax8 b_ax9 of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqLogic0 :: GHC.Classes.Eq TestEq.Logic
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fEqLogic0 = GHC.Classes.D:Eq @ TestEq.Logic ==_aIY /=_aJ6

Rec {
$sshowsPrec_s1RT :: TestEq.Logic
                    -> GHC.Prim.Int#
                    -> GHC.Base.String
                    -> [GHC.Types.Char]
LclId
[Arity 2
 Str: DmdType SL]
$sshowsPrec_s1RT =
  \ (sc_s1QY :: TestEq.Logic) (sc_s1QZ :: GHC.Prim.Int#) ->
    case sc_s1QY of _ {
      TestEq.VarL b1_awv [ALWAYS Just L] ->
        case GHC.Prim.>=# sc_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xp
                (GHC.Show.$fShowChar_showList b1_awv x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xp
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
        };
      TestEq.T -> lvl_s1xr;
      TestEq.F -> lvl_s1xu;
      TestEq.Not b1_awx [ALWAYS Just L] ->
        let {
          g_s1Ot [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ot = showsPrec_aIk lvl_s1P3 b1_awx } in
        case GHC.Prim.>=# sc_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1xz (g_s1Ot x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xz
                   (g_s1Ot
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.Impl b1_awz [ALWAYS Just L] b2_awA [ALWAYS Just L] ->
        let {
          g_s1Ox [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ox = showsPrec_aIk lvl_s1P3 b2_awA } in
        let {
          f_s1Ov [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Ov = showsPrec_aIk lvl_s1P3 b1_awz } in
        case GHC.Prim.>=# sc_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xD
                (f_s1Ov
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Ox x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xD
                   (f_s1Ov
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Ox
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Equiv b1_awC [ALWAYS Just L] b2_awD [ALWAYS Just L] ->
        let {
          g_s1OB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OB = showsPrec_aIk lvl_s1P3 b2_awD } in
        let {
          f_s1Oz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Oz = showsPrec_aIk lvl_s1P3 b1_awC } in
        case GHC.Prim.>=# sc_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xI
                (f_s1Oz
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OB x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xI
                   (f_s1Oz
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OB
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Conj b1_awF [ALWAYS Just L] b2_awG [ALWAYS Just L] ->
        let {
          g_s1OF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OF = showsPrec_aIk lvl_s1P3 b2_awG } in
        let {
          f_s1OD [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OD = showsPrec_aIk lvl_s1P3 b1_awF } in
        case GHC.Prim.>=# sc_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xN
                (f_s1OD
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OF x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xN
                   (f_s1OD
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OF
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Disj b1_awI [ALWAYS Just L] b2_awJ [ALWAYS Just L] ->
        let {
          g_s1OJ [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OJ = showsPrec_aIk lvl_s1P3 b2_awJ } in
        let {
          f_s1OH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OH = showsPrec_aIk lvl_s1P3 b1_awI } in
        case GHC.Prim.>=# sc_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xS
                (f_s1OH
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OJ x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xS
                   (f_s1OH
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OJ
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
    }
showsPrec_aIk [ALWAYS LoopBreaker Nothing] :: GHC.Types.Int
                                              -> TestEq.Logic
                                              -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType LS
 RULES: "SC:showsPrec0" [0]
            forall {sc_s1QY :: TestEq.Logic sc_s1QZ :: GHC.Prim.Int#}
              showsPrec_aIk (GHC.Types.I# sc_s1QZ) sc_s1QY
              = $sshowsPrec_s1RT sc_s1QY sc_s1QZ]
showsPrec_aIk =
  \ (a_awu :: GHC.Types.Int) (ds_d1au :: TestEq.Logic) ->
    case ds_d1au of _ {
      TestEq.VarL b1_awv [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xp
                (GHC.Show.$fShowChar_showList b1_awv x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xp
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
        }
        };
      TestEq.T -> lvl_s1xr;
      TestEq.F -> lvl_s1xu;
      TestEq.Not b1_awx [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        let {
          g_s1Ot [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ot = showsPrec_aIk lvl_s1P3 b1_awx } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl_s1xz (g_s1Ot x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xz
                   (g_s1Ot
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        }
        };
      TestEq.Impl b1_awz [ALWAYS Just L] b2_awA [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        let {
          g_s1Ox [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ox = showsPrec_aIk lvl_s1P3 b2_awA } in
        let {
          f_s1Ov [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Ov = showsPrec_aIk lvl_s1P3 b1_awz } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xD
                (f_s1Ov
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Ox x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xD
                   (f_s1Ov
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Ox
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        };
      TestEq.Equiv b1_awC [ALWAYS Just L] b2_awD [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        let {
          g_s1OB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OB = showsPrec_aIk lvl_s1P3 b2_awD } in
        let {
          f_s1Oz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Oz = showsPrec_aIk lvl_s1P3 b1_awC } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xI
                (f_s1Oz
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OB x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xI
                   (f_s1Oz
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OB
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        };
      TestEq.Conj b1_awF [ALWAYS Just L] b2_awG [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        let {
          g_s1OF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OF = showsPrec_aIk lvl_s1P3 b2_awG } in
        let {
          f_s1OD [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OD = showsPrec_aIk lvl_s1P3 b1_awF } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xN
                (f_s1OD
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OF x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xN
                   (f_s1OD
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OF
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        };
      TestEq.Disj b1_awI [ALWAYS Just L] b2_awJ [ALWAYS Just L] ->
        case a_awu of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
        let {
          g_s1OJ [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OJ = showsPrec_aIk lvl_s1P3 b2_awJ } in
        let {
          f_s1OH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OH = showsPrec_aIk lvl_s1P3 b1_awI } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_s1xS
                (f_s1OH
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OJ x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl_s1xS
                   (f_s1OH
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OJ
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
        }
    }
end Rec }

showList_aIt :: [TestEq.Logic] -> GHC.Show.ShowS
LclId
[Arity 2
 Str: DmdType SL]
showList_aIt =
  \ (ds1_a1gk :: [TestEq.Logic])
    (s_a1gl [ALWAYS Just L] :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq [ALWAYS Just L] xs_a1gr [ALWAYS Just L] ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (showsPrec_aIk
             a_s1IG
             x_a1gq
             (let {
                lvl12_s1OL :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1OL =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1ON [ALWAYS LoopBreaker Nothing] :: [TestEq.Logic]
                                                           -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1ON =
                  \ (ds2_a1gv :: [TestEq.Logic]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1OL;
                      : y_a1gA [ALWAYS Just L] ys_a1gB [ALWAYS Just L] ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (showsPrec_aIk a_s1IG y_a1gA (showl_s1ON ys_a1gB))
                    }; } in
              showl_s1ON xs_a1gr))
    }

a_s1ly :: TestEq.Logic -> GHC.Base.String
LclId
[Arity 1
 Str: DmdType S]
a_s1ly =
  \ (x_X1z6 :: TestEq.Logic) ->
    showsPrec_aIk
      GHC.Base.zeroInt x_X1z6 (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowLogic :: GHC.Show.Show TestEq.Logic
LclIdX[DFunId]
[Str: DmdType m]
TestEq.$fShowLogic =
  GHC.Show.D:Show @ TestEq.Logic showsPrec_aIk a_s1ly showList_aIt

Rec {
==_aH2 [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                       (GHC.Classes.Eq a_afw) =>
                                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType L]
==_aH2 =
  \ (@ a_afw) ($dEq_aGJ [ALWAYS Just L] :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1OP :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1OP = ==_aH2 @ a_afw $dEq_aGJ } in
    \ (ds_d1ao :: TestEq.Tree a_afw) (ds_d1ap :: TestEq.Tree a_afw) ->
      case ds_d1ao of _ {
        TestEq.Leaf ->
          case ds_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
          };
        TestEq.Bin a1_awi a2_awj a3_awk ->
          case ds_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.False;
            TestEq.Bin b1_awl [ALWAYS Just L] b2_awm b3_awn ->
              case $dEq_aGJ
              of _ { GHC.Classes.D:Eq tpl_Xjj [ALWAYS Just C(C(S))] _ ->
              case tpl_Xjj a1_awi b1_awl of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True ->
                  case ==_s1OP a2_awj b2_awm of _ {
                    GHC.Bool.False -> GHC.Bool.False;
                    GHC.Bool.True -> ==_s1OP a3_awk b3_awn
                  }
              }
              }
          }
      }
end Rec }

/=_aHb :: forall a_afw.
          (GHC.Classes.Eq a_afw) =>
          TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
LclId
[Arity 1
 Str: DmdType L]
/=_aHb =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1OR [ALWAYS Just L] :: TestEq.Tree a_afw
                                 -> TestEq.Tree a_afw
                                 -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1OR = ==_aH2 @ a_afw $dEq_aGJ } in
    \ (a_aws :: TestEq.Tree a_afw) (b_awt :: TestEq.Tree a_afw) ->
      case ==_s1OR a_aws b_awt of _ {
        GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
      }

TestEq.$fEqTree0 :: forall a_afw.
                    (GHC.Classes.Eq a_afw) =>
                    GHC.Classes.Eq (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fEqTree0 =
  __inline_me (\ (@ a_afw)
                 ($dEq_aGJ [ALWAYS Just L] :: GHC.Classes.Eq a_afw) ->
                 GHC.Classes.D:Eq
                   @ (TestEq.Tree a_afw)
                   (==_aH2 @ a_afw $dEq_aGJ)
                   (/=_aHb @ a_afw $dEq_aGJ))

Rec {
showsPrec_aGw [ALWAYS LoopBreaker Nothing] :: forall a_afw.
                                              (GHC.Show.Show a_afw) =>
                                              GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
showsPrec_aGw =
  \ (@ a_afw) ($dShow_aFW [ALWAYS Just L] :: GHC.Show.Show a_afw) ->
    let {
      a_s1OT :: GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
      LclId
      [Str: DmdType]
      a_s1OT = showsPrec_aGw @ a_afw $dShow_aFW } in
    \ (ds_d1ak :: GHC.Types.Int) (ds_d1al :: TestEq.Tree a_afw) ->
      case ds_d1al of _ {
        TestEq.Leaf -> lvl_s1xW;
        TestEq.Bin b1_awf [ALWAYS Just L]
                   b2_awg [ALWAYS Just L]
                   b3_awh [ALWAYS Just L] ->
          case ds_d1ak of _ { GHC.Types.I# x_a1Ea [ALWAYS Just L] ->
          let {
            g_s1OZ [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            g_s1OZ = a_s1OT lvl_s1P3 b3_awh } in
          let {
            f_s1OX [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f_s1OX = a_s1OT lvl_s1P3 b2_awg } in
          let {
            f_s1OV [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f_s1OV =
              case $dShow_aFW
              of _ { GHC.Show.D:Show tpl_XjE [ALWAYS Just C(C(S))] _ _ ->
              tpl_XjE lvl_s1P3 b1_awf
              } } in
          let {
            p_s1P1 :: GHC.Show.ShowS
            LclId
            [Arity 1
             Str: DmdType L]
            p_s1P1 =
              \ (x_X1B8 :: GHC.Base.String) ->
                GHC.Base.++
                  @ GHC.Types.Char
                  lvl_s1y0
                  (f_s1OV
                     (GHC.Types.:
                        @ GHC.Types.Char
                        GHC.Show.showSpace1
                        (f_s1OX
                           (GHC.Types.:
                              @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OZ x_X1B8))))) } in
          case GHC.Prim.>=# x_a1Ea 11 of _ {
            GHC.Bool.False -> p_s1P1;
            GHC.Bool.True ->
              \ (x_a1hb :: GHC.Base.String) ->
                GHC.Types.:
                  @ GHC.Types.Char
                  GHC.Show.showSignedInt2
                  (p_s1P1
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))
          }
          }
      }
end Rec }

showList_aGI :: forall a_afw.
                (GHC.Show.Show a_afw) =>
                [TestEq.Tree a_afw] -> GHC.Show.ShowS
LclId
[Arity 1
 Str: DmdType L]
showList_aGI =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    GHC.Show.showList__
      @ (TestEq.Tree a_afw) (showsPrec_aGw @ a_afw $dShow_aFW a_s1IG)

TestEq.$fShowTree :: forall a_afw.
                     (GHC.Show.Show a_afw) =>
                     GHC.Show.Show (TestEq.Tree a_afw)
LclIdX[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowTree =
  __inline_me (\ (@ a_afw)
                 ($dShow_aFW [ALWAYS Just L] :: GHC.Show.Show a_afw) ->
                 let {
                   a_s1lR [ALWAYS Just L] :: [TestEq.Tree a_afw] -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a_s1lR = showList_aGI @ a_afw $dShow_aFW } in
                 let {
                   a_s1lS [ALWAYS Just L] :: GHC.Types.Int
                                             -> TestEq.Tree a_afw
                                             -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a_s1lS = showsPrec_aGw @ a_afw $dShow_aFW } in
                 letrec {
                   $dShow_s1lP :: GHC.Show.Show (TestEq.Tree a_afw)
                   LclId
                   [Str: DmdType m]
                   $dShow_s1lP =
                     GHC.Show.D:Show @ (TestEq.Tree a_afw) a_s1lS a_s1lQ a_s1lR;
                   a_s1lQ [ALWAYS LoopBreaker Nothing] :: TestEq.Tree a_afw
                                                          -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a_s1lQ = GHC.Show.$dmshow @ (TestEq.Tree a_afw) $dShow_s1lP; } in
                 $dShow_s1lP)




==================== Tidy Core ====================
TestEq.eq' :: forall a_afv.
              (TestEq.Eq a_afv) =>
              a_afv -> a_afv -> GHC.Bool.Bool
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.eq' =
  \ (@ a_afv) (tpl_B1 [ALWAYS Once Nothing] :: TestEq.Eq a_afv) ->
    tpl_B1
    `cast` (TestEq.NTCo:T:Eq a_afv
            :: TestEq.T:Eq a_afv ~ (a_afv -> a_afv -> GHC.Bool.Bool))

TestEq.to :: forall a_afx.
             (TestEq.Representable a_afx) =>
             TestEq.Rep a_afx -> a_afx
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(SA)]
TestEq.to =
  \ (@ a_afx)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Representable a_afx) ->
    case tpl_B1
    of _ { TestEq.D:Representable tpl_B2 [ALWAYS Once Nothing] _ ->
    tpl_B2
    }

TestEq.from :: forall a_afx.
               (TestEq.Representable a_afx) =>
               a_afx -> TestEq.Rep a_afx
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(AS)]
TestEq.from =
  \ (@ a_afx)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Representable a_afx) ->
    case tpl_B1
    of _ { TestEq.D:Representable _ tpl_B3 [ALWAYS Once Nothing] ->
    tpl_B3
    }

TestEq.conName :: forall c_afy.
                  (TestEq.Constructor c_afy) =>
                  forall (t_afz :: * -> * -> *) a_afA.
                  t_afz c_afy a_afA -> GHC.Base.String
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(SAA)]
TestEq.conName =
  \ (@ c_afy)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Constructor c_afy) ->
    case tpl_B1
    of _ { TestEq.D:Constructor tpl_B2 [ALWAYS Once Nothing] _ _ ->
    tpl_B2
    }

TestEq.conFixity :: forall c_afy.
                    (TestEq.Constructor c_afy) =>
                    forall (t_afB :: * -> * -> *) a_afC.
                    t_afB c_afy a_afC -> TestEq.Fixity
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(ASA)]
TestEq.conFixity =
  \ (@ c_afy)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Constructor c_afy) ->
    case tpl_B1
    of _ { TestEq.D:Constructor _ tpl_B3 [ALWAYS Once Nothing] _ ->
    tpl_B3
    }

TestEq.conIsRecord :: forall c_afy.
                      (TestEq.Constructor c_afy) =>
                      forall (t_afD :: * -> * -> *) a_afE.
                      t_afD c_afy a_afE -> GHC.Bool.Bool
GblId[ClassOp]
[Arity 1
 NoCafRefs
 Str: DmdType U(AAS)]
TestEq.conIsRecord =
  \ (@ c_afy)
    (tpl_B1 [ALWAYS Once! Nothing] :: TestEq.Constructor c_afy) ->
    case tpl_B1
    of _ { TestEq.D:Constructor _ _ tpl_B4 [ALWAYS Once Nothing] ->
    tpl_B4
    }

TestEq.$wa2 :: forall b_a1As.
               (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As
GblId
[Arity 1
 Str: DmdType L]
TestEq.$wa2 =
  \ (@ b_a1As)
    (w_s1Lk :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As) ->
    Text.Read.Lex.lex1
      @ b_a1As
      (let {
         lvl10_s1P2 :: Text.ParserCombinators.ReadP.P b_a1As
         LclId
         []
         lvl10_s1P2 = w_s1Lk TestEq.U } in
       \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
         case a3_a1AS of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
           Text.Read.Lex.Ident ds_d1cz ->
             case ds_d1cz of _ {
               [] -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
               : ds1_d1cA ds2_d1cB ->
                 case ds1_d1cA of _ { GHC.Types.C# ds3_d1cC ->
                 case ds3_d1cC of _ {
                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                   'U' ->
                     case ds2_d1cB of _ {
                       [] -> lvl10_s1P2;
                       : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1As
                     }
                 }
                 }
             }
         })

TestEq.$fReadU3 :: Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As
GblId
[Arity 2
 Worker TestEq.$wa2
 Str: DmdType AL]
TestEq.$fReadU3 =
  __inline_me (\ _
                 (@ b_a1As)
                 (w1_s1Lk :: TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As) ->
                 TestEq.$wa2 @ b_a1As w1_s1Lk)

TestEq.$fShowFixity1 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fShowFixity1 = GHC.Types.I# 0

TestEq.$fRead:+:2 :: GHC.Types.Int
GblId
[NoCafRefs]
TestEq.$fRead:+:2 = GHC.Types.I# 11

TestEq.$fReadC1 :: forall c_Xus a_Xuu.
                   (GHC.Read.Read a_Xuu) =>
                   Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xus a_Xuu)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fReadC1 =
  \ (@ c_Xus)
    (@ a_Xuu)
    ($dRead_aVL :: GHC.Read.Read a_Xuu)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.C c_Xus a_Xuu)
      ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta1_X8a :: TestEq.C c_Xus a_Xuu
                       -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do ->
          case GHC.Prim.<=# x_a1Do 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.C c_Xus a_Xuu) @ b_a1As eta1_X8a);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1As
                (let {
                   lvl10_s1P4 :: a_Xuu -> Text.ParserCombinators.ReadP.P b_a1As
                   LclId
                   [Arity 1]
                   lvl10_s1P4 =
                     \ (a3_X1Qk :: a_Xuu) ->
                       eta1_X8a (TestEq.C @ c_Xus @ a_Xuu a3_X1Qk) } in
                 \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                   case a3_a1AS of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                     Text.Read.Lex.Ident ds_d1bP ->
                       case ds_d1bP of _ {
                         [] -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         : ds1_d1bQ ds2_d1bR ->
                           case ds1_d1bQ of _ { GHC.Types.C# ds3_d1bS ->
                           case ds3_d1bS of _ {
                             __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                             'C' ->
                               case ds2_d1bR of _ {
                                 [] ->
                                   case $dRead_aVL of _ { GHC.Read.D:Read _ _ tpl3_Xf2 _ ->
                                   (((tpl3_Xf2
                                      `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xuu
                                              :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xuu
                                                   ~
                                                 (Text.ParserCombinators.ReadPrec.Prec
                                                  -> Text.ParserCombinators.ReadP.ReadP a_Xuu)))
                                       TestEq.$fRead:+:2)
                                    `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xuu
                                            :: Text.ParserCombinators.ReadP.ReadP a_Xuu
                                                 ~
                                               (forall b_a1As.
                                                (a_Xuu -> Text.ParserCombinators.ReadP.P b_a1As)
                                                -> Text.ParserCombinators.ReadP.P b_a1As)))
                                     @ b_a1As lvl10_s1P4
                                   };
                                 : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a1As
                               }
                           }
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.C c_Xus a_Xuu)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (TestEq.C c_Xus a_Xuu)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.C c_Xus a_Xuu -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xus a_Xuu)))
      eta_B1

TestEq.$fReadC2 :: forall c_Xux a_Xuz.
                   (GHC.Read.Read a_Xuz) =>
                   Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xux a_Xuz]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadC2 =
  \ (@ c_Xux) (@ a_Xuz) ($dRead_X1aD :: GHC.Read.Read a_Xuz) ->
    GHC.Read.$dmreadList2
      @ (TestEq.C c_Xux a_Xuz)
      ((TestEq.$fReadC1 @ c_Xux @ a_Xuz $dRead_X1aD)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (TestEq.C c_Xux a_Xuz))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xux a_Xuz))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xux a_Xuz)))

TestEq.$fRead:+:1 :: forall a_Xtt b_Xtv.
                     (GHC.Read.Read a_Xtt, GHC.Read.Read b_Xtv) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (a_Xtt TestEq.:+: b_Xtv)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:+:1 =
  \ (@ a_Xtt)
    (@ b_Xtv)
    ($dRead_aYo :: GHC.Read.Read a_Xtt)
    ($dRead1_aYp :: GHC.Read.Read b_Xtv) ->
    GHC.Read.$fRead()5
      @ (a_Xtt TestEq.:+: b_Xtv)
      ((Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
          @ (a_Xtt TestEq.:+: b_Xtv)
          ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b1_a1As)
              (eta_B1 :: (a_Xtt TestEq.:+: b_Xtv)
                         -> Text.ParserCombinators.ReadP.P b1_a1As) ->
              case c_a1Dl of _ { GHC.Types.I# x_a1Do ->
              case GHC.Prim.<=# x_a1Do 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xtt TestEq.:+: b_Xtv) @ b1_a1As eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b1_a1As
                    (let {
                       lvl10_s1P6 :: a_Xtt -> Text.ParserCombinators.ReadP.P b1_a1As
                       LclId
                       [Arity 1]
                       lvl10_s1P6 =
                         \ (a3_X1Pi :: a_Xtt) ->
                           eta_B1 (TestEq.L @ a_Xtt @ b_Xtv a3_X1Pi) } in
                     \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                       case a3_a1AS of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                         Text.Read.Lex.Ident ds_d1ca ->
                           case ds_d1ca of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                             : ds1_d1cb ds2_d1cc ->
                               case ds1_d1cb of _ { GHC.Types.C# ds3_d1cd ->
                               case ds3_d1cd of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                                 'L' ->
                                   case ds2_d1cc of _ {
                                     [] ->
                                       case $dRead_aYo of _ { GHC.Read.D:Read _ _ tpl3_Xe0 _ ->
                                       (((tpl3_Xe0
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    a_Xtt
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xtt
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP a_Xtt)))
                                           TestEq.$fRead:+:2)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xtt
                                                :: Text.ParserCombinators.ReadP.ReadP a_Xtt
                                                     ~
                                                   (forall b_a1As.
                                                    (a_Xtt -> Text.ParserCombinators.ReadP.P b_a1As)
                                                    -> Text.ParserCombinators.ReadP.P b_a1As)))
                                         @ b1_a1As lvl10_s1P6
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b1_a1As
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xtt TestEq.:+: b_Xtv)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xtt TestEq.:+: b_Xtv)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1As.
                          ((a_Xtt TestEq.:+: b_Xtv) -> Text.ParserCombinators.ReadP.P b_a1As)
                          -> Text.ParserCombinators.ReadP.P b_a1As)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtt TestEq.:+: b_Xtv)))
          ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
              (@ b1_a1As)
              (eta_B1 :: (a_Xtt TestEq.:+: b_Xtv)
                         -> Text.ParserCombinators.ReadP.P b1_a1As) ->
              case c_a1Dl of _ { GHC.Types.I# x_a1Do ->
              case GHC.Prim.<=# x_a1Do 10 of _ {
                GHC.Bool.False ->
                  __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                                 @ (a_Xtt TestEq.:+: b_Xtv) @ b1_a1As eta_B1);
                GHC.Bool.True ->
                  Text.Read.Lex.lex1
                    @ b1_a1As
                    (let {
                       lvl10_s1P8 :: b_Xtv -> Text.ParserCombinators.ReadP.P b1_a1As
                       LclId
                       [Arity 1]
                       lvl10_s1P8 =
                         \ (a3_X1Pi :: b_Xtv) ->
                           eta_B1 (TestEq.R @ a_Xtt @ b_Xtv a3_X1Pi) } in
                     \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                       case a3_a1AS of _ {
                         __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                         Text.Read.Lex.Ident ds_d1cl ->
                           case ds_d1cl of _ {
                             [] -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                             : ds1_d1cm ds2_d1cn ->
                               case ds1_d1cm of _ { GHC.Types.C# ds3_d1co ->
                               case ds3_d1co of _ {
                                 __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                                 'R' ->
                                   case ds2_d1cn of _ {
                                     [] ->
                                       case $dRead1_aYp of _ { GHC.Read.D:Read _ _ tpl3_Xe0 _ ->
                                       (((tpl3_Xe0
                                          `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                    b_Xtv
                                                  :: Text.ParserCombinators.ReadPrec.ReadPrec b_Xtv
                                                       ~
                                                     (Text.ParserCombinators.ReadPrec.Prec
                                                      -> Text.ParserCombinators.ReadP.ReadP b_Xtv)))
                                           TestEq.$fRead:+:2)
                                        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_Xtv
                                                :: Text.ParserCombinators.ReadP.ReadP b_Xtv
                                                     ~
                                                   (forall b_a1As.
                                                    (b_Xtv -> Text.ParserCombinators.ReadP.P b_a1As)
                                                    -> Text.ParserCombinators.ReadP.P b_a1As)))
                                         @ b1_a1As lvl10_s1P8
                                       };
                                     : _ _ -> Text.ParserCombinators.ReadP.Fail @ b1_a1As
                                   }
                               }
                               }
                           }
                       })
              }
              })
           `cast` (trans
                     (Text.ParserCombinators.ReadPrec.Prec
                      -> sym
                           (Text.ParserCombinators.ReadP.NTCo:ReadP (a_Xtt TestEq.:+: b_Xtv)))
                     (sym
                        (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                           (a_Xtt TestEq.:+: b_Xtv)))
                   :: (Text.ParserCombinators.ReadPrec.Prec
                       -> forall b_a1As.
                          ((a_Xtt TestEq.:+: b_Xtv) -> Text.ParserCombinators.ReadP.P b_a1As)
                          -> Text.ParserCombinators.ReadP.P b_a1As)
                        ~
                      Text.ParserCombinators.ReadPrec.ReadPrec
                        (a_Xtt TestEq.:+: b_Xtv))))
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xtt TestEq.:+: b_Xtv))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xtt TestEq.:+: b_Xtv))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtt TestEq.:+: b_Xtv)))

TestEq.$fRead:+:3 :: forall a_Xtz b_XtB.
                     (GHC.Read.Read a_Xtz, GHC.Read.Read b_XtB) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [a_Xtz TestEq.:+: b_XtB]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:+:3 =
  \ (@ a_Xtz)
    (@ b_XtB)
    ($dRead_X1ce :: GHC.Read.Read a_Xtz)
    ($dRead1_X1cg :: GHC.Read.Read b_XtB) ->
    GHC.Read.$dmreadList2
      @ (a_Xtz TestEq.:+: b_XtB)
      ((TestEq.$fRead:+:1 @ a_Xtz @ b_XtB $dRead_X1ce $dRead1_X1cg)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xtz TestEq.:+: b_XtB))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xtz TestEq.:+: b_XtB))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xtz TestEq.:+: b_XtB)))

TestEq.$fReadU2 :: Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP TestEq.U
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadU2 =
  GHC.Read.$fRead()5
    @ TestEq.U
    (TestEq.$fReadU3
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

TestEq.$fReadU1 :: Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP [TestEq.U]
GblId
[Str: DmdType]
TestEq.$fReadU1 =
  GHC.Read.$dmreadList2
    @ TestEq.U
    (TestEq.$fReadU2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))

TestEq.$fReadU4 :: Text.ParserCombinators.ReadP.P [TestEq.U]
GblId
[Str: DmdType]
TestEq.$fReadU4 =
  ((TestEq.$fReadU1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.U]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.U]
                ~
              (forall b_a1As.
               ([TestEq.U] -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As)))
    @ [TestEq.U]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.U])

lvl_r1Vq :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl_r1Vq = GHC.Base.unpackCString# "Bin "

TestEq.$fConstructorLeaf1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLeaf1 = GHC.Base.unpackCString# "Leaf"

lvl1_r1Vt :: GHC.Show.ShowS
GblId
[Arity 1
 Str: DmdType L]
lvl1_r1Vt = GHC.Base.++ @ GHC.Types.Char TestEq.$fConstructorLeaf1

lvl2_r1Vv :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl2_r1Vv = GHC.Base.unpackCString# "Disj "

lvl3_r1Vx :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl3_r1Vx = GHC.Base.unpackCString# "Conj "

lvl4_r1Vz :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl4_r1Vz = GHC.Base.unpackCString# "Equiv "

lvl5_r1VB :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl5_r1VB = GHC.Base.unpackCString# "Impl "

lvl6_r1VD :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl6_r1VD = GHC.Base.unpackCString# "Not "

TestEq.$fConstructorLogic_F_2 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_F_2 = GHC.Types.C# 'F'

TestEq.$fConstructorLogic_F_1 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorLogic_F_1 =
  GHC.Types.:
    @ GHC.Types.Char
    TestEq.$fConstructorLogic_F_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl7_r1VH :: GHC.Show.ShowS
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
lvl7_r1VH =
  GHC.Base.++ @ GHC.Types.Char TestEq.$fConstructorLogic_F_1

TestEq.$fConstructorLogic_T_2 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_T_2 = GHC.Types.C# 'T'

TestEq.$fConstructorLogic_T_1 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorLogic_T_1 =
  GHC.Types.:
    @ GHC.Types.Char
    TestEq.$fConstructorLogic_T_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl8_r1VL :: GHC.Show.ShowS
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
lvl8_r1VL =
  GHC.Base.++ @ GHC.Types.Char TestEq.$fConstructorLogic_T_1

lvl9_r1VN :: [GHC.Types.Char]
GblId
[Str: DmdType]
lvl9_r1VN = GHC.Base.unpackCString# "VarL "

TestEq.$fShowFixity2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowFixity2 = GHC.Base.unpackCString# "Infix "

TestEq.$fReadFixity7 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadFixity7 = GHC.Base.unpackCString# "Prefix"

TestEq.$fReadAssociativity9 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

TestEq.$fReadAssociativity12 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

TestEq.$fReadAssociativity15 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

TestEq.$fReadFixity5 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadFixity5 = GHC.Base.unpackCString# "Infix"

TestEq.$wa1 :: forall b_a1As.
               (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As
GblId
[Arity 1
 Str: DmdType L]
TestEq.$wa1 =
  \ (@ b_a1As)
    (w_s1Mu :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1As) ->
    Text.Read.Lex.lex1
      @ b_a1As
      (let {
         lvl10_s1P9 :: Text.ParserCombinators.ReadP.P b_a1As
         LclId
         []
         lvl10_s1P9 = w_s1Mu TestEq.Prefix } in
       \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
         case a3_a1AS of _ {
           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
           Text.Read.Lex.Ident ds_d1bp ->
             case GHC.Base.eqString ds_d1bp TestEq.$fReadFixity7 of _ {
               GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
               GHC.Bool.True -> lvl10_s1P9
             }
         })

TestEq.$fReadFixity6 :: Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1As.
                           (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                           -> Text.ParserCombinators.ReadP.P b_a1As
GblId
[Arity 2
 Worker TestEq.$wa1
 Str: DmdType AL]
TestEq.$fReadFixity6 =
  __inline_me (\ _
                 (@ b_a1As)
                 (w1_s1Mu :: TestEq.Fixity
                             -> Text.ParserCombinators.ReadP.P b_a1As) ->
                 TestEq.$wa1 @ b_a1As w1_s1Mu)

TestEq.$fShowRec1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowRec1 = GHC.Base.unpackCString# "Rec "

TestEq.$wshowsPrec3 :: forall a_afF.
                       (GHC.Show.Show a_afF) =>
                       GHC.Prim.Int# -> a_afF -> GHC.Base.String -> [GHC.Types.Char]
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$wshowsPrec3 =
  \ (@ a_afF)
    (w_s1Mh :: GHC.Show.Show a_afF)
    (ww_s1Mk :: GHC.Prim.Int#)
    (ww1_s1Mo :: a_afF) ->
    let {
      g_s1N3 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1N3 =
        case w_s1Mh of _ { GHC.Show.D:Show tpl1_Xg1 _ _ ->
        tpl1_Xg1 TestEq.$fRead:+:2 ww1_s1Mo
        } } in
    case GHC.Prim.>=# ww_s1Mk 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char TestEq.$fShowRec1 (g_s1N3 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               TestEq.$fShowRec1
               (g_s1N3
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

TestEq.$fReadRec2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadRec2 = GHC.Base.unpackCString# "Rec"

TestEq.$fReadRec1 :: forall a_Xvg.
                     (GHC.Read.Read a_Xvg) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvg)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fReadRec1 =
  \ (@ a_Xvg)
    ($dRead_aTD :: GHC.Read.Read a_Xvg)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Rec a_Xvg)
      ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta1_X8v :: TestEq.Rec a_Xvg
                       -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do ->
          case GHC.Prim.<=# x_a1Do 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Rec a_Xvg) @ b_a1As eta1_X8v);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1As
                (let {
                   lvl10_s1Pb :: a_Xvg -> Text.ParserCombinators.ReadP.P b_a1As
                   LclId
                   [Arity 1]
                   lvl10_s1Pb =
                     \ (a3_X1QY :: a_Xvg) -> eta1_X8v (TestEq.Rec @ a_Xvg a3_X1QY) } in
                 \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                   case a3_a1AS of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                     Text.Read.Lex.Ident ds_d1bB ->
                       case GHC.Base.eqString ds_d1bB TestEq.$fReadRec2 of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         GHC.Bool.True ->
                           case $dRead_aTD of _ { GHC.Read.D:Read _ _ tpl3_XfR _ ->
                           (((tpl3_XfR
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_Xvg
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_Xvg
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_Xvg)))
                               TestEq.$fRead:+:2)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_Xvg
                                    :: Text.ParserCombinators.ReadP.ReadP a_Xvg
                                         ~
                                       (forall b_a1As.
                                        (a_Xvg -> Text.ParserCombinators.ReadP.P b_a1As)
                                        -> Text.ParserCombinators.ReadP.P b_a1As)))
                             @ b_a1As lvl10_s1Pb
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Rec a_Xvg)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvg)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.Rec a_Xvg -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvg)))
      eta_B1

TestEq.$fReadRec3 :: forall a_Xvk.
                     (GHC.Read.Read a_Xvk) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xvk]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadRec3 =
  \ (@ a_Xvk) ($dRead_X19j :: GHC.Read.Read a_Xvk) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Rec a_Xvk)
      ((TestEq.$fReadRec1 @ a_Xvk $dRead_X19j)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvk))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvk))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvk)))

TestEq.$fShowVar1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowVar1 = GHC.Base.unpackCString# "Var "

TestEq.$wshowsPrec4 :: forall a_afG.
                       (GHC.Show.Show a_afG) =>
                       GHC.Prim.Int# -> a_afG -> GHC.Base.String -> [GHC.Types.Char]
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$wshowsPrec4 =
  \ (@ a_afG)
    (w_s1M4 :: GHC.Show.Show a_afG)
    (ww_s1M7 :: GHC.Prim.Int#)
    (ww1_s1Mb :: a_afG) ->
    let {
      g_s1N7 [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1N7 =
        case w_s1M4 of _ { GHC.Show.D:Show tpl1_XfC _ _ ->
        tpl1_XfC TestEq.$fRead:+:2 ww1_s1Mb
        } } in
    case GHC.Prim.>=# ww_s1M7 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char TestEq.$fShowVar1 (g_s1N7 x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               TestEq.$fShowVar1
               (g_s1N7
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

TestEq.$fReadVar2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fReadVar2 = GHC.Base.unpackCString# "Var"

TestEq.$fReadVar1 :: forall a_XuS.
                     (GHC.Read.Read a_XuS) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuS)
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fReadVar1 =
  \ (@ a_XuS)
    ($dRead_aUH :: GHC.Read.Read a_XuS)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (TestEq.Var a_XuS)
      ((\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b_a1As)
          (eta1_X8k :: TestEq.Var a_XuS
                       -> Text.ParserCombinators.ReadP.P b_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do ->
          case GHC.Prim.<=# x_a1Do 10 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (TestEq.Var a_XuS) @ b_a1As eta1_X8k);
            GHC.Bool.True ->
              Text.Read.Lex.lex1
                @ b_a1As
                (let {
                   lvl10_s1Pd :: a_XuS -> Text.ParserCombinators.ReadP.P b_a1As
                   LclId
                   [Arity 1]
                   lvl10_s1Pd =
                     \ (a3_X1Qz :: a_XuS) -> eta1_X8k (TestEq.Var @ a_XuS a3_X1Qz) } in
                 \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
                   case a3_a1AS of _ {
                     __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                     Text.Read.Lex.Ident ds_d1bI ->
                       case GHC.Base.eqString ds_d1bI TestEq.$fReadVar2 of _ {
                         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                         GHC.Bool.True ->
                           case $dRead_aUH of _ { GHC.Read.D:Read _ _ tpl3_Xfs _ ->
                           (((tpl3_Xfs
                              `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XuS
                                      :: Text.ParserCombinators.ReadPrec.ReadPrec a_XuS
                                           ~
                                         (Text.ParserCombinators.ReadPrec.Prec
                                          -> Text.ParserCombinators.ReadP.ReadP a_XuS)))
                               TestEq.$fRead:+:2)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XuS
                                    :: Text.ParserCombinators.ReadP.ReadP a_XuS
                                         ~
                                       (forall b_a1As.
                                        (a_XuS -> Text.ParserCombinators.ReadP.P b_a1As)
                                        -> Text.ParserCombinators.ReadP.P b_a1As)))
                             @ b_a1As lvl10_s1Pd
                           }
                       }
                   })
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (TestEq.Var a_XuS)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuS)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      (TestEq.Var a_XuS -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuS)))
      eta_B1

TestEq.$fReadVar3 :: forall a_XuW.
                     (GHC.Read.Read a_XuW) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuW]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadVar3 =
  \ (@ a_XuW) ($dRead_X19Y :: GHC.Read.Read a_XuW) ->
    GHC.Read.$dmreadList2
      @ (TestEq.Var a_XuW)
      ((TestEq.$fReadVar1 @ a_XuW $dRead_X19Y)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuW))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuW))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuW)))

TestEq.$fShowC1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShowC1 = GHC.Base.unpackCString# "C "

TestEq.$wshowsPrec2 :: forall c_afH a_afI.
                       (GHC.Show.Show a_afI) =>
                       GHC.Prim.Int# -> a_afI -> GHC.Base.String -> [GHC.Types.Char]
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$wshowsPrec2 =
  \ (@ c_afH)
    (@ a_afI)
    (w_s1LR :: GHC.Show.Show a_afI)
    (ww_s1LU :: GHC.Prim.Int#)
    (ww1_s1LY :: a_afI) ->
    let {
      g_s1Nb [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1Nb =
        case w_s1LR of _ { GHC.Show.D:Show tpl1_Xfe _ _ ->
        tpl1_Xfe TestEq.$fRead:+:2 ww1_s1LY
        } } in
    case GHC.Prim.>=# ww_s1LU 11 of _ {
      GHC.Bool.False ->
        \ (x_a1hi :: GHC.Base.String) ->
          GHC.Base.++ @ GHC.Types.Char TestEq.$fShowC1 (g_s1Nb x_a1hi);
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (GHC.Base.++
               @ GHC.Types.Char
               TestEq.$fShowC1
               (g_s1Nb
                  (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
    }

TestEq.$fRead:*:3 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fRead:*:3 = GHC.Types.I# 7

TestEq.$fShow:*:1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShow:*:1 = GHC.Base.unpackCString# " :*: "

TestEq.$wshowsPrec :: forall a_afJ b_afK.
                      (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                      GHC.Prim.Int#
                      -> a_afJ
                      -> b_afK
                      -> GHC.Base.String
                      -> GHC.Base.String
GblId
[Arity 5
 Str: DmdType LLLLL]
TestEq.$wshowsPrec =
  \ (@ a_afJ)
    (@ b_afK)
    (w_s1LC :: GHC.Show.Show a_afJ)
    (w1_s1LD :: GHC.Show.Show b_afK)
    (ww_s1LG :: GHC.Prim.Int#)
    (ww1_s1LK :: a_afJ)
    (ww2_s1LL :: b_afK) ->
    let {
      g_s1Nf [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      g_s1Nf =
        case w1_s1LD of _ { GHC.Show.D:Show tpl1_XeQ _ _ ->
        tpl1_XeQ TestEq.$fRead:*:3 ww2_s1LL
        } } in
    let {
      f_s1Nd [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
      LclId
      [Str: DmdType]
      f_s1Nd =
        case w_s1LC of _ { GHC.Show.D:Show tpl1_XeM _ _ ->
        tpl1_XeM TestEq.$fRead:*:3 ww1_s1LK
        } } in
    case GHC.Prim.>=# ww_s1LG 7 of _ {
      GHC.Bool.False ->
        \ (x_X1w8 :: GHC.Base.String) ->
          f_s1Nd
            (GHC.Base.++ @ GHC.Types.Char TestEq.$fShow:*:1 (g_s1Nf x_X1w8));
      GHC.Bool.True ->
        \ (x_a1hb :: GHC.Base.String) ->
          GHC.Types.:
            @ GHC.Types.Char
            GHC.Show.showSignedInt2
            (f_s1Nd
               (GHC.Base.++
                  @ GHC.Types.Char
                  TestEq.$fShow:*:1
                  (g_s1Nf
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
    }

TestEq.$fRead:*:2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fRead:*:2 = GHC.Base.unpackCString# ":*:"

TestEq.$fRead:*:1 :: forall a_XtY b_Xu0.
                     (GHC.Read.Read a_XtY, GHC.Read.Read b_Xu0) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP (a_XtY TestEq.:*: b_Xu0)
GblId
[Arity 3
 Str: DmdType LLL]
TestEq.$fRead:*:1 =
  \ (@ a_XtY)
    (@ b_Xu0)
    ($dRead_aWX :: GHC.Read.Read a_XtY)
    ($dRead1_aWY :: GHC.Read.Read b_Xu0)
    (eta_B1 :: Text.ParserCombinators.ReadPrec.Prec) ->
    GHC.Read.$fRead()5
      @ (a_XtY TestEq.:*: b_Xu0)
      (let {
         ds1_s1Ni [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadP
                                       a_XtY
         LclId
         [Str: DmdType]
         ds1_s1Ni =
           case $dRead_aWX of _ { GHC.Read.D:Read _ _ tpl3_Xeu _ ->
           (tpl3_Xeu
            `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_XtY
                    :: Text.ParserCombinators.ReadPrec.ReadPrec a_XtY
                         ~
                       (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP a_XtY)))
             TestEq.$fRead:*:3
           } } in
       (\ (c_a1Dl :: Text.ParserCombinators.ReadPrec.Prec)
          (@ b1_a1As)
          (eta1_X5L :: (a_XtY TestEq.:*: b_Xu0)
                       -> Text.ParserCombinators.ReadP.P b1_a1As) ->
          case c_a1Dl of _ { GHC.Types.I# x_a1Do ->
          case GHC.Prim.<=# x_a1Do 6 of _ {
            GHC.Bool.False ->
              __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                             @ (a_XtY TestEq.:*: b_Xu0) @ b1_a1As eta1_X5L);
            GHC.Bool.True ->
              (ds1_s1Ni
               `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP a_XtY
                       :: Text.ParserCombinators.ReadP.ReadP a_XtY
                            ~
                          (forall b_a1As.
                           (a_XtY -> Text.ParserCombinators.ReadP.P b_a1As)
                           -> Text.ParserCombinators.ReadP.P b_a1As)))
                @ b1_a1As
                (\ (a3_a1AS :: a_XtY) ->
                   Text.Read.Lex.lex1
                     @ b1_a1As
                     (let {
                        lvl10_s1Ph :: b_Xu0 -> Text.ParserCombinators.ReadP.P b1_a1As
                        LclId
                        [Arity 1]
                        lvl10_s1Ph =
                          \ (a31_X1PM :: b_Xu0) ->
                            eta1_X5L (TestEq.:*: @ a_XtY @ b_Xu0 a3_a1AS a31_X1PM) } in
                      \ (a31_X1Kt :: Text.Read.Lex.Lexeme) ->
                        case a31_X1Kt of _ {
                          __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                          Text.Read.Lex.Symbol ds_d1c2 ->
                            case GHC.Base.eqString ds_d1c2 TestEq.$fRead:*:2 of _ {
                              GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b1_a1As;
                              GHC.Bool.True ->
                                case $dRead1_aWY of _ { GHC.Read.D:Read _ _ tpl3_Xez _ ->
                                (((tpl3_Xez
                                   `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec b_Xu0
                                           :: Text.ParserCombinators.ReadPrec.ReadPrec b_Xu0
                                                ~
                                              (Text.ParserCombinators.ReadPrec.Prec
                                               -> Text.ParserCombinators.ReadP.ReadP b_Xu0)))
                                    TestEq.$fRead:*:3)
                                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP b_Xu0
                                         :: Text.ParserCombinators.ReadP.ReadP b_Xu0
                                              ~
                                            (forall b_a1As.
                                             (b_Xu0 -> Text.ParserCombinators.ReadP.P b_a1As)
                                             -> Text.ParserCombinators.ReadP.P b_a1As)))
                                  @ b1_a1As lvl10_s1Ph
                                }
                            }
                        }))
          }
          })
       `cast` (trans
                 (Text.ParserCombinators.ReadPrec.Prec
                  -> sym
                       (Text.ParserCombinators.ReadP.NTCo:ReadP (a_XtY TestEq.:*: b_Xu0)))
                 (sym
                    (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                       (a_XtY TestEq.:*: b_Xu0)))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> forall b_a1As.
                      ((a_XtY TestEq.:*: b_Xu0) -> Text.ParserCombinators.ReadP.P b_a1As)
                      -> Text.ParserCombinators.ReadP.P b_a1As)
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_XtY TestEq.:*: b_Xu0)))
      eta_B1

TestEq.$fRead:*:4 :: forall a_Xu4 b_Xu6.
                     (GHC.Read.Read a_Xu4, GHC.Read.Read b_Xu6) =>
                     Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP [a_Xu4 TestEq.:*: b_Xu6]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:*:4 =
  \ (@ a_Xu4)
    (@ b_Xu6)
    ($dRead_X1bk :: GHC.Read.Read a_Xu4)
    ($dRead1_X1bm :: GHC.Read.Read b_Xu6) ->
    GHC.Read.$dmreadList2
      @ (a_Xu4 TestEq.:*: b_Xu6)
      ((TestEq.$fRead:*:1 @ a_Xu4 @ b_Xu6 $dRead_X1bk $dRead1_X1bm)
       `cast` (sym
                 (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                    (a_Xu4 TestEq.:*: b_Xu6))
               :: (Text.ParserCombinators.ReadPrec.Prec
                   -> Text.ParserCombinators.ReadP.ReadP (a_Xu4 TestEq.:*: b_Xu6))
                    ~
                  Text.ParserCombinators.ReadPrec.ReadPrec (a_Xu4 TestEq.:*: b_Xu6)))

TestEq.$fShow:+:1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShow:+:1 = GHC.Base.unpackCString# "R "

TestEq.$fShow:+:2 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fShow:+:2 = GHC.Base.unpackCString# "L "

TestEq.$wshowsPrec1 :: forall a_afL b_afM.
                       (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                       GHC.Prim.Int#
                       -> (a_afL TestEq.:+: b_afM)
                       -> GHC.Base.String
                       -> [GHC.Types.Char]
GblId
[Arity 4
 Str: DmdType LLLS]
TestEq.$wshowsPrec1 =
  \ (@ a_afL)
    (@ b_afM)
    (w_s1Lq :: GHC.Show.Show a_afL)
    (w1_s1Lr :: GHC.Show.Show b_afM)
    (ww_s1Lu :: GHC.Prim.Int#)
    (w2_s1Lw :: a_afL TestEq.:+: b_afM) ->
    case w2_s1Lw of _ {
      TestEq.L b1_ayF ->
        let {
          g_s1Nn [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Nn =
            case w_s1Lq of _ { GHC.Show.D:Show tpl1_Xei _ _ ->
            tpl1_Xei TestEq.$fRead:+:2 b1_ayF
            } } in
        case GHC.Prim.>=# ww_s1Lu 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char TestEq.$fShow:+:2 (g_s1Nn x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   TestEq.$fShow:+:2
                   (g_s1Nn
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.R b1_ayH ->
        let {
          g_s1Np [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Np =
            case w1_s1Lr of _ { GHC.Show.D:Show tpl1_Xei _ _ ->
            tpl1_Xei TestEq.$fRead:+:2 b1_ayH
            } } in
        case GHC.Prim.>=# ww_s1Lu 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char TestEq.$fShow:+:1 (g_s1Np x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   TestEq.$fShow:+:1
                   (g_s1Np
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        }
    }

TestEq.$fShowU2 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fShowU2 = GHC.Types.C# 'U'

TestEq.$fShowU1 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fShowU1 =
  GHC.Types.:
    @ GHC.Types.Char TestEq.$fShowU2 (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fRepresentable[]2 :: TestEq.C TestEq.List_Nil_ TestEq.U
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentable[]2 =
  TestEq.C @ TestEq.List_Nil_ @ TestEq.U TestEq.U

TestEq.$fRepresentable[]1 :: forall a_agN.
                             TestEq.C TestEq.List_Nil_ TestEq.U
                             TestEq.:+: TestEq.C
                                          TestEq.List_Cons_
                                          (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fRepresentable[]1 =
  \ (@ a_agN) ->
    TestEq.L
      @ (TestEq.C TestEq.List_Nil_ TestEq.U)
      @ (TestEq.C
           TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
      TestEq.$fRepresentable[]2

TestEq.fromTree2 :: forall c_aFl. TestEq.C c_aFl TestEq.U
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.fromTree2 =
  \ (@ c_aFl) -> TestEq.C @ c_aFl @ TestEq.U TestEq.U

TestEq.fromTree1 :: forall t_aF3 c_aFf c1_aFl.
                    TestEq.C c1_aFl TestEq.U
                    TestEq.:+: TestEq.C
                                 c_aFf
                                 (TestEq.Var t_aF3
                                  TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                              TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
GblId
[NoCafRefs
 Str: DmdType]
TestEq.fromTree1 =
  \ (@ t_aF3) (@ c_aFf) (@ c1_aFl) ->
    TestEq.L
      @ (TestEq.C c1_aFl TestEq.U)
      @ (TestEq.C
           c_aFf
           (TestEq.Var t_aF3
            TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                        TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
      (TestEq.fromTree2 @ c1_aFl)

TestEq.fromLogic4 :: forall c_aCl. TestEq.C c_aCl TestEq.U
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.fromLogic4 =
  \ (@ c_aCl) -> TestEq.C @ c_aCl @ TestEq.U TestEq.U

TestEq.fromLogic3 :: forall c_aCl
                            c1_aCz
                            c2_aCT
                            c3_aDg
                            c4_aDG
                            c5_aE7.
                     TestEq.C c_aCl TestEq.U
                     TestEq.:+: (TestEq.C c1_aCz (TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c2_aCT
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c3_aDg
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c4_aDG
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: TestEq.C
                                                                                  c5_aE7
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic
                                                                                   TestEq.:*: TestEq.Rec
                                                                                                TestEq.Logic)))))
GblId
[NoCafRefs
 Str: DmdType]
TestEq.fromLogic3 =
  \ (@ c_aCl)
    (@ c1_aCz)
    (@ c2_aCT)
    (@ c3_aDg)
    (@ c4_aDG)
    (@ c5_aE7) ->
    TestEq.L
      @ (TestEq.C c_aCl TestEq.U)
      @ (TestEq.C c1_aCz (TestEq.Rec TestEq.Logic)
         TestEq.:+: (TestEq.C
                       c2_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                     TestEq.:+: (TestEq.C
                                   c3_aDg
                                   (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c4_aDG
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: TestEq.C
                                                          c5_aE7
                                                          (TestEq.Rec TestEq.Logic
                                                           TestEq.:*: TestEq.Rec TestEq.Logic)))))
      (TestEq.fromLogic4 @ c_aCl)

TestEq.fromLogic2 :: forall c_aCb
                            c1_aCl
                            c2_aCz
                            c3_aCT
                            c4_aDg
                            c5_aDG
                            c6_aE7.
                     TestEq.C c_aCb TestEq.U
                     TestEq.:+: (TestEq.C c1_aCl TestEq.U
                                 TestEq.:+: (TestEq.C c2_aCz (TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c3_aCT
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c4_aDg
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: (TestEq.C
                                                                                   c5_aDG
                                                                                   (TestEq.Rec
                                                                                      TestEq.Logic
                                                                                    TestEq.:*: TestEq.Rec
                                                                                                 TestEq.Logic)
                                                                                 TestEq.:+: TestEq.C
                                                                                              c6_aE7
                                                                                              (TestEq.Rec
                                                                                                 TestEq.Logic
                                                                                               TestEq.:*: TestEq.Rec
                                                                                                            TestEq.Logic))))))
GblId
[NoCafRefs
 Str: DmdType]
TestEq.fromLogic2 =
  \ (@ c_aCb)
    (@ c1_aCl)
    (@ c2_aCz)
    (@ c3_aCT)
    (@ c4_aDg)
    (@ c5_aDG)
    (@ c6_aE7) ->
    TestEq.R
      @ (TestEq.C c_aCb TestEq.U)
      @ (TestEq.C c1_aCl TestEq.U
         TestEq.:+: (TestEq.C c2_aCz (TestEq.Rec TestEq.Logic)
                     TestEq.:+: (TestEq.C
                                   c3_aCT
                                   (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c4_aDg
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c5_aDG
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: TestEq.C
                                                                      c6_aE7
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic))))))
      (TestEq.fromLogic3
         @ c1_aCl @ c2_aCz @ c3_aCT @ c4_aDg @ c5_aDG @ c6_aE7)

TestEq.fromLogic1 :: forall c_aC3
                            c1_aCb
                            c2_aCl
                            c3_aCz
                            c4_aCT
                            c5_aDg
                            c6_aDG
                            c7_aE7.
                     TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                     TestEq.:+: (TestEq.C c1_aCb TestEq.U
                                 TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c4_aCT
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: (TestEq.C
                                                                                   c5_aDg
                                                                                   (TestEq.Rec
                                                                                      TestEq.Logic
                                                                                    TestEq.:*: TestEq.Rec
                                                                                                 TestEq.Logic)
                                                                                 TestEq.:+: (TestEq.C
                                                                                               c6_aDG
                                                                                               (TestEq.Rec
                                                                                                  TestEq.Logic
                                                                                                TestEq.:*: TestEq.Rec
                                                                                                             TestEq.Logic)
                                                                                             TestEq.:+: TestEq.C
                                                                                                          c7_aE7
                                                                                                          (TestEq.Rec
                                                                                                             TestEq.Logic
                                                                                                           TestEq.:*: TestEq.Rec
                                                                                                                        TestEq.Logic)))))))
GblId
[NoCafRefs
 Str: DmdType]
TestEq.fromLogic1 =
  \ (@ c_aC3)
    (@ c1_aCb)
    (@ c2_aCl)
    (@ c3_aCz)
    (@ c4_aCT)
    (@ c5_aDg)
    (@ c6_aDG)
    (@ c7_aE7) ->
    TestEq.R
      @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
      @ (TestEq.C c1_aCb TestEq.U
         TestEq.:+: (TestEq.C c2_aCl TestEq.U
                     TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c4_aCT
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c5_aDg
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c6_aDG
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: TestEq.C
                                                                                  c7_aE7
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic
                                                                                   TestEq.:*: TestEq.Rec
                                                                                                TestEq.Logic)))))))
      (TestEq.fromLogic2
         @ c1_aCb @ c2_aCl @ c3_aCz @ c4_aCT @ c5_aDg @ c6_aDG @ c7_aE7)

TestEq.fromLogic7 :: forall c_aCb. TestEq.C c_aCb TestEq.U
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.fromLogic7 =
  \ (@ c_aCb) -> TestEq.C @ c_aCb @ TestEq.U TestEq.U

TestEq.fromLogic6 :: forall c_aCb
                            c1_aCl
                            c2_aCz
                            c3_aCT
                            c4_aDg
                            c5_aDG
                            c6_aE7.
                     TestEq.C c_aCb TestEq.U
                     TestEq.:+: (TestEq.C c1_aCl TestEq.U
                                 TestEq.:+: (TestEq.C c2_aCz (TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c3_aCT
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c4_aDg
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: (TestEq.C
                                                                                   c5_aDG
                                                                                   (TestEq.Rec
                                                                                      TestEq.Logic
                                                                                    TestEq.:*: TestEq.Rec
                                                                                                 TestEq.Logic)
                                                                                 TestEq.:+: TestEq.C
                                                                                              c6_aE7
                                                                                              (TestEq.Rec
                                                                                                 TestEq.Logic
                                                                                               TestEq.:*: TestEq.Rec
                                                                                                            TestEq.Logic))))))
GblId
[NoCafRefs
 Str: DmdType]
TestEq.fromLogic6 =
  \ (@ c_aCb)
    (@ c1_aCl)
    (@ c2_aCz)
    (@ c3_aCT)
    (@ c4_aDg)
    (@ c5_aDG)
    (@ c6_aE7) ->
    TestEq.L
      @ (TestEq.C c_aCb TestEq.U)
      @ (TestEq.C c1_aCl TestEq.U
         TestEq.:+: (TestEq.C c2_aCz (TestEq.Rec TestEq.Logic)
                     TestEq.:+: (TestEq.C
                                   c3_aCT
                                   (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c4_aDg
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c5_aDG
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: TestEq.C
                                                                      c6_aE7
                                                                      (TestEq.Rec TestEq.Logic
                                                                       TestEq.:*: TestEq.Rec
                                                                                    TestEq.Logic))))))
      (TestEq.fromLogic7 @ c_aCb)

TestEq.fromLogic5 :: forall c_aC3
                            c1_aCb
                            c2_aCl
                            c3_aCz
                            c4_aCT
                            c5_aDg
                            c6_aDG
                            c7_aE7.
                     TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                     TestEq.:+: (TestEq.C c1_aCb TestEq.U
                                 TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                             TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c4_aCT
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: (TestEq.C
                                                                                   c5_aDg
                                                                                   (TestEq.Rec
                                                                                      TestEq.Logic
                                                                                    TestEq.:*: TestEq.Rec
                                                                                                 TestEq.Logic)
                                                                                 TestEq.:+: (TestEq.C
                                                                                               c6_aDG
                                                                                               (TestEq.Rec
                                                                                                  TestEq.Logic
                                                                                                TestEq.:*: TestEq.Rec
                                                                                                             TestEq.Logic)
                                                                                             TestEq.:+: TestEq.C
                                                                                                          c7_aE7
                                                                                                          (TestEq.Rec
                                                                                                             TestEq.Logic
                                                                                                           TestEq.:*: TestEq.Rec
                                                                                                                        TestEq.Logic)))))))
GblId
[NoCafRefs
 Str: DmdType]
TestEq.fromLogic5 =
  \ (@ c_aC3)
    (@ c1_aCb)
    (@ c2_aCl)
    (@ c3_aCz)
    (@ c4_aCT)
    (@ c5_aDg)
    (@ c6_aDG)
    (@ c7_aE7) ->
    TestEq.R
      @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
      @ (TestEq.C c1_aCb TestEq.U
         TestEq.:+: (TestEq.C c2_aCl TestEq.U
                     TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                 TestEq.:+: (TestEq.C
                                               c4_aCT
                                               (TestEq.Rec TestEq.Logic
                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                             TestEq.:+: (TestEq.C
                                                           c5_aDg
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic)
                                                         TestEq.:+: (TestEq.C
                                                                       c6_aDG
                                                                       (TestEq.Rec TestEq.Logic
                                                                        TestEq.:*: TestEq.Rec
                                                                                     TestEq.Logic)
                                                                     TestEq.:+: TestEq.C
                                                                                  c7_aE7
                                                                                  (TestEq.Rec
                                                                                     TestEq.Logic
                                                                                   TestEq.:*: TestEq.Rec
                                                                                                TestEq.Logic)))))))
      (TestEq.fromLogic6
         @ c1_aCb @ c2_aCl @ c3_aCz @ c4_aCT @ c5_aDg @ c6_aDG @ c7_aE7)

TestEq.$fConstructorList_Nil_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorList_Nil_1 = GHC.Base.unpackCString# "[]"

TestEq.$fConstructorBin1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorBin1 = GHC.Base.unpackCString# "Bin"

TestEq.$fConstructorLogic_Var_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Var_1 = GHC.Base.unpackCString# "VarL"

TestEq.$fConstructorLogic_Impl_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Impl_1 = GHC.Base.unpackCString# "Impl"

TestEq.$fConstructorLogic_Equiv_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Equiv_1 = GHC.Base.unpackCString# "Equiv"

TestEq.$fConstructorLogic_Conj_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Conj_1 = GHC.Base.unpackCString# "Conj"

TestEq.$fConstructorLogic_Disj_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Disj_1 = GHC.Base.unpackCString# "Disj"

TestEq.$fConstructorLogic_Not_1 :: [GHC.Types.Char]
GblId
[Str: DmdType]
TestEq.$fConstructorLogic_Not_1 = GHC.Base.unpackCString# "Not"

TestEq.$fConstructorList_Cons_2 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorList_Cons_2 = GHC.Types.I# 5

TestEq.$fConstructorList_Cons_1 :: TestEq.Fixity
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorList_Cons_1 =
  TestEq.Infix
    TestEq.RightAssociative TestEq.$fConstructorList_Cons_2

TestEq.$fConstructorList_Cons_4 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorList_Cons_4 = GHC.Types.C# ':'

TestEq.$fConstructorList_Cons_3 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.$fConstructorList_Cons_3 =
  GHC.Types.:
    @ GHC.Types.Char
    TestEq.$fConstructorList_Cons_4
    (GHC.Types.[] @ GHC.Types.Char)

TestEq.$dmconIsRecord_$s$dmconIsRecord11 :: forall (t_afD :: *
                                                             -> *
                                                             -> *)
                                                   a_afE.
                                            t_afD TestEq.List_Nil_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord11 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord10 :: forall (t_afD :: *
                                                             -> *
                                                             -> *)
                                                   a_afE.
                                            t_afD TestEq.List_Cons_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord10 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord9 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Bin a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord9 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord8 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Leaf a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord8 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord7 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Var_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord7 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord6 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Impl_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord6 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord5 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Equiv_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord5 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord4 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Conj_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord4 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord3 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Disj_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord3 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord2 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_Not_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord2 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord1 :: forall (t_afD :: *
                                                            -> *
                                                            -> *)
                                                  a_afE.
                                           t_afD TestEq.Logic_T_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord1 =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconIsRecord_$s$dmconIsRecord :: forall (t_afD :: *
                                                           -> *
                                                           -> *)
                                                 a_afE.
                                          t_afD TestEq.Logic_F_ a_afE -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconIsRecord_$s$dmconIsRecord =
  __inline_me (\ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconFixity_$s$dmconFixity10 :: forall (t_afB :: *
                                                         -> *
                                                         -> *)
                                               a_afC.
                                        t_afB TestEq.Logic_F_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity10 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity9 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_T_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity9 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity8 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Not_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity8 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity7 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Disj_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity7 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity6 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Conj_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity6 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity5 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Equiv_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity5 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity4 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Impl_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity4 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity3 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Logic_Var_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity3 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity2 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Leaf a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity2 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity1 :: forall (t_afB :: *
                                                        -> *
                                                        -> *)
                                              a_afC.
                                       t_afB TestEq.Bin a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity1 =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$dmconFixity_$s$dmconFixity :: forall (t_afB :: * -> * -> *)
                                             a_afC.
                                      t_afB TestEq.List_Nil_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$dmconFixity_$s$dmconFixity =
  __inline_me (\ (@ t_aFL::* -> * -> *) (@ a_aFM) _ -> TestEq.Prefix)

TestEq.$fOrdAssociativity1 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> TestEq.Associativity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity1 =
  \ (x_a1kz :: TestEq.Associativity)
    (y_a1kA :: TestEq.Associativity) ->
    case x_a1kz of _ {
      TestEq.LeftAssociative ->
        case y_a1kA of _ { __DEFAULT -> TestEq.LeftAssociative };
      TestEq.RightAssociative ->
        case y_a1kA of _ {
          TestEq.LeftAssociative -> TestEq.LeftAssociative;
          TestEq.RightAssociative -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.RightAssociative
        };
      TestEq.NotAssociative -> y_a1kA
    }

TestEq.$fOrdAssociativity2 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> TestEq.Associativity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity2 =
  \ (x_a1kf :: TestEq.Associativity)
    (y_a1kg :: TestEq.Associativity) ->
    case x_a1kf of _ {
      TestEq.LeftAssociative -> y_a1kg;
      TestEq.RightAssociative ->
        case y_a1kg of _ {
          __DEFAULT -> TestEq.RightAssociative;
          TestEq.NotAssociative -> TestEq.NotAssociative
        };
      TestEq.NotAssociative ->
        case y_a1kg of _ { __DEFAULT -> TestEq.NotAssociative }
    }

TestEq.$fOrdAssociativity3 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity3 =
  \ (x_a1jY :: TestEq.Associativity)
    (y_a1jZ :: TestEq.Associativity) ->
    case x_a1jY of _ {
      TestEq.LeftAssociative ->
        case y_a1jZ of _ { __DEFAULT -> GHC.Bool.True };
      TestEq.RightAssociative ->
        case y_a1jZ of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1jZ of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

TestEq.$fOrdAssociativity4 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity4 =
  \ (x_a1jH :: TestEq.Associativity)
    (y_a1jI :: TestEq.Associativity) ->
    case x_a1jH of _ {
      TestEq.LeftAssociative ->
        case y_a1jI of _ { __DEFAULT -> GHC.Bool.False };
      TestEq.RightAssociative ->
        case y_a1jI of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jI of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

TestEq.$fOrdAssociativity5 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity5 =
  \ (x_a1jq :: TestEq.Associativity)
    (y_a1jr :: TestEq.Associativity) ->
    case x_a1jq of _ {
      TestEq.LeftAssociative ->
        case y_a1jr of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case y_a1jr of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case y_a1jr of _ { __DEFAULT -> GHC.Bool.True }
    }

TestEq.$fOrdAssociativity6 :: TestEq.Associativity
                              -> TestEq.Associativity
                              -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity6 =
  \ (x_a1j9 :: TestEq.Associativity)
    (y_a1ja :: TestEq.Associativity) ->
    case x_a1j9 of _ {
      TestEq.LeftAssociative ->
        case y_a1ja of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case y_a1ja of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case y_a1ja of _ { __DEFAULT -> GHC.Bool.False }
    }

TestEq.$fShowAssociativity1 :: TestEq.Associativity
                               -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType S]
TestEq.$fShowAssociativity1 =
  \ (x_X1xG :: TestEq.Associativity) ->
    case x_X1xG of _ {
      TestEq.LeftAssociative -> TestEq.$fReadAssociativity15;
      TestEq.RightAssociative -> TestEq.$fReadAssociativity12;
      TestEq.NotAssociative -> TestEq.$fReadAssociativity9
    }

TestEq.$fReadAssociativity14 :: Text.ParserCombinators.ReadPrec.Prec
                                -> forall b_X1Qz.
                                   (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1Qz)
                                   -> Text.ParserCombinators.ReadP.P b_X1Qz
GblId
[Arity 2
 NoCafRefs]
TestEq.$fReadAssociativity14 =
  \ _
    (@ b_X1Qz)
    (eta_X1QB :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1Qz) ->
    eta_X1QB TestEq.LeftAssociative

TestEq.$fReadAssociativity13 :: ([GHC.Types.Char],
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
GblId
[]
TestEq.$fReadAssociativity13 =
  (TestEq.$fReadAssociativity15,
   TestEq.$fReadAssociativity14
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1As.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                  -> Text.ParserCombinators.ReadP.P b_a1As)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity11 :: Text.ParserCombinators.ReadPrec.Prec
                                -> forall b_X1QG.
                                   (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1QG)
                                   -> Text.ParserCombinators.ReadP.P b_X1QG
GblId
[Arity 2
 NoCafRefs]
TestEq.$fReadAssociativity11 =
  \ _
    (@ b_X1QG)
    (eta_X1QI :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1QG) ->
    eta_X1QI TestEq.RightAssociative

TestEq.$fReadAssociativity10 :: ([GHC.Types.Char],
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
GblId
[]
TestEq.$fReadAssociativity10 =
  (TestEq.$fReadAssociativity12,
   TestEq.$fReadAssociativity11
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1As.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                  -> Text.ParserCombinators.ReadP.P b_a1As)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity8 :: Text.ParserCombinators.ReadPrec.Prec
                               -> forall b_X1QN.
                                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_X1QN)
                                  -> Text.ParserCombinators.ReadP.P b_X1QN
GblId
[Arity 2
 NoCafRefs]
TestEq.$fReadAssociativity8 =
  \ _
    (@ b_X1QN)
    (eta_X1QP :: TestEq.Associativity
                 -> Text.ParserCombinators.ReadP.P b_X1QN) ->
    eta_X1QP TestEq.NotAssociative

TestEq.$fReadAssociativity7 :: ([GHC.Types.Char],
                                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
GblId
[]
TestEq.$fReadAssociativity7 =
  (TestEq.$fReadAssociativity9,
   TestEq.$fReadAssociativity8
   `cast` (right
             (inst
                (trans
                   (forall a_a1fq.
                    a_a1fq
                    -> Text.ParserCombinators.ReadPrec.Prec
                    -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP a_a1fq))
                   (forall a_a1fr.
                    a_a1fr
                    -> sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec a_a1fr)))
                TestEq.Associativity)
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a1As.
                  (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                  -> Text.ParserCombinators.ReadP.P b_a1As)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity6 :: [(GHC.Base.String,
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
GblId
[]
TestEq.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    TestEq.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity5 :: [(GHC.Base.String,
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
GblId
[]
TestEq.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    TestEq.$fReadAssociativity10
    TestEq.$fReadAssociativity6

TestEq.$fReadAssociativity4 :: [(GHC.Base.String,
                                 Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)]
GblId
[]
TestEq.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
    TestEq.$fReadAssociativity13
    TestEq.$fReadAssociativity5

TestEq.$fReadAssociativity3 :: Text.ParserCombinators.ReadPrec.ReadPrec
                                 TestEq.Associativity
GblId
[Str: DmdType]
TestEq.$fReadAssociativity3 =
  GHC.Read.choose1 @ TestEq.Associativity TestEq.$fReadAssociativity4

TestEq.$fReadAssociativity17 :: GHC.Types.Int
                                -> Text.ParserCombinators.ReadP.ReadS TestEq.Associativity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadAssociativity17 =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Associativity
      (((GHC.Read.$fRead()5
           @ TestEq.Associativity TestEq.$fReadAssociativity3 eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  TestEq.Associativity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                     ~
                   (forall b_a1As.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ TestEq.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ TestEq.Associativity))

TestEq.$fReadAssociativity2 :: Text.ParserCombinators.ReadPrec.Prec
                               -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadAssociativity2 =
  GHC.Read.$fRead()5
    @ TestEq.Associativity TestEq.$fReadAssociativity3

TestEq.$fReadAssociativity1 :: Text.ParserCombinators.ReadPrec.Prec
                               -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ TestEq.Associativity
    (TestEq.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))

TestEq.$fReadAssociativity16 :: Text.ParserCombinators.ReadP.P
                                  [TestEq.Associativity]
GblId
[Str: DmdType]
TestEq.$fReadAssociativity16 =
  ((TestEq.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [TestEq.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity]
                ~
              (forall b_a1As.
               ([TestEq.Associativity] -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As)))
    @ [TestEq.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_return
       @ [TestEq.Associativity])

Rec {
a7_r1Xp :: Text.ParserCombinators.ReadPrec.Prec
           -> forall b_a1B4.
              (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1B4)
              -> Text.ParserCombinators.ReadP.P b_a1B4
GblId
[Arity 2
 Str: DmdType AL]
a7_r1Xp =
  __inline_me (GHC.Read.$fRead()4
                 @ TestEq.Associativity
                 (TestEq.$fReadFixity_a8
                  `cast` (trans
                            (trans
                               (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                            (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                          :: (Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                               ~
                             Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)))
TestEq.$fReadFixity_a8 :: Text.ParserCombinators.ReadPrec.Prec
                          -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity_a8 =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Associativity
    TestEq.$fReadAssociativity3
    (a7_r1Xp
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym
                     (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Associativity))
               (sym
                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                     TestEq.Associativity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
end Rec }

TestEq.$wa :: GHC.Prim.Int#
              -> forall b_a1As.
                 (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                 -> Text.ParserCombinators.ReadP.P b_a1As
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$wa =
  \ (ww_s1MA :: GHC.Prim.Int#)
    (@ b_a1As)
    (w_s1MC :: TestEq.Fixity
               -> Text.ParserCombinators.ReadP.P b_a1As) ->
    case GHC.Prim.<=# ww_s1MA 10 of _ {
      GHC.Bool.False ->
        __inline_me (Text.ParserCombinators.ReadP.$fMonadPlusReadP2
                       @ TestEq.Fixity @ b_a1As w_s1MC);
      GHC.Bool.True ->
        Text.Read.Lex.lex1
          @ b_a1As
          (let {
             lvl10_s1Pu :: Text.ParserCombinators.ReadP.P b_a1As
             LclId
             []
             lvl10_s1Pu =
               let {
                 k_s1NH :: TestEq.Associativity
                           -> Text.ParserCombinators.ReadP.P b_a1As
                 LclId
                 [Arity 1
                  Str: DmdType L]
                 k_s1NH =
                   \ (a3_X1Sa :: TestEq.Associativity) ->
                     ((GHC.Read.$dmreadsPrec14 TestEq.$fRead:+:2)
                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                              :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                   ~
                                 (forall b_a1As.
                                  (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_a1As)
                                  -> Text.ParserCombinators.ReadP.P b_a1As)))
                       @ b_a1As
                       (\ (a31_X1LK :: GHC.Types.Int) ->
                          w_s1MC (TestEq.Infix a3_X1Sa a31_X1LK)) } in
               Text.ParserCombinators.ReadP.$fMonadPlusP_mplus
                 @ b_a1As
                 ((((TestEq.$fReadAssociativity3
                     `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                               TestEq.Associativity
                             :: Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity
                                  ~
                                (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)))
                      TestEq.$fRead:+:2)
                   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                             TestEq.Associativity
                           :: Text.ParserCombinators.ReadP.ReadP TestEq.Associativity
                                ~
                              (forall b_a1As.
                               (TestEq.Associativity -> Text.ParserCombinators.ReadP.P b_a1As)
                               -> Text.ParserCombinators.ReadP.P b_a1As)))
                    @ b_a1As k_s1NH)
                 (GHC.Read.$wa
                    @ TestEq.Associativity
                    (TestEq.$fReadFixity_a8
                     `cast` (trans
                               (trans
                                  (sym
                                     (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                        TestEq.Associativity))
                                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity)
                             :: (Text.ParserCombinators.ReadPrec.Prec
                                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                                  ~
                                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
                    @ b_a1As
                    k_s1NH) } in
           \ (a3_a1AS :: Text.Read.Lex.Lexeme) ->
             case a3_a1AS of _ {
               __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
               Text.Read.Lex.Ident ds_d1bu ->
                 case GHC.Base.eqString ds_d1bu TestEq.$fReadFixity5 of _ {
                   GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a1As;
                   GHC.Bool.True -> lvl10_s1Pu
                 }
             })
    }

TestEq.$fReadFixity4 :: Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1As.
                           (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                           -> Text.ParserCombinators.ReadP.P b_a1As
GblId
[Arity 2
 Worker TestEq.$wa
 Str: DmdType U(L)L]
TestEq.$fReadFixity4 =
  __inline_me (\ (w_s1My :: Text.ParserCombinators.ReadPrec.Prec)
                 (@ b_a1As)
                 (w1_s1MC :: TestEq.Fixity
                             -> Text.ParserCombinators.ReadP.P b_a1As) ->
                 case w_s1My of _ { GHC.Types.I# ww_s1MA ->
                 TestEq.$wa ww_s1MA @ b_a1As w1_s1MC
                 })

TestEq.$fReadFixity3 :: Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity3 =
  Text.ParserCombinators.ReadPrec.$fMonadPlusReadPrec1
    @ TestEq.Fixity
    (TestEq.$fReadFixity6
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (TestEq.$fReadFixity4
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity))
               (trans
                  (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                  (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a1As.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

TestEq.$fReadFixity9 :: GHC.Types.Int
                        -> Text.ParserCombinators.ReadP.ReadS TestEq.Fixity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity9 =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.Fixity
      (((GHC.Read.$fRead()5
           @ TestEq.Fixity
           (TestEq.$fReadFixity3
            `cast` (trans
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
                      (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.Fixity
                :: Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
                     ~
                   (forall b_a1As.
                    (TestEq.Fixity -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ TestEq.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.Fixity))

TestEq.$fReadFixity2 :: Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity2 =
  GHC.Read.$fRead()5
    @ TestEq.Fixity
    (TestEq.$fReadFixity3
     `cast` (trans
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity))
               (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

TestEq.$fReadFixity1 :: Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
GblId
[Str: DmdType]
TestEq.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ TestEq.Fixity
    (TestEq.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))

TestEq.$fReadFixity8 :: Text.ParserCombinators.ReadP.P
                          [TestEq.Fixity]
GblId
[Str: DmdType]
TestEq.$fReadFixity8 =
  ((TestEq.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity]
                ~
              (forall b_a1As.
               ([TestEq.Fixity] -> Text.ParserCombinators.ReadP.P b_a1As)
               -> Text.ParserCombinators.ReadP.P b_a1As)))
    @ [TestEq.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_return @ [TestEq.Fixity])

TestEq.$fShowU3 :: TestEq.U -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType U()]
TestEq.$fShowU3 =
  __inline_me (\ (x_a1gf :: TestEq.U) ->
                 case x_a1gf of _ { TestEq.U -> TestEq.$fShowU1 })

TestEq.$fReadU5 :: GHC.Types.Int
                   -> Text.ParserCombinators.ReadP.ReadS TestEq.U
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadU5 =
  \ (eta_a1fN :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ TestEq.U
      (((GHC.Read.$fRead()5
           @ TestEq.U
           (TestEq.$fReadU3
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U))
                      (trans
                         (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U))
                         (Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a1As.
                           (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                           -> Text.ParserCombinators.ReadP.P b_a1As)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
           eta_a1fN)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP TestEq.U
                :: Text.ParserCombinators.ReadP.ReadP TestEq.U
                     ~
                   (forall b_a1As.
                    (TestEq.U -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ TestEq.U
         (Text.ParserCombinators.ReadP.$fMonadP_return @ TestEq.U))

TestEq.$fRepresentableRec2 :: forall a_XnQ.
                              (TestEq.Representable a_XnQ) =>
                              TestEq.Rep (TestEq.Rec a_XnQ) -> TestEq.Rep (TestEq.Rec a_XnQ)
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableRec2 =
  __inline_me (\ (@ a_XnQ)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Rec a_XnQ)) ->
                 eta_B1)

TestEq.$fRepresentableRec1 :: forall a_ah2.
                              (TestEq.Representable a_ah2) =>
                              TestEq.Rec a_ah2 -> TestEq.Rec a_ah2
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableRec1 =
  __inline_me (\ (@ a_ah2) _ (eta_B1 :: TestEq.Rec a_ah2) -> eta_B1)

TestEq.$fRepresentableVar2 :: forall a_XnL.
                              (TestEq.Representable a_XnL) =>
                              TestEq.Rep (TestEq.Var a_XnL) -> TestEq.Rep (TestEq.Var a_XnL)
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableVar2 =
  __inline_me (\ (@ a_XnL)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.Var a_XnL)) ->
                 eta_B1)

TestEq.$fRepresentableVar1 :: forall a_ah4.
                              (TestEq.Representable a_ah4) =>
                              TestEq.Var a_ah4 -> TestEq.Var a_ah4
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableVar1 =
  __inline_me (\ (@ a_ah4) _ (eta_B1 :: TestEq.Var a_ah4) -> eta_B1)

TestEq.$fRepresentableC2 :: forall a_XnF c_XnH.
                            (TestEq.Representable a_XnF) =>
                            TestEq.Rep (TestEq.C c_XnH a_XnF)
                            -> TestEq.Rep (TestEq.C c_XnH a_XnF)
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableC2 =
  __inline_me (\ (@ a_XnF)
                 (@ c_XnH)
                 _
                 (eta_B1 :: TestEq.Rep (TestEq.C c_XnH a_XnF)) ->
                 eta_B1)

TestEq.$fRepresentableC1 :: forall a_ah6 c_ah7.
                            (TestEq.Representable a_ah6) =>
                            TestEq.C c_ah7 a_ah6 -> TestEq.C c_ah7 a_ah6
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AS]
TestEq.$fRepresentableC1 =
  __inline_me (\ (@ a_ah6)
                 (@ c_ah7)
                 _
                 (eta_B1 :: TestEq.C c_ah7 a_ah6) ->
                 eta_B1)

TestEq.$fRepresentable:+:2 :: forall a_Xnz b_XnB.
                              (TestEq.Representable a_Xnz, TestEq.Representable b_XnB) =>
                              TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
                              -> TestEq.Rep (a_Xnz TestEq.:+: b_XnB)
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:+:2 =
  __inline_me (\ (@ a_Xnz)
                 (@ b_XnB)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xnz TestEq.:+: b_XnB)) ->
                 eta_B1)

TestEq.$fRepresentable:+:1 :: forall a_aha b_ahb.
                              (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                              (a_aha TestEq.:+: b_ahb) -> a_aha TestEq.:+: b_ahb
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:+:1 =
  __inline_me (\ (@ a_aha)
                 (@ b_ahb)
                 _
                 _
                 (eta_B1 :: a_aha TestEq.:+: b_ahb) ->
                 eta_B1)

TestEq.$fRepresentable:*:2 :: forall a_Xns b_Xnu.
                              (TestEq.Representable a_Xns, TestEq.Representable b_Xnu) =>
                              TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
                              -> TestEq.Rep (a_Xns TestEq.:*: b_Xnu)
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:*:2 =
  __inline_me (\ (@ a_Xns)
                 (@ b_Xnu)
                 _
                 _
                 (eta_B1 :: TestEq.Rep (a_Xns TestEq.:*: b_Xnu)) ->
                 eta_B1)

TestEq.$fRepresentable:*:1 :: forall a_ahe b_ahf.
                              (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                              (a_ahe TestEq.:*: b_ahf) -> a_ahe TestEq.:*: b_ahf
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AAS]
TestEq.$fRepresentable:*:1 =
  __inline_me (\ (@ a_ahe)
                 (@ b_ahf)
                 _
                 _
                 (eta_B1 :: a_ahe TestEq.:*: b_ahf) ->
                 eta_B1)

TestEq.logic11 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic11 = TestEq.Not TestEq.F

TestEq.logic13 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic13 = TestEq.Not TestEq.T

TestEq.logic16 :: GHC.Types.Char
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.logic16 = GHC.Types.C# 'x'

TestEq.logic15 :: [GHC.Types.Char]
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic15 =
  GHC.Types.:
    @ GHC.Types.Char TestEq.logic16 (GHC.Types.[] @ GHC.Types.Char)

TestEq.logic14 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic14 = TestEq.VarL TestEq.logic15

TestEq.logic12 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic12 = TestEq.Equiv TestEq.logic14 TestEq.logic13

TestEq.tree01 :: GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType m]
TestEq.tree01 = GHC.Types.I# 3

TestEq.$dmconIsRecord :: forall c_afy.
                         (TestEq.Constructor c_afy) =>
                         forall (t_afD :: * -> * -> *) a_afE.
                         t_afD c_afy a_afE -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.$dmconIsRecord =
  __inline_me (\ (@ c_afy) _ (@ t_aFS::* -> * -> *) (@ a_aFT) _ ->
                 GHC.Bool.False)

TestEq.$dmconFixity :: forall c_afy.
                       (TestEq.Constructor c_afy) =>
                       forall (t_afB :: * -> * -> *) a_afC.
                       t_afB c_afy a_afC -> TestEq.Fixity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.$dmconFixity =
  __inline_me (\ (@ c_afy) _ (@ t_aFL::* -> * -> *) (@ a_aFM) _ ->
                 TestEq.Prefix)

TestEq.$fRepresentableInt :: TestEq.Representable GHC.Types.Int
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableInt =
  TestEq.D:Representable
    @ GHC.Types.Int
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Int))
     `cast` (TestEq.Rep GHC.Types.Int -> TestEq.TFCo:R:RepInt
             :: (TestEq.Rep GHC.Types.Int -> TestEq.Rep GHC.Types.Int)
                  ~
                (TestEq.Rep GHC.Types.Int -> TestEq.R:RepInt)))
    ((GHC.Base.id @ GHC.Types.Int)
     `cast` (GHC.Types.Int -> sym TestEq.TFCo:R:RepInt
             :: (GHC.Types.Int -> TestEq.R:RepInt)
                  ~
                (GHC.Types.Int -> TestEq.Rep GHC.Types.Int)))

TestEq.$fRepresentableChar :: TestEq.Representable GHC.Types.Char
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableChar =
  TestEq.D:Representable
    @ GHC.Types.Char
    ((GHC.Base.id @ (TestEq.Rep GHC.Types.Char))
     `cast` (TestEq.Rep GHC.Types.Char -> TestEq.TFCo:R:RepChar
             :: (TestEq.Rep GHC.Types.Char -> TestEq.Rep GHC.Types.Char)
                  ~
                (TestEq.Rep GHC.Types.Char -> TestEq.R:RepChar)))
    ((GHC.Base.id @ GHC.Types.Char)
     `cast` (GHC.Types.Char -> sym TestEq.TFCo:R:RepChar
             :: (GHC.Types.Char -> TestEq.R:RepChar)
                  ~
                (GHC.Types.Char -> TestEq.Rep GHC.Types.Char)))

TestEq.$fRepresentableU :: TestEq.Representable TestEq.U
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableU =
  TestEq.D:Representable
    @ TestEq.U
    ((GHC.Base.id @ (TestEq.Rep TestEq.U))
     `cast` (TestEq.Rep TestEq.U -> TestEq.TFCo:R:RepU
             :: (TestEq.Rep TestEq.U -> TestEq.Rep TestEq.U)
                  ~
                (TestEq.Rep TestEq.U -> TestEq.R:RepU)))
    ((GHC.Base.id @ TestEq.U)
     `cast` (TestEq.U -> sym TestEq.TFCo:R:RepU
             :: (TestEq.U -> TestEq.R:RepU)
                  ~
                (TestEq.U -> TestEq.Rep TestEq.U)))

TestEq.$fRepresentable:*: :: forall a_ahe b_ahf.
                             (TestEq.Representable a_ahe, TestEq.Representable b_ahf) =>
                             TestEq.Representable (a_ahe TestEq.:*: b_ahf)
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType LLm]
TestEq.$fRepresentable:*: =
  __inline_me (\ (@ a_Xnx)
                 (@ b_Xnz)
                 ($dRepresentable_X1bW :: TestEq.Representable a_Xnx)
                 ($dRepresentable1_X1bY :: TestEq.Representable b_Xnz) ->
                 TestEq.D:Representable
                   @ (a_Xnx TestEq.:*: b_Xnz)
                   ((TestEq.$fRepresentable:*:2
                       @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable1_X1bY)
                    `cast` (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                            -> TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz
                            :: (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz))
                                 ~
                               (TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.R:Rep:*: a_Xnx b_Xnz)))
                   ((TestEq.$fRepresentable:*:1
                       @ a_Xnx @ b_Xnz $dRepresentable_X1bW $dRepresentable1_X1bY)
                    `cast` ((a_Xnx TestEq.:*: b_Xnz)
                            -> sym (TestEq.TFCo:R:Rep:*: a_Xnx b_Xnz)
                            :: ((a_Xnx TestEq.:*: b_Xnz) -> TestEq.R:Rep:*: a_Xnx b_Xnz)
                                 ~
                               ((a_Xnx TestEq.:*: b_Xnz)
                                -> TestEq.Rep (a_Xnx TestEq.:*: b_Xnz)))))

TestEq.$fRepresentable:+: :: forall a_aha b_ahb.
                             (TestEq.Representable a_aha, TestEq.Representable b_ahb) =>
                             TestEq.Representable (a_aha TestEq.:+: b_ahb)
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType LLm]
TestEq.$fRepresentable:+: =
  __inline_me (\ (@ a_XnE)
                 (@ b_XnG)
                 ($dRepresentable_X1bU :: TestEq.Representable a_XnE)
                 ($dRepresentable1_X1bW :: TestEq.Representable b_XnG) ->
                 TestEq.D:Representable
                   @ (a_XnE TestEq.:+: b_XnG)
                   ((TestEq.$fRepresentable:+:2
                       @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable1_X1bW)
                    `cast` (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                            -> TestEq.TFCo:R:Rep:+: a_XnE b_XnG
                            :: (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG))
                                 ~
                               (TestEq.Rep (a_XnE TestEq.:+: b_XnG)
                                -> TestEq.R:Rep:+: a_XnE b_XnG)))
                   ((TestEq.$fRepresentable:+:1
                       @ a_XnE @ b_XnG $dRepresentable_X1bU $dRepresentable1_X1bW)
                    `cast` ((a_XnE TestEq.:+: b_XnG)
                            -> sym (TestEq.TFCo:R:Rep:+: a_XnE b_XnG)
                            :: ((a_XnE TestEq.:+: b_XnG) -> TestEq.R:Rep:+: a_XnE b_XnG)
                                 ~
                               ((a_XnE TestEq.:+: b_XnG)
                                -> TestEq.Rep (a_XnE TestEq.:+: b_XnG)))))

TestEq.$fRepresentableC :: forall a_ah6 c_ah7.
                           (TestEq.Representable a_ah6) =>
                           TestEq.Representable (TestEq.C c_ah7 a_ah6)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fRepresentableC =
  __inline_me (\ (@ a_XnJ)
                 (@ c_XnL)
                 ($dRepresentable_X1iw :: TestEq.Representable a_XnJ) ->
                 TestEq.D:Representable
                   @ (TestEq.C c_XnL a_XnJ)
                   ((TestEq.$fRepresentableC2 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                            -> TestEq.TFCo:R:RepC c_XnL a_XnJ
                            :: (TestEq.Rep (TestEq.C c_XnL a_XnJ)
                                -> TestEq.Rep (TestEq.C c_XnL a_XnJ))
                                 ~
                               (TestEq.Rep (TestEq.C c_XnL a_XnJ) -> TestEq.R:RepC c_XnL a_XnJ)))
                   ((TestEq.$fRepresentableC1 @ a_XnJ @ c_XnL $dRepresentable_X1iw)
                    `cast` (TestEq.C c_XnL a_XnJ
                            -> sym (TestEq.TFCo:R:RepC c_XnL a_XnJ)
                            :: (TestEq.C c_XnL a_XnJ -> TestEq.R:RepC c_XnL a_XnJ)
                                 ~
                               (TestEq.C c_XnL a_XnJ -> TestEq.Rep (TestEq.C c_XnL a_XnJ)))))

TestEq.$fRepresentableVar :: forall a_ah4.
                             (TestEq.Representable a_ah4) =>
                             TestEq.Representable (TestEq.Var a_ah4)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fRepresentableVar =
  __inline_me (\ (@ a_XnO)
                 ($dRepresentable_X1bL :: TestEq.Representable a_XnO) ->
                 TestEq.D:Representable
                   @ (TestEq.Var a_XnO)
                   ((TestEq.$fRepresentableVar2 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.TFCo:R:RepVar a_XnO
                            :: (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.Rep (TestEq.Var a_XnO))
                                 ~
                               (TestEq.Rep (TestEq.Var a_XnO) -> TestEq.R:RepVar a_XnO)))
                   ((TestEq.$fRepresentableVar1 @ a_XnO $dRepresentable_X1bL)
                    `cast` (TestEq.Var a_XnO -> sym (TestEq.TFCo:R:RepVar a_XnO)
                            :: (TestEq.Var a_XnO -> TestEq.R:RepVar a_XnO)
                                 ~
                               (TestEq.Var a_XnO -> TestEq.Rep (TestEq.Var a_XnO)))))

TestEq.$fRepresentableRec :: forall a_ah2.
                             (TestEq.Representable a_ah2) =>
                             TestEq.Representable (TestEq.Rec a_ah2)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fRepresentableRec =
  __inline_me (\ (@ a_XnT)
                 ($dRepresentable_X1bF :: TestEq.Representable a_XnT) ->
                 TestEq.D:Representable
                   @ (TestEq.Rec a_XnT)
                   ((TestEq.$fRepresentableRec2 @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.TFCo:R:RepRec a_XnT
                            :: (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.Rep (TestEq.Rec a_XnT))
                                 ~
                               (TestEq.Rep (TestEq.Rec a_XnT) -> TestEq.R:RepRec a_XnT)))
                   ((TestEq.$fRepresentableRec1 @ a_XnT $dRepresentable_X1bF)
                    `cast` (TestEq.Rec a_XnT -> sym (TestEq.TFCo:R:RepRec a_XnT)
                            :: (TestEq.Rec a_XnT -> TestEq.R:RepRec a_XnT)
                                 ~
                               (TestEq.Rec a_XnT -> TestEq.Rep (TestEq.Rec a_XnT)))))

TestEq.$fEqInt :: TestEq.Eq GHC.Types.Int
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType U(L)U(L)]
TestEq.$fEqInt =
  __inline_me (GHC.Base.eqInt
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Int)
                       :: (GHC.Types.Int -> GHC.Types.Int -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Int))

TestEq.$fEqChar :: TestEq.Eq GHC.Types.Char
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType U(L)U(L)]
TestEq.$fEqChar =
  __inline_me (GHC.Base.$fEqChar_==
               `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
                       :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                            ~
                          TestEq.T:Eq GHC.Types.Char))

Rec {
TestEq.eqTree :: TestEq.Tree GHC.Types.Int
                 -> TestEq.Tree GHC.Types.Int
                 -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.eqTree =
  \ (x_agA :: TestEq.Tree GHC.Types.Int)
    (y_agB :: TestEq.Tree GHC.Types.Int) ->
    case x_agA of _ {
      TestEq.Leaf ->
        case y_agB of _ {
          TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
        };
      TestEq.Bin x1_afO l_afP r_afQ ->
        case y_agB of _ {
          TestEq.Leaf -> GHC.Bool.False;
          TestEq.Bin x2_Xov l1_Xox r1_Xoz ->
            case x1_afO of _ { GHC.Types.I# x3_a1yv ->
            case x2_Xov of _ { GHC.Types.I# y1_a1yz ->
            case GHC.Prim.==# x3_a1yv y1_a1yz of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True ->
                case TestEq.eqTree l_afP l1_Xox of _ {
                  GHC.Bool.False -> GHC.Bool.False;
                  GHC.Bool.True -> TestEq.eqTree r_afQ r1_Xoz
                }
            }
            }
            }
        }
    }
end Rec }

TestEq.$fEqTree :: TestEq.Eq (TestEq.Tree GHC.Types.Int)
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqTree =
  TestEq.eqTree
  `cast` (sym (TestEq.NTCo:T:Eq (TestEq.Tree GHC.Types.Int))
          :: (TestEq.Tree GHC.Types.Int
              -> TestEq.Tree GHC.Types.Int
              -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq (TestEq.Tree GHC.Types.Int))

TestEq.eq :: forall a_afN.
             (TestEq.Representable a_afN, TestEq.Eq (TestEq.Rep a_afN)) =>
             a_afN -> a_afN -> GHC.Bool.Bool
GblId
[Arity 4
 NoCafRefs
 Str: DmdType LC(C(S))LL]
TestEq.eq =
  \ (@ a_azl)
    ($dRepresentable_azt :: TestEq.Representable a_azl)
    ($dEq_azu :: TestEq.Eq (TestEq.Rep a_azl))
    (eta_B2 :: a_azl)
    (eta1_B1 :: a_azl) ->
    ($dEq_azu
     `cast` (TestEq.NTCo:T:Eq (TestEq.Rep a_azl)
             :: TestEq.T:Eq (TestEq.Rep a_azl)
                  ~
                (TestEq.Rep a_azl -> TestEq.Rep a_azl -> GHC.Bool.Bool)))
      (case $dRepresentable_azt
       of _ { TestEq.D:Representable _ tpl2_B3 ->
       tpl2_B3 eta_B2
       })
      (case $dRepresentable_azt
       of _ { TestEq.D:Representable _ tpl2_B3 ->
       tpl2_B3 eta1_B1
       })

TestEq.eqRec :: forall t_azF.
                (TestEq.Eq t_azF) =>
                TestEq.Rec t_azF -> TestEq.Rec t_azF -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.eqRec =
  __inline_me (\ (@ t_azF)
                 ($dEq_azL :: TestEq.Eq t_azF)
                 (ds_d17K :: TestEq.Rec t_azF)
                 (ds1_d17L :: TestEq.Rec t_azF) ->
                 case ds_d17K of _ { TestEq.Rec x_agu ->
                 case ds1_d17L of _ { TestEq.Rec x'_agv ->
                 ($dEq_azL
                  `cast` (TestEq.NTCo:T:Eq t_azF
                          :: TestEq.T:Eq t_azF ~ (t_azF -> t_azF -> GHC.Bool.Bool)))
                   x_agu x'_agv
                 }
                 })

TestEq.$fEqRec :: forall a_agD.
                  (TestEq.Eq a_agD) =>
                  TestEq.Eq (TestEq.Rec a_agD)
GblId[DFunId]
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqRec =
  __inline_me (TestEq.eqRec
               `cast` (forall a_agD.
                       (TestEq.Eq a_agD) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Rec a_agD))
                       :: (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.Rec a_agD -> TestEq.Rec a_agD -> GHC.Bool.Bool)
                            ~
                          (forall a_agD.
                           (TestEq.Eq a_agD) =>
                           TestEq.T:Eq (TestEq.Rec a_agD))))

TestEq.eqVar :: forall t_azR.
                (TestEq.Eq t_azR) =>
                TestEq.Var t_azR -> TestEq.Var t_azR -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.eqVar =
  __inline_me (\ (@ t_azR)
                 ($dEq_azX :: TestEq.Eq t_azR)
                 (ds_d17O :: TestEq.Var t_azR)
                 (ds1_d17P :: TestEq.Var t_azR) ->
                 case ds_d17O of _ { TestEq.Var x_ags ->
                 case ds1_d17P of _ { TestEq.Var x'_agt ->
                 ($dEq_azX
                  `cast` (TestEq.NTCo:T:Eq t_azR
                          :: TestEq.T:Eq t_azR ~ (t_azR -> t_azR -> GHC.Bool.Bool)))
                   x_ags x'_agt
                 }
                 })

TestEq.$fEqVar :: forall a_agE.
                  (TestEq.Eq a_agE) =>
                  TestEq.Eq (TestEq.Var a_agE)
GblId[DFunId]
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqVar =
  __inline_me (TestEq.eqVar
               `cast` (forall a_agE.
                       (TestEq.Eq a_agE) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.Var a_agE))
                       :: (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.Var a_agE -> TestEq.Var a_agE -> GHC.Bool.Bool)
                            ~
                          (forall a_agE.
                           (TestEq.Eq a_agE) =>
                           TestEq.T:Eq (TestEq.Var a_agE))))

TestEq.eqC :: forall t_aA2 t1_aA4 t2_aA5.
              (TestEq.Eq t1_aA4) =>
              TestEq.C t_aA2 t1_aA4 -> TestEq.C t2_aA5 t1_aA4 -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.eqC =
  __inline_me (\ (@ t_aA2)
                 (@ t1_aA4)
                 (@ t2_aA5)
                 ($dEq_aAb :: TestEq.Eq t1_aA4)
                 (ds_d17S :: TestEq.C t_aA2 t1_aA4)
                 (ds1_d17T :: TestEq.C t2_aA5 t1_aA4) ->
                 case ds_d17S of _ { TestEq.C a1_agq ->
                 case ds1_d17T of _ { TestEq.C a'_agr ->
                 ($dEq_aAb
                  `cast` (TestEq.NTCo:T:Eq t1_aA4
                          :: TestEq.T:Eq t1_aA4 ~ (t1_aA4 -> t1_aA4 -> GHC.Bool.Bool)))
                   a1_agq a'_agr
                 }
                 })

TestEq.eqTimes :: forall t_aAi t1_aAj.
                  (TestEq.Eq t_aAi, TestEq.Eq t1_aAj) =>
                  (t_aAi TestEq.:*: t1_aAj)
                  -> (t_aAi TestEq.:*: t1_aAj)
                  -> GHC.Bool.Bool
GblId
[Arity 4
 NoCafRefs
 Str: DmdType C(C(S))LU(LL)U(LL)]
TestEq.eqTimes =
  __inline_me (\ (@ t_aAi)
                 (@ t1_aAj)
                 ($dEq_aAt :: TestEq.Eq t_aAi)
                 ($dEq1_aAu :: TestEq.Eq t1_aAj)
                 (ds_d17W :: t_aAi TestEq.:*: t1_aAj)
                 (ds1_d17X :: t_aAi TestEq.:*: t1_aAj) ->
                 case ds_d17W of _ { TestEq.:*: a1_agi b_agj ->
                 case ds1_d17X of _ { TestEq.:*: a'_agk b'_agl ->
                 case ($dEq_aAt
                       `cast` (TestEq.NTCo:T:Eq t_aAi
                               :: TestEq.T:Eq t_aAi ~ (t_aAi -> t_aAi -> GHC.Bool.Bool)))
                        a1_agi a'_agk
                 of _ {
                   GHC.Bool.False -> GHC.Bool.False;
                   GHC.Bool.True ->
                     ($dEq1_aAu
                      `cast` (TestEq.NTCo:T:Eq t1_aAj
                              :: TestEq.T:Eq t1_aAj ~ (t1_aAj -> t1_aAj -> GHC.Bool.Bool)))
                       b_agj b'_agl
                 }
                 }
                 })

TestEq.$fEq:*: :: forall a_agH b_agI.
                  (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                  TestEq.Eq (a_agH TestEq.:*: b_agI)
GblId[DFunId]
[Arity 4
 NoCafRefs
 Str: DmdType C(C(S))LU(LL)U(LL)]
TestEq.$fEq:*: =
  __inline_me (TestEq.eqTimes
               `cast` (forall a_agH b_agI.
                       (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                       sym (TestEq.NTCo:T:Eq (a_agH TestEq.:*: b_agI))
                       :: (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           (a_agH TestEq.:*: b_agI)
                           -> (a_agH TestEq.:*: b_agI)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agH b_agI.
                           (TestEq.Eq a_agH, TestEq.Eq b_agI) =>
                           TestEq.T:Eq (a_agH TestEq.:*: b_agI))))

TestEq.eqPlus :: forall t_aAB t1_aAH.
                 (TestEq.Eq t_aAB, TestEq.Eq t1_aAH) =>
                 (t_aAB TestEq.:+: t1_aAH)
                 -> (t_aAB TestEq.:+: t1_aAH)
                 -> GHC.Bool.Bool
GblId
[Arity 4
 NoCafRefs
 Str: DmdType LLSS]
TestEq.eqPlus =
  \ (@ t_aAB)
    (@ t1_aAH)
    ($dEq_aAM :: TestEq.Eq t_aAB)
    ($dEq1_aAN :: TestEq.Eq t1_aAH)
    (ds_d180 :: t_aAB TestEq.:+: t1_aAH)
    (ds1_d181 :: t_aAB TestEq.:+: t1_aAH) ->
    case ds_d180 of _ {
      TestEq.L x_age ->
        case ds1_d181 of _ {
          TestEq.L x'_agf ->
            ($dEq_aAM
             `cast` (TestEq.NTCo:T:Eq t_aAB
                     :: TestEq.T:Eq t_aAB ~ (t_aAB -> t_aAB -> GHC.Bool.Bool)))
              x_age x'_agf;
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x_agg ->
        case ds1_d181 of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh ->
            ($dEq1_aAN
             `cast` (TestEq.NTCo:T:Eq t1_aAH
                     :: TestEq.T:Eq t1_aAH ~ (t1_aAH -> t1_aAH -> GHC.Bool.Bool)))
              x_agg x'_agh
        }
    }

TestEq.$fEq:+: :: forall a_agJ b_agK.
                  (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                  TestEq.Eq (a_agJ TestEq.:+: b_agK)
GblId[DFunId]
[Arity 4
 NoCafRefs
 Str: DmdType LLSS]
TestEq.$fEq:+: =
  __inline_me (TestEq.eqPlus
               `cast` (forall a_agJ b_agK.
                       (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                       sym (TestEq.NTCo:T:Eq (a_agJ TestEq.:+: b_agK))
                       :: (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           (a_agJ TestEq.:+: b_agK)
                           -> (a_agJ TestEq.:+: b_agK)
                           -> GHC.Bool.Bool)
                            ~
                          (forall a_agJ b_agK.
                           (TestEq.Eq a_agJ, TestEq.Eq b_agK) =>
                           TestEq.T:Eq (a_agJ TestEq.:+: b_agK))))

TestEq.eqU :: forall t_aAS t1_aAT. t_aAS -> t1_aAT -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.eqU =
  __inline_me (\ (@ t_aAS) (@ t1_aAT) _ _ -> GHC.Bool.True)

TestEq.$fEqU :: TestEq.Eq TestEq.U
GblId[DFunId]
[Arity 2
 NoCafRefs
 Str: DmdType AA]
TestEq.$fEqU =
  __inline_me ((TestEq.eqU @ TestEq.U @ TestEq.U)
               `cast` (sym (TestEq.NTCo:T:Eq TestEq.U)
                       :: (TestEq.U -> TestEq.U -> GHC.Bool.Bool) ~ TestEq.T:Eq TestEq.U))

TestEq.toLogic :: forall t_aB0
                         t1_aB6
                         t2_aBa
                         t3_aBe
                         t4_aBk
                         t5_aBu
                         t6_aBE
                         t7_aBM.
                  (TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                   TestEq.:+: (TestEq.C t1_aB6 TestEq.U
                               TestEq.:+: (TestEq.C t2_aBa TestEq.U
                                           TestEq.:+: (TestEq.C t3_aBe (TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: (TestEq.C
                                                                     t4_aBk
                                                                     (TestEq.Rec TestEq.Logic
                                                                      TestEq.:*: TestEq.Rec
                                                                                   TestEq.Logic)
                                                                   TestEq.:+: (TestEq.C
                                                                                 t5_aBu
                                                                                 (TestEq.Rec
                                                                                    TestEq.Logic
                                                                                  TestEq.:*: TestEq.Rec
                                                                                               TestEq.Logic)
                                                                               TestEq.:+: (TestEq.C
                                                                                             t6_aBE
                                                                                             (TestEq.Rec
                                                                                                TestEq.Logic
                                                                                              TestEq.:*: TestEq.Rec
                                                                                                           TestEq.Logic)
                                                                                           TestEq.:+: TestEq.C
                                                                                                        t7_aBM
                                                                                                        (TestEq.Rec
                                                                                                           TestEq.Logic
                                                                                                         TestEq.:*: TestEq.Rec
                                                                                                                      TestEq.Logic))))))))
                  -> TestEq.Logic
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.toLogic =
  \ (@ t_aB0)
    (@ t1_aB6)
    (@ t2_aBa)
    (@ t3_aBe)
    (@ t4_aBk)
    (@ t5_aBu)
    (@ t6_aBE)
    (@ t7_aBM)
    (ds_d18a :: TestEq.C t_aB0 (TestEq.Var GHC.Base.String)
                TestEq.:+: (TestEq.C t1_aB6 TestEq.U
                            TestEq.:+: (TestEq.C t2_aBa TestEq.U
                                        TestEq.:+: (TestEq.C t3_aBe (TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  t4_aBk
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: (TestEq.C
                                                                              t5_aBu
                                                                              (TestEq.Rec
                                                                                 TestEq.Logic
                                                                               TestEq.:*: TestEq.Rec
                                                                                            TestEq.Logic)
                                                                            TestEq.:+: (TestEq.C
                                                                                          t6_aBE
                                                                                          (TestEq.Rec
                                                                                             TestEq.Logic
                                                                                           TestEq.:*: TestEq.Rec
                                                                                                        TestEq.Logic)
                                                                                        TestEq.:+: TestEq.C
                                                                                                     t7_aBM
                                                                                                     (TestEq.Rec
                                                                                                        TestEq.Logic
                                                                                                      TestEq.:*: TestEq.Rec
                                                                                                                   TestEq.Logic)))))))) ->
    case ds_d18a of _ {
      TestEq.L ds1_d18D ->
        case ds1_d18D of _ { TestEq.C ds2_d18E ->
        case ds2_d18E of _ { TestEq.Var f0_ag4 -> TestEq.VarL f0_ag4 }
        };
      TestEq.R ds1_d18b ->
        case ds1_d18b of _ {
          TestEq.L ds2_d18B ->
            case ds2_d18B of _ { TestEq.C ds3_d18C ->
            case ds3_d18C of _ { TestEq.U -> TestEq.T }
            };
          TestEq.R ds2_d18c ->
            case ds2_d18c of _ {
              TestEq.L ds3_d18z ->
                case ds3_d18z of _ { TestEq.C ds4_d18A ->
                case ds4_d18A of _ { TestEq.U -> TestEq.F }
                };
              TestEq.R ds3_d18d ->
                case ds3_d18d of _ {
                  TestEq.L ds4_d18x ->
                    case ds4_d18x of _ { TestEq.C ds5_d18y ->
                    case ds5_d18y of _ { TestEq.Rec f0_ag5 -> TestEq.Not f0_ag5 }
                    };
                  TestEq.R ds4_d18e ->
                    case ds4_d18e of _ {
                      TestEq.L ds5_d18t ->
                        case ds5_d18t of _ { TestEq.C ds6_d18u ->
                        case ds6_d18u of _ { TestEq.:*: ds7_d18v ds8_d18w ->
                        case ds7_d18v of _ { TestEq.Rec f0_ag6 ->
                        case ds8_d18w of _ { TestEq.Rec f1_ag7 ->
                        TestEq.Impl f0_ag6 f1_ag7
                        }
                        }
                        }
                        };
                      TestEq.R ds5_d18f ->
                        case ds5_d18f of _ {
                          TestEq.L ds6_d18p ->
                            case ds6_d18p of _ { TestEq.C ds7_d18q ->
                            case ds7_d18q of _ { TestEq.:*: ds8_d18r ds9_d18s ->
                            case ds8_d18r of _ { TestEq.Rec f0_ag8 ->
                            case ds9_d18s of _ { TestEq.Rec f1_ag9 ->
                            TestEq.Equiv f0_ag8 f1_ag9
                            }
                            }
                            }
                            };
                          TestEq.R ds6_d18g ->
                            case ds6_d18g of _ {
                              TestEq.L ds7_d18l ->
                                case ds7_d18l of _ { TestEq.C ds8_d18m ->
                                case ds8_d18m of _ { TestEq.:*: ds9_d18n ds10_d18o ->
                                case ds9_d18n of _ { TestEq.Rec f0_aga ->
                                case ds10_d18o of _ { TestEq.Rec f1_agb ->
                                TestEq.Conj f0_aga f1_agb
                                }
                                }
                                }
                                };
                              TestEq.R ds7_d18h ->
                                case ds7_d18h of _ { TestEq.C ds8_d18i ->
                                case ds8_d18i of _ { TestEq.:*: ds9_d18j ds10_d18k ->
                                case ds9_d18j of _ { TestEq.Rec f0_agc ->
                                case ds10_d18k of _ { TestEq.Rec f1_agd ->
                                TestEq.Disj f0_agc f1_agd
                                }
                                }
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

TestEq.fromLogic :: forall c_aC3
                           c1_aCb
                           c2_aCl
                           c3_aCz
                           c4_aCT
                           c5_aDg
                           c6_aDG
                           c7_aE7.
                    TestEq.Logic
                    -> TestEq.C c_aC3 (TestEq.Var GHC.Base.String)
                       TestEq.:+: (TestEq.C c1_aCb TestEq.U
                                   TestEq.:+: (TestEq.C c2_aCl TestEq.U
                                               TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                                           TestEq.:+: (TestEq.C
                                                                         c4_aCT
                                                                         (TestEq.Rec TestEq.Logic
                                                                          TestEq.:*: TestEq.Rec
                                                                                       TestEq.Logic)
                                                                       TestEq.:+: (TestEq.C
                                                                                     c5_aDg
                                                                                     (TestEq.Rec
                                                                                        TestEq.Logic
                                                                                      TestEq.:*: TestEq.Rec
                                                                                                   TestEq.Logic)
                                                                                   TestEq.:+: (TestEq.C
                                                                                                 c6_aDG
                                                                                                 (TestEq.Rec
                                                                                                    TestEq.Logic
                                                                                                  TestEq.:*: TestEq.Rec
                                                                                                               TestEq.Logic)
                                                                                               TestEq.:+: TestEq.C
                                                                                                            c7_aE7
                                                                                                            (TestEq.Rec
                                                                                                               TestEq.Logic
                                                                                                             TestEq.:*: TestEq.Rec
                                                                                                                          TestEq.Logic)))))))
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.fromLogic =
  \ (@ c_aC3)
    (@ c1_aCb)
    (@ c2_aCl)
    (@ c3_aCz)
    (@ c4_aCT)
    (@ c5_aDg)
    (@ c6_aDG)
    (@ c7_aE7)
    (ds_d19h :: TestEq.Logic) ->
    case ds_d19h of _ {
      TestEq.VarL f0_afU ->
        TestEq.L
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c1_aCb TestEq.U
             TestEq.:+: (TestEq.C c2_aCl TestEq.U
                         TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c4_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c5_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c6_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c7_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.C
             @ c_aC3
             @ (TestEq.Var GHC.Base.String)
             (TestEq.Var @ GHC.Base.String f0_afU));
      TestEq.T ->
        TestEq.fromLogic5
          @ c_aC3
          @ c1_aCb
          @ c2_aCl
          @ c3_aCz
          @ c4_aCT
          @ c5_aDg
          @ c6_aDG
          @ c7_aE7;
      TestEq.F ->
        TestEq.fromLogic1
          @ c_aC3
          @ c1_aCb
          @ c2_aCl
          @ c3_aCz
          @ c4_aCT
          @ c5_aDg
          @ c6_aDG
          @ c7_aE7;
      TestEq.Not f0_afV ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c1_aCb TestEq.U
             TestEq.:+: (TestEq.C c2_aCl TestEq.U
                         TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c4_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c5_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c6_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c7_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c1_aCb TestEq.U)
             @ (TestEq.C c2_aCl TestEq.U
                TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c4_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c5_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c6_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c7_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c2_aCl TestEq.U)
                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c5_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c6_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c7_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.L
                   @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c5_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c6_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c7_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.C
                      @ c3_aCz
                      @ (TestEq.Rec TestEq.Logic)
                      (TestEq.Rec @ TestEq.Logic f0_afV)))));
      TestEq.Impl f0_afW f1_afX ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c1_aCb TestEq.U
             TestEq.:+: (TestEq.C c2_aCl TestEq.U
                         TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c4_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c5_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c6_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c7_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c1_aCb TestEq.U)
             @ (TestEq.C c2_aCl TestEq.U
                TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c4_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c5_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c6_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c7_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c2_aCl TestEq.U)
                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c5_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c6_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c7_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c5_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c6_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c7_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.L
                      @ (TestEq.C
                           c4_aCT
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c5_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c6_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c7_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.C
                         @ c4_aCT
                         @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         (TestEq.:*:
                            @ (TestEq.Rec TestEq.Logic)
                            @ (TestEq.Rec TestEq.Logic)
                            (TestEq.Rec @ TestEq.Logic f0_afW)
                            (TestEq.Rec @ TestEq.Logic f1_afX)))))));
      TestEq.Equiv f0_afY f1_afZ ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c1_aCb TestEq.U
             TestEq.:+: (TestEq.C c2_aCl TestEq.U
                         TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c4_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c5_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c6_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c7_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c1_aCb TestEq.U)
             @ (TestEq.C c2_aCl TestEq.U
                TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c4_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c5_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c6_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c7_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c2_aCl TestEq.U)
                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c5_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c6_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c7_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c5_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c6_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c7_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c4_aCT
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c5_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c6_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c7_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.L
                         @ (TestEq.C
                              c5_aDg
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c6_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c7_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.C
                            @ c5_aDg
                            @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            (TestEq.:*:
                               @ (TestEq.Rec TestEq.Logic)
                               @ (TestEq.Rec TestEq.Logic)
                               (TestEq.Rec @ TestEq.Logic f0_afY)
                               (TestEq.Rec @ TestEq.Logic f1_afZ))))))));
      TestEq.Conj f0_ag0 f1_ag1 ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c1_aCb TestEq.U
             TestEq.:+: (TestEq.C c2_aCl TestEq.U
                         TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c4_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c5_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c6_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c7_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c1_aCb TestEq.U)
             @ (TestEq.C c2_aCl TestEq.U
                TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c4_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c5_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c6_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c7_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c2_aCl TestEq.U)
                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c5_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c6_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c7_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c5_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c6_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c7_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c4_aCT
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c5_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c6_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c7_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.R
                         @ (TestEq.C
                              c5_aDg
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c6_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c7_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.L
                            @ (TestEq.C
                                 c6_aDG
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            @ (TestEq.C
                                 c7_aE7
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            (TestEq.C
                               @ c6_aDG
                               @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               (TestEq.:*:
                                  @ (TestEq.Rec TestEq.Logic)
                                  @ (TestEq.Rec TestEq.Logic)
                                  (TestEq.Rec @ TestEq.Logic f0_ag0)
                                  (TestEq.Rec @ TestEq.Logic f1_ag1)))))))));
      TestEq.Disj f0_ag2 f1_ag3 ->
        TestEq.R
          @ (TestEq.C c_aC3 (TestEq.Var GHC.Base.String))
          @ (TestEq.C c1_aCb TestEq.U
             TestEq.:+: (TestEq.C c2_aCl TestEq.U
                         TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                                     TestEq.:+: (TestEq.C
                                                   c4_aCT
                                                   (TestEq.Rec TestEq.Logic
                                                    TestEq.:*: TestEq.Rec TestEq.Logic)
                                                 TestEq.:+: (TestEq.C
                                                               c5_aDg
                                                               (TestEq.Rec TestEq.Logic
                                                                TestEq.:*: TestEq.Rec TestEq.Logic)
                                                             TestEq.:+: (TestEq.C
                                                                           c6_aDG
                                                                           (TestEq.Rec TestEq.Logic
                                                                            TestEq.:*: TestEq.Rec
                                                                                         TestEq.Logic)
                                                                         TestEq.:+: TestEq.C
                                                                                      c7_aE7
                                                                                      (TestEq.Rec
                                                                                         TestEq.Logic
                                                                                       TestEq.:*: TestEq.Rec
                                                                                                    TestEq.Logic)))))))
          (TestEq.R
             @ (TestEq.C c1_aCb TestEq.U)
             @ (TestEq.C c2_aCl TestEq.U
                TestEq.:+: (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                            TestEq.:+: (TestEq.C
                                          c4_aCT
                                          (TestEq.Rec TestEq.Logic
                                           TestEq.:*: TestEq.Rec TestEq.Logic)
                                        TestEq.:+: (TestEq.C
                                                      c5_aDg
                                                      (TestEq.Rec TestEq.Logic
                                                       TestEq.:*: TestEq.Rec TestEq.Logic)
                                                    TestEq.:+: (TestEq.C
                                                                  c6_aDG
                                                                  (TestEq.Rec TestEq.Logic
                                                                   TestEq.:*: TestEq.Rec
                                                                                TestEq.Logic)
                                                                TestEq.:+: TestEq.C
                                                                             c7_aE7
                                                                             (TestEq.Rec
                                                                                TestEq.Logic
                                                                              TestEq.:*: TestEq.Rec
                                                                                           TestEq.Logic))))))
             (TestEq.R
                @ (TestEq.C c2_aCl TestEq.U)
                @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic)
                   TestEq.:+: (TestEq.C
                                 c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               TestEq.:+: (TestEq.C
                                             c5_aDg
                                             (TestEq.Rec TestEq.Logic
                                              TestEq.:*: TestEq.Rec TestEq.Logic)
                                           TestEq.:+: (TestEq.C
                                                         c6_aDG
                                                         (TestEq.Rec TestEq.Logic
                                                          TestEq.:*: TestEq.Rec TestEq.Logic)
                                                       TestEq.:+: TestEq.C
                                                                    c7_aE7
                                                                    (TestEq.Rec TestEq.Logic
                                                                     TestEq.:*: TestEq.Rec
                                                                                  TestEq.Logic)))))
                (TestEq.R
                   @ (TestEq.C c3_aCz (TestEq.Rec TestEq.Logic))
                   @ (TestEq.C
                        c4_aCT (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                      TestEq.:+: (TestEq.C
                                    c5_aDg
                                    (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                  TestEq.:+: (TestEq.C
                                                c6_aDG
                                                (TestEq.Rec TestEq.Logic
                                                 TestEq.:*: TestEq.Rec TestEq.Logic)
                                              TestEq.:+: TestEq.C
                                                           c7_aE7
                                                           (TestEq.Rec TestEq.Logic
                                                            TestEq.:*: TestEq.Rec TestEq.Logic))))
                   (TestEq.R
                      @ (TestEq.C
                           c4_aCT
                           (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                      @ (TestEq.C
                           c5_aDg (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                         TestEq.:+: (TestEq.C
                                       c6_aDG
                                       (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                                     TestEq.:+: TestEq.C
                                                  c7_aE7
                                                  (TestEq.Rec TestEq.Logic
                                                   TestEq.:*: TestEq.Rec TestEq.Logic)))
                      (TestEq.R
                         @ (TestEq.C
                              c5_aDg
                              (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                         @ (TestEq.C
                              c6_aDG (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                            TestEq.:+: TestEq.C
                                         c7_aE7
                                         (TestEq.Rec TestEq.Logic
                                          TestEq.:*: TestEq.Rec TestEq.Logic))
                         (TestEq.R
                            @ (TestEq.C
                                 c6_aDG
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            @ (TestEq.C
                                 c7_aE7
                                 (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic))
                            (TestEq.C
                               @ c7_aE7
                               @ (TestEq.Rec TestEq.Logic TestEq.:*: TestEq.Rec TestEq.Logic)
                               (TestEq.:*:
                                  @ (TestEq.Rec TestEq.Logic)
                                  @ (TestEq.Rec TestEq.Logic)
                                  (TestEq.Rec @ TestEq.Logic f0_ag2)
                                  (TestEq.Rec @ TestEq.Logic f1_ag3)))))))))
    }

TestEq.$fRepresentableLogic :: TestEq.Representable TestEq.Logic
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableLogic =
  TestEq.D:Representable
    @ TestEq.Logic
    ((TestEq.toLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (sym TestEq.TFCo:R:RepLogic -> TestEq.Logic
             :: (TestEq.R:RepLogic -> TestEq.Logic)
                  ~
                (TestEq.Rep TestEq.Logic -> TestEq.Logic)))
    ((TestEq.fromLogic
        @ TestEq.Logic_Var_
        @ TestEq.Logic_T_
        @ TestEq.Logic_F_
        @ TestEq.Logic_Not_
        @ TestEq.Logic_Impl_
        @ TestEq.Logic_Equiv_
        @ TestEq.Logic_Conj_
        @ TestEq.Logic_Disj_)
     `cast` (TestEq.Logic -> sym TestEq.TFCo:R:RepLogic
             :: (TestEq.Logic -> TestEq.R:RepLogic)
                  ~
                (TestEq.Logic -> TestEq.Rep TestEq.Logic)))

TestEq.toTree :: forall t_aEI t1_aEN t2_aEV.
                 (TestEq.C t2_aEV TestEq.U
                  TestEq.:+: TestEq.C
                               t_aEI
                               (TestEq.Var t1_aEN
                                TestEq.:*: (TestEq.Rec (TestEq.Tree t1_aEN)
                                            TestEq.:*: TestEq.Rec (TestEq.Tree t1_aEN))))
                 -> TestEq.Tree t1_aEN
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.toTree =
  \ (@ t_aEI)
    (@ t1_aEN)
    (@ t2_aEV)
    (ds_d19q :: TestEq.C t2_aEV TestEq.U
                TestEq.:+: TestEq.C
                             t_aEI
                             (TestEq.Var t1_aEN
                              TestEq.:*: (TestEq.Rec (TestEq.Tree t1_aEN)
                                          TestEq.:*: TestEq.Rec (TestEq.Tree t1_aEN)))) ->
    case ds_d19q of _ {
      TestEq.L ds1_d19x ->
        case ds1_d19x of _ { TestEq.C ds2_d19y ->
        case ds2_d19y of _ { TestEq.U -> TestEq.Leaf @ t1_aEN }
        };
      TestEq.R ds1_d19r ->
        case ds1_d19r of _ { TestEq.C ds2_d19s ->
        case ds2_d19s of _ { TestEq.:*: ds3_d19t ds4_d19u ->
        case ds3_d19t of _ { TestEq.Var x_afR ->
        case ds4_d19u of _ { TestEq.:*: ds5_d19v ds6_d19w ->
        case ds5_d19v of _ { TestEq.Rec l_afS ->
        case ds6_d19w of _ { TestEq.Rec r_afT ->
        TestEq.Bin @ t1_aEN x_afR l_afS r_afT
        }
        }
        }
        }
        }
        }
    }

TestEq.fromTree :: forall t_aF3 c_aFf c1_aFl.
                   TestEq.Tree t_aF3
                   -> TestEq.C c1_aFl TestEq.U
                      TestEq.:+: TestEq.C
                                   c_aFf
                                   (TestEq.Var t_aF3
                                    TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                                                TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.fromTree =
  \ (@ t_aF3) (@ c_aFf) (@ c1_aFl) (ds_d19J :: TestEq.Tree t_aF3) ->
    case ds_d19J of _ {
      TestEq.Leaf -> TestEq.fromTree1 @ t_aF3 @ c_aFf @ c1_aFl;
      TestEq.Bin x_afO l_afP r_afQ ->
        TestEq.R
          @ (TestEq.C c1_aFl TestEq.U)
          @ (TestEq.C
               c_aFf
               (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))))
          (TestEq.C
             @ c_aFf
             @ (TestEq.Var t_aF3
                TestEq.:*: (TestEq.Rec (TestEq.Tree t_aF3)
                            TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3)))
             (TestEq.:*:
                @ (TestEq.Var t_aF3)
                @ (TestEq.Rec (TestEq.Tree t_aF3)
                   TestEq.:*: TestEq.Rec (TestEq.Tree t_aF3))
                (TestEq.Var @ t_aF3 x_afO)
                (TestEq.:*:
                   @ (TestEq.Rec (TestEq.Tree t_aF3))
                   @ (TestEq.Rec (TestEq.Tree t_aF3))
                   (TestEq.Rec @ (TestEq.Tree t_aF3) l_afP)
                   (TestEq.Rec @ (TestEq.Tree t_aF3) r_afQ))))
    }

TestEq.$fRepresentableTree1 :: forall a_agL.
                               TestEq.Tree a_agL
                               -> TestEq.C TestEq.Leaf TestEq.U
                                  TestEq.:+: TestEq.C
                                               TestEq.Bin
                                               (TestEq.Var a_agL
                                                TestEq.:*: (TestEq.Rec (TestEq.Tree a_agL)
                                                            TestEq.:*: TestEq.Rec
                                                                         (TestEq.Tree a_agL)))
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.$fRepresentableTree1 =
  \ (@ a_agL) (sub_a178 :: TestEq.Tree a_agL) ->
    TestEq.fromTree @ a_agL @ TestEq.Bin @ TestEq.Leaf sub_a178

TestEq.$fRepresentableTree :: forall a_agL.
                              TestEq.Representable (TestEq.Tree a_agL)
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentableTree =
  \ (@ a_XnZ) ->
    TestEq.D:Representable
      @ (TestEq.Tree a_XnZ)
      ((TestEq.toTree @ TestEq.Bin @ a_XnZ @ TestEq.Leaf)
       `cast` (sym (TestEq.TFCo:R:RepTree a_XnZ) -> TestEq.Tree a_XnZ
               :: (TestEq.R:RepTree a_XnZ -> TestEq.Tree a_XnZ)
                    ~
                  (TestEq.Rep (TestEq.Tree a_XnZ) -> TestEq.Tree a_XnZ)))
      ((TestEq.$fRepresentableTree1 @ a_XnZ)
       `cast` (TestEq.Tree a_XnZ -> sym (TestEq.TFCo:R:RepTree a_XnZ)
               :: (TestEq.Tree a_XnZ -> TestEq.R:RepTree a_XnZ)
                    ~
                  (TestEq.Tree a_XnZ -> TestEq.Rep (TestEq.Tree a_XnZ))))

TestEq.tree0 :: TestEq.Tree GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType]
TestEq.tree0 =
  TestEq.Bin
    @ GHC.Types.Int
    TestEq.tree01
    (TestEq.Leaf @ GHC.Types.Int)
    (TestEq.Leaf @ GHC.Types.Int)

TestEq.tree1 :: TestEq.Tree GHC.Types.Int
GblId
[NoCafRefs
 Str: DmdType]
TestEq.tree1 =
  TestEq.Bin
    @ GHC.Types.Int
    TestEq.$fConstructorList_Cons_2
    TestEq.tree0
    TestEq.tree0

TestEq.t1 :: GHC.Bool.Bool
GblId
[Str: DmdType]
TestEq.t1 = TestEq.eqTree TestEq.tree1 TestEq.tree1

TestEq.logic1 :: TestEq.Logic
GblId
[NoCafRefs
 Str: DmdType]
TestEq.logic1 = TestEq.Impl TestEq.logic12 TestEq.logic11

TestEq.$fRepresentable[]_from :: forall a_agN.
                                 [a_agN] -> TestEq.Rep [a_agN]
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.$fRepresentable[]_from =
  \ (@ a_agN) (ds_d1dc :: [a_agN]) ->
    case ds_d1dc of _ {
      [] ->
        (TestEq.$fRepresentable[]1 @ a_agN)
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN]);
      : a1_agO as_agP ->
        (TestEq.R
           @ (TestEq.C TestEq.List_Nil_ TestEq.U)
           @ (TestEq.C
                TestEq.List_Cons_ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN]))
           (TestEq.C
              @ TestEq.List_Cons_
              @ (TestEq.Var a_agN TestEq.:*: TestEq.Rec [a_agN])
              (TestEq.:*:
                 @ (TestEq.Var a_agN)
                 @ (TestEq.Rec [a_agN])
                 (TestEq.Var @ a_agN a1_agO)
                 (TestEq.Rec @ [a_agN] as_agP))))
        `cast` (sym (TestEq.TFCo:R:Rep[] a_agN)
                :: TestEq.R:Rep[] a_agN ~ TestEq.Rep [a_agN])
    }

TestEq.$fRepresentable[]_to :: forall a_agN.
                               TestEq.Rep [a_agN] -> [a_agN]
GblId
[Arity 1
 NoCafRefs
 Str: DmdType S]
TestEq.$fRepresentable[]_to =
  \ (@ a_agN) (ds_d1cW :: TestEq.Rep [a_agN]) ->
    case ds_d1cW
         `cast` (sym (sym (TestEq.TFCo:R:Rep[] a_agN))
                 :: TestEq.Rep [a_agN] ~ TestEq.R:Rep[] a_agN)
    of _ {
      TestEq.L ds1_d1d2 ->
        case ds1_d1d2 of _ { TestEq.C ds2_d1d3 ->
        case ds2_d1d3 of _ { TestEq.U -> GHC.Types.[] @ a_agN }
        };
      TestEq.R ds1_d1cY ->
        case ds1_d1cY of _ { TestEq.C ds2_d1cZ ->
        case ds2_d1cZ of _ { TestEq.:*: ds3_d1d0 ds4_d1d1 ->
        case ds3_d1d0 of _ { TestEq.Var a1_agZ ->
        case ds4_d1d1 of _ { TestEq.Rec as_ah0 ->
        GHC.Types.: @ a_agN a1_agZ as_ah0
        }
        }
        }
        }
    }

TestEq.$fRepresentable[] :: forall a_agN.
                            TestEq.Representable [a_agN]
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fRepresentable[] =
  \ (@ a_agN) ->
    TestEq.D:Representable
      @ [a_agN]
      (TestEq.$fRepresentable[]_to @ a_agN)
      (TestEq.$fRepresentable[]_from @ a_agN)

TestEq.$fConstructorList_Nil__conName :: forall (t_afz :: *
                                                          -> *
                                                          -> *)
                                                a_afA.
                                         t_afz TestEq.List_Nil_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorList_Nil__conName =
  __inline_me (\ (@ t_a14g::* -> * -> *) (@ a_a14h) _ ->
                 TestEq.$fConstructorList_Nil_1)

TestEq.$fConstructorList_Nil_ :: TestEq.Constructor
                                   TestEq.List_Nil_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorList_Nil_ =
  TestEq.D:Constructor
    @ TestEq.List_Nil_
    TestEq.$fConstructorList_Nil__conName
    TestEq.$dmconFixity_$s$dmconFixity
    TestEq.$dmconIsRecord_$s$dmconIsRecord11

TestEq.$fConstructorList_Cons__conFixity :: forall (t_afB :: *
                                                             -> *
                                                             -> *)
                                                   a_afC.
                                            t_afB TestEq.List_Cons_ a_afC -> TestEq.Fixity
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorList_Cons__conFixity =
  __inline_me (\ (@ t_a147::* -> * -> *) (@ a_a148) _ ->
                 TestEq.$fConstructorList_Cons_1)

TestEq.$fConstructorList_Cons__conName :: forall (t_afz :: *
                                                           -> *
                                                           -> *)
                                                 a_afA.
                                          t_afz TestEq.List_Cons_ a_afA -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorList_Cons__conName =
  __inline_me (\ (@ t_a141::* -> * -> *) (@ a_a142) _ ->
                 TestEq.$fConstructorList_Cons_3)

TestEq.$fConstructorList_Cons_ :: TestEq.Constructor
                                    TestEq.List_Cons_
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorList_Cons_ =
  TestEq.D:Constructor
    @ TestEq.List_Cons_
    TestEq.$fConstructorList_Cons__conName
    TestEq.$fConstructorList_Cons__conFixity
    TestEq.$dmconIsRecord_$s$dmconIsRecord10

TestEq.$fConstructorBin_conName :: forall (t_afz :: * -> * -> *)
                                          a_afA.
                                   t_afz TestEq.Bin a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorBin_conName =
  __inline_me (\ (@ t_a13Q::* -> * -> *) (@ a_a13R) _ ->
                 TestEq.$fConstructorBin1)

TestEq.$fConstructorBin :: TestEq.Constructor TestEq.Bin
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorBin =
  TestEq.D:Constructor
    @ TestEq.Bin
    TestEq.$fConstructorBin_conName
    TestEq.$dmconFixity_$s$dmconFixity1
    TestEq.$dmconIsRecord_$s$dmconIsRecord9

TestEq.$fConstructorLeaf_conName :: forall (t_afz :: * -> * -> *)
                                           a_afA.
                                    t_afz TestEq.Leaf a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLeaf_conName =
  __inline_me (\ (@ t_a13F::* -> * -> *) (@ a_a13G) _ ->
                 TestEq.$fConstructorLeaf1)

TestEq.$fConstructorLeaf :: TestEq.Constructor TestEq.Leaf
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLeaf =
  TestEq.D:Constructor
    @ TestEq.Leaf
    TestEq.$fConstructorLeaf_conName
    TestEq.$dmconFixity_$s$dmconFixity2
    TestEq.$dmconIsRecord_$s$dmconIsRecord8

TestEq.$fConstructorLogic_Var__conName :: forall (t_afz :: *
                                                           -> *
                                                           -> *)
                                                 a_afA.
                                          t_afz TestEq.Logic_Var_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Var__conName =
  __inline_me (\ (@ t_a13f::* -> * -> *) (@ a_a13g) _ ->
                 TestEq.$fConstructorLogic_Var_1)

TestEq.$fConstructorLogic_Var_ :: TestEq.Constructor
                                    TestEq.Logic_Var_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Var_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Var_
    TestEq.$fConstructorLogic_Var__conName
    TestEq.$dmconFixity_$s$dmconFixity3
    TestEq.$dmconIsRecord_$s$dmconIsRecord7

TestEq.$fConstructorLogic_Impl__conName :: forall (t_afz :: *
                                                            -> *
                                                            -> *)
                                                  a_afA.
                                           t_afz TestEq.Logic_Impl_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Impl__conName =
  __inline_me (\ (@ t_a134::* -> * -> *) (@ a_a135) _ ->
                 TestEq.$fConstructorLogic_Impl_1)

TestEq.$fConstructorLogic_Impl_ :: TestEq.Constructor
                                     TestEq.Logic_Impl_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Impl_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Impl_
    TestEq.$fConstructorLogic_Impl__conName
    TestEq.$dmconFixity_$s$dmconFixity4
    TestEq.$dmconIsRecord_$s$dmconIsRecord6

TestEq.$fConstructorLogic_Equiv__conName :: forall (t_afz :: *
                                                             -> *
                                                             -> *)
                                                   a_afA.
                                            t_afz TestEq.Logic_Equiv_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Equiv__conName =
  __inline_me (\ (@ t_a12T::* -> * -> *) (@ a_a12U) _ ->
                 TestEq.$fConstructorLogic_Equiv_1)

TestEq.$fConstructorLogic_Equiv_ :: TestEq.Constructor
                                      TestEq.Logic_Equiv_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Equiv_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Equiv_
    TestEq.$fConstructorLogic_Equiv__conName
    TestEq.$dmconFixity_$s$dmconFixity5
    TestEq.$dmconIsRecord_$s$dmconIsRecord5

TestEq.$fConstructorLogic_Conj__conName :: forall (t_afz :: *
                                                            -> *
                                                            -> *)
                                                  a_afA.
                                           t_afz TestEq.Logic_Conj_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Conj__conName =
  __inline_me (\ (@ t_a12I::* -> * -> *) (@ a_a12J) _ ->
                 TestEq.$fConstructorLogic_Conj_1)

TestEq.$fConstructorLogic_Conj_ :: TestEq.Constructor
                                     TestEq.Logic_Conj_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Conj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Conj_
    TestEq.$fConstructorLogic_Conj__conName
    TestEq.$dmconFixity_$s$dmconFixity6
    TestEq.$dmconIsRecord_$s$dmconIsRecord4

TestEq.$fConstructorLogic_Disj__conName :: forall (t_afz :: *
                                                            -> *
                                                            -> *)
                                                  a_afA.
                                           t_afz TestEq.Logic_Disj_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Disj__conName =
  __inline_me (\ (@ t_a12x::* -> * -> *) (@ a_a12y) _ ->
                 TestEq.$fConstructorLogic_Disj_1)

TestEq.$fConstructorLogic_Disj_ :: TestEq.Constructor
                                     TestEq.Logic_Disj_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Disj_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Disj_
    TestEq.$fConstructorLogic_Disj__conName
    TestEq.$dmconFixity_$s$dmconFixity7
    TestEq.$dmconIsRecord_$s$dmconIsRecord3

TestEq.$fConstructorLogic_Not__conName :: forall (t_afz :: *
                                                           -> *
                                                           -> *)
                                                 a_afA.
                                          t_afz TestEq.Logic_Not_ a_afA -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType A]
TestEq.$fConstructorLogic_Not__conName =
  __inline_me (\ (@ t_a12m::* -> * -> *) (@ a_a12n) _ ->
                 TestEq.$fConstructorLogic_Not_1)

TestEq.$fConstructorLogic_Not_ :: TestEq.Constructor
                                    TestEq.Logic_Not_
GblId[DFunId]
[Str: DmdType m]
TestEq.$fConstructorLogic_Not_ =
  TestEq.D:Constructor
    @ TestEq.Logic_Not_
    TestEq.$fConstructorLogic_Not__conName
    TestEq.$dmconFixity_$s$dmconFixity8
    TestEq.$dmconIsRecord_$s$dmconIsRecord2

TestEq.$fConstructorLogic_T__conName :: forall (t_afz :: *
                                                         -> *
                                                         -> *)
                                               a_afA.
                                        t_afz TestEq.Logic_T_ a_afA -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorLogic_T__conName =
  __inline_me (\ (@ t_a12b::* -> * -> *) (@ a_a12c) _ ->
                 TestEq.$fConstructorLogic_T_1)

TestEq.$fConstructorLogic_T_ :: TestEq.Constructor TestEq.Logic_T_
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_T_ =
  TestEq.D:Constructor
    @ TestEq.Logic_T_
    TestEq.$fConstructorLogic_T__conName
    TestEq.$dmconFixity_$s$dmconFixity9
    TestEq.$dmconIsRecord_$s$dmconIsRecord1

TestEq.$fConstructorLogic_F__conName :: forall (t_afz :: *
                                                         -> *
                                                         -> *)
                                               a_afA.
                                        t_afz TestEq.Logic_F_ a_afA -> GHC.Base.String
GblId
[Arity 1
 NoCafRefs
 Str: DmdType A]
TestEq.$fConstructorLogic_F__conName =
  __inline_me (\ (@ t_a120::* -> * -> *) (@ a_a121) _ ->
                 TestEq.$fConstructorLogic_F_1)

TestEq.$fConstructorLogic_F_ :: TestEq.Constructor TestEq.Logic_F_
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fConstructorLogic_F_ =
  TestEq.D:Constructor
    @ TestEq.Logic_F_
    TestEq.$fConstructorLogic_F__conName
    TestEq.$dmconFixity_$s$dmconFixity10
    TestEq.$dmconIsRecord_$s$dmconIsRecord

TestEq.$fEqC_eq' :: forall a_agF c_agG.
                    (TestEq.Eq a_agF) =>
                    TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool
GblId
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqC_eq' =
  __inline_me (\ (@ a_agF)
                 (@ c_agG)
                 ($dEq_a110 :: TestEq.Eq a_agF)
                 (eta_B2 :: TestEq.C c_agG a_agF)
                 (eta1_B1 :: TestEq.C c_agG a_agF) ->
                 case eta_B2 of _ { TestEq.C a1_agq ->
                 case eta1_B1 of _ { TestEq.C a'_agr ->
                 ($dEq_a110
                  `cast` (TestEq.NTCo:T:Eq a_agF
                          :: TestEq.T:Eq a_agF ~ (a_agF -> a_agF -> GHC.Bool.Bool)))
                   a1_agq a'_agr
                 }
                 })

TestEq.$fEqC :: forall a_agF c_agG.
                (TestEq.Eq a_agF) =>
                TestEq.Eq (TestEq.C c_agG a_agF)
GblId[DFunId]
[Arity 3
 NoCafRefs
 Str: DmdType C(C(S))U(L)U(L)]
TestEq.$fEqC =
  __inline_me (TestEq.$fEqC_eq'
               `cast` (forall a_agF c_agG.
                       (TestEq.Eq a_agF) =>
                       sym (TestEq.NTCo:T:Eq (TestEq.C c_agG a_agF))
                       :: (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.C c_agG a_agF -> TestEq.C c_agG a_agF -> GHC.Bool.Bool)
                            ~
                          (forall a_agF c_agG.
                           (TestEq.Eq a_agF) =>
                           TestEq.T:Eq (TestEq.C c_agG a_agF))))

Rec {
TestEq.$fEq[]_eq' :: forall a_agC.
                     (TestEq.Eq a_agC) =>
                     [a_agC] -> [a_agC] -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEq[]_eq' =
  \ (@ a_agC) ($dEq_a10b :: TestEq.Eq a_agC) ->
    let {
      a1_s1NL :: [a_agC] -> [a_agC] -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      a1_s1NL = TestEq.$fEq[]_eq' @ a_agC $dEq_a10b } in
    \ (eta_Xby :: [a_agC]) (eta1_Xn5 :: [a_agC]) ->
      case eta_Xby of _ {
        [] ->
          case eta1_Xn5 of _ {
            [] -> GHC.Bool.True; : _ _ -> GHC.Bool.False
          };
        : a2_agO as_agP ->
          case eta1_Xn5 of _ {
            [] -> GHC.Bool.False;
            : a3_Xoa as1_Xoc ->
              case ($dEq_a10b
                    `cast` (TestEq.NTCo:T:Eq a_agC
                            :: TestEq.T:Eq a_agC ~ (a_agC -> a_agC -> GHC.Bool.Bool)))
                     a2_agO a3_Xoa
              of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True -> a1_s1NL as_agP as1_Xoc
              }
          }
      }
end Rec }

TestEq.$fEq[] :: forall a_agC.
                 (TestEq.Eq a_agC) =>
                 TestEq.Eq [a_agC]
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEq[] =
  __inline_me (TestEq.$fEq[]_eq'
               `cast` (forall a_agC.
                       (TestEq.Eq a_agC) =>
                       sym (TestEq.NTCo:T:Eq [a_agC])
                       :: (forall a_agC.
                           (TestEq.Eq a_agC) =>
                           [a_agC] -> [a_agC] -> GHC.Bool.Bool)
                            ~
                          (forall a_agC. (TestEq.Eq a_agC) => TestEq.T:Eq [a_agC])))

a_r1Ya :: [GHC.Types.Char] -> [GHC.Types.Char] -> GHC.Bool.Bool
GblId
[Str: DmdType]
a_r1Ya =
  TestEq.$fEq[]_eq'
    @ GHC.Types.Char
    (GHC.Base.$fEqChar_==
     `cast` (sym (TestEq.NTCo:T:Eq GHC.Types.Char)
             :: (GHC.Types.Char -> GHC.Types.Char -> GHC.Bool.Bool)
                  ~
                TestEq.T:Eq GHC.Types.Char))

Rec {
$wa3_r1Yc :: TestEq.Logic
             -> TestEq.Rec TestEq.Logic
             -> TestEq.Logic
             -> TestEq.Rec TestEq.Logic
             -> GHC.Bool.Bool
GblId
[Arity 4
 Str: DmdType SLSL]
$wa3_r1Yc =
  \ (ww_s1L4 :: TestEq.Logic)
    (ww1_s1L6 :: TestEq.Rec TestEq.Logic)
    (ww2_s1Lc :: TestEq.Logic)
    (ww3_s1Le :: TestEq.Rec TestEq.Logic) ->
    case TestEq.eqLogic ww_s1L4 ww2_s1Lc of _ {
      GHC.Bool.False -> GHC.Bool.False;
      GHC.Bool.True ->
        case ww1_s1L6 of _ { TestEq.Rec x_Xsx ->
        case ww3_s1Le of _ { TestEq.Rec x'_XsB ->
        TestEq.eqLogic x_Xsx x'_XsB
        }
        }
    }
TestEq.eqLogic :: TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool
GblId
[Arity 2
 Str: DmdType SS]
TestEq.eqLogic =
  \ (x_agy :: TestEq.Logic) (y_agz :: TestEq.Logic) ->
    case TestEq.fromLogic
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           @ GHC.Prim.Any
           x_agy
    of _ {
      TestEq.L x1_age ->
        case TestEq.fromLogic
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               y_agz
        of _ {
          TestEq.L x'_agf ->
            case x1_age of _ { TestEq.C a1_agq ->
            case x'_agf of _ { TestEq.C a'_agr ->
            case a1_agq of _ { TestEq.Var x2_ags ->
            case a'_agr of _ { TestEq.Var x'1_agt -> a_r1Ya x2_ags x'1_agt }
            }
            }
            };
          TestEq.R _ -> GHC.Bool.False
        };
      TestEq.R x1_agg ->
        case TestEq.fromLogic
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               @ GHC.Prim.Any
               y_agz
        of _ {
          TestEq.L _ -> GHC.Bool.False;
          TestEq.R x'_agh ->
            case x1_agg of _ {
              TestEq.L x2_age ->
                case x'_agh of _ {
                  TestEq.L x'1_agf ->
                    case x2_age of _ { TestEq.C _ ->
                    case x'1_agf of _ { TestEq.C _ -> GHC.Bool.True }
                    };
                  TestEq.R _ -> GHC.Bool.False
                };
              TestEq.R x2_XnP ->
                case x'_agh of _ {
                  TestEq.L _ -> GHC.Bool.False;
                  TestEq.R x'1_XnT ->
                    case x2_XnP of _ {
                      TestEq.L x3_age ->
                        case x'1_XnT of _ {
                          TestEq.L x'2_agf ->
                            case x3_age of _ { TestEq.C _ ->
                            case x'2_agf of _ { TestEq.C _ -> GHC.Bool.True }
                            };
                          TestEq.R _ -> GHC.Bool.False
                        };
                      TestEq.R x3_XsY ->
                        case x'1_XnT of _ {
                          TestEq.L _ -> GHC.Bool.False;
                          TestEq.R x'2_Xt2 ->
                            case x3_XsY of _ {
                              TestEq.L x4_age ->
                                case x'2_Xt2 of _ {
                                  TestEq.L x'3_agf ->
                                    case x4_age of _ { TestEq.C a1_agq ->
                                    case x'3_agf of _ { TestEq.C a'_agr ->
                                    case a1_agq of _ { TestEq.Rec x5_agu ->
                                    case a'_agr of _ { TestEq.Rec x'4_agv ->
                                    TestEq.eqLogic x5_agu x'4_agv
                                    }
                                    }
                                    }
                                    };
                                  TestEq.R _ -> GHC.Bool.False
                                };
                              TestEq.R x4_Xp8 ->
                                case x'2_Xt2 of _ {
                                  TestEq.L _ -> GHC.Bool.False;
                                  TestEq.R x'3_Xpc ->
                                    case x4_Xp8 of _ {
                                      TestEq.L x5_age ->
                                        case x'3_Xpc of _ {
                                          TestEq.L x'4_agf ->
                                            case x5_age of _ { TestEq.C a1_agq ->
                                            case x'4_agf of _ { TestEq.C a'_agr ->
                                            case a1_agq of _ { TestEq.:*: ww_s1L2 ww1_s1L6 ->
                                            case ww_s1L2 of _ { TestEq.Rec ww3_s1L4 ->
                                            case a'_agr of _ { TestEq.:*: ww4_s1La ww5_s1Le ->
                                            case ww4_s1La of _ { TestEq.Rec ww7_s1Lc ->
                                            $wa3_r1Yc ww3_s1L4 ww1_s1L6 ww7_s1Lc ww5_s1Le
                                            }
                                            }
                                            }
                                            }
                                            }
                                            };
                                          TestEq.R _ -> GHC.Bool.False
                                        };
                                      TestEq.R x5_Xpf ->
                                        case x'3_Xpc of _ {
                                          TestEq.L _ -> GHC.Bool.False;
                                          TestEq.R x'4_Xpj ->
                                            case x5_Xpf of _ {
                                              TestEq.L x6_age ->
                                                case x'4_Xpj of _ {
                                                  TestEq.L x'5_agf ->
                                                    case x6_age of _ { TestEq.C a1_agq ->
                                                    case x'5_agf of _ { TestEq.C a'_agr ->
                                                    case a1_agq
                                                    of _ { TestEq.:*: ww_s1L2 ww1_s1L6 ->
                                                    case ww_s1L2 of _ { TestEq.Rec ww3_s1L4 ->
                                                    case a'_agr
                                                    of _ { TestEq.:*: ww4_s1La ww5_s1Le ->
                                                    case ww4_s1La of _ { TestEq.Rec ww7_s1Lc ->
                                                    $wa3_r1Yc ww3_s1L4 ww1_s1L6 ww7_s1Lc ww5_s1Le
                                                    }
                                                    }
                                                    }
                                                    }
                                                    }
                                                    };
                                                  TestEq.R _ -> GHC.Bool.False
                                                };
                                              TestEq.R x6_Xpm ->
                                                case x'4_Xpj of _ {
                                                  TestEq.L _ -> GHC.Bool.False;
                                                  TestEq.R x'5_Xpq ->
                                                    case x6_Xpm of _ {
                                                      TestEq.L x7_age ->
                                                        case x'5_Xpq of _ {
                                                          TestEq.L x'6_agf ->
                                                            case x7_age of _ { TestEq.C a1_agq ->
                                                            case x'6_agf of _ { TestEq.C a'_agr ->
                                                            case a1_agq
                                                            of _ { TestEq.:*: ww_s1L2 ww1_s1L6 ->
                                                            case ww_s1L2
                                                            of _ { TestEq.Rec ww3_s1L4 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww4_s1La ww5_s1Le ->
                                                            case ww4_s1La
                                                            of _ { TestEq.Rec ww7_s1Lc ->
                                                            $wa3_r1Yc
                                                              ww3_s1L4 ww1_s1L6 ww7_s1Lc ww5_s1Le
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            };
                                                          TestEq.R _ -> GHC.Bool.False
                                                        };
                                                      TestEq.R x7_Xpt ->
                                                        case x'5_Xpq of _ {
                                                          TestEq.L _ -> GHC.Bool.False;
                                                          TestEq.R x'6_Xpx ->
                                                            case x7_Xpt of _ { TestEq.C a1_agq ->
                                                            case x'6_Xpx of _ { TestEq.C a'_agr ->
                                                            case a1_agq
                                                            of _ { TestEq.:*: ww_s1L2 ww1_s1L6 ->
                                                            case ww_s1L2
                                                            of _ { TestEq.Rec ww3_s1L4 ->
                                                            case a'_agr
                                                            of _ { TestEq.:*: ww4_s1La ww5_s1Le ->
                                                            case ww4_s1La
                                                            of _ { TestEq.Rec ww7_s1Lc ->
                                                            $wa3_r1Yc
                                                              ww3_s1L4 ww1_s1L6 ww7_s1Lc ww5_s1Le
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
end Rec }

TestEq.t2 :: GHC.Bool.Bool
GblId
[Str: DmdType]
TestEq.t2 = TestEq.eqLogic TestEq.logic1 TestEq.logic1

TestEq.$fEqLogic :: TestEq.Eq TestEq.Logic
GblId[DFunId]
[Arity 2
 Str: DmdType SS]
TestEq.$fEqLogic =
  TestEq.eqLogic
  `cast` (sym (TestEq.NTCo:T:Eq TestEq.Logic)
          :: (TestEq.Logic -> TestEq.Logic -> GHC.Bool.Bool)
               ~
             TestEq.T:Eq TestEq.Logic)

TestEq.$fReadU_readListDefault :: Text.ParserCombinators.ReadP.ReadS
                                    [TestEq.U]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadU_readListDefault =
  Text.ParserCombinators.ReadP.run @ [TestEq.U] TestEq.$fReadU4

TestEq.$fReadU :: GHC.Read.Read TestEq.U
GblId[DFunId]
[Str: DmdType m]
TestEq.$fReadU =
  GHC.Read.D:Read
    @ TestEq.U
    TestEq.$fReadU5
    TestEq.$fReadU_readListDefault
    (TestEq.$fReadU2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.U)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.U)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.U))
    (TestEq.$fReadU1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.U])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.U])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.U]))

TestEq.$fShowU_showList :: [TestEq.U] -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowU_showList =
  \ (ds1_a1gk :: [TestEq.U]) (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.U ->
           GHC.Base.++
             @ GHC.Types.Char
             TestEq.$fShowU1
             (let {
                lvl12_s1NN :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NN =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1NP :: [TestEq.U] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NP =
                  \ (ds2_a1gv :: [TestEq.U]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NN;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.U ->
                           GHC.Base.++ @ GHC.Types.Char TestEq.$fShowU1 (showl_s1NP ys_a1gB)
                           })
                    }; } in
              showl_s1NP xs_a1gr)
           })
    }

TestEq.$fShowU_showsPrec :: GHC.Types.Int
                            -> TestEq.U
                            -> GHC.Show.ShowS
GblId
[Arity 3
 NoCafRefs
 Str: DmdType AU()L]
TestEq.$fShowU_showsPrec =
  __inline_me (\ _
                 (ds1_d1cw :: TestEq.U)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds1_d1cw of _ { TestEq.U ->
                 GHC.Base.++ @ GHC.Types.Char TestEq.$fShowU1 eta_B1
                 })

TestEq.$fShowU :: GHC.Show.Show TestEq.U
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowU =
  GHC.Show.D:Show
    @ TestEq.U
    TestEq.$fShowU_showsPrec
    TestEq.$fShowU3
    TestEq.$fShowU_showList

TestEq.$fRead:+:_readList :: forall a_afL b_afM.
                             (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                             Text.ParserCombinators.ReadP.ReadS [a_afL TestEq.:+: b_afM]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:+:_readList =
  \ (@ a_XtF)
    (@ b_XtH)
    ($dRead_X1ck :: GHC.Read.Read a_XtF)
    ($dRead1_X1cm :: GHC.Read.Read b_XtH) ->
    Text.ParserCombinators.ReadP.run
      @ [a_XtF TestEq.:+: b_XtH]
      (((GHC.Read.$dmreadList2
           @ (a_XtF TestEq.:+: b_XtH)
           ((TestEq.$fRead:+:1 @ a_XtF @ b_XtH $dRead_X1ck $dRead1_X1cm)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_XtF TestEq.:+: b_XtH))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_XtF TestEq.:+: b_XtH))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_XtF TestEq.:+: b_XtH)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_XtF TestEq.:+: b_XtH]
                :: Text.ParserCombinators.ReadP.ReadP [a_XtF TestEq.:+: b_XtH]
                     ~
                   (forall b_a1As.
                    ([a_XtF TestEq.:+: b_XtH] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [a_XtF TestEq.:+: b_XtH]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_XtF TestEq.:+: b_XtH]))

TestEq.$fRead:+: :: forall a_afL b_afM.
                    (GHC.Read.Read a_afL, GHC.Read.Read b_afM) =>
                    GHC.Read.Read (a_afL TestEq.:+: b_afM)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:+: =
  __inline_me (\ (@ a_Xty)
                 (@ b_XtA)
                 ($dRead_X1cd :: GHC.Read.Read a_Xty)
                 ($dRead1_X1cf :: GHC.Read.Read b_XtA) ->
                 let {
                   a1_s1IW [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [a_Xty TestEq.:+: b_XtA]
                   LclId
                   [Str: DmdType]
                   a1_s1IW =
                     TestEq.$fRead:+:3 @ a_Xty @ b_XtA $dRead_X1cd $dRead1_X1cf } in
                 let {
                   a2_s1E3 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   (a_Xty TestEq.:+: b_XtA)
                   LclId
                   [Str: DmdType]
                   a2_s1E3 =
                     TestEq.$fRead:+:1 @ a_Xty @ b_XtA $dRead_X1cd $dRead1_X1cf } in
                 let {
                   a3_s1nO [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [a_Xty TestEq.:+: b_XtA]
                   LclId
                   [Str: DmdType]
                   a3_s1nO =
                     TestEq.$fRead:+:_readList
                       @ a_Xty @ b_XtA $dRead_X1cd $dRead1_X1cf } in
                 letrec {
                   $dRead2_s1nK :: GHC.Read.Read (a_Xty TestEq.:+: b_XtA)
                   LclId
                   [Str: DmdType m]
                   $dRead2_s1nK =
                     GHC.Read.D:Read
                       @ (a_Xty TestEq.:+: b_XtA)
                       a4_s1nL
                       a3_s1nO
                       (a2_s1E3
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_Xty TestEq.:+: b_XtA))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_Xty TestEq.:+: b_XtA))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_Xty TestEq.:+: b_XtA)))
                       (a1_s1IW
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_Xty TestEq.:+: b_XtA])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_Xty TestEq.:+: b_XtA])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_Xty TestEq.:+: b_XtA]));
                   a4_s1nL :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (a_Xty TestEq.:+: b_XtA)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1nL =
                     GHC.Read.$dmreadsPrec @ (a_Xty TestEq.:+: b_XtA) $dRead2_s1nK; } in
                 $dRead2_s1nK)

TestEq.$fShow:+:_showsPrec :: forall a_afL b_afM.
                              (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                              GHC.Types.Int -> (a_afL TestEq.:+: b_afM) -> GHC.Show.ShowS
GblId
[Arity 4
 Worker TestEq.$wshowsPrec1
 Str: DmdType LLU(L)S]
TestEq.$fShow:+:_showsPrec =
  __inline_me (\ (@ a_afL)
                 (@ b_afM)
                 (w_s1Lq :: GHC.Show.Show a_afL)
                 (w1_s1Lr :: GHC.Show.Show b_afM)
                 (w2_s1Ls :: GHC.Types.Int)
                 (w3_s1Lw :: a_afL TestEq.:+: b_afM) ->
                 case w2_s1Ls of _ { GHC.Types.I# ww_s1Lu ->
                 TestEq.$wshowsPrec1 @ a_afL @ b_afM w_s1Lq w1_s1Lr ww_s1Lu w3_s1Lw
                 })

TestEq.$fShow:+:_showList :: forall a_afL b_afM.
                             (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                             [a_afL TestEq.:+: b_afM] -> GHC.Show.ShowS
GblId
[Arity 4
 Str: DmdType LLSL]
TestEq.$fShow:+:_showList =
  \ (@ a_XtR)
    (@ b_XtT)
    ($dShow_aXP :: GHC.Show.Show a_XtR)
    ($dShow1_aXQ :: GHC.Show.Show b_XtT)
    (eta_B2 :: [a_XtR TestEq.:+: b_XtT])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (TestEq.$wshowsPrec1
             @ a_XtR
             @ b_XtT
             $dShow_aXP
             $dShow1_aXQ
             0
             x_a1gq
             (let {
                lvl12_s1NR :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NR =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1NT :: [a_XtR TestEq.:+: b_XtT] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NT =
                  \ (ds2_a1gv :: [a_XtR TestEq.:+: b_XtT]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NR;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (TestEq.$wshowsPrec1
                             @ a_XtR
                             @ b_XtT
                             $dShow_aXP
                             $dShow1_aXQ
                             0
                             y_a1gA
                             (showl_s1NT ys_a1gB))
                    }; } in
              showl_s1NT xs_a1gr))
    }

TestEq.$fShow:+: :: forall a_afL b_afM.
                    (GHC.Show.Show a_afL, GHC.Show.Show b_afM) =>
                    GHC.Show.Show (a_afL TestEq.:+: b_afM)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:+: =
  __inline_me (\ (@ a_XtQ)
                 (@ b_XtS)
                 ($dShow_aXP :: GHC.Show.Show a_XtQ)
                 ($dShow1_aXQ :: GHC.Show.Show b_XtS) ->
                 let {
                   a1_s1nE :: [a_XtQ TestEq.:+: b_XtS] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1nE =
                     TestEq.$fShow:+:_showList
                       @ a_XtQ @ b_XtS $dShow_aXP $dShow1_aXQ } in
                 let {
                   a2_s1nF :: GHC.Types.Int
                              -> (a_XtQ TestEq.:+: b_XtS)
                              -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)S]
                   a2_s1nF =
                     TestEq.$fShow:+:_showsPrec
                       @ a_XtQ @ b_XtS $dShow_aXP $dShow1_aXQ } in
                 letrec {
                   $dShow2_s1nC :: GHC.Show.Show (a_XtQ TestEq.:+: b_XtS)
                   LclId
                   [Str: DmdType m]
                   $dShow2_s1nC =
                     GHC.Show.D:Show @ (a_XtQ TestEq.:+: b_XtS) a2_s1nF a3_s1nD a1_s1nE;
                   a3_s1nD :: (a_XtQ TestEq.:+: b_XtS) -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1nD =
                     GHC.Show.$dmshow @ (a_XtQ TestEq.:+: b_XtS) $dShow2_s1nC; } in
                 $dShow2_s1nC)

TestEq.$fRead:*:_readList :: forall a_afJ b_afK.
                             (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                             Text.ParserCombinators.ReadP.ReadS [a_afJ TestEq.:*: b_afK]
GblId
[Arity 2
 Str: DmdType LL]
TestEq.$fRead:*:_readList =
  \ (@ a_Xua)
    (@ b_Xuc)
    ($dRead_X1bq :: GHC.Read.Read a_Xua)
    ($dRead1_X1bs :: GHC.Read.Read b_Xuc) ->
    Text.ParserCombinators.ReadP.run
      @ [a_Xua TestEq.:*: b_Xuc]
      (((GHC.Read.$dmreadList2
           @ (a_Xua TestEq.:*: b_Xuc)
           ((TestEq.$fRead:*:1 @ a_Xua @ b_Xuc $dRead_X1bq $dRead1_X1bs)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (a_Xua TestEq.:*: b_Xuc))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (a_Xua TestEq.:*: b_Xuc))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (a_Xua TestEq.:*: b_Xuc)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [a_Xua TestEq.:*: b_Xuc]
                :: Text.ParserCombinators.ReadP.ReadP [a_Xua TestEq.:*: b_Xuc]
                     ~
                   (forall b_a1As.
                    ([a_Xua TestEq.:*: b_Xuc] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [a_Xua TestEq.:*: b_Xuc]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [a_Xua TestEq.:*: b_Xuc]))

TestEq.$fRead:*: :: forall a_afJ b_afK.
                    (GHC.Read.Read a_afJ, GHC.Read.Read b_afK) =>
                    GHC.Read.Read (a_afJ TestEq.:*: b_afK)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fRead:*: =
  __inline_me (\ (@ a_Xu3)
                 (@ b_Xu5)
                 ($dRead_X1bj :: GHC.Read.Read a_Xu3)
                 ($dRead1_X1bl :: GHC.Read.Read b_Xu5) ->
                 let {
                   a1_s1IY [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [a_Xu3 TestEq.:*: b_Xu5]
                   LclId
                   [Str: DmdType]
                   a1_s1IY =
                     TestEq.$fRead:*:4 @ a_Xu3 @ b_Xu5 $dRead_X1bj $dRead1_X1bl } in
                 let {
                   a2_s1EC :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (a_Xu3 TestEq.:*: b_Xu5)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1EC =
                     TestEq.$fRead:*:1 @ a_Xu3 @ b_Xu5 $dRead_X1bj $dRead1_X1bl } in
                 let {
                   a3_s1ns [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [a_Xu3 TestEq.:*: b_Xu5]
                   LclId
                   [Str: DmdType]
                   a3_s1ns =
                     TestEq.$fRead:*:_readList
                       @ a_Xu3 @ b_Xu5 $dRead_X1bj $dRead1_X1bl } in
                 letrec {
                   $dRead2_s1no :: GHC.Read.Read (a_Xu3 TestEq.:*: b_Xu5)
                   LclId
                   [Str: DmdType m]
                   $dRead2_s1no =
                     GHC.Read.D:Read
                       @ (a_Xu3 TestEq.:*: b_Xu5)
                       a4_s1np
                       a3_s1ns
                       (a2_s1EC
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (a_Xu3 TestEq.:*: b_Xu5))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (a_Xu3 TestEq.:*: b_Xu5))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     (a_Xu3 TestEq.:*: b_Xu5)))
                       (a1_s1IY
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [a_Xu3 TestEq.:*: b_Xu5])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [a_Xu3 TestEq.:*: b_Xu5])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [a_Xu3 TestEq.:*: b_Xu5]));
                   a4_s1np :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (a_Xu3 TestEq.:*: b_Xu5)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1np =
                     GHC.Read.$dmreadsPrec @ (a_Xu3 TestEq.:*: b_Xu5) $dRead2_s1no; } in
                 $dRead2_s1no)

TestEq.$fShow:*:_showsPrec :: forall a_afJ b_afK.
                              (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                              GHC.Types.Int -> (a_afJ TestEq.:*: b_afK) -> GHC.Show.ShowS
GblId
[Arity 4
 Worker TestEq.$wshowsPrec
 Str: DmdType LLU(L)U(LL)]
TestEq.$fShow:*:_showsPrec =
  __inline_me (\ (@ a_afJ)
                 (@ b_afK)
                 (w_s1LC :: GHC.Show.Show a_afJ)
                 (w1_s1LD :: GHC.Show.Show b_afK)
                 (w2_s1LE :: GHC.Types.Int)
                 (w3_s1LI :: a_afJ TestEq.:*: b_afK) ->
                 case w2_s1LE of _ { GHC.Types.I# ww_s1LG ->
                 case w3_s1LI of _ { TestEq.:*: ww1_s1LK ww2_s1LL ->
                 TestEq.$wshowsPrec
                   @ a_afJ @ b_afK w_s1LC w1_s1LD ww_s1LG ww1_s1LK ww2_s1LL
                 }
                 })

TestEq.$fShow:*:_showList :: forall a_afJ b_afK.
                             (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                             [a_afJ TestEq.:*: b_afK] -> GHC.Show.ShowS
GblId
[Arity 4
 Str: DmdType LLSL]
TestEq.$fShow:*:_showList =
  \ (@ a_Xuj)
    (@ b_Xul)
    ($dShow_aWq :: GHC.Show.Show a_Xuj)
    ($dShow1_aWr :: GHC.Show.Show b_Xul)
    (eta_B2 :: [a_Xuj TestEq.:*: b_Xul])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.:*: ww_s1LK ww1_s1LL ->
           TestEq.$wshowsPrec
             @ a_Xuj
             @ b_Xul
             $dShow_aWq
             $dShow1_aWr
             0
             ww_s1LK
             ww1_s1LL
             (let {
                lvl12_s1NV :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NV =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1NX :: [a_Xuj TestEq.:*: b_Xul] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1NX =
                  \ (ds2_a1gv :: [a_Xuj TestEq.:*: b_Xul]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NV;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.:*: ww2_X1Vu ww3_X1Vw ->
                           TestEq.$wshowsPrec
                             @ a_Xuj
                             @ b_Xul
                             $dShow_aWq
                             $dShow1_aWr
                             0
                             ww2_X1Vu
                             ww3_X1Vw
                             (showl_s1NX ys_a1gB)
                           })
                    }; } in
              showl_s1NX xs_a1gr)
           })
    }

TestEq.$fShow:*: :: forall a_afJ b_afK.
                    (GHC.Show.Show a_afJ, GHC.Show.Show b_afK) =>
                    GHC.Show.Show (a_afJ TestEq.:*: b_afK)
GblId[DFunId]
[Arity 2
 Str: DmdType LLm]
TestEq.$fShow:*: =
  __inline_me (\ (@ a_Xui)
                 (@ b_Xuk)
                 ($dShow_aWq :: GHC.Show.Show a_Xui)
                 ($dShow1_aWr :: GHC.Show.Show b_Xuk) ->
                 let {
                   a1_s1ni :: [a_Xui TestEq.:*: b_Xuk] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1ni =
                     TestEq.$fShow:*:_showList
                       @ a_Xui @ b_Xuk $dShow_aWq $dShow1_aWr } in
                 let {
                   a2_s1nj :: GHC.Types.Int
                              -> (a_Xui TestEq.:*: b_Xuk)
                              -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(LL)]
                   a2_s1nj =
                     TestEq.$fShow:*:_showsPrec
                       @ a_Xui @ b_Xuk $dShow_aWq $dShow1_aWr } in
                 letrec {
                   $dShow2_s1ng :: GHC.Show.Show (a_Xui TestEq.:*: b_Xuk)
                   LclId
                   [Str: DmdType m]
                   $dShow2_s1ng =
                     GHC.Show.D:Show @ (a_Xui TestEq.:*: b_Xuk) a2_s1nj a3_s1nh a1_s1ni;
                   a3_s1nh :: (a_Xui TestEq.:*: b_Xuk) -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1nh =
                     GHC.Show.$dmshow @ (a_Xui TestEq.:*: b_Xuk) $dShow2_s1ng; } in
                 $dShow2_s1ng)

TestEq.$fReadC_readList :: forall c_afH a_afI.
                           (GHC.Read.Read a_afI) =>
                           Text.ParserCombinators.ReadP.ReadS [TestEq.C c_afH a_afI]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadC_readList =
  \ (@ c_XuC) (@ a_XuE) ($dRead_X1aI :: GHC.Read.Read a_XuE) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.C c_XuC a_XuE]
      (((GHC.Read.$dmreadList2
           @ (TestEq.C c_XuC a_XuE)
           ((TestEq.$fReadC1 @ c_XuC @ a_XuE $dRead_X1aI)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                         (TestEq.C c_XuC a_XuE))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_XuC a_XuE))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_XuC a_XuE)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
                  [TestEq.C c_XuC a_XuE]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.C c_XuC a_XuE]
                     ~
                   (forall b_a1As.
                    ([TestEq.C c_XuC a_XuE] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [TestEq.C c_XuC a_XuE]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.C c_XuC a_XuE]))

TestEq.$fReadC :: forall c_afH a_afI.
                  (GHC.Read.Read a_afI) =>
                  GHC.Read.Read (TestEq.C c_afH a_afI)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadC =
  __inline_me (\ (@ c_Xuw)
                 (@ a_Xuy)
                 ($dRead_X1aC :: GHC.Read.Read a_Xuy) ->
                 let {
                   a1_s1J0 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [TestEq.C c_Xuw a_Xuy]
                   LclId
                   [Str: DmdType]
                   a1_s1J0 = TestEq.$fReadC2 @ c_Xuw @ a_Xuy $dRead_X1aC } in
                 let {
                   a2_s1F6 :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuw a_Xuy)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1F6 = TestEq.$fReadC1 @ c_Xuw @ a_Xuy $dRead_X1aC } in
                 let {
                   a3_s1n6 [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.C c_Xuw a_Xuy]
                   LclId
                   [Str: DmdType]
                   a3_s1n6 = TestEq.$fReadC_readList @ c_Xuw @ a_Xuy $dRead_X1aC } in
                 letrec {
                   $dRead1_s1n2 :: GHC.Read.Read (TestEq.C c_Xuw a_Xuy)
                   LclId
                   [Str: DmdType m]
                   $dRead1_s1n2 =
                     GHC.Read.D:Read
                       @ (TestEq.C c_Xuw a_Xuy)
                       a4_s1n3
                       a3_s1n6
                       (a2_s1F6
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     (TestEq.C c_Xuw a_Xuy))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.C c_Xuw a_Xuy))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.C c_Xuw a_Xuy)))
                       (a1_s1J0
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                     [TestEq.C c_Xuw a_Xuy])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.C c_Xuw a_Xuy])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec
                                     [TestEq.C c_Xuw a_Xuy]));
                   a4_s1n3 :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (TestEq.C c_Xuw a_Xuy)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1n3 =
                     GHC.Read.$dmreadsPrec @ (TestEq.C c_Xuw a_Xuy) $dRead1_s1n2; } in
                 $dRead1_s1n2)

TestEq.$fShowC_showsPrec :: forall c_afH a_afI.
                            (GHC.Show.Show a_afI) =>
                            GHC.Types.Int -> TestEq.C c_afH a_afI -> GHC.Show.ShowS
GblId
[Arity 3
 Worker TestEq.$wshowsPrec2
 Str: DmdType LU(L)U(L)]
TestEq.$fShowC_showsPrec =
  __inline_me (\ (@ c_afH)
                 (@ a_afI)
                 (w_s1LR :: GHC.Show.Show a_afI)
                 (w1_s1LS :: GHC.Types.Int)
                 (w2_s1LW :: TestEq.C c_afH a_afI) ->
                 case w1_s1LS of _ { GHC.Types.I# ww_s1LU ->
                 case w2_s1LW of _ { TestEq.C ww1_s1LY ->
                 TestEq.$wshowsPrec2 @ c_afH @ a_afI w_s1LR ww_s1LU ww1_s1LY
                 }
                 })

TestEq.$fShowC_showList :: forall c_afH a_afI.
                           (GHC.Show.Show a_afI) =>
                           [TestEq.C c_afH a_afI] -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowC_showList =
  \ (@ c_XuK)
    (@ a_XuM)
    ($dShow_aVm :: GHC.Show.Show a_XuM)
    (eta_B2 :: [TestEq.C c_XuK a_XuM])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.C ww_s1LY ->
           TestEq.$wshowsPrec2
             @ c_XuK
             @ a_XuM
             $dShow_aVm
             0
             ww_s1LY
             (let {
                lvl12_s1NZ :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1NZ =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1O1 :: [TestEq.C c_XuK a_XuM] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1O1 =
                  \ (ds2_a1gv :: [TestEq.C c_XuK a_XuM]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1NZ;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.C ww1_X1VL ->
                           TestEq.$wshowsPrec2
                             @ c_XuK @ a_XuM $dShow_aVm 0 ww1_X1VL (showl_s1O1 ys_a1gB)
                           })
                    }; } in
              showl_s1O1 xs_a1gr)
           })
    }

TestEq.$fShowC :: forall c_afH a_afI.
                  (GHC.Show.Show a_afI) =>
                  GHC.Show.Show (TestEq.C c_afH a_afI)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowC =
  __inline_me (\ (@ c_XuJ)
                 (@ a_XuL)
                 ($dShow_aVm :: GHC.Show.Show a_XuL) ->
                 let {
                   a1_s1mW :: [TestEq.C c_XuJ a_XuL] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1mW = TestEq.$fShowC_showList @ c_XuJ @ a_XuL $dShow_aVm } in
                 let {
                   a2_s1mX :: GHC.Types.Int -> TestEq.C c_XuJ a_XuL -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a2_s1mX = TestEq.$fShowC_showsPrec @ c_XuJ @ a_XuL $dShow_aVm } in
                 letrec {
                   $dShow1_s1mU :: GHC.Show.Show (TestEq.C c_XuJ a_XuL)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1mU =
                     GHC.Show.D:Show @ (TestEq.C c_XuJ a_XuL) a2_s1mX a3_s1mV a1_s1mW;
                   a3_s1mV :: TestEq.C c_XuJ a_XuL -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1mV =
                     GHC.Show.$dmshow @ (TestEq.C c_XuJ a_XuL) $dShow1_s1mU; } in
                 $dShow1_s1mU)

TestEq.$fReadVar_readList :: forall a_afG.
                             (GHC.Read.Read a_afG) =>
                             Text.ParserCombinators.ReadP.ReadS [TestEq.Var a_afG]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadVar_readList =
  \ (@ a_Xv0) ($dRead_X1a2 :: GHC.Read.Read a_Xv0) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Var a_Xv0]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Var a_Xv0)
           ((TestEq.$fReadVar1 @ a_Xv0 $dRead_X1a2)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_Xv0))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_Xv0))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_Xv0)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Var a_Xv0]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_Xv0]
                     ~
                   (forall b_a1As.
                    ([TestEq.Var a_Xv0] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [TestEq.Var a_Xv0]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Var a_Xv0]))

TestEq.$fReadVar :: forall a_afG.
                    (GHC.Read.Read a_afG) =>
                    GHC.Read.Read (TestEq.Var a_afG)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadVar =
  __inline_me (\ (@ a_XuV) ($dRead_X19X :: GHC.Read.Read a_XuV) ->
                 let {
                   a1_s1J2 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [TestEq.Var a_XuV]
                   LclId
                   [Str: DmdType]
                   a1_s1J2 = TestEq.$fReadVar3 @ a_XuV $dRead_X19X } in
                 let {
                   a2_s1Fs :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuV)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1Fs = TestEq.$fReadVar1 @ a_XuV $dRead_X19X } in
                 let {
                   a3_s1mK [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.Var a_XuV]
                   LclId
                   [Str: DmdType]
                   a3_s1mK = TestEq.$fReadVar_readList @ a_XuV $dRead_X19X } in
                 letrec {
                   $dRead1_s1mG :: GHC.Read.Read (TestEq.Var a_XuV)
                   LclId
                   [Str: DmdType m]
                   $dRead1_s1mG =
                     GHC.Read.D:Read
                       @ (TestEq.Var a_XuV)
                       a4_s1mH
                       a3_s1mK
                       (a2_s1Fs
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Var a_XuV))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Var a_XuV))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Var a_XuV)))
                       (a1_s1J2
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Var a_XuV])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Var a_XuV])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Var a_XuV]));
                   a4_s1mH :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (TestEq.Var a_XuV)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1mH =
                     GHC.Read.$dmreadsPrec @ (TestEq.Var a_XuV) $dRead1_s1mG; } in
                 $dRead1_s1mG)

TestEq.$fShowVar_showsPrec :: forall a_afG.
                              (GHC.Show.Show a_afG) =>
                              GHC.Types.Int -> TestEq.Var a_afG -> GHC.Show.ShowS
GblId
[Arity 3
 Worker TestEq.$wshowsPrec4
 Str: DmdType LU(L)U(L)]
TestEq.$fShowVar_showsPrec =
  __inline_me (\ (@ a_afG)
                 (w_s1M4 :: GHC.Show.Show a_afG)
                 (w1_s1M5 :: GHC.Types.Int)
                 (w2_s1M9 :: TestEq.Var a_afG) ->
                 case w1_s1M5 of _ { GHC.Types.I# ww_s1M7 ->
                 case w2_s1M9 of _ { TestEq.Var ww1_s1Mb ->
                 TestEq.$wshowsPrec4 @ a_afG w_s1M4 ww_s1M7 ww1_s1Mb
                 }
                 })

TestEq.$fShowVar_showList :: forall a_afG.
                             (GHC.Show.Show a_afG) =>
                             [TestEq.Var a_afG] -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowVar_showList =
  \ (@ a_Xv8)
    ($dShow_aUi :: GHC.Show.Show a_Xv8)
    (eta_B2 :: [TestEq.Var a_Xv8])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Var ww_s1Mb ->
           TestEq.$wshowsPrec4
             @ a_Xv8
             $dShow_aUi
             0
             ww_s1Mb
             (let {
                lvl12_s1O3 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1O3 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1O5 :: [TestEq.Var a_Xv8] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1O5 =
                  \ (ds2_a1gv :: [TestEq.Var a_Xv8]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1O3;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Var ww1_X1W2 ->
                           TestEq.$wshowsPrec4
                             @ a_Xv8 $dShow_aUi 0 ww1_X1W2 (showl_s1O5 ys_a1gB)
                           })
                    }; } in
              showl_s1O5 xs_a1gr)
           })
    }

TestEq.$fShowVar :: forall a_afG.
                    (GHC.Show.Show a_afG) =>
                    GHC.Show.Show (TestEq.Var a_afG)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowVar =
  __inline_me (\ (@ a_Xv7) ($dShow_aUi :: GHC.Show.Show a_Xv7) ->
                 let {
                   a1_s1mA :: [TestEq.Var a_Xv7] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1mA = TestEq.$fShowVar_showList @ a_Xv7 $dShow_aUi } in
                 let {
                   a2_s1mB :: GHC.Types.Int -> TestEq.Var a_Xv7 -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a2_s1mB = TestEq.$fShowVar_showsPrec @ a_Xv7 $dShow_aUi } in
                 letrec {
                   $dShow1_s1my :: GHC.Show.Show (TestEq.Var a_Xv7)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1my =
                     GHC.Show.D:Show @ (TestEq.Var a_Xv7) a2_s1mB a3_s1mz a1_s1mA;
                   a3_s1mz :: TestEq.Var a_Xv7 -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1mz = GHC.Show.$dmshow @ (TestEq.Var a_Xv7) $dShow1_s1my; } in
                 $dShow1_s1my)

TestEq.$fReadRec_readList :: forall a_afF.
                             (GHC.Read.Read a_afF) =>
                             Text.ParserCombinators.ReadP.ReadS [TestEq.Rec a_afF]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadRec_readList =
  \ (@ a_Xvo) ($dRead_X19n :: GHC.Read.Read a_Xvo) ->
    Text.ParserCombinators.ReadP.run
      @ [TestEq.Rec a_Xvo]
      (((GHC.Read.$dmreadList2
           @ (TestEq.Rec a_Xvo)
           ((TestEq.$fReadRec1 @ a_Xvo $dRead_X19n)
            `cast` (sym
                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvo))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvo))
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvo)))
           GHC.Read.$dmreadList1)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [TestEq.Rec a_Xvo]
                :: Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xvo]
                     ~
                   (forall b_a1As.
                    ([TestEq.Rec a_Xvo] -> Text.ParserCombinators.ReadP.P b_a1As)
                    -> Text.ParserCombinators.ReadP.P b_a1As)))
         @ [TestEq.Rec a_Xvo]
         (Text.ParserCombinators.ReadP.$fMonadP_return
            @ [TestEq.Rec a_Xvo]))

TestEq.$fReadRec :: forall a_afF.
                    (GHC.Read.Read a_afF) =>
                    GHC.Read.Read (TestEq.Rec a_afF)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fReadRec =
  __inline_me (\ (@ a_Xvj) ($dRead_X19i :: GHC.Read.Read a_Xvj) ->
                 let {
                   a1_s1J4 [ALWAYS Just L] :: Text.ParserCombinators.ReadPrec.Prec
                                              -> Text.ParserCombinators.ReadP.ReadP
                                                   [TestEq.Rec a_Xvj]
                   LclId
                   [Str: DmdType]
                   a1_s1J4 = TestEq.$fReadRec3 @ a_Xvj $dRead_X19i } in
                 let {
                   a2_s1FO :: Text.ParserCombinators.ReadPrec.Prec
                              -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvj)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a2_s1FO = TestEq.$fReadRec1 @ a_Xvj $dRead_X19i } in
                 let {
                   a3_s1mo [ALWAYS Just L] :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.Rec a_Xvj]
                   LclId
                   [Str: DmdType]
                   a3_s1mo = TestEq.$fReadRec_readList @ a_Xvj $dRead_X19i } in
                 letrec {
                   $dRead1_s1mk :: GHC.Read.Read (TestEq.Rec a_Xvj)
                   LclId
                   [Str: DmdType m]
                   $dRead1_s1mk =
                     GHC.Read.D:Read
                       @ (TestEq.Rec a_Xvj)
                       a4_s1ml
                       a3_s1mo
                       (a2_s1FO
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec (TestEq.Rec a_Xvj))
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP (TestEq.Rec a_Xvj))
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec (TestEq.Rec a_Xvj)))
                       (a1_s1J4
                        `cast` (sym
                                  (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Rec a_Xvj])
                                :: (Text.ParserCombinators.ReadPrec.Prec
                                    -> Text.ParserCombinators.ReadP.ReadP [TestEq.Rec a_Xvj])
                                     ~
                                   Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Rec a_Xvj]));
                   a4_s1ml :: GHC.Types.Int
                              -> Text.ParserCombinators.ReadP.ReadS (TestEq.Rec a_Xvj)
                   LclId
                   [Arity 1
                    Str: DmdType L]
                   a4_s1ml =
                     GHC.Read.$dmreadsPrec @ (TestEq.Rec a_Xvj) $dRead1_s1mk; } in
                 $dRead1_s1mk)

TestEq.$fShowRec_showsPrec :: forall a_afF.
                              (GHC.Show.Show a_afF) =>
                              GHC.Types.Int -> TestEq.Rec a_afF -> GHC.Show.ShowS
GblId
[Arity 3
 Worker TestEq.$wshowsPrec3
 Str: DmdType LU(L)U(L)]
TestEq.$fShowRec_showsPrec =
  __inline_me (\ (@ a_afF)
                 (w_s1Mh :: GHC.Show.Show a_afF)
                 (w1_s1Mi :: GHC.Types.Int)
                 (w2_s1Mm :: TestEq.Rec a_afF) ->
                 case w1_s1Mi of _ { GHC.Types.I# ww_s1Mk ->
                 case w2_s1Mm of _ { TestEq.Rec ww1_s1Mo ->
                 TestEq.$wshowsPrec3 @ a_afF w_s1Mh ww_s1Mk ww1_s1Mo
                 }
                 })

TestEq.$fShowRec_showList :: forall a_afF.
                             (GHC.Show.Show a_afF) =>
                             [TestEq.Rec a_afF] -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowRec_showList =
  \ (@ a_Xvw)
    ($dShow_aTe :: GHC.Show.Show a_Xvw)
    (eta_B2 :: [TestEq.Rec a_Xvw])
    (eta1_B1 :: GHC.Base.String) ->
    case eta_B2 of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 eta1_B1;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_a1gq of _ { TestEq.Rec ww_s1Mo ->
           TestEq.$wshowsPrec3
             @ a_Xvw
             $dShow_aTe
             0
             ww_s1Mo
             (let {
                lvl12_s1O7 :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1O7 =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 eta1_B1 } in
              letrec {
                showl_s1O9 :: [TestEq.Rec a_Xvw] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1O9 =
                  \ (ds2_a1gv :: [TestEq.Rec a_Xvw]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1O7;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (case y_a1gA of _ { TestEq.Rec ww1_X1Wk ->
                           TestEq.$wshowsPrec3
                             @ a_Xvw $dShow_aTe 0 ww1_X1Wk (showl_s1O9 ys_a1gB)
                           })
                    }; } in
              showl_s1O9 xs_a1gr)
           })
    }

TestEq.$fShowRec :: forall a_afF.
                    (GHC.Show.Show a_afF) =>
                    GHC.Show.Show (TestEq.Rec a_afF)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowRec =
  __inline_me (\ (@ a_Xvv) ($dShow_aTe :: GHC.Show.Show a_Xvv) ->
                 let {
                   a1_s1me :: [TestEq.Rec a_Xvv] -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType SL]
                   a1_s1me = TestEq.$fShowRec_showList @ a_Xvv $dShow_aTe } in
                 let {
                   a2_s1mf :: GHC.Types.Int -> TestEq.Rec a_Xvv -> GHC.Show.ShowS
                   LclId
                   [Arity 2
                    Str: DmdType U(L)U(L)]
                   a2_s1mf = TestEq.$fShowRec_showsPrec @ a_Xvv $dShow_aTe } in
                 letrec {
                   $dShow1_s1mc :: GHC.Show.Show (TestEq.Rec a_Xvv)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1mc =
                     GHC.Show.D:Show @ (TestEq.Rec a_Xvv) a2_s1mf a3_s1md a1_s1me;
                   a3_s1md :: TestEq.Rec a_Xvv -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1md = GHC.Show.$dmshow @ (TestEq.Rec a_Xvv) $dShow1_s1mc; } in
                 $dShow1_s1mc)

TestEq.$fReadFixity_readListDefault :: Text.ParserCombinators.ReadP.ReadS
                                         [TestEq.Fixity]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [TestEq.Fixity] TestEq.$fReadFixity8

TestEq.$fReadFixity :: GHC.Read.Read TestEq.Fixity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fReadFixity =
  GHC.Read.D:Read
    @ TestEq.Fixity
    TestEq.$fReadFixity9
    TestEq.$fReadFixity_readListDefault
    (TestEq.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec TestEq.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Fixity))
    (TestEq.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [TestEq.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Fixity]))

TestEq.$fOrdFixity_compare :: TestEq.Fixity
                              -> TestEq.Fixity
                              -> GHC.Ordering.Ordering
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity_compare =
  \ (a1_axy :: TestEq.Fixity) (b_axz :: TestEq.Fixity) ->
    let {
      $j_s1Od :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      LclId
      [Arity 1
       Str: DmdType L]
      $j_s1Od =
        \ (a#_axF :: GHC.Prim.Int#) ->
          let {
            $j1_s1Ob :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            LclId
            [Arity 1
             Str: DmdType L]
            $j1_s1Ob =
              \ (b#_axG :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_axF b#_axG of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_axF b#_axG of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a1_axy of _ {
                      TestEq.Prefix -> GHC.Ordering.EQ;
                      TestEq.Infix a11_axB a2_axC ->
                        case b_axz of _ {
                          TestEq.Prefix -> GHC.Ordering.EQ;
                          TestEq.Infix b1_axD b2_axE ->
                            case a11_axB of _ {
                              TestEq.LeftAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.RightAssociative -> GHC.Ordering.LT;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.RightAssociative ->
                                case b1_axD of _ {
                                  TestEq.LeftAssociative -> GHC.Ordering.GT;
                                  TestEq.RightAssociative -> GHC.Base.compareInt a2_axC b2_axE;
                                  TestEq.NotAssociative -> GHC.Ordering.LT
                                };
                              TestEq.NotAssociative ->
                                case b1_axD of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  TestEq.NotAssociative -> GHC.Base.compareInt a2_axC b2_axE
                                }
                            }
                        }
                    }
                } } in
          case b_axz of _ {
            TestEq.Prefix -> $j1_s1Ob 0; TestEq.Infix _ _ -> $j1_s1Ob 1
          } } in
    case a1_axy of _ {
      TestEq.Prefix -> $j_s1Od 0; TestEq.Infix _ _ -> $j_s1Od 1
    }

TestEq.$fOrdFixity6 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity6 =
  \ (x_a1j9 :: TestEq.Fixity) (y_a1ja :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1j9 y_a1ja of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

TestEq.$fOrdFixity5 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity5 =
  \ (x_a1jq :: TestEq.Fixity) (y_a1jr :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1jq y_a1jr of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

TestEq.$fOrdFixity4 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity4 =
  \ (x_a1jH :: TestEq.Fixity) (y_a1jI :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1jH y_a1jI of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

TestEq.$fOrdFixity3 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity3 =
  \ (x_a1jY :: TestEq.Fixity) (y_a1jZ :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1jY y_a1jZ of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

TestEq.$fOrdFixity2 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> TestEq.Fixity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity2 =
  \ (x_a1kf :: TestEq.Fixity) (y_a1kg :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1kf y_a1kg of _ {
      __DEFAULT -> y_a1kg; GHC.Ordering.GT -> x_a1kf
    }

TestEq.$fOrdFixity1 :: TestEq.Fixity
                       -> TestEq.Fixity
                       -> TestEq.Fixity
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdFixity1 =
  \ (x_a1kz :: TestEq.Fixity) (y_a1kA :: TestEq.Fixity) ->
    case TestEq.$fOrdFixity_compare x_a1kz y_a1kA of _ {
      __DEFAULT -> x_a1kz; GHC.Ordering.GT -> y_a1kA
    }

TestEq.$fShowFixity_showsPrec :: GHC.Types.Int
                                 -> TestEq.Fixity
                                 -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType LSL]
TestEq.$fShowFixity_showsPrec =
  \ (ds_d1ba :: GHC.Types.Int)
    (ds1_d1bb :: TestEq.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_d1bb of _ {
      TestEq.Prefix ->
        GHC.Base.++ @ GHC.Types.Char TestEq.$fReadFixity7 eta_B1;
      TestEq.Infix b1_axw b2_axx ->
        case ds_d1ba of _ { GHC.Types.I# x_a1Ea ->
        let {
          p_s1Of :: GHC.Show.ShowS
          LclId
          [Arity 1
           Str: DmdType L]
          p_s1Of =
            \ (x1_X1z8 :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                TestEq.$fShowFixity2
                (case b1_axw of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       TestEq.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1HD ->
                           GHC.Show.$wshowSignedInt 11 ww_a1HD x1_X1z8
                           }));
                   TestEq.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       TestEq.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1HD ->
                           GHC.Show.$wshowSignedInt 11 ww_a1HD x1_X1z8
                           }));
                   TestEq.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       TestEq.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_axx of _ { GHC.Types.I# ww_a1HD ->
                           GHC.Show.$wshowSignedInt 11 ww_a1HD x1_X1z8
                           }))
                 }) } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False -> p_s1Of eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.showSignedInt2
              (p_s1Of
                 (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 eta_B1))
        }
        }
    }

TestEq.$fShowFixity_showList :: [TestEq.Fixity] -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowFixity_showList =
  \ (ds1_a1gk :: [TestEq.Fixity]) (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (TestEq.$fShowFixity_showsPrec
             TestEq.$fShowFixity1
             x_a1gq
             (let {
                lvl12_s1Oh :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1Oh =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1Oj :: [TestEq.Fixity] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1Oj =
                  \ (ds2_a1gv :: [TestEq.Fixity]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1Oh;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (TestEq.$fShowFixity_showsPrec
                             TestEq.$fShowFixity1 y_a1gA (showl_s1Oj ys_a1gB))
                    }; } in
              showl_s1Oj xs_a1gr))
    }

TestEq.$fShowFixity3 :: TestEq.Fixity -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType S]
TestEq.$fShowFixity3 =
  \ (x_X1xU :: TestEq.Fixity) ->
    TestEq.$fShowFixity_showsPrec
      GHC.Base.zeroInt x_X1xU (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowFixity :: GHC.Show.Show TestEq.Fixity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowFixity =
  GHC.Show.D:Show
    @ TestEq.Fixity
    TestEq.$fShowFixity_showsPrec
    TestEq.$fShowFixity3
    TestEq.$fShowFixity_showList

TestEq.$fEqFixity_== :: TestEq.Fixity
                        -> TestEq.Fixity
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqFixity_== =
  \ (ds_d1b4 :: TestEq.Fixity) (ds1_d1b5 :: TestEq.Fixity) ->
    case ds_d1b4 of _ {
      TestEq.Prefix ->
        case ds1_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.True; TestEq.Infix _ _ -> GHC.Bool.False
        };
      TestEq.Infix a1_axl a2_axm ->
        case ds1_d1b5 of _ {
          TestEq.Prefix -> GHC.Bool.False;
          TestEq.Infix b1_axn b2_axo ->
            case a1_axl of _ {
              TestEq.LeftAssociative ->
                case b1_axn of _ {
                  TestEq.LeftAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1yv ->
                    case b2_axo of _ { GHC.Types.I# y_a1yz ->
                    GHC.Prim.==# x_a1yv y_a1yz
                    }
                    };
                  TestEq.RightAssociative -> GHC.Bool.False;
                  TestEq.NotAssociative -> GHC.Bool.False
                };
              TestEq.RightAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.RightAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1yv ->
                    case b2_axo of _ { GHC.Types.I# y_a1yz ->
                    GHC.Prim.==# x_a1yv y_a1yz
                    }
                    }
                };
              TestEq.NotAssociative ->
                case b1_axn of _ {
                  __DEFAULT -> GHC.Bool.False;
                  TestEq.NotAssociative ->
                    case a2_axm of _ { GHC.Types.I# x_a1yv ->
                    case b2_axo of _ { GHC.Types.I# y_a1yz ->
                    GHC.Prim.==# x_a1yv y_a1yz
                    }
                    }
                }
            }
        }
    }

TestEq.$fEqFixity_/= :: TestEq.Fixity
                        -> TestEq.Fixity
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqFixity_/= =
  \ (a1_axt :: TestEq.Fixity) (b_axu :: TestEq.Fixity) ->
    case TestEq.$fEqFixity_== a1_axt b_axu of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqFixity :: GHC.Classes.Eq TestEq.Fixity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fEqFixity =
  GHC.Classes.D:Eq
    @ TestEq.Fixity TestEq.$fEqFixity_== TestEq.$fEqFixity_/=

TestEq.$fOrdFixity :: GHC.Classes.Ord TestEq.Fixity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fOrdFixity =
  GHC.Classes.D:Ord
    @ TestEq.Fixity
    TestEq.$fEqFixity
    TestEq.$fOrdFixity_compare
    TestEq.$fOrdFixity6
    TestEq.$fOrdFixity5
    TestEq.$fOrdFixity4
    TestEq.$fOrdFixity3
    TestEq.$fOrdFixity2
    TestEq.$fOrdFixity1

TestEq.$fReadAssociativity_readListDefault :: Text.ParserCombinators.ReadP.ReadS
                                                [TestEq.Associativity]
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [TestEq.Associativity] TestEq.$fReadAssociativity16

TestEq.$fReadAssociativity :: GHC.Read.Read TestEq.Associativity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fReadAssociativity =
  GHC.Read.D:Read
    @ TestEq.Associativity
    TestEq.$fReadAssociativity17
    TestEq.$fReadAssociativity_readListDefault
    (TestEq.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  TestEq.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP TestEq.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec TestEq.Associativity))
    (TestEq.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [TestEq.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [TestEq.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [TestEq.Associativity]))

TestEq.$fOrdAssociativity_compare :: TestEq.Associativity
                                     -> TestEq.Associativity
                                     -> GHC.Ordering.Ordering
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fOrdAssociativity_compare =
  \ (a1_axg :: TestEq.Associativity)
    (b_axh :: TestEq.Associativity) ->
    case a1_axg of _ {
      TestEq.LeftAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.EQ;
          TestEq.RightAssociative -> GHC.Ordering.LT;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.RightAssociative ->
        case b_axh of _ {
          TestEq.LeftAssociative -> GHC.Ordering.GT;
          TestEq.RightAssociative -> GHC.Ordering.EQ;
          TestEq.NotAssociative -> GHC.Ordering.LT
        };
      TestEq.NotAssociative ->
        case b_axh of _ {
          __DEFAULT -> GHC.Ordering.GT;
          TestEq.NotAssociative -> GHC.Ordering.EQ
        }
    }

TestEq.$fShowAssociativity_showList :: [TestEq.Associativity]
                                       -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowAssociativity_showList =
  \ (ds1_a1gk :: [TestEq.Associativity])
    (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1Op [ALWAYS Just L] :: GHC.Base.String
             LclId
             [Str: DmdType]
             eta_s1Op =
               let {
                 lvl12_s1Ol :: [GHC.Types.Char]
                 LclId
                 [Str: DmdType]
                 lvl12_s1Ol =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
               letrec {
                 showl_s1On :: [TestEq.Associativity] -> [GHC.Types.Char]
                 LclId
                 [Arity 1
                  Str: DmdType S]
                 showl_s1On =
                   \ (ds2_a1gv :: [TestEq.Associativity]) ->
                     case ds2_a1gv of _ {
                       [] -> lvl12_s1Ol;
                       : y_a1gA ys_a1gB ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_a1gA of _ {
                              TestEq.LeftAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char
                                  TestEq.$fReadAssociativity15
                                  (showl_s1On ys_a1gB);
                              TestEq.RightAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char
                                  TestEq.$fReadAssociativity12
                                  (showl_s1On ys_a1gB);
                              TestEq.NotAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char TestEq.$fReadAssociativity9 (showl_s1On ys_a1gB)
                            })
                     }; } in
               showl_s1On xs_a1gr } in
           case x_a1gq of _ {
             TestEq.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity15 eta_s1Op;
             TestEq.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity12 eta_s1Op;
             TestEq.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity9 eta_s1Op
           })
    }

TestEq.$fShowAssociativity_showsPrec :: GHC.Types.Int
                                        -> TestEq.Associativity
                                        -> GHC.Show.ShowS
GblId
[Arity 3
 Str: DmdType ASL]
TestEq.$fShowAssociativity_showsPrec =
  __inline_me (\ _
                 (ds1_d1aU :: TestEq.Associativity)
                 (eta_B1 :: GHC.Base.String) ->
                 case ds1_d1aU of _ {
                   TestEq.LeftAssociative ->
                     GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity15 eta_B1;
                   TestEq.RightAssociative ->
                     GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity12 eta_B1;
                   TestEq.NotAssociative ->
                     GHC.Base.++ @ GHC.Types.Char TestEq.$fReadAssociativity9 eta_B1
                 })

TestEq.$fShowAssociativity :: GHC.Show.Show TestEq.Associativity
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowAssociativity =
  GHC.Show.D:Show
    @ TestEq.Associativity
    TestEq.$fShowAssociativity_showsPrec
    TestEq.$fShowAssociativity1
    TestEq.$fShowAssociativity_showList

TestEq.$fEqAssociativity_/= :: TestEq.Associativity
                               -> TestEq.Associativity
                               -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqAssociativity_/= =
  \ (a1_axe :: TestEq.Associativity)
    (b_axf :: TestEq.Associativity) ->
    case a1_axe of _ {
      TestEq.LeftAssociative ->
        case b_axf of _ {
          TestEq.LeftAssociative -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True;
          TestEq.NotAssociative -> GHC.Bool.True
        };
      TestEq.RightAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False
        };
      TestEq.NotAssociative ->
        case b_axf of _ {
          __DEFAULT -> GHC.Bool.True; TestEq.NotAssociative -> GHC.Bool.False
        }
    }

TestEq.$fEqAssociativity_== :: TestEq.Associativity
                               -> TestEq.Associativity
                               -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqAssociativity_== =
  \ (a1_axa :: TestEq.Associativity)
    (b_axb :: TestEq.Associativity) ->
    case a1_axa of _ {
      TestEq.LeftAssociative ->
        case b_axb of _ {
          TestEq.LeftAssociative -> GHC.Bool.True;
          TestEq.RightAssociative -> GHC.Bool.False;
          TestEq.NotAssociative -> GHC.Bool.False
        };
      TestEq.RightAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False;
          TestEq.RightAssociative -> GHC.Bool.True
        };
      TestEq.NotAssociative ->
        case b_axb of _ {
          __DEFAULT -> GHC.Bool.False; TestEq.NotAssociative -> GHC.Bool.True
        }
    }

TestEq.$fEqAssociativity :: GHC.Classes.Eq TestEq.Associativity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ TestEq.Associativity
    TestEq.$fEqAssociativity_==
    TestEq.$fEqAssociativity_/=

TestEq.$fOrdAssociativity :: GHC.Classes.Ord TestEq.Associativity
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ TestEq.Associativity
    TestEq.$fEqAssociativity
    TestEq.$fOrdAssociativity_compare
    TestEq.$fOrdAssociativity6
    TestEq.$fOrdAssociativity5
    TestEq.$fOrdAssociativity4
    TestEq.$fOrdAssociativity3
    TestEq.$fOrdAssociativity2
    TestEq.$fOrdAssociativity1

Rec {
TestEq.$fEqLogic0_== :: TestEq.Logic
                        -> TestEq.Logic
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqLogic0_== =
  \ (ds_d1aD :: TestEq.Logic) (ds1_d1aE :: TestEq.Logic) ->
    let {
      $wfail_s1Or :: GHC.Prim.State# GHC.Prim.RealWorld -> GHC.Bool.Bool
      LclId
      [Arity 1
       Str: DmdType A]
      $wfail_s1Or =
        \ _ ->
          case ds_d1aD of _ {
            TestEq.VarL _ ->
              case ds1_d1aE of _ {
                TestEq.VarL _ -> GHC.Bool.True;
                TestEq.T -> GHC.Bool.False;
                TestEq.F -> GHC.Bool.False;
                TestEq.Not _ -> GHC.Bool.False;
                TestEq.Impl _ _ -> GHC.Bool.False;
                TestEq.Equiv _ _ -> GHC.Bool.False;
                TestEq.Conj _ _ -> GHC.Bool.False;
                TestEq.Disj _ _ -> GHC.Bool.False
              };
            TestEq.T ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.T -> GHC.Bool.True
              };
            TestEq.F ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.F -> GHC.Bool.True
              };
            TestEq.Not _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Not _ -> GHC.Bool.True
              };
            TestEq.Impl _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Impl _ _ -> GHC.Bool.True
              };
            TestEq.Equiv _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Equiv _ _ -> GHC.Bool.True
              };
            TestEq.Conj _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Conj _ _ -> GHC.Bool.True
              };
            TestEq.Disj _ _ ->
              case ds1_d1aE of _ {
                __DEFAULT -> GHC.Bool.False; TestEq.Disj _ _ -> GHC.Bool.True
              }
          } } in
    case ds_d1aD of _ {
      __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
      TestEq.VarL a1_awK ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.VarL b1_awL -> GHC.Base.eqString a1_awK b1_awL
        };
      TestEq.Not a1_awM ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Not b1_awN -> TestEq.$fEqLogic0_== a1_awM b1_awN
        };
      TestEq.Impl a1_awO a2_awP ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Impl b1_awQ b2_awR ->
            case TestEq.$fEqLogic0_== a1_awO b1_awQ of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_awP b2_awR
            }
        };
      TestEq.Equiv a1_awS a2_awT ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Equiv b1_awU b2_awV ->
            case TestEq.$fEqLogic0_== a1_awS b1_awU of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_awT b2_awV
            }
        };
      TestEq.Conj a1_awW a2_awX ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Conj b1_awY b2_awZ ->
            case TestEq.$fEqLogic0_== a1_awW b1_awY of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_awX b2_awZ
            }
        };
      TestEq.Disj a1_ax0 a2_ax1 ->
        case ds1_d1aE of _ {
          __DEFAULT -> $wfail_s1Or GHC.Prim.realWorld#;
          TestEq.Disj b1_ax2 b2_ax3 ->
            case TestEq.$fEqLogic0_== a1_ax0 b1_ax2 of _ {
              GHC.Bool.False -> GHC.Bool.False;
              GHC.Bool.True -> TestEq.$fEqLogic0_== a2_ax1 b2_ax3
            }
        }
    }
end Rec }

TestEq.$fEqLogic0_/= :: TestEq.Logic
                        -> TestEq.Logic
                        -> GHC.Bool.Bool
GblId
[Arity 2
 NoCafRefs
 Str: DmdType SS]
TestEq.$fEqLogic0_/= =
  \ (a1_ax8 :: TestEq.Logic) (b_ax9 :: TestEq.Logic) ->
    case TestEq.$fEqLogic0_== a1_ax8 b_ax9 of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

TestEq.$fEqLogic0 :: GHC.Classes.Eq TestEq.Logic
GblId[DFunId]
[NoCafRefs
 Str: DmdType m]
TestEq.$fEqLogic0 =
  GHC.Classes.D:Eq
    @ TestEq.Logic TestEq.$fEqLogic0_== TestEq.$fEqLogic0_/=

Rec {
TestEq.$fShowLogic_$sshowsPrec :: TestEq.Logic
                                  -> GHC.Prim.Int#
                                  -> GHC.Base.String
                                  -> [GHC.Types.Char]
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowLogic_$sshowsPrec =
  \ (sc_s1QY :: TestEq.Logic) (sc1_s1QZ :: GHC.Prim.Int#) ->
    case sc_s1QY of _ {
      TestEq.VarL b1_awv ->
        case GHC.Prim.>=# sc1_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl9_r1VN
                (GHC.Show.$fShowChar_showList b1_awv x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl9_r1VN
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb))))
        };
      TestEq.T -> lvl8_r1VL;
      TestEq.F -> lvl7_r1VH;
      TestEq.Not b1_awx ->
        let {
          g_s1Ot [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ot = TestEq.$fShowLogic_$sshowsPrec b1_awx 11 } in
        case GHC.Prim.>=# sc1_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl6_r1VD (g_s1Ot x_a1hi);
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl6_r1VD
                   (g_s1Ot
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))
        };
      TestEq.Impl b1_awz b2_awA ->
        let {
          g_s1Ox [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ox = TestEq.$fShowLogic_$sshowsPrec b2_awA 11 } in
        let {
          f_s1Ov [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Ov = TestEq.$fShowLogic_$sshowsPrec b1_awz 11 } in
        case GHC.Prim.>=# sc1_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl5_r1VB
                (f_s1Ov
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Ox x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl5_r1VB
                   (f_s1Ov
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Ox
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Equiv b1_awC b2_awD ->
        let {
          g_s1OB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OB = TestEq.$fShowLogic_$sshowsPrec b2_awD 11 } in
        let {
          f_s1Oz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Oz = TestEq.$fShowLogic_$sshowsPrec b1_awC 11 } in
        case GHC.Prim.>=# sc1_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl4_r1Vz
                (f_s1Oz
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OB x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl4_r1Vz
                   (f_s1Oz
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OB
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Conj b1_awF b2_awG ->
        let {
          g_s1OF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OF = TestEq.$fShowLogic_$sshowsPrec b2_awG 11 } in
        let {
          f_s1OD [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OD = TestEq.$fShowLogic_$sshowsPrec b1_awF 11 } in
        case GHC.Prim.>=# sc1_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl3_r1Vx
                (f_s1OD
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OF x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl3_r1Vx
                   (f_s1OD
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OF
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        };
      TestEq.Disj b1_awI b2_awJ ->
        let {
          g_s1OJ [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OJ = TestEq.$fShowLogic_$sshowsPrec b2_awJ 11 } in
        let {
          f_s1OH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OH = TestEq.$fShowLogic_$sshowsPrec b1_awI 11 } in
        case GHC.Prim.>=# sc1_s1QZ 11 of _ {
          GHC.Bool.False ->
            \ (x_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl2_r1Vv
                (f_s1OH
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OJ x_X1Au)));
          GHC.Bool.True ->
            \ (x_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl2_r1Vv
                   (f_s1OH
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OJ
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x_a1hb)))))
        }
    }
end Rec }

TestEq.$fShowLogic_showsPrec :: GHC.Types.Int
                                -> TestEq.Logic
                                -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType LS]
TestEq.$fShowLogic_showsPrec =
  \ (a1_awu :: GHC.Types.Int) (ds_d1au :: TestEq.Logic) ->
    case ds_d1au of _ {
      TestEq.VarL b1_awv ->
        case a1_awu of _ { GHC.Types.I# x_a1Ea ->
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x1_a1hi :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl9_r1VN
                (GHC.Show.$fShowChar_showList b1_awv x1_a1hi);
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl9_r1VN
                   (GHC.Types.:
                      @ GHC.Types.Char
                      GHC.Show.$fShowChar1
                      (GHC.Show.$fShowChar_showl
                         b1_awv
                         (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb))))
        }
        };
      TestEq.T -> lvl8_r1VL;
      TestEq.F -> lvl7_r1VH;
      TestEq.Not b1_awx ->
        case a1_awu of _ { GHC.Types.I# x_a1Ea ->
        let {
          g_s1Ot [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ot = TestEq.$fShowLogic_$sshowsPrec b1_awx 11 } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x1_a1hi :: GHC.Base.String) ->
              GHC.Base.++ @ GHC.Types.Char lvl6_r1VD (g_s1Ot x1_a1hi);
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl6_r1VD
                   (g_s1Ot
                      (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))
        }
        };
      TestEq.Impl b1_awz b2_awA ->
        case a1_awu of _ { GHC.Types.I# x_a1Ea ->
        let {
          g_s1Ox [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1Ox = TestEq.$fShowLogic_$sshowsPrec b2_awA 11 } in
        let {
          f_s1Ov [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Ov = TestEq.$fShowLogic_$sshowsPrec b1_awz 11 } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl5_r1VB
                (f_s1Ov
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1Ox x1_X1Au)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl5_r1VB
                   (f_s1Ov
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1Ox
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        };
      TestEq.Equiv b1_awC b2_awD ->
        case a1_awu of _ { GHC.Types.I# x_a1Ea ->
        let {
          g_s1OB [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OB = TestEq.$fShowLogic_$sshowsPrec b2_awD 11 } in
        let {
          f_s1Oz [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1Oz = TestEq.$fShowLogic_$sshowsPrec b1_awC 11 } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl4_r1Vz
                (f_s1Oz
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OB x1_X1Au)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl4_r1Vz
                   (f_s1Oz
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OB
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        };
      TestEq.Conj b1_awF b2_awG ->
        case a1_awu of _ { GHC.Types.I# x_a1Ea ->
        let {
          g_s1OF [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OF = TestEq.$fShowLogic_$sshowsPrec b2_awG 11 } in
        let {
          f_s1OD [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OD = TestEq.$fShowLogic_$sshowsPrec b1_awF 11 } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl3_r1Vx
                (f_s1OD
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OF x1_X1Au)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl3_r1Vx
                   (f_s1OD
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OF
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        };
      TestEq.Disj b1_awI b2_awJ ->
        case a1_awu of _ { GHC.Types.I# x_a1Ea ->
        let {
          g_s1OJ [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          g_s1OJ = TestEq.$fShowLogic_$sshowsPrec b2_awJ 11 } in
        let {
          f_s1OH [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
          LclId
          [Str: DmdType]
          f_s1OH = TestEq.$fShowLogic_$sshowsPrec b1_awI 11 } in
        case GHC.Prim.>=# x_a1Ea 11 of _ {
          GHC.Bool.False ->
            \ (x1_X1Au :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl2_r1Vv
                (f_s1OH
                   (GHC.Types.:
                      @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OJ x1_X1Au)));
          GHC.Bool.True ->
            \ (x1_a1hb :: GHC.Base.String) ->
              GHC.Types.:
                @ GHC.Types.Char
                GHC.Show.showSignedInt2
                (GHC.Base.++
                   @ GHC.Types.Char
                   lvl2_r1Vv
                   (f_s1OH
                      (GHC.Types.:
                         @ GHC.Types.Char
                         GHC.Show.showSpace1
                         (g_s1OJ
                            (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb)))))
        }
        }
    }

TestEq.$fShowLogic_showList :: [TestEq.Logic] -> GHC.Show.ShowS
GblId
[Arity 2
 Str: DmdType SL]
TestEq.$fShowLogic_showList =
  \ (ds1_a1gk :: [TestEq.Logic]) (s_a1gl :: GHC.Base.String) ->
    case ds1_a1gk of _ {
      [] -> GHC.Base.++ @ GHC.Types.Char GHC.Show.showList__4 s_a1gl;
      : x_a1gq xs_a1gr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (TestEq.$fShowLogic_$sshowsPrec
             x_a1gq
             0
             (let {
                lvl12_s1OL :: [GHC.Types.Char]
                LclId
                [Str: DmdType]
                lvl12_s1OL =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1gl } in
              letrec {
                showl_s1ON :: [TestEq.Logic] -> [GHC.Types.Char]
                LclId
                [Arity 1
                 Str: DmdType S]
                showl_s1ON =
                  \ (ds2_a1gv :: [TestEq.Logic]) ->
                    case ds2_a1gv of _ {
                      [] -> lvl12_s1OL;
                      : y_a1gA ys_a1gB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (TestEq.$fShowLogic_$sshowsPrec y_a1gA 0 (showl_s1ON ys_a1gB))
                    }; } in
              showl_s1ON xs_a1gr))
    }

TestEq.$fShowLogic1 :: TestEq.Logic -> GHC.Base.String
GblId
[Arity 1
 Str: DmdType S]
TestEq.$fShowLogic1 =
  \ (x_X1z6 :: TestEq.Logic) ->
    TestEq.$fShowLogic_showsPrec
      GHC.Base.zeroInt x_X1z6 (GHC.Types.[] @ GHC.Types.Char)

TestEq.$fShowLogic :: GHC.Show.Show TestEq.Logic
GblId[DFunId]
[Str: DmdType m]
TestEq.$fShowLogic =
  GHC.Show.D:Show
    @ TestEq.Logic
    TestEq.$fShowLogic_showsPrec
    TestEq.$fShowLogic1
    TestEq.$fShowLogic_showList

Rec {
TestEq.$fEqTree0_== :: forall a_afw.
                       (GHC.Classes.Eq a_afw) =>
                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEqTree0_== =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1OP :: TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1OP = TestEq.$fEqTree0_== @ a_afw $dEq_aGJ } in
    \ (ds_d1ao :: TestEq.Tree a_afw) (ds1_d1ap :: TestEq.Tree a_afw) ->
      case ds_d1ao of _ {
        TestEq.Leaf ->
          case ds1_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.True; TestEq.Bin _ _ _ -> GHC.Bool.False
          };
        TestEq.Bin a1_awi a2_awj a3_awk ->
          case ds1_d1ap of _ {
            TestEq.Leaf -> GHC.Bool.False;
            TestEq.Bin b1_awl b2_awm b3_awn ->
              case $dEq_aGJ of _ { GHC.Classes.D:Eq tpl1_Xjj _ ->
              case tpl1_Xjj a1_awi b1_awl of _ {
                GHC.Bool.False -> GHC.Bool.False;
                GHC.Bool.True ->
                  case ==_s1OP a2_awj b2_awm of _ {
                    GHC.Bool.False -> GHC.Bool.False;
                    GHC.Bool.True -> ==_s1OP a3_awk b3_awn
                  }
              }
              }
          }
      }
end Rec }

TestEq.$fEqTree0_/= :: forall a_afw.
                       (GHC.Classes.Eq a_afw) =>
                       TestEq.Tree a_afw -> TestEq.Tree a_afw -> GHC.Bool.Bool
GblId
[Arity 1
 NoCafRefs
 Str: DmdType L]
TestEq.$fEqTree0_/= =
  \ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
    let {
      ==_s1OR [ALWAYS Just L] :: TestEq.Tree a_afw
                                 -> TestEq.Tree a_afw
                                 -> GHC.Bool.Bool
      LclId
      [Str: DmdType]
      ==_s1OR = TestEq.$fEqTree0_== @ a_afw $dEq_aGJ } in
    \ (a1_aws :: TestEq.Tree a_afw) (b_awt :: TestEq.Tree a_afw) ->
      case ==_s1OR a1_aws b_awt of _ {
        GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
      }

TestEq.$fEqTree0 :: forall a_afw.
                    (GHC.Classes.Eq a_afw) =>
                    GHC.Classes.Eq (TestEq.Tree a_afw)
GblId[DFunId]
[Arity 1
 NoCafRefs
 Str: DmdType Lm]
TestEq.$fEqTree0 =
  __inline_me (\ (@ a_afw) ($dEq_aGJ :: GHC.Classes.Eq a_afw) ->
                 GHC.Classes.D:Eq
                   @ (TestEq.Tree a_afw)
                   (TestEq.$fEqTree0_== @ a_afw $dEq_aGJ)
                   (TestEq.$fEqTree0_/= @ a_afw $dEq_aGJ))

Rec {
TestEq.$fShowTree_showsPrec :: forall a_afw.
                               (GHC.Show.Show a_afw) =>
                               GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fShowTree_showsPrec =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    let {
      a1_s1OT :: GHC.Types.Int -> TestEq.Tree a_afw -> GHC.Show.ShowS
      LclId
      [Str: DmdType]
      a1_s1OT = TestEq.$fShowTree_showsPrec @ a_afw $dShow_aFW } in
    \ (ds_d1ak :: GHC.Types.Int) (ds1_d1al :: TestEq.Tree a_afw) ->
      case ds1_d1al of _ {
        TestEq.Leaf -> lvl1_r1Vt;
        TestEq.Bin b1_awf b2_awg b3_awh ->
          case ds_d1ak of _ { GHC.Types.I# x_a1Ea ->
          let {
            g_s1OZ [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            g_s1OZ = a1_s1OT TestEq.$fRead:+:2 b3_awh } in
          let {
            f_s1OX [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f_s1OX = a1_s1OT TestEq.$fRead:+:2 b2_awg } in
          let {
            f1_s1OV [ALWAYS Just L] :: GHC.Base.String -> GHC.Base.String
            LclId
            [Str: DmdType]
            f1_s1OV =
              case $dShow_aFW of _ { GHC.Show.D:Show tpl1_XjE _ _ ->
              tpl1_XjE TestEq.$fRead:+:2 b1_awf
              } } in
          let {
            p_s1P1 :: GHC.Show.ShowS
            LclId
            [Arity 1
             Str: DmdType L]
            p_s1P1 =
              \ (x1_X1B8 :: GHC.Base.String) ->
                GHC.Base.++
                  @ GHC.Types.Char
                  lvl_r1Vq
                  (f1_s1OV
                     (GHC.Types.:
                        @ GHC.Types.Char
                        GHC.Show.showSpace1
                        (f_s1OX
                           (GHC.Types.:
                              @ GHC.Types.Char GHC.Show.showSpace1 (g_s1OZ x1_X1B8))))) } in
          case GHC.Prim.>=# x_a1Ea 11 of _ {
            GHC.Bool.False -> p_s1P1;
            GHC.Bool.True ->
              \ (x1_a1hb :: GHC.Base.String) ->
                GHC.Types.:
                  @ GHC.Types.Char
                  GHC.Show.showSignedInt2
                  (p_s1P1
                     (GHC.Types.: @ GHC.Types.Char GHC.Show.showSignedInt1 x1_a1hb))
          }
          }
      }
end Rec }

TestEq.$fShowTree_showList :: forall a_afw.
                              (GHC.Show.Show a_afw) =>
                              [TestEq.Tree a_afw] -> GHC.Show.ShowS
GblId
[Arity 1
 Str: DmdType L]
TestEq.$fShowTree_showList =
  \ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
    GHC.Show.showList__
      @ (TestEq.Tree a_afw)
      (TestEq.$fShowTree_showsPrec
         @ a_afw $dShow_aFW TestEq.$fShowFixity1)

TestEq.$fShowTree :: forall a_afw.
                     (GHC.Show.Show a_afw) =>
                     GHC.Show.Show (TestEq.Tree a_afw)
GblId[DFunId]
[Arity 1
 Str: DmdType Lm]
TestEq.$fShowTree =
  __inline_me (\ (@ a_afw) ($dShow_aFW :: GHC.Show.Show a_afw) ->
                 let {
                   a1_s1lR [ALWAYS Just L] :: [TestEq.Tree a_afw] -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a1_s1lR = TestEq.$fShowTree_showList @ a_afw $dShow_aFW } in
                 let {
                   a2_s1lS [ALWAYS Just L] :: GHC.Types.Int
                                              -> TestEq.Tree a_afw
                                              -> GHC.Show.ShowS
                   LclId
                   [Str: DmdType]
                   a2_s1lS = TestEq.$fShowTree_showsPrec @ a_afw $dShow_aFW } in
                 letrec {
                   $dShow1_s1lP :: GHC.Show.Show (TestEq.Tree a_afw)
                   LclId
                   [Str: DmdType m]
                   $dShow1_s1lP =
                     GHC.Show.D:Show @ (TestEq.Tree a_afw) a2_s1lS a3_s1lQ a1_s1lR;
                   a3_s1lQ :: TestEq.Tree a_afw -> GHC.Base.String
                   LclId
                   [Str: DmdType]
                   a3_s1lQ = GHC.Show.$dmshow @ (TestEq.Tree a_afw) $dShow1_s1lP; } in
                 $dShow1_s1lP)




==================== Tidy Core Rules ====================
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_F_]" ALWAYS
    forall {$dConstructor_s1oF [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_F_}
      TestEq.$dmconIsRecord @ TestEq.Logic_F_ $dConstructor_s1oF
      = TestEq.$dmconIsRecord_$s$dmconIsRecord
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_T_]" ALWAYS
    forall {$dConstructor_s1oI [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_T_}
      TestEq.$dmconIsRecord @ TestEq.Logic_T_ $dConstructor_s1oI
      = TestEq.$dmconIsRecord_$s$dmconIsRecord1
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Not_]" ALWAYS
    forall {$dConstructor_s1oL [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Not_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Not_ $dConstructor_s1oL
      = TestEq.$dmconIsRecord_$s$dmconIsRecord2
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s1oO [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Disj_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Disj_ $dConstructor_s1oO
      = TestEq.$dmconIsRecord_$s$dmconIsRecord3
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s1oR [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Conj_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Conj_ $dConstructor_s1oR
      = TestEq.$dmconIsRecord_$s$dmconIsRecord4
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s1oU [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Equiv_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Equiv_ $dConstructor_s1oU
      = TestEq.$dmconIsRecord_$s$dmconIsRecord5
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s1oX [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Impl_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Impl_ $dConstructor_s1oX
      = TestEq.$dmconIsRecord_$s$dmconIsRecord6
"SPEC TestEq.$dmconIsRecord [TestEq.Logic_Var_]" ALWAYS
    forall {$dConstructor_s1p0 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Var_}
      TestEq.$dmconIsRecord @ TestEq.Logic_Var_ $dConstructor_s1p0
      = TestEq.$dmconIsRecord_$s$dmconIsRecord7
"SPEC TestEq.$dmconIsRecord [TestEq.Leaf]" ALWAYS
    forall {$dConstructor_s1p3 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Leaf}
      TestEq.$dmconIsRecord @ TestEq.Leaf $dConstructor_s1p3
      = TestEq.$dmconIsRecord_$s$dmconIsRecord8
"SPEC TestEq.$dmconIsRecord [TestEq.Bin]" ALWAYS
    forall {$dConstructor_s1p6 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Bin}
      TestEq.$dmconIsRecord @ TestEq.Bin $dConstructor_s1p6
      = TestEq.$dmconIsRecord_$s$dmconIsRecord9
"SPEC TestEq.$dmconIsRecord [TestEq.List_Cons_]" ALWAYS
    forall {$dConstructor_s1p9 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.List_Cons_}
      TestEq.$dmconIsRecord @ TestEq.List_Cons_ $dConstructor_s1p9
      = TestEq.$dmconIsRecord_$s$dmconIsRecord10
"SPEC TestEq.$dmconIsRecord [TestEq.List_Nil_]" ALWAYS
    forall {$dConstructor_s1pc [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.List_Nil_}
      TestEq.$dmconIsRecord @ TestEq.List_Nil_ $dConstructor_s1pc
      = TestEq.$dmconIsRecord_$s$dmconIsRecord11
"SPEC TestEq.$dmconFixity [TestEq.Logic_F_]" ALWAYS
    forall {$dConstructor_s1o8 [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_F_}
      TestEq.$dmconFixity @ TestEq.Logic_F_ $dConstructor_s1o8
      = TestEq.$dmconFixity_$s$dmconFixity10
"SPEC TestEq.$dmconFixity [TestEq.Logic_T_]" ALWAYS
    forall {$dConstructor_s1ob [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_T_}
      TestEq.$dmconFixity @ TestEq.Logic_T_ $dConstructor_s1ob
      = TestEq.$dmconFixity_$s$dmconFixity9
"SPEC TestEq.$dmconFixity [TestEq.Logic_Not_]" ALWAYS
    forall {$dConstructor_s1oe [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Not_}
      TestEq.$dmconFixity @ TestEq.Logic_Not_ $dConstructor_s1oe
      = TestEq.$dmconFixity_$s$dmconFixity8
"SPEC TestEq.$dmconFixity [TestEq.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s1oh [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Disj_}
      TestEq.$dmconFixity @ TestEq.Logic_Disj_ $dConstructor_s1oh
      = TestEq.$dmconFixity_$s$dmconFixity7
"SPEC TestEq.$dmconFixity [TestEq.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s1ok [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Conj_}
      TestEq.$dmconFixity @ TestEq.Logic_Conj_ $dConstructor_s1ok
      = TestEq.$dmconFixity_$s$dmconFixity6
"SPEC TestEq.$dmconFixity [TestEq.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s1on [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Equiv_}
      TestEq.$dmconFixity @ TestEq.Logic_Equiv_ $dConstructor_s1on
      = TestEq.$dmconFixity_$s$dmconFixity5
"SPEC TestEq.$dmconFixity [TestEq.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s1oq [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Impl_}
      TestEq.$dmconFixity @ TestEq.Logic_Impl_ $dConstructor_s1oq
      = TestEq.$dmconFixity_$s$dmconFixity4
"SPEC TestEq.$dmconFixity [TestEq.Logic_Var_]" ALWAYS
    forall {$dConstructor_s1ot [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Logic_Var_}
      TestEq.$dmconFixity @ TestEq.Logic_Var_ $dConstructor_s1ot
      = TestEq.$dmconFixity_$s$dmconFixity3
"SPEC TestEq.$dmconFixity [TestEq.Leaf]" ALWAYS
    forall {$dConstructor_s1ow [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Leaf}
      TestEq.$dmconFixity @ TestEq.Leaf $dConstructor_s1ow
      = TestEq.$dmconFixity_$s$dmconFixity2
"SPEC TestEq.$dmconFixity [TestEq.Bin]" ALWAYS
    forall {$dConstructor_s1oz [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.Bin}
      TestEq.$dmconFixity @ TestEq.Bin $dConstructor_s1oz
      = TestEq.$dmconFixity_$s$dmconFixity1
"SPEC TestEq.$dmconFixity [TestEq.List_Nil_]" ALWAYS
    forall {$dConstructor_s1oC [ALWAYS Dead Nothing] :: TestEq.Constructor
                                                          TestEq.List_Nil_}
      TestEq.$dmconFixity @ TestEq.List_Nil_ $dConstructor_s1oC
      = TestEq.$dmconFixity_$s$dmconFixity
"SC:showsPrec0" [0]
    forall {sc_s1QY :: TestEq.Logic sc1_s1QZ :: GHC.Prim.Int#}
      TestEq.$fShowLogic_showsPrec (GHC.Types.I# sc1_s1QZ) sc_s1QY
      = TestEq.$fShowLogic_$sshowsPrec sc_s1QY sc1_s1QZ


