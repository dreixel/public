\documentclass{article}

\usepackage{xcolor}

\addtolength{\oddsidemargin}{-1.5in}
\addtolength{\evensidemargin}{-1.5in}
\addtolength{\textwidth}{3in}

\addtolength{\topmargin}{-.875in}
\addtolength{\textheight}{1.75in}


\newcommand{\ty}[1]{\mathit{#1}}
\newcommand{\con}[1]{{\color{blue}\mathit{#1}}}
\newcommand{\consyml}[1]{\mathrel{\con{\mathord{#1}}}}
\newcommand{\consym}[1]{\mathrel{\con{\mathord{#1}}}}
\newcommand{\id}[1]{\mathit{#1}}
\newcommand{\tyid}[1]{{\color{red}\ty{#1}}}
\newcommand{\keyw}[1]{\mathbf{#1}}
\newcommand{\tycon}[1]{{\color{red}\ty{#1}}}
\newcommand{\tyconsyml}[1]{\mathrel{\tycon{\mathord{#1}}}}
\newcommand{\tyconsym}[1]{\mathrel{\tycon{\mathord{#1}}}}
\newcommand{\tycls}[1]{{\color{red}\ty{#1}}}

%if False
\definecolor{blue}{named}{black}
\definecolor{red}{named}{black}
\definecolor{darkgreen}{named}{black}
\definecolor{orange}{named}{black}
\definecolor{green}{named}{black}
\renewcommand{\ty}[1]{\mathsf{#1}}
%endif

%include polycode.fmt

% Equational reasoning
% (note that this redefines the standard look of multi-line comments)
\def\commentbegin{\quad\{\ }
\def\commentend{\}}
%format ===           = "\equiv"
%format ==>           = "\Rightarrow"
%format QED           = "_{\blacksquare}"
%format Forall(x)     = "\forall_{" x "}"
%format In            = "\in"

%format //            = "\texttt{\{-\#}\enspace"
%format \\            = "\enspace\texttt{\#-\}}"

%format U             = "\tycon{U}"
%format :+:           = "\tyconsym{+}"
%format :*:           = "\tyconsym{\times}"
%format U_            = "\con{U}"
%format L             = "\con{L}"
%format R             = "\con{R}"
%format ::*::         = "\consym{\times}"
%format Leaf          = "\con{Leaf}"
%format Bin           = "\con{Bin}"

%format toTree        = to "_{" Tree "}"
%format fromTree      = from "_{" Tree "}"
%format toInt         = to "_{" Int "}"
%format fromInt       = from "_{" Int "}"
%format t1            = t "_{1}"
%format t2            = t "_{2}"
%format r1            = r "_{1}"
%format r2            = r "_{2}"
%format x0            = x "_{1}"
%format l0            = l "_{2}"
%format r0            = r "_{2}"

%format eqT = eq "_{*}"
%format eqP = eq "_{+}"
%format eqU = eq "_{U}"

\begin{document}

\section{Notes on Simplify.lhs}

Note [Case elimination]

The case-elimination transformation discards redundant case expressions.
Start with a simple situation:
\begin{spec}
        case x# of      ===>   e[x#/y#]
          y# -> e
\end{spec}
(when |x#, y#| are of primitive type, of course).  We can't (in general)
do this for algebraic cases, because we might turn bottom into
non-bottom!

The code in SimplUtils.prepareAlts has the effect of generalise this
idea to look for a case where we're scrutinising a variable, and we
know that only the default case can match.  For example:
\begin{spec}
        case x of
          0#      -> ...
          DEFAULT -> ...(case x of
                         0#      -> ...
                         DEFAULT -> ...) ...
\end{spec}
Here the inner case is first trimmed to have only one alternative, the
DEFAULT, after which it's an instance of the previous case.  This
really only shows up in eliminating error-checking code.

We also make sure that we deal with this very common case:
\begin{spec}
        case e of
          x -> ...x...
\end{spec}
Here we are using the case as a strict let; if x is used only once
then we want to inline it.  We have to be careful that this doesn't
make the program terminate when it would have diverged before, so we
check that
        - e is already evaluated (it may so if e is a variable)
        - x is used strictly, or

Lastly, the code in SimplUtils.mkCase combines identical RHSs.  So
\begin{spec}
        case e of       ===> case e of DEFAULT -> r
           True  -> r
           False -> r
\end{spec}
Now again the case may be elminated by the CaseElim transformation.


Further notes about case elimination

Consider:       
\begin{spec}
                test :: Integer -> IO ()
                test = print
\end{spec}
Turns out that this compiles to:
\begin{spec}
    Print.test
      = \ eta :: Integer
          eta1 :: State# RealWorld ->
          case PrelNum.< eta PrelNum.zeroInteger of wild { __DEFAULT ->
          case hPutStr stdout
                 (PrelNum.jtos eta ($w[] @@ Char))
                 eta1
          of wild1 { (# new_s, a4 #) -> PrelIO.lvl23 new_s  }}
\end{spec}
Notice the strange |<| which has no effect at all. This is a funny one.
It started like this:
\begin{spec}
f x y = if x < 0 then jtos x
          else if y==0 then "" else jtos x
\end{spec}
At a particular call site we have (f v 1).  So we inline to get
\begin{spec}
        if v < 0 then jtos x
        else if 1==0 then "" else jtos x
\end{spec}
Now simplify the 1==0 conditional:
\begin{spec}
        if v<0 then jtos v else jtos v
\end{spec}
Now common-up the two branches of the case:
\begin{spec}
        case (v<0) of DEFAULT -> jtos v
\end{spec}
Why don't we drop the case?  Because it's strict in v.  It's technically
wrong to drop even unnecessary evaluations, and in practice they
may be a result of |seq| so we *definitely* don't want to drop those.
I don't really know how to improve this situation.


\section{Worked-out specialization and simplification of generic equality}

\begin{spec}
data U        = U_             deriving (Show)
data a :+: b  = L a | R b      deriving (Show)
data a :*: b  = a ::*:: b      deriving (Show)
\end{spec}

\begin{spec}
eq x y = eq' (from x) (from y)

eq' :: (...) => a -> a -> Bool
eqU _ _   =   True
eqP x y   =   case x of
                (L x) ->  case y of 
                            (L x') ->   eq' x x'
                            (R x') ->   False
                (R x) ->   case y of 
                            (L x') ->   False
                            (R x') ->   eq' x x'
eqT x y   =   case x of 
                (a ::*:: b) -> case y of (a' ::*:: b') -> eq' a a' && eq' b b'
\end{spec}

\begin{spec}
data Tree a = Leaf | Bin a (Tree a) (Tree a) deriving (Show)

fromTree x = case x of 
  Bin x l r  -> R (x ::*:: l ::*:: r)
  Leaf       -> L U_
toTree x = case x of
  R (x ::*:: l ::*:: r)  -> Bin x l r
  L U_                -> Leaf
\end{spec}

\begin{spec}
eq' (fromTree t1) (fromTree t2)

=== {- def fromTree twice -}

eq' (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ }) (case t2 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })

=== {- spec eq' to eq+ -}

eqP (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ }) (case t2 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })

=== {- def eq+ -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L l)  -> case (case t2 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of 
                (L l') -> eq' l l'
                (R _) -> False
    (R r)  -> case (case t2 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of 
                (L _) -> False
                (R r') -> eq' r r'
=== {- case-case fusion twice -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L ll)  -> case t2 of { Bin x l r -> False; Leaf -> eq' ll U_ }
    (R rr)  -> case t2 of { Bin x l r -> eq' rr (x ::*:: l ::*:: r); Leaf -> False }

=== {- spec eq' twice -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L ll)  -> case t2 of { Bin x l r -> False; Leaf -> eqU ll U_ }
    (R rr)  -> case t2 of { Bin x l r -> eqT rr (x ::*:: l ::*:: r); Leaf -> False }

=== {- def eqU, eq* -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L ll)  -> case t2 of {   Bin x l r   ->  False; Leaf -> True }
    (R rr)  -> case t2 of {   Bin x l r   ->  case rr of 
                                                (r1 ::*:: r2) ->  case (x ::*:: l ::*:: r) of
                                                                    (r'1 ::*:: r'2) -> eq' r1 r'1 && eq' r2 r'2
                              Leaf        -> False }

=== {- constant case -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L ll)  -> case t2 of {  Bin x l r   ->   False; Leaf -> True }
    (R rr)  -> case t2 of {  Bin x l r   ->   case rr of 
                                                 (r1 ::*:: r2) -> eq' r1 x && eq' r2 (l ::*:: r);
                             Leaf        -> False }

=== {- spec eq' -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L ll)  -> case t2 of {  Bin x l r   ->  False; Leaf -> True }
    (R rr)  -> case t2 of {  Bin x l r   ->  case rr of 
                                               (r1 ::*:: r2) -> eq' r1 x && eqT r2 (l ::*:: r);
                             Leaf        -> False }

=== {- def eq*, constant case -}

case (case t1 of { Bin x l r -> R (x ::*:: l ::*:: r); Leaf -> L U_ })) of
    (L ll)  -> case t2 of {  Bin x l r   ->  False; Leaf -> True }
    (R rr)  -> case t2 of {  Bin x l r   ->  case rr of 
                                               (r1 ::*:: r2) -> eq' r1 x &&  case r2 of 
                                                                               (r21 ::*:: r22) -> eq' r21 l && eq' r22 r;
                             Leaf        -> False }

=== {- case-case fusion -}

case t1 of
    Leaf          -> case t2 of {   Bin x l r   ->  False; Leaf -> True }
    Bin x0 l0 r0  -> case t2 of {   Bin x l r   ->  case (x0 ::*:: l0 ::*:: r0) of 
                                                      (r1 ::*:: r2) -> eq' r1 x &&  case r2 of 
                                                                                      (r21 ::*:: r22) -> eq' r21 l && eq' r22 r;
                                    Leaf        -> False }

=== {- constant case twice -}

case t1 of
    Leaf          -> case t2 of { Bin x l r -> False; Leaf -> True }
    Bin x0 l0 r0  -> case t2 of { Bin x l r -> eq' x0 x && eq' l0 l && eq' r0 r; Leaf -> False }

=== {- spec eq' to eq thrice -}

case t1 of
    Leaf          -> case t2 of { Bin x l r -> False; Leaf -> True }
    Bin x0 l0 r0  -> case t2 of { Bin x l r -> eq x0 x && eq l0 l && eq r0 r; Leaf -> False }

=== {- def eq thrice -}

case t1 of
    Leaf          ->  case t2 of { Bin x l r -> False; Leaf -> True }
    Bin x0 l0 r0  ->  case t2 of
                         Bin x l r  ->  eq' (fromInt x0) (fromInt x) && 
                                          eq' (fromTree l0) (fromTree l) && eq' (fromTree r0) (fromTree r)
                         Leaf       ->  False

=== {- def fromInt, spec eq' to eqInt -}

case t1 of
    Leaf          ->  case t2 of { Bin x l r -> False; Leaf -> True }
    Bin x0 l0 r0  ->  case t2 of 
                        Bin x l r   -> x0 == x && eq' (fromTree l0) (fromTree l) && eq' (fromTree r0) (fromTree r)
                        Leaf        -> False
\end{spec}


\end{document}