{-# LANGUAGE FlexibleInstances #-}

module Main where

import Prelude hiding (Eq, Show, show)
import Base
import GEq
{-
import Empty
import Show
import Update
import Decode
-}
import GEnum

--import GHC.Exts (inline)

--------------------------------------------------------------------------------
-- GENERIC FUNCTION USAGE
--------------------------------------------------------------------------------
-- Eq
-- Fine with -O
instance GEq (Tree Int) -- where geq x y = geq' (from x) (from y)
-- Only good with -O2 -fno-spec-constr-count
instance GEq Logic -- where geq x y = geq' (from x) (from y)


-- {-# SPECIALIZE geq :: Logic -> Logic -> Bool #-}
-- {-# SPECIALIZE geq :: Tree Int -> Tree Int -> Bool #-}

-- {-# NOINLINE eqLogic #-}

{-
eqLogic :: Logic -> Logic -> Bool
eqLogic x y = eq' (from x) (from y)

eqTree :: Tree Int -> Tree Int -> Bool
eqTree x y = eq' (from x) (from y)
-}
t1 = geq tree1 tree1
t2 = geq logic1 logic1

{-
--------------------------------------------------------------------------------
-- Show
instance Show (Tree Int) where show' = showTree
instance Show Logic where show' = showLogic

showLogic :: Logic -> String
showLogic = show' . from

showTree :: Tree Int -> String
showTree = show' . from

t3 = showTree tree1
t4 = showLogic logic1

--------------------------------------------------------------------------------
-- Empty
instance Empty (Tree Int) where empty' = emptyTree
instance Empty Logic where empty' = emptyLogic

emptyTree :: Tree Int
emptyTree = to empty'

emptyLogic :: Logic
emptyLogic = to empty'

t5 = emptyTree
t6 = emptyLogic

--------------------------------------------------------------------------------
-- Decode
instance Decode (Tree Int) where
  decode' bs = let (t :*: bs') = decode' bs in (to t :*: bs')

instance Decode Logic where
  decode' bs = let (l :*: bs') = decode' bs in (to l :*: bs')
{-
p2p :: (a,b) -> a :*: b
p2p (a,b) = a :*: b

p2p' :: a :*: b -> (a,b)
p2p' (a :*: b) = (a,b)
-}
fst' :: a :*: b -> a
fst' (a :*: b) = a

decodeTree :: Decoder (Tree Int)
decodeTree bs = let (t :*: bs') = decode' bs in (to t :*: bs')

decodeLogic :: Decoder Logic
decodeLogic bs = let (l :*: bs') = decode' bs in (to l :*: bs')

t7 = decodeTree  [1,5,0,0] -- Bin 53 Leaf Leaf
t8 = decodeLogic [1,1,1,1,0,0,5,1,0] -- Impl (VarL "x") T
-}
--------------------------------------------------------------------------------
-- Enum

-- It seems to be essential that we repeat the default here, instead of just
-- calling it. I wish I knew why.
instance GEnum (Tree Int) where genum = map to genum'
instance GEnum Logic      where genum = map to genum'
instance GEnum Nat        where genum = map to genum'
instance GEnum Nat2       where genum = map to genum'
instance GEnum [Int]      where genum = map to genum'

-- {-# SPECIALIZE genum :: [Logic] #-}

t9 :: [Tree Int]
t9  = take 10 genum

t10 :: [Logic]
t10 = take 10 genum

t11 :: [Nat]
t11 = take 10 genum

t12 :: [Nat2]
t12 = take 10 genum

t13 :: [[Int]]
t13 = take 10 genum

--------------------------------------------------------------------------------

-- Main function
main = print ( t1, t2 {- , t3, t4, showTree t5, showLogic t6
             , showTree (fst' t7), showLogic (fst' t8)
             -} , t9, t10)
