{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Base where

--------------------------------------------------------------------------------
-- BASE
--------------------------------------------------------------------------------

infixr 5 :+:
infixr 6 :*:

data U          = U              deriving (Show, Read)
data a :+: b    = L a | R b      deriving (Show, Read)
data a :*: b    = a :*: b        deriving (Show, Read)
data C c a   = C a            deriving (Show, Read)
data Var a   = Var a          deriving (Show, Read)
data Rec a   = Rec a          deriving (Show, Read)

class Constructor c where
  conName   :: t c a -> String
  {-# INLINE conFixity #-}
  conFixity :: t c a -> Fixity
  conFixity = const Prefix
  {-# INLINE conIsRecord #-}
  conIsRecord :: t c a -> Bool
  conIsRecord = const False

data Fixity = Prefix | Infix Associativity Int
  deriving (Eq, Show, Ord, Read)

data Associativity = LeftAssociative | RightAssociative | NotAssociative
  deriving (Eq, Show, Ord, Read)


class Representable a where
  type Rep a
  to   :: Rep a -> a
  from :: a -> Rep a

--------------------------------------------------------------------------------
-- INSTANCES
--------------------------------------------------------------------------------

instance Representable Int where 
  type Rep Int = Int
  to = id
  from = id
  
instance Representable Char where 
  type Rep Char = Char
  to = id
  from = id
{-
instance Representable U where 
  type Rep U = U
  to = id
  from = id

instance (Representable a, Representable b) => Representable (a :*: b) where 
  type Rep (a :*: b) = a :*: b
  to = id
  from = id

instance (Representable a, Representable b) => Representable (a :+: b) where 
  type Rep (a :+: b) = a :+: b
  to = id
  from = id
  
instance Representable a => Representable (C c a) where 
  type Rep (C c a) = C c a
  to = id
  from = id
  
instance Representable a => Representable (Var a) where 
  type Rep (Var a) = Var a
  to = id
  from = id

instance Representable a => Representable (Rec a) where 
  type Rep (Rec a) = Rec a
  to = id
  from = id
-}

-- Nat
data Nat = Ze | Su Nat

instance Representable Nat where
  type Rep Nat = C Nat_Ze_ U :+: C Nat_Su_ (Rec Nat)
  {-# INLINE [1] from #-}
  from Ze            = L (C U)
  from (Su n)        = R (C (Rec n))
  {-# INLINE [1] to #-}
  to (L (C U))       = Ze
  to (R (C (Rec n))) = Su n

data Nat_Ze_
data Nat_Su_
instance Constructor Nat_Ze_  where conName _ = "Ze"
instance Constructor Nat_Su_  where conName _   = "Su"

-- Nat2
data Nat2 = Ze2 | Su2 Nat2 Nat2

instance Representable Nat2 where
  type Rep Nat2 = U :+: Rec Nat2 :*: Rec Nat2

  {-# INLINE [1] to #-}
  to (L U)                   = Ze2
  to (R (Rec n1 :*: Rec n2)) = Su2 n1 n2

  
-- Lists
instance Representable [a] where
  type Rep [a] = C List_Nil_ U :+: C List_Cons_ (Var a :*: Rec [a])
  {-# INLINE [1] from #-}
  from []                           = L (C U)
  from (a:as)                       = R (C (Var a :*: Rec as))
  {-# INLINE [1] to #-}
  to (L (C U))                  = []
  to (R (C (Var a :*: Rec as))) = (a:as)

data List_Nil_
data List_Cons_
instance Constructor List_Nil_  where conName _ = "[]"
instance Constructor List_Cons_  where
  conName _   = ":"
  conFixity _ = Infix RightAssociative 5

--------------------------------------------------------------------------------
-- USER DATATYPES
--------------------------------------------------------------------------------

data Logic = VarL String
           | T                            -- true
           | F                            -- false
           | Not Logic                    -- not
           | Impl  Logic Logic            -- implication
           | Equiv Logic Logic            -- equivalence
           | Conj  Logic Logic            -- and (conjunction)
           | Disj  Logic Logic            -- or (disjunction)
  deriving (Show, Eq)

logic1 :: Logic
logic1 = Impl (Equiv (VarL "x") (Not T))
              (Not F)

data Tree a = Leaf | Bin a (Tree a) (Tree a) deriving (Show, Eq)

tree0, tree1 :: Tree Int
tree0 = Bin 3 Leaf Leaf
tree1 = Bin 5 tree0 tree0


-- ig Tree instances
data Bin
instance Constructor Bin  where conName _ = "Bin"
data Leaf
instance Constructor Leaf where conName _ = "Leaf"


instance Representable (Tree a) where
  type Rep (Tree a) =     C Leaf U
                      :+: C Bin  (Var a :*: Rec (Tree a) :*: Rec (Tree a))
  {-# INLINE [1] from #-}
  from (Bin x l r) = R (C (Var x :*: Rec l :*: Rec r))
  from Leaf        = L (C U)
  {-# INLINE [1] to #-}
  to (R (C (Var x :*: (Rec l) :*: (Rec r)))) = Bin x l r
  to (L (C U))                               = Leaf


-- ig Logic instances
data Logic_Var_
data Logic_Impl_
data Logic_Equiv_
data Logic_Conj_
data Logic_Disj_
data Logic_Not_
data Logic_T_
data Logic_F_

instance Constructor Logic_Var_ where
    { conName _ = "VarL" }
instance Constructor Logic_Impl_ where
    { conName _ = "Impl" }
instance Constructor Logic_Equiv_ where
    { conName _ = "Equiv" }
instance Constructor Logic_Conj_ where
    { conName _ = "Conj" }
instance Constructor Logic_Disj_ where
    { conName _ = "Disj" }
instance Constructor Logic_Not_ where
    { conName _ = "Not" }
instance Constructor Logic_T_ where
    { conName _ = "T" }
instance Constructor Logic_F_ where
    { conName _ = "F" }


instance Representable Logic where
  type Rep Logic =    (C Logic_Var_ (Var String))
                  :+: (C Logic_T_ U)
                  :+: (C Logic_F_ U)
                  :+: (C Logic_Not_ (Rec Logic))
                  :+: (C Logic_Impl_ (Rec Logic :*: Rec Logic))
                  :+: (C Logic_Equiv_ (Rec Logic :*: Rec Logic))
                  :+: (C Logic_Conj_ (Rec Logic :*: Rec Logic))
                  :+: (C Logic_Disj_ (Rec Logic :*: Rec Logic))
  {-# INLINE [1] from #-}
  from (VarL f0) = L (C (Var f0))
  from T = R (L (C U))
  from F = R (R (L (C U)))
  from (Not f0) = R (R (R (L (C (Rec f0)))))
  from (Impl f0 f1) = R (R (R (R (L (C ((:*:) (Rec f0) (Rec f1)))))))
  from (Equiv f0 f1) = R (R (R (R (R (L (C ((:*:) (Rec f0) (Rec f1))))))))
  from (Conj f0 f1) = R (R (R (R (R (R (L (C ((:*:) (Rec f0) (Rec f1)))))))))
  from (Disj f0 f1) = R (R (R (R (R (R (R (C ((:*:) (Rec f0) (Rec f1)))))))))
  {-# INLINE [1] to #-}
  to (L (C (Var f0))) = VarL f0
  to (R (L (C U))) = T
  to (R (R (L (C U)))) = F
  to (R (R (R (L (C (Rec f0)))))) = Not f0
  to (R (R (R (R (L (C ((:*:) (Rec f0) (Rec f1)))))))) = Impl f0 f1
  to (R (R (R (R (R (L (C ((:*:) (Rec f0) (Rec f1))))))))) = Equiv f0 f1
  to (R (R (R (R (R (R (L (C ((:*:) (Rec f0) (Rec f1)))))))))) = Conj f0 f1
  to (R (R (R (R (R (R (R (C ((:*:) (Rec f0) (Rec f1)))))))))) = Disj f0 f1
