{-# OPTIONS_GHC -fglasgow-exts #-}
{-# LANGUAGE OverlappingInstances #-}

module Show where

import Base
import Prelude hiding (Show, show)
import qualified Prelude as P (Show, show)
import Data.List (intersperse)

-- Generic show on Representable (worker)
class Show a where
  {-# INLINE show' #-}
  show' :: a -> String

instance Show U where
  {-# INLINE show' #-}
  show' U = ""
  
instance (Show a, Show b) => Show (a :+: b) where
  {-# INLINE show' #-}
  show' (L x) = show' x
  show' (R x) = show' x
  
instance (Show a, Show b) => Show (a :*: b) where
  {-# INLINE show' #-}
  show' (a :*: b) = show' a `space` show' b
  
instance (Show a, Constructor c) => Show (C c a) where
  {-# INLINE show' #-}
  show' c@(C a) | show' a == "" = paren $ conName c
                | otherwise     = paren $ (conName c) `space` show' a

instance Show a => Show (Var a) where
  {-# INLINE show' #-}
  show' (Var x) = show' x

instance Show a => Show (Rec a) where
  {-# INLINE show' #-}
  show' (Rec x) = show' x


-- Dispatcher
{-# INLINE show #-}
show :: (Representable a, Show (Rep a)) => a -> String
show = show' . from


-- Adhoc instances
instance Show Int      where show' = P.show
instance Show Integer  where show' = P.show
instance Show Float    where show' = P.show
instance Show Double   where show' = P.show
instance Show Char     where show' = P.show
instance Show Bool     where show' = P.show

instance Show a => Show [a] where
  show' = concat . wrap "[" "]" . intersperse "," . map show'

instance Show [Char] where
  show' = P.show


-- Generic instances


-- Utilities
{-# INLINE space #-}
space :: String -> String -> String
space a b = a ++ " " ++ b

{-# INLINE paren #-}
paren :: String -> String
paren x = "(" ++ x ++ ")"

{-# INLINE wrap #-}
wrap :: a -> a -> [a] -> [a]
wrap h t l = h:l++[t]
