{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DefaultSignatures #-}

module GEq where

import Base

--------------------------------------------------------------------------------
-- GENERIC FUNCTION DEFINITION
--------------------------------------------------------------------------------

-- Generic eq on Representable (worker)
class GEq' a where
  geq' :: a -> a -> Bool

instance GEq' U where
  {-# INLINE geq' #-}
  geq' _ _ = True

instance (GEq' a, GEq' b) => GEq' (a :+: b) where
  {-# INLINE geq' #-}
  geq' (L x) (L x') = geq' x x'
  geq' (R x) (R x') = geq' x x'
  geq' _     _      = False

instance (GEq' a, GEq' b) => GEq' (a :*: b) where
  {-# INLINE geq' #-}
  geq' (a :*: b) (a' :*: b') = geq' a a' && geq' b b'

instance (GEq' a) => GEq' (C c a) where
  {-# INLINE geq' #-}
  geq' (C a) (C a') = geq' a a'

instance GEq a => GEq' (Var a) where
  {-# INLINE geq' #-}
  geq' (Var x) (Var x') = geq x x'

instance (GEq a) => GEq' (Rec a) where
  {-# INLINE geq' #-}
  geq' (Rec x) (Rec x') = geq x x'


-- Dispatcher
class GEq a where
  geq :: a -> a -> Bool
  
  {-# INLINE geq #-}
  default geq :: (Representable a, GEq' (Rep a)) => a -> a -> Bool
  geq x y = geq' (from x) (from y)

-- Adhoc instances
instance GEq Int where
  {-# INLINE geq #-}
  geq x y = x == y

instance GEq Char where
  {-# INLINE geq #-}
  geq x y = x == y

-- Generic instances
instance (GEq a) => GEq [a]