{-# OPTIONS_GHC -fglasgow-exts -fenable-rewrite-rules #-}

module Test where

import SYB

-- Tree datatype
data Tree a = Leaf | Bin a (Tree a) (Tree a) deriving (Show, Eq, Typeable)

instance Data (Tree Int) where
  gmapT = gmapTTree

{-# INLINE gmapTTree #-}
gmapTTree :: (forall b. Data b => b -> b) -> Tree Int -> Tree Int
gmapTTree _ Leaf        = Leaf
gmapTTree f (Bin x l r) = Bin (f x) (f l) (f r)


-- | The type constructor for transformations
newtype T x = T { unT :: x -> x }

-- | Apply a transformation everywhere in bottom-up manner
{-# INLINE everywhere #-}
everywhere :: (forall a. Data a => a -> a) -> (forall a. Data a => a -> a)
everywhere f = f . gmapT (everywhere f)

-- | Make a generic transformation;
--   start from a type-specific case; preserve the term otherwise
{-# INLINE mkT #-}
mkT :: (Typeable a , Typeable b) => (b -> b) -> a -> a
mkT = extT id

-- | Extend a generic transformation by a type-specific case
{-# INLINE extT #-}
extT :: (Typeable a, Typeable b) => (a -> a) -> (b -> b) -> a  -> a
extT def ext = unT ((T def) `ext0` (T ext))

-- | Flexible type extension
{-# INLINE ext0 #-}
ext0 :: (Typeable a, Typeable b) => c a -> c b -> c a
ext0 def ext = maybe def id (gcast ext)


{-# INLINE updateInt #-}
{-# SPECIALIZE updateInt :: Tree Int -> Tree Int #-}
updateInt :: (Data a) => a -> a
updateInt = everywhere (mkT (\n -> if odd n then n+1 else (n-1 :: Int)))


-- Test
tree1, tree2 :: Tree Int
tree1 = Leaf
tree2 = Bin 2 Leaf (Bin 3 Leaf Leaf)

test1 = updateInt tree1
test2 = updateInt tree2
