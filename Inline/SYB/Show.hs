{-# OPTIONS_GHC -fglasgow-exts -fenable-rewrite-rules #-}

module Test where

import SYB

-- Tree datatype
data Tree a = Leaf | Bin a (Tree a) (Tree a) deriving (Show, Eq, Typeable)

instance Data (Tree Int) where
  gfoldl k z Leaf = error "undefined as it is unnecessary" --z Leaf
  gfoldl k z (Bin x l r) = error "undefined as it is unnecessary" --z Bin `k` x `k` l `k` r
  
  toConstr Leaf        = leafConstr
  toConstr (Bin _ _ _) = binConstr
  
  dataTypeOf _ = error "undefined as it is unnecessary" --treeDataType
  
  dataCast1 f = error "undefined as it is unnecessary" --gcast1 f
  
  gunfold = error "undefined as it is unnecessary"
  
  gmapQ = gmapQTree

{-# INLINE gmapQTree #-}
{-# RULES 
"gmapQTree/Leaf" forall (f :: (forall d. Data d => d -> u)). gmapQTree f Leaf = [] 
-- "gmapQTree/Bin" forall (f :: (forall d. Data d => d -> u)) x l r. gmapQTree f (Bin x l r) = [f x, f l, f r] 
  #-}
{-
"gmapQTree/gen" forall (f :: (forall d. Data d => d -> u)) x. 
  gmapQTree f x = case x of
                    Leaf -> []
                    Bin x l r -> [f x, f l, f r]
-}
gmapQTree :: (forall d. Data d => d -> u) -> Tree Int -> [u]
gmapQTree f Leaf = []
gmapQTree f (Bin x l r) = [f x, f l, f r]

{-# INLINE leafConstr #-}
{-# INLINE binConstr  #-}
leafConstr, binConstr :: Constr
leafConstr = mkConstr treeDataType "Leaf" [] Prefix
binConstr  = mkConstr treeDataType "Bin"  [] Prefix

{-# INLINE treeDataType #-}
treeDataType :: DataType
treeDataType = mkDataType "Test.Tree" [leafConstr,binConstr]


{-# INLINE gshow #-}
{-# SPECIALIZE gshow :: Tree Int -> String #-}
gshow :: (Data a) => a -> String
gshow = ( \t ->
                "("
             ++ showConstr (toConstr t)
             ++ concat (gmapQ ((++) " " . gshow) t)
             ++ ")"
        ) -- `extQ` (show :: String -> String) -- removed to avoid casts


-- | Extend a generic query by a type-specific case
{-# INLINE extQ #-}
extQ :: ( Typeable a
        , Typeable b
        )
     => (a -> q)
     -> (b -> q)
     -> a
     -> q
extQ f g a = maybe (f a) g (cast a)

-- Test
tree1, tree2 :: Tree Int
tree1 = Leaf
tree2 = Bin 2 Leaf (Bin 3 Leaf Leaf)

test1 = gshow tree1
test2 = gshow tree2
