
==================== Tidy Core ====================
Test.updateStringf
  :: forall (f_aiN :: * -> *).
     (Test.UpdateString f_aiN) =>
     forall a_aiO. (a_aiO -> a_aiO) -> f_aiN a_aiO -> f_aiN a_aiO
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.updateStringf =
  \ (@ f_aiN::* -> *)
    (tpl_B1 [Occ=Once] :: Test.UpdateString f_aiN) ->
    tpl_B1
    `cast` (Test.NTCo:T:UpdateString f_aiN
            :: Test.T:UpdateString f_aiN
                 ~
               (forall a_aiO. (a_aiO -> a_aiO) -> f_aiN a_aiO -> f_aiN a_aiO))

Test.from [InlPrag=NOINLINE]
  :: forall a_aiP.
     (Test.Regular a_aiP) =>
     a_aiP -> Test.PF a_aiP a_aiP
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.from: "Class op from"]
Test.from =
  \ (@ a_aiP) (tpl_B1 [Occ=Once!] :: Test.Regular a_aiP) ->
    case tpl_B1 of _ { Test.D:Regular tpl_B2 [Occ=Once] _ -> tpl_B2 }

Test.to [InlPrag=NOINLINE]
  :: forall a_aiP.
     (Test.Regular a_aiP) =>
     Test.PF a_aiP a_aiP -> a_aiP
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.to: "Class op to"]
Test.to =
  \ (@ a_aiP) (tpl_B1 [Occ=Once!] :: Test.Regular a_aiP) ->
    case tpl_B1 of _ { Test.D:Regular _ tpl_B3 [Occ=Once] -> tpl_B3 }

Test.conName [InlPrag=NOINLINE]
  :: forall c_aiR.
     (Test.Constructor c_aiR) =>
     forall (t_aiS :: * -> (* -> *) -> * -> *) (f_aiT :: * -> *) r_aiU.
     t_aiS c_aiR f_aiT r_aiU -> GHC.Base.String
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SAA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conName: "Class op conName"]
Test.conName =
  \ (@ c_aiR) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiR) ->
    case tpl_B1 of _ { Test.D:Constructor tpl_B2 [Occ=Once] _ _ ->
    tpl_B2
    }

Test.conFixity [InlPrag=NOINLINE]
  :: forall c_aiR.
     (Test.Constructor c_aiR) =>
     forall (t_aiV :: * -> (* -> *) -> * -> *) (f_aiW :: * -> *) r_aiX.
     t_aiV c_aiR f_aiW r_aiX -> Test.Fixity
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(ASA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conFixity: "Class op conFixity"]
Test.conFixity =
  \ (@ c_aiR) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiR) ->
    case tpl_B1 of _ { Test.D:Constructor _ tpl_B3 [Occ=Once] _ ->
    tpl_B3
    }

Test.conIsRecord [InlPrag=NOINLINE]
  :: forall c_aiR.
     (Test.Constructor c_aiR) =>
     forall (t_aiY :: * -> (* -> *) -> * -> *) (f_aiZ :: * -> *) r_aj0.
     t_aiY c_aiR f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AAS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conIsRecord: "Class op conIsRecord"]
Test.conIsRecord =
  \ (@ c_aiR) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiR) ->
    case tpl_B1 of _ { Test.D:Constructor _ _ tpl_B4 [Occ=Once] ->
    tpl_B4
    }

lvl_r1hl :: [GHC.Types.Char]
[GblId, Str=DmdType]
lvl_r1hl = GHC.Base.unpackCString# "Infix "

lvl1_r1hn :: [GHC.Types.Char]
[GblId, Str=DmdType]
lvl1_r1hn = GHC.Base.unpackCString# "Prefix"

Test.$fReadAssociativity9 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

Test.$fReadAssociativity12 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

Test.$fReadAssociativity15 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

lvl2_r1hp :: [GHC.Types.Char]
[GblId, Str=DmdType]
lvl2_r1hp = GHC.Base.unpackCString# "Infix"

Test.$fConstructorLogic_Var_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Var_1 = GHC.Base.unpackCString# "Var"

Test.$fConstructorLogic_Impl_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Impl_1 = GHC.Base.unpackCString# "Impl"

Test.$fConstructorLogic_Equiv_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorLogic_Equiv_1 = GHC.Base.unpackCString# "Equiv"

Test.$fConstructorLogic_Conj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Conj_1 = GHC.Base.unpackCString# "Conj"

Test.$fConstructorLogic_Disj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Disj_1 = GHC.Base.unpackCString# "Disj"

Test.$fConstructorLogic_Not_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Not_1 = GHC.Base.unpackCString# "Not"

Test.$fConstructorLogic_T_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_T_2 = GHC.Types.C# 'T'

Test.$fConstructorLogic_T_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_T_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_T_2
    (GHC.Types.[] @ GHC.Types.Char)

Test.$fConstructorLogic_F_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_F_2 = GHC.Types.C# 'F'

Test.$fConstructorLogic_F_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_F_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_F_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl3_r1hr :: forall c_awj. Test.C c_awj Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl3_r1hr =
  \ (@ c_awj) ->
    Test.C @ c_awj @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl4_r1ht
  :: forall c_awj c1_awx c2_awQ c3_axc c4_axB c5_ay1.
     (Test.:+:)
       (Test.C c_awj Test.U)
       (Test.C c1_awx Test.I
        Test.:+: (Test.C c2_awQ (Test.I Test.:*: Test.I)
                  Test.:+: (Test.C c3_axc (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axB (Test.I Test.:*: Test.I)
                                      Test.:+: Test.C c5_ay1 (Test.I Test.:*: Test.I)))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl4_r1ht =
  \ (@ c_awj)
    (@ c1_awx)
    (@ c2_awQ)
    (@ c3_axc)
    (@ c4_axB)
    (@ c5_ay1) ->
    Test.L
      @ (Test.C c_awj Test.U)
      @ (Test.C c1_awx Test.I
         Test.:+: (Test.C c2_awQ (Test.I Test.:*: Test.I)
                   Test.:+: (Test.C c3_axc (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axB (Test.I Test.:*: Test.I)
                                       Test.:+: Test.C c5_ay1 (Test.I Test.:*: Test.I)))))
      @ Test.Logic
      (lvl3_r1hr @ c_awj)

lvl5_r1hv
  :: forall c_aw7 c1_awj c2_awx c3_awQ c4_axc c5_axB c6_ay1.
     (Test.:+:)
       (Test.C c_aw7 Test.U)
       (Test.C c1_awj Test.U
        Test.:+: (Test.C c2_awx Test.I
                  Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay1 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl5_r1hv =
  \ (@ c_aw7)
    (@ c1_awj)
    (@ c2_awx)
    (@ c3_awQ)
    (@ c4_axc)
    (@ c5_axB)
    (@ c6_ay1) ->
    Test.R
      @ (Test.C c_aw7 Test.U)
      @ (Test.C c1_awj Test.U
         Test.:+: (Test.C c2_awx Test.I
                   Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay1 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl4_r1ht @ c1_awj @ c2_awx @ c3_awQ @ c4_axc @ c5_axB @ c6_ay1)

lvl6_r1hx
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     (Test.:+:)
       (Test.C c_avX (Test.K GHC.Base.String))
       (Test.C c1_aw7 Test.U
        Test.:+: (Test.C c2_awj Test.U
                  Test.:+: (Test.C c3_awx Test.I
                            Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay1
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl6_r1hx =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1) ->
    Test.R
      @ (Test.C c_avX (Test.K GHC.Base.String))
      @ (Test.C c1_aw7 Test.U
         Test.:+: (Test.C c2_awj Test.U
                   Test.:+: (Test.C c3_awx Test.I
                             Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay1
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl5_r1hv
         @ c1_aw7 @ c2_awj @ c3_awx @ c4_awQ @ c5_axc @ c6_axB @ c7_ay1)

lvl7_r1hz :: forall c_aw7. Test.C c_aw7 Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl7_r1hz =
  \ (@ c_aw7) ->
    Test.C @ c_aw7 @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl8_r1hB
  :: forall c_aw7 c1_awj c2_awx c3_awQ c4_axc c5_axB c6_ay1.
     (Test.:+:)
       (Test.C c_aw7 Test.U)
       (Test.C c1_awj Test.U
        Test.:+: (Test.C c2_awx Test.I
                  Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay1 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl8_r1hB =
  \ (@ c_aw7)
    (@ c1_awj)
    (@ c2_awx)
    (@ c3_awQ)
    (@ c4_axc)
    (@ c5_axB)
    (@ c6_ay1) ->
    Test.L
      @ (Test.C c_aw7 Test.U)
      @ (Test.C c1_awj Test.U
         Test.:+: (Test.C c2_awx Test.I
                   Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay1 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl7_r1hz @ c_aw7)

lvl9_r1hD
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     (Test.:+:)
       (Test.C c_avX (Test.K GHC.Base.String))
       (Test.C c1_aw7 Test.U
        Test.:+: (Test.C c2_awj Test.U
                  Test.:+: (Test.C c3_awx Test.I
                            Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay1
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl9_r1hD =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1) ->
    Test.R
      @ (Test.C c_avX (Test.K GHC.Base.String))
      @ (Test.C c1_aw7 Test.U
         Test.:+: (Test.C c2_awj Test.U
                   Test.:+: (Test.C c3_awx Test.I
                             Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay1
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl8_r1hB
         @ c1_aw7 @ c2_awj @ c3_awx @ c4_awQ @ c5_axc @ c6_axB @ c7_ay1)

Test.$dmconFixity_$s$dmconFixity7
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_F_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity7 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity6
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_T_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity6 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity5
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Not_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity5 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity4
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Disj_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity4 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity3
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Conj_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity3 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity2
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Equiv_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity2 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity1
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Impl_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity1 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Var_ f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconIsRecord_$s$dmconIsRecord7
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_F_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord7 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord6
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_T_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord6 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord5
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_Not_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord5 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord4
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_Disj_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord4 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord3
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_Conj_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord3 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord2
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_Equiv_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord2 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord1
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_Impl_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord1 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord
  :: forall (t_aiY :: * -> (* -> *) -> * -> *)
            (f_aiZ :: * -> *)
            r_aj0.
     t_aiY Test.Logic_Var_ f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$fShowFixity1 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fShowFixity1 = GHC.Types.I# 0

Test.$fReadAssociativity14
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWj.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj)
        -> Text.ParserCombinators.ReadP.P b_aWj
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity14 =
  \ _
    (@ b_aWj)
    (eta_aWk
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj) ->
    eta_aWk Test.LeftAssociative

Test.$fReadAssociativity13
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity13 =
  (Test.$fReadAssociativity15,
   Test.$fReadAssociativity14
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWB.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                  -> Text.ParserCombinators.ReadP.P b_aWB)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity11
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWj.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj)
        -> Text.ParserCombinators.ReadP.P b_aWj
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity11 =
  \ _
    (@ b_aWj)
    (eta_aWk
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj) ->
    eta_aWk Test.RightAssociative

Test.$fReadAssociativity10
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity10 =
  (Test.$fReadAssociativity12,
   Test.$fReadAssociativity11
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWB.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                  -> Text.ParserCombinators.ReadP.P b_aWB)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity8
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWj.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj)
        -> Text.ParserCombinators.ReadP.P b_aWj
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity8 =
  \ _
    (@ b_aWj)
    (eta_aWk
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj) ->
    eta_aWk Test.NotAssociative

Test.$fReadAssociativity7
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity7 =
  (Test.$fReadAssociativity9,
   Test.$fReadAssociativity8
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWB.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                  -> Text.ParserCombinators.ReadP.P b_aWB)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity6
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity5
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity10
    Test.$fReadAssociativity6

Test.$fReadAssociativity4
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity13
    Test.$fReadAssociativity5

Test.$fReadAssociativity3
  :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity3 =
  GHC.Read.choose1 @ Test.Associativity Test.$fReadAssociativity4

Test.$fReadAssociativity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Associativity Test.$fReadAssociativity3

Test.$fReadAssociativity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ Test.Associativity
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity16
  :: Text.ParserCombinators.ReadP.P [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadAssociativity16 =
  ((Test.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [Test.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
                ~
              (forall b_aWB.
               ([Test.Associativity] -> Text.ParserCombinators.ReadP.P b_aWB)
               -> Text.ParserCombinators.ReadP.P b_aWB)))
    @ [Test.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn
       @ [Test.Associativity])

lvl10_r1hF
  :: forall b_X10y.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10y
[GblId, Arity=1, Caf=NoCafRefs]
lvl10_r1hF =
  \ (@ b_X10y) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10y
    }

lvl11_r1hH :: forall b_X10y. Text.ParserCombinators.ReadP.P b_X10y
[GblId, Caf=NoCafRefs]
lvl11_r1hH =
  \ (@ b_X10y) ->
    Text.ParserCombinators.ReadP.Look @ b_X10y (lvl10_r1hF @ b_X10y)

lvl12_r1hJ
  :: forall b_X10y.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10y
[GblId, Arity=1, Caf=NoCafRefs]
lvl12_r1hJ =
  \ (@ b_X10y) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10y
    }

lvl13_r1hL :: forall b_X10y. Text.ParserCombinators.ReadP.P b_X10y
[GblId, Caf=NoCafRefs]
lvl13_r1hL =
  \ (@ b_X10y) ->
    Text.ParserCombinators.ReadP.Look @ b_X10y (lvl12_r1hJ @ b_X10y)

lvl14_r1hN :: GHC.Types.Int
[GblId, Caf=NoCafRefs]
lvl14_r1hN = GHC.Types.I# 11

lvl15_r1hP
  :: forall b_a15I.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15I
[GblId, Arity=1, Caf=NoCafRefs]
lvl15_r1hP =
  \ (@ b_a15I) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15I
    }

lvl16_r1hR :: forall b_a15I. Text.ParserCombinators.ReadP.P b_a15I
[GblId, Caf=NoCafRefs]
lvl16_r1hR =
  \ (@ b_a15I) ->
    Text.ParserCombinators.ReadP.Look @ b_a15I (lvl15_r1hP @ b_a15I)

lvl17_r1hT
  :: forall b_a15I.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15I
[GblId, Arity=1, Caf=NoCafRefs]
lvl17_r1hT =
  \ (@ b_a15I) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15I
    }

lvl18_r1hV :: forall b_a15I. Text.ParserCombinators.ReadP.P b_a15I
[GblId, Caf=NoCafRefs]
lvl18_r1hV =
  \ (@ b_a15I) ->
    Text.ParserCombinators.ReadP.Look @ b_a15I (lvl17_r1hT @ b_a15I)

lvl19_r1hX
  :: forall b_X10y.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10y
[GblId, Arity=1, Caf=NoCafRefs]
lvl19_r1hX =
  \ (@ b_X10y) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10y
    }

lvl20_r1hZ :: forall b_X10y. Text.ParserCombinators.ReadP.P b_X10y
[GblId, Caf=NoCafRefs]
lvl20_r1hZ =
  \ (@ b_X10y) ->
    Text.ParserCombinators.ReadP.Look @ b_X10y (lvl19_r1hX @ b_X10y)

lvl21_r1i1
  :: forall b_X10y.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10y
[GblId, Arity=1, Caf=NoCafRefs]
lvl21_r1i1 =
  \ (@ b_X10y) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10y
    }

lvl22_r1i3 :: forall b_X10y. Text.ParserCombinators.ReadP.P b_X10y
[GblId, Caf=NoCafRefs]
lvl22_r1i3 =
  \ (@ b_X10y) ->
    Text.ParserCombinators.ReadP.Look @ b_X10y (lvl21_r1i1 @ b_X10y)

Rec {
a1_r1i5
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId, Arity=1, Str=DmdType L]
a1_r1i5 =
  \ (n_X1aB :: Text.ParserCombinators.ReadPrec.Prec) ->
    let {
      ds2_s1fu [Dmd=Just L]
        :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
      [LclId, Str=DmdType]
      ds2_s1fu =
        (Test.$fReadAssociativity3
         `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                   Test.Associativity
                 :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
                      ~
                    (Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)))
          n_X1aB } in
    (\ (@ b_a15I)
       (k1_a15J
          :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a15I) ->
       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
         @ b_a15I
         ((ds2_s1fu
           `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                   :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                        ~
                      (forall b_aWB.
                       (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                       -> Text.ParserCombinators.ReadP.P b_aWB)))
            @ b_a15I k1_a15J)
         (Text.ParserCombinators.ReadP.Look
            @ b_a15I
            (let {
               $wk_s1fI [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15I
               [LclId, Str=DmdType]
               $wk_s1fI =
                 Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                   @ b_a15I
                   (lvl16_r1hR @ b_a15I)
                   (Text.Read.Lex.hsLex2
                      @ b_a15I
                      (let {
                         lvl81_s1fE :: Text.ParserCombinators.ReadP.P b_a15I
                         [LclId, Str=DmdType]
                         lvl81_s1fE =
                           ((a1_r1i5 Text.ParserCombinators.ReadPrec.minPrec)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                                    :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                         ~
                                       (forall b_aWB.
                                        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                                        -> Text.ParserCombinators.ReadP.P b_aWB)))
                             @ b_a15I
                             (\ (a12_a163 :: Test.Associativity) ->
                                Text.ParserCombinators.ReadP.Look
                                  @ b_a15I
                                  (let {
                                     $wk1_s1fA [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15I
                                     [LclId, Str=DmdType]
                                     $wk1_s1fA =
                                       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                                         @ b_a15I
                                         (lvl18_r1hV @ b_a15I)
                                         (Text.Read.Lex.hsLex2
                                            @ b_a15I
                                            (let {
                                               lvl91_s1fw :: Text.ParserCombinators.ReadP.P b_a15I
                                               [LclId, Str=DmdType]
                                               lvl91_s1fw = k1_a15J a12_a163 } in
                                             \ (a111_a165 :: Text.Read.Lex.Lexeme) ->
                                               case a111_a165 of _ {
                                                 __DEFAULT ->
                                                   Text.ParserCombinators.ReadP.Fail @ b_a15I;
                                                 Text.Read.Lex.Punc ds_a168 ->
                                                   case ds_a168 of _ {
                                                     [] ->
                                                       Text.ParserCombinators.ReadP.Fail @ b_a15I;
                                                     : ds1_a16d ds21_a16e ->
                                                       case ds1_a16d of _ { GHC.Types.C# ds3_a16i ->
                                                       case ds3_a16i of _ {
                                                         __DEFAULT ->
                                                           Text.ParserCombinators.ReadP.Fail
                                                             @ b_a15I;
                                                         ')' ->
                                                           case ds21_a16e of _ {
                                                             [] -> lvl91_s1fw;
                                                             : _ _ ->
                                                               Text.ParserCombinators.ReadP.Fail
                                                                 @ b_a15I
                                                           }
                                                       }
                                                       }
                                                   }
                                               })) } in
                                   let {
                                     k_s1fC :: () -> Text.ParserCombinators.ReadP.P b_a15I
                                     [LclId, Arity=1, Str=DmdType A]
                                     k_s1fC = \ _ -> $wk1_s1fA } in
                                   \ (a111_a17e :: GHC.Base.String) ->
                                     ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                                              :: Text.ParserCombinators.ReadP.ReadP ()
                                                   ~
                                                 (forall b_aWB.
                                                  (() -> Text.ParserCombinators.ReadP.P b_aWB)
                                                  -> Text.ParserCombinators.ReadP.P b_aWB)))
                                       @ b_a15I k_s1fC)) } in
                       \ (a12_a16q :: Text.Read.Lex.Lexeme) ->
                         case a12_a16q of _ {
                           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15I;
                           Text.Read.Lex.Punc ds_a16t ->
                             case ds_a16t of _ {
                               [] -> Text.ParserCombinators.ReadP.Fail @ b_a15I;
                               : ds1_a16y ds21_a16z ->
                                 case ds1_a16y of _ { GHC.Types.C# ds3_a16D ->
                                 case ds3_a16D of _ {
                                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15I;
                                   '(' ->
                                     case ds21_a16z of _ {
                                       [] -> lvl81_s1fE;
                                       : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a15I
                                     }
                                 }
                                 }
                             }
                         })) } in
             let {
               k_s1fK :: () -> Text.ParserCombinators.ReadP.P b_a15I
               [LclId, Arity=1, Str=DmdType A]
               k_s1fK = \ _ -> $wk_s1fI } in
             \ (a111_a17e :: GHC.Base.String) ->
               ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                        :: Text.ParserCombinators.ReadP.ReadP ()
                             ~
                           (forall b_aWB.
                            (() -> Text.ParserCombinators.ReadP.P b_aWB)
                            -> Text.ParserCombinators.ReadP.P b_aWB)))
                 @ b_a15I k_s1fK)))
    `cast` (sym
              (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity)
            :: (forall b_aWB.
                (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                -> Text.ParserCombinators.ReadP.P b_aWB)
                 ~
               Text.ParserCombinators.ReadP.ReadP Test.Associativity)
end Rec }

Rec {
Test.$fReadFixity3
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_X10y.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_X10y)
        -> Text.ParserCombinators.ReadP.P b_X10y
[GblId, Arity=2, Str=DmdType LL]
Test.$fReadFixity3 =
  \ (n_a15G :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_X10y)
    (eta_X3Z
       :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_X10y) ->
    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
      @ b_X10y
      (Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
         @ b_X10y
         (Text.ParserCombinators.ReadP.Look
            @ b_X10y
            (let {
               $wk_s1fm [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_X10y
               [LclId, Str=DmdType]
               $wk_s1fm =
                 Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                   @ b_X10y
                   (lvl22_r1i3 @ b_X10y)
                   (Text.Read.Lex.hsLex2
                      @ b_X10y
                      (let {
                         lvl27_s1h1 :: Text.ParserCombinators.ReadP.P b_X10y
                         [LclId]
                         lvl27_s1h1 = eta_X3Z Test.Prefix } in
                       \ (a12_aWP :: Text.Read.Lex.Lexeme) ->
                         case a12_aWP of _ {
                           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                           Text.Read.Lex.Ident ds_dT5 ->
                             case GHC.Base.eqString ds_dT5 lvl1_r1hn of _ {
                               GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                               GHC.Bool.True -> lvl27_s1h1
                             }
                         })) } in
             let {
               k_s1fo :: () -> Text.ParserCombinators.ReadP.P b_X10y
               [LclId, Arity=1, Str=DmdType A]
               k_s1fo = \ _ -> $wk_s1fm } in
             \ (a111_a17e :: GHC.Base.String) ->
               ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                        :: Text.ParserCombinators.ReadP.ReadP ()
                             ~
                           (forall b_aWB.
                            (() -> Text.ParserCombinators.ReadP.P b_aWB)
                            -> Text.ParserCombinators.ReadP.P b_aWB)))
                 @ b_X10y k_s1fo))
         (case n_a15G of _ { GHC.Types.I# x_a17G ->
          case GHC.Prim.<=# x_a17G 10 of _ {
            GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
            GHC.Bool.True ->
              Text.ParserCombinators.ReadP.Look
                @ b_X10y
                (let {
                   $wk_s1fP :: Text.ParserCombinators.ReadP.P b_X10y
                   [LclId, Str=DmdType]
                   $wk_s1fP =
                     Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                       @ b_X10y
                       (lvl20_r1hZ @ b_X10y)
                       (Text.Read.Lex.hsLex2
                          @ b_X10y
                          (let {
                             lvl27_s1h4 :: Text.ParserCombinators.ReadP.P b_X10y
                             [LclId]
                             lvl27_s1h4 =
                               ((a1_r1i5 lvl14_r1hN)
                                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                                        :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                             ~
                                           (forall b_aWB.
                                            (Test.Associativity
                                             -> Text.ParserCombinators.ReadP.P b_aWB)
                                            -> Text.ParserCombinators.ReadP.P b_aWB)))
                                 @ b_X10y
                                 (\ (a12_X154 :: Test.Associativity) ->
                                    ((GHC.Read.$dmreadsPrec11 lvl14_r1hN)
                                     `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                                             :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                                  ~
                                                (forall b_aWB.
                                                 (GHC.Types.Int
                                                  -> Text.ParserCombinators.ReadP.P b_aWB)
                                                 -> Text.ParserCombinators.ReadP.P b_aWB)))
                                      @ b_X10y
                                      (\ (a13_X15a :: GHC.Types.Int) ->
                                         eta_X3Z (Test.Infix a12_X154 a13_X15a))) } in
                           \ (a12_aWP :: Text.Read.Lex.Lexeme) ->
                             case a12_aWP of _ {
                               __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                               Text.Read.Lex.Ident ds_dTa ->
                                 case GHC.Base.eqString ds_dTa lvl2_r1hp of _ {
                                   GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                                   GHC.Bool.True -> lvl27_s1h4
                                 }
                             })) } in
                 let {
                   k_s1fR :: () -> Text.ParserCombinators.ReadP.P b_X10y
                   [LclId, Arity=1, Str=DmdType A]
                   k_s1fR = \ _ -> $wk_s1fP } in
                 \ (a111_a17e :: GHC.Base.String) ->
                   ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                    `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                            :: Text.ParserCombinators.ReadP.ReadP ()
                                 ~
                               (forall b_aWB.
                                (() -> Text.ParserCombinators.ReadP.P b_aWB)
                                -> Text.ParserCombinators.ReadP.P b_aWB)))
                     @ b_X10y k_s1fR)
          }
          }))
      (Text.ParserCombinators.ReadP.Look
         @ b_X10y
         (let {
            $wk_s1g5 [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_X10y
            [LclId, Str=DmdType]
            $wk_s1g5 =
              Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                @ b_X10y
                (lvl11_r1hH @ b_X10y)
                (Text.Read.Lex.hsLex2
                   @ b_X10y
                   (let {
                      lvl81_s1g1 :: Text.ParserCombinators.ReadP.P b_X10y
                      [LclId, Str=DmdType]
                      lvl81_s1g1 =
                        Test.$fReadFixity3
                          Text.ParserCombinators.ReadPrec.minPrec
                          @ b_X10y
                          (\ (a12_a163 :: Test.Fixity) ->
                             Text.ParserCombinators.ReadP.Look
                               @ b_X10y
                               (let {
                                  $wk1_s1fX [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_X10y
                                  [LclId, Str=DmdType]
                                  $wk1_s1fX =
                                    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                                      @ b_X10y
                                      (lvl13_r1hL @ b_X10y)
                                      (Text.Read.Lex.hsLex2
                                         @ b_X10y
                                         (let {
                                            lvl91_s1fT :: Text.ParserCombinators.ReadP.P b_X10y
                                            [LclId, Str=DmdType]
                                            lvl91_s1fT = eta_X3Z a12_a163 } in
                                          \ (a111_a165 :: Text.Read.Lex.Lexeme) ->
                                            case a111_a165 of _ {
                                              __DEFAULT ->
                                                Text.ParserCombinators.ReadP.Fail @ b_X10y;
                                              Text.Read.Lex.Punc ds_a168 ->
                                                case ds_a168 of _ {
                                                  [] -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                                                  : ds1_a16d ds2_a16e ->
                                                    case ds1_a16d of _ { GHC.Types.C# ds3_a16i ->
                                                    case ds3_a16i of _ {
                                                      __DEFAULT ->
                                                        Text.ParserCombinators.ReadP.Fail @ b_X10y;
                                                      ')' ->
                                                        case ds2_a16e of _ {
                                                          [] -> lvl91_s1fT;
                                                          : _ _ ->
                                                            Text.ParserCombinators.ReadP.Fail
                                                              @ b_X10y
                                                        }
                                                    }
                                                    }
                                                }
                                            })) } in
                                let {
                                  k_s1fZ :: () -> Text.ParserCombinators.ReadP.P b_X10y
                                  [LclId, Arity=1, Str=DmdType A]
                                  k_s1fZ = \ _ -> $wk1_s1fX } in
                                \ (a111_a17e :: GHC.Base.String) ->
                                  ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                                   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                                           :: Text.ParserCombinators.ReadP.ReadP ()
                                                ~
                                              (forall b_aWB.
                                               (() -> Text.ParserCombinators.ReadP.P b_aWB)
                                               -> Text.ParserCombinators.ReadP.P b_aWB)))
                                    @ b_X10y k_s1fZ)) } in
                    \ (a12_a16q :: Text.Read.Lex.Lexeme) ->
                      case a12_a16q of _ {
                        __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                        Text.Read.Lex.Punc ds_a16t ->
                          case ds_a16t of _ {
                            [] -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                            : ds1_a16y ds2_a16z ->
                              case ds1_a16y of _ { GHC.Types.C# ds3_a16D ->
                              case ds3_a16D of _ {
                                __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10y;
                                '(' ->
                                  case ds2_a16z of _ {
                                    [] -> lvl81_s1g1;
                                    : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_X10y
                                  }
                              }
                              }
                          }
                      })) } in
          let {
            k_s1g7 :: () -> Text.ParserCombinators.ReadP.P b_X10y
            [LclId, Arity=1, Str=DmdType A]
            k_s1g7 = \ _ -> $wk_s1g5 } in
          \ (a111_a17e :: GHC.Base.String) ->
            ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
             `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                     :: Text.ParserCombinators.ReadP.ReadP ()
                          ~
                        (forall b_aWB.
                         (() -> Text.ParserCombinators.ReadP.P b_aWB)
                         -> Text.ParserCombinators.ReadP.P b_aWB)))
              @ b_X10y k_s1g7))
end Rec }

Test.$fReadFixity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWB.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
        -> Text.ParserCombinators.ReadP.P b_aWB
[GblId,
 Arity=2,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 3 0}]
Test.$fReadFixity2 =
  \ (eta_a15E :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_aWB)
    (eta1_B1 :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB) ->
    Test.$fReadFixity3 eta_a15E @ b_aWB eta1_B1

Test.$fReadFixity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ Test.Fixity
    (Test.$fReadFixity2
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_aWB.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
                    -> Text.ParserCombinators.ReadP.P b_aWB)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity4 :: Text.ParserCombinators.ReadP.P [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadFixity4 =
  ((Test.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [Test.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
                ~
              (forall b_aWB.
               ([Test.Fixity] -> Text.ParserCombinators.ReadP.P b_aWB)
               -> Text.ParserCombinators.ReadP.P b_aWB)))
    @ [Test.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ [Test.Fixity])

Test.logic3 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic3 = GHC.Types.C# 'x'

Test.logic2 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic2 =
  GHC.Types.:
    @ GHC.Types.Char Test.logic3 (GHC.Types.[] @ GHC.Types.Char)

Test.logic1 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic1 = Test.Var Test.logic2

Test.logic4 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic4 = Test.Not Test.T

a_r1i7
  :: forall t_aAv t1_aAx t2_aAy r_aAB.
     t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy t1_aAx
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType AS]
a_r1i7 =
  \ (@ t_aAv)
    (@ t1_aAx)
    (@ t2_aAy)
    (@ r_aAB)
    _
    (eta1_X7t :: Test.K t2_aAy t1_aAx) ->
    eta1_X7t

a11_r1i9
  :: forall t_aBy r_aBB. (t_aBy -> r_aBB) -> Test.I t_aBy -> r_aBB
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType C(S)L]
a11_r1i9 =
  \ (@ t_aBy)
    (@ r_aBB)
    (eta_B2 :: t_aBy -> r_aBB)
    (eta1_B1 :: Test.I t_aBy) ->
    eta_B2 (eta1_B1 `cast` (Test.NTCo:I t_aBy :: Test.I t_aBy ~ t_aBy))

Test.unI1 :: forall r_auO. Test.I r_auO -> Test.I r_auO
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unI1 = \ (@ r_auO) (ds_dSv :: Test.I r_auO) -> ds_dSv

Test.unK1
  :: forall a_auQ r_auR. Test.K a_auQ r_auR -> Test.K a_auQ r_auR
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unK1 =
  \ (@ a_auQ) (@ r_auR) (ds_dSx :: Test.K a_auQ r_auR) -> ds_dSx

Test.unK :: forall a_ajc r_ajd. Test.K a_ajc r_ajd -> a_ajc
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unK =
  Test.unK1
  `cast` (forall a_auQ r_auR.
          Test.K a_auQ r_auR -> Test.NTCo:K a_auQ r_auR
          :: (forall a_auQ r_auR. Test.K a_auQ r_auR -> Test.K a_auQ r_auR)
               ~
             (forall a_auQ r_auR. Test.K a_auQ r_auR -> a_auQ))

Test.unI :: forall r_ajb. Test.I r_ajb -> r_ajb
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unI =
  Test.unI1
  `cast` (forall r_auO. Test.I r_auO -> Test.NTCo:I r_auO
          :: (forall r_auO. Test.I r_auO -> Test.I r_auO)
               ~
             (forall r_auO. Test.I r_auO -> r_auO))

Test.unC
  :: forall c_aj1 (f_aj2 :: * -> *) r_aj3.
     Test.C c_aj1 f_aj2 r_aj3 -> f_aj2 r_aj3
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(S),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_auK)
                 (@ f_auL::* -> *)
                 (@ r_auM)
                 (ds_dSs [Occ=Once!] :: Test.C c_auK f_auL r_auM) ->
                 case ds_dSs of _ { Test.C ds1_dSt [Occ=Once] -> ds1_dSt }}]
Test.unC =
  \ (@ c_auK)
    (@ f_auL::* -> *)
    (@ r_auM)
    (ds_dSs :: Test.C c_auK f_auL r_auM) ->
    case ds_dSs of _ { Test.C ds1_dSt -> ds1_dSt }

Test.fromLogic [InlPrag=INLINE]
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     Test.Logic
     -> (Test.:+:)
          (Test.C c_avX (Test.K GHC.Base.String))
          (Test.C c1_aw7 Test.U
           Test.:+: (Test.C c2_awj Test.U
                     Test.:+: (Test.C c3_awx Test.I
                               Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        c7_ay1
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_avX)
                 (@ c1_aw7)
                 (@ c2_awj)
                 (@ c3_awx)
                 (@ c4_awQ)
                 (@ c5_axc)
                 (@ c6_axB)
                 (@ c7_ay1)
                 (ds_dRV [Occ=Once!] :: Test.Logic) ->
                 case ds_dRV of _ {
                   Test.Var f0_ajE [Occ=Once] ->
                     Test.L
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.C
                          @ c_avX
                          @ (Test.K GHC.Base.String)
                          @ Test.Logic
                          (f0_ajE
                           `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                                   :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
                   Test.T ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.L
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.C @ c1_aw7 @ Test.U @ Test.Logic (Test.U @ Test.Logic)));
                   Test.F ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.L
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.C @ c2_awj @ Test.U @ Test.Logic (Test.U @ Test.Logic))));
                   Test.Not f0_ajF [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.L
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.C
                                   @ c3_awx
                                   @ Test.I
                                   @ Test.Logic
                                   (f0_ajF
                                    `cast` (sym (Test.NTCo:I Test.Logic)
                                            :: Test.Logic ~ Test.I Test.Logic))))));
                   Test.Impl f0_ajG [Occ=Once] f1_ajH [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.L
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.C
                                      @ c4_awQ
                                      @ (Test.I Test.:*: Test.I)
                                      @ Test.Logic
                                      (Test.:*:
                                         @ Test.I
                                         @ Test.I
                                         @ Test.Logic
                                         (f0_ajG
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))
                                         (f1_ajH
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))))))));
                   Test.Equiv f0_ajI [Occ=Once] f1_ajJ [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.L
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.C
                                         @ c5_axc
                                         @ (Test.I Test.:*: Test.I)
                                         @ Test.Logic
                                         (Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ Test.Logic
                                            (f0_ajI
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic))
                                            (f1_ajJ
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic)))))))));
                   Test.Conj f0_ajK [Occ=Once] f1_ajL [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.L
                                         @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c6_axB
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajK
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajL
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))));
                   Test.Disj f0_ajM [Occ=Once] f1_ajN [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c7_ay1
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajM
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajN
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))))
                 }}]
Test.fromLogic =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1)
    (eta_B1 :: Test.Logic) ->
    case eta_B1 of _ {
      Test.Var f0_ajE ->
        Test.L
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.C
             @ c_avX
             @ (Test.K GHC.Base.String)
             @ Test.Logic
             (f0_ajE
              `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                      :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
      Test.T ->
        lvl9_r1hD
          @ c_avX
          @ c1_aw7
          @ c2_awj
          @ c3_awx
          @ c4_awQ
          @ c5_axc
          @ c6_axB
          @ c7_ay1;
      Test.F ->
        lvl6_r1hx
          @ c_avX
          @ c1_aw7
          @ c2_awj
          @ c3_awx
          @ c4_awQ
          @ c5_axc
          @ c6_axB
          @ c7_ay1;
      Test.Not f0_ajF ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.L
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.C
                      @ c3_awx
                      @ Test.I
                      @ Test.Logic
                      (f0_ajF
                       `cast` (sym (Test.NTCo:I Test.Logic)
                               :: Test.Logic ~ Test.I Test.Logic))))));
      Test.Impl f0_ajG f1_ajH ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.L
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.C
                         @ c4_awQ
                         @ (Test.I Test.:*: Test.I)
                         @ Test.Logic
                         (Test.:*:
                            @ Test.I
                            @ Test.I
                            @ Test.Logic
                            (f0_ajG
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))
                            (f1_ajH
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))))))));
      Test.Equiv f0_ajI f1_ajJ ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.L
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.C
                            @ c5_axc
                            @ (Test.I Test.:*: Test.I)
                            @ Test.Logic
                            (Test.:*:
                               @ Test.I
                               @ Test.I
                               @ Test.Logic
                               (f0_ajI
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic))
                               (f1_ajJ
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic)))))))));
      Test.Conj f0_ajK f1_ajL ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.L
                            @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c6_axB
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ajK
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ajL
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))));
      Test.Disj f0_ajM f1_ajN ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.R
                            @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c7_ay1
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ajM
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ajN
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))))
    }

Test.toLogic [InlPrag=INLINE]
  :: forall t_av9 t1_avf t2_avj t3_avn t4_avs t5_avy t6_avE t7_avI.
     (Test.:+:)
       (Test.C t_av9 (Test.K GHC.Base.String))
       (Test.C t1_avf Test.U
        Test.:+: (Test.C t2_avj Test.U
                  Test.:+: (Test.C t3_avn Test.I
                            Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C t5_avy (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C t6_avE (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     t7_avI
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
     -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_av9)
                 (@ t1_avf)
                 (@ t2_avj)
                 (@ t3_avn)
                 (@ t4_avs)
                 (@ t5_avy)
                 (@ t6_avE)
                 (@ t7_avI)
                 (ds_dQY [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C t_av9 (Test.K GHC.Base.String))
                         (Test.C t1_avf Test.U
                          Test.:+: (Test.C t2_avj Test.U
                                    Test.:+: (Test.C t3_avn Test.I
                                              Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    t5_avy (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              t6_avE
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       t7_avI
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                         Test.Logic) ->
                 case ds_dQY of _ {
                   Test.L ds1_dRr [Occ=Once!] ->
                     case ds1_dRr of _ { Test.C ds2_dRs [Occ=Once] ->
                     Test.Var
                       (ds2_dRs
                        `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                     };
                   Test.R ds1_dQZ [Occ=Once!] ->
                     case ds1_dQZ of _ {
                       Test.L ds2_dRp [Occ=Once!] ->
                         case ds2_dRp of _ { Test.C ds3_dRq [Occ=Once!] ->
                         case ds3_dRq of _ { Test.U -> Test.T }
                         };
                       Test.R ds2_dR0 [Occ=Once!] ->
                         case ds2_dR0 of _ {
                           Test.L ds3_dRn [Occ=Once!] ->
                             case ds3_dRn of _ { Test.C ds4_dRo [Occ=Once!] ->
                             case ds4_dRo of _ { Test.U -> Test.F }
                             };
                           Test.R ds3_dR1 [Occ=Once!] ->
                             case ds3_dR1 of _ {
                               Test.L ds4_dRl [Occ=Once!] ->
                                 case ds4_dRl of _ { Test.C ds5_dRm [Occ=Once] ->
                                 Test.Not
                                   (ds5_dRm
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 };
                               Test.R ds4_dR2 [Occ=Once!] ->
                                 case ds4_dR2 of _ {
                                   Test.L ds5_dRh [Occ=Once!] ->
                                     case ds5_dRh of _ { Test.C ds6_dRi [Occ=Once!] ->
                                     case ds6_dRi
                                     of _ { Test.:*: ds7_dRj [Occ=Once] ds8_dRk [Occ=Once] ->
                                     Test.Impl
                                       (ds7_dRj
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds8_dRk
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds5_dR3 [Occ=Once!] ->
                                     case ds5_dR3 of _ {
                                       Test.L ds6_dRd [Occ=Once!] ->
                                         case ds6_dRd of _ { Test.C ds7_dRe [Occ=Once!] ->
                                         case ds7_dRe
                                         of _ { Test.:*: ds8_dRf [Occ=Once] ds9_dRg [Occ=Once] ->
                                         Test.Equiv
                                           (ds8_dRf
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds9_dRg
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds6_dR4 [Occ=Once!] ->
                                         case ds6_dR4 of _ {
                                           Test.L ds7_dR9 [Occ=Once!] ->
                                             case ds7_dR9 of _ { Test.C ds8_dRa [Occ=Once!] ->
                                             case ds8_dRa
                                             of _
                                             { Test.:*: ds9_dRb [Occ=Once] ds10_dRc [Occ=Once] ->
                                             Test.Conj
                                               (ds9_dRb
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dRc
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             };
                                           Test.R ds7_dR5 [Occ=Once!] ->
                                             case ds7_dR5 of _ { Test.C ds8_dR6 [Occ=Once!] ->
                                             case ds8_dR6
                                             of _
                                             { Test.:*: ds9_dR7 [Occ=Once] ds10_dR8 [Occ=Once] ->
                                             Test.Disj
                                               (ds9_dR7
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dR8
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }}]
Test.toLogic =
  \ (@ t_av9)
    (@ t1_avf)
    (@ t2_avj)
    (@ t3_avn)
    (@ t4_avs)
    (@ t5_avy)
    (@ t6_avE)
    (@ t7_avI)
    (eta_B1
       :: (Test.:+:)
            (Test.C t_av9 (Test.K GHC.Base.String))
            (Test.C t1_avf Test.U
             Test.:+: (Test.C t2_avj Test.U
                       Test.:+: (Test.C t3_avn Test.I
                                 Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C t5_avy (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 t6_avE (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          t7_avI
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
            Test.Logic) ->
    case eta_B1 of _ {
      Test.L ds_dRr ->
        case ds_dRr of _ { Test.C ds1_dRs ->
        Test.Var
          (ds1_dRs
           `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                   :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
        };
      Test.R ds_dQZ ->
        case ds_dQZ of _ {
          Test.L ds1_dRp ->
            case ds1_dRp of _ { Test.C ds2_dRq ->
            case ds2_dRq of _ { Test.U -> Test.T }
            };
          Test.R ds1_dR0 ->
            case ds1_dR0 of _ {
              Test.L ds2_dRn ->
                case ds2_dRn of _ { Test.C ds3_dRo ->
                case ds3_dRo of _ { Test.U -> Test.F }
                };
              Test.R ds2_dR1 ->
                case ds2_dR1 of _ {
                  Test.L ds3_dRl ->
                    case ds3_dRl of _ { Test.C ds4_dRm ->
                    Test.Not
                      (ds4_dRm
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    };
                  Test.R ds3_dR2 ->
                    case ds3_dR2 of _ {
                      Test.L ds4_dRh ->
                        case ds4_dRh of _ { Test.C ds5_dRi ->
                        case ds5_dRi of _ { Test.:*: ds6_dRj ds7_dRk ->
                        Test.Impl
                          (ds6_dRj
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds7_dRk
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds4_dR3 ->
                        case ds4_dR3 of _ {
                          Test.L ds5_dRd ->
                            case ds5_dRd of _ { Test.C ds6_dRe ->
                            case ds6_dRe of _ { Test.:*: ds7_dRf ds8_dRg ->
                            Test.Equiv
                              (ds7_dRf
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds8_dRg
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds5_dR4 ->
                            case ds5_dR4 of _ {
                              Test.L ds6_dR9 ->
                                case ds6_dR9 of _ { Test.C ds7_dRa ->
                                case ds7_dRa of _ { Test.:*: ds8_dRb ds9_dRc ->
                                Test.Conj
                                  (ds8_dRb
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRc
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                };
                              Test.R ds6_dR5 ->
                                case ds6_dR5 of _ { Test.C ds7_dR6 ->
                                case ds7_dR6 of _ { Test.:*: ds8_dR7 ds9_dR8 ->
                                Test.Disj
                                  (ds8_dR7
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dR8
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

Test.updateStringfTimes [InlPrag=INLINE]
  :: forall (t_azR :: * -> *) (t1_azT :: * -> *) r_azX.
     (Test.UpdateString t_azR, Test.UpdateString t1_azT) =>
     (r_azX -> r_azX)
     -> (Test.:*:) t_azR t1_azT r_azX
     -> (Test.:*:) t_azR t1_azT r_azX
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azR::* -> *)
                 (@ t1_azT::* -> *)
                 (@ r_azX)
                 ($dUpdateString_aA1 [Occ=Once] :: Test.UpdateString t_azR)
                 ($dUpdateString1_aA2 [Occ=Once] :: Test.UpdateString t1_azT)
                 (f_ajz :: r_azX -> r_azX)
                 (ds_dS6 [Occ=Once!] :: (Test.:*:) t_azR t1_azT r_azX) ->
                 case ds_dS6 of _ { Test.:*: x_ajA [Occ=Once] y_ajB [Occ=Once] ->
                 Test.:*:
                   @ t_azR
                   @ t1_azT
                   @ r_azX
                   (($dUpdateString_aA1
                     `cast` (Test.NTCo:T:UpdateString t_azR
                             :: Test.T:UpdateString t_azR
                                  ~
                                (forall a_aiO. (a_aiO -> a_aiO) -> t_azR a_aiO -> t_azR a_aiO)))
                      @ r_azX f_ajz x_ajA)
                   (($dUpdateString1_aA2
                     `cast` (Test.NTCo:T:UpdateString t1_azT
                             :: Test.T:UpdateString t1_azT
                                  ~
                                (forall a_aiO. (a_aiO -> a_aiO) -> t1_azT a_aiO -> t1_azT a_aiO)))
                      @ r_azX f_ajz y_ajB)
                 }}]
Test.updateStringfTimes =
  \ (@ t_azR::* -> *)
    (@ t1_azT::* -> *)
    (@ r_azX)
    ($dUpdateString_aA1 :: Test.UpdateString t_azR)
    ($dUpdateString1_aA2 :: Test.UpdateString t1_azT)
    (eta_B2 :: r_azX -> r_azX)
    (eta1_B1 :: (Test.:*:) t_azR t1_azT r_azX) ->
    case eta1_B1 of _ { Test.:*: x_ajA y_ajB ->
    Test.:*:
      @ t_azR
      @ t1_azT
      @ r_azX
      (($dUpdateString_aA1
        `cast` (Test.NTCo:T:UpdateString t_azR
                :: Test.T:UpdateString t_azR
                     ~
                   (forall a_aiO. (a_aiO -> a_aiO) -> t_azR a_aiO -> t_azR a_aiO)))
         @ r_azX eta_B2 x_ajA)
      (($dUpdateString1_aA2
        `cast` (Test.NTCo:T:UpdateString t1_azT
                :: Test.T:UpdateString t1_azT
                     ~
                   (forall a_aiO. (a_aiO -> a_aiO) -> t1_azT a_aiO -> t1_azT a_aiO)))
         @ r_azX eta_B2 y_ajB)
    }

Test.$fUpdateString:*:1
  :: forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
     (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
     forall a_aPI.
     (a_aPI -> a_aPI)
     -> (Test.:*:) f_ak0 g_ak1 a_aPI
     -> (Test.:*:) f_ak0 g_ak1 a_aPI
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_ak0::* -> *)
                 (@ g_ak1::* -> *)
                 ($dUpdateString_aPC [Occ=Once] :: Test.UpdateString f_ak0)
                 ($dUpdateString1_aPD [Occ=Once] :: Test.UpdateString g_ak1)
                 (@ a_aPI)
                 (eta_B2 [Occ=Once] :: a_aPI -> a_aPI)
                 (eta1_B1 [Occ=Once] :: (Test.:*:) f_ak0 g_ak1 a_aPI) ->
                 Test.updateStringfTimes
                   @ f_ak0
                   @ g_ak1
                   @ a_aPI
                   $dUpdateString_aPC
                   $dUpdateString1_aPD
                   eta_B2
                   eta1_B1}]
Test.$fUpdateString:*:1 =
  \ (@ f_ak0::* -> *)
    (@ g_ak1::* -> *)
    ($dUpdateString_aPC :: Test.UpdateString f_ak0)
    ($dUpdateString1_aPD :: Test.UpdateString g_ak1)
    (@ a_aPI)
    (eta_B2 :: a_aPI -> a_aPI)
    (eta1_B1 :: (Test.:*:) f_ak0 g_ak1 a_aPI) ->
    Test.updateStringfTimes
      @ f_ak0
      @ g_ak1
      @ a_aPI
      $dUpdateString_aPC
      $dUpdateString1_aPD
      eta_B2
      eta1_B1

Test.updateStringfPlus [InlPrag=INLINE]
  :: forall (t_aAb :: * -> *) r_aAg (g_aAi :: * -> *).
     (Test.UpdateString t_aAb, Test.UpdateString g_aAi) =>
     (r_aAg -> r_aAg)
     -> (Test.:+:) t_aAb g_aAi r_aAg
     -> (Test.:+:) t_aAb g_aAi r_aAg
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAb::* -> *)
                 (@ r_aAg)
                 (@ g_aAi::* -> *)
                 ($dUpdateString_aAp [Occ=Once] :: Test.UpdateString t_aAb)
                 ($dUpdateString1_aAq [Occ=Once] :: Test.UpdateString g_aAi)
                 (f_ajv [Occ=Once*] :: r_aAg -> r_aAg)
                 (ds_dS8 [Occ=Once!] :: (Test.:+:) t_aAb g_aAi r_aAg) ->
                 case ds_dS8 of _ {
                   Test.L x_ajw [Occ=Once] ->
                     Test.L
                       @ t_aAb
                       @ g_aAi
                       @ r_aAg
                       (($dUpdateString_aAp
                         `cast` (Test.NTCo:T:UpdateString t_aAb
                                 :: Test.T:UpdateString t_aAb
                                      ~
                                    (forall a_aiO. (a_aiO -> a_aiO) -> t_aAb a_aiO -> t_aAb a_aiO)))
                          @ r_aAg f_ajv x_ajw);
                   Test.R x_ajy [Occ=Once] ->
                     Test.R
                       @ t_aAb
                       @ g_aAi
                       @ r_aAg
                       (($dUpdateString1_aAq
                         `cast` (Test.NTCo:T:UpdateString g_aAi
                                 :: Test.T:UpdateString g_aAi
                                      ~
                                    (forall a_aiO. (a_aiO -> a_aiO) -> g_aAi a_aiO -> g_aAi a_aiO)))
                          @ r_aAg f_ajv x_ajy)
                 }}]
Test.updateStringfPlus =
  \ (@ t_aAb::* -> *)
    (@ r_aAg)
    (@ g_aAi::* -> *)
    ($dUpdateString_aAp :: Test.UpdateString t_aAb)
    ($dUpdateString1_aAq :: Test.UpdateString g_aAi)
    (eta_B2 :: r_aAg -> r_aAg)
    (eta1_B1 :: (Test.:+:) t_aAb g_aAi r_aAg) ->
    case eta1_B1 of _ {
      Test.L x_ajw ->
        Test.L
          @ t_aAb
          @ g_aAi
          @ r_aAg
          (($dUpdateString_aAp
            `cast` (Test.NTCo:T:UpdateString t_aAb
                    :: Test.T:UpdateString t_aAb
                         ~
                       (forall a_aiO. (a_aiO -> a_aiO) -> t_aAb a_aiO -> t_aAb a_aiO)))
             @ r_aAg eta_B2 x_ajw);
      Test.R x_ajy ->
        Test.R
          @ t_aAb
          @ g_aAi
          @ r_aAg
          (($dUpdateString1_aAq
            `cast` (Test.NTCo:T:UpdateString g_aAi
                    :: Test.T:UpdateString g_aAi
                         ~
                       (forall a_aiO. (a_aiO -> a_aiO) -> g_aAi a_aiO -> g_aAi a_aiO)))
             @ r_aAg eta_B2 x_ajy)
    }

Test.$fUpdateString:+:1
  :: forall (f_ak2 :: * -> *) (g_ak3 :: * -> *).
     (Test.UpdateString f_ak2, Test.UpdateString g_ak3) =>
     forall a_aPT.
     (a_aPT -> a_aPT)
     -> (Test.:+:) f_ak2 g_ak3 a_aPT
     -> (Test.:+:) f_ak2 g_ak3 a_aPT
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0 0 0] 5 0}]
Test.$fUpdateString:+:1 =
  \ (@ f_ak2::* -> *)
    (@ g_ak3::* -> *)
    ($dUpdateString_aPN :: Test.UpdateString f_ak2)
    ($dUpdateString1_aPO :: Test.UpdateString g_ak3)
    (@ a_aPT)
    (eta_B2 :: a_aPT -> a_aPT)
    (eta1_B1 :: (Test.:+:) f_ak2 g_ak3 a_aPT) ->
    Test.updateStringfPlus
      @ f_ak2
      @ a_aPT
      @ g_ak3
      $dUpdateString_aPN
      $dUpdateString1_aPO
      eta_B2
      eta1_B1

Test.updateStringfC [InlPrag=INLINE]
  :: forall t_azw (t1_azz :: * -> *) r_azE c_azG.
     (Test.UpdateString t1_azz) =>
     (r_azE -> r_azE)
     -> Test.C t_azw t1_azz r_azE
     -> Test.C c_azG t1_azz r_azE
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azw)
                 (@ t1_azz::* -> *)
                 (@ r_azE)
                 (@ c_azG)
                 ($dUpdateString_azI [Occ=Once] :: Test.UpdateString t1_azz)
                 (f_ajC [Occ=Once] :: r_azE -> r_azE)
                 (ds_dS4 [Occ=Once!] :: Test.C t_azw t1_azz r_azE) ->
                 case ds_dS4 of _ { Test.C x_ajD [Occ=Once] ->
                 Test.C
                   @ c_azG
                   @ t1_azz
                   @ r_azE
                   (($dUpdateString_azI
                     `cast` (Test.NTCo:T:UpdateString t1_azz
                             :: Test.T:UpdateString t1_azz
                                  ~
                                (forall a_aiO. (a_aiO -> a_aiO) -> t1_azz a_aiO -> t1_azz a_aiO)))
                      @ r_azE f_ajC x_ajD)
                 }}]
Test.updateStringfC =
  \ (@ t_azw)
    (@ t1_azz::* -> *)
    (@ r_azE)
    (@ c_azG)
    ($dUpdateString_azI :: Test.UpdateString t1_azz)
    (eta_B2 :: r_azE -> r_azE)
    (eta1_B1 :: Test.C t_azw t1_azz r_azE) ->
    case eta1_B1 of _ { Test.C x_ajD ->
    Test.C
      @ c_azG
      @ t1_azz
      @ r_azE
      (($dUpdateString_azI
        `cast` (Test.NTCo:T:UpdateString t1_azz
                :: Test.T:UpdateString t1_azz
                     ~
                   (forall a_aiO. (a_aiO -> a_aiO) -> t1_azz a_aiO -> t1_azz a_aiO)))
         @ r_azE eta_B2 x_ajD)
    }

Test.$fUpdateStringC1
  :: forall (f_ajY :: * -> *) c_ajZ.
     (Test.UpdateString f_ajY) =>
     forall a_aPy.
     (a_aPy -> a_aPy)
     -> Test.C c_ajZ f_ajY a_aPy
     -> Test.C c_ajZ f_ajY a_aPy
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_ajY::* -> *)
                 (@ c_ajZ)
                 ($dUpdateString_aPt [Occ=Once] :: Test.UpdateString f_ajY)
                 (@ a_aPy)
                 (eta_B2 [Occ=Once] :: a_aPy -> a_aPy)
                 (eta1_B1 [Occ=Once] :: Test.C c_ajZ f_ajY a_aPy) ->
                 Test.updateStringfC
                   @ c_ajZ @ f_ajY @ a_aPy @ c_ajZ $dUpdateString_aPt eta_B2 eta1_B1}]
Test.$fUpdateStringC1 =
  \ (@ f_ajY::* -> *)
    (@ c_ajZ)
    ($dUpdateString_aPt :: Test.UpdateString f_ajY)
    (@ a_aPy)
    (eta_B2 :: a_aPy -> a_aPy)
    (eta1_B1 :: Test.C c_ajZ f_ajY a_aPy) ->
    Test.updateStringfC
      @ c_ajZ @ f_ajY @ a_aPy @ c_ajZ $dUpdateString_aPt eta_B2 eta1_B1

Test.updateStringfI [InlPrag=INLINE]
  :: forall t_aBy r_aBB.
     (t_aBy -> r_aBB) -> Test.I t_aBy -> Test.I r_aBB
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= (\ (@ t_XFb)
                  (@ r_XFf)
                  (f_ajf [Occ=Once!] :: t_XFb -> r_XFf)
                  (ds_dSh [Occ=Once] :: Test.I t_XFb) ->
                  f_ajf (ds_dSh `cast` (Test.NTCo:I t_XFb :: Test.I t_XFb ~ t_XFb)))
               `cast` (forall t_XFb r_XFf.
                       (t_XFb -> r_XFf) -> Test.I t_XFb -> sym (Test.NTCo:I r_XFf)
                       :: (forall t_XFb r_XFf. (t_XFb -> r_XFf) -> Test.I t_XFb -> r_XFf)
                            ~
                          (forall t_XFb r_XFf.
                           (t_XFb -> r_XFf) -> Test.I t_XFb -> Test.I r_XFf))}]
Test.updateStringfI =
  a11_r1i9
  `cast` (forall t_aBy r_aBB.
          (t_aBy -> r_aBB) -> Test.I t_aBy -> sym (Test.NTCo:I r_aBB)
          :: (forall t_aBy r_aBB. (t_aBy -> r_aBB) -> Test.I t_aBy -> r_aBB)
               ~
             (forall t_aBy r_aBB.
              (t_aBy -> r_aBB) -> Test.I t_aBy -> Test.I r_aBB))

Test.updateString1
  :: forall a_XVl.
     (a_XVl -> a_XVl)
     -> (Test.:+:)
          (Test.C Test.Logic_Not_ Test.I)
          (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
           Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                               Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
          a_XVl
     -> (Test.:+:)
          (Test.C Test.Logic_Not_ Test.I)
          (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
           Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                               Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
          a_XVl
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType LS]
Test.updateString1 =
  \ (@ a_XVl)
    (eta_X5v :: a_XVl -> a_XVl)
    (eta1_XaZ
       :: (Test.:+:)
            (Test.C Test.Logic_Not_ Test.I)
            (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
             Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                       Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                 Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
            a_XVl) ->
    case eta1_XaZ of _ {
      Test.L x_ajw ->
        Test.L
          @ (Test.C Test.Logic_Not_ Test.I)
          @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
             Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                       Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                 Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
          @ a_XVl
          (case x_ajw of _ { Test.C x1_ajD ->
           Test.C
             @ Test.Logic_Not_
             @ Test.I
             @ a_XVl
             (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v x1_ajD)
           });
      Test.R x_ajy ->
        Test.R
          @ (Test.C Test.Logic_Not_ Test.I)
          @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
             Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                       Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                 Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
          @ a_XVl
          (case x_ajy of _ {
             Test.L x1_ajw ->
               Test.L
                 @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                    Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                              Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                 @ a_XVl
                 (case x1_ajw of _ { Test.C x2_ajD ->
                  Test.C
                    @ Test.Logic_Impl_
                    @ (Test.I Test.:*: Test.I)
                    @ a_XVl
                    (case x2_ajD of _ { Test.:*: x3_ajA y_ajB ->
                     Test.:*:
                       @ Test.I
                       @ Test.I
                       @ a_XVl
                       (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v x3_ajA)
                       (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v y_ajB)
                     })
                  });
             Test.R x1_Xp9 ->
               Test.R
                 @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                    Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                              Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                 @ a_XVl
                 (case x1_Xp9 of _ {
                    Test.L x2_ajw ->
                      Test.L
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ a_XVl
                        (case x2_ajw of _ { Test.C x3_ajD ->
                         Test.C
                           @ Test.Logic_Equiv_
                           @ (Test.I Test.:*: Test.I)
                           @ a_XVl
                           (case x3_ajD of _ { Test.:*: x4_ajA y_ajB ->
                            Test.:*:
                              @ Test.I
                              @ Test.I
                              @ a_XVl
                              (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v x4_ajA)
                              (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v y_ajB)
                            })
                         });
                    Test.R x2_XoY ->
                      Test.R
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ a_XVl
                        (case x2_XoY of _ {
                           Test.L x3_ajw ->
                             Test.L
                               @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                               @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                               @ a_XVl
                               (case x3_ajw of _ { Test.C x4_ajD ->
                                Test.C
                                  @ Test.Logic_Conj_
                                  @ (Test.I Test.:*: Test.I)
                                  @ a_XVl
                                  (case x4_ajD of _ { Test.:*: x5_ajA y_ajB ->
                                   Test.:*:
                                     @ Test.I
                                     @ Test.I
                                     @ a_XVl
                                     (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v x5_ajA)
                                     (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v y_ajB)
                                   })
                                });
                           Test.R x3_XoN ->
                             Test.R
                               @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                               @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                               @ a_XVl
                               (case x3_XoN of _ { Test.C x4_ajD ->
                                Test.C
                                  @ Test.Logic_Disj_
                                  @ (Test.I Test.:*: Test.I)
                                  @ a_XVl
                                  (case x4_ajD of _ { Test.:*: x5_ajA y_ajB ->
                                   Test.:*:
                                     @ Test.I
                                     @ Test.I
                                     @ a_XVl
                                     (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v x5_ajA)
                                     (Test.updateStringfI @ a_XVl @ a_XVl eta_X5v y_ajB)
                                   })
                                })
                         })
                  })
           })
    }

Rec {
Test.testLogic_updateString :: Test.Logic -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
Test.testLogic_updateString =
  \ (x_aUp :: Test.Logic) ->
    case x_aUp of _ {
      Test.Var f0_ajE ->
        Test.Var
          (case f0_ajE of _ {
             [] -> GHC.Types.[] @ GHC.Types.Char;
             : ipv_sU3 ipv1_sU4 ->
               GHC.Types.:
                 @ GHC.Types.Char
                 (GHC.List.last_last' @ GHC.Types.Char ipv_sU3 ipv1_sU4)
                 ipv1_sU4
           });
      Test.T -> Test.T;
      Test.F -> Test.F;
      Test.Not f0_ajF ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.L
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.C
                     @ Test.Logic_Not_
                     @ Test.I
                     @ Test.Logic
                     (f0_ajF
                      `cast` (sym (Test.NTCo:I Test.Logic)
                              :: Test.Logic ~ Test.I Test.Logic))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Impl f0_ajG f1_ajH ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.L
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.C
                        @ Test.Logic_Impl_
                        @ (Test.I Test.:*: Test.I)
                        @ Test.Logic
                        (Test.:*:
                           @ Test.I
                           @ Test.I
                           @ Test.Logic
                           (f0_ajG
                            `cast` (sym (Test.NTCo:I Test.Logic)
                                    :: Test.Logic ~ Test.I Test.Logic))
                           (f1_ajH
                            `cast` (sym (Test.NTCo:I Test.Logic)
                                    :: Test.Logic ~ Test.I Test.Logic))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Equiv f0_ajI f1_ajJ ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.L
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ Test.Logic
                        (Test.C
                           @ Test.Logic_Equiv_
                           @ (Test.I Test.:*: Test.I)
                           @ Test.Logic
                           (Test.:*:
                              @ Test.I
                              @ Test.I
                              @ Test.Logic
                              (f0_ajI
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic))
                              (f1_ajJ
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic)))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Conj f0_ajK f1_ajL ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ Test.Logic
                        (Test.L
                           @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                           @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Conj_
                              @ (Test.I Test.:*: Test.I)
                              @ Test.Logic
                              (Test.:*:
                                 @ Test.I
                                 @ Test.I
                                 @ Test.Logic
                                 (f0_ajK
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))
                                 (f1_ajL
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Disj f0_ajM f1_ajN ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                           @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Disj_
                              @ (Test.I Test.:*: Test.I)
                              @ Test.Logic
                              (Test.:*:
                                 @ Test.I
                                 @ Test.I
                                 @ Test.Logic
                                 (f0_ajM
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))
                                 (f1_ajN
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        }
    }
end Rec }

Rec {
updateString2_r1ib :: Test.Logic -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
updateString2_r1ib =
  \ (x_aUp :: Test.Logic) ->
    case x_aUp of _ {
      Test.Var f0_ajE ->
        Test.Var
          (case f0_ajE of _ {
             [] -> GHC.Types.[] @ GHC.Types.Char;
             : ipv_sU3 ipv1_sU4 ->
               GHC.Types.:
                 @ GHC.Types.Char
                 (GHC.List.last_last' @ GHC.Types.Char ipv_sU3 ipv1_sU4)
                 ipv1_sU4
           });
      Test.T -> Test.T;
      Test.F -> Test.F;
      Test.Not f0_ajF ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1ib
               (Test.L
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.C
                     @ Test.Logic_Not_
                     @ Test.I
                     @ Test.Logic
                     (f0_ajF
                      `cast` (sym (Test.NTCo:I Test.Logic)
                              :: Test.Logic ~ Test.I Test.Logic))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Impl f0_ajG f1_ajH ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1ib
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.L
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.C
                        @ Test.Logic_Impl_
                        @ (Test.I Test.:*: Test.I)
                        @ Test.Logic
                        (Test.:*:
                           @ Test.I
                           @ Test.I
                           @ Test.Logic
                           (f0_ajG
                            `cast` (sym (Test.NTCo:I Test.Logic)
                                    :: Test.Logic ~ Test.I Test.Logic))
                           (f1_ajH
                            `cast` (sym (Test.NTCo:I Test.Logic)
                                    :: Test.Logic ~ Test.I Test.Logic))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Equiv f0_ajI f1_ajJ ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1ib
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.L
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ Test.Logic
                        (Test.C
                           @ Test.Logic_Equiv_
                           @ (Test.I Test.:*: Test.I)
                           @ Test.Logic
                           (Test.:*:
                              @ Test.I
                              @ Test.I
                              @ Test.Logic
                              (f0_ajI
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic))
                              (f1_ajJ
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic)))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Conj f0_ajK f1_ajL ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1ib
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ Test.Logic
                        (Test.L
                           @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                           @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Conj_
                              @ (Test.I Test.:*: Test.I)
                              @ Test.Logic
                              (Test.:*:
                                 @ Test.I
                                 @ Test.I
                                 @ Test.Logic
                                 (f0_ajK
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))
                                 (f1_ajL
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        };
      Test.Disj f0_ajM f1_ajN ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1ib
               (Test.R
                  @ (Test.C Test.Logic_Not_ Test.I)
                  @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                               Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C
                                                    Test.Logic_Disj_ (Test.I Test.:*: Test.I))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                     @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                  Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                        @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                           Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                           @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Disj_
                              @ (Test.I Test.:*: Test.I)
                              @ Test.Logic
                              (Test.:*:
                                 @ Test.I
                                 @ Test.I
                                 @ Test.Logic
                                 (f0_ajM
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))
                                 (f1_ajN
                                  `cast` (sym (Test.NTCo:I Test.Logic)
                                          :: Test.Logic ~ Test.I Test.Logic))))))))
        of _ {
          Test.L ds_dRl ->
            case ds_dRl of _ { Test.C ds1_dRm ->
            Test.Not
              (ds1_dRm
               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
            };
          Test.R ds_dR2 ->
            case ds_dR2 of _ {
              Test.L ds1_dRh ->
                case ds1_dRh of _ { Test.C ds2_dRi ->
                case ds2_dRi of _ { Test.:*: ds3_dRj ds4_dRk ->
                Test.Impl
                  (ds3_dRj
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                  (ds4_dRk
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                }
                };
              Test.R ds1_dR3 ->
                case ds1_dR3 of _ {
                  Test.L ds2_dRd ->
                    case ds2_dRd of _ { Test.C ds3_dRe ->
                    case ds3_dRe of _ { Test.:*: ds4_dRf ds5_dRg ->
                    Test.Equiv
                      (ds4_dRf
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds5_dRg
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds2_dR4 ->
                    case ds2_dR4 of _ {
                      Test.L ds3_dR9 ->
                        case ds3_dR9 of _ { Test.C ds4_dRa ->
                        case ds4_dRa of _ { Test.:*: ds5_dRb ds6_dRc ->
                        Test.Conj
                          (ds5_dRb
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dRc
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds3_dR5 ->
                        case ds3_dR5 of _ { Test.C ds4_dR6 ->
                        case ds4_dR6 of _ { Test.:*: ds5_dR7 ds6_dR8 ->
                        Test.Disj
                          (ds5_dR7
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds6_dR8
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        }
                    }
                }
            }
        }
    }
end Rec }

Test.updateString_$supdateString :: Test.Logic -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=InlineRule(sat, -)
         Tmpl= letrec {
                 updateString3_X14u [Occ=LoopBreaker] :: Test.Logic -> Test.Logic
                 [LclId, Arity=1]
                 updateString3_X14u =
                   \ (x_aUp [Occ=Once!] :: Test.Logic) ->
                     case x_aUp of _ {
                       Test.Var f0_ajE [Occ=Once!] ->
                         Test.Var
                           (case f0_ajE of _ {
                              [] -> GHC.Types.[] @ GHC.Types.Char;
                              : ipv_sU3 [Occ=Once] ipv1_sU4 ->
                                GHC.Types.:
                                  @ GHC.Types.Char
                                  (GHC.List.last_last' @ GHC.Types.Char ipv_sU3 ipv1_sU4)
                                  ipv1_sU4
                            });
                       Test.T -> Test.T;
                       Test.F -> Test.F;
                       Test.Not f0_ajF [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14u
                                (Test.L
                                   @ (Test.C Test.Logic_Not_ Test.I)
                                   @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I))))
                                   @ Test.Logic
                                   (Test.C
                                      @ Test.Logic_Not_
                                      @ Test.I
                                      @ Test.Logic
                                      (f0_ajF
                                       `cast` (sym (Test.NTCo:I Test.Logic)
                                               :: Test.Logic ~ Test.I Test.Logic))))
                         of _ {
                           Test.L ds_dRl [Occ=Once!] ->
                             case ds_dRl of _ { Test.C ds1_dRm [Occ=Once] ->
                             Test.Not
                               (ds1_dRm
                                `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                             };
                           Test.R ds_dR2 [Occ=Once!] ->
                             case ds_dR2 of _ {
                               Test.L ds1_dRh [Occ=Once!] ->
                                 case ds1_dRh of _ { Test.C ds2_dRi [Occ=Once!] ->
                                 case ds2_dRi
                                 of _ { Test.:*: ds3_dRj [Occ=Once] ds4_dRk [Occ=Once] ->
                                 Test.Impl
                                   (ds3_dRj
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                   (ds4_dRk
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 }
                                 };
                               Test.R ds1_dR3 [Occ=Once!] ->
                                 case ds1_dR3 of _ {
                                   Test.L ds2_dRd [Occ=Once!] ->
                                     case ds2_dRd of _ { Test.C ds3_dRe [Occ=Once!] ->
                                     case ds3_dRe
                                     of _ { Test.:*: ds4_dRf [Occ=Once] ds5_dRg [Occ=Once] ->
                                     Test.Equiv
                                       (ds4_dRf
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds5_dRg
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds2_dR4 [Occ=Once!] ->
                                     case ds2_dR4 of _ {
                                       Test.L ds3_dR9 [Occ=Once!] ->
                                         case ds3_dR9 of _ { Test.C ds4_dRa [Occ=Once!] ->
                                         case ds4_dRa
                                         of _ { Test.:*: ds5_dRb [Occ=Once] ds6_dRc [Occ=Once] ->
                                         Test.Conj
                                           (ds5_dRb
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dRc
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds3_dR5 [Occ=Once!] ->
                                         case ds3_dR5 of _ { Test.C ds4_dR6 [Occ=Once!] ->
                                         case ds4_dR6
                                         of _ { Test.:*: ds5_dR7 [Occ=Once] ds6_dR8 [Occ=Once] ->
                                         Test.Disj
                                           (ds5_dR7
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dR8
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         }
                                     }
                                 }
                             }
                         };
                       Test.Impl f0_ajG [Occ=Once] f1_ajH [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14u
                                (Test.R
                                   @ (Test.C Test.Logic_Not_ Test.I)
                                   @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I))))
                                   @ Test.Logic
                                   (Test.L
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ Test.Logic
                                      (Test.C
                                         @ Test.Logic_Impl_
                                         @ (Test.I Test.:*: Test.I)
                                         @ Test.Logic
                                         (Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ Test.Logic
                                            (f0_ajG
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic))
                                            (f1_ajH
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic))))))
                         of _ {
                           Test.L ds_dRl [Occ=Once!] ->
                             case ds_dRl of _ { Test.C ds1_dRm [Occ=Once] ->
                             Test.Not
                               (ds1_dRm
                                `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                             };
                           Test.R ds_dR2 [Occ=Once!] ->
                             case ds_dR2 of _ {
                               Test.L ds1_dRh [Occ=Once!] ->
                                 case ds1_dRh of _ { Test.C ds2_dRi [Occ=Once!] ->
                                 case ds2_dRi
                                 of _ { Test.:*: ds3_dRj [Occ=Once] ds4_dRk [Occ=Once] ->
                                 Test.Impl
                                   (ds3_dRj
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                   (ds4_dRk
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 }
                                 };
                               Test.R ds1_dR3 [Occ=Once!] ->
                                 case ds1_dR3 of _ {
                                   Test.L ds2_dRd [Occ=Once!] ->
                                     case ds2_dRd of _ { Test.C ds3_dRe [Occ=Once!] ->
                                     case ds3_dRe
                                     of _ { Test.:*: ds4_dRf [Occ=Once] ds5_dRg [Occ=Once] ->
                                     Test.Equiv
                                       (ds4_dRf
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds5_dRg
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds2_dR4 [Occ=Once!] ->
                                     case ds2_dR4 of _ {
                                       Test.L ds3_dR9 [Occ=Once!] ->
                                         case ds3_dR9 of _ { Test.C ds4_dRa [Occ=Once!] ->
                                         case ds4_dRa
                                         of _ { Test.:*: ds5_dRb [Occ=Once] ds6_dRc [Occ=Once] ->
                                         Test.Conj
                                           (ds5_dRb
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dRc
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds3_dR5 [Occ=Once!] ->
                                         case ds3_dR5 of _ { Test.C ds4_dR6 [Occ=Once!] ->
                                         case ds4_dR6
                                         of _ { Test.:*: ds5_dR7 [Occ=Once] ds6_dR8 [Occ=Once] ->
                                         Test.Disj
                                           (ds5_dR7
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dR8
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         }
                                     }
                                 }
                             }
                         };
                       Test.Equiv f0_ajI [Occ=Once] f1_ajJ [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14u
                                (Test.R
                                   @ (Test.C Test.Logic_Not_ Test.I)
                                   @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ Test.Logic
                                      (Test.L
                                         @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                         @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                            Test.:+: Test.C
                                                       Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ Test.Logic_Equiv_
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajI
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajJ
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic)))))))
                         of _ {
                           Test.L ds_dRl [Occ=Once!] ->
                             case ds_dRl of _ { Test.C ds1_dRm [Occ=Once] ->
                             Test.Not
                               (ds1_dRm
                                `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                             };
                           Test.R ds_dR2 [Occ=Once!] ->
                             case ds_dR2 of _ {
                               Test.L ds1_dRh [Occ=Once!] ->
                                 case ds1_dRh of _ { Test.C ds2_dRi [Occ=Once!] ->
                                 case ds2_dRi
                                 of _ { Test.:*: ds3_dRj [Occ=Once] ds4_dRk [Occ=Once] ->
                                 Test.Impl
                                   (ds3_dRj
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                   (ds4_dRk
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 }
                                 };
                               Test.R ds1_dR3 [Occ=Once!] ->
                                 case ds1_dR3 of _ {
                                   Test.L ds2_dRd [Occ=Once!] ->
                                     case ds2_dRd of _ { Test.C ds3_dRe [Occ=Once!] ->
                                     case ds3_dRe
                                     of _ { Test.:*: ds4_dRf [Occ=Once] ds5_dRg [Occ=Once] ->
                                     Test.Equiv
                                       (ds4_dRf
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds5_dRg
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds2_dR4 [Occ=Once!] ->
                                     case ds2_dR4 of _ {
                                       Test.L ds3_dR9 [Occ=Once!] ->
                                         case ds3_dR9 of _ { Test.C ds4_dRa [Occ=Once!] ->
                                         case ds4_dRa
                                         of _ { Test.:*: ds5_dRb [Occ=Once] ds6_dRc [Occ=Once] ->
                                         Test.Conj
                                           (ds5_dRb
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dRc
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds3_dR5 [Occ=Once!] ->
                                         case ds3_dR5 of _ { Test.C ds4_dR6 [Occ=Once!] ->
                                         case ds4_dR6
                                         of _ { Test.:*: ds5_dR7 [Occ=Once] ds6_dR8 [Occ=Once] ->
                                         Test.Disj
                                           (ds5_dR7
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dR8
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         }
                                     }
                                 }
                             }
                         };
                       Test.Conj f0_ajK [Occ=Once] f1_ajL [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14u
                                (Test.R
                                   @ (Test.C Test.Logic_Not_ Test.I)
                                   @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                         @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                            Test.:+: Test.C
                                                       Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.L
                                            @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                            @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                            @ Test.Logic
                                            (Test.C
                                               @ Test.Logic_Conj_
                                               @ (Test.I Test.:*: Test.I)
                                               @ Test.Logic
                                               (Test.:*:
                                                  @ Test.I
                                                  @ Test.I
                                                  @ Test.Logic
                                                  (f0_ajK
                                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                                           :: Test.Logic ~ Test.I Test.Logic))
                                                  (f1_ajL
                                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                                           :: Test.Logic ~ Test.I Test.Logic))))))))
                         of _ {
                           Test.L ds_dRl [Occ=Once!] ->
                             case ds_dRl of _ { Test.C ds1_dRm [Occ=Once] ->
                             Test.Not
                               (ds1_dRm
                                `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                             };
                           Test.R ds_dR2 [Occ=Once!] ->
                             case ds_dR2 of _ {
                               Test.L ds1_dRh [Occ=Once!] ->
                                 case ds1_dRh of _ { Test.C ds2_dRi [Occ=Once!] ->
                                 case ds2_dRi
                                 of _ { Test.:*: ds3_dRj [Occ=Once] ds4_dRk [Occ=Once] ->
                                 Test.Impl
                                   (ds3_dRj
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                   (ds4_dRk
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 }
                                 };
                               Test.R ds1_dR3 [Occ=Once!] ->
                                 case ds1_dR3 of _ {
                                   Test.L ds2_dRd [Occ=Once!] ->
                                     case ds2_dRd of _ { Test.C ds3_dRe [Occ=Once!] ->
                                     case ds3_dRe
                                     of _ { Test.:*: ds4_dRf [Occ=Once] ds5_dRg [Occ=Once] ->
                                     Test.Equiv
                                       (ds4_dRf
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds5_dRg
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds2_dR4 [Occ=Once!] ->
                                     case ds2_dR4 of _ {
                                       Test.L ds3_dR9 [Occ=Once!] ->
                                         case ds3_dR9 of _ { Test.C ds4_dRa [Occ=Once!] ->
                                         case ds4_dRa
                                         of _ { Test.:*: ds5_dRb [Occ=Once] ds6_dRc [Occ=Once] ->
                                         Test.Conj
                                           (ds5_dRb
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dRc
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds3_dR5 [Occ=Once!] ->
                                         case ds3_dR5 of _ { Test.C ds4_dR6 [Occ=Once!] ->
                                         case ds4_dR6
                                         of _ { Test.:*: ds5_dR7 [Occ=Once] ds6_dR8 [Occ=Once] ->
                                         Test.Disj
                                           (ds5_dR7
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dR8
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         }
                                     }
                                 }
                             }
                         };
                       Test.Disj f0_ajM [Occ=Once] f1_ajN [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14u
                                (Test.R
                                   @ (Test.C Test.Logic_Not_ Test.I)
                                   @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                         @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                            Test.:+: Test.C
                                                       Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.R
                                            @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                            @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                            @ Test.Logic
                                            (Test.C
                                               @ Test.Logic_Disj_
                                               @ (Test.I Test.:*: Test.I)
                                               @ Test.Logic
                                               (Test.:*:
                                                  @ Test.I
                                                  @ Test.I
                                                  @ Test.Logic
                                                  (f0_ajM
                                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                                           :: Test.Logic ~ Test.I Test.Logic))
                                                  (f1_ajN
                                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                                           :: Test.Logic ~ Test.I Test.Logic))))))))
                         of _ {
                           Test.L ds_dRl [Occ=Once!] ->
                             case ds_dRl of _ { Test.C ds1_dRm [Occ=Once] ->
                             Test.Not
                               (ds1_dRm
                                `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                             };
                           Test.R ds_dR2 [Occ=Once!] ->
                             case ds_dR2 of _ {
                               Test.L ds1_dRh [Occ=Once!] ->
                                 case ds1_dRh of _ { Test.C ds2_dRi [Occ=Once!] ->
                                 case ds2_dRi
                                 of _ { Test.:*: ds3_dRj [Occ=Once] ds4_dRk [Occ=Once] ->
                                 Test.Impl
                                   (ds3_dRj
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                   (ds4_dRk
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 }
                                 };
                               Test.R ds1_dR3 [Occ=Once!] ->
                                 case ds1_dR3 of _ {
                                   Test.L ds2_dRd [Occ=Once!] ->
                                     case ds2_dRd of _ { Test.C ds3_dRe [Occ=Once!] ->
                                     case ds3_dRe
                                     of _ { Test.:*: ds4_dRf [Occ=Once] ds5_dRg [Occ=Once] ->
                                     Test.Equiv
                                       (ds4_dRf
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds5_dRg
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds2_dR4 [Occ=Once!] ->
                                     case ds2_dR4 of _ {
                                       Test.L ds3_dR9 [Occ=Once!] ->
                                         case ds3_dR9 of _ { Test.C ds4_dRa [Occ=Once!] ->
                                         case ds4_dRa
                                         of _ { Test.:*: ds5_dRb [Occ=Once] ds6_dRc [Occ=Once] ->
                                         Test.Conj
                                           (ds5_dRb
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dRc
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds3_dR5 [Occ=Once!] ->
                                         case ds3_dR5 of _ { Test.C ds4_dR6 [Occ=Once!] ->
                                         case ds4_dR6
                                         of _ { Test.:*: ds5_dR7 [Occ=Once] ds6_dR8 [Occ=Once] ->
                                         Test.Disj
                                           (ds5_dR7
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds6_dR8
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         }
                                     }
                                 }
                             }
                         }
                     }; } in
               updateString3_X14u}]
Test.updateString_$supdateString = updateString2_r1ib

Test.updateStringfK [InlPrag=INLINE]
  :: forall t_aAv t1_aAx t2_aAy r_aAB.
     t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy r_aAB
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= (\ (@ t_XEg)
                  (@ t1_XEj)
                  (@ t2_XEl)
                  (@ r_XEp)
                  _
                  (ds_dSb [Occ=Once] :: Test.K t2_XEl t1_XEj) ->
                  ds_dSb)
               `cast` (forall t_XEg t1_XEj t2_XEl r_XEp.
                       t_XEg
                       -> Test.K t2_XEl t1_XEj
                       -> trans
                            (Test.NTCo:K t2_XEl t1_XEj) (sym (Test.NTCo:K t2_XEl r_XEp))
                       :: (forall t_XEg t1_XEj t2_XEl r_XEp.
                           t_XEg -> Test.K t2_XEl t1_XEj -> Test.K t2_XEl t1_XEj)
                            ~
                          (forall t_XEg t1_XEj t2_XEl r_XEp.
                           t_XEg -> Test.K t2_XEl t1_XEj -> Test.K t2_XEl r_XEp))}]
Test.updateStringfK =
  a_r1i7
  `cast` (forall t_aAv t1_aAx t2_aAy r_aAB.
          t_aAv
          -> Test.K t2_aAy t1_aAx
          -> trans
               (Test.NTCo:K t2_aAy t1_aAx) (sym (Test.NTCo:K t2_aAy r_aAB))
          :: (forall t_aAv t1_aAx t2_aAy r_aAB.
              t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy t1_aAx)
               ~
             (forall t_aAv t1_aAx t2_aAy r_aAB.
              t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy r_aAB))

Test.updateStringfKString [InlPrag=INLINE]
  :: forall t_aAH t1_aAJ r_aAM.
     t_aAH
     -> Test.K [GHC.Types.Char] t1_aAJ
     -> Test.K [GHC.Types.Char] r_aAM
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAH)
                 (@ t1_aAJ)
                 (@ r_aAM)
                 _
                 (ds_dSc [Occ=Once] :: Test.K [GHC.Types.Char] t1_aAJ) ->
                 case ds_dSc
                      `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAJ
                              :: Test.K [GHC.Types.Char] t1_aAJ ~ [GHC.Types.Char])
                 of _ {
                   [] ->
                     (GHC.Types.[] @ GHC.Types.Char)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM);
                   : ipv_sU3 [Occ=Once] ipv1_sU4 ->
                     (GHC.Types.:
                        @ GHC.Types.Char
                        (GHC.List.last_last' @ GHC.Types.Char ipv_sU3 ipv1_sU4)
                        ipv1_sU4)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM)
                 }}]
Test.updateStringfKString =
  \ (@ t_aAH)
    (@ t1_aAJ)
    (@ r_aAM)
    _
    (eta1_X7H :: Test.K [GHC.Types.Char] t1_aAJ) ->
    case eta1_X7H
         `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAJ
                 :: Test.K [GHC.Types.Char] t1_aAJ ~ [GHC.Types.Char])
    of _ {
      [] ->
        (GHC.Types.[] @ GHC.Types.Char)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM);
      : ipv_sTG ipv1_sTH ->
        (GHC.Types.:
           @ GHC.Types.Char
           (GHC.List.last_last' @ GHC.Types.Char ipv_sTG ipv1_sTH)
           ipv1_sTH)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM)
    }

Test.updateStringfU [InlPrag=INLINE]
  :: forall t_aBG a_aBL. t_aBG -> a_aBL -> a_aBL
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= \ (@ t_aBG) (@ a_aBL) _ -> GHC.Base.id @ a_aBL}]
Test.updateStringfU =
  \ (@ t_aBG) (@ a_aBL) _ (eta1_B1 :: a_aBL) -> eta1_B1

Test.logic :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic = Test.Impl Test.logic4 Test.logic1

Test.testLogic :: Test.Logic
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testLogic = Test.testLogic_updateString Test.logic

Test.$fRegularLogic [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Logic
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
                                   :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                                        ~
                                      (Test.Logic -> Test.PF Test.Logic Test.Logic))),
                          ((Test.toLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
                                   :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                                        ~
                                      (Test.PF Test.Logic Test.Logic -> Test.Logic)))]]
Test.$fRegularLogic =
  Test.D:Regular
    @ Test.Logic
    ((Test.fromLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
             :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                  ~
                (Test.Logic -> Test.PF Test.Logic Test.Logic)))
    ((Test.toLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
             :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                  ~
                (Test.PF Test.Logic Test.Logic -> Test.Logic)))

Test.$fUpdateString:*:
  :: forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
     (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
     Test.UpdateString (f_ak0 Test.:*: g_ak1)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateString:*:1
               `cast` (forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
                       (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
                       sym (Test.NTCo:T:UpdateString (f_ak0 Test.:*: g_ak1))
                       :: (forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
                           (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
                           forall a_aiO.
                           (a_aiO -> a_aiO)
                           -> (Test.:*:) f_ak0 g_ak1 a_aiO
                           -> (Test.:*:) f_ak0 g_ak1 a_aiO)
                            ~
                          (forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
                           (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
                           Test.T:UpdateString (f_ak0 Test.:*: g_ak1)))}]
Test.$fUpdateString:*: =
  Test.$fUpdateString:*:1
  `cast` (forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
          (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
          sym (Test.NTCo:T:UpdateString (f_ak0 Test.:*: g_ak1))
          :: (forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
              (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
              forall a_aiO.
              (a_aiO -> a_aiO)
              -> (Test.:*:) f_ak0 g_ak1 a_aiO
              -> (Test.:*:) f_ak0 g_ak1 a_aiO)
               ~
             (forall (f_ak0 :: * -> *) (g_ak1 :: * -> *).
              (Test.UpdateString f_ak0, Test.UpdateString g_ak1) =>
              Test.T:UpdateString (f_ak0 Test.:*: g_ak1)))

Test.$fUpdateString:+:
  :: forall (f_ak2 :: * -> *) (g_ak3 :: * -> *).
     (Test.UpdateString f_ak2, Test.UpdateString g_ak3) =>
     Test.UpdateString (f_ak2 Test.:+: g_ak3)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateString:+: =
  Test.$fUpdateString:+:1
  `cast` (forall (f_ak2 :: * -> *) (g_ak3 :: * -> *).
          (Test.UpdateString f_ak2, Test.UpdateString g_ak3) =>
          sym (Test.NTCo:T:UpdateString (f_ak2 Test.:+: g_ak3))
          :: (forall (f_ak2 :: * -> *) (g_ak3 :: * -> *).
              (Test.UpdateString f_ak2, Test.UpdateString g_ak3) =>
              forall a_aiO.
              (a_aiO -> a_aiO)
              -> (Test.:+:) f_ak2 g_ak3 a_aiO
              -> (Test.:+:) f_ak2 g_ak3 a_aiO)
               ~
             (forall (f_ak2 :: * -> *) (g_ak3 :: * -> *).
              (Test.UpdateString f_ak2, Test.UpdateString g_ak3) =>
              Test.T:UpdateString (f_ak2 Test.:+: g_ak3)))

Test.$fUpdateStringC
  :: forall (f_ajY :: * -> *) c_ajZ.
     (Test.UpdateString f_ajY) =>
     Test.UpdateString (Test.C c_ajZ f_ajY)
[GblId[DFunId(newtype)],
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateStringC1
               `cast` (forall (f_ajY :: * -> *) c_ajZ.
                       (Test.UpdateString f_ajY) =>
                       sym (Test.NTCo:T:UpdateString (Test.C c_ajZ f_ajY))
                       :: (forall (f_ajY :: * -> *) c_ajZ.
                           (Test.UpdateString f_ajY) =>
                           forall a_aiO.
                           (a_aiO -> a_aiO)
                           -> Test.C c_ajZ f_ajY a_aiO
                           -> Test.C c_ajZ f_ajY a_aiO)
                            ~
                          (forall (f_ajY :: * -> *) c_ajZ.
                           (Test.UpdateString f_ajY) =>
                           Test.T:UpdateString (Test.C c_ajZ f_ajY)))}]
Test.$fUpdateStringC =
  Test.$fUpdateStringC1
  `cast` (forall (f_ajY :: * -> *) c_ajZ.
          (Test.UpdateString f_ajY) =>
          sym (Test.NTCo:T:UpdateString (Test.C c_ajZ f_ajY))
          :: (forall (f_ajY :: * -> *) c_ajZ.
              (Test.UpdateString f_ajY) =>
              forall a_aiO.
              (a_aiO -> a_aiO)
              -> Test.C c_ajZ f_ajY a_aiO
              -> Test.C c_ajZ f_ajY a_aiO)
               ~
             (forall (f_ajY :: * -> *) c_ajZ.
              (Test.UpdateString f_ajY) =>
              Test.T:UpdateString (Test.C c_ajZ f_ajY)))

Test.$fUpdateStringI :: Test.UpdateString Test.I
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateStringI =
  (\ (@ a_aQe) -> Test.updateStringfI @ a_aQe @ a_aQe)
  `cast` (sym (Test.NTCo:T:UpdateString Test.I)
          :: (forall a_aiO. (a_aiO -> a_aiO) -> Test.I a_aiO -> Test.I a_aiO)
               ~
             Test.T:UpdateString Test.I)

Test.$fUpdateStringK
  :: forall x_ak4. Test.UpdateString (Test.K x_ak4)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ x_ak4) (@ a_aQ2) ->
                  Test.updateStringfK @ (a_aQ2 -> a_aQ2) @ a_aQ2 @ x_ak4 @ a_aQ2)
               `cast` (forall x_ak4. sym (Test.NTCo:T:UpdateString (Test.K x_ak4))
                       :: (forall x_ak4 a_aiO.
                           (a_aiO -> a_aiO) -> Test.K x_ak4 a_aiO -> Test.K x_ak4 a_aiO)
                            ~
                          (forall x_ak4. Test.T:UpdateString (Test.K x_ak4)))}]
Test.$fUpdateStringK =
  (\ (@ x_ak4) (@ a_aQ2) ->
     Test.updateStringfK @ (a_aQ2 -> a_aQ2) @ a_aQ2 @ x_ak4 @ a_aQ2)
  `cast` (forall x_ak4. sym (Test.NTCo:T:UpdateString (Test.K x_ak4))
          :: (forall x_ak4 a_aiO.
              (a_aiO -> a_aiO) -> Test.K x_ak4 a_aiO -> Test.K x_ak4 a_aiO)
               ~
             (forall x_ak4. Test.T:UpdateString (Test.K x_ak4)))

Test.$fUpdateStringK0 :: Test.UpdateString (Test.K GHC.Base.String)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQ8) ->
                  Test.updateStringfKString @ (a_aQ8 -> a_aQ8) @ a_aQ8 @ a_aQ8)
               `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
                       :: (forall a_aiO.
                           (a_aiO -> a_aiO)
                           -> Test.K GHC.Base.String a_aiO
                           -> Test.K GHC.Base.String a_aiO)
                            ~
                          Test.T:UpdateString (Test.K GHC.Base.String))}]
Test.$fUpdateStringK0 =
  (\ (@ a_aQ8) ->
     Test.updateStringfKString @ (a_aQ8 -> a_aQ8) @ a_aQ8 @ a_aQ8)
  `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
          :: (forall a_aiO.
              (a_aiO -> a_aiO)
              -> Test.K GHC.Base.String a_aiO
              -> Test.K GHC.Base.String a_aiO)
               ~
             Test.T:UpdateString (Test.K GHC.Base.String))

Test.$fUpdateStringU :: Test.UpdateString Test.U
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQk) ->
                  Test.updateStringfU @ (a_aQk -> a_aQk) @ (Test.U a_aQk))
               `cast` (sym (Test.NTCo:T:UpdateString Test.U)
                       :: (forall a_aiO. (a_aiO -> a_aiO) -> Test.U a_aiO -> Test.U a_aiO)
                            ~
                          Test.T:UpdateString Test.U)}]
Test.$fUpdateStringU =
  (\ (@ a_aQk) ->
     Test.updateStringfU @ (a_aQk -> a_aQk) @ (Test.U a_aQk))
  `cast` (sym (Test.NTCo:T:UpdateString Test.U)
          :: (forall a_aiO. (a_aiO -> a_aiO) -> Test.U a_aiO -> Test.U a_aiO)
               ~
             Test.T:UpdateString Test.U)

Test.$dmconFixity
  :: forall c_aiR.
     (Test.Constructor c_aiR) =>
     forall (t_aiV :: * -> (* -> *) -> * -> *) (f_aiW :: * -> *) r_aiX.
     t_aiV c_aiR f_aiW r_aiX -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aiR)
                 _
                 (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity =
  \ (@ c_aiR)
    _
    (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconIsRecord
  :: forall c_aiR.
     (Test.Constructor c_aiR) =>
     forall (t_aiY :: * -> (* -> *) -> * -> *) (f_aiZ :: * -> *) r_aj0.
     t_aiY c_aiR f_aiZ r_aj0 -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aiR)
                 _
                 (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord =
  \ (@ c_aiR)
    _
    (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.updateString [InlPrag=INLINE]
  :: forall a_aje.
     (Test.Regular a_aje, Test.UpdateString (Test.PF a_aje)) =>
     a_aje -> a_aje
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ a_ay9)
                 ($dRegular_ayB :: Test.Regular a_ay9)
                 ($dUpdateString_ayC :: Test.UpdateString (Test.PF a_ay9)) ->
                 letrec {
                   sub_aQR [Occ=OnceL!] :: Test.PF a_ay9 a_ay9 -> Test.PF a_ay9 a_ay9
                   [LclId]
                   sub_aQR =
                     ($dUpdateString_ayC
                      `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay9)
                              :: Test.T:UpdateString (Test.PF a_ay9)
                                   ~
                                 (forall a_aiO.
                                  (a_aiO -> a_aiO) -> Test.PF a_ay9 a_aiO -> Test.PF a_ay9 a_aiO)))
                       @ a_ay9 updateString3_aya;
                   updateString3_aya [Occ=LoopBreaker] :: a_ay9 -> a_ay9
                   [LclId, Arity=1]
                   updateString3_aya =
                     \ (x_aUp [Occ=Once] :: a_ay9) ->
                       Test.to
                         @ a_ay9
                         $dRegular_ayB
                         (sub_aQR (Test.from @ a_ay9 $dRegular_ayB x_aUp)); } in
                 updateString3_aya}]
Test.updateString =
  \ (@ a_ay9)
    ($dRegular_ayB :: Test.Regular a_ay9)
    ($dUpdateString_ayC :: Test.UpdateString (Test.PF a_ay9)) ->
    letrec {
      sub_s1g9 :: Test.PF a_ay9 a_ay9 -> Test.PF a_ay9 a_ay9
      [LclId, Str=DmdType]
      sub_s1g9 =
        ($dUpdateString_ayC
         `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay9)
                 :: Test.T:UpdateString (Test.PF a_ay9)
                      ~
                    (forall a_aiO.
                     (a_aiO -> a_aiO) -> Test.PF a_ay9 a_aiO -> Test.PF a_ay9 a_aiO)))
          @ a_ay9 updateString3_s1ga;
      updateString3_s1ga [Occ=LoopBreaker] :: a_ay9 -> a_ay9
      [LclId, Arity=1, Str=DmdType L]
      updateString3_s1ga =
        \ (x_aUp :: a_ay9) ->
          Test.to
            @ a_ay9
            $dRegular_ayB
            (sub_s1g9 (Test.from @ a_ay9 $dRegular_ayB x_aUp)); } in
    updateString3_s1ga

Test.$fConstructorLogic_Var__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Var_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aPl::* -> (* -> *) -> * -> *)
                 (@ f_aPm::* -> *)
                 (@ r_aPn)
                 _ ->
                 Test.$fConstructorLogic_Var_1}]
Test.$fConstructorLogic_Var__$cconName =
  \ (@ t_aPl::* -> (* -> *) -> * -> *)
    (@ f_aPm::* -> *)
    (@ r_aPn)
    _ ->
    Test.$fConstructorLogic_Var_1

Test.$fConstructorLogic_Var_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Var_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Var__$cconName,
                              Test.$dmconFixity_$s$dmconFixity,
                              Test.$dmconIsRecord_$s$dmconIsRecord]]
Test.$fConstructorLogic_Var_ =
  Test.D:Constructor
    @ Test.Logic_Var_
    Test.$fConstructorLogic_Var__$cconName
    Test.$dmconFixity_$s$dmconFixity
    Test.$dmconIsRecord_$s$dmconIsRecord

Test.$fConstructorLogic_Impl__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Impl_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aP9::* -> (* -> *) -> * -> *)
                 (@ f_aPa::* -> *)
                 (@ r_aPb)
                 _ ->
                 Test.$fConstructorLogic_Impl_1}]
Test.$fConstructorLogic_Impl__$cconName =
  \ (@ t_aP9::* -> (* -> *) -> * -> *)
    (@ f_aPa::* -> *)
    (@ r_aPb)
    _ ->
    Test.$fConstructorLogic_Impl_1

Test.$fConstructorLogic_Impl_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Impl_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Impl__$cconName,
                              Test.$dmconFixity_$s$dmconFixity1,
                              Test.$dmconIsRecord_$s$dmconIsRecord1]]
Test.$fConstructorLogic_Impl_ =
  Test.D:Constructor
    @ Test.Logic_Impl_
    Test.$fConstructorLogic_Impl__$cconName
    Test.$dmconFixity_$s$dmconFixity1
    Test.$dmconIsRecord_$s$dmconIsRecord1

Test.$fConstructorLogic_Equiv__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Equiv_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOX::* -> (* -> *) -> * -> *)
                 (@ f_aOY::* -> *)
                 (@ r_aOZ)
                 _ ->
                 Test.$fConstructorLogic_Equiv_1}]
Test.$fConstructorLogic_Equiv__$cconName =
  \ (@ t_aOX::* -> (* -> *) -> * -> *)
    (@ f_aOY::* -> *)
    (@ r_aOZ)
    _ ->
    Test.$fConstructorLogic_Equiv_1

Test.$fConstructorLogic_Equiv_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Equiv_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Equiv__$cconName,
                              Test.$dmconFixity_$s$dmconFixity2,
                              Test.$dmconIsRecord_$s$dmconIsRecord2]]
Test.$fConstructorLogic_Equiv_ =
  Test.D:Constructor
    @ Test.Logic_Equiv_
    Test.$fConstructorLogic_Equiv__$cconName
    Test.$dmconFixity_$s$dmconFixity2
    Test.$dmconIsRecord_$s$dmconIsRecord2

Test.$fConstructorLogic_Conj__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Conj_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOL::* -> (* -> *) -> * -> *)
                 (@ f_aOM::* -> *)
                 (@ r_aON)
                 _ ->
                 Test.$fConstructorLogic_Conj_1}]
Test.$fConstructorLogic_Conj__$cconName =
  \ (@ t_aOL::* -> (* -> *) -> * -> *)
    (@ f_aOM::* -> *)
    (@ r_aON)
    _ ->
    Test.$fConstructorLogic_Conj_1

Test.$fConstructorLogic_Conj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Conj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Conj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity3,
                              Test.$dmconIsRecord_$s$dmconIsRecord3]]
Test.$fConstructorLogic_Conj_ =
  Test.D:Constructor
    @ Test.Logic_Conj_
    Test.$fConstructorLogic_Conj__$cconName
    Test.$dmconFixity_$s$dmconFixity3
    Test.$dmconIsRecord_$s$dmconIsRecord3

Test.$fConstructorLogic_Disj__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Disj_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOz::* -> (* -> *) -> * -> *)
                 (@ f_aOA::* -> *)
                 (@ r_aOB)
                 _ ->
                 Test.$fConstructorLogic_Disj_1}]
Test.$fConstructorLogic_Disj__$cconName =
  \ (@ t_aOz::* -> (* -> *) -> * -> *)
    (@ f_aOA::* -> *)
    (@ r_aOB)
    _ ->
    Test.$fConstructorLogic_Disj_1

Test.$fConstructorLogic_Disj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Disj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Disj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity4,
                              Test.$dmconIsRecord_$s$dmconIsRecord4]]
Test.$fConstructorLogic_Disj_ =
  Test.D:Constructor
    @ Test.Logic_Disj_
    Test.$fConstructorLogic_Disj__$cconName
    Test.$dmconFixity_$s$dmconFixity4
    Test.$dmconIsRecord_$s$dmconIsRecord4

Test.$fConstructorLogic_Not__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Not_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOn::* -> (* -> *) -> * -> *)
                 (@ f_aOo::* -> *)
                 (@ r_aOp)
                 _ ->
                 Test.$fConstructorLogic_Not_1}]
Test.$fConstructorLogic_Not__$cconName =
  \ (@ t_aOn::* -> (* -> *) -> * -> *)
    (@ f_aOo::* -> *)
    (@ r_aOp)
    _ ->
    Test.$fConstructorLogic_Not_1

Test.$fConstructorLogic_Not_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Not_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Not__$cconName,
                              Test.$dmconFixity_$s$dmconFixity5,
                              Test.$dmconIsRecord_$s$dmconIsRecord5]]
Test.$fConstructorLogic_Not_ =
  Test.D:Constructor
    @ Test.Logic_Not_
    Test.$fConstructorLogic_Not__$cconName
    Test.$dmconFixity_$s$dmconFixity5
    Test.$dmconIsRecord_$s$dmconIsRecord5

Test.$fConstructorLogic_T__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_T_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOb::* -> (* -> *) -> * -> *)
                 (@ f_aOc::* -> *)
                 (@ r_aOd)
                 _ ->
                 Test.$fConstructorLogic_T_1}]
Test.$fConstructorLogic_T__$cconName =
  \ (@ t_aOb::* -> (* -> *) -> * -> *)
    (@ f_aOc::* -> *)
    (@ r_aOd)
    _ ->
    Test.$fConstructorLogic_T_1

Test.$fConstructorLogic_T_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_T_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_T__$cconName,
                              Test.$dmconFixity_$s$dmconFixity6,
                              Test.$dmconIsRecord_$s$dmconIsRecord6]]
Test.$fConstructorLogic_T_ =
  Test.D:Constructor
    @ Test.Logic_T_
    Test.$fConstructorLogic_T__$cconName
    Test.$dmconFixity_$s$dmconFixity6
    Test.$dmconIsRecord_$s$dmconIsRecord6

Test.$fConstructorLogic_F__$cconName
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_F_ f_aiT r_aiU -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aNZ::* -> (* -> *) -> * -> *)
                 (@ f_aO0::* -> *)
                 (@ r_aO1)
                 _ ->
                 Test.$fConstructorLogic_F_1}]
Test.$fConstructorLogic_F__$cconName =
  \ (@ t_aNZ::* -> (* -> *) -> * -> *)
    (@ f_aO0::* -> *)
    (@ r_aO1)
    _ ->
    Test.$fConstructorLogic_F_1

Test.$fConstructorLogic_F_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_F_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_F__$cconName,
                              Test.$dmconFixity_$s$dmconFixity7,
                              Test.$dmconIsRecord_$s$dmconIsRecord7]]
Test.$fConstructorLogic_F_ =
  Test.D:Constructor
    @ Test.Logic_F_
    Test.$fConstructorLogic_F__$cconName
    Test.$dmconFixity_$s$dmconFixity7
    Test.$dmconIsRecord_$s$dmconIsRecord7

Test.$fReadFixity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Fixity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run @ [Test.Fixity] Test.$fReadFixity4

Test.$fReadFixity_$creadsPrec
  :: GHC.Types.Int -> Text.ParserCombinators.ReadP.ReadS Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 5 6}]
Test.$fReadFixity_$creadsPrec =
  \ (eta_aXG :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Fixity
      (Test.$fReadFixity3
         eta_aXG
         @ Test.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ Test.Fixity))

Test.$fReadFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadFixity_$creadsPrec,
                           Test.$fReadFixity_readListDefault,
                           ((Test.$fReadFixity2
                             `cast` (Text.ParserCombinators.ReadPrec.Prec
                                     -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity)
                                     :: (Text.ParserCombinators.ReadPrec.Prec
                                         -> forall b_aWB.
                                            (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
                                            -> Text.ParserCombinators.ReadP.P b_aWB)
                                          ~
                                        (Text.ParserCombinators.ReadPrec.Prec
                                         -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)))
                            `cast` (right
                                      (inst
                                         (forall a_aXm.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXm
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXm))
                                         Test.Fixity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity)),
                           (Test.$fReadFixity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))]]
Test.$fReadFixity =
  GHC.Read.D:Read
    @ Test.Fixity
    Test.$fReadFixity_$creadsPrec
    Test.$fReadFixity_readListDefault
    (Test.$fReadFixity2
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_aWB.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
                    -> Text.ParserCombinators.ReadP.P b_aWB)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
    (Test.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))

Test.$fOrdFixity_$cmin :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 25 0}]
Test.$fOrdFixity_$cmin =
  \ (x_aYZ :: Test.Fixity) (y_aZ0 :: Test.Fixity) ->
    case x_aYZ of wild_X8I {
      Test.Prefix -> case y_aZ0 of _ { __DEFAULT -> Test.Prefix };
      Test.Infix ds_dSo ds1_dSp ->
        case y_aZ0 of wild1_X8K {
          Test.Prefix -> Test.Prefix;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> wild1_X8K; GHC.Bool.True -> wild_X8I
                        };
                      GHC.Bool.True -> wild_X8I
                    }
                    }
                    };
                  Test.RightAssociative -> wild_X8I;
                  Test.NotAssociative -> wild_X8I
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> wild1_X8K;
                  Test.RightAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> wild1_X8K; GHC.Bool.True -> wild_X8I
                        };
                      GHC.Bool.True -> wild_X8I
                    }
                    }
                    };
                  Test.NotAssociative -> wild_X8I
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> wild1_X8K;
                  Test.NotAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> wild1_X8K; GHC.Bool.True -> wild_X8I
                        };
                      GHC.Bool.True -> wild_X8I
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$cmax :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 2] 24 0}]
Test.$fOrdFixity_$cmax =
  \ (x_aZ8 :: Test.Fixity) (y_aZ9 :: Test.Fixity) ->
    case x_aZ8 of wild_X8I {
      Test.Prefix -> y_aZ9;
      Test.Infix ds_dSo ds1_dSp ->
        case y_aZ9 of wild1_X8K {
          Test.Prefix -> wild_X8I;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> wild_X8I; GHC.Bool.True -> wild1_X8K
                        };
                      GHC.Bool.True -> wild1_X8K
                    }
                    }
                    };
                  Test.RightAssociative -> wild1_X8K;
                  Test.NotAssociative -> wild1_X8K
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> wild_X8I;
                  Test.RightAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> wild_X8I; GHC.Bool.True -> wild1_X8K
                        };
                      GHC.Bool.True -> wild1_X8K
                    }
                    }
                    };
                  Test.NotAssociative -> wild1_X8K
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> wild_X8I;
                  Test.NotAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> wild_X8I; GHC.Bool.True -> wild1_X8K
                        };
                      GHC.Bool.True -> wild1_X8K
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c<=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 22 8}]
Test.$fOrdFixity_$c<= =
  \ (x_aZh :: Test.Fixity) (y_aZi :: Test.Fixity) ->
    case x_aZh of _ {
      Test.Prefix -> case y_aZi of _ { __DEFAULT -> GHC.Bool.True };
      Test.Infix ds_dSo ds1_dSp ->
        case y_aZi of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False -> GHC.Prim.==# x#_aWU y#_aWY;
                      GHC.Bool.True -> GHC.Bool.True
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.True;
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False -> GHC.Prim.==# x#_aWU y#_aWY;
                      GHC.Bool.True -> GHC.Bool.True
                    }
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False -> GHC.Prim.==# x#_aWU y#_aWY;
                      GHC.Bool.True -> GHC.Bool.True
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c> :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 25 14}]
Test.$fOrdFixity_$c> =
  \ (x_aZo :: Test.Fixity) (y_aZp :: Test.Fixity) ->
    case x_aZo of _ {
      Test.Prefix -> case y_aZp of _ { __DEFAULT -> GHC.Bool.False };
      Test.Infix ds_dSo ds1_dSp ->
        case y_aZp of _ {
          Test.Prefix -> GHC.Bool.True;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                        };
                      GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> GHC.Bool.True;
                  Test.RightAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                        };
                      GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.NotAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWU y#_aWY of _ {
                          GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                        };
                      GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c>=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 19 11}]
Test.$fOrdFixity_$c>= =
  \ (x_aZv :: Test.Fixity) (y_aZw :: Test.Fixity) ->
    case x_aZv of _ {
      Test.Prefix ->
        case y_aZw of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix ds_dSo ds1_dSp ->
        case y_aZw of _ {
          Test.Prefix -> GHC.Bool.True;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> GHC.Bool.True;
                  Test.RightAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.NotAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    case GHC.Prim.<# x#_aWU y#_aWY of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c< :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 5}]
Test.$fOrdFixity_$c< =
  \ (x_aZC :: Test.Fixity) (y_aZD :: Test.Fixity) ->
    case x_aZC of _ {
      Test.Prefix ->
        case y_aZD of _ {
          Test.Prefix -> GHC.Bool.False; Test.Infix _ _ -> GHC.Bool.True
        };
      Test.Infix ds_dSo ds1_dSp ->
        case y_aZD of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    GHC.Prim.<# x#_aWU y#_aWY
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.True;
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    GHC.Prim.<# x#_aWU y#_aWY
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case ds1_dSp of _ { GHC.Types.I# x#_aWU ->
                    case ds3_XXC of _ { GHC.Types.I# y#_aWY ->
                    GHC.Prim.<# x#_aWU y#_aWY
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$ccompare
  :: Test.Fixity -> Test.Fixity -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 5}]
Test.$fOrdFixity_$ccompare =
  \ (a2_atT :: Test.Fixity) (b_atU :: Test.Fixity) ->
    case a2_atT of _ {
      Test.Prefix ->
        case b_atU of _ {
          Test.Prefix -> GHC.Ordering.EQ; Test.Infix _ _ -> GHC.Ordering.LT
        };
      Test.Infix ds_dSo ds1_dSp ->
        case b_atU of _ {
          Test.Prefix -> GHC.Ordering.GT;
          Test.Infix ds2_XXA ds3_XXC ->
            case ds_dSo of _ {
              Test.LeftAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> GHC.Base.compareInt ds1_dSp ds3_XXC;
                  Test.RightAssociative -> GHC.Ordering.LT;
                  Test.NotAssociative -> GHC.Ordering.LT
                };
              Test.RightAssociative ->
                case ds2_XXA of _ {
                  Test.LeftAssociative -> GHC.Ordering.GT;
                  Test.RightAssociative -> GHC.Base.compareInt ds1_dSp ds3_XXC;
                  Test.NotAssociative -> GHC.Ordering.LT
                };
              Test.NotAssociative ->
                case ds2_XXA of _ {
                  __DEFAULT -> GHC.Ordering.GT;
                  Test.NotAssociative -> GHC.Base.compareInt ds1_dSp ds3_XXC
                }
            }
        }
    }

Test.$fShowFixity_$cshowsPrec
  :: GHC.Types.Int -> Test.Fixity -> GHC.Show.ShowS
[GblId, Arity=3, Str=DmdType LSL]
Test.$fShowFixity_$cshowsPrec =
  \ (ds_dSQ :: GHC.Types.Int)
    (ds1_dSR :: Test.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_dSR of _ {
      Test.Prefix -> GHC.Base.++ @ GHC.Types.Char lvl1_r1hn eta_B1;
      Test.Infix b1_atR b2_atS ->
        case ds_dSQ of _ { GHC.Types.I# x_aX5 ->
        let {
          p_s1gc :: GHC.Show.ShowS
          [LclId, Arity=1, Str=DmdType L]
          p_s1gc =
            \ (x1_aUp :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                lvl_r1hl
                (case b1_atR of _ {
                   Test.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a19w ->
                           case GHC.Prim.<# ww_a19w 0 of _ {
                             GHC.Bool.False -> GHC.Show.$dmshow_itos' ww_a19w x1_aUp;
                             GHC.Bool.True ->
                               GHC.Types.:
                                 @ GHC.Types.Char
                                 GHC.Show.$dmshow6
                                 (case ww_a19w of wild11_a19T {
                                    __DEFAULT ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           (GHC.Prim.negateInt# wild11_a19T)
                                           (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUp));
                                    (-2147483648) ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           214748364
                                           (GHC.Show.$dmshow_itos'
                                              8
                                              (GHC.Types.:
                                                 @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUp)))
                                  })
                           }
                           }));
                   Test.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a19w ->
                           case GHC.Prim.<# ww_a19w 0 of _ {
                             GHC.Bool.False -> GHC.Show.$dmshow_itos' ww_a19w x1_aUp;
                             GHC.Bool.True ->
                               GHC.Types.:
                                 @ GHC.Types.Char
                                 GHC.Show.$dmshow6
                                 (case ww_a19w of wild11_a19T {
                                    __DEFAULT ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           (GHC.Prim.negateInt# wild11_a19T)
                                           (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUp));
                                    (-2147483648) ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           214748364
                                           (GHC.Show.$dmshow_itos'
                                              8
                                              (GHC.Types.:
                                                 @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUp)))
                                  })
                           }
                           }));
                   Test.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a19w ->
                           case GHC.Prim.<# ww_a19w 0 of _ {
                             GHC.Bool.False -> GHC.Show.$dmshow_itos' ww_a19w x1_aUp;
                             GHC.Bool.True ->
                               GHC.Types.:
                                 @ GHC.Types.Char
                                 GHC.Show.$dmshow6
                                 (case ww_a19w of wild11_a19T {
                                    __DEFAULT ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           (GHC.Prim.negateInt# wild11_a19T)
                                           (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUp));
                                    (-2147483648) ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           214748364
                                           (GHC.Show.$dmshow_itos'
                                              8
                                              (GHC.Types.:
                                                 @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUp)))
                                  })
                           }
                           }))
                 }) } in
        case GHC.Prim.>=# x_aX5 11 of _ {
          GHC.Bool.False -> p_s1gc eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.$dmshow6
              (p_s1gc (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 eta_B1))
        }
        }
    }

Test.$fShowFixity_$cshow :: Test.Fixity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 4 0}]
Test.$fShowFixity_$cshow =
  \ (x_aYF :: Test.Fixity) ->
    Test.$fShowFixity_$cshowsPrec
      GHC.Base.zeroInt x_aYF (GHC.Types.[] @ GHC.Types.Char)

Test.$fShowFixity_$cshowList :: [Test.Fixity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 25 3}]
Test.$fShowFixity_$cshowList =
  \ (ds1_aYf :: [Test.Fixity]) (s_aYg :: GHC.Base.String) ->
    case ds1_aYf of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYg;
      : x_aYl xs_aYm ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (Test.$fShowFixity_$cshowsPrec
             Test.$fShowFixity1
             x_aYl
             (let {
                lvl141_s1ge :: [GHC.Types.Char]
                [LclId, Str=DmdType]
                lvl141_s1ge =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYg } in
              letrec {
                showl_s1gg [Occ=LoopBreaker] :: [Test.Fixity] -> [GHC.Types.Char]
                [LclId, Arity=1, Str=DmdType S]
                showl_s1gg =
                  \ (ds2_aYq :: [Test.Fixity]) ->
                    case ds2_aYq of _ {
                      [] -> lvl141_s1ge;
                      : y_aYv ys_aYw ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (Test.$fShowFixity_$cshowsPrec
                             Test.$fShowFixity1 y_aYv (showl_s1gg ys_aYw))
                    }; } in
              showl_s1gg xs_aYm))
    }

Test.$fShowFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowFixity_$cshowsPrec,
                           Test.$fShowFixity_$cshow, Test.$fShowFixity_$cshowList]]
Test.$fShowFixity =
  GHC.Show.D:Show
    @ Test.Fixity
    Test.$fShowFixity_$cshowsPrec
    Test.$fShowFixity_$cshow
    Test.$fShowFixity_$cshowList

Test.$fEqFixity_$c/= :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 19 10}]
Test.$fEqFixity_$c/= =
  \ (a2_atO :: Test.Fixity) (b_atP :: Test.Fixity) ->
    case a2_atO of _ {
      Test.Prefix ->
        case b_atP of _ {
          Test.Prefix -> GHC.Bool.False; Test.Infix _ _ -> GHC.Bool.True
        };
      Test.Infix a12_atG a21_atH ->
        case b_atP of _ {
          Test.Prefix -> GHC.Bool.True;
          Test.Infix b1_atI b2_atJ ->
            case a12_atG of _ {
              Test.LeftAssociative ->
                case b1_atI of _ {
                  Test.LeftAssociative ->
                    case a21_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj ->
                    case GHC.Prim.==# x_aXf y_aXj of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.True;
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.RightAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.RightAssociative ->
                    case a21_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj ->
                    case GHC.Prim.==# x_aXf y_aXj of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                };
              Test.NotAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.NotAssociative ->
                    case a21_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj ->
                    case GHC.Prim.==# x_aXf y_aXj of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fEqFixity_$c== :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 4}]
Test.$fEqFixity_$c== =
  \ (ds_dSK :: Test.Fixity) (ds1_dSL :: Test.Fixity) ->
    case ds_dSK of _ {
      Test.Prefix ->
        case ds1_dSL of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix a12_atG a2_atH ->
        case ds1_dSL of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix b1_atI b2_atJ ->
            case a12_atG of _ {
              Test.LeftAssociative ->
                case b1_atI of _ {
                  Test.LeftAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj -> GHC.Prim.==# x_aXf y_aXj }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj -> GHC.Prim.==# x_aXf y_aXj }
                    }
                };
              Test.NotAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj -> GHC.Prim.==# x_aXf y_aXj }
                    }
                }
            }
        }
    }

Test.$fEqFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqFixity_$c==,
                            Test.$fEqFixity_$c/=]]
Test.$fEqFixity =
  GHC.Classes.D:Eq
    @ Test.Fixity Test.$fEqFixity_$c== Test.$fEqFixity_$c/=

Test.$fOrdFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqFixity,
                             Test.$fOrdFixity_$ccompare, Test.$fOrdFixity_$c<,
                             Test.$fOrdFixity_$c>=, Test.$fOrdFixity_$c>, Test.$fOrdFixity_$c<=,
                             Test.$fOrdFixity_$cmax, Test.$fOrdFixity_$cmin]]
Test.$fOrdFixity =
  GHC.Classes.D:Ord
    @ Test.Fixity
    Test.$fEqFixity
    Test.$fOrdFixity_$ccompare
    Test.$fOrdFixity_$c<
    Test.$fOrdFixity_$c>=
    Test.$fOrdFixity_$c>
    Test.$fOrdFixity_$c<=
    Test.$fOrdFixity_$cmax
    Test.$fOrdFixity_$cmin

Test.$fReadAssociativity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Associativity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [Test.Associativity] Test.$fReadAssociativity16

lvl23_r1id
  :: forall b_a15I.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15I
[GblId, Arity=1, Caf=NoCafRefs]
lvl23_r1id =
  \ (@ b_a15I) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15I
    }

lvl24_r1if :: forall b_a15I. Text.ParserCombinators.ReadP.P b_a15I
[GblId, Caf=NoCafRefs]
lvl24_r1if =
  \ (@ b_a15I) ->
    Text.ParserCombinators.ReadP.Look @ b_a15I (lvl23_r1id @ b_a15I)

lvl25_r1ih
  :: forall b_a15I.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15I
[GblId, Arity=1, Caf=NoCafRefs]
lvl25_r1ih =
  \ (@ b_a15I) (a111_a16V :: GHC.Base.String) ->
    case a111_a16V of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15I
    }

lvl26_r1ij :: forall b_a15I. Text.ParserCombinators.ReadP.P b_a15I
[GblId, Caf=NoCafRefs]
lvl26_r1ij =
  \ (@ b_a15I) ->
    Text.ParserCombinators.ReadP.Look @ b_a15I (lvl25_r1ih @ b_a15I)

Rec {
Test.$fReadAssociativity_a1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId, Arity=1, Str=DmdType L]
Test.$fReadAssociativity_a1 =
  \ (n_a15G :: Text.ParserCombinators.ReadPrec.Prec) ->
    let {
      ds2_s1gk [Dmd=Just L]
        :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
      [LclId, Str=DmdType]
      ds2_s1gk =
        (Test.$fReadAssociativity3
         `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                   Test.Associativity
                 :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
                      ~
                    (Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)))
          n_a15G } in
    (\ (@ b_a15I)
       (k1_a15J
          :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a15I) ->
       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
         @ b_a15I
         ((ds2_s1gk
           `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                   :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                        ~
                      (forall b_aWB.
                       (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                       -> Text.ParserCombinators.ReadP.P b_aWB)))
            @ b_a15I k1_a15J)
         (Text.ParserCombinators.ReadP.Look
            @ b_a15I
            (let {
               $wk_s1gy [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15I
               [LclId, Str=DmdType]
               $wk_s1gy =
                 Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                   @ b_a15I
                   (lvl24_r1if @ b_a15I)
                   (Text.Read.Lex.hsLex2
                      @ b_a15I
                      (let {
                         lvl81_s1gu :: Text.ParserCombinators.ReadP.P b_a15I
                         [LclId, Str=DmdType]
                         lvl81_s1gu =
                           ((Test.$fReadAssociativity_a1
                               Text.ParserCombinators.ReadPrec.minPrec)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                                    :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                         ~
                                       (forall b_aWB.
                                        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                                        -> Text.ParserCombinators.ReadP.P b_aWB)))
                             @ b_a15I
                             (\ (a12_a163 :: Test.Associativity) ->
                                Text.ParserCombinators.ReadP.Look
                                  @ b_a15I
                                  (let {
                                     $wk1_s1gq [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15I
                                     [LclId, Str=DmdType]
                                     $wk1_s1gq =
                                       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                                         @ b_a15I
                                         (lvl26_r1ij @ b_a15I)
                                         (Text.Read.Lex.hsLex2
                                            @ b_a15I
                                            (let {
                                               lvl91_s1gm :: Text.ParserCombinators.ReadP.P b_a15I
                                               [LclId, Str=DmdType]
                                               lvl91_s1gm = k1_a15J a12_a163 } in
                                             \ (a111_a165 :: Text.Read.Lex.Lexeme) ->
                                               case a111_a165 of _ {
                                                 __DEFAULT ->
                                                   Text.ParserCombinators.ReadP.Fail @ b_a15I;
                                                 Text.Read.Lex.Punc ds_a168 ->
                                                   case ds_a168 of _ {
                                                     [] ->
                                                       Text.ParserCombinators.ReadP.Fail @ b_a15I;
                                                     : ds1_a16d ds21_a16e ->
                                                       case ds1_a16d of _ { GHC.Types.C# ds3_a16i ->
                                                       case ds3_a16i of _ {
                                                         __DEFAULT ->
                                                           Text.ParserCombinators.ReadP.Fail
                                                             @ b_a15I;
                                                         ')' ->
                                                           case ds21_a16e of _ {
                                                             [] -> lvl91_s1gm;
                                                             : _ _ ->
                                                               Text.ParserCombinators.ReadP.Fail
                                                                 @ b_a15I
                                                           }
                                                       }
                                                       }
                                                   }
                                               })) } in
                                   let {
                                     k_s1gs :: () -> Text.ParserCombinators.ReadP.P b_a15I
                                     [LclId, Arity=1, Str=DmdType A]
                                     k_s1gs = \ _ -> $wk1_s1gq } in
                                   \ (a111_a17e :: GHC.Base.String) ->
                                     ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                                              :: Text.ParserCombinators.ReadP.ReadP ()
                                                   ~
                                                 (forall b_aWB.
                                                  (() -> Text.ParserCombinators.ReadP.P b_aWB)
                                                  -> Text.ParserCombinators.ReadP.P b_aWB)))
                                       @ b_a15I k_s1gs)) } in
                       \ (a12_a16q :: Text.Read.Lex.Lexeme) ->
                         case a12_a16q of _ {
                           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15I;
                           Text.Read.Lex.Punc ds_a16t ->
                             case ds_a16t of _ {
                               [] -> Text.ParserCombinators.ReadP.Fail @ b_a15I;
                               : ds1_a16y ds21_a16z ->
                                 case ds1_a16y of _ { GHC.Types.C# ds3_a16D ->
                                 case ds3_a16D of _ {
                                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15I;
                                   '(' ->
                                     case ds21_a16z of _ {
                                       [] -> lvl81_s1gu;
                                       : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a15I
                                     }
                                 }
                                 }
                             }
                         })) } in
             let {
               k_s1gA :: () -> Text.ParserCombinators.ReadP.P b_a15I
               [LclId, Arity=1, Str=DmdType A]
               k_s1gA = \ _ -> $wk_s1gy } in
             \ (a111_a17e :: GHC.Base.String) ->
               ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17e)
                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                        :: Text.ParserCombinators.ReadP.ReadP ()
                             ~
                           (forall b_aWB.
                            (() -> Text.ParserCombinators.ReadP.P b_aWB)
                            -> Text.ParserCombinators.ReadP.P b_aWB)))
                 @ b_a15I k_s1gA)))
    `cast` (sym
              (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity)
            :: (forall b_aWB.
                (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                -> Text.ParserCombinators.ReadP.P b_aWB)
                 ~
               Text.ParserCombinators.ReadP.ReadP Test.Associativity)
end Rec }

Test.$fReadAssociativity_$creadsPrec
  :: GHC.Types.Int
     -> Text.ParserCombinators.ReadP.ReadS Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 5 6}]
Test.$fReadAssociativity_$creadsPrec =
  \ (eta_aXG :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Associativity
      (((Test.$fReadAssociativity_a1 eta_aXG)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                     ~
                   (forall b_aWB.
                    (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                    -> Text.ParserCombinators.ReadP.P b_aWB)))
         @ Test.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn
            @ Test.Associativity))

Test.$fReadAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadAssociativity_$creadsPrec,
                           Test.$fReadAssociativity_readListDefault,
                           (Test.$fReadAssociativity2
                            `cast` (right
                                      (inst
                                         (forall a_aXm.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXm
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXm))
                                         Test.Associativity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         Test.Associativity)),
                           (Test.$fReadAssociativity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                         [Test.Associativity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         [Test.Associativity]))]]
Test.$fReadAssociativity =
  GHC.Read.D:Read
    @ Test.Associativity
    Test.$fReadAssociativity_$creadsPrec
    Test.$fReadAssociativity_readListDefault
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))
    (Test.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [Test.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Associativity]))

Test.$fOrdAssociativity_$cmin
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmin =
  \ (x_aYZ :: Test.Associativity) (y_aZ0 :: Test.Associativity) ->
    case x_aYZ of _ {
      Test.LeftAssociative ->
        case y_aZ0 of _ { __DEFAULT -> Test.LeftAssociative };
      Test.RightAssociative ->
        case y_aZ0 of _ {
          Test.LeftAssociative -> Test.LeftAssociative;
          Test.RightAssociative -> Test.RightAssociative;
          Test.NotAssociative -> Test.RightAssociative
        };
      Test.NotAssociative -> y_aZ0
    }

Test.$fOrdAssociativity_$cmax
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmax =
  \ (x_aZ8 :: Test.Associativity) (y_aZ9 :: Test.Associativity) ->
    case x_aZ8 of _ {
      Test.LeftAssociative -> y_aZ9;
      Test.RightAssociative ->
        case y_aZ9 of _ {
          __DEFAULT -> Test.RightAssociative;
          Test.NotAssociative -> Test.NotAssociative
        };
      Test.NotAssociative ->
        case y_aZ9 of _ { __DEFAULT -> Test.NotAssociative }
    }

Test.$fOrdAssociativity_$c<=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c<= =
  \ (x_aZh :: Test.Associativity) (y_aZi :: Test.Associativity) ->
    case x_aZh of _ {
      Test.LeftAssociative ->
        case y_aZi of _ { __DEFAULT -> GHC.Bool.True };
      Test.RightAssociative ->
        case y_aZi of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZi of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fOrdAssociativity_$c>
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c> =
  \ (x_aZo :: Test.Associativity) (y_aZp :: Test.Associativity) ->
    case x_aZo of _ {
      Test.LeftAssociative ->
        case y_aZp of _ { __DEFAULT -> GHC.Bool.False };
      Test.RightAssociative ->
        case y_aZp of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZp of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fOrdAssociativity_$c>=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c>= =
  \ (x_aZv :: Test.Associativity) (y_aZw :: Test.Associativity) ->
    case x_aZv of _ {
      Test.LeftAssociative ->
        case y_aZw of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case y_aZw of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZw of _ { __DEFAULT -> GHC.Bool.True }
    }

Test.$fOrdAssociativity_$c<
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c< =
  \ (x_aZC :: Test.Associativity) (y_aZD :: Test.Associativity) ->
    case x_aZC of _ {
      Test.LeftAssociative ->
        case y_aZD of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case y_aZD of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZD of _ { __DEFAULT -> GHC.Bool.False }
    }

Test.$fOrdAssociativity_$ccompare
  :: Test.Associativity
     -> Test.Associativity
     -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$ccompare =
  \ (a2_atB :: Test.Associativity) (b_atC :: Test.Associativity) ->
    case a2_atB of _ {
      Test.LeftAssociative ->
        case b_atC of _ {
          Test.LeftAssociative -> GHC.Ordering.EQ;
          Test.RightAssociative -> GHC.Ordering.LT;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.RightAssociative ->
        case b_atC of _ {
          Test.LeftAssociative -> GHC.Ordering.GT;
          Test.RightAssociative -> GHC.Ordering.EQ;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.NotAssociative ->
        case b_atC of _ {
          __DEFAULT -> GHC.Ordering.GT;
          Test.NotAssociative -> GHC.Ordering.EQ
        }
    }

Test.$fShowAssociativity_$cshowList
  :: [Test.Associativity] -> GHC.Show.ShowS
[GblId, Arity=2, Str=DmdType SL]
Test.$fShowAssociativity_$cshowList =
  \ (ds1_aYf :: [Test.Associativity]) (s_aYg :: GHC.Base.String) ->
    case ds1_aYf of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYg;
      : x_aYl xs_aYm ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_aYl of _ {
             Test.LeftAssociative ->
               GHC.Base.++
                 @ GHC.Types.Char
                 Test.$fReadAssociativity15
                 (let {
                    lvl141_s1gC :: [GHC.Types.Char]
                    [LclId, Str=DmdType]
                    lvl141_s1gC =
                      GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYg } in
                  letrec {
                    showl_s1gE [Occ=LoopBreaker]
                      :: [Test.Associativity] -> [GHC.Types.Char]
                    [LclId, Arity=1, Str=DmdType S]
                    showl_s1gE =
                      \ (ds2_aYq :: [Test.Associativity]) ->
                        case ds2_aYq of _ {
                          [] -> lvl141_s1gC;
                          : y_aYv ys_aYw ->
                            GHC.Types.:
                              @ GHC.Types.Char
                              GHC.Show.showList__1
                              (case y_aYv of _ {
                                 Test.LeftAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity15
                                     (showl_s1gE ys_aYw);
                                 Test.RightAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity12
                                     (showl_s1gE ys_aYw);
                                 Test.NotAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1gE ys_aYw)
                               })
                        }; } in
                  showl_s1gE xs_aYm);
             Test.RightAssociative ->
               GHC.Base.++
                 @ GHC.Types.Char
                 Test.$fReadAssociativity12
                 (let {
                    lvl141_s1gG :: [GHC.Types.Char]
                    [LclId, Str=DmdType]
                    lvl141_s1gG =
                      GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYg } in
                  letrec {
                    showl_s1gI [Occ=LoopBreaker]
                      :: [Test.Associativity] -> [GHC.Types.Char]
                    [LclId, Arity=1, Str=DmdType S]
                    showl_s1gI =
                      \ (ds2_aYq :: [Test.Associativity]) ->
                        case ds2_aYq of _ {
                          [] -> lvl141_s1gG;
                          : y_aYv ys_aYw ->
                            GHC.Types.:
                              @ GHC.Types.Char
                              GHC.Show.showList__1
                              (case y_aYv of _ {
                                 Test.LeftAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity15
                                     (showl_s1gI ys_aYw);
                                 Test.RightAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity12
                                     (showl_s1gI ys_aYw);
                                 Test.NotAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1gI ys_aYw)
                               })
                        }; } in
                  showl_s1gI xs_aYm);
             Test.NotAssociative ->
               GHC.Base.++
                 @ GHC.Types.Char
                 Test.$fReadAssociativity9
                 (let {
                    lvl141_s1gK :: [GHC.Types.Char]
                    [LclId, Str=DmdType]
                    lvl141_s1gK =
                      GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYg } in
                  letrec {
                    showl_s1gM [Occ=LoopBreaker]
                      :: [Test.Associativity] -> [GHC.Types.Char]
                    [LclId, Arity=1, Str=DmdType S]
                    showl_s1gM =
                      \ (ds2_aYq :: [Test.Associativity]) ->
                        case ds2_aYq of _ {
                          [] -> lvl141_s1gK;
                          : y_aYv ys_aYw ->
                            GHC.Types.:
                              @ GHC.Types.Char
                              GHC.Show.showList__1
                              (case y_aYv of _ {
                                 Test.LeftAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity15
                                     (showl_s1gM ys_aYw);
                                 Test.RightAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity12
                                     (showl_s1gM ys_aYw);
                                 Test.NotAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1gM ys_aYw)
                               })
                        }; } in
                  showl_s1gM xs_aYm)
           })
    }

Test.$fShowAssociativity_$cshow
  :: Test.Associativity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0}]
Test.$fShowAssociativity_$cshow =
  \ (x_aYF :: Test.Associativity) ->
    case x_aYF of _ {
      Test.LeftAssociative -> Test.$fReadAssociativity15;
      Test.RightAssociative -> Test.$fReadAssociativity12;
      Test.NotAssociative -> Test.$fReadAssociativity9
    }

Test.$fShowAssociativity_$cshowsPrec
  :: GHC.Types.Int -> Test.Associativity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType ASL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, -)
         Tmpl= \ _
                 (ds1_dSA [Occ=Once!] :: Test.Associativity)
                 (eta_B1 [Occ=Once*] :: GHC.Base.String) ->
                 case ds1_dSA of _ {
                   Test.LeftAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a18A)
                          (c_a18B [Occ=Once] :: GHC.Types.Char -> b_a18A -> b_a18A)
                          (n_a18C [Occ=Once] :: b_a18A) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a18A c_a18B n_a18C Test.$fReadAssociativity15)
                       eta_B1;
                   Test.RightAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a18A)
                          (c_a18B [Occ=Once] :: GHC.Types.Char -> b_a18A -> b_a18A)
                          (n_a18C [Occ=Once] :: b_a18A) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a18A c_a18B n_a18C Test.$fReadAssociativity12)
                       eta_B1;
                   Test.NotAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a18A)
                          (c_a18B [Occ=Once] :: GHC.Types.Char -> b_a18A -> b_a18A)
                          (n_a18C [Occ=Once] :: b_a18A) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a18A c_a18B n_a18C Test.$fReadAssociativity9)
                       eta_B1
                 }}]
Test.$fShowAssociativity_$cshowsPrec =
  \ _ (ds1_dSA :: Test.Associativity) (eta_B1 :: GHC.Base.String) ->
    case ds1_dSA of _ {
      Test.LeftAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_B1;
      Test.RightAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_B1;
      Test.NotAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_B1
    }

Test.$fShowAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowAssociativity_$cshowsPrec,
                           Test.$fShowAssociativity_$cshow,
                           Test.$fShowAssociativity_$cshowList]]
Test.$fShowAssociativity =
  GHC.Show.D:Show
    @ Test.Associativity
    Test.$fShowAssociativity_$cshowsPrec
    Test.$fShowAssociativity_$cshow
    Test.$fShowAssociativity_$cshowList

Test.$fEqAssociativity_$c/=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c/= =
  \ (a2_atz :: Test.Associativity) (b_atA :: Test.Associativity) ->
    case a2_atz of _ {
      Test.LeftAssociative ->
        case b_atA of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case b_atA of _ {
          __DEFAULT -> GHC.Bool.True; Test.RightAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case b_atA of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fEqAssociativity_$c==
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c== =
  \ (a2_atv :: Test.Associativity) (b_atw :: Test.Associativity) ->
    case a2_atv of _ {
      Test.LeftAssociative ->
        case b_atw of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case b_atw of _ {
          __DEFAULT -> GHC.Bool.False; Test.RightAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case b_atw of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fEqAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqAssociativity_$c==,
                            Test.$fEqAssociativity_$c/=]]
Test.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ Test.Associativity
    Test.$fEqAssociativity_$c==
    Test.$fEqAssociativity_$c/=

Test.$fOrdAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqAssociativity,
                             Test.$fOrdAssociativity_$ccompare, Test.$fOrdAssociativity_$c<,
                             Test.$fOrdAssociativity_$c>=, Test.$fOrdAssociativity_$c>,
                             Test.$fOrdAssociativity_$c<=, Test.$fOrdAssociativity_$cmax,
                             Test.$fOrdAssociativity_$cmin]]
Test.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ Test.Associativity
    Test.$fEqAssociativity
    Test.$fOrdAssociativity_$ccompare
    Test.$fOrdAssociativity_$c<
    Test.$fOrdAssociativity_$c>=
    Test.$fOrdAssociativity_$c>
    Test.$fOrdAssociativity_$c<=
    Test.$fOrdAssociativity_$cmax
    Test.$fOrdAssociativity_$cmin


------ Local rules for imported ids --------
"SPEC Test.$dmconFixity [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10F [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconFixity @ Test.Logic_F_ $dConstructor_s10F
      = Test.$dmconFixity_$s$dmconFixity7
"SPEC Test.$dmconFixity [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10I [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconFixity @ Test.Logic_T_ $dConstructor_s10I
      = Test.$dmconFixity_$s$dmconFixity6
"SPEC Test.$dmconFixity [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10L [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconFixity @ Test.Logic_Not_ $dConstructor_s10L
      = Test.$dmconFixity_$s$dmconFixity5
"SPEC Test.$dmconFixity [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10O [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconFixity @ Test.Logic_Disj_ $dConstructor_s10O
      = Test.$dmconFixity_$s$dmconFixity4
"SPEC Test.$dmconFixity [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10R [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconFixity @ Test.Logic_Conj_ $dConstructor_s10R
      = Test.$dmconFixity_$s$dmconFixity3
"SPEC Test.$dmconFixity [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s10U [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconFixity @ Test.Logic_Equiv_ $dConstructor_s10U
      = Test.$dmconFixity_$s$dmconFixity2
"SPEC Test.$dmconFixity [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s10X [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconFixity @ Test.Logic_Impl_ $dConstructor_s10X
      = Test.$dmconFixity_$s$dmconFixity1
"SPEC Test.$dmconFixity [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s110 [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconFixity @ Test.Logic_Var_ $dConstructor_s110
      = Test.$dmconFixity_$s$dmconFixity
"SPEC Test.$dmconIsRecord [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10h [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconIsRecord @ Test.Logic_F_ $dConstructor_s10h
      = Test.$dmconIsRecord_$s$dmconIsRecord7
"SPEC Test.$dmconIsRecord [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10k [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconIsRecord @ Test.Logic_T_ $dConstructor_s10k
      = Test.$dmconIsRecord_$s$dmconIsRecord6
"SPEC Test.$dmconIsRecord [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10n [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconIsRecord @ Test.Logic_Not_ $dConstructor_s10n
      = Test.$dmconIsRecord_$s$dmconIsRecord5
"SPEC Test.$dmconIsRecord [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10q [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconIsRecord @ Test.Logic_Disj_ $dConstructor_s10q
      = Test.$dmconIsRecord_$s$dmconIsRecord4
"SPEC Test.$dmconIsRecord [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10t [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconIsRecord @ Test.Logic_Conj_ $dConstructor_s10t
      = Test.$dmconIsRecord_$s$dmconIsRecord3
"SPEC Test.$dmconIsRecord [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s10w [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconIsRecord @ Test.Logic_Equiv_ $dConstructor_s10w
      = Test.$dmconIsRecord_$s$dmconIsRecord2
"SPEC Test.$dmconIsRecord [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s10z [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconIsRecord @ Test.Logic_Impl_ $dConstructor_s10z
      = Test.$dmconIsRecord_$s$dmconIsRecord1
"SPEC Test.$dmconIsRecord [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s10C [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconIsRecord @ Test.Logic_Var_ $dConstructor_s10C
      = Test.$dmconIsRecord_$s$dmconIsRecord
"SPEC Test.updateString [Test.Logic]" ALWAYS
    forall {$dRegular_s105 :: Test.Regular Test.Logic
            $dUpdateString_X17n :: Test.UpdateString (Test.PF Test.Logic)}
      Test.updateString @ Test.Logic $dRegular_s105 $dUpdateString_X17n
      = Test.updateString_$supdateString


