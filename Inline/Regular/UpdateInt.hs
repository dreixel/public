{-# OPTIONS_GHC -fglasgow-exts    #-}
{-# LANGUAGE OverlappingInstances #-}

module Test where

-----------------------------------------------------------------------------
-- Functorial structural representation types.
-----------------------------------------------------------------------------

-- | Structure type for constant values.
newtype K a r    = K { unK :: a }

-- | Structure type for recursive values.
newtype I r      = I { unI :: r }

-- | Structure type for empty constructors.
data U r         = U

-- | Structure type for alternatives in a type.
data (f :+: g) r = L (f r) | R (g r)

-- | Structure type for fields of a constructor.
data (f :*: g) r = f r :*: g r

-- | Structure type to store the name of a constructor.
data C c f r =  C { unC :: f r }

infixr 6 :+:
infixr 7 :*:

-- | Class for datatypes that represent data constructors.
-- For non-symbolic constructors, only 'conName' has to be defined.
-- The weird argument is supposed to be instantiated with 'C' from
-- base, hence the complex kind.
class Constructor c where
  conName   :: t c (f :: * -> *) r -> String
  conFixity :: t c (f :: * -> *) r -> Fixity
  conFixity = const Prefix
  conIsRecord :: t c (f :: * -> *) r -> Bool
  conIsRecord = const False

-- | Datatype to represent the fixity of a constructor. An infix declaration
-- directly corresponds to an application of 'Infix'.
data Fixity = Prefix | Infix Associativity Int
  deriving (Eq, Show, Ord, Read)

data Associativity = LeftAssociative | RightAssociative | NotAssociative
  deriving (Eq, Show, Ord, Read)

-----------------------------------------------------------------------------
-- Type class capturing the structural representation of a type and the
-- corresponding embedding-projection pairs.
-----------------------------------------------------------------------------
-- | The type family @PF@ represents the pattern functor of a datatype.
-- 
-- To be able to use the generic functions, the user is required to provide
-- an instance of this type family.
type family PF a :: * -> *

-- | The type class @Regular@ captures the structural representation of a 
-- type and the corresponding embedding-projection pairs.
--
-- To be able to use the generic functions, the user is required to provide
-- an instance of this type class.
class Regular a where
  from      :: a -> PF a a
  to        :: PF a a -> a


-----------------------------------------------------------------------------
-- Generic update function: updates all Ints, leaves the rest unchanged.
-----------------------------------------------------------------------------

class UpdateInt f where
  -- {-# INLINE updateIntf #-}
  updateIntf :: (a -> a) -> f a -> f a

instance UpdateInt U where
  updateIntf = updateIntfU

instance UpdateInt I where
  updateIntf = updateIntfI

instance UpdateInt (K Int) where
  updateIntf = updateIntfKInt

instance UpdateInt (K x) where
  updateIntf = updateIntfK

instance (UpdateInt f, UpdateInt g) => UpdateInt (f :+: g) where
  updateIntf = updateIntfPlus

instance (UpdateInt f, UpdateInt g) => UpdateInt (f :*: g) where
  updateIntf = updateIntfTimes

instance (UpdateInt f) => UpdateInt (C c f) where
  updateIntf = updateIntfC
  
{-# INLINE updateIntfU #-}
updateIntfU _ = id

{-# INLINE updateIntfI #-}
updateIntfI f (I x) = I (f x)

{-# INLINE updateIntfKInt #-}
updateIntfKInt f (K n) | odd n     = K (n + 1)
                       | otherwise = K (n - 1)

{-# INLINE updateIntfK #-}
updateIntfK f (K x) = K x

{-# INLINE updateIntfPlus #-}
updateIntfPlus f (L x) = L (updateIntf f x)
updateIntfPlus f (R x) = R (updateIntf f x)

{-# INLINE updateIntfTimes #-}
updateIntfTimes f (x :*: y) = updateIntf f x :*: updateIntf f y

{-# INLINE updateIntfC #-}
updateIntfC f (C x) = C (updateIntf f x)


-- {-# INLINE updateInt #-}
updateInt :: (Regular a, UpdateInt (PF a)) => a -> a
updateInt = to . updateIntf (updateInt) . from


-----------------------------------------------------------------------------
-- Test datatypes and their generic representations
-----------------------------------------------------------------------------

data One = One One

data Two = Two0 | Two Int Two

data Three = Three Three Three Int | Three0 | Three1 Int

data Four = Four Four Int String Four | Four0 | Four1 Int | Four2 Int String


data One_One_
instance Constructor One_One_ where  conName _ = "One"
type PFOne = C One_One_ I
instance Regular One where
  from = fromOne
  to = toOne
  
fromOne (One f0) = C (I f0)
toOne (C (I f0)) = One f0

data Two_Two0_
data Two_Two_
instance Constructor Two_Two0_ where conName _ = "Two0"
instance Constructor Two_Two_ where conName _ = "Two"
type PFTwo = (:+:) (C Two_Two0_ U) (C Two_Two_ ((:*:) (K Int) I))
instance Regular Two where
  from = fromTwo
  to = toTwo

fromTwo Two0 = L (C U)
fromTwo (Two f0 f1) = R (C ((:*:) (K f0) (I f1)))
toTwo (L (C U)) = Two0
toTwo (R (C ((:*:) (K f0) (I f1)))) = Two f0 f1

data Three_Three_
data Three_Three0_
data Three_Three1_
instance Constructor Three_Three_ where conName _ = "Three"
instance Constructor Three_Three0_ where conName _ = "Three0"
instance Constructor Three_Three1_ where conName _ = "Three1"
type PFThree = (:+:) (C Three_Three_ ((:*:) I ((:*:) I (K Int)))) ((:+:) (C Three_Three0_ U) (C Three_Three1_ (K Int)))
instance Regular Three where
  from = fromThree
  to = toThree
  
fromThree (Three f0 f1 f2) = L (C ((:*:) (I f0) ((:*:) (I f1) (K f2))))
fromThree Three0 = R (L (C U))
fromThree (Three1 f0) = R (R (C (K f0)))
toThree (L (C ((:*:) (I f0) ((:*:) (I f1) (K f2))))) = Three f0 f1 f2
toThree (R (L (C U))) = Three0
toThree (R (R (C (K f0)))) = Three1 f0 

data Four_Four_
data Four_Four0_
data Four_Four1_
data Four_Four2_
instance Constructor Four_Four_ where conName _ = "Four"
instance Constructor Four_Four0_ where conName _ = "Four0"
instance Constructor Four_Four1_ where conName _ = "Four1"
instance Constructor Four_Four2_ where conName _ = "Four2"
type PFFour = (:+:) (C Four_Four_ ((:*:) I ((:*:) (K Int) ((:*:) (K String) I)))) ((:+:) (C Four_Four0_ U) ((:+:) (C Four_Four1_ (K Int)) (C Four_Four2_ ((:*:) (K Int) (K String)))))
instance Regular Four where
  from = fromFour
  to = toFour
  
fromFour (Four f0 f1 f2 f3) = L (C ((I f0) :*: (K f1) :*: (K f2) :*: (I f3)))
fromFour Four0 = R (L (C U))
fromFour (Four1 f0) = R (R (L (C (K f0))))
fromFour (Four2 f0 f1) = R (R (R (C ((:*:) (K f0) (K f1)))))
toFour (L (C ((I f0) :*: (K f1) :*: (K f2) :*: (I f3)))) = Four f0 f1 f2 f3
toFour (R (L (C U))) = Four0
toFour (R (R (L (C (K f0))))) = Four1 f0
toFour (R (R (R (C ((:*:) (K f0) (K f1)))))) = Four2 f0 f1 

type instance PF One = PFOne
type instance PF Two = PFTwo
type instance PF Three = PFThree
type instance PF Four = PFFour

-----------------------------------------------------------------------------
-- INLINE pragmas for the generic representations
-----------------------------------------------------------------------------

{-# INLINE fromOne #-}
{-# INLINE fromTwo #-}
{-# INLINE fromThree #-}
{-# INLINE fromFour #-}

{-# INLINE toOne #-}
{-# INLINE toTwo #-}
{-# INLINE toThree #-}
{-# INLINE toFour #-}

-----------------------------------------------------------------------------
-- Test values
-----------------------------------------------------------------------------
one :: One
one = One one

two :: Two
two = Two 3 Two0

three :: Three
three = Three Three0 (Three1 2) 3

four :: Four
four = Four Four0 3 "p" (Four1 2)

testOne = updateInt one
testTwo = updateInt two
testThree = updateInt three
testFour = updateInt four
