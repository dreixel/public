{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE EmptyDataDecls        #-}
{-# LANGUAGE TypeFamilies          #-}

module ReadInline where

-----------------------------------------------------------------------------
-- Generic read.
-----------------------------------------------------------------------------

import Generics.Regular.Base

import Data.Char
import Control.Monad
import Text.Read hiding (readsPrec, readPrec, read)
import Prelude hiding (readsPrec, read)
import qualified Prelude as P (readsPrec)
import Text.Read.Lex
import Text.ParserCombinators.ReadPrec


-- * Generic read

class HReadPrec f where
  {-# INLINE hreader #-}
  hreader :: ReadPrec a -> ReadPrec (f a)


instance HReadPrec U where
   hreader = hreaderU

instance (Read a) => HReadPrec (K a) where
   hreader = hreaderK

instance HReadPrec I where
   hreader = hreaderI

instance (HReadPrec f, HReadPrec g) => HReadPrec (f :+: g) where
   hreader = hreaderPlus

instance (HReadPrec f, HReadPrec g) => HReadPrec (f :*: g) where
   hreader = hreaderTimes

{-# INLINE hreaderU #-}
hreaderU _ = return U

{-# INLINE hreaderK #-}
hreaderK _ = liftM K (readS_to_Prec P.readsPrec)

{-# INLINE hreaderI #-}
hreaderI f = liftM I f

{-# INLINE hreaderPlus #-}
hreaderPlus f = liftM L (hreader f) +++ liftM R (hreader f)

{-# INLINE hreaderTimes #-}
hreaderTimes f = liftM2 (:*:) (hreader f) (hreader f)


instance (Constructor c, HReadPrec f) => HReadPrec (C c f) where
   hreader = hreaderC

{-# INLINE hreaderC #-}
hreaderC :: forall c f r. (Constructor c, HReadPrec f)
         => ReadPrec r -> ReadPrec (C c f r)
hreaderC f = let constr = undefined :: C c f r
                 name   = conName constr
             in  liftM C (readPrefixCons f name)


{-# INLINE readPrefixCons #-}
readPrefixCons :: (HReadPrec f) 
               => ReadPrec a -> String -> ReadPrec (f a)
readPrefixCons f name = do Ident n <- lexP
                           guard (name == n)
                           hreader f

{-# INLINE paren #-}
paren p = do
            Punc "(" <- lexP
            x <- p
            Punc ")" <- lexP
            return x


-- Exported functions
-- {-# INLINE readPrec #-}
readPrec :: (Regular a, HReadPrec (PF a)) => ReadPrec a
readPrec = liftM to (paren (hreader readPrec))

-- {-# INLINE readsPrec #-}
readsPrec :: (Regular a, HReadPrec (PF a)) => Int -> ReadS a
readsPrec n = readPrec_to_S readPrec n

-- {-# INLINE read #-}
read :: (Regular a, HReadPrec (PF a)) => String -> a
read s = case (readsPrec 0 s) of
           (h:_) -> fst h
           []    -> error "no parse"

-----------------------------------------------------------------------------
-- Test datatypes and their generic representations
-----------------------------------------------------------------------------
{-
data Logic = Var String
           | T                            -- true
           | F                            -- false
           | Not Logic                    -- not
           | Impl  Logic Logic            -- implication
           | Equiv Logic Logic            -- equivalence
           | Conj  Logic Logic            -- and (conjunction)
           | Disj  Logic Logic            -- or (disjunction)
  deriving Eq

data Logic_Var_
data Logic_Impl_
data Logic_Equiv_
data Logic_Conj_
data Logic_Disj_
data Logic_Not_
data Logic_T_
data Logic_F_

instance Constructor Logic_Var_ where
    { conName _ = "Var" }
instance Constructor Logic_Impl_ where
    { conName _ = "Impl" }
instance Constructor Logic_Equiv_ where
    { conName _ = "Equiv" }
instance Constructor Logic_Conj_ where
    { conName _ = "Conj" }
instance Constructor Logic_Disj_ where
    { conName _ = "Disj" }
instance Constructor Logic_Not_ where
    { conName _ = "Not" }
instance Constructor Logic_T_ where
    { conName _ = "T" }
instance Constructor Logic_F_ where
    { conName _ = "F" }
    
type PFLogic =
      (C Logic_Var_ (K String))
  :+: (C Logic_T_ U)
  :+: (C Logic_F_ U)
  :+: (C Logic_Not_ I)
  :+: (C Logic_Impl_ ((:*:) I I))
  :+: (C Logic_Equiv_ ((:*:) I I))
  :+: (C Logic_Conj_ ((:*:) I I))
  :+: (C Logic_Disj_ ((:*:) I I))

type instance PF Logic = PFLogic
instance Regular Logic where
  from = fromLogic  
  to   = toLogic

fromLogic (Var f0) = L (C (K f0))
fromLogic T = R (L (C U))
fromLogic F = R (R (L (C U)))
fromLogic (Not f0) = R (R (R (L (C (I f0)))))
fromLogic (Impl f0 f1) = R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))
fromLogic (Equiv f0 f1) = R (R (R (R (R (L (C ((:*:) (I f0) (I f1))))))))
fromLogic (Conj f0 f1) = R (R (R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))))
fromLogic (Disj f0 f1) = R (R (R (R (R (R (R (C ((:*:) (I f0) (I f1)))))))))

toLogic (L (C (K f0))) = Var f0
toLogic (R (L (C U))) = T
toLogic (R (R (L (C U)))) = F
toLogic (R (R (R (L (C (I f0)))))) = Not f0
toLogic (R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))) = Impl f0 f1
toLogic (R (R (R (R (R (L (C ((:*:) (I f0) (I f1))))))))) = Equiv f0 f1
toLogic (R (R (R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))))) = Conj f0 f1
toLogic (R (R (R (R (R (R (R (C ((:*:) (I f0) (I f1)))))))))) = Disj f0 f1

{-# INLINE [1] fromLogic #-}
{-# INLINE [1] toLogic #-}
-}
data Two = Two0 | Two Int Two deriving Eq

data Two_Two0_
data Two_Two_
instance Constructor Two_Two0_ where conName _ = "Two0"
instance Constructor Two_Two_ where conName _ = "Two"
type PFTwo = (:+:) (C Two_Two0_ U) (C Two_Two_ ((:*:) (K Int) I))
instance Regular Two where
  from = fromTwo
  to = toTwo

fromTwo Two0 = L (C U)
fromTwo (Two f0 f1) = R (C ((:*:) (K f0) (I f1)))
toTwo (L (C U)) = Two0
toTwo (R (C ((:*:) (K f0) (I f1)))) = Two f0 f1

type instance PF Two = PFTwo

{-# INLINE [1] fromTwo #-}
{-# INLINE [1] toTwo #-}
{-
data Three = Three Three Three Int | Three0 | Three1 Int deriving Eq

data Three_Three_
data Three_Three0_
data Three_Three1_
instance Constructor Three_Three_ where conName _ = "Three"
instance Constructor Three_Three0_ where conName _ = "Three0"
instance Constructor Three_Three1_ where conName _ = "Three1"
type PFThree = (:+:) (C Three_Three_ ((:*:) I ((:*:) I (K Int)))) ((:+:) (C Three_Three0_ U) (C Three_Three1_ (K Int)))
instance Regular Three where
  from = fromThree
  to = toThree
  
fromThree (Three f0 f1 f2) = L (C ((:*:) (I f0) ((:*:) (I f1) (K f2))))
fromThree Three0 = R (L (C U))
fromThree (Three1 f0) = R (R (C (K f0)))
toThree (L (C ((:*:) (I f0) ((:*:) (I f1) (K f2))))) = Three f0 f1 f2
toThree (R (L (C U))) = Three0
toThree (R (R (C (K f0)))) = Three1 f0 

type instance PF Three = PFThree

{-# INLINE [1] fromThree #-}
{-# INLINE [1] toThree #-}


data Four = Four Four Int String Four | Four0 | Four1 Int | Four2 Int String
  deriving Eq

data Four_Four_
data Four_Four0_
data Four_Four1_
data Four_Four2_
instance Constructor Four_Four_ where conName _ = "Four"
instance Constructor Four_Four0_ where conName _ = "Four0"
instance Constructor Four_Four1_ where conName _ = "Four1"
instance Constructor Four_Four2_ where conName _ = "Four2"
type PFFour = (:+:) (C Four_Four_ ((:*:) I ((:*:) (K Int) ((:*:) (K String) I)))) ((:+:) (C Four_Four0_ U) ((:+:) (C Four_Four1_ (K Int)) (C Four_Four2_ ((:*:) (K Int) (K String)))))
instance Regular Four where
  from = fromFour
  to = toFour
  
fromFour (Four f0 f1 f2 f3) = L (C ((I f0) :*: (K f1) :*: (K f2) :*: (I f3)))
fromFour Four0 = R (L (C U))
fromFour (Four1 f0) = R (R (L (C (K f0))))
fromFour (Four2 f0 f1) = R (R (R (C ((:*:) (K f0) (K f1)))))
toFour (L (C ((I f0) :*: (K f1) :*: (K f2) :*: (I f3)))) = Four f0 f1 f2 f3
toFour (R (L (C U))) = Four0
toFour (R (R (L (C (K f0))))) = Four1 f0
toFour (R (R (R (C ((:*:) (K f0) (K f1)))))) = Four2 f0 f1 

type instance PF Four = PFFour

{-# INLINE [1] fromFour #-}
{-# INLINE [1] toFour #-}
-}
-----------------------------------------------------------------------------
-- Test values
-----------------------------------------------------------------------------
{-
logic :: Logic
logic = Impl (Not T) (Var "x")
-}
two :: Two
two = Two 3 Two0
{-
three :: Three
three = Three Three0 (Three1 2) 3

four :: Four
four = Four Four0 3 "p" (Four1 2)
-}
testTwo = read "(Two 3 (Two0))" == two
{-
testThree = read "(Three (Three0) (Three1 2) 3)" == three
testFour = read "(Four (Four0) 3 \"p\" (Four1 2))" == four
testLogic = read "(Impl (Not (T)) (Var \"x\"))" == logic
-}