
==================== Tidy Core ====================
Test.updateStringf
  :: forall (f_aiO :: * -> *).
     (Test.UpdateString f_aiO) =>
     forall a_aiP. (a_aiP -> a_aiP) -> f_aiO a_aiP -> f_aiO a_aiP
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.updateStringf =
  \ (@ f_aiO::* -> *)
    (tpl_B1 [Occ=Once] :: Test.UpdateString f_aiO) ->
    tpl_B1
    `cast` (Test.NTCo:T:UpdateString f_aiO
            :: Test.T:UpdateString f_aiO
                 ~
               (forall a_aiP. (a_aiP -> a_aiP) -> f_aiO a_aiP -> f_aiO a_aiP))

Test.from [InlPrag=NOINLINE]
  :: forall a_aiQ.
     (Test.Regular a_aiQ) =>
     a_aiQ -> Test.PF a_aiQ a_aiQ
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.from: "Class op from"]
Test.from =
  \ (@ a_aiQ) (tpl_B1 [Occ=Once!] :: Test.Regular a_aiQ) ->
    case tpl_B1 of _ { Test.D:Regular tpl_B2 [Occ=Once] _ -> tpl_B2 }

Test.to [InlPrag=NOINLINE]
  :: forall a_aiQ.
     (Test.Regular a_aiQ) =>
     Test.PF a_aiQ a_aiQ -> a_aiQ
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.to: "Class op to"]
Test.to =
  \ (@ a_aiQ) (tpl_B1 [Occ=Once!] :: Test.Regular a_aiQ) ->
    case tpl_B1 of _ { Test.D:Regular _ tpl_B3 [Occ=Once] -> tpl_B3 }

Test.conName [InlPrag=NOINLINE]
  :: forall c_aiS.
     (Test.Constructor c_aiS) =>
     forall (t_aiT :: * -> (* -> *) -> * -> *) (f_aiU :: * -> *) r_aiV.
     t_aiT c_aiS f_aiU r_aiV -> GHC.Base.String
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SAA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conName: "Class op conName"]
Test.conName =
  \ (@ c_aiS) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiS) ->
    case tpl_B1 of _ { Test.D:Constructor tpl_B2 [Occ=Once] _ _ ->
    tpl_B2
    }

Test.conFixity [InlPrag=NOINLINE]
  :: forall c_aiS.
     (Test.Constructor c_aiS) =>
     forall (t_aiW :: * -> (* -> *) -> * -> *) (f_aiX :: * -> *) r_aiY.
     t_aiW c_aiS f_aiX r_aiY -> Test.Fixity
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(ASA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conFixity: "Class op conFixity"]
Test.conFixity =
  \ (@ c_aiS) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiS) ->
    case tpl_B1 of _ { Test.D:Constructor _ tpl_B3 [Occ=Once] _ ->
    tpl_B3
    }

Test.conIsRecord [InlPrag=NOINLINE]
  :: forall c_aiS.
     (Test.Constructor c_aiS) =>
     forall (t_aiZ :: * -> (* -> *) -> * -> *) (f_aj0 :: * -> *) r_aj1.
     t_aiZ c_aiS f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AAS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conIsRecord: "Class op conIsRecord"]
Test.conIsRecord =
  \ (@ c_aiS) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiS) ->
    case tpl_B1 of _ { Test.D:Constructor _ _ tpl_B4 [Occ=Once] ->
    tpl_B4
    }

Test.$fShowFixity2 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity2 = GHC.Base.unpackCString# "Infix "

Test.$fReadFixity6 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fReadFixity6 = GHC.Base.unpackCString# "Prefix"

Test.$fReadAssociativity9 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

Test.$fReadAssociativity12 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

Test.$fReadAssociativity15 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

Test.$fReadFixity4 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fReadFixity4 = GHC.Base.unpackCString# "Infix"

Test.$fConstructorLogic_Var_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Var_1 = GHC.Base.unpackCString# "Var"

Test.$fConstructorLogic_Impl_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Impl_1 = GHC.Base.unpackCString# "Impl"

Test.$fConstructorLogic_Equiv_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorLogic_Equiv_1 = GHC.Base.unpackCString# "Equiv"

Test.$fConstructorLogic_Conj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Conj_1 = GHC.Base.unpackCString# "Conj"

Test.$fConstructorLogic_Disj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Disj_1 = GHC.Base.unpackCString# "Disj"

Test.$fConstructorLogic_Not_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Not_1 = GHC.Base.unpackCString# "Not"

Test.$fConstructorLogic_T_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_T_2 = GHC.Types.C# 'T'

Test.$fConstructorLogic_T_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_T_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_T_2
    (GHC.Types.[] @ GHC.Types.Char)

Test.$fConstructorLogic_F_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_F_2 = GHC.Types.C# 'F'

Test.$fConstructorLogic_F_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_F_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_F_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl_r1bQ :: forall c_awj. Test.C c_awj Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl_r1bQ =
  \ (@ c_awj) ->
    Test.C @ c_awj @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl1_r1bS
  :: forall c_awj c1_awx c2_awQ c3_axc c4_axB c5_ay1.
     (Test.:+:)
       (Test.C c_awj Test.U)
       (Test.C c1_awx Test.I
        Test.:+: (Test.C c2_awQ (Test.I Test.:*: Test.I)
                  Test.:+: (Test.C c3_axc (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axB (Test.I Test.:*: Test.I)
                                      Test.:+: Test.C c5_ay1 (Test.I Test.:*: Test.I)))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl1_r1bS =
  \ (@ c_awj)
    (@ c1_awx)
    (@ c2_awQ)
    (@ c3_axc)
    (@ c4_axB)
    (@ c5_ay1) ->
    Test.L
      @ (Test.C c_awj Test.U)
      @ (Test.C c1_awx Test.I
         Test.:+: (Test.C c2_awQ (Test.I Test.:*: Test.I)
                   Test.:+: (Test.C c3_axc (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axB (Test.I Test.:*: Test.I)
                                       Test.:+: Test.C c5_ay1 (Test.I Test.:*: Test.I)))))
      @ Test.Logic
      (lvl_r1bQ @ c_awj)

lvl2_r1bU
  :: forall c_aw7 c1_awj c2_awx c3_awQ c4_axc c5_axB c6_ay1.
     (Test.:+:)
       (Test.C c_aw7 Test.U)
       (Test.C c1_awj Test.U
        Test.:+: (Test.C c2_awx Test.I
                  Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay1 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl2_r1bU =
  \ (@ c_aw7)
    (@ c1_awj)
    (@ c2_awx)
    (@ c3_awQ)
    (@ c4_axc)
    (@ c5_axB)
    (@ c6_ay1) ->
    Test.R
      @ (Test.C c_aw7 Test.U)
      @ (Test.C c1_awj Test.U
         Test.:+: (Test.C c2_awx Test.I
                   Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay1 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl1_r1bS @ c1_awj @ c2_awx @ c3_awQ @ c4_axc @ c5_axB @ c6_ay1)

lvl3_r1bW
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     (Test.:+:)
       (Test.C c_avX (Test.K GHC.Base.String))
       (Test.C c1_aw7 Test.U
        Test.:+: (Test.C c2_awj Test.U
                  Test.:+: (Test.C c3_awx Test.I
                            Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay1
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl3_r1bW =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1) ->
    Test.R
      @ (Test.C c_avX (Test.K GHC.Base.String))
      @ (Test.C c1_aw7 Test.U
         Test.:+: (Test.C c2_awj Test.U
                   Test.:+: (Test.C c3_awx Test.I
                             Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay1
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl2_r1bU
         @ c1_aw7 @ c2_awj @ c3_awx @ c4_awQ @ c5_axc @ c6_axB @ c7_ay1)

lvl4_r1bY :: forall c_aw7. Test.C c_aw7 Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl4_r1bY =
  \ (@ c_aw7) ->
    Test.C @ c_aw7 @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl5_r1c0
  :: forall c_aw7 c1_awj c2_awx c3_awQ c4_axc c5_axB c6_ay1.
     (Test.:+:)
       (Test.C c_aw7 Test.U)
       (Test.C c1_awj Test.U
        Test.:+: (Test.C c2_awx Test.I
                  Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay1 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl5_r1c0 =
  \ (@ c_aw7)
    (@ c1_awj)
    (@ c2_awx)
    (@ c3_awQ)
    (@ c4_axc)
    (@ c5_axB)
    (@ c6_ay1) ->
    Test.L
      @ (Test.C c_aw7 Test.U)
      @ (Test.C c1_awj Test.U
         Test.:+: (Test.C c2_awx Test.I
                   Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay1 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl4_r1bY @ c_aw7)

lvl6_r1c2
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     (Test.:+:)
       (Test.C c_avX (Test.K GHC.Base.String))
       (Test.C c1_aw7 Test.U
        Test.:+: (Test.C c2_awj Test.U
                  Test.:+: (Test.C c3_awx Test.I
                            Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay1
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl6_r1c2 =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1) ->
    Test.R
      @ (Test.C c_avX (Test.K GHC.Base.String))
      @ (Test.C c1_aw7 Test.U
         Test.:+: (Test.C c2_awj Test.U
                   Test.:+: (Test.C c3_awx Test.I
                             Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay1
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl5_r1c0
         @ c1_aw7 @ c2_awj @ c3_awx @ c4_awQ @ c5_axc @ c6_axB @ c7_ay1)

Test.$dmconFixity_$s$dmconFixity7
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_F_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity7 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity6
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_T_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity6 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity5
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_Not_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity5 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity4
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_Disj_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity4 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity3
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_Conj_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity3 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity2
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_Equiv_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity2 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity1
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_Impl_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity1 =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity
  :: forall (t_aiW :: * -> (* -> *) -> * -> *)
            (f_aiX :: * -> *)
            r_aiY.
     t_aiW Test.Logic_Var_ f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity =
  \ (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconIsRecord_$s$dmconIsRecord7
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_F_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord7 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord6
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_T_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord6 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord5
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_Not_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord5 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord4
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_Disj_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord4 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord3
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_Conj_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord3 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord2
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_Equiv_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord2 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord1
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_Impl_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord1 =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord
  :: forall (t_aiZ :: * -> (* -> *) -> * -> *)
            (f_aj0 :: * -> *)
            r_aj1.
     t_aiZ Test.Logic_Var_ f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord =
  \ (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.$fShowFixity1 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fShowFixity1 = GHC.Types.I# 0

Test.$fReadAssociativity14
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWj.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj)
        -> Text.ParserCombinators.ReadP.P b_aWj
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity14 =
  \ _
    (@ b_aWj)
    (eta_aWk
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj) ->
    eta_aWk Test.LeftAssociative

Test.$fReadAssociativity13
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity13 =
  (Test.$fReadAssociativity15,
   Test.$fReadAssociativity14
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWB.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                  -> Text.ParserCombinators.ReadP.P b_aWB)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity11
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWj.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj)
        -> Text.ParserCombinators.ReadP.P b_aWj
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity11 =
  \ _
    (@ b_aWj)
    (eta_aWk
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj) ->
    eta_aWk Test.RightAssociative

Test.$fReadAssociativity10
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity10 =
  (Test.$fReadAssociativity12,
   Test.$fReadAssociativity11
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWB.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                  -> Text.ParserCombinators.ReadP.P b_aWB)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity8
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWj.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj)
        -> Text.ParserCombinators.ReadP.P b_aWj
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity8 =
  \ _
    (@ b_aWj)
    (eta_aWk
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWj) ->
    eta_aWk Test.NotAssociative

Test.$fReadAssociativity7
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity7 =
  (Test.$fReadAssociativity9,
   Test.$fReadAssociativity8
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWB.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                  -> Text.ParserCombinators.ReadP.P b_aWB)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity6
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity5
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity10
    Test.$fReadAssociativity6

Test.$fReadAssociativity4
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity13
    Test.$fReadAssociativity5

Test.$fReadAssociativity3
  :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity3 =
  GHC.Read.choose1 @ Test.Associativity Test.$fReadAssociativity4

Test.$fReadAssociativity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Associativity Test.$fReadAssociativity3

Test.$fReadAssociativity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ Test.Associativity
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity16
  :: Text.ParserCombinators.ReadP.P [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadAssociativity16 =
  ((Test.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [Test.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
                ~
              (forall b_aWB.
               ([Test.Associativity] -> Text.ParserCombinators.ReadP.P b_aWB)
               -> Text.ParserCombinators.ReadP.P b_aWB)))
    @ [Test.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn
       @ [Test.Associativity])

Test.$fReadFixity8
  :: forall b_aWB.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_aWB
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 1}]
Test.$fReadFixity8 =
  \ (@ b_aWB) (a111_a193 :: GHC.Base.String) ->
    case a111_a193 of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_aWB
    }

Test.$fReadFixity7
  :: forall b_aWB. Text.ParserCombinators.ReadP.P b_aWB
[GblId,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fReadFixity7 =
  \ (@ b_aWB) ->
    Text.ParserCombinators.ReadP.Look
      @ b_aWB (Test.$fReadFixity8 @ b_aWB)

Test.$fReadFixity5 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fReadFixity5 = GHC.Types.I# 11

Test.$fReadFixity3
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWB.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
        -> Text.ParserCombinators.ReadP.P b_aWB
[GblId,
 Arity=2,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 12] 63 0}]
Test.$fReadFixity3 =
  \ (n_a16R :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_aWB)
    (eta_B1 :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB) ->
    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
      @ b_aWB
      (Text.ParserCombinators.ReadP.Look
         @ b_aWB
         (let {
            $wk_s1af [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_aWB
            [LclId, Str=DmdType]
            $wk_s1af =
              Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                @ b_aWB
                (Test.$fReadFixity7 @ b_aWB)
                (Text.Read.Lex.hsLex2
                   @ b_aWB
                   (let {
                      lvl18_s1bl :: Text.ParserCombinators.ReadP.P b_aWB
                      [LclId]
                      lvl18_s1bl = eta_B1 Test.Prefix } in
                    \ (a11_aWP :: Text.Read.Lex.Lexeme) ->
                      case a11_aWP of _ {
                        __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_aWB;
                        Text.Read.Lex.Ident ds_dT5 ->
                          case GHC.Base.eqString ds_dT5 Test.$fReadFixity6 of _ {
                            GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_aWB;
                            GHC.Bool.True -> lvl18_s1bl
                          }
                      })) } in
          let {
            k_s1ah :: () -> Text.ParserCombinators.ReadP.P b_aWB
            [LclId, Arity=1, Str=DmdType A]
            k_s1ah = \ _ -> $wk_s1af } in
          \ (a111_a19m :: GHC.Base.String) ->
            ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a19m)
             `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                     :: Text.ParserCombinators.ReadP.ReadP ()
                          ~
                        (forall b_aWB.
                         (() -> Text.ParserCombinators.ReadP.P b_aWB)
                         -> Text.ParserCombinators.ReadP.P b_aWB)))
              @ b_aWB k_s1ah))
      (case n_a16R of _ { GHC.Types.I# x_a16v ->
       case GHC.Prim.<=# x_a16v 10 of _ {
         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_aWB;
         GHC.Bool.True ->
           Text.ParserCombinators.ReadP.Look
             @ b_aWB
             (let {
                $wk_s1ap :: Text.ParserCombinators.ReadP.P b_aWB
                [LclId, Str=DmdType]
                $wk_s1ap =
                  let {
                    lvl18_s1bm :: Text.ParserCombinators.ReadP.P b_aWB
                    [LclId]
                    lvl18_s1bm =
                      ((GHC.Read.$dmreadsPrec3
                          @ Test.Associativity Test.$fReadAssociativity3 Test.$fReadFixity5)
                       `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                               :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                    ~
                                  (forall b_aWB.
                                   (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                                   -> Text.ParserCombinators.ReadP.P b_aWB)))
                        @ b_aWB
                        (\ (a11_X11a :: Test.Associativity) ->
                           ((GHC.Read.$dmreadsPrec11 Test.$fReadFixity5)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                                    :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                         ~
                                       (forall b_aWB.
                                        (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_aWB)
                                        -> Text.ParserCombinators.ReadP.P b_aWB)))
                             @ b_aWB
                             (\ (a12_X11H :: GHC.Types.Int) ->
                                eta_B1 (Test.Infix a11_X11a a12_X11H))) } in
                  Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                    @ b_aWB
                    (Text.ParserCombinators.ReadP.Look
                       @ b_aWB
                       (\ (a111_a193 :: GHC.Base.String) ->
                          case a111_a193 of _ { __DEFAULT ->
                          Text.ParserCombinators.ReadP.Fail @ b_aWB
                          }))
                    (Text.Read.Lex.hsLex2
                       @ b_aWB
                       (\ (a11_aWP :: Text.Read.Lex.Lexeme) ->
                          case a11_aWP of _ {
                            __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_aWB;
                            Text.Read.Lex.Ident ds_dTa ->
                              case GHC.Base.eqString ds_dTa Test.$fReadFixity4 of _ {
                                GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_aWB;
                                GHC.Bool.True -> lvl18_s1bm
                              }
                          })) } in
              let {
                k_s1ar :: () -> Text.ParserCombinators.ReadP.P b_aWB
                [LclId, Arity=1, Str=DmdType A]
                k_s1ar = \ _ -> $wk_s1ap } in
              \ (a111_a19m :: GHC.Base.String) ->
                ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a19m)
                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                         :: Text.ParserCombinators.ReadP.ReadP ()
                              ~
                            (forall b_aWB.
                             (() -> Text.ParserCombinators.ReadP.P b_aWB)
                             -> Text.ParserCombinators.ReadP.P b_aWB)))
                  @ b_aWB k_s1ar)
       }
       })

Test.$fReadFixity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Fixity
    (Test.$fReadFixity3
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_aWB.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
                    -> Text.ParserCombinators.ReadP.P b_aWB)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ Test.Fixity
    (Test.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity9 :: Text.ParserCombinators.ReadP.P [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadFixity9 =
  ((Test.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [Test.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
                ~
              (forall b_aWB.
               ([Test.Fixity] -> Text.ParserCombinators.ReadP.P b_aWB)
               -> Text.ParserCombinators.ReadP.P b_aWB)))
    @ [Test.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ [Test.Fixity])

Test.logic3 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic3 = GHC.Types.C# 'x'

Test.logic2 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic2 =
  GHC.Types.:
    @ GHC.Types.Char Test.logic3 (GHC.Types.[] @ GHC.Types.Char)

Test.logic1 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic1 = Test.Var Test.logic2

Test.logic4 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic4 = Test.Not Test.T

a_r1c8
  :: forall t_aAv t1_aAx t2_aAy r_aAB.
     t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy t1_aAx
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType AS]
a_r1c8 =
  \ (@ t_aAv)
    (@ t1_aAx)
    (@ t2_aAy)
    (@ r_aAB)
    _
    (eta1_X7t :: Test.K t2_aAy t1_aAx) ->
    eta1_X7t

a1_r1ca
  :: forall t_aBy r_aBB. (t_aBy -> r_aBB) -> Test.I t_aBy -> r_aBB
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType C(S)L]
a1_r1ca =
  \ (@ t_aBy)
    (@ r_aBB)
    (eta_B2 :: t_aBy -> r_aBB)
    (eta1_B1 :: Test.I t_aBy) ->
    eta_B2 (eta1_B1 `cast` (Test.NTCo:I t_aBy :: Test.I t_aBy ~ t_aBy))

Test.unI1 :: forall r_auO. Test.I r_auO -> Test.I r_auO
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unI1 = \ (@ r_auO) (ds_dSv :: Test.I r_auO) -> ds_dSv

Test.unK1
  :: forall a_auQ r_auR. Test.K a_auQ r_auR -> Test.K a_auQ r_auR
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unK1 =
  \ (@ a_auQ) (@ r_auR) (ds_dSx :: Test.K a_auQ r_auR) -> ds_dSx

Test.unK :: forall a_ajd r_aje. Test.K a_ajd r_aje -> a_ajd
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unK =
  Test.unK1
  `cast` (forall a_auQ r_auR.
          Test.K a_auQ r_auR -> Test.NTCo:K a_auQ r_auR
          :: (forall a_auQ r_auR. Test.K a_auQ r_auR -> Test.K a_auQ r_auR)
               ~
             (forall a_auQ r_auR. Test.K a_auQ r_auR -> a_auQ))

Test.unI :: forall r_ajc. Test.I r_ajc -> r_ajc
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unI =
  Test.unI1
  `cast` (forall r_auO. Test.I r_auO -> Test.NTCo:I r_auO
          :: (forall r_auO. Test.I r_auO -> Test.I r_auO)
               ~
             (forall r_auO. Test.I r_auO -> r_auO))

Test.unC
  :: forall c_aj2 (f_aj3 :: * -> *) r_aj4.
     Test.C c_aj2 f_aj3 r_aj4 -> f_aj3 r_aj4
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(S),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_auK)
                 (@ f_auL::* -> *)
                 (@ r_auM)
                 (ds_dSs [Occ=Once!] :: Test.C c_auK f_auL r_auM) ->
                 case ds_dSs of _ { Test.C ds1_dSt [Occ=Once] -> ds1_dSt }}]
Test.unC =
  \ (@ c_auK)
    (@ f_auL::* -> *)
    (@ r_auM)
    (ds_dSs :: Test.C c_auK f_auL r_auM) ->
    case ds_dSs of _ { Test.C ds1_dSt -> ds1_dSt }

Test.fromLogic [InlPrag=INLINE]
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     Test.Logic
     -> (Test.:+:)
          (Test.C c_avX (Test.K GHC.Base.String))
          (Test.C c1_aw7 Test.U
           Test.:+: (Test.C c2_awj Test.U
                     Test.:+: (Test.C c3_awx Test.I
                               Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        c7_ay1
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_avX)
                 (@ c1_aw7)
                 (@ c2_awj)
                 (@ c3_awx)
                 (@ c4_awQ)
                 (@ c5_axc)
                 (@ c6_axB)
                 (@ c7_ay1)
                 (ds_dRV [Occ=Once!] :: Test.Logic) ->
                 case ds_dRV of _ {
                   Test.Var f0_ajF [Occ=Once] ->
                     Test.L
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.C
                          @ c_avX
                          @ (Test.K GHC.Base.String)
                          @ Test.Logic
                          (f0_ajF
                           `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                                   :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
                   Test.T ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.L
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.C @ c1_aw7 @ Test.U @ Test.Logic (Test.U @ Test.Logic)));
                   Test.F ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.L
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.C @ c2_awj @ Test.U @ Test.Logic (Test.U @ Test.Logic))));
                   Test.Not f0_ajG [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.L
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.C
                                   @ c3_awx
                                   @ Test.I
                                   @ Test.Logic
                                   (f0_ajG
                                    `cast` (sym (Test.NTCo:I Test.Logic)
                                            :: Test.Logic ~ Test.I Test.Logic))))));
                   Test.Impl f0_ajH [Occ=Once] f1_ajI [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.L
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.C
                                      @ c4_awQ
                                      @ (Test.I Test.:*: Test.I)
                                      @ Test.Logic
                                      (Test.:*:
                                         @ Test.I
                                         @ Test.I
                                         @ Test.Logic
                                         (f0_ajH
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))
                                         (f1_ajI
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))))))));
                   Test.Equiv f0_ajJ [Occ=Once] f1_ajK [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.L
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.C
                                         @ c5_axc
                                         @ (Test.I Test.:*: Test.I)
                                         @ Test.Logic
                                         (Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ Test.Logic
                                            (f0_ajJ
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic))
                                            (f1_ajK
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic)))))))));
                   Test.Conj f0_ajL [Occ=Once] f1_ajM [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.L
                                         @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c6_axB
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajL
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajM
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))));
                   Test.Disj f0_ajN [Occ=Once] f1_ajO [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c7_ay1
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajN
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajO
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))))
                 }}]
Test.fromLogic =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1)
    (eta_B1 :: Test.Logic) ->
    case eta_B1 of _ {
      Test.Var f0_ajF ->
        Test.L
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.C
             @ c_avX
             @ (Test.K GHC.Base.String)
             @ Test.Logic
             (f0_ajF
              `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                      :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
      Test.T ->
        lvl6_r1c2
          @ c_avX
          @ c1_aw7
          @ c2_awj
          @ c3_awx
          @ c4_awQ
          @ c5_axc
          @ c6_axB
          @ c7_ay1;
      Test.F ->
        lvl3_r1bW
          @ c_avX
          @ c1_aw7
          @ c2_awj
          @ c3_awx
          @ c4_awQ
          @ c5_axc
          @ c6_axB
          @ c7_ay1;
      Test.Not f0_ajG ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.L
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.C
                      @ c3_awx
                      @ Test.I
                      @ Test.Logic
                      (f0_ajG
                       `cast` (sym (Test.NTCo:I Test.Logic)
                               :: Test.Logic ~ Test.I Test.Logic))))));
      Test.Impl f0_ajH f1_ajI ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.L
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.C
                         @ c4_awQ
                         @ (Test.I Test.:*: Test.I)
                         @ Test.Logic
                         (Test.:*:
                            @ Test.I
                            @ Test.I
                            @ Test.Logic
                            (f0_ajH
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))
                            (f1_ajI
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))))))));
      Test.Equiv f0_ajJ f1_ajK ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.L
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.C
                            @ c5_axc
                            @ (Test.I Test.:*: Test.I)
                            @ Test.Logic
                            (Test.:*:
                               @ Test.I
                               @ Test.I
                               @ Test.Logic
                               (f0_ajJ
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic))
                               (f1_ajK
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic)))))))));
      Test.Conj f0_ajL f1_ajM ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.L
                            @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c6_axB
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ajL
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ajM
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))));
      Test.Disj f0_ajN f1_ajO ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.R
                            @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c7_ay1
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ajN
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ajO
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))))
    }

Test.toLogic [InlPrag=INLINE]
  :: forall t_av9 t1_avf t2_avj t3_avn t4_avs t5_avy t6_avE t7_avI.
     (Test.:+:)
       (Test.C t_av9 (Test.K GHC.Base.String))
       (Test.C t1_avf Test.U
        Test.:+: (Test.C t2_avj Test.U
                  Test.:+: (Test.C t3_avn Test.I
                            Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C t5_avy (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C t6_avE (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     t7_avI
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
     -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_av9)
                 (@ t1_avf)
                 (@ t2_avj)
                 (@ t3_avn)
                 (@ t4_avs)
                 (@ t5_avy)
                 (@ t6_avE)
                 (@ t7_avI)
                 (ds_dQY [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C t_av9 (Test.K GHC.Base.String))
                         (Test.C t1_avf Test.U
                          Test.:+: (Test.C t2_avj Test.U
                                    Test.:+: (Test.C t3_avn Test.I
                                              Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    t5_avy (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              t6_avE
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       t7_avI
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                         Test.Logic) ->
                 case ds_dQY of _ {
                   Test.L ds1_dRr [Occ=Once!] ->
                     case ds1_dRr of _ { Test.C ds2_dRs [Occ=Once] ->
                     Test.Var
                       (ds2_dRs
                        `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                     };
                   Test.R ds1_dQZ [Occ=Once!] ->
                     case ds1_dQZ of _ {
                       Test.L ds2_dRp [Occ=Once!] ->
                         case ds2_dRp of _ { Test.C ds3_dRq [Occ=Once!] ->
                         case ds3_dRq of _ { Test.U -> Test.T }
                         };
                       Test.R ds2_dR0 [Occ=Once!] ->
                         case ds2_dR0 of _ {
                           Test.L ds3_dRn [Occ=Once!] ->
                             case ds3_dRn of _ { Test.C ds4_dRo [Occ=Once!] ->
                             case ds4_dRo of _ { Test.U -> Test.F }
                             };
                           Test.R ds3_dR1 [Occ=Once!] ->
                             case ds3_dR1 of _ {
                               Test.L ds4_dRl [Occ=Once!] ->
                                 case ds4_dRl of _ { Test.C ds5_dRm [Occ=Once] ->
                                 Test.Not
                                   (ds5_dRm
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 };
                               Test.R ds4_dR2 [Occ=Once!] ->
                                 case ds4_dR2 of _ {
                                   Test.L ds5_dRh [Occ=Once!] ->
                                     case ds5_dRh of _ { Test.C ds6_dRi [Occ=Once!] ->
                                     case ds6_dRi
                                     of _ { Test.:*: ds7_dRj [Occ=Once] ds8_dRk [Occ=Once] ->
                                     Test.Impl
                                       (ds7_dRj
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds8_dRk
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds5_dR3 [Occ=Once!] ->
                                     case ds5_dR3 of _ {
                                       Test.L ds6_dRd [Occ=Once!] ->
                                         case ds6_dRd of _ { Test.C ds7_dRe [Occ=Once!] ->
                                         case ds7_dRe
                                         of _ { Test.:*: ds8_dRf [Occ=Once] ds9_dRg [Occ=Once] ->
                                         Test.Equiv
                                           (ds8_dRf
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds9_dRg
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds6_dR4 [Occ=Once!] ->
                                         case ds6_dR4 of _ {
                                           Test.L ds7_dR9 [Occ=Once!] ->
                                             case ds7_dR9 of _ { Test.C ds8_dRa [Occ=Once!] ->
                                             case ds8_dRa
                                             of _
                                             { Test.:*: ds9_dRb [Occ=Once] ds10_dRc [Occ=Once] ->
                                             Test.Conj
                                               (ds9_dRb
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dRc
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             };
                                           Test.R ds7_dR5 [Occ=Once!] ->
                                             case ds7_dR5 of _ { Test.C ds8_dR6 [Occ=Once!] ->
                                             case ds8_dR6
                                             of _
                                             { Test.:*: ds9_dR7 [Occ=Once] ds10_dR8 [Occ=Once] ->
                                             Test.Disj
                                               (ds9_dR7
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dR8
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }}]
Test.toLogic =
  \ (@ t_av9)
    (@ t1_avf)
    (@ t2_avj)
    (@ t3_avn)
    (@ t4_avs)
    (@ t5_avy)
    (@ t6_avE)
    (@ t7_avI)
    (eta_B1
       :: (Test.:+:)
            (Test.C t_av9 (Test.K GHC.Base.String))
            (Test.C t1_avf Test.U
             Test.:+: (Test.C t2_avj Test.U
                       Test.:+: (Test.C t3_avn Test.I
                                 Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C t5_avy (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 t6_avE (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          t7_avI
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
            Test.Logic) ->
    case eta_B1 of _ {
      Test.L ds_dRr ->
        case ds_dRr of _ { Test.C ds1_dRs ->
        Test.Var
          (ds1_dRs
           `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                   :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
        };
      Test.R ds_dQZ ->
        case ds_dQZ of _ {
          Test.L ds1_dRp ->
            case ds1_dRp of _ { Test.C ds2_dRq ->
            case ds2_dRq of _ { Test.U -> Test.T }
            };
          Test.R ds1_dR0 ->
            case ds1_dR0 of _ {
              Test.L ds2_dRn ->
                case ds2_dRn of _ { Test.C ds3_dRo ->
                case ds3_dRo of _ { Test.U -> Test.F }
                };
              Test.R ds2_dR1 ->
                case ds2_dR1 of _ {
                  Test.L ds3_dRl ->
                    case ds3_dRl of _ { Test.C ds4_dRm ->
                    Test.Not
                      (ds4_dRm
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    };
                  Test.R ds3_dR2 ->
                    case ds3_dR2 of _ {
                      Test.L ds4_dRh ->
                        case ds4_dRh of _ { Test.C ds5_dRi ->
                        case ds5_dRi of _ { Test.:*: ds6_dRj ds7_dRk ->
                        Test.Impl
                          (ds6_dRj
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds7_dRk
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds4_dR3 ->
                        case ds4_dR3 of _ {
                          Test.L ds5_dRd ->
                            case ds5_dRd of _ { Test.C ds6_dRe ->
                            case ds6_dRe of _ { Test.:*: ds7_dRf ds8_dRg ->
                            Test.Equiv
                              (ds7_dRf
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds8_dRg
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds5_dR4 ->
                            case ds5_dR4 of _ {
                              Test.L ds6_dR9 ->
                                case ds6_dR9 of _ { Test.C ds7_dRa ->
                                case ds7_dRa of _ { Test.:*: ds8_dRb ds9_dRc ->
                                Test.Conj
                                  (ds8_dRb
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRc
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                };
                              Test.R ds6_dR5 ->
                                case ds6_dR5 of _ { Test.C ds7_dR6 ->
                                case ds7_dR6 of _ { Test.:*: ds8_dR7 ds9_dR8 ->
                                Test.Disj
                                  (ds8_dR7
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dR8
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

Test.updateStringfTimes [InlPrag=INLINE]
  :: forall (t_azR :: * -> *) (t1_azT :: * -> *) r_azX.
     (Test.UpdateString t_azR, Test.UpdateString t1_azT) =>
     (r_azX -> r_azX)
     -> (Test.:*:) t_azR t1_azT r_azX
     -> (Test.:*:) t_azR t1_azT r_azX
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azR::* -> *)
                 (@ t1_azT::* -> *)
                 (@ r_azX)
                 ($dUpdateString_aA1 [Occ=Once] :: Test.UpdateString t_azR)
                 ($dUpdateString1_aA2 [Occ=Once] :: Test.UpdateString t1_azT)
                 (f_ajA :: r_azX -> r_azX)
                 (ds_dS6 [Occ=Once!] :: (Test.:*:) t_azR t1_azT r_azX) ->
                 case ds_dS6 of _ { Test.:*: x_ajB [Occ=Once] y_ajC [Occ=Once] ->
                 Test.:*:
                   @ t_azR
                   @ t1_azT
                   @ r_azX
                   (($dUpdateString_aA1
                     `cast` (Test.NTCo:T:UpdateString t_azR
                             :: Test.T:UpdateString t_azR
                                  ~
                                (forall a_aiP. (a_aiP -> a_aiP) -> t_azR a_aiP -> t_azR a_aiP)))
                      @ r_azX f_ajA x_ajB)
                   (($dUpdateString1_aA2
                     `cast` (Test.NTCo:T:UpdateString t1_azT
                             :: Test.T:UpdateString t1_azT
                                  ~
                                (forall a_aiP. (a_aiP -> a_aiP) -> t1_azT a_aiP -> t1_azT a_aiP)))
                      @ r_azX f_ajA y_ajC)
                 }}]
Test.updateStringfTimes =
  \ (@ t_azR::* -> *)
    (@ t1_azT::* -> *)
    (@ r_azX)
    ($dUpdateString_aA1 :: Test.UpdateString t_azR)
    ($dUpdateString1_aA2 :: Test.UpdateString t1_azT)
    (eta_B2 :: r_azX -> r_azX)
    (eta1_B1 :: (Test.:*:) t_azR t1_azT r_azX) ->
    case eta1_B1 of _ { Test.:*: x_ajB y_ajC ->
    Test.:*:
      @ t_azR
      @ t1_azT
      @ r_azX
      (($dUpdateString_aA1
        `cast` (Test.NTCo:T:UpdateString t_azR
                :: Test.T:UpdateString t_azR
                     ~
                   (forall a_aiP. (a_aiP -> a_aiP) -> t_azR a_aiP -> t_azR a_aiP)))
         @ r_azX eta_B2 x_ajB)
      (($dUpdateString1_aA2
        `cast` (Test.NTCo:T:UpdateString t1_azT
                :: Test.T:UpdateString t1_azT
                     ~
                   (forall a_aiP. (a_aiP -> a_aiP) -> t1_azT a_aiP -> t1_azT a_aiP)))
         @ r_azX eta_B2 y_ajC)
    }

Test.$fUpdateString:*:1
  :: forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
     (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
     forall a_aPI.
     (a_aPI -> a_aPI)
     -> (Test.:*:) f_ak1 g_ak2 a_aPI
     -> (Test.:*:) f_ak1 g_ak2 a_aPI
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_ak1::* -> *)
                 (@ g_ak2::* -> *)
                 ($dUpdateString_aPC [Occ=Once] :: Test.UpdateString f_ak1)
                 ($dUpdateString1_aPD [Occ=Once] :: Test.UpdateString g_ak2)
                 (@ a_aPI)
                 (eta_B2 [Occ=Once] :: a_aPI -> a_aPI)
                 (eta1_B1 [Occ=Once] :: (Test.:*:) f_ak1 g_ak2 a_aPI) ->
                 Test.updateStringfTimes
                   @ f_ak1
                   @ g_ak2
                   @ a_aPI
                   $dUpdateString_aPC
                   $dUpdateString1_aPD
                   eta_B2
                   eta1_B1}]
Test.$fUpdateString:*:1 =
  \ (@ f_ak1::* -> *)
    (@ g_ak2::* -> *)
    ($dUpdateString_aPC :: Test.UpdateString f_ak1)
    ($dUpdateString1_aPD :: Test.UpdateString g_ak2)
    (@ a_aPI)
    (eta_B2 :: a_aPI -> a_aPI)
    (eta1_B1 :: (Test.:*:) f_ak1 g_ak2 a_aPI) ->
    Test.updateStringfTimes
      @ f_ak1
      @ g_ak2
      @ a_aPI
      $dUpdateString_aPC
      $dUpdateString1_aPD
      eta_B2
      eta1_B1

Test.updateStringfPlus [InlPrag=INLINE]
  :: forall (t_aAb :: * -> *) r_aAg (g_aAi :: * -> *).
     (Test.UpdateString t_aAb, Test.UpdateString g_aAi) =>
     (r_aAg -> r_aAg)
     -> (Test.:+:) t_aAb g_aAi r_aAg
     -> (Test.:+:) t_aAb g_aAi r_aAg
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAb::* -> *)
                 (@ r_aAg)
                 (@ g_aAi::* -> *)
                 ($dUpdateString_aAp [Occ=Once] :: Test.UpdateString t_aAb)
                 ($dUpdateString1_aAq [Occ=Once] :: Test.UpdateString g_aAi)
                 (f_ajw [Occ=Once*] :: r_aAg -> r_aAg)
                 (ds_dS8 [Occ=Once!] :: (Test.:+:) t_aAb g_aAi r_aAg) ->
                 case ds_dS8 of _ {
                   Test.L x_ajx [Occ=Once] ->
                     Test.L
                       @ t_aAb
                       @ g_aAi
                       @ r_aAg
                       (($dUpdateString_aAp
                         `cast` (Test.NTCo:T:UpdateString t_aAb
                                 :: Test.T:UpdateString t_aAb
                                      ~
                                    (forall a_aiP. (a_aiP -> a_aiP) -> t_aAb a_aiP -> t_aAb a_aiP)))
                          @ r_aAg f_ajw x_ajx);
                   Test.R x_ajz [Occ=Once] ->
                     Test.R
                       @ t_aAb
                       @ g_aAi
                       @ r_aAg
                       (($dUpdateString1_aAq
                         `cast` (Test.NTCo:T:UpdateString g_aAi
                                 :: Test.T:UpdateString g_aAi
                                      ~
                                    (forall a_aiP. (a_aiP -> a_aiP) -> g_aAi a_aiP -> g_aAi a_aiP)))
                          @ r_aAg f_ajw x_ajz)
                 }}]
Test.updateStringfPlus =
  \ (@ t_aAb::* -> *)
    (@ r_aAg)
    (@ g_aAi::* -> *)
    ($dUpdateString_aAp :: Test.UpdateString t_aAb)
    ($dUpdateString1_aAq :: Test.UpdateString g_aAi)
    (eta_B2 :: r_aAg -> r_aAg)
    (eta1_B1 :: (Test.:+:) t_aAb g_aAi r_aAg) ->
    case eta1_B1 of _ {
      Test.L x_ajx ->
        Test.L
          @ t_aAb
          @ g_aAi
          @ r_aAg
          (($dUpdateString_aAp
            `cast` (Test.NTCo:T:UpdateString t_aAb
                    :: Test.T:UpdateString t_aAb
                         ~
                       (forall a_aiP. (a_aiP -> a_aiP) -> t_aAb a_aiP -> t_aAb a_aiP)))
             @ r_aAg eta_B2 x_ajx);
      Test.R x_ajz ->
        Test.R
          @ t_aAb
          @ g_aAi
          @ r_aAg
          (($dUpdateString1_aAq
            `cast` (Test.NTCo:T:UpdateString g_aAi
                    :: Test.T:UpdateString g_aAi
                         ~
                       (forall a_aiP. (a_aiP -> a_aiP) -> g_aAi a_aiP -> g_aAi a_aiP)))
             @ r_aAg eta_B2 x_ajz)
    }

Test.$fUpdateString:+:1
  :: forall (f_ak3 :: * -> *) (g_ak4 :: * -> *).
     (Test.UpdateString f_ak3, Test.UpdateString g_ak4) =>
     forall a_aPT.
     (a_aPT -> a_aPT)
     -> (Test.:+:) f_ak3 g_ak4 a_aPT
     -> (Test.:+:) f_ak3 g_ak4 a_aPT
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0 0 0] 5 0}]
Test.$fUpdateString:+:1 =
  \ (@ f_ak3::* -> *)
    (@ g_ak4::* -> *)
    ($dUpdateString_aPN :: Test.UpdateString f_ak3)
    ($dUpdateString1_aPO :: Test.UpdateString g_ak4)
    (@ a_aPT)
    (eta_B2 :: a_aPT -> a_aPT)
    (eta1_B1 :: (Test.:+:) f_ak3 g_ak4 a_aPT) ->
    Test.updateStringfPlus
      @ f_ak3
      @ a_aPT
      @ g_ak4
      $dUpdateString_aPN
      $dUpdateString1_aPO
      eta_B2
      eta1_B1

Test.updateStringfC [InlPrag=INLINE]
  :: forall t_azw (t1_azz :: * -> *) r_azE c_azG.
     (Test.UpdateString t1_azz) =>
     (r_azE -> r_azE)
     -> Test.C t_azw t1_azz r_azE
     -> Test.C c_azG t1_azz r_azE
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azw)
                 (@ t1_azz::* -> *)
                 (@ r_azE)
                 (@ c_azG)
                 ($dUpdateString_azI [Occ=Once] :: Test.UpdateString t1_azz)
                 (f_ajD [Occ=Once] :: r_azE -> r_azE)
                 (ds_dS4 [Occ=Once!] :: Test.C t_azw t1_azz r_azE) ->
                 case ds_dS4 of _ { Test.C x_ajE [Occ=Once] ->
                 Test.C
                   @ c_azG
                   @ t1_azz
                   @ r_azE
                   (($dUpdateString_azI
                     `cast` (Test.NTCo:T:UpdateString t1_azz
                             :: Test.T:UpdateString t1_azz
                                  ~
                                (forall a_aiP. (a_aiP -> a_aiP) -> t1_azz a_aiP -> t1_azz a_aiP)))
                      @ r_azE f_ajD x_ajE)
                 }}]
Test.updateStringfC =
  \ (@ t_azw)
    (@ t1_azz::* -> *)
    (@ r_azE)
    (@ c_azG)
    ($dUpdateString_azI :: Test.UpdateString t1_azz)
    (eta_B2 :: r_azE -> r_azE)
    (eta1_B1 :: Test.C t_azw t1_azz r_azE) ->
    case eta1_B1 of _ { Test.C x_ajE ->
    Test.C
      @ c_azG
      @ t1_azz
      @ r_azE
      (($dUpdateString_azI
        `cast` (Test.NTCo:T:UpdateString t1_azz
                :: Test.T:UpdateString t1_azz
                     ~
                   (forall a_aiP. (a_aiP -> a_aiP) -> t1_azz a_aiP -> t1_azz a_aiP)))
         @ r_azE eta_B2 x_ajE)
    }

Test.$fUpdateStringC1
  :: forall (f_ajZ :: * -> *) c_ak0.
     (Test.UpdateString f_ajZ) =>
     forall a_aPy.
     (a_aPy -> a_aPy)
     -> Test.C c_ak0 f_ajZ a_aPy
     -> Test.C c_ak0 f_ajZ a_aPy
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_ajZ::* -> *)
                 (@ c_ak0)
                 ($dUpdateString_aPt [Occ=Once] :: Test.UpdateString f_ajZ)
                 (@ a_aPy)
                 (eta_B2 [Occ=Once] :: a_aPy -> a_aPy)
                 (eta1_B1 [Occ=Once] :: Test.C c_ak0 f_ajZ a_aPy) ->
                 Test.updateStringfC
                   @ c_ak0 @ f_ajZ @ a_aPy @ c_ak0 $dUpdateString_aPt eta_B2 eta1_B1}]
Test.$fUpdateStringC1 =
  \ (@ f_ajZ::* -> *)
    (@ c_ak0)
    ($dUpdateString_aPt :: Test.UpdateString f_ajZ)
    (@ a_aPy)
    (eta_B2 :: a_aPy -> a_aPy)
    (eta1_B1 :: Test.C c_ak0 f_ajZ a_aPy) ->
    Test.updateStringfC
      @ c_ak0 @ f_ajZ @ a_aPy @ c_ak0 $dUpdateString_aPt eta_B2 eta1_B1

Test.updateStringfI [InlPrag=INLINE]
  :: forall t_aBy r_aBB.
     (t_aBy -> r_aBB) -> Test.I t_aBy -> Test.I r_aBB
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= (\ (@ t_XFb)
                  (@ r_XFf)
                  (f_ajg [Occ=Once!] :: t_XFb -> r_XFf)
                  (ds_dSh [Occ=Once] :: Test.I t_XFb) ->
                  f_ajg (ds_dSh `cast` (Test.NTCo:I t_XFb :: Test.I t_XFb ~ t_XFb)))
               `cast` (forall t_XFb r_XFf.
                       (t_XFb -> r_XFf) -> Test.I t_XFb -> sym (Test.NTCo:I r_XFf)
                       :: (forall t_XFb r_XFf. (t_XFb -> r_XFf) -> Test.I t_XFb -> r_XFf)
                            ~
                          (forall t_XFb r_XFf.
                           (t_XFb -> r_XFf) -> Test.I t_XFb -> Test.I r_XFf))}]
Test.updateStringfI =
  a1_r1ca
  `cast` (forall t_aBy r_aBB.
          (t_aBy -> r_aBB) -> Test.I t_aBy -> sym (Test.NTCo:I r_aBB)
          :: (forall t_aBy r_aBB. (t_aBy -> r_aBB) -> Test.I t_aBy -> r_aBB)
               ~
             (forall t_aBy r_aBB.
              (t_aBy -> r_aBB) -> Test.I t_aBy -> Test.I r_aBB))

Test.updateStringfK [InlPrag=INLINE]
  :: forall t_aAv t1_aAx t2_aAy r_aAB.
     t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy r_aAB
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= (\ (@ t_XEg)
                  (@ t1_XEj)
                  (@ t2_XEl)
                  (@ r_XEp)
                  _
                  (ds_dSb [Occ=Once] :: Test.K t2_XEl t1_XEj) ->
                  ds_dSb)
               `cast` (forall t_XEg t1_XEj t2_XEl r_XEp.
                       t_XEg
                       -> Test.K t2_XEl t1_XEj
                       -> trans
                            (Test.NTCo:K t2_XEl t1_XEj) (sym (Test.NTCo:K t2_XEl r_XEp))
                       :: (forall t_XEg t1_XEj t2_XEl r_XEp.
                           t_XEg -> Test.K t2_XEl t1_XEj -> Test.K t2_XEl t1_XEj)
                            ~
                          (forall t_XEg t1_XEj t2_XEl r_XEp.
                           t_XEg -> Test.K t2_XEl t1_XEj -> Test.K t2_XEl r_XEp))}]
Test.updateStringfK =
  a_r1c8
  `cast` (forall t_aAv t1_aAx t2_aAy r_aAB.
          t_aAv
          -> Test.K t2_aAy t1_aAx
          -> trans
               (Test.NTCo:K t2_aAy t1_aAx) (sym (Test.NTCo:K t2_aAy r_aAB))
          :: (forall t_aAv t1_aAx t2_aAy r_aAB.
              t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy t1_aAx)
               ~
             (forall t_aAv t1_aAx t2_aAy r_aAB.
              t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy r_aAB))

Test.updateStringfKString [InlPrag=INLINE]
  :: forall t_aAH t1_aAJ r_aAM.
     t_aAH
     -> Test.K [GHC.Types.Char] t1_aAJ
     -> Test.K [GHC.Types.Char] r_aAM
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAH)
                 (@ t1_aAJ)
                 (@ r_aAM)
                 _
                 (ds_dSc [Occ=Once] :: Test.K [GHC.Types.Char] t1_aAJ) ->
                 case ds_dSc
                      `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAJ
                              :: Test.K [GHC.Types.Char] t1_aAJ ~ [GHC.Types.Char])
                 of _ {
                   [] ->
                     (GHC.Types.[] @ GHC.Types.Char)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM);
                   : ipv_sU3 [Occ=Once] ipv1_sU4 ->
                     (GHC.Types.:
                        @ GHC.Types.Char
                        (GHC.List.last_last' @ GHC.Types.Char ipv_sU3 ipv1_sU4)
                        ipv1_sU4)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM)
                 }}]
Test.updateStringfKString =
  \ (@ t_aAH)
    (@ t1_aAJ)
    (@ r_aAM)
    _
    (eta1_X7H :: Test.K [GHC.Types.Char] t1_aAJ) ->
    case eta1_X7H
         `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAJ
                 :: Test.K [GHC.Types.Char] t1_aAJ ~ [GHC.Types.Char])
    of _ {
      [] ->
        (GHC.Types.[] @ GHC.Types.Char)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM);
      : ipv_sTG ipv1_sTH ->
        (GHC.Types.:
           @ GHC.Types.Char
           (GHC.List.last_last' @ GHC.Types.Char ipv_sTG ipv1_sTH)
           ipv1_sTH)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM)
    }

Test.updateString1
  :: forall a_XVU.
     (a_XVU -> a_XVU)
     -> (Test.:+:)
          (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          (Test.C Test.Logic_T_ Test.U
           Test.:+: (Test.C Test.Logic_F_ Test.U
                     Test.:+: (Test.C Test.Logic_Not_ Test.I
                               Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Conj_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        Test.Logic_Disj_
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          a_XVU
     -> (Test.:+:)
          (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          (Test.C Test.Logic_T_ Test.U
           Test.:+: (Test.C Test.Logic_F_ Test.U
                     Test.:+: (Test.C Test.Logic_Not_ Test.I
                               Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Conj_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        Test.Logic_Disj_
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          a_XVU
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType LS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 8] 69 2}]
Test.updateString1 =
  \ (@ a_XVU)
    (eta_X64 :: a_XVU -> a_XVU)
    (eta1_Xc7
       :: (Test.:+:)
            (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
            (Test.C Test.Logic_T_ Test.U
             Test.:+: (Test.C Test.Logic_F_ Test.U
                       Test.:+: (Test.C Test.Logic_Not_ Test.I
                                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 Test.Logic_Conj_
                                                                 (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          Test.Logic_Disj_
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
            a_XVU) ->
    case eta1_Xc7 of _ {
      Test.L x_ajx ->
        Test.L
          @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          @ (Test.C Test.Logic_T_ Test.U
             Test.:+: (Test.C Test.Logic_F_ Test.U
                       Test.:+: (Test.C Test.Logic_Not_ Test.I
                                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 Test.Logic_Conj_
                                                                 (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          Test.Logic_Disj_
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ a_XVU
          (case x_ajx of _ { Test.C x1_ajE ->
           Test.C
             @ Test.Logic_Var_
             @ (Test.K GHC.Base.String)
             @ a_XVU
             (Test.updateStringfKString
                @ (a_XVU -> a_XVU) @ a_XVU @ a_XVU eta_X64 x1_ajE)
           });
      Test.R x_ajz ->
        Test.R
          @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          @ (Test.C Test.Logic_T_ Test.U
             Test.:+: (Test.C Test.Logic_F_ Test.U
                       Test.:+: (Test.C Test.Logic_Not_ Test.I
                                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 Test.Logic_Conj_
                                                                 (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          Test.Logic_Disj_
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ a_XVU
          (case x_ajz of wild1_X60 {
             Test.L x1_ajx -> wild1_X60;
             Test.R x1_XpI ->
               Test.R
                 @ (Test.C Test.Logic_T_ Test.U)
                 @ (Test.C Test.Logic_F_ Test.U
                    Test.:+: (Test.C Test.Logic_Not_ Test.I
                              Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                  Test.:+: (Test.C
                                                              Test.Logic_Conj_
                                                              (Test.I Test.:*: Test.I)
                                                            Test.:+: Test.C
                                                                       Test.Logic_Disj_
                                                                       (Test.I Test.:*: Test.I))))))
                 @ a_XVU
                 (case x1_XpI of wild2_X5U {
                    Test.L x2_ajx -> wild2_X5U;
                    Test.R x2_XpC ->
                      Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ a_XVU
                        (case x2_XpC of _ {
                           Test.L x3_ajx ->
                             Test.L
                               @ (Test.C Test.Logic_Not_ Test.I)
                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                  Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                      Test.:+: Test.C
                                                                 Test.Logic_Disj_
                                                                 (Test.I Test.:*: Test.I))))
                               @ a_XVU
                               (case x3_ajx of _ { Test.C x4_ajE ->
                                Test.C
                                  @ Test.Logic_Not_
                                  @ Test.I
                                  @ a_XVU
                                  (Test.updateStringfI @ a_XVU @ a_XVU eta_X64 x4_ajE)
                                });
                           Test.R x3_Xnj ->
                             Test.R
                               @ (Test.C Test.Logic_Not_ Test.I)
                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                  Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                      Test.:+: Test.C
                                                                 Test.Logic_Disj_
                                                                 (Test.I Test.:*: Test.I))))
                               @ a_XVU
                               (case x3_Xnj of _ {
                                  Test.L x4_ajx ->
                                    Test.L
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ a_XVU
                                      (case x4_ajx of _ { Test.C x5_ajE ->
                                       Test.C
                                         @ Test.Logic_Impl_
                                         @ (Test.I Test.:*: Test.I)
                                         @ a_XVU
                                         (case x5_ajE of _ { Test.:*: x6_ajB y_ajC ->
                                          Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ a_XVU
                                            (Test.updateStringfI @ a_XVU @ a_XVU eta_X64 x6_ajB)
                                            (Test.updateStringfI @ a_XVU @ a_XVU eta_X64 y_ajC)
                                          })
                                       });
                                  Test.R x4_Xpa ->
                                    Test.R
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ a_XVU
                                      (case x4_Xpa of _ {
                                         Test.L x5_ajx ->
                                           Test.L
                                             @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                             @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))
                                             @ a_XVU
                                             (case x5_ajx of _ { Test.C x6_ajE ->
                                              Test.C
                                                @ Test.Logic_Equiv_
                                                @ (Test.I Test.:*: Test.I)
                                                @ a_XVU
                                                (case x6_ajE of _ { Test.:*: x7_ajB y_ajC ->
                                                 Test.:*:
                                                   @ Test.I
                                                   @ Test.I
                                                   @ a_XVU
                                                   (Test.updateStringfI
                                                      @ a_XVU @ a_XVU eta_X64 x7_ajB)
                                                   (Test.updateStringfI
                                                      @ a_XVU @ a_XVU eta_X64 y_ajC)
                                                 })
                                              });
                                         Test.R x5_Xnq ->
                                           Test.R
                                             @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                             @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))
                                             @ a_XVU
                                             (case x5_Xnq of _ {
                                                Test.L x6_ajx ->
                                                  Test.L
                                                    @ (Test.C
                                                         Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                                    @ (Test.C
                                                         Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                                    @ a_XVU
                                                    (case x6_ajx of _ { Test.C x7_ajE ->
                                                     Test.C
                                                       @ Test.Logic_Conj_
                                                       @ (Test.I Test.:*: Test.I)
                                                       @ a_XVU
                                                       (case x7_ajE of _ { Test.:*: x8_ajB y_ajC ->
                                                        Test.:*:
                                                          @ Test.I
                                                          @ Test.I
                                                          @ a_XVU
                                                          (Test.updateStringfI
                                                             @ a_XVU @ a_XVU eta_X64 x8_ajB)
                                                          (Test.updateStringfI
                                                             @ a_XVU @ a_XVU eta_X64 y_ajC)
                                                        })
                                                     });
                                                Test.R x6_XoO ->
                                                  Test.R
                                                    @ (Test.C
                                                         Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                                    @ (Test.C
                                                         Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                                    @ a_XVU
                                                    (case x6_XoO of _ { Test.C x7_ajE ->
                                                     Test.C
                                                       @ Test.Logic_Disj_
                                                       @ (Test.I Test.:*: Test.I)
                                                       @ a_XVU
                                                       (case x7_ajE of _ { Test.:*: x8_ajB y_ajC ->
                                                        Test.:*:
                                                          @ Test.I
                                                          @ Test.I
                                                          @ a_XVU
                                                          (Test.updateStringfI
                                                             @ a_XVU @ a_XVU eta_X64 x8_ajB)
                                                          (Test.updateStringfI
                                                             @ a_XVU @ a_XVU eta_X64 y_ajC)
                                                        })
                                                     })
                                              })
                                       })
                                })
                         })
                  })
           })
    }

lvl7_r1cd :: Test.C Test.Logic_F_ Test.U Test.Logic
[GblId, Caf=NoCafRefs]
lvl7_r1cd =
  Test.C @ Test.Logic_F_ @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl8_r1cf
  :: (Test.:+:)
       (Test.C Test.Logic_F_ Test.U)
       (Test.C Test.Logic_Not_ Test.I
        Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                  Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                      Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl8_r1cf =
  Test.L
    @ (Test.C Test.Logic_F_ Test.U)
    @ (Test.C Test.Logic_Not_ Test.I
       Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                 Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                           Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                     Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))))
    @ Test.Logic
    lvl7_r1cd

lvl9_r1ch
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl9_r1ch =
  Test.R
    @ (Test.C Test.Logic_T_ Test.U)
    @ (Test.C Test.Logic_F_ Test.U
       Test.:+: (Test.C Test.Logic_Not_ Test.I
                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                           Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                               Test.:+: Test.C
                                                          Test.Logic_Disj_
                                                          (Test.I Test.:*: Test.I))))))
    @ Test.Logic
    lvl8_r1cf

lvl10_r1cj
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl10_r1cj =
  Test.R
    @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
    @ (Test.C Test.Logic_T_ Test.U
       Test.:+: (Test.C Test.Logic_F_ Test.U
                 Test.:+: (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))))
    @ Test.Logic
    lvl9_r1ch

lvl11_r1cl :: Test.C Test.Logic_T_ Test.U Test.Logic
[GblId, Caf=NoCafRefs]
lvl11_r1cl =
  Test.C @ Test.Logic_T_ @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl12_r1cn
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl12_r1cn =
  Test.L
    @ (Test.C Test.Logic_T_ Test.U)
    @ (Test.C Test.Logic_F_ Test.U
       Test.:+: (Test.C Test.Logic_Not_ Test.I
                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                           Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                               Test.:+: Test.C
                                                          Test.Logic_Disj_
                                                          (Test.I Test.:*: Test.I))))))
    @ Test.Logic
    lvl11_r1cl

lvl13_r1cp
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl13_r1cp =
  Test.R
    @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
    @ (Test.C Test.Logic_T_ Test.U
       Test.:+: (Test.C Test.Logic_F_ Test.U
                 Test.:+: (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))))
    @ Test.Logic
    lvl12_r1cn

$j_r1cr
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
     -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs]
$j_r1cr =
  \ (ds_dQZ
       :: (Test.:+:)
            (Test.C Test.Logic_T_ Test.U)
            (Test.C Test.Logic_F_ Test.U
             Test.:+: (Test.C Test.Logic_Not_ Test.I
                       Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                     Test.:+: Test.C
                                                                Test.Logic_Disj_
                                                                (Test.I Test.:*: Test.I))))))
            Test.Logic) ->
    case ds_dQZ of _ {
      Test.L ds1_dRp ->
        case ds1_dRp of _ { Test.C ds2_dRq ->
        case ds2_dRq of _ { Test.U -> Test.T }
        };
      Test.R ds1_dR0 ->
        case ds1_dR0 of _ {
          Test.L ds2_dRn ->
            case ds2_dRn of _ { Test.C ds3_dRo ->
            case ds3_dRo of _ { Test.U -> Test.F }
            };
          Test.R ds2_dR1 ->
            case ds2_dR1 of _ {
              Test.L ds3_dRl ->
                case ds3_dRl of _ { Test.C ds4_dRm ->
                Test.Not
                  (ds4_dRm
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                };
              Test.R ds3_dR2 ->
                case ds3_dR2 of _ {
                  Test.L ds4_dRh ->
                    case ds4_dRh of _ { Test.C ds5_dRi ->
                    case ds5_dRi of _ { Test.:*: ds6_dRj ds7_dRk ->
                    Test.Impl
                      (ds6_dRj
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds7_dRk
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds4_dR3 ->
                    case ds4_dR3 of _ {
                      Test.L ds5_dRd ->
                        case ds5_dRd of _ { Test.C ds6_dRe ->
                        case ds6_dRe of _ { Test.:*: ds7_dRf ds8_dRg ->
                        Test.Equiv
                          (ds7_dRf
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds8_dRg
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds5_dR4 ->
                        case ds5_dR4 of _ {
                          Test.L ds6_dR9 ->
                            case ds6_dR9 of _ { Test.C ds7_dRa ->
                            case ds7_dRa of _ { Test.:*: ds8_dRb ds9_dRc ->
                            Test.Conj
                              (ds8_dRb
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds9_dRc
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds6_dR5 ->
                            case ds6_dR5 of _ { Test.C ds7_dR6 ->
                            case ds7_dR6 of _ { Test.:*: ds8_dR7 ds9_dR8 ->
                            Test.Disj
                              (ds8_dR7
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds9_dR8
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            }
                        }
                    }
                }
            }
        }
    }

Rec {
lvl14_r1ct
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId]
lvl14_r1ct =
  Test.updateString1
    @ Test.Logic Test.testLogic_updateString lvl10_r1cj

lvl15_r1cv
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId]
lvl15_r1cv =
  Test.updateString1
    @ Test.Logic Test.testLogic_updateString lvl13_r1cp

Test.testLogic_updateString :: Test.Logic -> Test.Logic
[GblId, Arity=1, Str=DmdType S]
Test.testLogic_updateString =
  \ (x_aUp :: Test.Logic) ->
    case x_aUp of _ {
      Test.Var f0_ajF ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.L
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.C
                     @ Test.Logic_Var_
                     @ (Test.K GHC.Base.String)
                     @ Test.Logic
                     (f0_ajF
                      `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                              :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.T ->
        case lvl15_r1cv of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.F ->
        case lvl14_r1ct of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.Not f0_ajG ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.L
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Not_
                              @ Test.I
                              @ Test.Logic
                              (f0_ajG
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic)))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.Impl f0_ajH f1_ajI ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.L
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.C
                                 @ Test.Logic_Impl_
                                 @ (Test.I Test.:*: Test.I)
                                 @ Test.Logic
                                 (Test.:*:
                                    @ Test.I
                                    @ Test.I
                                    @ Test.Logic
                                    (f0_ajH
                                     `cast` (sym (Test.NTCo:I Test.Logic)
                                             :: Test.Logic ~ Test.I Test.Logic))
                                    (f1_ajI
                                     `cast` (sym (Test.NTCo:I Test.Logic)
                                             :: Test.Logic ~ Test.I Test.Logic)))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.Equiv f0_ajJ f1_ajK ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.L
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.C
                                    @ Test.Logic_Equiv_
                                    @ (Test.I Test.:*: Test.I)
                                    @ Test.Logic
                                    (Test.:*:
                                       @ Test.I
                                       @ Test.I
                                       @ Test.Logic
                                       (f0_ajJ
                                        `cast` (sym (Test.NTCo:I Test.Logic)
                                                :: Test.Logic ~ Test.I Test.Logic))
                                       (f1_ajK
                                        `cast` (sym (Test.NTCo:I Test.Logic)
                                                :: Test.Logic ~ Test.I Test.Logic))))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.Conj f0_ajL f1_ajM ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.R
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.L
                                    @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                    @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                    @ Test.Logic
                                    (Test.C
                                       @ Test.Logic_Conj_
                                       @ (Test.I Test.:*: Test.I)
                                       @ Test.Logic
                                       (Test.:*:
                                          @ Test.I
                                          @ Test.I
                                          @ Test.Logic
                                          (f0_ajL
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic))
                                          (f1_ajM
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic)))))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        };
      Test.Disj f0_ajN f1_ajO ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.R
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.R
                                    @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                    @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                    @ Test.Logic
                                    (Test.C
                                       @ Test.Logic_Disj_
                                       @ (Test.I Test.:*: Test.I)
                                       @ Test.Logic
                                       (Test.:*:
                                          @ Test.I
                                          @ Test.I
                                          @ Test.Logic
                                          (f0_ajN
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic))
                                          (f1_ajO
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic)))))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j_r1cr ds_dQZ
        }
    }
end Rec }

$j1_r1cx
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
     -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs]
$j1_r1cx =
  \ (ds_dQZ
       :: (Test.:+:)
            (Test.C Test.Logic_T_ Test.U)
            (Test.C Test.Logic_F_ Test.U
             Test.:+: (Test.C Test.Logic_Not_ Test.I
                       Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                     Test.:+: Test.C
                                                                Test.Logic_Disj_
                                                                (Test.I Test.:*: Test.I))))))
            Test.Logic) ->
    case ds_dQZ of _ {
      Test.L ds1_dRp ->
        case ds1_dRp of _ { Test.C ds2_dRq ->
        case ds2_dRq of _ { Test.U -> Test.T }
        };
      Test.R ds1_dR0 ->
        case ds1_dR0 of _ {
          Test.L ds2_dRn ->
            case ds2_dRn of _ { Test.C ds3_dRo ->
            case ds3_dRo of _ { Test.U -> Test.F }
            };
          Test.R ds2_dR1 ->
            case ds2_dR1 of _ {
              Test.L ds3_dRl ->
                case ds3_dRl of _ { Test.C ds4_dRm ->
                Test.Not
                  (ds4_dRm
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                };
              Test.R ds3_dR2 ->
                case ds3_dR2 of _ {
                  Test.L ds4_dRh ->
                    case ds4_dRh of _ { Test.C ds5_dRi ->
                    case ds5_dRi of _ { Test.:*: ds6_dRj ds7_dRk ->
                    Test.Impl
                      (ds6_dRj
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds7_dRk
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds4_dR3 ->
                    case ds4_dR3 of _ {
                      Test.L ds5_dRd ->
                        case ds5_dRd of _ { Test.C ds6_dRe ->
                        case ds6_dRe of _ { Test.:*: ds7_dRf ds8_dRg ->
                        Test.Equiv
                          (ds7_dRf
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds8_dRg
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds5_dR4 ->
                        case ds5_dR4 of _ {
                          Test.L ds6_dR9 ->
                            case ds6_dR9 of _ { Test.C ds7_dRa ->
                            case ds7_dRa of _ { Test.:*: ds8_dRb ds9_dRc ->
                            Test.Conj
                              (ds8_dRb
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds9_dRc
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds6_dR5 ->
                            case ds6_dR5 of _ { Test.C ds7_dR6 ->
                            case ds7_dR6 of _ { Test.:*: ds8_dR7 ds9_dR8 ->
                            Test.Disj
                              (ds8_dR7
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds9_dR8
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            }
                        }
                    }
                }
            }
        }
    }

Rec {
lvl16_r1cz
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId]
lvl16_r1cz =
  Test.updateString1 @ Test.Logic updateString2_r1cC lvl10_r1cj

lvl17_r1cD
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId]
lvl17_r1cD =
  Test.updateString1 @ Test.Logic updateString2_r1cC lvl13_r1cp

updateString2_r1cC :: Test.Logic -> Test.Logic
[GblId, Arity=1, Str=DmdType S]
updateString2_r1cC =
  \ (x_aUp :: Test.Logic) ->
    case x_aUp of _ {
      Test.Var f0_ajF ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1cC
               (Test.L
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.C
                     @ Test.Logic_Var_
                     @ (Test.K GHC.Base.String)
                     @ Test.Logic
                     (f0_ajF
                      `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                              :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.T ->
        case lvl17_r1cD of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.F ->
        case lvl16_r1cz of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.Not f0_ajG ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1cC
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.L
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Not_
                              @ Test.I
                              @ Test.Logic
                              (f0_ajG
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic)))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.Impl f0_ajH f1_ajI ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1cC
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.L
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.C
                                 @ Test.Logic_Impl_
                                 @ (Test.I Test.:*: Test.I)
                                 @ Test.Logic
                                 (Test.:*:
                                    @ Test.I
                                    @ Test.I
                                    @ Test.Logic
                                    (f0_ajH
                                     `cast` (sym (Test.NTCo:I Test.Logic)
                                             :: Test.Logic ~ Test.I Test.Logic))
                                    (f1_ajI
                                     `cast` (sym (Test.NTCo:I Test.Logic)
                                             :: Test.Logic ~ Test.I Test.Logic)))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.Equiv f0_ajJ f1_ajK ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1cC
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.L
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.C
                                    @ Test.Logic_Equiv_
                                    @ (Test.I Test.:*: Test.I)
                                    @ Test.Logic
                                    (Test.:*:
                                       @ Test.I
                                       @ Test.I
                                       @ Test.Logic
                                       (f0_ajJ
                                        `cast` (sym (Test.NTCo:I Test.Logic)
                                                :: Test.Logic ~ Test.I Test.Logic))
                                       (f1_ajK
                                        `cast` (sym (Test.NTCo:I Test.Logic)
                                                :: Test.Logic ~ Test.I Test.Logic))))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.Conj f0_ajL f1_ajM ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1cC
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.R
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.L
                                    @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                    @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                    @ Test.Logic
                                    (Test.C
                                       @ Test.Logic_Conj_
                                       @ (Test.I Test.:*: Test.I)
                                       @ Test.Logic
                                       (Test.:*:
                                          @ Test.I
                                          @ Test.I
                                          @ Test.Logic
                                          (f0_ajL
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic))
                                          (f1_ajM
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic)))))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        };
      Test.Disj f0_ajN f1_ajO ->
        case Test.updateString1
               @ Test.Logic
               updateString2_r1cC
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.R
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.R
                                    @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                    @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                    @ Test.Logic
                                    (Test.C
                                       @ Test.Logic_Disj_
                                       @ (Test.I Test.:*: Test.I)
                                       @ Test.Logic
                                       (Test.:*:
                                          @ Test.I
                                          @ Test.I
                                          @ Test.Logic
                                          (f0_ajN
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic))
                                          (f1_ajO
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic)))))))))))
        of _ {
          Test.L ds_dRr ->
            case ds_dRr of _ { Test.C ds1_dRs ->
            Test.Var
              (ds1_dRs
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dQZ -> $j1_r1cx ds_dQZ
        }
    }
end Rec }

Test.updateString_$supdateString :: Test.Logic -> Test.Logic
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=InlineRule(sat, -)
         Tmpl= letrec {
                 updateString3_X14I [Occ=LoopBreaker] :: Test.Logic -> Test.Logic
                 [LclId, Arity=1]
                 updateString3_X14I =
                   \ (x_aUp [Occ=Once!] :: Test.Logic) ->
                     let {
                       $j2_s19X [Occ=Once*!]
                         :: (Test.:+:)
                              (Test.C Test.Logic_T_ Test.U)
                              (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I))))))
                              Test.Logic
                            -> Test.Logic
                       [LclId, Arity=1]
                       $j2_s19X =
                         \ (ds_dQZ [Occ=Once!]
                              :: (Test.:+:)
                                   (Test.C Test.Logic_T_ Test.U)
                                   (Test.C Test.Logic_F_ Test.U
                                    Test.:+: (Test.C Test.Logic_Not_ Test.I
                                              Test.:+: (Test.C
                                                          Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    Test.Logic_Equiv_
                                                                    (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              Test.Logic_Conj_
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       Test.Logic_Disj_
                                                                                       (Test.I
                                                                                        Test.:*: Test.I))))))
                                   Test.Logic) ->
                           case ds_dQZ of _ {
                             Test.L ds1_dRp [Occ=Once!] ->
                               case ds1_dRp of _ { Test.C ds2_dRq [Occ=Once!] ->
                               case ds2_dRq of _ { Test.U -> Test.T }
                               };
                             Test.R ds1_dR0 [Occ=Once!] ->
                               case ds1_dR0 of _ {
                                 Test.L ds2_dRn [Occ=Once!] ->
                                   case ds2_dRn of _ { Test.C ds3_dRo [Occ=Once!] ->
                                   case ds3_dRo of _ { Test.U -> Test.F }
                                   };
                                 Test.R ds2_dR1 [Occ=Once!] ->
                                   case ds2_dR1 of _ {
                                     Test.L ds3_dRl [Occ=Once!] ->
                                       case ds3_dRl of _ { Test.C ds4_dRm [Occ=Once] ->
                                       Test.Not
                                         (ds4_dRm
                                          `cast` (Test.NTCo:I Test.Logic
                                                  :: Test.I Test.Logic ~ Test.Logic))
                                       };
                                     Test.R ds3_dR2 [Occ=Once!] ->
                                       case ds3_dR2 of _ {
                                         Test.L ds4_dRh [Occ=Once!] ->
                                           case ds4_dRh of _ { Test.C ds5_dRi [Occ=Once!] ->
                                           case ds5_dRi
                                           of _ { Test.:*: ds6_dRj [Occ=Once] ds7_dRk [Occ=Once] ->
                                           Test.Impl
                                             (ds6_dRj
                                              `cast` (Test.NTCo:I Test.Logic
                                                      :: Test.I Test.Logic ~ Test.Logic))
                                             (ds7_dRk
                                              `cast` (Test.NTCo:I Test.Logic
                                                      :: Test.I Test.Logic ~ Test.Logic))
                                           }
                                           };
                                         Test.R ds4_dR3 [Occ=Once!] ->
                                           case ds4_dR3 of _ {
                                             Test.L ds5_dRd [Occ=Once!] ->
                                               case ds5_dRd of _ { Test.C ds6_dRe [Occ=Once!] ->
                                               case ds6_dRe
                                               of _
                                               { Test.:*: ds7_dRf [Occ=Once] ds8_dRg [Occ=Once] ->
                                               Test.Equiv
                                                 (ds7_dRf
                                                  `cast` (Test.NTCo:I Test.Logic
                                                          :: Test.I Test.Logic ~ Test.Logic))
                                                 (ds8_dRg
                                                  `cast` (Test.NTCo:I Test.Logic
                                                          :: Test.I Test.Logic ~ Test.Logic))
                                               }
                                               };
                                             Test.R ds5_dR4 [Occ=Once!] ->
                                               case ds5_dR4 of _ {
                                                 Test.L ds6_dR9 [Occ=Once!] ->
                                                   case ds6_dR9 of _ { Test.C ds7_dRa [Occ=Once!] ->
                                                   case ds7_dRa
                                                   of _
                                                   { Test.:*: ds8_dRb [Occ=Once]
                                                              ds9_dRc [Occ=Once] ->
                                                   Test.Conj
                                                     (ds8_dRb
                                                      `cast` (Test.NTCo:I Test.Logic
                                                              :: Test.I Test.Logic ~ Test.Logic))
                                                     (ds9_dRc
                                                      `cast` (Test.NTCo:I Test.Logic
                                                              :: Test.I Test.Logic ~ Test.Logic))
                                                   }
                                                   };
                                                 Test.R ds6_dR5 [Occ=Once!] ->
                                                   case ds6_dR5 of _ { Test.C ds7_dR6 [Occ=Once!] ->
                                                   case ds7_dR6
                                                   of _
                                                   { Test.:*: ds8_dR7 [Occ=Once]
                                                              ds9_dR8 [Occ=Once] ->
                                                   Test.Disj
                                                     (ds8_dR7
                                                      `cast` (Test.NTCo:I Test.Logic
                                                              :: Test.I Test.Logic ~ Test.Logic))
                                                     (ds9_dR8
                                                      `cast` (Test.NTCo:I Test.Logic
                                                              :: Test.I Test.Logic ~ Test.Logic))
                                                   }
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
                           } } in
                     case x_aUp of _ {
                       Test.Var f0_ajF [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.L
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.C
                                      @ Test.Logic_Var_
                                      @ (Test.K GHC.Base.String)
                                      @ Test.Logic
                                      (f0_ajF
                                       `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                                               :: GHC.Base.String
                                                    ~
                                                  Test.K GHC.Base.String Test.Logic))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.T ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.L
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.C
                                         @ Test.Logic_T_
                                         @ Test.U
                                         @ Test.Logic
                                         (Test.U @ Test.Logic))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.F ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.L
                                         @ (Test.C Test.Logic_F_ Test.U)
                                         @ (Test.C Test.Logic_Not_ Test.I
                                            Test.:+: (Test.C
                                                        Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Equiv_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: (Test.C
                                                                            Test.Logic_Conj_
                                                                            (Test.I Test.:*: Test.I)
                                                                          Test.:+: Test.C
                                                                                     Test.Logic_Disj_
                                                                                     (Test.I
                                                                                      Test.:*: Test.I)))))
                                         @ Test.Logic
                                         (Test.C
                                            @ Test.Logic_F_
                                            @ Test.U
                                            @ Test.Logic
                                            (Test.U @ Test.Logic)))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.Not f0_ajG [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_F_ Test.U)
                                         @ (Test.C Test.Logic_Not_ Test.I
                                            Test.:+: (Test.C
                                                        Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Equiv_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: (Test.C
                                                                            Test.Logic_Conj_
                                                                            (Test.I Test.:*: Test.I)
                                                                          Test.:+: Test.C
                                                                                     Test.Logic_Disj_
                                                                                     (Test.I
                                                                                      Test.:*: Test.I)))))
                                         @ Test.Logic
                                         (Test.L
                                            @ (Test.C Test.Logic_Not_ Test.I)
                                            @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Equiv_
                                                           (Test.I Test.:*: Test.I)
                                                         Test.:+: (Test.C
                                                                     Test.Logic_Conj_
                                                                     (Test.I Test.:*: Test.I)
                                                                   Test.:+: Test.C
                                                                              Test.Logic_Disj_
                                                                              (Test.I
                                                                               Test.:*: Test.I))))
                                            @ Test.Logic
                                            (Test.C
                                               @ Test.Logic_Not_
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajG
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic)))))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.Impl f0_ajH [Occ=Once] f1_ajI [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_F_ Test.U)
                                         @ (Test.C Test.Logic_Not_ Test.I
                                            Test.:+: (Test.C
                                                        Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Equiv_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: (Test.C
                                                                            Test.Logic_Conj_
                                                                            (Test.I Test.:*: Test.I)
                                                                          Test.:+: Test.C
                                                                                     Test.Logic_Disj_
                                                                                     (Test.I
                                                                                      Test.:*: Test.I)))))
                                         @ Test.Logic
                                         (Test.R
                                            @ (Test.C Test.Logic_Not_ Test.I)
                                            @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Equiv_
                                                           (Test.I Test.:*: Test.I)
                                                         Test.:+: (Test.C
                                                                     Test.Logic_Conj_
                                                                     (Test.I Test.:*: Test.I)
                                                                   Test.:+: Test.C
                                                                              Test.Logic_Disj_
                                                                              (Test.I
                                                                               Test.:*: Test.I))))
                                            @ Test.Logic
                                            (Test.L
                                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                               @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                  Test.:+: (Test.C
                                                              Test.Logic_Conj_
                                                              (Test.I Test.:*: Test.I)
                                                            Test.:+: Test.C
                                                                       Test.Logic_Disj_
                                                                       (Test.I Test.:*: Test.I)))
                                               @ Test.Logic
                                               (Test.C
                                                  @ Test.Logic_Impl_
                                                  @ (Test.I Test.:*: Test.I)
                                                  @ Test.Logic
                                                  (Test.:*:
                                                     @ Test.I
                                                     @ Test.I
                                                     @ Test.Logic
                                                     (f0_ajH
                                                      `cast` (sym (Test.NTCo:I Test.Logic)
                                                              :: Test.Logic ~ Test.I Test.Logic))
                                                     (f1_ajI
                                                      `cast` (sym (Test.NTCo:I Test.Logic)
                                                              :: Test.Logic
                                                                   ~
                                                                 Test.I Test.Logic)))))))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.Equiv f0_ajJ [Occ=Once] f1_ajK [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_F_ Test.U)
                                         @ (Test.C Test.Logic_Not_ Test.I
                                            Test.:+: (Test.C
                                                        Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Equiv_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: (Test.C
                                                                            Test.Logic_Conj_
                                                                            (Test.I Test.:*: Test.I)
                                                                          Test.:+: Test.C
                                                                                     Test.Logic_Disj_
                                                                                     (Test.I
                                                                                      Test.:*: Test.I)))))
                                         @ Test.Logic
                                         (Test.R
                                            @ (Test.C Test.Logic_Not_ Test.I)
                                            @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Equiv_
                                                           (Test.I Test.:*: Test.I)
                                                         Test.:+: (Test.C
                                                                     Test.Logic_Conj_
                                                                     (Test.I Test.:*: Test.I)
                                                                   Test.:+: Test.C
                                                                              Test.Logic_Disj_
                                                                              (Test.I
                                                                               Test.:*: Test.I))))
                                            @ Test.Logic
                                            (Test.R
                                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                               @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                  Test.:+: (Test.C
                                                              Test.Logic_Conj_
                                                              (Test.I Test.:*: Test.I)
                                                            Test.:+: Test.C
                                                                       Test.Logic_Disj_
                                                                       (Test.I Test.:*: Test.I)))
                                               @ Test.Logic
                                               (Test.L
                                                  @ (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                                  @ (Test.C
                                                       Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                     Test.:+: Test.C
                                                                Test.Logic_Disj_
                                                                (Test.I Test.:*: Test.I))
                                                  @ Test.Logic
                                                  (Test.C
                                                     @ Test.Logic_Equiv_
                                                     @ (Test.I Test.:*: Test.I)
                                                     @ Test.Logic
                                                     (Test.:*:
                                                        @ Test.I
                                                        @ Test.I
                                                        @ Test.Logic
                                                        (f0_ajJ
                                                         `cast` (sym (Test.NTCo:I Test.Logic)
                                                                 :: Test.Logic ~ Test.I Test.Logic))
                                                        (f1_ajK
                                                         `cast` (sym (Test.NTCo:I Test.Logic)
                                                                 :: Test.Logic
                                                                      ~
                                                                    Test.I Test.Logic))))))))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.Conj f0_ajL [Occ=Once] f1_ajM [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_F_ Test.U)
                                         @ (Test.C Test.Logic_Not_ Test.I
                                            Test.:+: (Test.C
                                                        Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Equiv_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: (Test.C
                                                                            Test.Logic_Conj_
                                                                            (Test.I Test.:*: Test.I)
                                                                          Test.:+: Test.C
                                                                                     Test.Logic_Disj_
                                                                                     (Test.I
                                                                                      Test.:*: Test.I)))))
                                         @ Test.Logic
                                         (Test.R
                                            @ (Test.C Test.Logic_Not_ Test.I)
                                            @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Equiv_
                                                           (Test.I Test.:*: Test.I)
                                                         Test.:+: (Test.C
                                                                     Test.Logic_Conj_
                                                                     (Test.I Test.:*: Test.I)
                                                                   Test.:+: Test.C
                                                                              Test.Logic_Disj_
                                                                              (Test.I
                                                                               Test.:*: Test.I))))
                                            @ Test.Logic
                                            (Test.R
                                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                               @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                  Test.:+: (Test.C
                                                              Test.Logic_Conj_
                                                              (Test.I Test.:*: Test.I)
                                                            Test.:+: Test.C
                                                                       Test.Logic_Disj_
                                                                       (Test.I Test.:*: Test.I)))
                                               @ Test.Logic
                                               (Test.R
                                                  @ (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                                  @ (Test.C
                                                       Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                     Test.:+: Test.C
                                                                Test.Logic_Disj_
                                                                (Test.I Test.:*: Test.I))
                                                  @ Test.Logic
                                                  (Test.L
                                                     @ (Test.C
                                                          Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                                     @ (Test.C
                                                          Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                                     @ Test.Logic
                                                     (Test.C
                                                        @ Test.Logic_Conj_
                                                        @ (Test.I Test.:*: Test.I)
                                                        @ Test.Logic
                                                        (Test.:*:
                                                           @ Test.I
                                                           @ Test.I
                                                           @ Test.Logic
                                                           (f0_ajL
                                                            `cast` (sym (Test.NTCo:I Test.Logic)
                                                                    :: Test.Logic
                                                                         ~
                                                                       Test.I Test.Logic))
                                                           (f1_ajM
                                                            `cast` (sym (Test.NTCo:I Test.Logic)
                                                                    :: Test.Logic
                                                                         ~
                                                                       Test.I Test.Logic)))))))))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         };
                       Test.Disj f0_ajN [Occ=Once] f1_ajO [Occ=Once] ->
                         case Test.updateString1
                                @ Test.Logic
                                updateString3_X14I
                                (Test.R
                                   @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                                   @ (Test.C Test.Logic_T_ Test.U
                                      Test.:+: (Test.C Test.Logic_F_ Test.U
                                                Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                          Test.:+: (Test.C
                                                                      Test.Logic_Impl_
                                                                      (Test.I Test.:*: Test.I)
                                                                    Test.:+: (Test.C
                                                                                Test.Logic_Equiv_
                                                                                (Test.I
                                                                                 Test.:*: Test.I)
                                                                              Test.:+: (Test.C
                                                                                          Test.Logic_Conj_
                                                                                          (Test.I
                                                                                           Test.:*: Test.I)
                                                                                        Test.:+: Test.C
                                                                                                   Test.Logic_Disj_
                                                                                                   (Test.I
                                                                                                    Test.:*: Test.I)))))))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C Test.Logic_T_ Test.U)
                                      @ (Test.C Test.Logic_F_ Test.U
                                         Test.:+: (Test.C Test.Logic_Not_ Test.I
                                                   Test.:+: (Test.C
                                                               Test.Logic_Impl_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Equiv_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: (Test.C
                                                                                   Test.Logic_Conj_
                                                                                   (Test.I
                                                                                    Test.:*: Test.I)
                                                                                 Test.:+: Test.C
                                                                                            Test.Logic_Disj_
                                                                                            (Test.I
                                                                                             Test.:*: Test.I))))))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C Test.Logic_F_ Test.U)
                                         @ (Test.C Test.Logic_Not_ Test.I
                                            Test.:+: (Test.C
                                                        Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Equiv_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: (Test.C
                                                                            Test.Logic_Conj_
                                                                            (Test.I Test.:*: Test.I)
                                                                          Test.:+: Test.C
                                                                                     Test.Logic_Disj_
                                                                                     (Test.I
                                                                                      Test.:*: Test.I)))))
                                         @ Test.Logic
                                         (Test.R
                                            @ (Test.C Test.Logic_Not_ Test.I)
                                            @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Equiv_
                                                           (Test.I Test.:*: Test.I)
                                                         Test.:+: (Test.C
                                                                     Test.Logic_Conj_
                                                                     (Test.I Test.:*: Test.I)
                                                                   Test.:+: Test.C
                                                                              Test.Logic_Disj_
                                                                              (Test.I
                                                                               Test.:*: Test.I))))
                                            @ Test.Logic
                                            (Test.R
                                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                               @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                  Test.:+: (Test.C
                                                              Test.Logic_Conj_
                                                              (Test.I Test.:*: Test.I)
                                                            Test.:+: Test.C
                                                                       Test.Logic_Disj_
                                                                       (Test.I Test.:*: Test.I)))
                                               @ Test.Logic
                                               (Test.R
                                                  @ (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                                  @ (Test.C
                                                       Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                     Test.:+: Test.C
                                                                Test.Logic_Disj_
                                                                (Test.I Test.:*: Test.I))
                                                  @ Test.Logic
                                                  (Test.R
                                                     @ (Test.C
                                                          Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                                     @ (Test.C
                                                          Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                                     @ Test.Logic
                                                     (Test.C
                                                        @ Test.Logic_Disj_
                                                        @ (Test.I Test.:*: Test.I)
                                                        @ Test.Logic
                                                        (Test.:*:
                                                           @ Test.I
                                                           @ Test.I
                                                           @ Test.Logic
                                                           (f0_ajN
                                                            `cast` (sym (Test.NTCo:I Test.Logic)
                                                                    :: Test.Logic
                                                                         ~
                                                                       Test.I Test.Logic))
                                                           (f1_ajO
                                                            `cast` (sym (Test.NTCo:I Test.Logic)
                                                                    :: Test.Logic
                                                                         ~
                                                                       Test.I Test.Logic)))))))))))
                         of _ {
                           Test.L ds_dRr [Occ=Once!] ->
                             case ds_dRr of _ { Test.C ds1_dRs [Occ=Once] ->
                             Test.Var
                               (ds1_dRs
                                `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                        :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                             };
                           Test.R ds_dQZ [Occ=Once] -> $j2_s19X ds_dQZ
                         }
                     }; } in
               updateString3_X14I}]
Test.updateString_$supdateString = updateString2_r1cC

Test.updateStringfU [InlPrag=INLINE]
  :: forall t_aBG a_aBL. t_aBG -> a_aBL -> a_aBL
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= \ (@ t_aBG) (@ a_aBL) _ -> GHC.Base.id @ a_aBL}]
Test.updateStringfU =
  \ (@ t_aBG) (@ a_aBL) _ (eta1_B1 :: a_aBL) -> eta1_B1

Test.logic :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic = Test.Impl Test.logic4 Test.logic1

Test.testLogic :: Test.Logic
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testLogic = Test.testLogic_updateString Test.logic

Test.$fRegularLogic [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Logic
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
                                   :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                                        ~
                                      (Test.Logic -> Test.PF Test.Logic Test.Logic))),
                          ((Test.toLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
                                   :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                                        ~
                                      (Test.PF Test.Logic Test.Logic -> Test.Logic)))]]
Test.$fRegularLogic =
  Test.D:Regular
    @ Test.Logic
    ((Test.fromLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
             :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                  ~
                (Test.Logic -> Test.PF Test.Logic Test.Logic)))
    ((Test.toLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
             :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                  ~
                (Test.PF Test.Logic Test.Logic -> Test.Logic)))

Test.$fUpdateString:*:
  :: forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
     (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
     Test.UpdateString (f_ak1 Test.:*: g_ak2)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateString:*:1
               `cast` (forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
                       (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
                       sym (Test.NTCo:T:UpdateString (f_ak1 Test.:*: g_ak2))
                       :: (forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
                           (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
                           forall a_aiP.
                           (a_aiP -> a_aiP)
                           -> (Test.:*:) f_ak1 g_ak2 a_aiP
                           -> (Test.:*:) f_ak1 g_ak2 a_aiP)
                            ~
                          (forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
                           (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
                           Test.T:UpdateString (f_ak1 Test.:*: g_ak2)))}]
Test.$fUpdateString:*: =
  Test.$fUpdateString:*:1
  `cast` (forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
          (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
          sym (Test.NTCo:T:UpdateString (f_ak1 Test.:*: g_ak2))
          :: (forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
              (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
              forall a_aiP.
              (a_aiP -> a_aiP)
              -> (Test.:*:) f_ak1 g_ak2 a_aiP
              -> (Test.:*:) f_ak1 g_ak2 a_aiP)
               ~
             (forall (f_ak1 :: * -> *) (g_ak2 :: * -> *).
              (Test.UpdateString f_ak1, Test.UpdateString g_ak2) =>
              Test.T:UpdateString (f_ak1 Test.:*: g_ak2)))

Test.$fUpdateString:+:
  :: forall (f_ak3 :: * -> *) (g_ak4 :: * -> *).
     (Test.UpdateString f_ak3, Test.UpdateString g_ak4) =>
     Test.UpdateString (f_ak3 Test.:+: g_ak4)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateString:+: =
  Test.$fUpdateString:+:1
  `cast` (forall (f_ak3 :: * -> *) (g_ak4 :: * -> *).
          (Test.UpdateString f_ak3, Test.UpdateString g_ak4) =>
          sym (Test.NTCo:T:UpdateString (f_ak3 Test.:+: g_ak4))
          :: (forall (f_ak3 :: * -> *) (g_ak4 :: * -> *).
              (Test.UpdateString f_ak3, Test.UpdateString g_ak4) =>
              forall a_aiP.
              (a_aiP -> a_aiP)
              -> (Test.:+:) f_ak3 g_ak4 a_aiP
              -> (Test.:+:) f_ak3 g_ak4 a_aiP)
               ~
             (forall (f_ak3 :: * -> *) (g_ak4 :: * -> *).
              (Test.UpdateString f_ak3, Test.UpdateString g_ak4) =>
              Test.T:UpdateString (f_ak3 Test.:+: g_ak4)))

Test.$fUpdateStringC
  :: forall (f_ajZ :: * -> *) c_ak0.
     (Test.UpdateString f_ajZ) =>
     Test.UpdateString (Test.C c_ak0 f_ajZ)
[GblId[DFunId(newtype)],
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateStringC1
               `cast` (forall (f_ajZ :: * -> *) c_ak0.
                       (Test.UpdateString f_ajZ) =>
                       sym (Test.NTCo:T:UpdateString (Test.C c_ak0 f_ajZ))
                       :: (forall (f_ajZ :: * -> *) c_ak0.
                           (Test.UpdateString f_ajZ) =>
                           forall a_aiP.
                           (a_aiP -> a_aiP)
                           -> Test.C c_ak0 f_ajZ a_aiP
                           -> Test.C c_ak0 f_ajZ a_aiP)
                            ~
                          (forall (f_ajZ :: * -> *) c_ak0.
                           (Test.UpdateString f_ajZ) =>
                           Test.T:UpdateString (Test.C c_ak0 f_ajZ)))}]
Test.$fUpdateStringC =
  Test.$fUpdateStringC1
  `cast` (forall (f_ajZ :: * -> *) c_ak0.
          (Test.UpdateString f_ajZ) =>
          sym (Test.NTCo:T:UpdateString (Test.C c_ak0 f_ajZ))
          :: (forall (f_ajZ :: * -> *) c_ak0.
              (Test.UpdateString f_ajZ) =>
              forall a_aiP.
              (a_aiP -> a_aiP)
              -> Test.C c_ak0 f_ajZ a_aiP
              -> Test.C c_ak0 f_ajZ a_aiP)
               ~
             (forall (f_ajZ :: * -> *) c_ak0.
              (Test.UpdateString f_ajZ) =>
              Test.T:UpdateString (Test.C c_ak0 f_ajZ)))

Test.$fUpdateStringI :: Test.UpdateString Test.I
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateStringI =
  (\ (@ a_aQe) -> Test.updateStringfI @ a_aQe @ a_aQe)
  `cast` (sym (Test.NTCo:T:UpdateString Test.I)
          :: (forall a_aiP. (a_aiP -> a_aiP) -> Test.I a_aiP -> Test.I a_aiP)
               ~
             Test.T:UpdateString Test.I)

Test.$fUpdateStringK
  :: forall x_ak5. Test.UpdateString (Test.K x_ak5)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ x_ak5) (@ a_aQ2) ->
                  Test.updateStringfK @ (a_aQ2 -> a_aQ2) @ a_aQ2 @ x_ak5 @ a_aQ2)
               `cast` (forall x_ak5. sym (Test.NTCo:T:UpdateString (Test.K x_ak5))
                       :: (forall x_ak5 a_aiP.
                           (a_aiP -> a_aiP) -> Test.K x_ak5 a_aiP -> Test.K x_ak5 a_aiP)
                            ~
                          (forall x_ak5. Test.T:UpdateString (Test.K x_ak5)))}]
Test.$fUpdateStringK =
  (\ (@ x_ak5) (@ a_aQ2) ->
     Test.updateStringfK @ (a_aQ2 -> a_aQ2) @ a_aQ2 @ x_ak5 @ a_aQ2)
  `cast` (forall x_ak5. sym (Test.NTCo:T:UpdateString (Test.K x_ak5))
          :: (forall x_ak5 a_aiP.
              (a_aiP -> a_aiP) -> Test.K x_ak5 a_aiP -> Test.K x_ak5 a_aiP)
               ~
             (forall x_ak5. Test.T:UpdateString (Test.K x_ak5)))

Test.$fUpdateStringK0 :: Test.UpdateString (Test.K GHC.Base.String)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQ8) ->
                  Test.updateStringfKString @ (a_aQ8 -> a_aQ8) @ a_aQ8 @ a_aQ8)
               `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
                       :: (forall a_aiP.
                           (a_aiP -> a_aiP)
                           -> Test.K GHC.Base.String a_aiP
                           -> Test.K GHC.Base.String a_aiP)
                            ~
                          Test.T:UpdateString (Test.K GHC.Base.String))}]
Test.$fUpdateStringK0 =
  (\ (@ a_aQ8) ->
     Test.updateStringfKString @ (a_aQ8 -> a_aQ8) @ a_aQ8 @ a_aQ8)
  `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
          :: (forall a_aiP.
              (a_aiP -> a_aiP)
              -> Test.K GHC.Base.String a_aiP
              -> Test.K GHC.Base.String a_aiP)
               ~
             Test.T:UpdateString (Test.K GHC.Base.String))

Test.$fUpdateStringU :: Test.UpdateString Test.U
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQk) ->
                  Test.updateStringfU @ (a_aQk -> a_aQk) @ (Test.U a_aQk))
               `cast` (sym (Test.NTCo:T:UpdateString Test.U)
                       :: (forall a_aiP. (a_aiP -> a_aiP) -> Test.U a_aiP -> Test.U a_aiP)
                            ~
                          Test.T:UpdateString Test.U)}]
Test.$fUpdateStringU =
  (\ (@ a_aQk) ->
     Test.updateStringfU @ (a_aQk -> a_aQk) @ (Test.U a_aQk))
  `cast` (sym (Test.NTCo:T:UpdateString Test.U)
          :: (forall a_aiP. (a_aiP -> a_aiP) -> Test.U a_aiP -> Test.U a_aiP)
               ~
             Test.T:UpdateString Test.U)

Test.$dmconFixity
  :: forall c_aiS.
     (Test.Constructor c_aiS) =>
     forall (t_aiW :: * -> (* -> *) -> * -> *) (f_aiX :: * -> *) r_aiY.
     t_aiW c_aiS f_aiX r_aiY -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aiS)
                 _
                 (@ t_aBR::* -> (* -> *) -> * -> *)
                 (@ f_aBS::* -> *)
                 (@ r_aBT)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity =
  \ (@ c_aiS)
    _
    (@ t_aBR::* -> (* -> *) -> * -> *)
    (@ f_aBS::* -> *)
    (@ r_aBT)
    _ ->
    Test.Prefix

Test.$dmconIsRecord
  :: forall c_aiS.
     (Test.Constructor c_aiS) =>
     forall (t_aiZ :: * -> (* -> *) -> * -> *) (f_aj0 :: * -> *) r_aj1.
     t_aiZ c_aiS f_aj0 r_aj1 -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aiS)
                 _
                 (@ t_aBZ::* -> (* -> *) -> * -> *)
                 (@ f_aC0::* -> *)
                 (@ r_aC1)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord =
  \ (@ c_aiS)
    _
    (@ t_aBZ::* -> (* -> *) -> * -> *)
    (@ f_aC0::* -> *)
    (@ r_aC1)
    _ ->
    GHC.Bool.False

Test.updateString [InlPrag=INLINE]
  :: forall a_ajf.
     (Test.Regular a_ajf, Test.UpdateString (Test.PF a_ajf)) =>
     a_ajf -> a_ajf
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ a_ay9)
                 ($dRegular_ayB :: Test.Regular a_ay9)
                 ($dUpdateString_ayC :: Test.UpdateString (Test.PF a_ay9)) ->
                 letrec {
                   sub_aQR [Occ=OnceL!] :: Test.PF a_ay9 a_ay9 -> Test.PF a_ay9 a_ay9
                   [LclId]
                   sub_aQR =
                     ($dUpdateString_ayC
                      `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay9)
                              :: Test.T:UpdateString (Test.PF a_ay9)
                                   ~
                                 (forall a_aiP.
                                  (a_aiP -> a_aiP) -> Test.PF a_ay9 a_aiP -> Test.PF a_ay9 a_aiP)))
                       @ a_ay9 updateString3_aya;
                   updateString3_aya [Occ=LoopBreaker] :: a_ay9 -> a_ay9
                   [LclId, Arity=1]
                   updateString3_aya =
                     \ (x_aUp [Occ=Once] :: a_ay9) ->
                       Test.to
                         @ a_ay9
                         $dRegular_ayB
                         (sub_aQR (Test.from @ a_ay9 $dRegular_ayB x_aUp)); } in
                 updateString3_aya}]
Test.updateString =
  \ (@ a_ay9)
    ($dRegular_ayB :: Test.Regular a_ay9)
    ($dUpdateString_ayC :: Test.UpdateString (Test.PF a_ay9)) ->
    letrec {
      sub_s1aP :: Test.PF a_ay9 a_ay9 -> Test.PF a_ay9 a_ay9
      [LclId, Str=DmdType]
      sub_s1aP =
        ($dUpdateString_ayC
         `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay9)
                 :: Test.T:UpdateString (Test.PF a_ay9)
                      ~
                    (forall a_aiP.
                     (a_aiP -> a_aiP) -> Test.PF a_ay9 a_aiP -> Test.PF a_ay9 a_aiP)))
          @ a_ay9 updateString3_s1aQ;
      updateString3_s1aQ [Occ=LoopBreaker] :: a_ay9 -> a_ay9
      [LclId, Arity=1, Str=DmdType L]
      updateString3_s1aQ =
        \ (x_aUp :: a_ay9) ->
          Test.to
            @ a_ay9
            $dRegular_ayB
            (sub_s1aP (Test.from @ a_ay9 $dRegular_ayB x_aUp)); } in
    updateString3_s1aQ

Test.$fConstructorLogic_Var__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_Var_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aPl::* -> (* -> *) -> * -> *)
                 (@ f_aPm::* -> *)
                 (@ r_aPn)
                 _ ->
                 Test.$fConstructorLogic_Var_1}]
Test.$fConstructorLogic_Var__$cconName =
  \ (@ t_aPl::* -> (* -> *) -> * -> *)
    (@ f_aPm::* -> *)
    (@ r_aPn)
    _ ->
    Test.$fConstructorLogic_Var_1

Test.$fConstructorLogic_Var_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Var_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Var__$cconName,
                              Test.$dmconFixity_$s$dmconFixity,
                              Test.$dmconIsRecord_$s$dmconIsRecord]]
Test.$fConstructorLogic_Var_ =
  Test.D:Constructor
    @ Test.Logic_Var_
    Test.$fConstructorLogic_Var__$cconName
    Test.$dmconFixity_$s$dmconFixity
    Test.$dmconIsRecord_$s$dmconIsRecord

Test.$fConstructorLogic_Impl__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_Impl_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aP9::* -> (* -> *) -> * -> *)
                 (@ f_aPa::* -> *)
                 (@ r_aPb)
                 _ ->
                 Test.$fConstructorLogic_Impl_1}]
Test.$fConstructorLogic_Impl__$cconName =
  \ (@ t_aP9::* -> (* -> *) -> * -> *)
    (@ f_aPa::* -> *)
    (@ r_aPb)
    _ ->
    Test.$fConstructorLogic_Impl_1

Test.$fConstructorLogic_Impl_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Impl_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Impl__$cconName,
                              Test.$dmconFixity_$s$dmconFixity1,
                              Test.$dmconIsRecord_$s$dmconIsRecord1]]
Test.$fConstructorLogic_Impl_ =
  Test.D:Constructor
    @ Test.Logic_Impl_
    Test.$fConstructorLogic_Impl__$cconName
    Test.$dmconFixity_$s$dmconFixity1
    Test.$dmconIsRecord_$s$dmconIsRecord1

Test.$fConstructorLogic_Equiv__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_Equiv_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOX::* -> (* -> *) -> * -> *)
                 (@ f_aOY::* -> *)
                 (@ r_aOZ)
                 _ ->
                 Test.$fConstructorLogic_Equiv_1}]
Test.$fConstructorLogic_Equiv__$cconName =
  \ (@ t_aOX::* -> (* -> *) -> * -> *)
    (@ f_aOY::* -> *)
    (@ r_aOZ)
    _ ->
    Test.$fConstructorLogic_Equiv_1

Test.$fConstructorLogic_Equiv_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Equiv_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Equiv__$cconName,
                              Test.$dmconFixity_$s$dmconFixity2,
                              Test.$dmconIsRecord_$s$dmconIsRecord2]]
Test.$fConstructorLogic_Equiv_ =
  Test.D:Constructor
    @ Test.Logic_Equiv_
    Test.$fConstructorLogic_Equiv__$cconName
    Test.$dmconFixity_$s$dmconFixity2
    Test.$dmconIsRecord_$s$dmconIsRecord2

Test.$fConstructorLogic_Conj__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_Conj_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOL::* -> (* -> *) -> * -> *)
                 (@ f_aOM::* -> *)
                 (@ r_aON)
                 _ ->
                 Test.$fConstructorLogic_Conj_1}]
Test.$fConstructorLogic_Conj__$cconName =
  \ (@ t_aOL::* -> (* -> *) -> * -> *)
    (@ f_aOM::* -> *)
    (@ r_aON)
    _ ->
    Test.$fConstructorLogic_Conj_1

Test.$fConstructorLogic_Conj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Conj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Conj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity3,
                              Test.$dmconIsRecord_$s$dmconIsRecord3]]
Test.$fConstructorLogic_Conj_ =
  Test.D:Constructor
    @ Test.Logic_Conj_
    Test.$fConstructorLogic_Conj__$cconName
    Test.$dmconFixity_$s$dmconFixity3
    Test.$dmconIsRecord_$s$dmconIsRecord3

Test.$fConstructorLogic_Disj__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_Disj_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOz::* -> (* -> *) -> * -> *)
                 (@ f_aOA::* -> *)
                 (@ r_aOB)
                 _ ->
                 Test.$fConstructorLogic_Disj_1}]
Test.$fConstructorLogic_Disj__$cconName =
  \ (@ t_aOz::* -> (* -> *) -> * -> *)
    (@ f_aOA::* -> *)
    (@ r_aOB)
    _ ->
    Test.$fConstructorLogic_Disj_1

Test.$fConstructorLogic_Disj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Disj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Disj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity4,
                              Test.$dmconIsRecord_$s$dmconIsRecord4]]
Test.$fConstructorLogic_Disj_ =
  Test.D:Constructor
    @ Test.Logic_Disj_
    Test.$fConstructorLogic_Disj__$cconName
    Test.$dmconFixity_$s$dmconFixity4
    Test.$dmconIsRecord_$s$dmconIsRecord4

Test.$fConstructorLogic_Not__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_Not_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOn::* -> (* -> *) -> * -> *)
                 (@ f_aOo::* -> *)
                 (@ r_aOp)
                 _ ->
                 Test.$fConstructorLogic_Not_1}]
Test.$fConstructorLogic_Not__$cconName =
  \ (@ t_aOn::* -> (* -> *) -> * -> *)
    (@ f_aOo::* -> *)
    (@ r_aOp)
    _ ->
    Test.$fConstructorLogic_Not_1

Test.$fConstructorLogic_Not_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Not_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Not__$cconName,
                              Test.$dmconFixity_$s$dmconFixity5,
                              Test.$dmconIsRecord_$s$dmconIsRecord5]]
Test.$fConstructorLogic_Not_ =
  Test.D:Constructor
    @ Test.Logic_Not_
    Test.$fConstructorLogic_Not__$cconName
    Test.$dmconFixity_$s$dmconFixity5
    Test.$dmconIsRecord_$s$dmconIsRecord5

Test.$fConstructorLogic_T__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_T_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOb::* -> (* -> *) -> * -> *)
                 (@ f_aOc::* -> *)
                 (@ r_aOd)
                 _ ->
                 Test.$fConstructorLogic_T_1}]
Test.$fConstructorLogic_T__$cconName =
  \ (@ t_aOb::* -> (* -> *) -> * -> *)
    (@ f_aOc::* -> *)
    (@ r_aOd)
    _ ->
    Test.$fConstructorLogic_T_1

Test.$fConstructorLogic_T_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_T_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_T__$cconName,
                              Test.$dmconFixity_$s$dmconFixity6,
                              Test.$dmconIsRecord_$s$dmconIsRecord6]]
Test.$fConstructorLogic_T_ =
  Test.D:Constructor
    @ Test.Logic_T_
    Test.$fConstructorLogic_T__$cconName
    Test.$dmconFixity_$s$dmconFixity6
    Test.$dmconIsRecord_$s$dmconIsRecord6

Test.$fConstructorLogic_F__$cconName
  :: forall (t_aiT :: * -> (* -> *) -> * -> *)
            (f_aiU :: * -> *)
            r_aiV.
     t_aiT Test.Logic_F_ f_aiU r_aiV -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aNZ::* -> (* -> *) -> * -> *)
                 (@ f_aO0::* -> *)
                 (@ r_aO1)
                 _ ->
                 Test.$fConstructorLogic_F_1}]
Test.$fConstructorLogic_F__$cconName =
  \ (@ t_aNZ::* -> (* -> *) -> * -> *)
    (@ f_aO0::* -> *)
    (@ r_aO1)
    _ ->
    Test.$fConstructorLogic_F_1

Test.$fConstructorLogic_F_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_F_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_F__$cconName,
                              Test.$dmconFixity_$s$dmconFixity7,
                              Test.$dmconIsRecord_$s$dmconIsRecord7]]
Test.$fConstructorLogic_F_ =
  Test.D:Constructor
    @ Test.Logic_F_
    Test.$fConstructorLogic_F__$cconName
    Test.$dmconFixity_$s$dmconFixity7
    Test.$dmconIsRecord_$s$dmconIsRecord7

Test.$fReadFixity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Fixity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run @ [Test.Fixity] Test.$fReadFixity9

Test.$fReadFixity_$creadsPrec
  :: GHC.Types.Int -> Text.ParserCombinators.ReadP.ReadS Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 6 6}]
Test.$fReadFixity_$creadsPrec =
  \ (eta_aXG :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Fixity
      (((GHC.Read.$dmreadsPrec3
           @ Test.Fixity
           (Test.$fReadFixity3
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_aWB.
                           (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
                           -> Text.ParserCombinators.ReadP.P b_aWB)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
           eta_aXG)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity
                :: Text.ParserCombinators.ReadP.ReadP Test.Fixity
                     ~
                   (forall b_aWB.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWB)
                    -> Text.ParserCombinators.ReadP.P b_aWB)))
         @ Test.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ Test.Fixity))

Test.$fReadFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadFixity_$creadsPrec,
                           Test.$fReadFixity_readListDefault,
                           (Test.$fReadFixity2
                            `cast` (right
                                      (inst
                                         (forall a_aXm.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXm
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXm))
                                         Test.Fixity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity)),
                           (Test.$fReadFixity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))]]
Test.$fReadFixity =
  GHC.Read.D:Read
    @ Test.Fixity
    Test.$fReadFixity_$creadsPrec
    Test.$fReadFixity_readListDefault
    (Test.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
    (Test.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))

Test.$fOrdFixity_$ccompare
  :: Test.Fixity -> Test.Fixity -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [6 6] 33 0}]
Test.$fOrdFixity_$ccompare =
  \ (a2_atT :: Test.Fixity) (b_atU :: Test.Fixity) ->
    let {
      $j2_s1aU :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      [LclId, Arity=1, Str=DmdType L]
      $j2_s1aU =
        \ (a#_au0 :: GHC.Prim.Int#) ->
          let {
            $j3_s1aS :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            [LclId, Arity=1, Str=DmdType L]
            $j3_s1aS =
              \ (b#_au1 :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_au0 b#_au1 of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_au0 b#_au1 of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a2_atT of _ {
                      Test.Prefix -> GHC.Ordering.EQ;
                      Test.Infix a11_atW a21_atX ->
                        case b_atU of _ {
                          Test.Prefix -> GHC.Ordering.EQ;
                          Test.Infix b1_atY b2_atZ ->
                            case a11_atW of _ {
                              Test.LeftAssociative ->
                                case b1_atY of _ {
                                  Test.LeftAssociative -> GHC.Base.compareInt a21_atX b2_atZ;
                                  Test.RightAssociative -> GHC.Ordering.LT;
                                  Test.NotAssociative -> GHC.Ordering.LT
                                };
                              Test.RightAssociative ->
                                case b1_atY of _ {
                                  Test.LeftAssociative -> GHC.Ordering.GT;
                                  Test.RightAssociative -> GHC.Base.compareInt a21_atX b2_atZ;
                                  Test.NotAssociative -> GHC.Ordering.LT
                                };
                              Test.NotAssociative ->
                                case b1_atY of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  Test.NotAssociative -> GHC.Base.compareInt a21_atX b2_atZ
                                }
                            }
                        }
                    }
                } } in
          case b_atU of _ {
            Test.Prefix -> $j3_s1aS 0; Test.Infix _ _ -> $j3_s1aS 1
          } } in
    case a2_atT of _ {
      Test.Prefix -> $j2_s1aU 0; Test.Infix _ _ -> $j2_s1aU 1
    }

Test.$fOrdFixity_$c< :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c< =
  \ (x_aZC :: Test.Fixity) (y_aZD :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZC y_aZD of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

Test.$fOrdFixity_$c>=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c>= =
  \ (x_aZv :: Test.Fixity) (y_aZw :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZv y_aZw of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

Test.$fOrdFixity_$c> :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c> =
  \ (x_aZo :: Test.Fixity) (y_aZp :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZo y_aZp of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

Test.$fOrdFixity_$c<=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c<= =
  \ (x_aZh :: Test.Fixity) (y_aZi :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZh y_aZi of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

Test.$fOrdFixity_$cmax :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 0}]
Test.$fOrdFixity_$cmax =
  \ (x_aZ8 :: Test.Fixity) (y_aZ9 :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZ8 y_aZ9 of _ {
      __DEFAULT -> y_aZ9; GHC.Ordering.GT -> x_aZ8
    }

Test.$fOrdFixity_$cmin :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 0}]
Test.$fOrdFixity_$cmin =
  \ (x_aYZ :: Test.Fixity) (y_aZ0 :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aYZ y_aZ0 of _ {
      __DEFAULT -> x_aYZ; GHC.Ordering.GT -> y_aZ0
    }

Test.$fShowFixity_$cshowsPrec
  :: GHC.Types.Int -> Test.Fixity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType LSL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 5 0] 46 3}]
Test.$fShowFixity_$cshowsPrec =
  \ (ds_dSQ :: GHC.Types.Int)
    (ds1_dSR :: Test.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_dSR of _ {
      Test.Prefix ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadFixity6 eta_B1;
      Test.Infix b1_atR b2_atS ->
        case ds_dSQ of _ { GHC.Types.I# x_aX5 ->
        let {
          p_s1aW :: GHC.Show.ShowS
          [LclId, Arity=1, Str=DmdType L]
          p_s1aW =
            \ (x1_X130 :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                Test.$fShowFixity2
                (case b1_atR of _ {
                   Test.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a18l ->
                           GHC.Show.$wshowSignedInt 11 ww_a18l x1_X130
                           }));
                   Test.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a18l ->
                           GHC.Show.$wshowSignedInt 11 ww_a18l x1_X130
                           }));
                   Test.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a18l ->
                           GHC.Show.$wshowSignedInt 11 ww_a18l x1_X130
                           }))
                 }) } in
        case GHC.Prim.>=# x_aX5 11 of _ {
          GHC.Bool.False -> p_s1aW eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.$dmshow6
              (p_s1aW (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 eta_B1))
        }
        }
    }

Test.$fShowFixity_$cshow :: Test.Fixity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 4 0}]
Test.$fShowFixity_$cshow =
  \ (x_X178 :: Test.Fixity) ->
    Test.$fShowFixity_$cshowsPrec
      GHC.Base.zeroInt x_X178 (GHC.Types.[] @ GHC.Types.Char)

Test.$fShowFixity_$cshowList :: [Test.Fixity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 25 3}]
Test.$fShowFixity_$cshowList =
  \ (ds1_aYf :: [Test.Fixity]) (s_aYg :: GHC.Base.String) ->
    case ds1_aYf of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYg;
      : x_aYl xs_aYm ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (Test.$fShowFixity_$cshowsPrec
             Test.$fShowFixity1
             x_aYl
             (let {
                lvl141_s1aY :: [GHC.Types.Char]
                [LclId, Str=DmdType]
                lvl141_s1aY =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYg } in
              letrec {
                showl_s1b0 [Occ=LoopBreaker] :: [Test.Fixity] -> [GHC.Types.Char]
                [LclId, Arity=1, Str=DmdType S]
                showl_s1b0 =
                  \ (ds2_aYq :: [Test.Fixity]) ->
                    case ds2_aYq of _ {
                      [] -> lvl141_s1aY;
                      : y_aYv ys_aYw ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (Test.$fShowFixity_$cshowsPrec
                             Test.$fShowFixity1 y_aYv (showl_s1b0 ys_aYw))
                    }; } in
              showl_s1b0 xs_aYm))
    }

Test.$fShowFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowFixity_$cshowsPrec,
                           Test.$fShowFixity_$cshow, Test.$fShowFixity_$cshowList]]
Test.$fShowFixity =
  GHC.Show.D:Show
    @ Test.Fixity
    Test.$fShowFixity_$cshowsPrec
    Test.$fShowFixity_$cshow
    Test.$fShowFixity_$cshowList

Test.$fEqFixity_$c== :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 4}]
Test.$fEqFixity_$c== =
  \ (ds_dSK :: Test.Fixity) (ds1_dSL :: Test.Fixity) ->
    case ds_dSK of _ {
      Test.Prefix ->
        case ds1_dSL of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix a11_atG a2_atH ->
        case ds1_dSL of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix b1_atI b2_atJ ->
            case a11_atG of _ {
              Test.LeftAssociative ->
                case b1_atI of _ {
                  Test.LeftAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj -> GHC.Prim.==# x_aXf y_aXj }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj -> GHC.Prim.==# x_aXf y_aXj }
                    }
                };
              Test.NotAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXf ->
                    case b2_atJ of _ { GHC.Types.I# y_aXj -> GHC.Prim.==# x_aXf y_aXj }
                    }
                }
            }
        }
    }

Test.$fEqFixity_$c/= :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fEqFixity_$c/= =
  \ (a2_atO :: Test.Fixity) (b_atP :: Test.Fixity) ->
    case Test.$fEqFixity_$c== a2_atO b_atP of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

Test.$fEqFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqFixity_$c==,
                            Test.$fEqFixity_$c/=]]
Test.$fEqFixity =
  GHC.Classes.D:Eq
    @ Test.Fixity Test.$fEqFixity_$c== Test.$fEqFixity_$c/=

Test.$fOrdFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqFixity,
                             Test.$fOrdFixity_$ccompare, Test.$fOrdFixity_$c<,
                             Test.$fOrdFixity_$c>=, Test.$fOrdFixity_$c>, Test.$fOrdFixity_$c<=,
                             Test.$fOrdFixity_$cmax, Test.$fOrdFixity_$cmin]]
Test.$fOrdFixity =
  GHC.Classes.D:Ord
    @ Test.Fixity
    Test.$fEqFixity
    Test.$fOrdFixity_$ccompare
    Test.$fOrdFixity_$c<
    Test.$fOrdFixity_$c>=
    Test.$fOrdFixity_$c>
    Test.$fOrdFixity_$c<=
    Test.$fOrdFixity_$cmax
    Test.$fOrdFixity_$cmin

Test.$fReadAssociativity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Associativity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [Test.Associativity] Test.$fReadAssociativity16

Test.$fReadAssociativity_$creadsPrec
  :: GHC.Types.Int
     -> Text.ParserCombinators.ReadP.ReadS Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 6 6}]
Test.$fReadAssociativity_$creadsPrec =
  \ (eta_aXG :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Associativity
      (((GHC.Read.$dmreadsPrec3
           @ Test.Associativity Test.$fReadAssociativity3 eta_aXG)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                     ~
                   (forall b_aWB.
                    (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWB)
                    -> Text.ParserCombinators.ReadP.P b_aWB)))
         @ Test.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn
            @ Test.Associativity))

Test.$fReadAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadAssociativity_$creadsPrec,
                           Test.$fReadAssociativity_readListDefault,
                           (Test.$fReadAssociativity2
                            `cast` (right
                                      (inst
                                         (forall a_aXm.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXm
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXm))
                                         Test.Associativity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         Test.Associativity)),
                           (Test.$fReadAssociativity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                         [Test.Associativity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         [Test.Associativity]))]]
Test.$fReadAssociativity =
  GHC.Read.D:Read
    @ Test.Associativity
    Test.$fReadAssociativity_$creadsPrec
    Test.$fReadAssociativity_readListDefault
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))
    (Test.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [Test.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Associativity]))

Test.$fOrdAssociativity_$cmin
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmin =
  \ (x_aYZ :: Test.Associativity) (y_aZ0 :: Test.Associativity) ->
    case x_aYZ of _ {
      Test.LeftAssociative ->
        case y_aZ0 of _ { __DEFAULT -> Test.LeftAssociative };
      Test.RightAssociative ->
        case y_aZ0 of _ {
          Test.LeftAssociative -> Test.LeftAssociative;
          Test.RightAssociative -> Test.RightAssociative;
          Test.NotAssociative -> Test.RightAssociative
        };
      Test.NotAssociative -> y_aZ0
    }

Test.$fOrdAssociativity_$cmax
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmax =
  \ (x_aZ8 :: Test.Associativity) (y_aZ9 :: Test.Associativity) ->
    case x_aZ8 of _ {
      Test.LeftAssociative -> y_aZ9;
      Test.RightAssociative ->
        case y_aZ9 of _ {
          __DEFAULT -> Test.RightAssociative;
          Test.NotAssociative -> Test.NotAssociative
        };
      Test.NotAssociative ->
        case y_aZ9 of _ { __DEFAULT -> Test.NotAssociative }
    }

Test.$fOrdAssociativity_$c<=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c<= =
  \ (x_aZh :: Test.Associativity) (y_aZi :: Test.Associativity) ->
    case x_aZh of _ {
      Test.LeftAssociative ->
        case y_aZi of _ { __DEFAULT -> GHC.Bool.True };
      Test.RightAssociative ->
        case y_aZi of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZi of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fOrdAssociativity_$c>
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c> =
  \ (x_aZo :: Test.Associativity) (y_aZp :: Test.Associativity) ->
    case x_aZo of _ {
      Test.LeftAssociative ->
        case y_aZp of _ { __DEFAULT -> GHC.Bool.False };
      Test.RightAssociative ->
        case y_aZp of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZp of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fOrdAssociativity_$c>=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c>= =
  \ (x_aZv :: Test.Associativity) (y_aZw :: Test.Associativity) ->
    case x_aZv of _ {
      Test.LeftAssociative ->
        case y_aZw of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case y_aZw of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZw of _ { __DEFAULT -> GHC.Bool.True }
    }

Test.$fOrdAssociativity_$c<
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c< =
  \ (x_aZC :: Test.Associativity) (y_aZD :: Test.Associativity) ->
    case x_aZC of _ {
      Test.LeftAssociative ->
        case y_aZD of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case y_aZD of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZD of _ { __DEFAULT -> GHC.Bool.False }
    }

Test.$fOrdAssociativity_$ccompare
  :: Test.Associativity
     -> Test.Associativity
     -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$ccompare =
  \ (a2_atB :: Test.Associativity) (b_atC :: Test.Associativity) ->
    case a2_atB of _ {
      Test.LeftAssociative ->
        case b_atC of _ {
          Test.LeftAssociative -> GHC.Ordering.EQ;
          Test.RightAssociative -> GHC.Ordering.LT;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.RightAssociative ->
        case b_atC of _ {
          Test.LeftAssociative -> GHC.Ordering.GT;
          Test.RightAssociative -> GHC.Ordering.EQ;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.NotAssociative ->
        case b_atC of _ {
          __DEFAULT -> GHC.Ordering.GT;
          Test.NotAssociative -> GHC.Ordering.EQ
        }
    }

Test.$fShowAssociativity_$cshowList
  :: [Test.Associativity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 42 3}]
Test.$fShowAssociativity_$cshowList =
  \ (ds1_aYf :: [Test.Associativity]) (s_aYg :: GHC.Base.String) ->
    case ds1_aYf of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYg;
      : x_aYl xs_aYm ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1b6 [Dmd=Just L] :: GHC.Base.String
             [LclId, Str=DmdType]
             eta_s1b6 =
               let {
                 lvl141_s1b2 :: [GHC.Types.Char]
                 [LclId, Str=DmdType]
                 lvl141_s1b2 =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYg } in
               letrec {
                 showl_s1b4 [Occ=LoopBreaker]
                   :: [Test.Associativity] -> [GHC.Types.Char]
                 [LclId, Arity=1, Str=DmdType S]
                 showl_s1b4 =
                   \ (ds2_aYq :: [Test.Associativity]) ->
                     case ds2_aYq of _ {
                       [] -> lvl141_s1b2;
                       : y_aYv ys_aYw ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_aYv of _ {
                              Test.LeftAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity15 (showl_s1b4 ys_aYw);
                              Test.RightAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity12 (showl_s1b4 ys_aYw);
                              Test.NotAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1b4 ys_aYw)
                            })
                     }; } in
               showl_s1b4 xs_aYm } in
           case x_aYl of _ {
             Test.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_s1b6;
             Test.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_s1b6;
             Test.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_s1b6
           })
    }

Test.$fShowAssociativity_$cshow
  :: Test.Associativity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0}]
Test.$fShowAssociativity_$cshow =
  \ (x_aYF :: Test.Associativity) ->
    case x_aYF of _ {
      Test.LeftAssociative -> Test.$fReadAssociativity15;
      Test.RightAssociative -> Test.$fReadAssociativity12;
      Test.NotAssociative -> Test.$fReadAssociativity9
    }

Test.$fShowAssociativity_$cshowsPrec
  :: GHC.Types.Int -> Test.Associativity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType ASL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, -)
         Tmpl= \ _
                 (ds1_dSA [Occ=Once!] :: Test.Associativity)
                 (eta_B1 [Occ=Once*] :: GHC.Base.String) ->
                 case ds1_dSA of _ {
                   Test.LeftAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a17p)
                          (c_a17q [Occ=Once] :: GHC.Types.Char -> b_a17p -> b_a17p)
                          (n_a17r [Occ=Once] :: b_a17p) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a17p c_a17q n_a17r Test.$fReadAssociativity15)
                       eta_B1;
                   Test.RightAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a17p)
                          (c_a17q [Occ=Once] :: GHC.Types.Char -> b_a17p -> b_a17p)
                          (n_a17r [Occ=Once] :: b_a17p) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a17p c_a17q n_a17r Test.$fReadAssociativity12)
                       eta_B1;
                   Test.NotAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a17p)
                          (c_a17q [Occ=Once] :: GHC.Types.Char -> b_a17p -> b_a17p)
                          (n_a17r [Occ=Once] :: b_a17p) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a17p c_a17q n_a17r Test.$fReadAssociativity9)
                       eta_B1
                 }}]
Test.$fShowAssociativity_$cshowsPrec =
  \ _ (ds1_dSA :: Test.Associativity) (eta_B1 :: GHC.Base.String) ->
    case ds1_dSA of _ {
      Test.LeftAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_B1;
      Test.RightAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_B1;
      Test.NotAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_B1
    }

Test.$fShowAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowAssociativity_$cshowsPrec,
                           Test.$fShowAssociativity_$cshow,
                           Test.$fShowAssociativity_$cshowList]]
Test.$fShowAssociativity =
  GHC.Show.D:Show
    @ Test.Associativity
    Test.$fShowAssociativity_$cshowsPrec
    Test.$fShowAssociativity_$cshow
    Test.$fShowAssociativity_$cshowList

Test.$fEqAssociativity_$c/=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c/= =
  \ (a2_atz :: Test.Associativity) (b_atA :: Test.Associativity) ->
    case a2_atz of _ {
      Test.LeftAssociative ->
        case b_atA of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case b_atA of _ {
          __DEFAULT -> GHC.Bool.True; Test.RightAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case b_atA of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fEqAssociativity_$c==
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c== =
  \ (a2_atv :: Test.Associativity) (b_atw :: Test.Associativity) ->
    case a2_atv of _ {
      Test.LeftAssociative ->
        case b_atw of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case b_atw of _ {
          __DEFAULT -> GHC.Bool.False; Test.RightAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case b_atw of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fEqAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqAssociativity_$c==,
                            Test.$fEqAssociativity_$c/=]]
Test.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ Test.Associativity
    Test.$fEqAssociativity_$c==
    Test.$fEqAssociativity_$c/=

Test.$fOrdAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqAssociativity,
                             Test.$fOrdAssociativity_$ccompare, Test.$fOrdAssociativity_$c<,
                             Test.$fOrdAssociativity_$c>=, Test.$fOrdAssociativity_$c>,
                             Test.$fOrdAssociativity_$c<=, Test.$fOrdAssociativity_$cmax,
                             Test.$fOrdAssociativity_$cmin]]
Test.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ Test.Associativity
    Test.$fEqAssociativity
    Test.$fOrdAssociativity_$ccompare
    Test.$fOrdAssociativity_$c<
    Test.$fOrdAssociativity_$c>=
    Test.$fOrdAssociativity_$c>
    Test.$fOrdAssociativity_$c<=
    Test.$fOrdAssociativity_$cmax
    Test.$fOrdAssociativity_$cmin


------ Local rules for imported ids --------
"SPEC Test.$dmconFixity [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10L [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconFixity @ Test.Logic_F_ $dConstructor_s10L
      = Test.$dmconFixity_$s$dmconFixity7
"SPEC Test.$dmconFixity [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10O [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconFixity @ Test.Logic_T_ $dConstructor_s10O
      = Test.$dmconFixity_$s$dmconFixity6
"SPEC Test.$dmconFixity [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10R [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconFixity @ Test.Logic_Not_ $dConstructor_s10R
      = Test.$dmconFixity_$s$dmconFixity5
"SPEC Test.$dmconFixity [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10U [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconFixity @ Test.Logic_Disj_ $dConstructor_s10U
      = Test.$dmconFixity_$s$dmconFixity4
"SPEC Test.$dmconFixity [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10X [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconFixity @ Test.Logic_Conj_ $dConstructor_s10X
      = Test.$dmconFixity_$s$dmconFixity3
"SPEC Test.$dmconFixity [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s110 [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconFixity @ Test.Logic_Equiv_ $dConstructor_s110
      = Test.$dmconFixity_$s$dmconFixity2
"SPEC Test.$dmconFixity [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s113 [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconFixity @ Test.Logic_Impl_ $dConstructor_s113
      = Test.$dmconFixity_$s$dmconFixity1
"SPEC Test.$dmconFixity [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s116 [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconFixity @ Test.Logic_Var_ $dConstructor_s116
      = Test.$dmconFixity_$s$dmconFixity
"SPEC Test.$dmconIsRecord [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10n [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconIsRecord @ Test.Logic_F_ $dConstructor_s10n
      = Test.$dmconIsRecord_$s$dmconIsRecord7
"SPEC Test.$dmconIsRecord [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10q [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconIsRecord @ Test.Logic_T_ $dConstructor_s10q
      = Test.$dmconIsRecord_$s$dmconIsRecord6
"SPEC Test.$dmconIsRecord [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10t [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconIsRecord @ Test.Logic_Not_ $dConstructor_s10t
      = Test.$dmconIsRecord_$s$dmconIsRecord5
"SPEC Test.$dmconIsRecord [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10w [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconIsRecord @ Test.Logic_Disj_ $dConstructor_s10w
      = Test.$dmconIsRecord_$s$dmconIsRecord4
"SPEC Test.$dmconIsRecord [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10z [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconIsRecord @ Test.Logic_Conj_ $dConstructor_s10z
      = Test.$dmconIsRecord_$s$dmconIsRecord3
"SPEC Test.$dmconIsRecord [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s10C [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconIsRecord @ Test.Logic_Equiv_ $dConstructor_s10C
      = Test.$dmconIsRecord_$s$dmconIsRecord2
"SPEC Test.$dmconIsRecord [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s10F [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconIsRecord @ Test.Logic_Impl_ $dConstructor_s10F
      = Test.$dmconIsRecord_$s$dmconIsRecord1
"SPEC Test.$dmconIsRecord [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s10I [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconIsRecord @ Test.Logic_Var_ $dConstructor_s10I
      = Test.$dmconIsRecord_$s$dmconIsRecord
"SPEC Test.updateString [Test.Logic]" ALWAYS
    forall {$dRegular_s10b :: Test.Regular Test.Logic
            $dUpdateString_X17t :: Test.UpdateString (Test.PF Test.Logic)}
      Test.updateString @ Test.Logic $dRegular_s10b $dUpdateString_X17t
      = Test.updateString_$supdateString


