
==================== Tidy Core ====================
Test.updateIntf
  :: forall (f_ahN :: * -> *).
     (Test.UpdateInt f_ahN) =>
     forall a_ahO. (a_ahO -> a_ahO) -> f_ahN a_ahO -> f_ahN a_ahO
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.updateIntf =
  \ (@ f_ahN::* -> *) (tpl_B1 [Occ=Once] :: Test.UpdateInt f_ahN) ->
    tpl_B1
    `cast` (Test.NTCo:T:UpdateInt f_ahN
            :: Test.T:UpdateInt f_ahN
                 ~
               (forall a_ahO. (a_ahO -> a_ahO) -> f_ahN a_ahO -> f_ahN a_ahO))

Test.from [InlPrag=NOINLINE]
  :: forall a_ahP.
     (Test.Regular a_ahP) =>
     a_ahP -> Test.PF a_ahP a_ahP
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.from: "Class op from"]
Test.from =
  \ (@ a_ahP) (tpl_B1 [Occ=Once!] :: Test.Regular a_ahP) ->
    case tpl_B1 of _ { Test.D:Regular tpl_B2 [Occ=Once] _ -> tpl_B2 }

Test.to [InlPrag=NOINLINE]
  :: forall a_ahP.
     (Test.Regular a_ahP) =>
     Test.PF a_ahP a_ahP -> a_ahP
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.to: "Class op to"]
Test.to =
  \ (@ a_ahP) (tpl_B1 [Occ=Once!] :: Test.Regular a_ahP) ->
    case tpl_B1 of _ { Test.D:Regular _ tpl_B3 [Occ=Once] -> tpl_B3 }

Test.conName [InlPrag=NOINLINE]
  :: forall c_ahR.
     (Test.Constructor c_ahR) =>
     forall (t_ahS :: * -> (* -> *) -> * -> *) (f_ahT :: * -> *) r_ahU.
     t_ahS c_ahR f_ahT r_ahU -> GHC.Base.String
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SAA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conName: "Class op conName"]
Test.conName =
  \ (@ c_ahR) (tpl_B1 [Occ=Once!] :: Test.Constructor c_ahR) ->
    case tpl_B1 of _ { Test.D:Constructor tpl_B2 [Occ=Once] _ _ ->
    tpl_B2
    }

Test.conFixity [InlPrag=NOINLINE]
  :: forall c_ahR.
     (Test.Constructor c_ahR) =>
     forall (t_ahV :: * -> (* -> *) -> * -> *) (f_ahW :: * -> *) r_ahX.
     t_ahV c_ahR f_ahW r_ahX -> Test.Fixity
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(ASA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conFixity: "Class op conFixity"]
Test.conFixity =
  \ (@ c_ahR) (tpl_B1 [Occ=Once!] :: Test.Constructor c_ahR) ->
    case tpl_B1 of _ { Test.D:Constructor _ tpl_B3 [Occ=Once] _ ->
    tpl_B3
    }

Test.conIsRecord [InlPrag=NOINLINE]
  :: forall c_ahR.
     (Test.Constructor c_ahR) =>
     forall (t_ahY :: * -> (* -> *) -> * -> *) (f_ahZ :: * -> *) r_ai0.
     t_ahY c_ahR f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AAS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conIsRecord: "Class op conIsRecord"]
Test.conIsRecord =
  \ (@ c_ahR) (tpl_B1 [Occ=Once!] :: Test.Constructor c_ahR) ->
    case tpl_B1 of _ { Test.D:Constructor _ _ tpl_B4 [Occ=Once] ->
    tpl_B4
    }

Test.$fShowFixity2 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity2 = GHC.Base.unpackCString# "Infix "

Test.$fShowFixity3 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity3 = GHC.Base.unpackCString# "Prefix"

Test.$fReadAssociativity9 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

Test.$fReadAssociativity12 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

Test.$fReadAssociativity15 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

lvl_r1uD :: [GHC.Types.Char]
[GblId, Str=DmdType]
lvl_r1uD = GHC.Base.unpackCString# "Infix"

lvl1_r1uF :: forall c_azl. Test.C c_azl Test.U Test.Two
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl1_r1uF =
  \ (@ c_azl) ->
    Test.C @ c_azl @ Test.U @ Test.Two (Test.U @ Test.Two)

lvl2_r1uH
  :: forall c_azl c1_azw.
     (Test.:+:)
       (Test.C c_azl Test.U)
       (Test.C c1_azw (Test.K GHC.Types.Int Test.:*: Test.I))
       Test.Two
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl2_r1uH =
  \ (@ c_azl) (@ c1_azw) ->
    Test.L
      @ (Test.C c_azl Test.U)
      @ (Test.C c1_azw (Test.K GHC.Types.Int Test.:*: Test.I))
      @ Test.Two
      (lvl1_r1uF @ c_azl)

lvl3_r1uJ :: forall c_ayI. Test.C c_ayI Test.U Test.Three
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl3_r1uJ =
  \ (@ c_ayI) ->
    Test.C @ c_ayI @ Test.U @ Test.Three (Test.U @ Test.Three)

lvl4_r1uL
  :: forall c_ayI c1_ayT.
     (Test.:+:)
       (Test.C c_ayI Test.U)
       (Test.C c1_ayT (Test.K GHC.Types.Int))
       Test.Three
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl4_r1uL =
  \ (@ c_ayI) (@ c1_ayT) ->
    Test.L
      @ (Test.C c_ayI Test.U)
      @ (Test.C c1_ayT (Test.K GHC.Types.Int))
      @ Test.Three
      (lvl3_r1uJ @ c_ayI)

lvl5_r1uN
  :: forall c_ayy c1_ayI c2_ayT.
     (Test.:+:)
       (Test.C
          c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
       (Test.C c1_ayI Test.U
        Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
       Test.Three
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl5_r1uN =
  \ (@ c_ayy) (@ c1_ayI) (@ c2_ayT) ->
    Test.R
      @ (Test.C
           c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
      @ (Test.C c1_ayI Test.U
         Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
      @ Test.Three
      (lvl4_r1uL @ c1_ayI @ c2_ayT)

lvl6_r1uP :: forall c_axp. Test.C c_axp Test.U Test.Four
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl6_r1uP =
  \ (@ c_axp) ->
    Test.C @ c_axp @ Test.U @ Test.Four (Test.U @ Test.Four)

lvl7_r1uR
  :: forall c_axp c1_axC c2_axT.
     (Test.:+:)
       (Test.C c_axp Test.U)
       (Test.C c1_axC (Test.K GHC.Types.Int)
        Test.:+: Test.C
                   c2_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
       Test.Four
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl7_r1uR =
  \ (@ c_axp) (@ c1_axC) (@ c2_axT) ->
    Test.L
      @ (Test.C c_axp Test.U)
      @ (Test.C c1_axC (Test.K GHC.Types.Int)
         Test.:+: Test.C
                    c2_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
      @ Test.Four
      (lvl6_r1uP @ c_axp)

lvl8_r1uT
  :: forall c_axf c1_axp c2_axC c3_axT.
     (Test.:+:)
       (Test.C
          c_axf
          (Test.I
           Test.:*: (Test.K GHC.Types.Int
                     Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
       (Test.C c1_axp Test.U
        Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                  Test.:+: Test.C
                             c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
       Test.Four
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl8_r1uT =
  \ (@ c_axf) (@ c1_axp) (@ c2_axC) (@ c3_axT) ->
    Test.R
      @ (Test.C
           c_axf
           (Test.I
            Test.:*: (Test.K GHC.Types.Int
                      Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
      @ (Test.C c1_axp Test.U
         Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                   Test.:+: Test.C
                              c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
      @ Test.Four
      (lvl7_r1uR @ c1_axp @ c2_axC @ c3_axT)

Test.$fConstructorOne_One_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorOne_One_1 = GHC.Base.unpackCString# "One"

Test.$fConstructorTwo_Two0_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorTwo_Two0_1 = GHC.Base.unpackCString# "Two0"

Test.$fConstructorTwo_Two_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorTwo_Two_1 = GHC.Base.unpackCString# "Two"

Test.$fConstructorThree_Three_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorThree_Three_1 = GHC.Base.unpackCString# "Three"

Test.$fConstructorThree_Three0_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorThree_Three0_1 = GHC.Base.unpackCString# "Three0"

Test.$fConstructorThree_Three1_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorThree_Three1_1 = GHC.Base.unpackCString# "Three1"

Test.$fConstructorFour_Four_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorFour_Four_1 = GHC.Base.unpackCString# "Four"

Test.$fConstructorFour_Four0_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorFour_Four0_1 = GHC.Base.unpackCString# "Four0"

Test.$fConstructorFour_Four1_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorFour_Four1_1 = GHC.Base.unpackCString# "Four1"

Test.$fConstructorFour_Four2_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorFour_Four2_1 = GHC.Base.unpackCString# "Four2"

lvl9_r1v5 :: GHC.Integer.Type.Integer
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl9_r1v5 = GHC.Integer.Type.S# 1

Rec {
Test.updateInt_updateInt :: Test.One -> Test.One
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType Sm,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True,
         Guidance=InlineRule(unsat, worker=Test.$wupdateInt)
         Tmpl= \ (w_s1rh [Occ=Once] :: Test.One) ->
                 case Test.$wupdateInt w_s1rh of _ { (# ww1_s1rn [Occ=Once] #) ->
                 Test.One ww1_s1rn
                 }}]
Test.updateInt_updateInt =
  \ (w_s1rh :: Test.One) ->
    case Test.$wupdateInt w_s1rh of _ { (# ww1_s1rn #) ->
    Test.One ww1_s1rn
    }

Test.$wupdateInt :: Test.One -> (# Test.One #)
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
Test.$wupdateInt =
  \ (w_s1rh :: Test.One) ->
    case w_s1rh of _ { Test.One f0_aiu ->
    (# Test.updateInt_updateInt f0_aiu #)
    }
end Rec }

lvl10_r1v9 :: GHC.Types.Int
[GblId, Caf=NoCafRefs]
lvl10_r1v9 = GHC.Types.I# 2147483647

Rec {
Test.testTwo_updateInt :: Test.Two -> Test.Two
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
Test.testTwo_updateInt =
  \ (x_a16F :: Test.Two) ->
    case x_a16F of _ {
      Test.Two0 -> Test.Two0;
      Test.Two f0_aiw f1_aix ->
        Test.Two
          (case f0_aiw of _ { GHC.Types.I# x1_a1k9 ->
           case x1_a1k9 of wild11_a1kb {
             __DEFAULT ->
               case GHC.Prim.remInt# wild11_a1kb 2 of _ {
                 __DEFAULT -> GHC.Types.I# (GHC.Prim.+# wild11_a1kb 1);
                 0 -> GHC.Types.I# (GHC.Prim.-# wild11_a1kb 1)
               };
             (-2147483648) -> lvl10_r1v9
           }
           })
          (Test.testTwo_updateInt f1_aix)
    }
end Rec }

Rec {
Test.testThree_updateInt :: Test.Three -> Test.Three
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
Test.testThree_updateInt =
  \ (x_a16F :: Test.Three) ->
    case x_a16F of _ {
      Test.Three f0_aiA f1_aiB f2_aiC ->
        Test.Three
          (Test.testThree_updateInt f0_aiA)
          (Test.testThree_updateInt f1_aiB)
          (case f2_aiC of _ { GHC.Types.I# x1_a1k9 ->
           case x1_a1k9 of wild11_a1kb {
             __DEFAULT ->
               case GHC.Prim.remInt# wild11_a1kb 2 of _ {
                 __DEFAULT -> GHC.Types.I# (GHC.Prim.+# wild11_a1kb 1);
                 0 -> GHC.Types.I# (GHC.Prim.-# wild11_a1kb 1)
               };
             (-2147483648) -> lvl10_r1v9
           }
           });
      Test.Three0 -> Test.Three0;
      Test.Three1 f0_aiD ->
        Test.Three1
          (case f0_aiD of _ { GHC.Types.I# x1_a1k9 ->
           case x1_a1k9 of wild11_a1kb {
             __DEFAULT ->
               case GHC.Prim.remInt# wild11_a1kb 2 of _ {
                 __DEFAULT -> GHC.Types.I# (GHC.Prim.+# wild11_a1kb 1);
                 0 -> GHC.Types.I# (GHC.Prim.-# wild11_a1kb 1)
               };
             (-2147483648) -> lvl10_r1v9
           }
           })
    }
end Rec }

Rec {
Test.testFour_updateInt :: Test.Four -> Test.Four
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
Test.testFour_updateInt =
  \ (x_a16F :: Test.Four) ->
    case x_a16F of _ {
      Test.Four f0_aiI f1_aiJ f2_aiK f3_aiL ->
        Test.Four
          (Test.testFour_updateInt f0_aiI)
          (case f1_aiJ of _ { GHC.Types.I# x1_a1k9 ->
           case x1_a1k9 of wild11_a1kb {
             __DEFAULT ->
               case GHC.Prim.remInt# wild11_a1kb 2 of _ {
                 __DEFAULT -> GHC.Types.I# (GHC.Prim.+# wild11_a1kb 1);
                 0 -> GHC.Types.I# (GHC.Prim.-# wild11_a1kb 1)
               };
             (-2147483648) -> lvl10_r1v9
           }
           })
          f2_aiK
          (Test.testFour_updateInt f3_aiL);
      Test.Four0 -> Test.Four0;
      Test.Four1 f0_aiM ->
        Test.Four1
          (case f0_aiM of _ { GHC.Types.I# x1_a1k9 ->
           case x1_a1k9 of wild11_a1kb {
             __DEFAULT ->
               case GHC.Prim.remInt# wild11_a1kb 2 of _ {
                 __DEFAULT -> GHC.Types.I# (GHC.Prim.+# wild11_a1kb 1);
                 0 -> GHC.Types.I# (GHC.Prim.-# wild11_a1kb 1)
               };
             (-2147483648) -> lvl10_r1v9
           }
           });
      Test.Four2 f0_aiN f1_aiO ->
        Test.Four2
          (case f0_aiN of _ { GHC.Types.I# x1_a1k9 ->
           case x1_a1k9 of wild11_a1kb {
             __DEFAULT ->
               case GHC.Prim.remInt# wild11_a1kb 2 of _ {
                 __DEFAULT -> GHC.Types.I# (GHC.Prim.+# wild11_a1kb 1);
                 0 -> GHC.Types.I# (GHC.Prim.-# wild11_a1kb 1)
               };
             (-2147483648) -> lvl10_r1v9
           }
           })
          f1_aiO
    }
end Rec }

Test.$dmconIsRecord_$s$dmconIsRecord9
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Four_Four2_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord9 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord8
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Four_Four1_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord8 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord7
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Four_Four0_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord7 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord6
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Four_Four_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord6 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord5
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Three_Three1_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord5 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord4
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Three_Three0_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord4 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord3
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Three_Three_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord3 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord2
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Two_Two_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord2 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord1
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.Two_Two0_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord1 =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord
  :: forall (t_ahY :: * -> (* -> *) -> * -> *)
            (f_ahZ :: * -> *)
            r_ai0.
     t_ahY Test.One_One_ f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord =
  \ (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconFixity_$s$dmconFixity9
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Four_Four2_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity9 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity8
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Four_Four1_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity8 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity7
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Four_Four0_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity7 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity6
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Four_Four_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity6 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity5
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Three_Three1_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity5 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity4
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Three_Three0_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity4 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity3
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Three_Three_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity3 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity2
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Two_Two_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity2 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity1
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.Two_Two0_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity1 =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity
  :: forall (t_ahV :: * -> (* -> *) -> * -> *)
            (f_ahW :: * -> *)
            r_ahX.
     t_ahV Test.One_One_ f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity =
  \ (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$fUpdateIntK0_$supdateIntfKInt
  :: forall t_aCK t1_aCM r_aGB.
     t_aCK -> Test.K GHC.Types.Int t1_aCM -> Test.K GHC.Types.Int r_aGB
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AU(L)m,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aCK)
                 (@ t1_aCM)
                 (@ r_aGB)
                 _
                 (eta1_X9T [Occ=Once] :: Test.K GHC.Types.Int t1_aCM) ->
                 case eta1_X9T
                      `cast` (Test.NTCo:K GHC.Types.Int t1_aCM
                              :: Test.K GHC.Types.Int t1_aCM ~ GHC.Types.Int)
                 of _ { GHC.Types.I# x_a1k9 [Occ=Once!] ->
                 case x_a1k9 of wild1_a1kb {
                   __DEFAULT ->
                     case GHC.Prim.remInt# wild1_a1kb 2 of _ {
                       __DEFAULT ->
                         (GHC.Types.I# (GHC.Prim.+# wild1_a1kb 1))
                         `cast` (sym (Test.NTCo:K GHC.Types.Int r_aGB)
                                 :: GHC.Types.Int ~ Test.K GHC.Types.Int r_aGB);
                       0 ->
                         (GHC.Types.I# (GHC.Prim.-# wild1_a1kb 1))
                         `cast` (sym (Test.NTCo:K GHC.Types.Int r_aGB)
                                 :: GHC.Types.Int ~ Test.K GHC.Types.Int r_aGB)
                     };
                   (-2147483648) ->
                     (GHC.Types.I# 2147483647)
                     `cast` (sym (Test.NTCo:K GHC.Types.Int r_aGB)
                             :: GHC.Types.Int ~ Test.K GHC.Types.Int r_aGB)
                 }
                 }}]
Test.$fUpdateIntK0_$supdateIntfKInt =
  \ (@ t_aCK)
    (@ t1_aCM)
    (@ r_aGB)
    _
    (eta1_X9T :: Test.K GHC.Types.Int t1_aCM) ->
    case eta1_X9T
         `cast` (Test.NTCo:K GHC.Types.Int t1_aCM
                 :: Test.K GHC.Types.Int t1_aCM ~ GHC.Types.Int)
    of _ { GHC.Types.I# x_a1k9 ->
    case x_a1k9 of wild1_a1kb {
      __DEFAULT ->
        case GHC.Prim.remInt# wild1_a1kb 2 of _ {
          __DEFAULT ->
            (GHC.Types.I# (GHC.Prim.+# wild1_a1kb 1))
            `cast` (sym (Test.NTCo:K GHC.Types.Int r_aGB)
                    :: GHC.Types.Int ~ Test.K GHC.Types.Int r_aGB);
          0 ->
            (GHC.Types.I# (GHC.Prim.-# wild1_a1kb 1))
            `cast` (sym (Test.NTCo:K GHC.Types.Int r_aGB)
                    :: GHC.Types.Int ~ Test.K GHC.Types.Int r_aGB)
        };
      (-2147483648) ->
        lvl10_r1v9
        `cast` (sym (Test.NTCo:K GHC.Types.Int r_aGB)
                :: GHC.Types.Int ~ Test.K GHC.Types.Int r_aGB)
    }
    }

Test.$fShowFixity1 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fShowFixity1 = GHC.Types.I# 0

Test.$fReadAssociativity14
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_a18V.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a18V)
        -> Text.ParserCombinators.ReadP.P b_a18V
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity14 =
  \ _
    (@ b_a18V)
    (eta_a18W
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a18V) ->
    eta_a18W Test.LeftAssociative

Test.$fReadAssociativity13
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity13 =
  (Test.$fReadAssociativity15,
   Test.$fReadAssociativity14
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a19d.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a19d)
                  -> Text.ParserCombinators.ReadP.P b_a19d)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity11
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_a18V.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a18V)
        -> Text.ParserCombinators.ReadP.P b_a18V
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity11 =
  \ _
    (@ b_a18V)
    (eta_a18W
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a18V) ->
    eta_a18W Test.RightAssociative

Test.$fReadAssociativity10
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity10 =
  (Test.$fReadAssociativity12,
   Test.$fReadAssociativity11
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a19d.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a19d)
                  -> Text.ParserCombinators.ReadP.P b_a19d)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity8
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_a18V.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a18V)
        -> Text.ParserCombinators.ReadP.P b_a18V
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity8 =
  \ _
    (@ b_a18V)
    (eta_a18W
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a18V) ->
    eta_a18W Test.NotAssociative

Test.$fReadAssociativity7
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity7 =
  (Test.$fReadAssociativity9,
   Test.$fReadAssociativity8
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_a19d.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a19d)
                  -> Text.ParserCombinators.ReadP.P b_a19d)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity6
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity5
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity10
    Test.$fReadAssociativity6

Test.$fReadAssociativity4
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity13
    Test.$fReadAssociativity5

Test.$fReadAssociativity3
  :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity3 =
  GHC.Read.choose1 @ Test.Associativity Test.$fReadAssociativity4

Test.$fReadAssociativity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Associativity Test.$fReadAssociativity3

Test.$fReadAssociativity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ Test.Associativity
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity16
  :: Text.ParserCombinators.ReadP.P [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadAssociativity16 =
  ((Test.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [Test.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
                ~
              (forall b_a19d.
               ([Test.Associativity] -> Text.ParserCombinators.ReadP.P b_a19d)
               -> Text.ParserCombinators.ReadP.P b_a19d)))
    @ [Test.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn
       @ [Test.Associativity])

lvl11_r1vN
  :: forall b_a19d.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a19d
[GblId, Arity=1, Caf=NoCafRefs]
lvl11_r1vN =
  \ (@ b_a19d) (a111_a1pJ :: GHC.Base.String) ->
    case a111_a1pJ of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a19d
    }

lvl12_r1vP :: forall b_a19d. Text.ParserCombinators.ReadP.P b_a19d
[GblId, Caf=NoCafRefs]
lvl12_r1vP =
  \ (@ b_a19d) ->
    Text.ParserCombinators.ReadP.Look @ b_a19d (lvl11_r1vN @ b_a19d)

lvl13_r1vR :: GHC.Types.Int
[GblId, Caf=NoCafRefs]
lvl13_r1vR = GHC.Types.I# 11

Test.$fReadFixity3
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_a19d.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_a19d)
        -> Text.ParserCombinators.ReadP.P b_a19d
[GblId, Arity=2, Str=DmdType LL]
Test.$fReadFixity3 =
  \ (n_a1m6 :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_a19d)
    (eta_B1 :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_a19d) ->
    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
      @ b_a19d
      (Text.ParserCombinators.ReadP.Look
         @ b_a19d
         (let {
            $wk_s1rP [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a19d
            [LclId, Str=DmdType]
            $wk_s1rP =
              Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                @ b_a19d
                (lvl12_r1vP @ b_a19d)
                (Text.Read.Lex.hsLex2
                   @ b_a19d
                   (let {
                      lvl14_s1sI :: Text.ParserCombinators.ReadP.P b_a19d
                      [LclId]
                      lvl14_s1sI = eta_B1 Test.Prefix } in
                    \ (a11_a19r :: Text.Read.Lex.Lexeme) ->
                      case a11_a19r of _ {
                        __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a19d;
                        Text.Read.Lex.Ident ds_d15I ->
                          case GHC.Base.eqString ds_d15I Test.$fShowFixity3 of _ {
                            GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a19d;
                            GHC.Bool.True -> lvl14_s1sI
                          }
                      })) } in
          let {
            k_s1rR :: () -> Text.ParserCombinators.ReadP.P b_a19d
            [LclId, Arity=1, Str=DmdType A]
            k_s1rR = \ _ -> $wk_s1rP } in
          \ (a111_a1q2 :: GHC.Base.String) ->
            ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a1q2)
             `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                     :: Text.ParserCombinators.ReadP.ReadP ()
                          ~
                        (forall b_a19d.
                         (() -> Text.ParserCombinators.ReadP.P b_a19d)
                         -> Text.ParserCombinators.ReadP.P b_a19d)))
              @ b_a19d k_s1rR))
      (case n_a1m6 of _ { GHC.Types.I# x_a1lK ->
       case GHC.Prim.<=# x_a1lK 10 of _ {
         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a19d;
         GHC.Bool.True ->
           Text.ParserCombinators.ReadP.Look
             @ b_a19d
             (let {
                $wk_s1rZ :: Text.ParserCombinators.ReadP.P b_a19d
                [LclId, Str=DmdType]
                $wk_s1rZ =
                  let {
                    lvl14_s1sJ :: Text.ParserCombinators.ReadP.P b_a19d
                    [LclId]
                    lvl14_s1sJ =
                      ((GHC.Read.$dmreadsPrec3
                          @ Test.Associativity Test.$fReadAssociativity3 lvl13_r1vR)
                       `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                               :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                    ~
                                  (forall b_a19d.
                                   (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a19d)
                                   -> Text.ParserCombinators.ReadP.P b_a19d)))
                        @ b_a19d
                        (\ (a11_X1eQ :: Test.Associativity) ->
                           ((GHC.Read.$dmreadsPrec11 lvl13_r1vR)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                                    :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                         ~
                                       (forall b_a19d.
                                        (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_a19d)
                                        -> Text.ParserCombinators.ReadP.P b_a19d)))
                             @ b_a19d
                             (\ (a12_X1fK :: GHC.Types.Int) ->
                                eta_B1 (Test.Infix a11_X1eQ a12_X1fK))) } in
                  Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                    @ b_a19d
                    (Text.ParserCombinators.ReadP.Look
                       @ b_a19d
                       (\ (a111_a1pJ :: GHC.Base.String) ->
                          case a111_a1pJ of _ { __DEFAULT ->
                          Text.ParserCombinators.ReadP.Fail @ b_a19d
                          }))
                    (Text.Read.Lex.hsLex2
                       @ b_a19d
                       (\ (a11_a19r :: Text.Read.Lex.Lexeme) ->
                          case a11_a19r of _ {
                            __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a19d;
                            Text.Read.Lex.Ident ds_d15N ->
                              case GHC.Base.eqString ds_d15N lvl_r1uD of _ {
                                GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_a19d;
                                GHC.Bool.True -> lvl14_s1sJ
                              }
                          })) } in
              let {
                k_s1s1 :: () -> Text.ParserCombinators.ReadP.P b_a19d
                [LclId, Arity=1, Str=DmdType A]
                k_s1s1 = \ _ -> $wk_s1rZ } in
              \ (a111_a1q2 :: GHC.Base.String) ->
                ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a1q2)
                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                         :: Text.ParserCombinators.ReadP.ReadP ()
                              ~
                            (forall b_a19d.
                             (() -> Text.ParserCombinators.ReadP.P b_a19d)
                             -> Text.ParserCombinators.ReadP.P b_a19d)))
                  @ b_a19d k_s1s1)
       }
       })

Test.$fReadFixity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Fixity
    (Test.$fReadFixity3
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_a19d.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_a19d)
                    -> Text.ParserCombinators.ReadP.P b_a19d)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ Test.Fixity
    (Test.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity4 :: Text.ParserCombinators.ReadP.P [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadFixity4 =
  ((Test.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [Test.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
                ~
              (forall b_a19d.
               ([Test.Fixity] -> Text.ParserCombinators.ReadP.P b_a19d)
               -> Text.ParserCombinators.ReadP.P b_a19d)))
    @ [Test.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ [Test.Fixity])

Test.unI1 :: forall r_aw5. Test.I r_aw5 -> Test.I r_aw5
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unI1 = \ (@ r_aw5) (ds_d158 :: Test.I r_aw5) -> ds_d158

Test.unK1
  :: forall a_aw7 r_aw8. Test.K a_aw7 r_aw8 -> Test.K a_aw7 r_aw8
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unK1 =
  \ (@ a_aw7) (@ r_aw8) (ds_d15a :: Test.K a_aw7 r_aw8) -> ds_d15a

a_r1vZ
  :: forall t_aJS r_aJV. (t_aJS -> r_aJV) -> Test.I t_aJS -> r_aJV
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType C(S)L]
a_r1vZ =
  \ (@ t_aJS)
    (@ r_aJV)
    (eta_X4V :: t_aJS -> r_aJV)
    (eta1_X9P :: Test.I t_aJS) ->
    eta_X4V
      (eta1_X9P `cast` (Test.NTCo:I t_aJS :: Test.I t_aJS ~ t_aJS))

a1_r1w1
  :: forall t_aCy t1_aCA t2_aCB r_aCE.
     t_aCy -> Test.K t2_aCB t1_aCA -> Test.K t2_aCB t1_aCA
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType AS]
a1_r1w1 =
  \ (@ t_aCy)
    (@ t1_aCA)
    (@ t2_aCB)
    (@ r_aCE)
    _
    (eta1_B1 :: Test.K t2_aCB t1_aCA) ->
    eta1_B1

Test.four5 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.four5 = GHC.Types.I# 3

Test.four2 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.four2 = GHC.Types.I# 2

Test.three1 :: Test.Three
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.three1 = Test.Three1 Test.four2

Test.four1 :: Test.Four
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.four1 = Test.Four1 Test.four2

Test.four4 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.four4 = GHC.Types.C# 'p'

Test.four3 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.four3 =
  GHC.Types.:
    @ GHC.Types.Char Test.four4 (GHC.Types.[] @ GHC.Types.Char)

Test.$dmconIsRecord
  :: forall c_ahR.
     (Test.Constructor c_ahR) =>
     forall (t_ahY :: * -> (* -> *) -> * -> *) (f_ahZ :: * -> *) r_ai0.
     t_ahY c_ahR f_ahZ r_ai0 -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_ahR)
                 _
                 (@ t_aKj::* -> (* -> *) -> * -> *)
                 (@ f_aKk::* -> *)
                 (@ r_aKl)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord =
  \ (@ c_ahR)
    _
    (@ t_aKj::* -> (* -> *) -> * -> *)
    (@ f_aKk::* -> *)
    (@ r_aKl)
    _ ->
    GHC.Bool.False

Test.$dmconFixity
  :: forall c_ahR.
     (Test.Constructor c_ahR) =>
     forall (t_ahV :: * -> (* -> *) -> * -> *) (f_ahW :: * -> *) r_ahX.
     t_ahV c_ahR f_ahW r_ahX -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_ahR)
                 _
                 (@ t_aKb::* -> (* -> *) -> * -> *)
                 (@ f_aKc::* -> *)
                 (@ r_aKd)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity =
  \ (@ c_ahR)
    _
    (@ t_aKb::* -> (* -> *) -> * -> *)
    (@ f_aKc::* -> *)
    (@ r_aKd)
    _ ->
    Test.Prefix

Test.$fUpdateIntK0 :: Test.UpdateInt (Test.K GHC.Types.Int)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AU(L)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_a10Y) ->
                  Test.$fUpdateIntK0_$supdateIntfKInt
                    @ (a_a10Y -> a_a10Y) @ a_a10Y @ a_a10Y)
               `cast` (sym (Test.NTCo:T:UpdateInt (Test.K GHC.Types.Int))
                       :: (forall a_ahO.
                           (a_ahO -> a_ahO)
                           -> Test.K GHC.Types.Int a_ahO
                           -> Test.K GHC.Types.Int a_ahO)
                            ~
                          Test.T:UpdateInt (Test.K GHC.Types.Int))}]
Test.$fUpdateIntK0 =
  (\ (@ a_a10Y) ->
     Test.$fUpdateIntK0_$supdateIntfKInt
       @ (a_a10Y -> a_a10Y) @ a_a10Y @ a_a10Y)
  `cast` (sym (Test.NTCo:T:UpdateInt (Test.K GHC.Types.Int))
          :: (forall a_ahO.
              (a_ahO -> a_ahO)
              -> Test.K GHC.Types.Int a_ahO
              -> Test.K GHC.Types.Int a_ahO)
               ~
             Test.T:UpdateInt (Test.K GHC.Types.Int))

Test.four :: Test.Four
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 5}]
Test.four = Test.Four Test.Four0 Test.four5 Test.four3 Test.four1

Test.testFour :: Test.Four
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testFour = Test.testFour_updateInt Test.four

Test.three :: Test.Three
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 4}]
Test.three = Test.Three Test.Three0 Test.three1 Test.four5

Test.testThree :: Test.Three
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testThree = Test.testThree_updateInt Test.three

Test.two :: Test.Two
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.two = Test.Two Test.four5 Test.Two0

Test.testTwo :: Test.Two
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testTwo = Test.testTwo_updateInt Test.two

Rec {
Test.one :: Test.One
[GblId, Caf=NoCafRefs, Str=DmdType m]
Test.one = Test.One Test.one
end Rec }

Test.testOne :: Test.One
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 2}]
Test.testOne =
  case Test.$wupdateInt Test.one of _ { (# ww1_s1rn #) ->
  Test.One ww1_s1rn
  }

Test.toFour [InlPrag=INLINE]
  :: forall t_awt t1_awI t2_awM t3_awQ.
     (Test.:+:)
       (Test.C
          t_awt
          (Test.I
           Test.:*: (Test.K GHC.Types.Int
                     Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
       (Test.C t1_awI Test.U
        Test.:+: (Test.C t2_awM (Test.K GHC.Types.Int)
                  Test.:+: Test.C
                             t3_awQ (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
       Test.Four
     -> Test.Four
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_awt)
                 (@ t1_awI)
                 (@ t2_awM)
                 (@ t3_awQ)
                 (ds_d12A [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C
                            t_awt
                            (Test.I
                             Test.:*: (Test.K GHC.Types.Int
                                       Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
                         (Test.C t1_awI Test.U
                          Test.:+: (Test.C t2_awM (Test.K GHC.Types.Int)
                                    Test.:+: Test.C
                                               t3_awQ
                                               (Test.K GHC.Types.Int
                                                Test.:*: Test.K GHC.Base.String)))
                         Test.Four) ->
                 case ds_d12A of _ {
                   Test.L ds1_d12L [Occ=Once!] ->
                     case ds1_d12L of _ { Test.C ds2_d12M [Occ=Once!] ->
                     case ds2_d12M
                     of _ { Test.:*: ds3_d12N [Occ=Once] ds4_d12O [Occ=Once!] ->
                     case ds4_d12O
                     of _ { Test.:*: ds5_d12P [Occ=Once] ds6_d12Q [Occ=Once!] ->
                     case ds6_d12Q
                     of _ { Test.:*: ds7_d12R [Occ=Once] ds8_d12S [Occ=Once] ->
                     Test.Four
                       (ds3_d12N
                        `cast` (Test.NTCo:I Test.Four :: Test.I Test.Four ~ Test.Four))
                       (ds5_d12P
                        `cast` (Test.NTCo:K GHC.Types.Int Test.Four
                                :: Test.K GHC.Types.Int Test.Four ~ GHC.Types.Int))
                       (ds7_d12R
                        `cast` (Test.NTCo:K GHC.Base.String Test.Four
                                :: Test.K GHC.Base.String Test.Four ~ GHC.Base.String))
                       (ds8_d12S
                        `cast` (Test.NTCo:I Test.Four :: Test.I Test.Four ~ Test.Four))
                     }
                     }
                     }
                     };
                   Test.R ds1_d12B [Occ=Once!] ->
                     case ds1_d12B of _ {
                       Test.L ds2_d12J [Occ=Once!] ->
                         case ds2_d12J of _ { Test.C ds3_d12K [Occ=Once!] ->
                         case ds3_d12K of _ { Test.U -> Test.Four0 }
                         };
                       Test.R ds2_d12C [Occ=Once!] ->
                         case ds2_d12C of _ {
                           Test.L ds3_d12H [Occ=Once!] ->
                             case ds3_d12H of _ { Test.C ds4_d12I [Occ=Once] ->
                             Test.Four1
                               (ds4_d12I
                                `cast` (Test.NTCo:K GHC.Types.Int Test.Four
                                        :: Test.K GHC.Types.Int Test.Four ~ GHC.Types.Int))
                             };
                           Test.R ds3_d12D [Occ=Once!] ->
                             case ds3_d12D of _ { Test.C ds4_d12E [Occ=Once!] ->
                             case ds4_d12E
                             of _ { Test.:*: ds5_d12F [Occ=Once] ds6_d12G [Occ=Once] ->
                             Test.Four2
                               (ds5_d12F
                                `cast` (Test.NTCo:K GHC.Types.Int Test.Four
                                        :: Test.K GHC.Types.Int Test.Four ~ GHC.Types.Int))
                               (ds6_d12G
                                `cast` (Test.NTCo:K GHC.Base.String Test.Four
                                        :: Test.K GHC.Base.String Test.Four ~ GHC.Base.String))
                             }
                             }
                         }
                     }
                 }}]
Test.toFour =
  \ (@ t_awt)
    (@ t1_awI)
    (@ t2_awM)
    (@ t3_awQ)
    (eta_B1
       :: (Test.:+:)
            (Test.C
               t_awt
               (Test.I
                Test.:*: (Test.K GHC.Types.Int
                          Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
            (Test.C t1_awI Test.U
             Test.:+: (Test.C t2_awM (Test.K GHC.Types.Int)
                       Test.:+: Test.C
                                  t3_awQ (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
            Test.Four) ->
    case eta_B1 of _ {
      Test.L ds_d12L ->
        case ds_d12L of _ { Test.C ds1_d12M ->
        case ds1_d12M of _ { Test.:*: ds2_d12N ds3_d12O ->
        case ds3_d12O of _ { Test.:*: ds4_d12P ds5_d12Q ->
        case ds5_d12Q of _ { Test.:*: ds6_d12R ds7_d12S ->
        Test.Four
          (ds2_d12N
           `cast` (Test.NTCo:I Test.Four :: Test.I Test.Four ~ Test.Four))
          (ds4_d12P
           `cast` (Test.NTCo:K GHC.Types.Int Test.Four
                   :: Test.K GHC.Types.Int Test.Four ~ GHC.Types.Int))
          (ds6_d12R
           `cast` (Test.NTCo:K GHC.Base.String Test.Four
                   :: Test.K GHC.Base.String Test.Four ~ GHC.Base.String))
          (ds7_d12S
           `cast` (Test.NTCo:I Test.Four :: Test.I Test.Four ~ Test.Four))
        }
        }
        }
        };
      Test.R ds_d12B ->
        case ds_d12B of _ {
          Test.L ds1_d12J ->
            case ds1_d12J of _ { Test.C ds2_d12K ->
            case ds2_d12K of _ { Test.U -> Test.Four0 }
            };
          Test.R ds1_d12C ->
            case ds1_d12C of _ {
              Test.L ds2_d12H ->
                case ds2_d12H of _ { Test.C ds3_d12I ->
                Test.Four1
                  (ds3_d12I
                   `cast` (Test.NTCo:K GHC.Types.Int Test.Four
                           :: Test.K GHC.Types.Int Test.Four ~ GHC.Types.Int))
                };
              Test.R ds2_d12D ->
                case ds2_d12D of _ { Test.C ds3_d12E ->
                case ds3_d12E of _ { Test.:*: ds4_d12F ds5_d12G ->
                Test.Four2
                  (ds4_d12F
                   `cast` (Test.NTCo:K GHC.Types.Int Test.Four
                           :: Test.K GHC.Types.Int Test.Four ~ GHC.Types.Int))
                  (ds5_d12G
                   `cast` (Test.NTCo:K GHC.Base.String Test.Four
                           :: Test.K GHC.Base.String Test.Four ~ GHC.Base.String))
                }
                }
            }
        }
    }

Test.fromFour [InlPrag=INLINE]
  :: forall c_axf c1_axp c2_axC c3_axT.
     Test.Four
     -> (Test.:+:)
          (Test.C
             c_axf
             (Test.I
              Test.:*: (Test.K GHC.Types.Int
                        Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
          (Test.C c1_axp Test.U
           Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                     Test.:+: Test.C
                                c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
          Test.Four
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_axf)
                 (@ c1_axp)
                 (@ c2_axC)
                 (@ c3_axT)
                 (ds_d138 [Occ=Once!] :: Test.Four) ->
                 case ds_d138 of _ {
                   Test.Four f0_aiI [Occ=Once]
                             f1_aiJ [Occ=Once]
                             f2_aiK [Occ=Once]
                             f3_aiL [Occ=Once] ->
                     Test.L
                       @ (Test.C
                            c_axf
                            (Test.I
                             Test.:*: (Test.K GHC.Types.Int
                                       Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
                       @ (Test.C c1_axp Test.U
                          Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                                    Test.:+: Test.C
                                               c3_axT
                                               (Test.K GHC.Types.Int
                                                Test.:*: Test.K GHC.Base.String)))
                       @ Test.Four
                       (Test.C
                          @ c_axf
                          @ (Test.I
                             Test.:*: (Test.K GHC.Types.Int
                                       Test.:*: (Test.K GHC.Base.String Test.:*: Test.I)))
                          @ Test.Four
                          (Test.:*:
                             @ Test.I
                             @ (Test.K GHC.Types.Int
                                Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))
                             @ Test.Four
                             (f0_aiI
                              `cast` (sym (Test.NTCo:I Test.Four)
                                      :: Test.Four ~ Test.I Test.Four))
                             (Test.:*:
                                @ (Test.K GHC.Types.Int)
                                @ (Test.K GHC.Base.String Test.:*: Test.I)
                                @ Test.Four
                                (f1_aiJ
                                 `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Four)
                                         :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Four))
                                (Test.:*:
                                   @ (Test.K GHC.Base.String)
                                   @ Test.I
                                   @ Test.Four
                                   (f2_aiK
                                    `cast` (sym (Test.NTCo:K GHC.Base.String Test.Four)
                                            :: GHC.Base.String ~ Test.K GHC.Base.String Test.Four))
                                   (f3_aiL
                                    `cast` (sym (Test.NTCo:I Test.Four)
                                            :: Test.Four ~ Test.I Test.Four))))));
                   Test.Four0 ->
                     Test.R
                       @ (Test.C
                            c_axf
                            (Test.I
                             Test.:*: (Test.K GHC.Types.Int
                                       Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
                       @ (Test.C c1_axp Test.U
                          Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                                    Test.:+: Test.C
                                               c3_axT
                                               (Test.K GHC.Types.Int
                                                Test.:*: Test.K GHC.Base.String)))
                       @ Test.Four
                       (Test.L
                          @ (Test.C c1_axp Test.U)
                          @ (Test.C c2_axC (Test.K GHC.Types.Int)
                             Test.:+: Test.C
                                        c3_axT
                                        (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                          @ Test.Four
                          (Test.C @ c1_axp @ Test.U @ Test.Four (Test.U @ Test.Four)));
                   Test.Four1 f0_aiM [Occ=Once] ->
                     Test.R
                       @ (Test.C
                            c_axf
                            (Test.I
                             Test.:*: (Test.K GHC.Types.Int
                                       Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
                       @ (Test.C c1_axp Test.U
                          Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                                    Test.:+: Test.C
                                               c3_axT
                                               (Test.K GHC.Types.Int
                                                Test.:*: Test.K GHC.Base.String)))
                       @ Test.Four
                       (Test.R
                          @ (Test.C c1_axp Test.U)
                          @ (Test.C c2_axC (Test.K GHC.Types.Int)
                             Test.:+: Test.C
                                        c3_axT
                                        (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                          @ Test.Four
                          (Test.L
                             @ (Test.C c2_axC (Test.K GHC.Types.Int))
                             @ (Test.C
                                  c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                             @ Test.Four
                             (Test.C
                                @ c2_axC
                                @ (Test.K GHC.Types.Int)
                                @ Test.Four
                                (f0_aiM
                                 `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Four)
                                         :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Four)))));
                   Test.Four2 f0_aiN [Occ=Once] f1_aiO [Occ=Once] ->
                     Test.R
                       @ (Test.C
                            c_axf
                            (Test.I
                             Test.:*: (Test.K GHC.Types.Int
                                       Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
                       @ (Test.C c1_axp Test.U
                          Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                                    Test.:+: Test.C
                                               c3_axT
                                               (Test.K GHC.Types.Int
                                                Test.:*: Test.K GHC.Base.String)))
                       @ Test.Four
                       (Test.R
                          @ (Test.C c1_axp Test.U)
                          @ (Test.C c2_axC (Test.K GHC.Types.Int)
                             Test.:+: Test.C
                                        c3_axT
                                        (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                          @ Test.Four
                          (Test.R
                             @ (Test.C c2_axC (Test.K GHC.Types.Int))
                             @ (Test.C
                                  c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                             @ Test.Four
                             (Test.C
                                @ c3_axT
                                @ (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)
                                @ Test.Four
                                (Test.:*:
                                   @ (Test.K GHC.Types.Int)
                                   @ (Test.K GHC.Base.String)
                                   @ Test.Four
                                   (f0_aiN
                                    `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Four)
                                            :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Four))
                                   (f1_aiO
                                    `cast` (sym (Test.NTCo:K GHC.Base.String Test.Four)
                                            :: GHC.Base.String
                                                 ~
                                               Test.K GHC.Base.String Test.Four))))))
                 }}]
Test.fromFour =
  \ (@ c_axf)
    (@ c1_axp)
    (@ c2_axC)
    (@ c3_axT)
    (eta_B1 :: Test.Four) ->
    case eta_B1 of _ {
      Test.Four f0_aiI f1_aiJ f2_aiK f3_aiL ->
        Test.L
          @ (Test.C
               c_axf
               (Test.I
                Test.:*: (Test.K GHC.Types.Int
                          Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
          @ (Test.C c1_axp Test.U
             Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                       Test.:+: Test.C
                                  c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
          @ Test.Four
          (Test.C
             @ c_axf
             @ (Test.I
                Test.:*: (Test.K GHC.Types.Int
                          Test.:*: (Test.K GHC.Base.String Test.:*: Test.I)))
             @ Test.Four
             (Test.:*:
                @ Test.I
                @ (Test.K GHC.Types.Int
                   Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))
                @ Test.Four
                (f0_aiI
                 `cast` (sym (Test.NTCo:I Test.Four)
                         :: Test.Four ~ Test.I Test.Four))
                (Test.:*:
                   @ (Test.K GHC.Types.Int)
                   @ (Test.K GHC.Base.String Test.:*: Test.I)
                   @ Test.Four
                   (f1_aiJ
                    `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Four)
                            :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Four))
                   (Test.:*:
                      @ (Test.K GHC.Base.String)
                      @ Test.I
                      @ Test.Four
                      (f2_aiK
                       `cast` (sym (Test.NTCo:K GHC.Base.String Test.Four)
                               :: GHC.Base.String ~ Test.K GHC.Base.String Test.Four))
                      (f3_aiL
                       `cast` (sym (Test.NTCo:I Test.Four)
                               :: Test.Four ~ Test.I Test.Four))))));
      Test.Four0 -> lvl8_r1uT @ c_axf @ c1_axp @ c2_axC @ c3_axT;
      Test.Four1 f0_aiM ->
        Test.R
          @ (Test.C
               c_axf
               (Test.I
                Test.:*: (Test.K GHC.Types.Int
                          Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
          @ (Test.C c1_axp Test.U
             Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                       Test.:+: Test.C
                                  c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
          @ Test.Four
          (Test.R
             @ (Test.C c1_axp Test.U)
             @ (Test.C c2_axC (Test.K GHC.Types.Int)
                Test.:+: Test.C
                           c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
             @ Test.Four
             (Test.L
                @ (Test.C c2_axC (Test.K GHC.Types.Int))
                @ (Test.C
                     c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                @ Test.Four
                (Test.C
                   @ c2_axC
                   @ (Test.K GHC.Types.Int)
                   @ Test.Four
                   (f0_aiM
                    `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Four)
                            :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Four)))));
      Test.Four2 f0_aiN f1_aiO ->
        Test.R
          @ (Test.C
               c_axf
               (Test.I
                Test.:*: (Test.K GHC.Types.Int
                          Test.:*: (Test.K GHC.Base.String Test.:*: Test.I))))
          @ (Test.C c1_axp Test.U
             Test.:+: (Test.C c2_axC (Test.K GHC.Types.Int)
                       Test.:+: Test.C
                                  c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)))
          @ Test.Four
          (Test.R
             @ (Test.C c1_axp Test.U)
             @ (Test.C c2_axC (Test.K GHC.Types.Int)
                Test.:+: Test.C
                           c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
             @ Test.Four
             (Test.R
                @ (Test.C c2_axC (Test.K GHC.Types.Int))
                @ (Test.C
                     c3_axT (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String))
                @ Test.Four
                (Test.C
                   @ c3_axT
                   @ (Test.K GHC.Types.Int Test.:*: Test.K GHC.Base.String)
                   @ Test.Four
                   (Test.:*:
                      @ (Test.K GHC.Types.Int)
                      @ (Test.K GHC.Base.String)
                      @ Test.Four
                      (f0_aiN
                       `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Four)
                               :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Four))
                      (f1_aiO
                       `cast` (sym (Test.NTCo:K GHC.Base.String Test.Four)
                               :: GHC.Base.String ~ Test.K GHC.Base.String Test.Four))))))
    }

Test.$fRegularFour [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Four
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromFour
                              @ Test.Four_Four_
                              @ Test.Four_Four0_
                              @ Test.Four_Four1_
                              @ Test.Four_Four2_)
                           `cast` (Test.Four -> sym Test.TFCo:R:PFFour Test.Four
                                   :: (Test.Four -> Test.R:PFFour Test.Four)
                                        ~
                                      (Test.Four -> Test.PF Test.Four Test.Four))),
                          ((Test.toFour
                              @ Test.Four_Four_
                              @ Test.Four_Four0_
                              @ Test.Four_Four1_
                              @ Test.Four_Four2_)
                           `cast` (sym Test.TFCo:R:PFFour Test.Four -> Test.Four
                                   :: (Test.R:PFFour Test.Four -> Test.Four)
                                        ~
                                      (Test.PF Test.Four Test.Four -> Test.Four)))]]
Test.$fRegularFour =
  Test.D:Regular
    @ Test.Four
    ((Test.fromFour
        @ Test.Four_Four_
        @ Test.Four_Four0_
        @ Test.Four_Four1_
        @ Test.Four_Four2_)
     `cast` (Test.Four -> sym Test.TFCo:R:PFFour Test.Four
             :: (Test.Four -> Test.R:PFFour Test.Four)
                  ~
                (Test.Four -> Test.PF Test.Four Test.Four)))
    ((Test.toFour
        @ Test.Four_Four_
        @ Test.Four_Four0_
        @ Test.Four_Four1_
        @ Test.Four_Four2_)
     `cast` (sym Test.TFCo:R:PFFour Test.Four -> Test.Four
             :: (Test.R:PFFour Test.Four -> Test.Four)
                  ~
                (Test.PF Test.Four Test.Four -> Test.Four)))

Test.toThree [InlPrag=INLINE]
  :: forall t_ay3 t1_aye t2_ayg.
     (Test.:+:)
       (Test.C
          t_ay3 (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
       (Test.C t1_aye Test.U
        Test.:+: Test.C t2_ayg (Test.K GHC.Types.Int))
       Test.Three
     -> Test.Three
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_ay3)
                 (@ t1_aye)
                 (@ t2_ayg)
                 (ds_d13d [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C
                            t_ay3 (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
                         (Test.C t1_aye Test.U
                          Test.:+: Test.C t2_ayg (Test.K GHC.Types.Int))
                         Test.Three) ->
                 case ds_d13d of _ {
                   Test.L ds1_d13j [Occ=Once!] ->
                     case ds1_d13j of _ { Test.C ds2_d13k [Occ=Once!] ->
                     case ds2_d13k
                     of _ { Test.:*: ds3_d13l [Occ=Once] ds4_d13m [Occ=Once!] ->
                     case ds4_d13m
                     of _ { Test.:*: ds5_d13n [Occ=Once] ds6_d13o [Occ=Once] ->
                     Test.Three
                       (ds3_d13l
                        `cast` (Test.NTCo:I Test.Three :: Test.I Test.Three ~ Test.Three))
                       (ds5_d13n
                        `cast` (Test.NTCo:I Test.Three :: Test.I Test.Three ~ Test.Three))
                       (ds6_d13o
                        `cast` (Test.NTCo:K GHC.Types.Int Test.Three
                                :: Test.K GHC.Types.Int Test.Three ~ GHC.Types.Int))
                     }
                     }
                     };
                   Test.R ds1_d13e [Occ=Once!] ->
                     case ds1_d13e of _ {
                       Test.L ds2_d13h [Occ=Once!] ->
                         case ds2_d13h of _ { Test.C ds3_d13i [Occ=Once!] ->
                         case ds3_d13i of _ { Test.U -> Test.Three0 }
                         };
                       Test.R ds2_d13f [Occ=Once!] ->
                         case ds2_d13f of _ { Test.C ds3_d13g [Occ=Once] ->
                         Test.Three1
                           (ds3_d13g
                            `cast` (Test.NTCo:K GHC.Types.Int Test.Three
                                    :: Test.K GHC.Types.Int Test.Three ~ GHC.Types.Int))
                         }
                     }
                 }}]
Test.toThree =
  \ (@ t_ay3)
    (@ t1_aye)
    (@ t2_ayg)
    (eta_B1
       :: (Test.:+:)
            (Test.C
               t_ay3 (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
            (Test.C t1_aye Test.U
             Test.:+: Test.C t2_ayg (Test.K GHC.Types.Int))
            Test.Three) ->
    case eta_B1 of _ {
      Test.L ds_d13j ->
        case ds_d13j of _ { Test.C ds1_d13k ->
        case ds1_d13k of _ { Test.:*: ds2_d13l ds3_d13m ->
        case ds3_d13m of _ { Test.:*: ds4_d13n ds5_d13o ->
        Test.Three
          (ds2_d13l
           `cast` (Test.NTCo:I Test.Three :: Test.I Test.Three ~ Test.Three))
          (ds4_d13n
           `cast` (Test.NTCo:I Test.Three :: Test.I Test.Three ~ Test.Three))
          (ds5_d13o
           `cast` (Test.NTCo:K GHC.Types.Int Test.Three
                   :: Test.K GHC.Types.Int Test.Three ~ GHC.Types.Int))
        }
        }
        };
      Test.R ds_d13e ->
        case ds_d13e of _ {
          Test.L ds1_d13h ->
            case ds1_d13h of _ { Test.C ds2_d13i ->
            case ds2_d13i of _ { Test.U -> Test.Three0 }
            };
          Test.R ds1_d13f ->
            case ds1_d13f of _ { Test.C ds2_d13g ->
            Test.Three1
              (ds2_d13g
               `cast` (Test.NTCo:K GHC.Types.Int Test.Three
                       :: Test.K GHC.Types.Int Test.Three ~ GHC.Types.Int))
            }
        }
    }

Test.fromThree [InlPrag=INLINE]
  :: forall c_ayy c1_ayI c2_ayT.
     Test.Three
     -> (Test.:+:)
          (Test.C
             c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
          (Test.C c1_ayI Test.U
           Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
          Test.Three
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_ayy)
                 (@ c1_ayI)
                 (@ c2_ayT)
                 (ds_d13z [Occ=Once!] :: Test.Three) ->
                 case ds_d13z of _ {
                   Test.Three f0_aiA [Occ=Once] f1_aiB [Occ=Once] f2_aiC [Occ=Once] ->
                     Test.L
                       @ (Test.C
                            c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
                       @ (Test.C c1_ayI Test.U
                          Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
                       @ Test.Three
                       (Test.C
                          @ c_ayy
                          @ (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int))
                          @ Test.Three
                          (Test.:*:
                             @ Test.I
                             @ (Test.I Test.:*: Test.K GHC.Types.Int)
                             @ Test.Three
                             (f0_aiA
                              `cast` (sym (Test.NTCo:I Test.Three)
                                      :: Test.Three ~ Test.I Test.Three))
                             (Test.:*:
                                @ Test.I
                                @ (Test.K GHC.Types.Int)
                                @ Test.Three
                                (f1_aiB
                                 `cast` (sym (Test.NTCo:I Test.Three)
                                         :: Test.Three ~ Test.I Test.Three))
                                (f2_aiC
                                 `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Three)
                                         :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Three)))));
                   Test.Three0 ->
                     Test.R
                       @ (Test.C
                            c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
                       @ (Test.C c1_ayI Test.U
                          Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
                       @ Test.Three
                       (Test.L
                          @ (Test.C c1_ayI Test.U)
                          @ (Test.C c2_ayT (Test.K GHC.Types.Int))
                          @ Test.Three
                          (Test.C @ c1_ayI @ Test.U @ Test.Three (Test.U @ Test.Three)));
                   Test.Three1 f0_aiD [Occ=Once] ->
                     Test.R
                       @ (Test.C
                            c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
                       @ (Test.C c1_ayI Test.U
                          Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
                       @ Test.Three
                       (Test.R
                          @ (Test.C c1_ayI Test.U)
                          @ (Test.C c2_ayT (Test.K GHC.Types.Int))
                          @ Test.Three
                          (Test.C
                             @ c2_ayT
                             @ (Test.K GHC.Types.Int)
                             @ Test.Three
                             (f0_aiD
                              `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Three)
                                      :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Three))))
                 }}]
Test.fromThree =
  \ (@ c_ayy) (@ c1_ayI) (@ c2_ayT) (eta_B1 :: Test.Three) ->
    case eta_B1 of _ {
      Test.Three f0_aiA f1_aiB f2_aiC ->
        Test.L
          @ (Test.C
               c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
          @ (Test.C c1_ayI Test.U
             Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
          @ Test.Three
          (Test.C
             @ c_ayy
             @ (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int))
             @ Test.Three
             (Test.:*:
                @ Test.I
                @ (Test.I Test.:*: Test.K GHC.Types.Int)
                @ Test.Three
                (f0_aiA
                 `cast` (sym (Test.NTCo:I Test.Three)
                         :: Test.Three ~ Test.I Test.Three))
                (Test.:*:
                   @ Test.I
                   @ (Test.K GHC.Types.Int)
                   @ Test.Three
                   (f1_aiB
                    `cast` (sym (Test.NTCo:I Test.Three)
                            :: Test.Three ~ Test.I Test.Three))
                   (f2_aiC
                    `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Three)
                            :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Three)))));
      Test.Three0 -> lvl5_r1uN @ c_ayy @ c1_ayI @ c2_ayT;
      Test.Three1 f0_aiD ->
        Test.R
          @ (Test.C
               c_ayy (Test.I Test.:*: (Test.I Test.:*: Test.K GHC.Types.Int)))
          @ (Test.C c1_ayI Test.U
             Test.:+: Test.C c2_ayT (Test.K GHC.Types.Int))
          @ Test.Three
          (Test.R
             @ (Test.C c1_ayI Test.U)
             @ (Test.C c2_ayT (Test.K GHC.Types.Int))
             @ Test.Three
             (Test.C
                @ c2_ayT
                @ (Test.K GHC.Types.Int)
                @ Test.Three
                (f0_aiD
                 `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Three)
                         :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Three))))
    }

Test.$fRegularThree [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Three
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromThree
                              @ Test.Three_Three_ @ Test.Three_Three0_ @ Test.Three_Three1_)
                           `cast` (Test.Three -> sym Test.TFCo:R:PFThree Test.Three
                                   :: (Test.Three -> Test.R:PFThree Test.Three)
                                        ~
                                      (Test.Three -> Test.PF Test.Three Test.Three))),
                          ((Test.toThree
                              @ Test.Three_Three_ @ Test.Three_Three0_ @ Test.Three_Three1_)
                           `cast` (sym Test.TFCo:R:PFThree Test.Three -> Test.Three
                                   :: (Test.R:PFThree Test.Three -> Test.Three)
                                        ~
                                      (Test.PF Test.Three Test.Three -> Test.Three)))]]
Test.$fRegularThree =
  Test.D:Regular
    @ Test.Three
    ((Test.fromThree
        @ Test.Three_Three_ @ Test.Three_Three0_ @ Test.Three_Three1_)
     `cast` (Test.Three -> sym Test.TFCo:R:PFThree Test.Three
             :: (Test.Three -> Test.R:PFThree Test.Three)
                  ~
                (Test.Three -> Test.PF Test.Three Test.Three)))
    ((Test.toThree
        @ Test.Three_Three_ @ Test.Three_Three0_ @ Test.Three_Three1_)
     `cast` (sym Test.TFCo:R:PFThree Test.Three -> Test.Three
             :: (Test.R:PFThree Test.Three -> Test.Three)
                  ~
                (Test.PF Test.Three Test.Three -> Test.Three)))

Test.toTwo [InlPrag=INLINE]
  :: forall t_az2 t1_az4.
     (Test.:+:)
       (Test.C t_az2 Test.U)
       (Test.C t1_az4 (Test.K GHC.Types.Int Test.:*: Test.I))
       Test.Two
     -> Test.Two
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_az2)
                 (@ t1_az4)
                 (ds_d13D [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C t_az2 Test.U)
                         (Test.C t1_az4 (Test.K GHC.Types.Int Test.:*: Test.I))
                         Test.Two) ->
                 case ds_d13D of _ {
                   Test.L ds1_d13I [Occ=Once!] ->
                     case ds1_d13I of _ { Test.C ds2_d13J [Occ=Once!] ->
                     case ds2_d13J of _ { Test.U -> Test.Two0 }
                     };
                   Test.R ds1_d13E [Occ=Once!] ->
                     case ds1_d13E of _ { Test.C ds2_d13F [Occ=Once!] ->
                     case ds2_d13F
                     of _ { Test.:*: ds3_d13G [Occ=Once] ds4_d13H [Occ=Once] ->
                     Test.Two
                       (ds3_d13G
                        `cast` (Test.NTCo:K GHC.Types.Int Test.Two
                                :: Test.K GHC.Types.Int Test.Two ~ GHC.Types.Int))
                       (ds4_d13H
                        `cast` (Test.NTCo:I Test.Two :: Test.I Test.Two ~ Test.Two))
                     }
                     }
                 }}]
Test.toTwo =
  \ (@ t_az2)
    (@ t1_az4)
    (eta_B1
       :: (Test.:+:)
            (Test.C t_az2 Test.U)
            (Test.C t1_az4 (Test.K GHC.Types.Int Test.:*: Test.I))
            Test.Two) ->
    case eta_B1 of _ {
      Test.L ds_d13I ->
        case ds_d13I of _ { Test.C ds1_d13J ->
        case ds1_d13J of _ { Test.U -> Test.Two0 }
        };
      Test.R ds_d13E ->
        case ds_d13E of _ { Test.C ds1_d13F ->
        case ds1_d13F of _ { Test.:*: ds2_d13G ds3_d13H ->
        Test.Two
          (ds2_d13G
           `cast` (Test.NTCo:K GHC.Types.Int Test.Two
                   :: Test.K GHC.Types.Int Test.Two ~ GHC.Types.Int))
          (ds3_d13H
           `cast` (Test.NTCo:I Test.Two :: Test.I Test.Two ~ Test.Two))
        }
        }
    }

Test.fromTwo [InlPrag=INLINE]
  :: forall c_azl c1_azw.
     Test.Two
     -> (Test.:+:)
          (Test.C c_azl Test.U)
          (Test.C c1_azw (Test.K GHC.Types.Int Test.:*: Test.I))
          Test.Two
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_azl) (@ c1_azw) (ds_d13Q [Occ=Once!] :: Test.Two) ->
                 case ds_d13Q of _ {
                   Test.Two0 ->
                     Test.L
                       @ (Test.C c_azl Test.U)
                       @ (Test.C c1_azw (Test.K GHC.Types.Int Test.:*: Test.I))
                       @ Test.Two
                       (Test.C @ c_azl @ Test.U @ Test.Two (Test.U @ Test.Two));
                   Test.Two f0_aiw [Occ=Once] f1_aix [Occ=Once] ->
                     Test.R
                       @ (Test.C c_azl Test.U)
                       @ (Test.C c1_azw (Test.K GHC.Types.Int Test.:*: Test.I))
                       @ Test.Two
                       (Test.C
                          @ c1_azw
                          @ (Test.K GHC.Types.Int Test.:*: Test.I)
                          @ Test.Two
                          (Test.:*:
                             @ (Test.K GHC.Types.Int)
                             @ Test.I
                             @ Test.Two
                             (f0_aiw
                              `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Two)
                                      :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Two))
                             (f1_aix
                              `cast` (sym (Test.NTCo:I Test.Two)
                                      :: Test.Two ~ Test.I Test.Two))))
                 }}]
Test.fromTwo =
  \ (@ c_azl) (@ c1_azw) (eta_B1 :: Test.Two) ->
    case eta_B1 of _ {
      Test.Two0 -> lvl2_r1uH @ c_azl @ c1_azw;
      Test.Two f0_aiw f1_aix ->
        Test.R
          @ (Test.C c_azl Test.U)
          @ (Test.C c1_azw (Test.K GHC.Types.Int Test.:*: Test.I))
          @ Test.Two
          (Test.C
             @ c1_azw
             @ (Test.K GHC.Types.Int Test.:*: Test.I)
             @ Test.Two
             (Test.:*:
                @ (Test.K GHC.Types.Int)
                @ Test.I
                @ Test.Two
                (f0_aiw
                 `cast` (sym (Test.NTCo:K GHC.Types.Int Test.Two)
                         :: GHC.Types.Int ~ Test.K GHC.Types.Int Test.Two))
                (f1_aix
                 `cast` (sym (Test.NTCo:I Test.Two)
                         :: Test.Two ~ Test.I Test.Two))))
    }

Test.$fRegularTwo [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Two
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromTwo
                              @ Test.Two_Two0_ @ Test.Two_Two_)
                           `cast` (Test.Two -> sym Test.TFCo:R:PFTwo Test.Two
                                   :: (Test.Two -> Test.R:PFTwo Test.Two)
                                        ~
                                      (Test.Two -> Test.PF Test.Two Test.Two))),
                          ((Test.toTwo @ Test.Two_Two0_ @ Test.Two_Two_)
                           `cast` (sym Test.TFCo:R:PFTwo Test.Two -> Test.Two
                                   :: (Test.R:PFTwo Test.Two -> Test.Two)
                                        ~
                                      (Test.PF Test.Two Test.Two -> Test.Two)))]]
Test.$fRegularTwo =
  Test.D:Regular
    @ Test.Two
    ((Test.fromTwo @ Test.Two_Two0_ @ Test.Two_Two_)
     `cast` (Test.Two -> sym Test.TFCo:R:PFTwo Test.Two
             :: (Test.Two -> Test.R:PFTwo Test.Two)
                  ~
                (Test.Two -> Test.PF Test.Two Test.Two)))
    ((Test.toTwo @ Test.Two_Two0_ @ Test.Two_Two_)
     `cast` (sym Test.TFCo:R:PFTwo Test.Two -> Test.Two
             :: (Test.R:PFTwo Test.Two -> Test.Two)
                  ~
                (Test.PF Test.Two Test.Two -> Test.Two)))

Test.toOne [InlPrag=INLINE]
  :: forall t_azB. Test.C t_azB Test.I Test.One -> Test.One
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(L)m,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= \ (@ t_azB)
                 (ds_d13T [Occ=Once!] :: Test.C t_azB Test.I Test.One) ->
                 case ds_d13T of _ { Test.C ds1_d13U [Occ=Once] ->
                 Test.One
                   (ds1_d13U
                    `cast` (Test.NTCo:I Test.One :: Test.I Test.One ~ Test.One))
                 }}]
Test.toOne =
  \ (@ t_azB) (eta_B1 :: Test.C t_azB Test.I Test.One) ->
    case eta_B1 of _ { Test.C ds_d13U ->
    Test.One
      (ds_d13U
       `cast` (Test.NTCo:I Test.One :: Test.I Test.One ~ Test.One))
    }

Test.fromOne [InlPrag=INLINE]
  :: forall c_azM. Test.One -> Test.C c_azM Test.I Test.One
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType Sm,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_azM) (ds_d13W [Occ=Once!] :: Test.One) ->
                 case ds_d13W of _ { Test.One f0_aiu [Occ=Once] ->
                 Test.C
                   @ c_azM
                   @ Test.I
                   @ Test.One
                   (f0_aiu
                    `cast` (sym (Test.NTCo:I Test.One) :: Test.One ~ Test.I Test.One))
                 }}]
Test.fromOne =
  \ (@ c_azM) (eta_B1 :: Test.One) ->
    case eta_B1 of _ { Test.One f0_aiu ->
    Test.C
      @ c_azM
      @ Test.I
      @ Test.One
      (f0_aiu
       `cast` (sym (Test.NTCo:I Test.One) :: Test.One ~ Test.I Test.One))
    }

Test.$fRegularOne [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.One
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromOne @ Test.One_One_)
                           `cast` (Test.One -> sym Test.TFCo:R:PFOne Test.One
                                   :: (Test.One -> Test.R:PFOne Test.One)
                                        ~
                                      (Test.One -> Test.PF Test.One Test.One))),
                          ((Test.toOne @ Test.One_One_)
                           `cast` (sym Test.TFCo:R:PFOne Test.One -> Test.One
                                   :: (Test.R:PFOne Test.One -> Test.One)
                                        ~
                                      (Test.PF Test.One Test.One -> Test.One)))]]
Test.$fRegularOne =
  Test.D:Regular
    @ Test.One
    ((Test.fromOne @ Test.One_One_)
     `cast` (Test.One -> sym Test.TFCo:R:PFOne Test.One
             :: (Test.One -> Test.R:PFOne Test.One)
                  ~
                (Test.One -> Test.PF Test.One Test.One)))
    ((Test.toOne @ Test.One_One_)
     `cast` (sym Test.TFCo:R:PFOne Test.One -> Test.One
             :: (Test.R:PFOne Test.One -> Test.One)
                  ~
                (Test.PF Test.One Test.One -> Test.One)))

Test.updateInt
  :: forall a_aie.
     (Test.Regular a_aie, Test.UpdateInt (Test.PF a_aie)) =>
     a_aie -> a_aie
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 0] 12 6}]
Test.updateInt =
  \ (@ a_azO)
    ($dRegular_aAg :: Test.Regular a_azO)
    ($dUpdateInt_aAh :: Test.UpdateInt (Test.PF a_azO)) ->
    letrec {
      sub_s1s3 :: Test.PF a_azO a_azO -> Test.PF a_azO a_azO
      [LclId, Str=DmdType]
      sub_s1s3 =
        ($dUpdateInt_aAh
         `cast` (Test.NTCo:T:UpdateInt (Test.PF a_azO)
                 :: Test.T:UpdateInt (Test.PF a_azO)
                      ~
                    (forall a_ahO.
                     (a_ahO -> a_ahO) -> Test.PF a_azO a_ahO -> Test.PF a_azO a_ahO)))
          @ a_azO updateInt1_s1s4;
      updateInt1_s1s4 [Occ=LoopBreaker] :: a_azO -> a_azO
      [LclId, Arity=1, Str=DmdType L]
      updateInt1_s1s4 =
        \ (x_a16F :: a_azO) ->
          Test.to
            @ a_azO
            $dRegular_aAg
            (sub_s1s3 (Test.from @ a_azO $dRegular_aAg x_a16F)); } in
    updateInt1_s1s4

Test.updateIntfC [InlPrag=INLINE]
  :: forall t_aBz (t1_aBC :: * -> *) r_aBH c_aBJ.
     (Test.UpdateInt t1_aBC) =>
     (r_aBH -> r_aBH)
     -> Test.C t_aBz t1_aBC r_aBH
     -> Test.C c_aBJ t1_aBC r_aBH
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aBz)
                 (@ t1_aBC::* -> *)
                 (@ r_aBH)
                 (@ c_aBJ)
                 ($dUpdateInt_aBL [Occ=Once] :: Test.UpdateInt t1_aBC)
                 (f_ais [Occ=Once] :: r_aBH -> r_aBH)
                 (ds_d13Y [Occ=Once!] :: Test.C t_aBz t1_aBC r_aBH) ->
                 case ds_d13Y of _ { Test.C x_ait [Occ=Once] ->
                 Test.C
                   @ c_aBJ
                   @ t1_aBC
                   @ r_aBH
                   (($dUpdateInt_aBL
                     `cast` (Test.NTCo:T:UpdateInt t1_aBC
                             :: Test.T:UpdateInt t1_aBC
                                  ~
                                (forall a_ahO. (a_ahO -> a_ahO) -> t1_aBC a_ahO -> t1_aBC a_ahO)))
                      @ r_aBH f_ais x_ait)
                 }}]
Test.updateIntfC =
  \ (@ t_aBz)
    (@ t1_aBC::* -> *)
    (@ r_aBH)
    (@ c_aBJ)
    ($dUpdateInt_aBL :: Test.UpdateInt t1_aBC)
    (eta_B2 :: r_aBH -> r_aBH)
    (eta1_B1 :: Test.C t_aBz t1_aBC r_aBH) ->
    case eta1_B1 of _ { Test.C x_ait ->
    Test.C
      @ c_aBJ
      @ t1_aBC
      @ r_aBH
      (($dUpdateInt_aBL
        `cast` (Test.NTCo:T:UpdateInt t1_aBC
                :: Test.T:UpdateInt t1_aBC
                     ~
                   (forall a_ahO. (a_ahO -> a_ahO) -> t1_aBC a_ahO -> t1_aBC a_ahO)))
         @ r_aBH eta_B2 x_ait)
    }

Test.$fUpdateIntC1
  :: forall (f_aiW :: * -> *) c_aiX.
     (Test.UpdateInt f_aiW) =>
     forall a_a10o.
     (a_a10o -> a_a10o)
     -> Test.C c_aiX f_aiW a_a10o
     -> Test.C c_aiX f_aiW a_a10o
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_aiW::* -> *)
                 (@ c_aiX)
                 ($dUpdateInt_a10j [Occ=Once] :: Test.UpdateInt f_aiW)
                 (@ a_a10o)
                 (eta_B2 [Occ=Once] :: a_a10o -> a_a10o)
                 (eta1_B1 [Occ=Once] :: Test.C c_aiX f_aiW a_a10o) ->
                 Test.updateIntfC
                   @ c_aiX @ f_aiW @ a_a10o @ c_aiX $dUpdateInt_a10j eta_B2 eta1_B1}]
Test.$fUpdateIntC1 =
  \ (@ f_aiW::* -> *)
    (@ c_aiX)
    ($dUpdateInt_a10j :: Test.UpdateInt f_aiW)
    (@ a_a10o)
    (eta_B2 :: a_a10o -> a_a10o)
    (eta1_B1 :: Test.C c_aiX f_aiW a_a10o) ->
    Test.updateIntfC
      @ c_aiX @ f_aiW @ a_a10o @ c_aiX $dUpdateInt_a10j eta_B2 eta1_B1

Test.$fUpdateIntC
  :: forall (f_aiW :: * -> *) c_aiX.
     (Test.UpdateInt f_aiW) =>
     Test.UpdateInt (Test.C c_aiX f_aiW)
[GblId[DFunId(newtype)],
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateIntC1
               `cast` (forall (f_aiW :: * -> *) c_aiX.
                       (Test.UpdateInt f_aiW) =>
                       sym (Test.NTCo:T:UpdateInt (Test.C c_aiX f_aiW))
                       :: (forall (f_aiW :: * -> *) c_aiX.
                           (Test.UpdateInt f_aiW) =>
                           forall a_ahO.
                           (a_ahO -> a_ahO)
                           -> Test.C c_aiX f_aiW a_ahO
                           -> Test.C c_aiX f_aiW a_ahO)
                            ~
                          (forall (f_aiW :: * -> *) c_aiX.
                           (Test.UpdateInt f_aiW) =>
                           Test.T:UpdateInt (Test.C c_aiX f_aiW)))}]
Test.$fUpdateIntC =
  Test.$fUpdateIntC1
  `cast` (forall (f_aiW :: * -> *) c_aiX.
          (Test.UpdateInt f_aiW) =>
          sym (Test.NTCo:T:UpdateInt (Test.C c_aiX f_aiW))
          :: (forall (f_aiW :: * -> *) c_aiX.
              (Test.UpdateInt f_aiW) =>
              forall a_ahO.
              (a_ahO -> a_ahO)
              -> Test.C c_aiX f_aiW a_ahO
              -> Test.C c_aiX f_aiW a_ahO)
               ~
             (forall (f_aiW :: * -> *) c_aiX.
              (Test.UpdateInt f_aiW) =>
              Test.T:UpdateInt (Test.C c_aiX f_aiW)))

Test.updateIntfTimes [InlPrag=INLINE]
  :: forall (t_aBU :: * -> *) (t1_aBW :: * -> *) r_aC0.
     (Test.UpdateInt t_aBU, Test.UpdateInt t1_aBW) =>
     (r_aC0 -> r_aC0)
     -> (Test.:*:) t_aBU t1_aBW r_aC0
     -> (Test.:*:) t_aBU t1_aBW r_aC0
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aBU::* -> *)
                 (@ t1_aBW::* -> *)
                 (@ r_aC0)
                 ($dUpdateInt_aC4 [Occ=Once] :: Test.UpdateInt t_aBU)
                 ($dUpdateInt1_aC5 [Occ=Once] :: Test.UpdateInt t1_aBW)
                 (f_aip :: r_aC0 -> r_aC0)
                 (ds_d140 [Occ=Once!] :: (Test.:*:) t_aBU t1_aBW r_aC0) ->
                 case ds_d140 of _ { Test.:*: x_aiq [Occ=Once] y_air [Occ=Once] ->
                 Test.:*:
                   @ t_aBU
                   @ t1_aBW
                   @ r_aC0
                   (($dUpdateInt_aC4
                     `cast` (Test.NTCo:T:UpdateInt t_aBU
                             :: Test.T:UpdateInt t_aBU
                                  ~
                                (forall a_ahO. (a_ahO -> a_ahO) -> t_aBU a_ahO -> t_aBU a_ahO)))
                      @ r_aC0 f_aip x_aiq)
                   (($dUpdateInt1_aC5
                     `cast` (Test.NTCo:T:UpdateInt t1_aBW
                             :: Test.T:UpdateInt t1_aBW
                                  ~
                                (forall a_ahO. (a_ahO -> a_ahO) -> t1_aBW a_ahO -> t1_aBW a_ahO)))
                      @ r_aC0 f_aip y_air)
                 }}]
Test.updateIntfTimes =
  \ (@ t_aBU::* -> *)
    (@ t1_aBW::* -> *)
    (@ r_aC0)
    ($dUpdateInt_aC4 :: Test.UpdateInt t_aBU)
    ($dUpdateInt1_aC5 :: Test.UpdateInt t1_aBW)
    (eta_B2 :: r_aC0 -> r_aC0)
    (eta1_B1 :: (Test.:*:) t_aBU t1_aBW r_aC0) ->
    case eta1_B1 of _ { Test.:*: x_aiq y_air ->
    Test.:*:
      @ t_aBU
      @ t1_aBW
      @ r_aC0
      (($dUpdateInt_aC4
        `cast` (Test.NTCo:T:UpdateInt t_aBU
                :: Test.T:UpdateInt t_aBU
                     ~
                   (forall a_ahO. (a_ahO -> a_ahO) -> t_aBU a_ahO -> t_aBU a_ahO)))
         @ r_aC0 eta_B2 x_aiq)
      (($dUpdateInt1_aC5
        `cast` (Test.NTCo:T:UpdateInt t1_aBW
                :: Test.T:UpdateInt t1_aBW
                     ~
                   (forall a_ahO. (a_ahO -> a_ahO) -> t1_aBW a_ahO -> t1_aBW a_ahO)))
         @ r_aC0 eta_B2 y_air)
    }

Test.$fUpdateInt:*:1
  :: forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
     (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
     forall a_a10y.
     (a_a10y -> a_a10y)
     -> (Test.:*:) f_aiY g_aiZ a_a10y
     -> (Test.:*:) f_aiY g_aiZ a_a10y
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_aiY::* -> *)
                 (@ g_aiZ::* -> *)
                 ($dUpdateInt_a10s [Occ=Once] :: Test.UpdateInt f_aiY)
                 ($dUpdateInt1_a10t [Occ=Once] :: Test.UpdateInt g_aiZ)
                 (@ a_a10y)
                 (eta_B2 [Occ=Once] :: a_a10y -> a_a10y)
                 (eta1_B1 [Occ=Once] :: (Test.:*:) f_aiY g_aiZ a_a10y) ->
                 Test.updateIntfTimes
                   @ f_aiY
                   @ g_aiZ
                   @ a_a10y
                   $dUpdateInt_a10s
                   $dUpdateInt1_a10t
                   eta_B2
                   eta1_B1}]
Test.$fUpdateInt:*:1 =
  \ (@ f_aiY::* -> *)
    (@ g_aiZ::* -> *)
    ($dUpdateInt_a10s :: Test.UpdateInt f_aiY)
    ($dUpdateInt1_a10t :: Test.UpdateInt g_aiZ)
    (@ a_a10y)
    (eta_B2 :: a_a10y -> a_a10y)
    (eta1_B1 :: (Test.:*:) f_aiY g_aiZ a_a10y) ->
    Test.updateIntfTimes
      @ f_aiY
      @ g_aiZ
      @ a_a10y
      $dUpdateInt_a10s
      $dUpdateInt1_a10t
      eta_B2
      eta1_B1

Test.$fUpdateInt:*:
  :: forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
     (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
     Test.UpdateInt (f_aiY Test.:*: g_aiZ)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateInt:*:1
               `cast` (forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
                       (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
                       sym (Test.NTCo:T:UpdateInt (f_aiY Test.:*: g_aiZ))
                       :: (forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
                           (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
                           forall a_ahO.
                           (a_ahO -> a_ahO)
                           -> (Test.:*:) f_aiY g_aiZ a_ahO
                           -> (Test.:*:) f_aiY g_aiZ a_ahO)
                            ~
                          (forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
                           (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
                           Test.T:UpdateInt (f_aiY Test.:*: g_aiZ)))}]
Test.$fUpdateInt:*: =
  Test.$fUpdateInt:*:1
  `cast` (forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
          (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
          sym (Test.NTCo:T:UpdateInt (f_aiY Test.:*: g_aiZ))
          :: (forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
              (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
              forall a_ahO.
              (a_ahO -> a_ahO)
              -> (Test.:*:) f_aiY g_aiZ a_ahO
              -> (Test.:*:) f_aiY g_aiZ a_ahO)
               ~
             (forall (f_aiY :: * -> *) (g_aiZ :: * -> *).
              (Test.UpdateInt f_aiY, Test.UpdateInt g_aiZ) =>
              Test.T:UpdateInt (f_aiY Test.:*: g_aiZ)))

Test.updateIntfPlus [InlPrag=INLINE]
  :: forall (t_aCe :: * -> *) r_aCj (g_aCl :: * -> *).
     (Test.UpdateInt t_aCe, Test.UpdateInt g_aCl) =>
     (r_aCj -> r_aCj)
     -> (Test.:+:) t_aCe g_aCl r_aCj
     -> (Test.:+:) t_aCe g_aCl r_aCj
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aCe::* -> *)
                 (@ r_aCj)
                 (@ g_aCl::* -> *)
                 ($dUpdateInt_aCs [Occ=Once] :: Test.UpdateInt t_aCe)
                 ($dUpdateInt1_aCt [Occ=Once] :: Test.UpdateInt g_aCl)
                 (f_ail [Occ=Once*] :: r_aCj -> r_aCj)
                 (ds_d142 [Occ=Once!] :: (Test.:+:) t_aCe g_aCl r_aCj) ->
                 case ds_d142 of _ {
                   Test.L x_aim [Occ=Once] ->
                     Test.L
                       @ t_aCe
                       @ g_aCl
                       @ r_aCj
                       (($dUpdateInt_aCs
                         `cast` (Test.NTCo:T:UpdateInt t_aCe
                                 :: Test.T:UpdateInt t_aCe
                                      ~
                                    (forall a_ahO. (a_ahO -> a_ahO) -> t_aCe a_ahO -> t_aCe a_ahO)))
                          @ r_aCj f_ail x_aim);
                   Test.R x_aio [Occ=Once] ->
                     Test.R
                       @ t_aCe
                       @ g_aCl
                       @ r_aCj
                       (($dUpdateInt1_aCt
                         `cast` (Test.NTCo:T:UpdateInt g_aCl
                                 :: Test.T:UpdateInt g_aCl
                                      ~
                                    (forall a_ahO. (a_ahO -> a_ahO) -> g_aCl a_ahO -> g_aCl a_ahO)))
                          @ r_aCj f_ail x_aio)
                 }}]
Test.updateIntfPlus =
  \ (@ t_aCe::* -> *)
    (@ r_aCj)
    (@ g_aCl::* -> *)
    ($dUpdateInt_aCs :: Test.UpdateInt t_aCe)
    ($dUpdateInt1_aCt :: Test.UpdateInt g_aCl)
    (eta_B2 :: r_aCj -> r_aCj)
    (eta1_B1 :: (Test.:+:) t_aCe g_aCl r_aCj) ->
    case eta1_B1 of _ {
      Test.L x_aim ->
        Test.L
          @ t_aCe
          @ g_aCl
          @ r_aCj
          (($dUpdateInt_aCs
            `cast` (Test.NTCo:T:UpdateInt t_aCe
                    :: Test.T:UpdateInt t_aCe
                         ~
                       (forall a_ahO. (a_ahO -> a_ahO) -> t_aCe a_ahO -> t_aCe a_ahO)))
             @ r_aCj eta_B2 x_aim);
      Test.R x_aio ->
        Test.R
          @ t_aCe
          @ g_aCl
          @ r_aCj
          (($dUpdateInt1_aCt
            `cast` (Test.NTCo:T:UpdateInt g_aCl
                    :: Test.T:UpdateInt g_aCl
                         ~
                       (forall a_ahO. (a_ahO -> a_ahO) -> g_aCl a_ahO -> g_aCl a_ahO)))
             @ r_aCj eta_B2 x_aio)
    }

Test.$fUpdateInt:+:1
  :: forall (f_aj0 :: * -> *) (g_aj1 :: * -> *).
     (Test.UpdateInt f_aj0, Test.UpdateInt g_aj1) =>
     forall a_a10J.
     (a_a10J -> a_a10J)
     -> (Test.:+:) f_aj0 g_aj1 a_a10J
     -> (Test.:+:) f_aj0 g_aj1 a_a10J
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0 0 0] 5 0}]
Test.$fUpdateInt:+:1 =
  \ (@ f_aj0::* -> *)
    (@ g_aj1::* -> *)
    ($dUpdateInt_a10D :: Test.UpdateInt f_aj0)
    ($dUpdateInt1_a10E :: Test.UpdateInt g_aj1)
    (@ a_a10J)
    (eta_B2 :: a_a10J -> a_a10J)
    (eta1_B1 :: (Test.:+:) f_aj0 g_aj1 a_a10J) ->
    Test.updateIntfPlus
      @ f_aj0
      @ a_a10J
      @ g_aj1
      $dUpdateInt_a10D
      $dUpdateInt1_a10E
      eta_B2
      eta1_B1

Test.$fUpdateInt:+:
  :: forall (f_aj0 :: * -> *) (g_aj1 :: * -> *).
     (Test.UpdateInt f_aj0, Test.UpdateInt g_aj1) =>
     Test.UpdateInt (f_aj0 Test.:+: g_aj1)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateInt:+: =
  Test.$fUpdateInt:+:1
  `cast` (forall (f_aj0 :: * -> *) (g_aj1 :: * -> *).
          (Test.UpdateInt f_aj0, Test.UpdateInt g_aj1) =>
          sym (Test.NTCo:T:UpdateInt (f_aj0 Test.:+: g_aj1))
          :: (forall (f_aj0 :: * -> *) (g_aj1 :: * -> *).
              (Test.UpdateInt f_aj0, Test.UpdateInt g_aj1) =>
              forall a_ahO.
              (a_ahO -> a_ahO)
              -> (Test.:+:) f_aj0 g_aj1 a_ahO
              -> (Test.:+:) f_aj0 g_aj1 a_ahO)
               ~
             (forall (f_aj0 :: * -> *) (g_aj1 :: * -> *).
              (Test.UpdateInt f_aj0, Test.UpdateInt g_aj1) =>
              Test.T:UpdateInt (f_aj0 Test.:+: g_aj1)))

Test.updateIntfK [InlPrag=INLINE]
  :: forall t_aCy t1_aCA t2_aCB r_aCE.
     t_aCy -> Test.K t2_aCB t1_aCA -> Test.K t2_aCB r_aCE
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= (\ (@ t_XHn)
                  (@ t1_XHq)
                  (@ t2_XHs)
                  (@ r_XHw)
                  _
                  (ds_d145 [Occ=Once] :: Test.K t2_XHs t1_XHq) ->
                  ds_d145)
               `cast` (forall t_XHn t1_XHq t2_XHs r_XHw.
                       t_XHn
                       -> Test.K t2_XHs t1_XHq
                       -> trans
                            (Test.NTCo:K t2_XHs t1_XHq) (sym (Test.NTCo:K t2_XHs r_XHw))
                       :: (forall t_XHn t1_XHq t2_XHs r_XHw.
                           t_XHn -> Test.K t2_XHs t1_XHq -> Test.K t2_XHs t1_XHq)
                            ~
                          (forall t_XHn t1_XHq t2_XHs r_XHw.
                           t_XHn -> Test.K t2_XHs t1_XHq -> Test.K t2_XHs r_XHw))}]
Test.updateIntfK =
  a1_r1w1
  `cast` (forall t_aCy t1_aCA t2_aCB r_aCE.
          t_aCy
          -> Test.K t2_aCB t1_aCA
          -> trans
               (Test.NTCo:K t2_aCB t1_aCA) (sym (Test.NTCo:K t2_aCB r_aCE))
          :: (forall t_aCy t1_aCA t2_aCB r_aCE.
              t_aCy -> Test.K t2_aCB t1_aCA -> Test.K t2_aCB t1_aCA)
               ~
             (forall t_aCy t1_aCA t2_aCB r_aCE.
              t_aCy -> Test.K t2_aCB t1_aCA -> Test.K t2_aCB r_aCE))

Test.$fUpdateIntK :: forall x_aj2. Test.UpdateInt (Test.K x_aj2)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ x_aj2) (@ a_a10S) ->
                  Test.updateIntfK @ (a_a10S -> a_a10S) @ a_a10S @ x_aj2 @ a_a10S)
               `cast` (forall x_aj2. sym (Test.NTCo:T:UpdateInt (Test.K x_aj2))
                       :: (forall x_aj2 a_ahO.
                           (a_ahO -> a_ahO) -> Test.K x_aj2 a_ahO -> Test.K x_aj2 a_ahO)
                            ~
                          (forall x_aj2. Test.T:UpdateInt (Test.K x_aj2)))}]
Test.$fUpdateIntK =
  (\ (@ x_aj2) (@ a_a10S) ->
     Test.updateIntfK @ (a_a10S -> a_a10S) @ a_a10S @ x_aj2 @ a_a10S)
  `cast` (forall x_aj2. sym (Test.NTCo:T:UpdateInt (Test.K x_aj2))
          :: (forall x_aj2 a_ahO.
              (a_ahO -> a_ahO) -> Test.K x_aj2 a_ahO -> Test.K x_aj2 a_ahO)
               ~
             (forall x_aj2. Test.T:UpdateInt (Test.K x_aj2)))

Test.updateIntfKInt [InlPrag=INLINE]
  :: forall t_aCK t1_aCM t2_aCN r_aGB.
     (GHC.Real.Integral t2_aCN) =>
     t_aCK -> Test.K t2_aCN t1_aCM -> Test.K t2_aCN r_aGB
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType S(SAAAAAAAA)AL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aCK)
                 (@ t1_aCM)
                 (@ t2_aCN)
                 (@ r_aGB)
                 ($dIntegral_aJl :: GHC.Real.Integral t2_aCN) ->
                 let {
                   $dNum_aJC :: GHC.Num.Num t2_aCN
                   [LclId]
                   $dNum_aJC =
                     GHC.Real.$p1Real
                       @ t2_aCN (GHC.Real.$p1Integral @ t2_aCN $dIntegral_aJl) } in
                 let {
                   lit_aGz [Occ=OnceL*] :: t2_aCN
                   [LclId]
                   lit_aGz =
                     GHC.Num.fromInteger @ t2_aCN $dNum_aJC (GHC.Integer.Type.S# 1) } in
                 let {
                   g1_a16N [Occ=OnceL!] :: t2_aCN -> GHC.Bool.Bool
                   [LclId]
                   g1_a16N = GHC.Real.even @ t2_aCN $dIntegral_aJl } in
                 \ _ (ds_d14R :: Test.K t2_aCN t1_aCM) ->
                   case g1_a16N
                          (ds_d14R
                           `cast` (Test.NTCo:K t2_aCN t1_aCM
                                   :: Test.K t2_aCN t1_aCM ~ t2_aCN))
                   of _ {
                     GHC.Bool.False ->
                       (GHC.Num.+
                          @ t2_aCN
                          $dNum_aJC
                          (ds_d14R
                           `cast` (Test.NTCo:K t2_aCN t1_aCM
                                   :: Test.K t2_aCN t1_aCM ~ t2_aCN))
                          lit_aGz)
                       `cast` (sym (Test.NTCo:K t2_aCN r_aGB)
                               :: t2_aCN ~ Test.K t2_aCN r_aGB);
                     GHC.Bool.True ->
                       (GHC.Num.-
                          @ t2_aCN
                          $dNum_aJC
                          (ds_d14R
                           `cast` (Test.NTCo:K t2_aCN t1_aCM
                                   :: Test.K t2_aCN t1_aCM ~ t2_aCN))
                          lit_aGz)
                       `cast` (sym (Test.NTCo:K t2_aCN r_aGB)
                               :: t2_aCN ~ Test.K t2_aCN r_aGB)
                   }}]
Test.updateIntfKInt =
  \ (@ t_aCK)
    (@ t1_aCM)
    (@ t2_aCN)
    (@ r_aGB)
    ($dIntegral_aJl :: GHC.Real.Integral t2_aCN)
    _
    (eta1_X9T :: Test.K t2_aCN t1_aCM) ->
    let {
      $dNum_s1s6 [Dmd=Just U(AATATAAAL)] :: GHC.Num.Num t2_aCN
      [LclId, Str=DmdType]
      $dNum_s1s6 =
        GHC.Real.$p1Real
          @ t2_aCN (GHC.Real.$p1Integral @ t2_aCN $dIntegral_aJl) } in
    case GHC.Real.even
           @ t2_aCN
           $dIntegral_aJl
           (eta1_X9T
            `cast` (Test.NTCo:K t2_aCN t1_aCM
                    :: Test.K t2_aCN t1_aCM ~ t2_aCN))
    of _ {
      GHC.Bool.False ->
        (GHC.Num.+
           @ t2_aCN
           $dNum_s1s6
           (eta1_X9T
            `cast` (Test.NTCo:K t2_aCN t1_aCM
                    :: Test.K t2_aCN t1_aCM ~ t2_aCN))
           (GHC.Num.fromInteger @ t2_aCN $dNum_s1s6 lvl9_r1v5))
        `cast` (sym (Test.NTCo:K t2_aCN r_aGB)
                :: t2_aCN ~ Test.K t2_aCN r_aGB);
      GHC.Bool.True ->
        (GHC.Num.-
           @ t2_aCN
           $dNum_s1s6
           (eta1_X9T
            `cast` (Test.NTCo:K t2_aCN t1_aCM
                    :: Test.K t2_aCN t1_aCM ~ t2_aCN))
           (GHC.Num.fromInteger @ t2_aCN $dNum_s1s6 lvl9_r1v5))
        `cast` (sym (Test.NTCo:K t2_aCN r_aGB)
                :: t2_aCN ~ Test.K t2_aCN r_aGB)
    }

Test.updateIntfI [InlPrag=INLINE]
  :: forall t_aJS r_aJV.
     (t_aJS -> r_aJV) -> Test.I t_aJS -> Test.I r_aJV
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= (\ (@ t_XOO)
                  (@ r_XOS)
                  (f_aif [Occ=Once!] :: t_XOO -> r_XOS)
                  (ds_d14U [Occ=Once] :: Test.I t_XOO) ->
                  f_aif (ds_d14U `cast` (Test.NTCo:I t_XOO :: Test.I t_XOO ~ t_XOO)))
               `cast` (forall t_XOO r_XOS.
                       (t_XOO -> r_XOS) -> Test.I t_XOO -> sym (Test.NTCo:I r_XOS)
                       :: (forall t_XOO r_XOS. (t_XOO -> r_XOS) -> Test.I t_XOO -> r_XOS)
                            ~
                          (forall t_XOO r_XOS.
                           (t_XOO -> r_XOS) -> Test.I t_XOO -> Test.I r_XOS))}]
Test.updateIntfI =
  a_r1vZ
  `cast` (forall t_aJS r_aJV.
          (t_aJS -> r_aJV) -> Test.I t_aJS -> sym (Test.NTCo:I r_aJV)
          :: (forall t_aJS r_aJV. (t_aJS -> r_aJV) -> Test.I t_aJS -> r_aJV)
               ~
             (forall t_aJS r_aJV.
              (t_aJS -> r_aJV) -> Test.I t_aJS -> Test.I r_aJV))

Test.$fUpdateIntI :: Test.UpdateInt Test.I
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateIntI =
  (\ (@ a_a11d) -> Test.updateIntfI @ a_a11d @ a_a11d)
  `cast` (sym (Test.NTCo:T:UpdateInt Test.I)
          :: (forall a_ahO. (a_ahO -> a_ahO) -> Test.I a_ahO -> Test.I a_ahO)
               ~
             Test.T:UpdateInt Test.I)

Test.updateIntfU [InlPrag=INLINE]
  :: forall t_aK0 a_aK5. t_aK0 -> a_aK5 -> a_aK5
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= \ (@ t_aK0) (@ a_aK5) _ -> GHC.Base.id @ a_aK5}]
Test.updateIntfU =
  \ (@ t_aK0) (@ a_aK5) _ (eta1_B1 :: a_aK5) -> eta1_B1

Test.$fUpdateIntU :: Test.UpdateInt Test.U
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_a11j) ->
                  Test.updateIntfU @ (a_a11j -> a_a11j) @ (Test.U a_a11j))
               `cast` (sym (Test.NTCo:T:UpdateInt Test.U)
                       :: (forall a_ahO. (a_ahO -> a_ahO) -> Test.U a_ahO -> Test.U a_ahO)
                            ~
                          Test.T:UpdateInt Test.U)}]
Test.$fUpdateIntU =
  (\ (@ a_a11j) ->
     Test.updateIntfU @ (a_a11j -> a_a11j) @ (Test.U a_a11j))
  `cast` (sym (Test.NTCo:T:UpdateInt Test.U)
          :: (forall a_ahO. (a_ahO -> a_ahO) -> Test.U a_ahO -> Test.U a_ahO)
               ~
             Test.T:UpdateInt Test.U)

Test.unK :: forall a_aic r_aid. Test.K a_aic r_aid -> a_aic
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unK =
  Test.unK1
  `cast` (forall a_aw7 r_aw8.
          Test.K a_aw7 r_aw8 -> Test.NTCo:K a_aw7 r_aw8
          :: (forall a_aw7 r_aw8. Test.K a_aw7 r_aw8 -> Test.K a_aw7 r_aw8)
               ~
             (forall a_aw7 r_aw8. Test.K a_aw7 r_aw8 -> a_aw7))

Test.unI :: forall r_aib. Test.I r_aib -> r_aib
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unI =
  Test.unI1
  `cast` (forall r_aw5. Test.I r_aw5 -> Test.NTCo:I r_aw5
          :: (forall r_aw5. Test.I r_aw5 -> Test.I r_aw5)
               ~
             (forall r_aw5. Test.I r_aw5 -> r_aw5))

Test.unC
  :: forall c_ai1 (f_ai2 :: * -> *) r_ai3.
     Test.C c_ai1 f_ai2 r_ai3 -> f_ai2 r_ai3
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(S),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aw1)
                 (@ f_aw2::* -> *)
                 (@ r_aw3)
                 (ds_d155 [Occ=Once!] :: Test.C c_aw1 f_aw2 r_aw3) ->
                 case ds_d155 of _ { Test.C ds1_d156 [Occ=Once] -> ds1_d156 }}]
Test.unC =
  \ (@ c_aw1)
    (@ f_aw2::* -> *)
    (@ r_aw3)
    (ds_d155 :: Test.C c_aw1 f_aw2 r_aw3) ->
    case ds_d155 of _ { Test.C ds1_d156 -> ds1_d156 }

Test.$fConstructorOne_One__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.One_One_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_a10b::* -> (* -> *) -> * -> *)
                 (@ f_a10c::* -> *)
                 (@ r_a10d)
                 _ ->
                 Test.$fConstructorOne_One_1}]
Test.$fConstructorOne_One__$cconName =
  \ (@ t_a10b::* -> (* -> *) -> * -> *)
    (@ f_a10c::* -> *)
    (@ r_a10d)
    _ ->
    Test.$fConstructorOne_One_1

Test.$fConstructorOne_One_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.One_One_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorOne_One__$cconName,
                              Test.$dmconFixity_$s$dmconFixity,
                              Test.$dmconIsRecord_$s$dmconIsRecord]]
Test.$fConstructorOne_One_ =
  Test.D:Constructor
    @ Test.One_One_
    Test.$fConstructorOne_One__$cconName
    Test.$dmconFixity_$s$dmconFixity
    Test.$dmconIsRecord_$s$dmconIsRecord

Test.$fConstructorTwo_Two0__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Two_Two0_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aZg::* -> (* -> *) -> * -> *)
                 (@ f_aZh::* -> *)
                 (@ r_aZi)
                 _ ->
                 Test.$fConstructorTwo_Two0_1}]
Test.$fConstructorTwo_Two0__$cconName =
  \ (@ t_aZg::* -> (* -> *) -> * -> *)
    (@ f_aZh::* -> *)
    (@ r_aZi)
    _ ->
    Test.$fConstructorTwo_Two0_1

Test.$fConstructorTwo_Two0_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Two_Two0_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorTwo_Two0__$cconName,
                              Test.$dmconFixity_$s$dmconFixity1,
                              Test.$dmconIsRecord_$s$dmconIsRecord1]]
Test.$fConstructorTwo_Two0_ =
  Test.D:Constructor
    @ Test.Two_Two0_
    Test.$fConstructorTwo_Two0__$cconName
    Test.$dmconFixity_$s$dmconFixity1
    Test.$dmconIsRecord_$s$dmconIsRecord1

Test.$fConstructorTwo_Two__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Two_Two_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aZ4::* -> (* -> *) -> * -> *)
                 (@ f_aZ5::* -> *)
                 (@ r_aZ6)
                 _ ->
                 Test.$fConstructorTwo_Two_1}]
Test.$fConstructorTwo_Two__$cconName =
  \ (@ t_aZ4::* -> (* -> *) -> * -> *)
    (@ f_aZ5::* -> *)
    (@ r_aZ6)
    _ ->
    Test.$fConstructorTwo_Two_1

Test.$fConstructorTwo_Two_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Two_Two_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorTwo_Two__$cconName,
                              Test.$dmconFixity_$s$dmconFixity2,
                              Test.$dmconIsRecord_$s$dmconIsRecord2]]
Test.$fConstructorTwo_Two_ =
  Test.D:Constructor
    @ Test.Two_Two_
    Test.$fConstructorTwo_Two__$cconName
    Test.$dmconFixity_$s$dmconFixity2
    Test.$dmconIsRecord_$s$dmconIsRecord2

Test.$fConstructorThree_Three__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Three_Three_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aXX::* -> (* -> *) -> * -> *)
                 (@ f_aXY::* -> *)
                 (@ r_aXZ)
                 _ ->
                 Test.$fConstructorThree_Three_1}]
Test.$fConstructorThree_Three__$cconName =
  \ (@ t_aXX::* -> (* -> *) -> * -> *)
    (@ f_aXY::* -> *)
    (@ r_aXZ)
    _ ->
    Test.$fConstructorThree_Three_1

Test.$fConstructorThree_Three_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Three_Three_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorThree_Three__$cconName,
                              Test.$dmconFixity_$s$dmconFixity3,
                              Test.$dmconIsRecord_$s$dmconIsRecord3]]
Test.$fConstructorThree_Three_ =
  Test.D:Constructor
    @ Test.Three_Three_
    Test.$fConstructorThree_Three__$cconName
    Test.$dmconFixity_$s$dmconFixity3
    Test.$dmconIsRecord_$s$dmconIsRecord3

Test.$fConstructorThree_Three0__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Three_Three0_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aXL::* -> (* -> *) -> * -> *)
                 (@ f_aXM::* -> *)
                 (@ r_aXN)
                 _ ->
                 Test.$fConstructorThree_Three0_1}]
Test.$fConstructorThree_Three0__$cconName =
  \ (@ t_aXL::* -> (* -> *) -> * -> *)
    (@ f_aXM::* -> *)
    (@ r_aXN)
    _ ->
    Test.$fConstructorThree_Three0_1

Test.$fConstructorThree_Three0_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Three_Three0_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorThree_Three0__$cconName,
                              Test.$dmconFixity_$s$dmconFixity4,
                              Test.$dmconIsRecord_$s$dmconIsRecord4]]
Test.$fConstructorThree_Three0_ =
  Test.D:Constructor
    @ Test.Three_Three0_
    Test.$fConstructorThree_Three0__$cconName
    Test.$dmconFixity_$s$dmconFixity4
    Test.$dmconIsRecord_$s$dmconIsRecord4

Test.$fConstructorThree_Three1__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Three_Three1_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aXz::* -> (* -> *) -> * -> *)
                 (@ f_aXA::* -> *)
                 (@ r_aXB)
                 _ ->
                 Test.$fConstructorThree_Three1_1}]
Test.$fConstructorThree_Three1__$cconName =
  \ (@ t_aXz::* -> (* -> *) -> * -> *)
    (@ f_aXA::* -> *)
    (@ r_aXB)
    _ ->
    Test.$fConstructorThree_Three1_1

Test.$fConstructorThree_Three1_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Three_Three1_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorThree_Three1__$cconName,
                              Test.$dmconFixity_$s$dmconFixity5,
                              Test.$dmconIsRecord_$s$dmconIsRecord5]]
Test.$fConstructorThree_Three1_ =
  Test.D:Constructor
    @ Test.Three_Three1_
    Test.$fConstructorThree_Three1__$cconName
    Test.$dmconFixity_$s$dmconFixity5
    Test.$dmconIsRecord_$s$dmconIsRecord5

Test.$fConstructorFour_Four__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Four_Four_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aWg::* -> (* -> *) -> * -> *)
                 (@ f_aWh::* -> *)
                 (@ r_aWi)
                 _ ->
                 Test.$fConstructorFour_Four_1}]
Test.$fConstructorFour_Four__$cconName =
  \ (@ t_aWg::* -> (* -> *) -> * -> *)
    (@ f_aWh::* -> *)
    (@ r_aWi)
    _ ->
    Test.$fConstructorFour_Four_1

Test.$fConstructorFour_Four_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Four_Four_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorFour_Four__$cconName,
                              Test.$dmconFixity_$s$dmconFixity6,
                              Test.$dmconIsRecord_$s$dmconIsRecord6]]
Test.$fConstructorFour_Four_ =
  Test.D:Constructor
    @ Test.Four_Four_
    Test.$fConstructorFour_Four__$cconName
    Test.$dmconFixity_$s$dmconFixity6
    Test.$dmconIsRecord_$s$dmconIsRecord6

Test.$fConstructorFour_Four0__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Four_Four0_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aW4::* -> (* -> *) -> * -> *)
                 (@ f_aW5::* -> *)
                 (@ r_aW6)
                 _ ->
                 Test.$fConstructorFour_Four0_1}]
Test.$fConstructorFour_Four0__$cconName =
  \ (@ t_aW4::* -> (* -> *) -> * -> *)
    (@ f_aW5::* -> *)
    (@ r_aW6)
    _ ->
    Test.$fConstructorFour_Four0_1

Test.$fConstructorFour_Four0_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Four_Four0_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorFour_Four0__$cconName,
                              Test.$dmconFixity_$s$dmconFixity7,
                              Test.$dmconIsRecord_$s$dmconIsRecord7]]
Test.$fConstructorFour_Four0_ =
  Test.D:Constructor
    @ Test.Four_Four0_
    Test.$fConstructorFour_Four0__$cconName
    Test.$dmconFixity_$s$dmconFixity7
    Test.$dmconIsRecord_$s$dmconIsRecord7

Test.$fConstructorFour_Four1__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Four_Four1_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aVS::* -> (* -> *) -> * -> *)
                 (@ f_aVT::* -> *)
                 (@ r_aVU)
                 _ ->
                 Test.$fConstructorFour_Four1_1}]
Test.$fConstructorFour_Four1__$cconName =
  \ (@ t_aVS::* -> (* -> *) -> * -> *)
    (@ f_aVT::* -> *)
    (@ r_aVU)
    _ ->
    Test.$fConstructorFour_Four1_1

Test.$fConstructorFour_Four1_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Four_Four1_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorFour_Four1__$cconName,
                              Test.$dmconFixity_$s$dmconFixity8,
                              Test.$dmconIsRecord_$s$dmconIsRecord8]]
Test.$fConstructorFour_Four1_ =
  Test.D:Constructor
    @ Test.Four_Four1_
    Test.$fConstructorFour_Four1__$cconName
    Test.$dmconFixity_$s$dmconFixity8
    Test.$dmconIsRecord_$s$dmconIsRecord8

Test.$fConstructorFour_Four2__$cconName
  :: forall (t_ahS :: * -> (* -> *) -> * -> *)
            (f_ahT :: * -> *)
            r_ahU.
     t_ahS Test.Four_Four2_ f_ahT r_ahU -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aVG::* -> (* -> *) -> * -> *)
                 (@ f_aVH::* -> *)
                 (@ r_aVI)
                 _ ->
                 Test.$fConstructorFour_Four2_1}]
Test.$fConstructorFour_Four2__$cconName =
  \ (@ t_aVG::* -> (* -> *) -> * -> *)
    (@ f_aVH::* -> *)
    (@ r_aVI)
    _ ->
    Test.$fConstructorFour_Four2_1

Test.$fConstructorFour_Four2_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Four_Four2_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorFour_Four2__$cconName,
                              Test.$dmconFixity_$s$dmconFixity9,
                              Test.$dmconIsRecord_$s$dmconIsRecord9]]
Test.$fConstructorFour_Four2_ =
  Test.D:Constructor
    @ Test.Four_Four2_
    Test.$fConstructorFour_Four2__$cconName
    Test.$dmconFixity_$s$dmconFixity9
    Test.$dmconIsRecord_$s$dmconIsRecord9

Test.$fReadFixity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Fixity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run @ [Test.Fixity] Test.$fReadFixity4

Test.$fReadFixity_$creadsPrec
  :: GHC.Types.Int -> Text.ParserCombinators.ReadP.ReadS Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 6 6}]
Test.$fReadFixity_$creadsPrec =
  \ (eta_a1ai :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Fixity
      (((GHC.Read.$dmreadsPrec3
           @ Test.Fixity
           (Test.$fReadFixity3
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_a19d.
                           (Test.Fixity -> Text.ParserCombinators.ReadP.P b_a19d)
                           -> Text.ParserCombinators.ReadP.P b_a19d)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
           eta_a1ai)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity
                :: Text.ParserCombinators.ReadP.ReadP Test.Fixity
                     ~
                   (forall b_a19d.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_a19d)
                    -> Text.ParserCombinators.ReadP.P b_a19d)))
         @ Test.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ Test.Fixity))

Test.$fReadFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadFixity_$creadsPrec,
                           Test.$fReadFixity_readListDefault,
                           (Test.$fReadFixity2
                            `cast` (right
                                      (inst
                                         (forall a_a19Y.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_a19Y
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_a19Y))
                                         Test.Fixity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity)),
                           (Test.$fReadFixity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))]]
Test.$fReadFixity =
  GHC.Read.D:Read
    @ Test.Fixity
    Test.$fReadFixity_$creadsPrec
    Test.$fReadFixity_readListDefault
    (Test.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
    (Test.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))

Test.$fOrdFixity_$ccompare
  :: Test.Fixity -> Test.Fixity -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [6 6] 33 0}]
Test.$fOrdFixity_$ccompare =
  \ (a2_ava :: Test.Fixity) (b_avb :: Test.Fixity) ->
    let {
      $j_s1sa :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      [LclId, Arity=1, Str=DmdType L]
      $j_s1sa =
        \ (a#_avh :: GHC.Prim.Int#) ->
          let {
            $j1_s1s8 :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            [LclId, Arity=1, Str=DmdType L]
            $j1_s1s8 =
              \ (b#_avi :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_avh b#_avi of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_avh b#_avi of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a2_ava of _ {
                      Test.Prefix -> GHC.Ordering.EQ;
                      Test.Infix a11_avd a21_ave ->
                        case b_avb of _ {
                          Test.Prefix -> GHC.Ordering.EQ;
                          Test.Infix b1_avf b2_avg ->
                            case a11_avd of _ {
                              Test.LeftAssociative ->
                                case b1_avf of _ {
                                  Test.LeftAssociative -> GHC.Base.compareInt a21_ave b2_avg;
                                  Test.RightAssociative -> GHC.Ordering.LT;
                                  Test.NotAssociative -> GHC.Ordering.LT
                                };
                              Test.RightAssociative ->
                                case b1_avf of _ {
                                  Test.LeftAssociative -> GHC.Ordering.GT;
                                  Test.RightAssociative -> GHC.Base.compareInt a21_ave b2_avg;
                                  Test.NotAssociative -> GHC.Ordering.LT
                                };
                              Test.NotAssociative ->
                                case b1_avf of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  Test.NotAssociative -> GHC.Base.compareInt a21_ave b2_avg
                                }
                            }
                        }
                    }
                } } in
          case b_avb of _ {
            Test.Prefix -> $j1_s1s8 0; Test.Infix _ _ -> $j1_s1s8 1
          } } in
    case a2_ava of _ {
      Test.Prefix -> $j_s1sa 0; Test.Infix _ _ -> $j_s1sa 1
    }

Test.$fOrdFixity_$c< :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c< =
  \ (x_a1ce :: Test.Fixity) (y_a1cf :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_a1ce y_a1cf of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

Test.$fOrdFixity_$c>=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c>= =
  \ (x_a1c7 :: Test.Fixity) (y_a1c8 :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_a1c7 y_a1c8 of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

Test.$fOrdFixity_$c> :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c> =
  \ (x_a1c0 :: Test.Fixity) (y_a1c1 :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_a1c0 y_a1c1 of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

Test.$fOrdFixity_$c<=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c<= =
  \ (x_a1bT :: Test.Fixity) (y_a1bU :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_a1bT y_a1bU of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

Test.$fOrdFixity_$cmax :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 0}]
Test.$fOrdFixity_$cmax =
  \ (x_a1bK :: Test.Fixity) (y_a1bL :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_a1bK y_a1bL of _ {
      __DEFAULT -> y_a1bL; GHC.Ordering.GT -> x_a1bK
    }

Test.$fOrdFixity_$cmin :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 0}]
Test.$fOrdFixity_$cmin =
  \ (x_a1bB :: Test.Fixity) (y_a1bC :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_a1bB y_a1bC of _ {
      __DEFAULT -> x_a1bB; GHC.Ordering.GT -> y_a1bC
    }

Test.$fShowFixity_$cshowsPrec
  :: GHC.Types.Int -> Test.Fixity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType LSL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 5 0] 46 3}]
Test.$fShowFixity_$cshowsPrec =
  \ (ds_d15t :: GHC.Types.Int)
    (ds1_d15u :: Test.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_d15u of _ {
      Test.Prefix ->
        GHC.Base.++ @ GHC.Types.Char Test.$fShowFixity3 eta_B1;
      Test.Infix b1_av8 b2_av9 ->
        case ds_d15t of _ { GHC.Types.I# x_a19H ->
        let {
          p_s1sc :: GHC.Show.ShowS
          [LclId, Arity=1, Str=DmdType L]
          p_s1sc =
            \ (x1_X1hN :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                Test.$fShowFixity2
                (case b1_av8 of _ {
                   Test.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_av9 of _ { GHC.Types.I# ww_a1nA ->
                           GHC.Show.$wshowSignedInt 11 ww_a1nA x1_X1hN
                           }));
                   Test.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_av9 of _ { GHC.Types.I# ww_a1nA ->
                           GHC.Show.$wshowSignedInt 11 ww_a1nA x1_X1hN
                           }));
                   Test.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_av9 of _ { GHC.Types.I# ww_a1nA ->
                           GHC.Show.$wshowSignedInt 11 ww_a1nA x1_X1hN
                           }))
                 }) } in
        case GHC.Prim.>=# x_a19H 11 of _ {
          GHC.Bool.False -> p_s1sc eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.$dmshow6
              (p_s1sc (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 eta_B1))
        }
        }
    }

Test.$fShowFixity_$cshow :: Test.Fixity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 4 0}]
Test.$fShowFixity_$cshow =
  \ (x_X1mh :: Test.Fixity) ->
    Test.$fShowFixity_$cshowsPrec
      GHC.Base.zeroInt x_X1mh (GHC.Types.[] @ GHC.Types.Char)

Test.$fShowFixity_$cshowList :: [Test.Fixity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 25 3}]
Test.$fShowFixity_$cshowList =
  \ (ds1_a1aR :: [Test.Fixity]) (s_a1aS :: GHC.Base.String) ->
    case ds1_a1aR of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_a1aS;
      : x_a1aX xs_a1aY ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (Test.$fShowFixity_$cshowsPrec
             Test.$fShowFixity1
             x_a1aX
             (let {
                lvl14_s1se :: [GHC.Types.Char]
                [LclId, Str=DmdType]
                lvl14_s1se =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1aS } in
              letrec {
                showl_s1sg [Occ=LoopBreaker] :: [Test.Fixity] -> [GHC.Types.Char]
                [LclId, Arity=1, Str=DmdType S]
                showl_s1sg =
                  \ (ds2_a1b2 :: [Test.Fixity]) ->
                    case ds2_a1b2 of _ {
                      [] -> lvl14_s1se;
                      : y_a1b7 ys_a1b8 ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (Test.$fShowFixity_$cshowsPrec
                             Test.$fShowFixity1 y_a1b7 (showl_s1sg ys_a1b8))
                    }; } in
              showl_s1sg xs_a1aY))
    }

Test.$fShowFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowFixity_$cshowsPrec,
                           Test.$fShowFixity_$cshow, Test.$fShowFixity_$cshowList]]
Test.$fShowFixity =
  GHC.Show.D:Show
    @ Test.Fixity
    Test.$fShowFixity_$cshowsPrec
    Test.$fShowFixity_$cshow
    Test.$fShowFixity_$cshowList

Test.$fEqFixity_$c== :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 4}]
Test.$fEqFixity_$c== =
  \ (ds_d15n :: Test.Fixity) (ds1_d15o :: Test.Fixity) ->
    case ds_d15n of _ {
      Test.Prefix ->
        case ds1_d15o of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix a11_auX a2_auY ->
        case ds1_d15o of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix b1_auZ b2_av0 ->
            case a11_auX of _ {
              Test.LeftAssociative ->
                case b1_auZ of _ {
                  Test.LeftAssociative ->
                    case a2_auY of _ { GHC.Types.I# x_a19R ->
                    case b2_av0 of _ { GHC.Types.I# y_a19V ->
                    GHC.Prim.==# x_a19R y_a19V
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case b1_auZ of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case a2_auY of _ { GHC.Types.I# x_a19R ->
                    case b2_av0 of _ { GHC.Types.I# y_a19V ->
                    GHC.Prim.==# x_a19R y_a19V
                    }
                    }
                };
              Test.NotAssociative ->
                case b1_auZ of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case a2_auY of _ { GHC.Types.I# x_a19R ->
                    case b2_av0 of _ { GHC.Types.I# y_a19V ->
                    GHC.Prim.==# x_a19R y_a19V
                    }
                    }
                }
            }
        }
    }

Test.$fEqFixity_$c/= :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fEqFixity_$c/= =
  \ (a2_av5 :: Test.Fixity) (b_av6 :: Test.Fixity) ->
    case Test.$fEqFixity_$c== a2_av5 b_av6 of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

Test.$fEqFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqFixity_$c==,
                            Test.$fEqFixity_$c/=]]
Test.$fEqFixity =
  GHC.Classes.D:Eq
    @ Test.Fixity Test.$fEqFixity_$c== Test.$fEqFixity_$c/=

Test.$fOrdFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqFixity,
                             Test.$fOrdFixity_$ccompare, Test.$fOrdFixity_$c<,
                             Test.$fOrdFixity_$c>=, Test.$fOrdFixity_$c>, Test.$fOrdFixity_$c<=,
                             Test.$fOrdFixity_$cmax, Test.$fOrdFixity_$cmin]]
Test.$fOrdFixity =
  GHC.Classes.D:Ord
    @ Test.Fixity
    Test.$fEqFixity
    Test.$fOrdFixity_$ccompare
    Test.$fOrdFixity_$c<
    Test.$fOrdFixity_$c>=
    Test.$fOrdFixity_$c>
    Test.$fOrdFixity_$c<=
    Test.$fOrdFixity_$cmax
    Test.$fOrdFixity_$cmin

Test.$fReadAssociativity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Associativity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [Test.Associativity] Test.$fReadAssociativity16

Test.$fReadAssociativity_$creadsPrec
  :: GHC.Types.Int
     -> Text.ParserCombinators.ReadP.ReadS Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 6 6}]
Test.$fReadAssociativity_$creadsPrec =
  \ (eta_a1ai :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Associativity
      (((GHC.Read.$dmreadsPrec3
           @ Test.Associativity Test.$fReadAssociativity3 eta_a1ai)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                     ~
                   (forall b_a19d.
                    (Test.Associativity -> Text.ParserCombinators.ReadP.P b_a19d)
                    -> Text.ParserCombinators.ReadP.P b_a19d)))
         @ Test.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn
            @ Test.Associativity))

Test.$fReadAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadAssociativity_$creadsPrec,
                           Test.$fReadAssociativity_readListDefault,
                           (Test.$fReadAssociativity2
                            `cast` (right
                                      (inst
                                         (forall a_a19Y.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_a19Y
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_a19Y))
                                         Test.Associativity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         Test.Associativity)),
                           (Test.$fReadAssociativity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                         [Test.Associativity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         [Test.Associativity]))]]
Test.$fReadAssociativity =
  GHC.Read.D:Read
    @ Test.Associativity
    Test.$fReadAssociativity_$creadsPrec
    Test.$fReadAssociativity_readListDefault
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))
    (Test.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [Test.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Associativity]))

Test.$fOrdAssociativity_$cmin
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmin =
  \ (x_a1bB :: Test.Associativity) (y_a1bC :: Test.Associativity) ->
    case x_a1bB of _ {
      Test.LeftAssociative ->
        case y_a1bC of _ { __DEFAULT -> Test.LeftAssociative };
      Test.RightAssociative ->
        case y_a1bC of _ {
          Test.LeftAssociative -> Test.LeftAssociative;
          Test.RightAssociative -> Test.RightAssociative;
          Test.NotAssociative -> Test.RightAssociative
        };
      Test.NotAssociative -> y_a1bC
    }

Test.$fOrdAssociativity_$cmax
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmax =
  \ (x_a1bK :: Test.Associativity) (y_a1bL :: Test.Associativity) ->
    case x_a1bK of _ {
      Test.LeftAssociative -> y_a1bL;
      Test.RightAssociative ->
        case y_a1bL of _ {
          __DEFAULT -> Test.RightAssociative;
          Test.NotAssociative -> Test.NotAssociative
        };
      Test.NotAssociative ->
        case y_a1bL of _ { __DEFAULT -> Test.NotAssociative }
    }

Test.$fOrdAssociativity_$c<=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c<= =
  \ (x_a1bT :: Test.Associativity) (y_a1bU :: Test.Associativity) ->
    case x_a1bT of _ {
      Test.LeftAssociative ->
        case y_a1bU of _ { __DEFAULT -> GHC.Bool.True };
      Test.RightAssociative ->
        case y_a1bU of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_a1bU of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fOrdAssociativity_$c>
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c> =
  \ (x_a1c0 :: Test.Associativity) (y_a1c1 :: Test.Associativity) ->
    case x_a1c0 of _ {
      Test.LeftAssociative ->
        case y_a1c1 of _ { __DEFAULT -> GHC.Bool.False };
      Test.RightAssociative ->
        case y_a1c1 of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_a1c1 of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fOrdAssociativity_$c>=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c>= =
  \ (x_a1c7 :: Test.Associativity) (y_a1c8 :: Test.Associativity) ->
    case x_a1c7 of _ {
      Test.LeftAssociative ->
        case y_a1c8 of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case y_a1c8 of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_a1c8 of _ { __DEFAULT -> GHC.Bool.True }
    }

Test.$fOrdAssociativity_$c<
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c< =
  \ (x_a1ce :: Test.Associativity) (y_a1cf :: Test.Associativity) ->
    case x_a1ce of _ {
      Test.LeftAssociative ->
        case y_a1cf of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case y_a1cf of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_a1cf of _ { __DEFAULT -> GHC.Bool.False }
    }

Test.$fOrdAssociativity_$ccompare
  :: Test.Associativity
     -> Test.Associativity
     -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$ccompare =
  \ (a2_auS :: Test.Associativity) (b_auT :: Test.Associativity) ->
    case a2_auS of _ {
      Test.LeftAssociative ->
        case b_auT of _ {
          Test.LeftAssociative -> GHC.Ordering.EQ;
          Test.RightAssociative -> GHC.Ordering.LT;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.RightAssociative ->
        case b_auT of _ {
          Test.LeftAssociative -> GHC.Ordering.GT;
          Test.RightAssociative -> GHC.Ordering.EQ;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.NotAssociative ->
        case b_auT of _ {
          __DEFAULT -> GHC.Ordering.GT;
          Test.NotAssociative -> GHC.Ordering.EQ
        }
    }

Test.$fShowAssociativity_$cshowList
  :: [Test.Associativity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 42 3}]
Test.$fShowAssociativity_$cshowList =
  \ (ds1_a1aR :: [Test.Associativity]) (s_a1aS :: GHC.Base.String) ->
    case ds1_a1aR of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_a1aS;
      : x_a1aX xs_a1aY ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1sm [Dmd=Just L] :: GHC.Base.String
             [LclId, Str=DmdType]
             eta_s1sm =
               let {
                 lvl14_s1si :: [GHC.Types.Char]
                 [LclId, Str=DmdType]
                 lvl14_s1si =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_a1aS } in
               letrec {
                 showl_s1sk [Occ=LoopBreaker]
                   :: [Test.Associativity] -> [GHC.Types.Char]
                 [LclId, Arity=1, Str=DmdType S]
                 showl_s1sk =
                   \ (ds2_a1b2 :: [Test.Associativity]) ->
                     case ds2_a1b2 of _ {
                       [] -> lvl14_s1si;
                       : y_a1b7 ys_a1b8 ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_a1b7 of _ {
                              Test.LeftAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity15 (showl_s1sk ys_a1b8);
                              Test.RightAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity12 (showl_s1sk ys_a1b8);
                              Test.NotAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1sk ys_a1b8)
                            })
                     }; } in
               showl_s1sk xs_a1aY } in
           case x_a1aX of _ {
             Test.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_s1sm;
             Test.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_s1sm;
             Test.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_s1sm
           })
    }

Test.$fShowAssociativity_$cshow
  :: Test.Associativity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0}]
Test.$fShowAssociativity_$cshow =
  \ (x_a1bh :: Test.Associativity) ->
    case x_a1bh of _ {
      Test.LeftAssociative -> Test.$fReadAssociativity15;
      Test.RightAssociative -> Test.$fReadAssociativity12;
      Test.NotAssociative -> Test.$fReadAssociativity9
    }

Test.$fShowAssociativity_$cshowsPrec
  :: GHC.Types.Int -> Test.Associativity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType ASL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, -)
         Tmpl= \ _
                 (ds1_d15d [Occ=Once!] :: Test.Associativity)
                 (eta_B1 [Occ=Once*] :: GHC.Base.String) ->
                 case ds1_d15d of _ {
                   Test.LeftAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a1mE)
                          (c_a1mF [Occ=Once] :: GHC.Types.Char -> b_a1mE -> b_a1mE)
                          (n_a1mG [Occ=Once] :: b_a1mE) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a1mE c_a1mF n_a1mG Test.$fReadAssociativity15)
                       eta_B1;
                   Test.RightAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a1mE)
                          (c_a1mF [Occ=Once] :: GHC.Types.Char -> b_a1mE -> b_a1mE)
                          (n_a1mG [Occ=Once] :: b_a1mE) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a1mE c_a1mF n_a1mG Test.$fReadAssociativity12)
                       eta_B1;
                   Test.NotAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a1mE)
                          (c_a1mF [Occ=Once] :: GHC.Types.Char -> b_a1mE -> b_a1mE)
                          (n_a1mG [Occ=Once] :: b_a1mE) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a1mE c_a1mF n_a1mG Test.$fReadAssociativity9)
                       eta_B1
                 }}]
Test.$fShowAssociativity_$cshowsPrec =
  \ _ (ds1_d15d :: Test.Associativity) (eta_B1 :: GHC.Base.String) ->
    case ds1_d15d of _ {
      Test.LeftAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_B1;
      Test.RightAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_B1;
      Test.NotAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_B1
    }

Test.$fShowAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowAssociativity_$cshowsPrec,
                           Test.$fShowAssociativity_$cshow,
                           Test.$fShowAssociativity_$cshowList]]
Test.$fShowAssociativity =
  GHC.Show.D:Show
    @ Test.Associativity
    Test.$fShowAssociativity_$cshowsPrec
    Test.$fShowAssociativity_$cshow
    Test.$fShowAssociativity_$cshowList

Test.$fEqAssociativity_$c/=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c/= =
  \ (a2_auQ :: Test.Associativity) (b_auR :: Test.Associativity) ->
    case a2_auQ of _ {
      Test.LeftAssociative ->
        case b_auR of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case b_auR of _ {
          __DEFAULT -> GHC.Bool.True; Test.RightAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case b_auR of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fEqAssociativity_$c==
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c== =
  \ (a2_auM :: Test.Associativity) (b_auN :: Test.Associativity) ->
    case a2_auM of _ {
      Test.LeftAssociative ->
        case b_auN of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case b_auN of _ {
          __DEFAULT -> GHC.Bool.False; Test.RightAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case b_auN of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fEqAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqAssociativity_$c==,
                            Test.$fEqAssociativity_$c/=]]
Test.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ Test.Associativity
    Test.$fEqAssociativity_$c==
    Test.$fEqAssociativity_$c/=

Test.$fOrdAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqAssociativity,
                             Test.$fOrdAssociativity_$ccompare, Test.$fOrdAssociativity_$c<,
                             Test.$fOrdAssociativity_$c>=, Test.$fOrdAssociativity_$c>,
                             Test.$fOrdAssociativity_$c<=, Test.$fOrdAssociativity_$cmax,
                             Test.$fOrdAssociativity_$cmin]]
Test.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ Test.Associativity
    Test.$fEqAssociativity
    Test.$fOrdAssociativity_$ccompare
    Test.$fOrdAssociativity_$c<
    Test.$fOrdAssociativity_$c>=
    Test.$fOrdAssociativity_$c>
    Test.$fOrdAssociativity_$c<=
    Test.$fOrdAssociativity_$cmax
    Test.$fOrdAssociativity_$cmin


------ Local rules for imported ids --------
"SPEC Test.$dmconIsRecord [Test.Four_Four2_]" ALWAYS
    forall {$dConstructor_s1ed [Occ=Dead]
              :: Test.Constructor Test.Four_Four2_}
      Test.$dmconIsRecord @ Test.Four_Four2_ $dConstructor_s1ed
      = Test.$dmconIsRecord_$s$dmconIsRecord9
"SPEC Test.$dmconIsRecord [Test.Four_Four1_]" ALWAYS
    forall {$dConstructor_s1eg [Occ=Dead]
              :: Test.Constructor Test.Four_Four1_}
      Test.$dmconIsRecord @ Test.Four_Four1_ $dConstructor_s1eg
      = Test.$dmconIsRecord_$s$dmconIsRecord8
"SPEC Test.$dmconIsRecord [Test.Four_Four0_]" ALWAYS
    forall {$dConstructor_s1ej [Occ=Dead]
              :: Test.Constructor Test.Four_Four0_}
      Test.$dmconIsRecord @ Test.Four_Four0_ $dConstructor_s1ej
      = Test.$dmconIsRecord_$s$dmconIsRecord7
"SPEC Test.$dmconIsRecord [Test.Four_Four_]" ALWAYS
    forall {$dConstructor_s1em [Occ=Dead]
              :: Test.Constructor Test.Four_Four_}
      Test.$dmconIsRecord @ Test.Four_Four_ $dConstructor_s1em
      = Test.$dmconIsRecord_$s$dmconIsRecord6
"SPEC Test.$dmconIsRecord [Test.Three_Three1_]" ALWAYS
    forall {$dConstructor_s1ep [Occ=Dead]
              :: Test.Constructor Test.Three_Three1_}
      Test.$dmconIsRecord @ Test.Three_Three1_ $dConstructor_s1ep
      = Test.$dmconIsRecord_$s$dmconIsRecord5
"SPEC Test.$dmconIsRecord [Test.Three_Three0_]" ALWAYS
    forall {$dConstructor_s1es [Occ=Dead]
              :: Test.Constructor Test.Three_Three0_}
      Test.$dmconIsRecord @ Test.Three_Three0_ $dConstructor_s1es
      = Test.$dmconIsRecord_$s$dmconIsRecord4
"SPEC Test.$dmconIsRecord [Test.Three_Three_]" ALWAYS
    forall {$dConstructor_s1ev [Occ=Dead]
              :: Test.Constructor Test.Three_Three_}
      Test.$dmconIsRecord @ Test.Three_Three_ $dConstructor_s1ev
      = Test.$dmconIsRecord_$s$dmconIsRecord3
"SPEC Test.$dmconIsRecord [Test.Two_Two_]" ALWAYS
    forall {$dConstructor_s1ey [Occ=Dead]
              :: Test.Constructor Test.Two_Two_}
      Test.$dmconIsRecord @ Test.Two_Two_ $dConstructor_s1ey
      = Test.$dmconIsRecord_$s$dmconIsRecord2
"SPEC Test.$dmconIsRecord [Test.Two_Two0_]" ALWAYS
    forall {$dConstructor_s1eB [Occ=Dead]
              :: Test.Constructor Test.Two_Two0_}
      Test.$dmconIsRecord @ Test.Two_Two0_ $dConstructor_s1eB
      = Test.$dmconIsRecord_$s$dmconIsRecord1
"SPEC Test.$dmconIsRecord [Test.One_One_]" ALWAYS
    forall {$dConstructor_s1eE [Occ=Dead]
              :: Test.Constructor Test.One_One_}
      Test.$dmconIsRecord @ Test.One_One_ $dConstructor_s1eE
      = Test.$dmconIsRecord_$s$dmconIsRecord
"SPEC Test.$dmconFixity [Test.Four_Four2_]" ALWAYS
    forall {$dConstructor_s1dJ [Occ=Dead]
              :: Test.Constructor Test.Four_Four2_}
      Test.$dmconFixity @ Test.Four_Four2_ $dConstructor_s1dJ
      = Test.$dmconFixity_$s$dmconFixity9
"SPEC Test.$dmconFixity [Test.Four_Four1_]" ALWAYS
    forall {$dConstructor_s1dM [Occ=Dead]
              :: Test.Constructor Test.Four_Four1_}
      Test.$dmconFixity @ Test.Four_Four1_ $dConstructor_s1dM
      = Test.$dmconFixity_$s$dmconFixity8
"SPEC Test.$dmconFixity [Test.Four_Four0_]" ALWAYS
    forall {$dConstructor_s1dP [Occ=Dead]
              :: Test.Constructor Test.Four_Four0_}
      Test.$dmconFixity @ Test.Four_Four0_ $dConstructor_s1dP
      = Test.$dmconFixity_$s$dmconFixity7
"SPEC Test.$dmconFixity [Test.Four_Four_]" ALWAYS
    forall {$dConstructor_s1dS [Occ=Dead]
              :: Test.Constructor Test.Four_Four_}
      Test.$dmconFixity @ Test.Four_Four_ $dConstructor_s1dS
      = Test.$dmconFixity_$s$dmconFixity6
"SPEC Test.$dmconFixity [Test.Three_Three1_]" ALWAYS
    forall {$dConstructor_s1dV [Occ=Dead]
              :: Test.Constructor Test.Three_Three1_}
      Test.$dmconFixity @ Test.Three_Three1_ $dConstructor_s1dV
      = Test.$dmconFixity_$s$dmconFixity5
"SPEC Test.$dmconFixity [Test.Three_Three0_]" ALWAYS
    forall {$dConstructor_s1dY [Occ=Dead]
              :: Test.Constructor Test.Three_Three0_}
      Test.$dmconFixity @ Test.Three_Three0_ $dConstructor_s1dY
      = Test.$dmconFixity_$s$dmconFixity4
"SPEC Test.$dmconFixity [Test.Three_Three_]" ALWAYS
    forall {$dConstructor_s1e1 [Occ=Dead]
              :: Test.Constructor Test.Three_Three_}
      Test.$dmconFixity @ Test.Three_Three_ $dConstructor_s1e1
      = Test.$dmconFixity_$s$dmconFixity3
"SPEC Test.$dmconFixity [Test.Two_Two_]" ALWAYS
    forall {$dConstructor_s1e4 [Occ=Dead]
              :: Test.Constructor Test.Two_Two_}
      Test.$dmconFixity @ Test.Two_Two_ $dConstructor_s1e4
      = Test.$dmconFixity_$s$dmconFixity2
"SPEC Test.$dmconFixity [Test.Two_Two0_]" ALWAYS
    forall {$dConstructor_s1e7 [Occ=Dead]
              :: Test.Constructor Test.Two_Two0_}
      Test.$dmconFixity @ Test.Two_Two0_ $dConstructor_s1e7
      = Test.$dmconFixity_$s$dmconFixity1
"SPEC Test.$dmconFixity [Test.One_One_]" ALWAYS
    forall {$dConstructor_s1ea [Occ=Dead]
              :: Test.Constructor Test.One_One_}
      Test.$dmconFixity @ Test.One_One_ $dConstructor_s1ea
      = Test.$dmconFixity_$s$dmconFixity
"SPEC Test.updateInt [Test.Four]" ALWAYS
    forall {$dRegular_s1cX :: Test.Regular Test.Four
            $dUpdateInt_X1mI :: Test.UpdateInt (Test.PF Test.Four)}
      Test.updateInt @ Test.Four $dRegular_s1cX $dUpdateInt_X1mI
      = Test.testFour_updateInt
"SPEC Test.updateInt [Test.Three]" ALWAYS
    forall {$dRegular_s1d9 :: Test.Regular Test.Three
            $dUpdateInt_X1mU :: Test.UpdateInt (Test.PF Test.Three)}
      Test.updateInt @ Test.Three $dRegular_s1d9 $dUpdateInt_X1mU
      = Test.testThree_updateInt
"SPEC Test.updateInt [Test.Two]" ALWAYS
    forall {$dRegular_s1dl :: Test.Regular Test.Two
            $dUpdateInt_X1n6 :: Test.UpdateInt (Test.PF Test.Two)}
      Test.updateInt @ Test.Two $dRegular_s1dl $dUpdateInt_X1n6
      = Test.testTwo_updateInt
"SPEC Test.updateInt [Test.One]" ALWAYS
    forall {$dRegular_s1dx :: Test.Regular Test.One
            $dUpdateInt_X1ni :: Test.UpdateInt (Test.PF Test.One)}
      Test.updateInt @ Test.One $dRegular_s1dx $dUpdateInt_X1ni
      = Test.updateInt_updateInt
"SPEC Test.updateIntfKInt [GHC.Types.Int]" ALWAYS
    forall {@ t_aCK
            @ t1_aCM
            @ r_aGB
            $dIntegral_s1cJ :: GHC.Real.Integral GHC.Types.Int}
      Test.updateIntfKInt @ t_aCK
                          @ t1_aCM
                          @ GHC.Types.Int
                          @ r_aGB
                          $dIntegral_s1cJ
      = Test.$fUpdateIntK0_$supdateIntfKInt @ t_aCK @ t1_aCM @ r_aGB


