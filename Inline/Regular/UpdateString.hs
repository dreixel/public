{-# OPTIONS_GHC -fglasgow-exts    #-}
{-# LANGUAGE OverlappingInstances #-}

module Test where

-----------------------------------------------------------------------------
-- Functorial structural representation types.
-----------------------------------------------------------------------------

-- | Structure type for constant values.
newtype K a r    = K { unK :: a }

-- | Structure type for recursive values.
newtype I r      = I { unI :: r }

-- | Structure type for empty constructors.
data U r         = U

-- | Structure type for alternatives in a type.
data (f :+: g) r = L (f r) | R (g r)

-- | Structure type for fields of a constructor.
data (f :*: g) r = f r :*: g r

-- | Structure type to store the name of a constructor.
data C c f r =  C { unC :: f r }

infixr 6 :+:
infixr 7 :*:

-- | Class for datatypes that represent data constructors.
-- For non-symbolic constructors, only 'conName' has to be defined.
-- The weird argument is supposed to be instantiated with 'C' from
-- base, hence the complex kind.
class Constructor c where
  conName   :: t c (f :: * -> *) r -> String
  conFixity :: t c (f :: * -> *) r -> Fixity
  conFixity = const Prefix
  conIsRecord :: t c (f :: * -> *) r -> Bool
  conIsRecord = const False

-- | Datatype to represent the fixity of a constructor. An infix declaration
-- directly corresponds to an application of 'Infix'.
data Fixity = Prefix | Infix Associativity Int
  deriving (Eq, Show, Ord, Read)

data Associativity = LeftAssociative | RightAssociative | NotAssociative
  deriving (Eq, Show, Ord, Read)

-----------------------------------------------------------------------------
-- Type class capturing the structural representation of a type and the
-- corresponding embedding-projection pairs.
-----------------------------------------------------------------------------
-- | The type family @PF@ represents the pattern functor of a datatype.
-- 
-- To be able to use the generic functions, the user is required to provide
-- an instance of this type family.
type family PF a :: * -> *

-- | The type class @Regular@ captures the structural representation of a 
-- type and the corresponding embedding-projection pairs.
--
-- To be able to use the generic functions, the user is required to provide
-- an instance of this type class.
class Regular a where
  from      :: a -> PF a a
  to        :: PF a a -> a

-----------------------------------------------------------------------------
-- Generic update function: updates all Strings, leaves the rest unchanged.
-----------------------------------------------------------------------------

class UpdateString f where
  updateStringf :: (a -> a) -> f a -> f a

instance UpdateString U where
  updateStringf = updateStringfU

instance UpdateString I where
  updateStringf = updateStringfI

instance UpdateString (K String) where
  updateStringf = updateStringfKString

instance UpdateString (K x) where
  updateStringf = updateStringfK

instance (UpdateString f, UpdateString g) => UpdateString (f :+: g) where
  updateStringf = updateStringfPlus

instance (UpdateString f, UpdateString g) => UpdateString (f :*: g) where
  updateStringf = updateStringfTimes

instance (UpdateString f) => UpdateString (C c f) where
  updateStringf = updateStringfC

{-# INLINE updateStringfU #-}
updateStringfU _ = id

{-# INLINE updateStringfI #-}
updateStringfI f (I x) = I (f x)

{-# INLINE updateStringfKString #-}
updateStringfKString f (K "") = K ""
updateStringfKString f (K s)  = K (last s : tail s)

{-# INLINE updateStringfK #-}
updateStringfK f (K x) = K x

{-# INLINE updateStringfPlus #-}
updateStringfPlus f (L x) = L (updateStringf f x)
updateStringfPlus f (R x) = R (updateStringf f x)

{-# INLINE updateStringfTimes #-}
updateStringfTimes f (x :*: y) = updateStringf f x :*: updateStringf f y

{-# INLINE updateStringfC #-}
updateStringfC f (C x) = C (updateStringf f x)


{-# INLINE updateString #-}
updateString :: (Regular a, UpdateString (PF a)) => a -> a
updateString = to . updateStringf (updateString) . from

-----------------------------------------------------------------------------
-- Test datatypes and their generic representations
-----------------------------------------------------------------------------

data Logic = Var String
           | T                            -- true
           | F                            -- false
           | Not Logic                    -- not
           | Impl  Logic Logic            -- implication
           | Equiv Logic Logic            -- equivalence
           | Conj  Logic Logic            -- and (conjunction)
           | Disj  Logic Logic            -- or (disjunction)


data Logic_Var_
data Logic_Impl_
data Logic_Equiv_
data Logic_Conj_
data Logic_Disj_
data Logic_Not_
data Logic_T_
data Logic_F_

instance Constructor Logic_Var_ where
    { conName _ = "Var" }
instance Constructor Logic_Impl_ where
    { conName _ = "Impl" }
instance Constructor Logic_Equiv_ where
    { conName _ = "Equiv" }
instance Constructor Logic_Conj_ where
    { conName _ = "Conj" }
instance Constructor Logic_Disj_ where
    { conName _ = "Disj" }
instance Constructor Logic_Not_ where
    { conName _ = "Not" }
instance Constructor Logic_T_ where
    { conName _ = "T" }
instance Constructor Logic_F_ where
    { conName _ = "F" }
    
type PFLogic =
      (C Logic_Var_ (K String))
  :+: (C Logic_T_ U)
  :+: (C Logic_F_ U)
  :+: (C Logic_Not_ I)
  :+: (C Logic_Impl_ ((:*:) I I))
  :+: (C Logic_Equiv_ ((:*:) I I))
  :+: (C Logic_Conj_ ((:*:) I I))
  :+: (C Logic_Disj_ ((:*:) I I))

type instance PF Logic = PFLogic
instance Regular Logic where
  from = fromLogic  
  to   = toLogic

fromLogic (Var f0) = L (C (K f0))
fromLogic T = R (L (C U))
fromLogic F = R (R (L (C U)))
fromLogic (Not f0) = R (R (R (L (C (I f0)))))
fromLogic (Impl f0 f1) = R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))
fromLogic (Equiv f0 f1) = R (R (R (R (R (L (C ((:*:) (I f0) (I f1))))))))
fromLogic (Conj f0 f1) = R (R (R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))))
fromLogic (Disj f0 f1) = R (R (R (R (R (R (R (C ((:*:) (I f0) (I f1)))))))))

toLogic (L (C (K f0))) = Var f0
toLogic (R (L (C U))) = T
toLogic (R (R (L (C U)))) = F
toLogic (R (R (R (L (C (I f0)))))) = Not f0
toLogic (R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))) = Impl f0 f1
toLogic (R (R (R (R (R (L (C ((:*:) (I f0) (I f1))))))))) = Equiv f0 f1
toLogic (R (R (R (R (R (R (L (C ((:*:) (I f0) (I f1)))))))))) = Conj f0 f1
toLogic (R (R (R (R (R (R (R (C ((:*:) (I f0) (I f1)))))))))) = Disj f0 f1

-----------------------------------------------------------------------------
-- INLINE pragmas for the generic representations
-----------------------------------------------------------------------------

{-# INLINE fromLogic #-}
{-# INLINE toLogic #-}

-----------------------------------------------------------------------------
-- Test values
-----------------------------------------------------------------------------

logic :: Logic
logic = Impl (Not T) (Var "x")

testLogic = updateString logic
