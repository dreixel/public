
==================== Tidy Core ====================
Test.updateStringf
  :: forall (f_aiK :: * -> *).
     (Test.UpdateString f_aiK) =>
     forall a_aiL. (a_aiL -> a_aiL) -> f_aiK a_aiL -> f_aiK a_aiL
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.updateStringf =
  \ (@ f_aiK::* -> *)
    (tpl_B1 [Occ=Once] :: Test.UpdateString f_aiK) ->
    tpl_B1
    `cast` (Test.NTCo:T:UpdateString f_aiK
            :: Test.T:UpdateString f_aiK
                 ~
               (forall a_aiL. (a_aiL -> a_aiL) -> f_aiK a_aiL -> f_aiK a_aiL))

Test.from [InlPrag=NOINLINE]
  :: forall a_aiM.
     (Test.Regular a_aiM) =>
     a_aiM -> Test.PF a_aiM a_aiM
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.from: "Class op from"]
Test.from =
  \ (@ a_aiM) (tpl_B1 [Occ=Once!] :: Test.Regular a_aiM) ->
    case tpl_B1 of _ { Test.D:Regular tpl_B2 [Occ=Once] _ -> tpl_B2 }

Test.to [InlPrag=NOINLINE]
  :: forall a_aiM.
     (Test.Regular a_aiM) =>
     Test.PF a_aiM a_aiM -> a_aiM
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.to: "Class op to"]
Test.to =
  \ (@ a_aiM) (tpl_B1 [Occ=Once!] :: Test.Regular a_aiM) ->
    case tpl_B1 of _ { Test.D:Regular _ tpl_B3 [Occ=Once] -> tpl_B3 }

Test.conName [InlPrag=NOINLINE]
  :: forall c_aiO.
     (Test.Constructor c_aiO) =>
     forall (t_aiP :: * -> (* -> *) -> * -> *) (f_aiQ :: * -> *) r_aiR.
     t_aiP c_aiO f_aiQ r_aiR -> GHC.Base.String
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SAA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conName: "Class op conName"]
Test.conName =
  \ (@ c_aiO) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiO) ->
    case tpl_B1 of _ { Test.D:Constructor tpl_B2 [Occ=Once] _ _ ->
    tpl_B2
    }

Test.conFixity [InlPrag=NOINLINE]
  :: forall c_aiO.
     (Test.Constructor c_aiO) =>
     forall (t_aiS :: * -> (* -> *) -> * -> *) (f_aiT :: * -> *) r_aiU.
     t_aiS c_aiO f_aiT r_aiU -> Test.Fixity
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(ASA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conFixity: "Class op conFixity"]
Test.conFixity =
  \ (@ c_aiO) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiO) ->
    case tpl_B1 of _ { Test.D:Constructor _ tpl_B3 [Occ=Once] _ ->
    tpl_B3
    }

Test.conIsRecord [InlPrag=NOINLINE]
  :: forall c_aiO.
     (Test.Constructor c_aiO) =>
     forall (t_aiV :: * -> (* -> *) -> * -> *) (f_aiW :: * -> *) r_aiX.
     t_aiV c_aiO f_aiW r_aiX -> GHC.Bool.Bool
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AAS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conIsRecord: "Class op conIsRecord"]
Test.conIsRecord =
  \ (@ c_aiO) (tpl_B1 [Occ=Once!] :: Test.Constructor c_aiO) ->
    case tpl_B1 of _ { Test.D:Constructor _ _ tpl_B4 [Occ=Once] ->
    tpl_B4
    }

Test.$fShowFixity2 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity2 = GHC.Base.unpackCString# "Infix "

Test.$fShowFixity3 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity3 = GHC.Base.unpackCString# "Prefix"

Test.$fReadAssociativity9 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

Test.$fReadAssociativity12 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

Test.$fReadAssociativity15 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

lvl_r1ln :: [GHC.Types.Char]
[GblId, Str=DmdType]
lvl_r1ln = GHC.Base.unpackCString# "Infix"

Test.$fConstructorLogic_Var_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Var_1 = GHC.Base.unpackCString# "Var"

Test.$fConstructorLogic_Impl_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Impl_1 = GHC.Base.unpackCString# "Impl"

Test.$fConstructorLogic_Equiv_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorLogic_Equiv_1 = GHC.Base.unpackCString# "Equiv"

Test.$fConstructorLogic_Conj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Conj_1 = GHC.Base.unpackCString# "Conj"

Test.$fConstructorLogic_Disj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Disj_1 = GHC.Base.unpackCString# "Disj"

Test.$fConstructorLogic_Not_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Not_1 = GHC.Base.unpackCString# "Not"

Test.$fConstructorLogic_T_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_T_2 = GHC.Types.C# 'T'

Test.$fConstructorLogic_T_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_T_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_T_2
    (GHC.Types.[] @ GHC.Types.Char)

Test.$fConstructorLogic_F_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_F_2 = GHC.Types.C# 'F'

Test.$fConstructorLogic_F_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_F_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_F_2
    (GHC.Types.[] @ GHC.Types.Char)

lvl1_r1lp :: forall c_awi. Test.C c_awi Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl1_r1lp =
  \ (@ c_awi) ->
    Test.C @ c_awi @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl2_r1lr
  :: forall c_awi c1_aww c2_awP c3_axb c4_axA c5_ay0.
     (Test.:+:)
       (Test.C c_awi Test.U)
       (Test.C c1_aww Test.I
        Test.:+: (Test.C c2_awP (Test.I Test.:*: Test.I)
                  Test.:+: (Test.C c3_axb (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axA (Test.I Test.:*: Test.I)
                                      Test.:+: Test.C c5_ay0 (Test.I Test.:*: Test.I)))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl2_r1lr =
  \ (@ c_awi)
    (@ c1_aww)
    (@ c2_awP)
    (@ c3_axb)
    (@ c4_axA)
    (@ c5_ay0) ->
    Test.L
      @ (Test.C c_awi Test.U)
      @ (Test.C c1_aww Test.I
         Test.:+: (Test.C c2_awP (Test.I Test.:*: Test.I)
                   Test.:+: (Test.C c3_axb (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axA (Test.I Test.:*: Test.I)
                                       Test.:+: Test.C c5_ay0 (Test.I Test.:*: Test.I)))))
      @ Test.Logic
      (lvl1_r1lp @ c_awi)

lvl3_r1lt
  :: forall c_aw6 c1_awi c2_aww c3_awP c4_axb c5_axA c6_ay0.
     (Test.:+:)
       (Test.C c_aw6 Test.U)
       (Test.C c1_awi Test.U
        Test.:+: (Test.C c2_aww Test.I
                  Test.:+: (Test.C c3_awP (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axb (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axA (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay0 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl3_r1lt =
  \ (@ c_aw6)
    (@ c1_awi)
    (@ c2_aww)
    (@ c3_awP)
    (@ c4_axb)
    (@ c5_axA)
    (@ c6_ay0) ->
    Test.R
      @ (Test.C c_aw6 Test.U)
      @ (Test.C c1_awi Test.U
         Test.:+: (Test.C c2_aww Test.I
                   Test.:+: (Test.C c3_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay0 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl2_r1lr @ c1_awi @ c2_aww @ c3_awP @ c4_axb @ c5_axA @ c6_ay0)

lvl4_r1lv
  :: forall c_avW c1_aw6 c2_awi c3_aww c4_awP c5_axb c6_axA c7_ay0.
     (Test.:+:)
       (Test.C c_avW (Test.K GHC.Base.String))
       (Test.C c1_aw6 Test.U
        Test.:+: (Test.C c2_awi Test.U
                  Test.:+: (Test.C c3_aww Test.I
                            Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay0
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl4_r1lv =
  \ (@ c_avW)
    (@ c1_aw6)
    (@ c2_awi)
    (@ c3_aww)
    (@ c4_awP)
    (@ c5_axb)
    (@ c6_axA)
    (@ c7_ay0) ->
    Test.R
      @ (Test.C c_avW (Test.K GHC.Base.String))
      @ (Test.C c1_aw6 Test.U
         Test.:+: (Test.C c2_awi Test.U
                   Test.:+: (Test.C c3_aww Test.I
                             Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay0
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl3_r1lt
         @ c1_aw6 @ c2_awi @ c3_aww @ c4_awP @ c5_axb @ c6_axA @ c7_ay0)

lvl5_r1lx :: forall c_aw6. Test.C c_aw6 Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl5_r1lx =
  \ (@ c_aw6) ->
    Test.C @ c_aw6 @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl6_r1lz
  :: forall c_aw6 c1_awi c2_aww c3_awP c4_axb c5_axA c6_ay0.
     (Test.:+:)
       (Test.C c_aw6 Test.U)
       (Test.C c1_awi Test.U
        Test.:+: (Test.C c2_aww Test.I
                  Test.:+: (Test.C c3_awP (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axb (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axA (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay0 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl6_r1lz =
  \ (@ c_aw6)
    (@ c1_awi)
    (@ c2_aww)
    (@ c3_awP)
    (@ c4_axb)
    (@ c5_axA)
    (@ c6_ay0) ->
    Test.L
      @ (Test.C c_aw6 Test.U)
      @ (Test.C c1_awi Test.U
         Test.:+: (Test.C c2_aww Test.I
                   Test.:+: (Test.C c3_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay0 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl5_r1lx @ c_aw6)

lvl7_r1lB
  :: forall c_avW c1_aw6 c2_awi c3_aww c4_awP c5_axb c6_axA c7_ay0.
     (Test.:+:)
       (Test.C c_avW (Test.K GHC.Base.String))
       (Test.C c1_aw6 Test.U
        Test.:+: (Test.C c2_awi Test.U
                  Test.:+: (Test.C c3_aww Test.I
                            Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay0
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl7_r1lB =
  \ (@ c_avW)
    (@ c1_aw6)
    (@ c2_awi)
    (@ c3_aww)
    (@ c4_awP)
    (@ c5_axb)
    (@ c6_axA)
    (@ c7_ay0) ->
    Test.R
      @ (Test.C c_avW (Test.K GHC.Base.String))
      @ (Test.C c1_aw6 Test.U
         Test.:+: (Test.C c2_awi Test.U
                   Test.:+: (Test.C c3_aww Test.I
                             Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay0
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl6_r1lz
         @ c1_aw6 @ c2_awi @ c3_aww @ c4_awP @ c5_axb @ c6_axA @ c7_ay0)

Test.$dmconFixity_$s$dmconFixity7
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_F_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity7 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity6
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_T_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity6 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity5
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Not_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity5 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity4
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Disj_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity4 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity3
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Conj_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity3 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity2
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Equiv_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity2 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity1
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Impl_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity1 =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity
  :: forall (t_aiS :: * -> (* -> *) -> * -> *)
            (f_aiT :: * -> *)
            r_aiU.
     t_aiS Test.Logic_Var_ f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity =
  \ (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconIsRecord_$s$dmconIsRecord7
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_F_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord7 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord6
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_T_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord6 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord5
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Not_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord5 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord4
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Disj_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord4 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord3
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Conj_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord3 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord2
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Equiv_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord2 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord1
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Impl_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord1 =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord
  :: forall (t_aiV :: * -> (* -> *) -> * -> *)
            (f_aiW :: * -> *)
            r_aiX.
     t_aiV Test.Logic_Var_ f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord =
  \ (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.$fShowFixity1 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fShowFixity1 = GHC.Types.I# 0

Test.$fReadAssociativity14
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWi.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWi)
        -> Text.ParserCombinators.ReadP.P b_aWi
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity14 =
  \ _
    (@ b_aWi)
    (eta_aWj
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWi) ->
    eta_aWj Test.LeftAssociative

Test.$fReadAssociativity13
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity13 =
  (Test.$fReadAssociativity15,
   Test.$fReadAssociativity14
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWA.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                  -> Text.ParserCombinators.ReadP.P b_aWA)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity11
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWi.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWi)
        -> Text.ParserCombinators.ReadP.P b_aWi
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity11 =
  \ _
    (@ b_aWi)
    (eta_aWj
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWi) ->
    eta_aWj Test.RightAssociative

Test.$fReadAssociativity10
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity10 =
  (Test.$fReadAssociativity12,
   Test.$fReadAssociativity11
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWA.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                  -> Text.ParserCombinators.ReadP.P b_aWA)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity8
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWi.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWi)
        -> Text.ParserCombinators.ReadP.P b_aWi
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity8 =
  \ _
    (@ b_aWi)
    (eta_aWj
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWi) ->
    eta_aWj Test.NotAssociative

Test.$fReadAssociativity7
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity7 =
  (Test.$fReadAssociativity9,
   Test.$fReadAssociativity8
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWA.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                  -> Text.ParserCombinators.ReadP.P b_aWA)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity6
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity5
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity10
    Test.$fReadAssociativity6

Test.$fReadAssociativity4
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity13
    Test.$fReadAssociativity5

Test.$fReadAssociativity3
  :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity3 =
  GHC.Read.choose1 @ Test.Associativity Test.$fReadAssociativity4

Test.$fReadAssociativity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Associativity Test.$fReadAssociativity3

Test.$fReadAssociativity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ Test.Associativity
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity16
  :: Text.ParserCombinators.ReadP.P [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadAssociativity16 =
  ((Test.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [Test.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
                ~
              (forall b_aWA.
               ([Test.Associativity] -> Text.ParserCombinators.ReadP.P b_aWA)
               -> Text.ParserCombinators.ReadP.P b_aWA)))
    @ [Test.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn
       @ [Test.Associativity])

lvl8_r1lD
  :: forall b_X10x.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10x
[GblId, Arity=1, Caf=NoCafRefs]
lvl8_r1lD =
  \ (@ b_X10x) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10x
    }

lvl9_r1lF :: forall b_X10x. Text.ParserCombinators.ReadP.P b_X10x
[GblId, Caf=NoCafRefs]
lvl9_r1lF =
  \ (@ b_X10x) ->
    Text.ParserCombinators.ReadP.Look @ b_X10x (lvl8_r1lD @ b_X10x)

lvl10_r1lH
  :: forall b_X10x.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10x
[GblId, Arity=1, Caf=NoCafRefs]
lvl10_r1lH =
  \ (@ b_X10x) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10x
    }

lvl11_r1lJ :: forall b_X10x. Text.ParserCombinators.ReadP.P b_X10x
[GblId, Caf=NoCafRefs]
lvl11_r1lJ =
  \ (@ b_X10x) ->
    Text.ParserCombinators.ReadP.Look @ b_X10x (lvl10_r1lH @ b_X10x)

lvl12_r1lL :: GHC.Types.Int
[GblId, Caf=NoCafRefs]
lvl12_r1lL = GHC.Types.I# 11

lvl13_r1lN
  :: forall b_a15H.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15H
[GblId, Arity=1, Caf=NoCafRefs]
lvl13_r1lN =
  \ (@ b_a15H) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15H
    }

lvl14_r1lP :: forall b_a15H. Text.ParserCombinators.ReadP.P b_a15H
[GblId, Caf=NoCafRefs]
lvl14_r1lP =
  \ (@ b_a15H) ->
    Text.ParserCombinators.ReadP.Look @ b_a15H (lvl13_r1lN @ b_a15H)

lvl15_r1lR
  :: forall b_a15H.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15H
[GblId, Arity=1, Caf=NoCafRefs]
lvl15_r1lR =
  \ (@ b_a15H) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15H
    }

lvl16_r1lT :: forall b_a15H. Text.ParserCombinators.ReadP.P b_a15H
[GblId, Caf=NoCafRefs]
lvl16_r1lT =
  \ (@ b_a15H) ->
    Text.ParserCombinators.ReadP.Look @ b_a15H (lvl15_r1lR @ b_a15H)

lvl17_r1lV
  :: forall b_X10x.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10x
[GblId, Arity=1, Caf=NoCafRefs]
lvl17_r1lV =
  \ (@ b_X10x) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10x
    }

lvl18_r1lX :: forall b_X10x. Text.ParserCombinators.ReadP.P b_X10x
[GblId, Caf=NoCafRefs]
lvl18_r1lX =
  \ (@ b_X10x) ->
    Text.ParserCombinators.ReadP.Look @ b_X10x (lvl17_r1lV @ b_X10x)

lvl19_r1lZ
  :: forall b_X10x.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_X10x
[GblId, Arity=1, Caf=NoCafRefs]
lvl19_r1lZ =
  \ (@ b_X10x) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_X10x
    }

lvl20_r1m1 :: forall b_X10x. Text.ParserCombinators.ReadP.P b_X10x
[GblId, Caf=NoCafRefs]
lvl20_r1m1 =
  \ (@ b_X10x) ->
    Text.ParserCombinators.ReadP.Look @ b_X10x (lvl19_r1lZ @ b_X10x)

Rec {
a1_r1m3
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId, Arity=1, Str=DmdType L]
a1_r1m3 =
  \ (n_X1aw :: Text.ParserCombinators.ReadPrec.Prec) ->
    let {
      ds2_s1jt [Dmd=Just L]
        :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
      [LclId, Str=DmdType]
      ds2_s1jt =
        (Test.$fReadAssociativity3
         `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                   Test.Associativity
                 :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
                      ~
                    (Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)))
          n_X1aw } in
    (\ (@ b_a15H)
       (k1_a15I
          :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a15H) ->
       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
         @ b_a15H
         ((ds2_s1jt
           `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                   :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                        ~
                      (forall b_aWA.
                       (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                       -> Text.ParserCombinators.ReadP.P b_aWA)))
            @ b_a15H k1_a15I)
         (Text.ParserCombinators.ReadP.Look
            @ b_a15H
            (let {
               $wk_s1jH [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15H
               [LclId, Str=DmdType]
               $wk_s1jH =
                 Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                   @ b_a15H
                   (lvl14_r1lP @ b_a15H)
                   (Text.Read.Lex.hsLex2
                      @ b_a15H
                      (let {
                         lvl81_s1jD :: Text.ParserCombinators.ReadP.P b_a15H
                         [LclId, Str=DmdType]
                         lvl81_s1jD =
                           ((a1_r1m3 Text.ParserCombinators.ReadPrec.minPrec)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                                    :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                         ~
                                       (forall b_aWA.
                                        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                                        -> Text.ParserCombinators.ReadP.P b_aWA)))
                             @ b_a15H
                             (\ (a12_a162 :: Test.Associativity) ->
                                Text.ParserCombinators.ReadP.Look
                                  @ b_a15H
                                  (let {
                                     $wk1_s1jz [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15H
                                     [LclId, Str=DmdType]
                                     $wk1_s1jz =
                                       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                                         @ b_a15H
                                         (lvl16_r1lT @ b_a15H)
                                         (Text.Read.Lex.hsLex2
                                            @ b_a15H
                                            (let {
                                               lvl91_s1jv :: Text.ParserCombinators.ReadP.P b_a15H
                                               [LclId, Str=DmdType]
                                               lvl91_s1jv = k1_a15I a12_a162 } in
                                             \ (a111_a164 :: Text.Read.Lex.Lexeme) ->
                                               case a111_a164 of _ {
                                                 __DEFAULT ->
                                                   Text.ParserCombinators.ReadP.Fail @ b_a15H;
                                                 Text.Read.Lex.Punc ds_a167 ->
                                                   case ds_a167 of _ {
                                                     [] ->
                                                       Text.ParserCombinators.ReadP.Fail @ b_a15H;
                                                     : ds1_a16c ds21_a16d ->
                                                       case ds1_a16c of _ { GHC.Types.C# ds3_a16h ->
                                                       case ds3_a16h of _ {
                                                         __DEFAULT ->
                                                           Text.ParserCombinators.ReadP.Fail
                                                             @ b_a15H;
                                                         ')' ->
                                                           case ds21_a16d of _ {
                                                             [] -> lvl91_s1jv;
                                                             : _ _ ->
                                                               Text.ParserCombinators.ReadP.Fail
                                                                 @ b_a15H
                                                           }
                                                       }
                                                       }
                                                   }
                                               })) } in
                                   let {
                                     k_s1jB :: () -> Text.ParserCombinators.ReadP.P b_a15H
                                     [LclId, Arity=1, Str=DmdType A]
                                     k_s1jB = \ _ -> $wk1_s1jz } in
                                   \ (a111_a17d :: GHC.Base.String) ->
                                     ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                                              :: Text.ParserCombinators.ReadP.ReadP ()
                                                   ~
                                                 (forall b_aWA.
                                                  (() -> Text.ParserCombinators.ReadP.P b_aWA)
                                                  -> Text.ParserCombinators.ReadP.P b_aWA)))
                                       @ b_a15H k_s1jB)) } in
                       \ (a12_a16p :: Text.Read.Lex.Lexeme) ->
                         case a12_a16p of _ {
                           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15H;
                           Text.Read.Lex.Punc ds_a16s ->
                             case ds_a16s of _ {
                               [] -> Text.ParserCombinators.ReadP.Fail @ b_a15H;
                               : ds1_a16x ds21_a16y ->
                                 case ds1_a16x of _ { GHC.Types.C# ds3_a16C ->
                                 case ds3_a16C of _ {
                                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15H;
                                   '(' ->
                                     case ds21_a16y of _ {
                                       [] -> lvl81_s1jD;
                                       : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a15H
                                     }
                                 }
                                 }
                             }
                         })) } in
             let {
               k_s1jJ :: () -> Text.ParserCombinators.ReadP.P b_a15H
               [LclId, Arity=1, Str=DmdType A]
               k_s1jJ = \ _ -> $wk_s1jH } in
             \ (a111_a17d :: GHC.Base.String) ->
               ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                        :: Text.ParserCombinators.ReadP.ReadP ()
                             ~
                           (forall b_aWA.
                            (() -> Text.ParserCombinators.ReadP.P b_aWA)
                            -> Text.ParserCombinators.ReadP.P b_aWA)))
                 @ b_a15H k_s1jJ)))
    `cast` (sym
              (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity)
            :: (forall b_aWA.
                (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                -> Text.ParserCombinators.ReadP.P b_aWA)
                 ~
               Text.ParserCombinators.ReadP.ReadP Test.Associativity)
end Rec }

Rec {
Test.$fReadFixity3
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_X10x.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_X10x)
        -> Text.ParserCombinators.ReadP.P b_X10x
[GblId, Arity=2, Str=DmdType LL]
Test.$fReadFixity3 =
  \ (n_a15F :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_X10x)
    (eta_X3Z
       :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_X10x) ->
    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
      @ b_X10x
      (Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
         @ b_X10x
         (Text.ParserCombinators.ReadP.Look
            @ b_X10x
            (let {
               $wk_s1jl [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_X10x
               [LclId, Str=DmdType]
               $wk_s1jl =
                 Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                   @ b_X10x
                   (lvl20_r1m1 @ b_X10x)
                   (Text.Read.Lex.hsLex2
                      @ b_X10x
                      (let {
                         lvl25_s1l0 :: Text.ParserCombinators.ReadP.P b_X10x
                         [LclId]
                         lvl25_s1l0 = eta_X3Z Test.Prefix } in
                       \ (a12_aWO :: Text.Read.Lex.Lexeme) ->
                         case a12_aWO of _ {
                           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                           Text.Read.Lex.Ident ds_dT4 ->
                             case GHC.Base.eqString ds_dT4 Test.$fShowFixity3 of _ {
                               GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                               GHC.Bool.True -> lvl25_s1l0
                             }
                         })) } in
             let {
               k_s1jn :: () -> Text.ParserCombinators.ReadP.P b_X10x
               [LclId, Arity=1, Str=DmdType A]
               k_s1jn = \ _ -> $wk_s1jl } in
             \ (a111_a17d :: GHC.Base.String) ->
               ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                        :: Text.ParserCombinators.ReadP.ReadP ()
                             ~
                           (forall b_aWA.
                            (() -> Text.ParserCombinators.ReadP.P b_aWA)
                            -> Text.ParserCombinators.ReadP.P b_aWA)))
                 @ b_X10x k_s1jn))
         (case n_a15F of _ { GHC.Types.I# x_a17F ->
          case GHC.Prim.<=# x_a17F 10 of _ {
            GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
            GHC.Bool.True ->
              Text.ParserCombinators.ReadP.Look
                @ b_X10x
                (let {
                   $wk_s1jO :: Text.ParserCombinators.ReadP.P b_X10x
                   [LclId, Str=DmdType]
                   $wk_s1jO =
                     Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                       @ b_X10x
                       (lvl18_r1lX @ b_X10x)
                       (Text.Read.Lex.hsLex2
                          @ b_X10x
                          (let {
                             lvl25_s1l3 :: Text.ParserCombinators.ReadP.P b_X10x
                             [LclId]
                             lvl25_s1l3 =
                               ((a1_r1m3 lvl12_r1lL)
                                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                                        :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                             ~
                                           (forall b_aWA.
                                            (Test.Associativity
                                             -> Text.ParserCombinators.ReadP.P b_aWA)
                                            -> Text.ParserCombinators.ReadP.P b_aWA)))
                                 @ b_X10x
                                 (\ (a12_X153 :: Test.Associativity) ->
                                    ((GHC.Read.$dmreadsPrec11 lvl12_r1lL)
                                     `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                                             :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                                  ~
                                                (forall b_aWA.
                                                 (GHC.Types.Int
                                                  -> Text.ParserCombinators.ReadP.P b_aWA)
                                                 -> Text.ParserCombinators.ReadP.P b_aWA)))
                                      @ b_X10x
                                      (\ (a13_X159 :: GHC.Types.Int) ->
                                         eta_X3Z (Test.Infix a12_X153 a13_X159))) } in
                           \ (a12_aWO :: Text.Read.Lex.Lexeme) ->
                             case a12_aWO of _ {
                               __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                               Text.Read.Lex.Ident ds_dT9 ->
                                 case GHC.Base.eqString ds_dT9 lvl_r1ln of _ {
                                   GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                                   GHC.Bool.True -> lvl25_s1l3
                                 }
                             })) } in
                 let {
                   k_s1jQ :: () -> Text.ParserCombinators.ReadP.P b_X10x
                   [LclId, Arity=1, Str=DmdType A]
                   k_s1jQ = \ _ -> $wk_s1jO } in
                 \ (a111_a17d :: GHC.Base.String) ->
                   ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                    `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                            :: Text.ParserCombinators.ReadP.ReadP ()
                                 ~
                               (forall b_aWA.
                                (() -> Text.ParserCombinators.ReadP.P b_aWA)
                                -> Text.ParserCombinators.ReadP.P b_aWA)))
                     @ b_X10x k_s1jQ)
          }
          }))
      (Text.ParserCombinators.ReadP.Look
         @ b_X10x
         (let {
            $wk_s1k4 [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_X10x
            [LclId, Str=DmdType]
            $wk_s1k4 =
              Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                @ b_X10x
                (lvl9_r1lF @ b_X10x)
                (Text.Read.Lex.hsLex2
                   @ b_X10x
                   (let {
                      lvl81_s1k0 :: Text.ParserCombinators.ReadP.P b_X10x
                      [LclId, Str=DmdType]
                      lvl81_s1k0 =
                        Test.$fReadFixity3
                          Text.ParserCombinators.ReadPrec.minPrec
                          @ b_X10x
                          (\ (a12_a162 :: Test.Fixity) ->
                             Text.ParserCombinators.ReadP.Look
                               @ b_X10x
                               (let {
                                  $wk1_s1jW [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_X10x
                                  [LclId, Str=DmdType]
                                  $wk1_s1jW =
                                    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                                      @ b_X10x
                                      (lvl11_r1lJ @ b_X10x)
                                      (Text.Read.Lex.hsLex2
                                         @ b_X10x
                                         (let {
                                            lvl91_s1jS :: Text.ParserCombinators.ReadP.P b_X10x
                                            [LclId, Str=DmdType]
                                            lvl91_s1jS = eta_X3Z a12_a162 } in
                                          \ (a111_a164 :: Text.Read.Lex.Lexeme) ->
                                            case a111_a164 of _ {
                                              __DEFAULT ->
                                                Text.ParserCombinators.ReadP.Fail @ b_X10x;
                                              Text.Read.Lex.Punc ds_a167 ->
                                                case ds_a167 of _ {
                                                  [] -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                                                  : ds1_a16c ds2_a16d ->
                                                    case ds1_a16c of _ { GHC.Types.C# ds3_a16h ->
                                                    case ds3_a16h of _ {
                                                      __DEFAULT ->
                                                        Text.ParserCombinators.ReadP.Fail @ b_X10x;
                                                      ')' ->
                                                        case ds2_a16d of _ {
                                                          [] -> lvl91_s1jS;
                                                          : _ _ ->
                                                            Text.ParserCombinators.ReadP.Fail
                                                              @ b_X10x
                                                        }
                                                    }
                                                    }
                                                }
                                            })) } in
                                let {
                                  k_s1jY :: () -> Text.ParserCombinators.ReadP.P b_X10x
                                  [LclId, Arity=1, Str=DmdType A]
                                  k_s1jY = \ _ -> $wk1_s1jW } in
                                \ (a111_a17d :: GHC.Base.String) ->
                                  ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                                   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                                           :: Text.ParserCombinators.ReadP.ReadP ()
                                                ~
                                              (forall b_aWA.
                                               (() -> Text.ParserCombinators.ReadP.P b_aWA)
                                               -> Text.ParserCombinators.ReadP.P b_aWA)))
                                    @ b_X10x k_s1jY)) } in
                    \ (a12_a16p :: Text.Read.Lex.Lexeme) ->
                      case a12_a16p of _ {
                        __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                        Text.Read.Lex.Punc ds_a16s ->
                          case ds_a16s of _ {
                            [] -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                            : ds1_a16x ds2_a16y ->
                              case ds1_a16x of _ { GHC.Types.C# ds3_a16C ->
                              case ds3_a16C of _ {
                                __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_X10x;
                                '(' ->
                                  case ds2_a16y of _ {
                                    [] -> lvl81_s1k0;
                                    : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_X10x
                                  }
                              }
                              }
                          }
                      })) } in
          let {
            k_s1k6 :: () -> Text.ParserCombinators.ReadP.P b_X10x
            [LclId, Arity=1, Str=DmdType A]
            k_s1k6 = \ _ -> $wk_s1k4 } in
          \ (a111_a17d :: GHC.Base.String) ->
            ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
             `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                     :: Text.ParserCombinators.ReadP.ReadP ()
                          ~
                        (forall b_aWA.
                         (() -> Text.ParserCombinators.ReadP.P b_aWA)
                         -> Text.ParserCombinators.ReadP.P b_aWA)))
              @ b_X10x k_s1k6))
end Rec }

Test.$fReadFixity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWA.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWA)
        -> Text.ParserCombinators.ReadP.P b_aWA
[GblId,
 Arity=2,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 3 0}]
Test.$fReadFixity2 =
  \ (eta_a15D :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_aWA)
    (eta1_B1 :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWA) ->
    Test.$fReadFixity3 eta_a15D @ b_aWA eta1_B1

Test.$fReadFixity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ Test.Fixity
    (Test.$fReadFixity2
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_aWA.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWA)
                    -> Text.ParserCombinators.ReadP.P b_aWA)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity4 :: Text.ParserCombinators.ReadP.P [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadFixity4 =
  ((Test.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [Test.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
                ~
              (forall b_aWA.
               ([Test.Fixity] -> Text.ParserCombinators.ReadP.P b_aWA)
               -> Text.ParserCombinators.ReadP.P b_aWA)))
    @ [Test.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ [Test.Fixity])

Test.logic3 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic3 = GHC.Types.C# 'x'

Test.logic2 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic2 =
  GHC.Types.:
    @ GHC.Types.Char Test.logic3 (GHC.Types.[] @ GHC.Types.Char)

Test.logic1 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic1 = Test.Var Test.logic2

Test.logic4 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic4 = Test.Not Test.T

a_r1m5
  :: forall t_aAu t1_aAw t2_aAx r_aAA.
     t_aAu -> Test.K t2_aAx t1_aAw -> Test.K t2_aAx t1_aAw
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType AS]
a_r1m5 =
  \ (@ t_aAu)
    (@ t1_aAw)
    (@ t2_aAx)
    (@ r_aAA)
    _
    (eta1_X7t :: Test.K t2_aAx t1_aAw) ->
    eta1_X7t

a11_r1m7
  :: forall t_aBx r_aBA. (t_aBx -> r_aBA) -> Test.I t_aBx -> r_aBA
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType C(S)L]
a11_r1m7 =
  \ (@ t_aBx)
    (@ r_aBA)
    (eta_B2 :: t_aBx -> r_aBA)
    (eta1_B1 :: Test.I t_aBx) ->
    eta_B2 (eta1_B1 `cast` (Test.NTCo:I t_aBx :: Test.I t_aBx ~ t_aBx))

Test.unI1 :: forall r_auN. Test.I r_auN -> Test.I r_auN
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unI1 = \ (@ r_auN) (ds_dSu :: Test.I r_auN) -> ds_dSu

Test.unK1
  :: forall a_auP r_auQ. Test.K a_auP r_auQ -> Test.K a_auP r_auQ
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unK1 =
  \ (@ a_auP) (@ r_auQ) (ds_dSw :: Test.K a_auP r_auQ) -> ds_dSw

Test.unK :: forall a_aj9 r_aja. Test.K a_aj9 r_aja -> a_aj9
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unK =
  Test.unK1
  `cast` (forall a_auP r_auQ.
          Test.K a_auP r_auQ -> Test.NTCo:K a_auP r_auQ
          :: (forall a_auP r_auQ. Test.K a_auP r_auQ -> Test.K a_auP r_auQ)
               ~
             (forall a_auP r_auQ. Test.K a_auP r_auQ -> a_auP))

Test.unI :: forall r_aj8. Test.I r_aj8 -> r_aj8
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unI =
  Test.unI1
  `cast` (forall r_auN. Test.I r_auN -> Test.NTCo:I r_auN
          :: (forall r_auN. Test.I r_auN -> Test.I r_auN)
               ~
             (forall r_auN. Test.I r_auN -> r_auN))

Test.unC
  :: forall c_aiY (f_aiZ :: * -> *) r_aj0.
     Test.C c_aiY f_aiZ r_aj0 -> f_aiZ r_aj0
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(S),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_auJ)
                 (@ f_auK::* -> *)
                 (@ r_auL)
                 (ds_dSr [Occ=Once!] :: Test.C c_auJ f_auK r_auL) ->
                 case ds_dSr of _ { Test.C ds1_dSs [Occ=Once] -> ds1_dSs }}]
Test.unC =
  \ (@ c_auJ)
    (@ f_auK::* -> *)
    (@ r_auL)
    (ds_dSr :: Test.C c_auJ f_auK r_auL) ->
    case ds_dSr of _ { Test.C ds1_dSs -> ds1_dSs }

Test.fromLogic [InlPrag=INLINE]
  :: forall c_avW c1_aw6 c2_awi c3_aww c4_awP c5_axb c6_axA c7_ay0.
     Test.Logic
     -> (Test.:+:)
          (Test.C c_avW (Test.K GHC.Base.String))
          (Test.C c1_aw6 Test.U
           Test.:+: (Test.C c2_awi Test.U
                     Test.:+: (Test.C c3_aww Test.I
                               Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        c7_ay0
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_avW)
                 (@ c1_aw6)
                 (@ c2_awi)
                 (@ c3_aww)
                 (@ c4_awP)
                 (@ c5_axb)
                 (@ c6_axA)
                 (@ c7_ay0)
                 (ds_dRU [Occ=Once!] :: Test.Logic) ->
                 case ds_dRU of _ {
                   Test.Var f0_ajB [Occ=Once] ->
                     Test.L
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.C
                          @ c_avW
                          @ (Test.K GHC.Base.String)
                          @ Test.Logic
                          (f0_ajB
                           `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                                   :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
                   Test.T ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.L
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.C @ c1_aw6 @ Test.U @ Test.Logic (Test.U @ Test.Logic)));
                   Test.F ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.L
                             @ (Test.C c2_awi Test.U)
                             @ (Test.C c3_aww Test.I
                                Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay0
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.C @ c2_awi @ Test.U @ Test.Logic (Test.U @ Test.Logic))));
                   Test.Not f0_ajC [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awi Test.U)
                             @ (Test.C c3_aww Test.I
                                Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay0
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.L
                                @ (Test.C c3_aww Test.I)
                                @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay0 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.C
                                   @ c3_aww
                                   @ Test.I
                                   @ Test.Logic
                                   (f0_ajC
                                    `cast` (sym (Test.NTCo:I Test.Logic)
                                            :: Test.Logic ~ Test.I Test.Logic))))));
                   Test.Impl f0_ajD [Occ=Once] f1_ajE [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awi Test.U)
                             @ (Test.C c3_aww Test.I
                                Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay0
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_aww Test.I)
                                @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay0 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.L
                                   @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.C
                                      @ c4_awP
                                      @ (Test.I Test.:*: Test.I)
                                      @ Test.Logic
                                      (Test.:*:
                                         @ Test.I
                                         @ Test.I
                                         @ Test.Logic
                                         (f0_ajD
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))
                                         (f1_ajE
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))))))));
                   Test.Equiv f0_ajF [Occ=Once] f1_ajG [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awi Test.U)
                             @ (Test.C c3_aww Test.I
                                Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay0
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_aww Test.I)
                                @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay0 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.L
                                      @ (Test.C c5_axb (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axA (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.C
                                         @ c5_axb
                                         @ (Test.I Test.:*: Test.I)
                                         @ Test.Logic
                                         (Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ Test.Logic
                                            (f0_ajF
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic))
                                            (f1_ajG
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic)))))))));
                   Test.Conj f0_ajH [Occ=Once] f1_ajI [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awi Test.U)
                             @ (Test.C c3_aww Test.I
                                Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay0
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_aww Test.I)
                                @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay0 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axb (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axA (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.L
                                         @ (Test.C c6_axA (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay0 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c6_axA
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajH
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajI
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))));
                   Test.Disj f0_ajJ [Occ=Once] f1_ajK [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avW (Test.K GHC.Base.String))
                       @ (Test.C c1_aw6 Test.U
                          Test.:+: (Test.C c2_awi Test.U
                                    Test.:+: (Test.C c3_aww Test.I
                                              Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axb (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axA
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay0
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw6 Test.U)
                          @ (Test.C c2_awi Test.U
                             Test.:+: (Test.C c3_aww Test.I
                                       Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axA
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay0
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awi Test.U)
                             @ (Test.C c3_aww Test.I
                                Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay0
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_aww Test.I)
                                @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay0 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axb (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axA (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C c6_axA (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay0 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c7_ay0
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ajJ
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ajK
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))))
                 }}]
Test.fromLogic =
  \ (@ c_avW)
    (@ c1_aw6)
    (@ c2_awi)
    (@ c3_aww)
    (@ c4_awP)
    (@ c5_axb)
    (@ c6_axA)
    (@ c7_ay0)
    (eta_B1 :: Test.Logic) ->
    case eta_B1 of _ {
      Test.Var f0_ajB ->
        Test.L
          @ (Test.C c_avW (Test.K GHC.Base.String))
          @ (Test.C c1_aw6 Test.U
             Test.:+: (Test.C c2_awi Test.U
                       Test.:+: (Test.C c3_aww Test.I
                                 Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axA (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay0
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.C
             @ c_avW
             @ (Test.K GHC.Base.String)
             @ Test.Logic
             (f0_ajB
              `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                      :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
      Test.T ->
        lvl7_r1lB
          @ c_avW
          @ c1_aw6
          @ c2_awi
          @ c3_aww
          @ c4_awP
          @ c5_axb
          @ c6_axA
          @ c7_ay0;
      Test.F ->
        lvl4_r1lv
          @ c_avW
          @ c1_aw6
          @ c2_awi
          @ c3_aww
          @ c4_awP
          @ c5_axb
          @ c6_axA
          @ c7_ay0;
      Test.Not f0_ajC ->
        Test.R
          @ (Test.C c_avW (Test.K GHC.Base.String))
          @ (Test.C c1_aw6 Test.U
             Test.:+: (Test.C c2_awi Test.U
                       Test.:+: (Test.C c3_aww Test.I
                                 Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axA (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay0
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw6 Test.U)
             @ (Test.C c2_awi Test.U
                Test.:+: (Test.C c3_aww Test.I
                          Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay0
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awi Test.U)
                @ (Test.C c3_aww Test.I
                   Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.L
                   @ (Test.C c3_aww Test.I)
                   @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.C
                      @ c3_aww
                      @ Test.I
                      @ Test.Logic
                      (f0_ajC
                       `cast` (sym (Test.NTCo:I Test.Logic)
                               :: Test.Logic ~ Test.I Test.Logic))))));
      Test.Impl f0_ajD f1_ajE ->
        Test.R
          @ (Test.C c_avW (Test.K GHC.Base.String))
          @ (Test.C c1_aw6 Test.U
             Test.:+: (Test.C c2_awi Test.U
                       Test.:+: (Test.C c3_aww Test.I
                                 Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axA (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay0
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw6 Test.U)
             @ (Test.C c2_awi Test.U
                Test.:+: (Test.C c3_aww Test.I
                          Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay0
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awi Test.U)
                @ (Test.C c3_aww Test.I
                   Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_aww Test.I)
                   @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.L
                      @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.C
                         @ c4_awP
                         @ (Test.I Test.:*: Test.I)
                         @ Test.Logic
                         (Test.:*:
                            @ Test.I
                            @ Test.I
                            @ Test.Logic
                            (f0_ajD
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))
                            (f1_ajE
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))))))));
      Test.Equiv f0_ajF f1_ajG ->
        Test.R
          @ (Test.C c_avW (Test.K GHC.Base.String))
          @ (Test.C c1_aw6 Test.U
             Test.:+: (Test.C c2_awi Test.U
                       Test.:+: (Test.C c3_aww Test.I
                                 Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axA (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay0
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw6 Test.U)
             @ (Test.C c2_awi Test.U
                Test.:+: (Test.C c3_aww Test.I
                          Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay0
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awi Test.U)
                @ (Test.C c3_aww Test.I
                   Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_aww Test.I)
                   @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.L
                         @ (Test.C c5_axb (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axA (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.C
                            @ c5_axb
                            @ (Test.I Test.:*: Test.I)
                            @ Test.Logic
                            (Test.:*:
                               @ Test.I
                               @ Test.I
                               @ Test.Logic
                               (f0_ajF
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic))
                               (f1_ajG
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic)))))))));
      Test.Conj f0_ajH f1_ajI ->
        Test.R
          @ (Test.C c_avW (Test.K GHC.Base.String))
          @ (Test.C c1_aw6 Test.U
             Test.:+: (Test.C c2_awi Test.U
                       Test.:+: (Test.C c3_aww Test.I
                                 Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axA (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay0
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw6 Test.U)
             @ (Test.C c2_awi Test.U
                Test.:+: (Test.C c3_aww Test.I
                          Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay0
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awi Test.U)
                @ (Test.C c3_aww Test.I
                   Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_aww Test.I)
                   @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axb (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axA (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.L
                            @ (Test.C c6_axA (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay0 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c6_axA
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ajH
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ajI
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))));
      Test.Disj f0_ajJ f1_ajK ->
        Test.R
          @ (Test.C c_avW (Test.K GHC.Base.String))
          @ (Test.C c1_aw6 Test.U
             Test.:+: (Test.C c2_awi Test.U
                       Test.:+: (Test.C c3_aww Test.I
                                 Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axA (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay0
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw6 Test.U)
             @ (Test.C c2_awi Test.U
                Test.:+: (Test.C c3_aww Test.I
                          Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay0
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awi Test.U)
                @ (Test.C c3_aww Test.I
                   Test.:+: (Test.C c4_awP (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_aww Test.I)
                   @ (Test.C c4_awP (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axb (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awP (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axb (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axA (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axb (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axA (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay0 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.R
                            @ (Test.C c6_axA (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay0 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c7_ay0
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ajJ
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ajK
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))))
    }

Test.toLogic [InlPrag=INLINE]
  :: forall t_av8 t1_ave t2_avi t3_avm t4_avr t5_avx t6_avD t7_avH.
     (Test.:+:)
       (Test.C t_av8 (Test.K GHC.Base.String))
       (Test.C t1_ave Test.U
        Test.:+: (Test.C t2_avi Test.U
                  Test.:+: (Test.C t3_avm Test.I
                            Test.:+: (Test.C t4_avr (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C t5_avx (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C t6_avD (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     t7_avH
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
     -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_av8)
                 (@ t1_ave)
                 (@ t2_avi)
                 (@ t3_avm)
                 (@ t4_avr)
                 (@ t5_avx)
                 (@ t6_avD)
                 (@ t7_avH)
                 (ds_dQX [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C t_av8 (Test.K GHC.Base.String))
                         (Test.C t1_ave Test.U
                          Test.:+: (Test.C t2_avi Test.U
                                    Test.:+: (Test.C t3_avm Test.I
                                              Test.:+: (Test.C t4_avr (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    t5_avx (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              t6_avD
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       t7_avH
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                         Test.Logic) ->
                 case ds_dQX of _ {
                   Test.L ds1_dRq [Occ=Once!] ->
                     case ds1_dRq of _ { Test.C ds2_dRr [Occ=Once] ->
                     Test.Var
                       (ds2_dRr
                        `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                     };
                   Test.R ds1_dQY [Occ=Once!] ->
                     case ds1_dQY of _ {
                       Test.L ds2_dRo [Occ=Once!] ->
                         case ds2_dRo of _ { Test.C ds3_dRp [Occ=Once!] ->
                         case ds3_dRp of _ { Test.U -> Test.T }
                         };
                       Test.R ds2_dQZ [Occ=Once!] ->
                         case ds2_dQZ of _ {
                           Test.L ds3_dRm [Occ=Once!] ->
                             case ds3_dRm of _ { Test.C ds4_dRn [Occ=Once!] ->
                             case ds4_dRn of _ { Test.U -> Test.F }
                             };
                           Test.R ds3_dR0 [Occ=Once!] ->
                             case ds3_dR0 of _ {
                               Test.L ds4_dRk [Occ=Once!] ->
                                 case ds4_dRk of _ { Test.C ds5_dRl [Occ=Once] ->
                                 Test.Not
                                   (ds5_dRl
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 };
                               Test.R ds4_dR1 [Occ=Once!] ->
                                 case ds4_dR1 of _ {
                                   Test.L ds5_dRg [Occ=Once!] ->
                                     case ds5_dRg of _ { Test.C ds6_dRh [Occ=Once!] ->
                                     case ds6_dRh
                                     of _ { Test.:*: ds7_dRi [Occ=Once] ds8_dRj [Occ=Once] ->
                                     Test.Impl
                                       (ds7_dRi
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds8_dRj
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds5_dR2 [Occ=Once!] ->
                                     case ds5_dR2 of _ {
                                       Test.L ds6_dRc [Occ=Once!] ->
                                         case ds6_dRc of _ { Test.C ds7_dRd [Occ=Once!] ->
                                         case ds7_dRd
                                         of _ { Test.:*: ds8_dRe [Occ=Once] ds9_dRf [Occ=Once] ->
                                         Test.Equiv
                                           (ds8_dRe
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds9_dRf
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds6_dR3 [Occ=Once!] ->
                                         case ds6_dR3 of _ {
                                           Test.L ds7_dR8 [Occ=Once!] ->
                                             case ds7_dR8 of _ { Test.C ds8_dR9 [Occ=Once!] ->
                                             case ds8_dR9
                                             of _
                                             { Test.:*: ds9_dRa [Occ=Once] ds10_dRb [Occ=Once] ->
                                             Test.Conj
                                               (ds9_dRa
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dRb
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             };
                                           Test.R ds7_dR4 [Occ=Once!] ->
                                             case ds7_dR4 of _ { Test.C ds8_dR5 [Occ=Once!] ->
                                             case ds8_dR5
                                             of _
                                             { Test.:*: ds9_dR6 [Occ=Once] ds10_dR7 [Occ=Once] ->
                                             Test.Disj
                                               (ds9_dR6
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dR7
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }}]
Test.toLogic =
  \ (@ t_av8)
    (@ t1_ave)
    (@ t2_avi)
    (@ t3_avm)
    (@ t4_avr)
    (@ t5_avx)
    (@ t6_avD)
    (@ t7_avH)
    (eta_B1
       :: (Test.:+:)
            (Test.C t_av8 (Test.K GHC.Base.String))
            (Test.C t1_ave Test.U
             Test.:+: (Test.C t2_avi Test.U
                       Test.:+: (Test.C t3_avm Test.I
                                 Test.:+: (Test.C t4_avr (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C t5_avx (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 t6_avD (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          t7_avH
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
            Test.Logic) ->
    case eta_B1 of _ {
      Test.L ds_dRq ->
        case ds_dRq of _ { Test.C ds1_dRr ->
        Test.Var
          (ds1_dRr
           `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                   :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
        };
      Test.R ds_dQY ->
        case ds_dQY of _ {
          Test.L ds1_dRo ->
            case ds1_dRo of _ { Test.C ds2_dRp ->
            case ds2_dRp of _ { Test.U -> Test.T }
            };
          Test.R ds1_dQZ ->
            case ds1_dQZ of _ {
              Test.L ds2_dRm ->
                case ds2_dRm of _ { Test.C ds3_dRn ->
                case ds3_dRn of _ { Test.U -> Test.F }
                };
              Test.R ds2_dR0 ->
                case ds2_dR0 of _ {
                  Test.L ds3_dRk ->
                    case ds3_dRk of _ { Test.C ds4_dRl ->
                    Test.Not
                      (ds4_dRl
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    };
                  Test.R ds3_dR1 ->
                    case ds3_dR1 of _ {
                      Test.L ds4_dRg ->
                        case ds4_dRg of _ { Test.C ds5_dRh ->
                        case ds5_dRh of _ { Test.:*: ds6_dRi ds7_dRj ->
                        Test.Impl
                          (ds6_dRi
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds7_dRj
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds4_dR2 ->
                        case ds4_dR2 of _ {
                          Test.L ds5_dRc ->
                            case ds5_dRc of _ { Test.C ds6_dRd ->
                            case ds6_dRd of _ { Test.:*: ds7_dRe ds8_dRf ->
                            Test.Equiv
                              (ds7_dRe
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds8_dRf
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds5_dR3 ->
                            case ds5_dR3 of _ {
                              Test.L ds6_dR8 ->
                                case ds6_dR8 of _ { Test.C ds7_dR9 ->
                                case ds7_dR9 of _ { Test.:*: ds8_dRa ds9_dRb ->
                                Test.Conj
                                  (ds8_dRa
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRb
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                };
                              Test.R ds6_dR4 ->
                                case ds6_dR4 of _ { Test.C ds7_dR5 ->
                                case ds7_dR5 of _ { Test.:*: ds8_dR6 ds9_dR7 ->
                                Test.Disj
                                  (ds8_dR6
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dR7
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

Test.updateStringfTimes [InlPrag=INLINE]
  :: forall (t_azQ :: * -> *) (t1_azS :: * -> *) r_azW.
     (Test.UpdateString t_azQ, Test.UpdateString t1_azS) =>
     (r_azW -> r_azW)
     -> (Test.:*:) t_azQ t1_azS r_azW
     -> (Test.:*:) t_azQ t1_azS r_azW
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azQ::* -> *)
                 (@ t1_azS::* -> *)
                 (@ r_azW)
                 ($dUpdateString_aA0 [Occ=Once] :: Test.UpdateString t_azQ)
                 ($dUpdateString1_aA1 [Occ=Once] :: Test.UpdateString t1_azS)
                 (f_ajw :: r_azW -> r_azW)
                 (ds_dS5 [Occ=Once!] :: (Test.:*:) t_azQ t1_azS r_azW) ->
                 case ds_dS5 of _ { Test.:*: x_ajx [Occ=Once] y_ajy [Occ=Once] ->
                 Test.:*:
                   @ t_azQ
                   @ t1_azS
                   @ r_azW
                   (($dUpdateString_aA0
                     `cast` (Test.NTCo:T:UpdateString t_azQ
                             :: Test.T:UpdateString t_azQ
                                  ~
                                (forall a_aiL. (a_aiL -> a_aiL) -> t_azQ a_aiL -> t_azQ a_aiL)))
                      @ r_azW f_ajw x_ajx)
                   (($dUpdateString1_aA1
                     `cast` (Test.NTCo:T:UpdateString t1_azS
                             :: Test.T:UpdateString t1_azS
                                  ~
                                (forall a_aiL. (a_aiL -> a_aiL) -> t1_azS a_aiL -> t1_azS a_aiL)))
                      @ r_azW f_ajw y_ajy)
                 }}]
Test.updateStringfTimes =
  \ (@ t_azQ::* -> *)
    (@ t1_azS::* -> *)
    (@ r_azW)
    ($dUpdateString_aA0 :: Test.UpdateString t_azQ)
    ($dUpdateString1_aA1 :: Test.UpdateString t1_azS)
    (eta_B2 :: r_azW -> r_azW)
    (eta1_B1 :: (Test.:*:) t_azQ t1_azS r_azW) ->
    case eta1_B1 of _ { Test.:*: x_ajx y_ajy ->
    Test.:*:
      @ t_azQ
      @ t1_azS
      @ r_azW
      (($dUpdateString_aA0
        `cast` (Test.NTCo:T:UpdateString t_azQ
                :: Test.T:UpdateString t_azQ
                     ~
                   (forall a_aiL. (a_aiL -> a_aiL) -> t_azQ a_aiL -> t_azQ a_aiL)))
         @ r_azW eta_B2 x_ajx)
      (($dUpdateString1_aA1
        `cast` (Test.NTCo:T:UpdateString t1_azS
                :: Test.T:UpdateString t1_azS
                     ~
                   (forall a_aiL. (a_aiL -> a_aiL) -> t1_azS a_aiL -> t1_azS a_aiL)))
         @ r_azW eta_B2 y_ajy)
    }

Test.$fUpdateString:*:1
  :: forall (f_ajX :: * -> *) (g_ajY :: * -> *).
     (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
     forall a_aPH.
     (a_aPH -> a_aPH)
     -> (Test.:*:) f_ajX g_ajY a_aPH
     -> (Test.:*:) f_ajX g_ajY a_aPH
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_ajX::* -> *)
                 (@ g_ajY::* -> *)
                 ($dUpdateString_aPB [Occ=Once] :: Test.UpdateString f_ajX)
                 ($dUpdateString1_aPC [Occ=Once] :: Test.UpdateString g_ajY)
                 (@ a_aPH)
                 (eta_B2 [Occ=Once] :: a_aPH -> a_aPH)
                 (eta1_B1 [Occ=Once] :: (Test.:*:) f_ajX g_ajY a_aPH) ->
                 Test.updateStringfTimes
                   @ f_ajX
                   @ g_ajY
                   @ a_aPH
                   $dUpdateString_aPB
                   $dUpdateString1_aPC
                   eta_B2
                   eta1_B1}]
Test.$fUpdateString:*:1 =
  \ (@ f_ajX::* -> *)
    (@ g_ajY::* -> *)
    ($dUpdateString_aPB :: Test.UpdateString f_ajX)
    ($dUpdateString1_aPC :: Test.UpdateString g_ajY)
    (@ a_aPH)
    (eta_B2 :: a_aPH -> a_aPH)
    (eta1_B1 :: (Test.:*:) f_ajX g_ajY a_aPH) ->
    Test.updateStringfTimes
      @ f_ajX
      @ g_ajY
      @ a_aPH
      $dUpdateString_aPB
      $dUpdateString1_aPC
      eta_B2
      eta1_B1

Test.updateStringfPlus [InlPrag=INLINE]
  :: forall (t_aAa :: * -> *) r_aAf (g_aAh :: * -> *).
     (Test.UpdateString t_aAa, Test.UpdateString g_aAh) =>
     (r_aAf -> r_aAf)
     -> (Test.:+:) t_aAa g_aAh r_aAf
     -> (Test.:+:) t_aAa g_aAh r_aAf
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAa::* -> *)
                 (@ r_aAf)
                 (@ g_aAh::* -> *)
                 ($dUpdateString_aAo [Occ=Once] :: Test.UpdateString t_aAa)
                 ($dUpdateString1_aAp [Occ=Once] :: Test.UpdateString g_aAh)
                 (f_ajs [Occ=Once*] :: r_aAf -> r_aAf)
                 (ds_dS7 [Occ=Once!] :: (Test.:+:) t_aAa g_aAh r_aAf) ->
                 case ds_dS7 of _ {
                   Test.L x_ajt [Occ=Once] ->
                     Test.L
                       @ t_aAa
                       @ g_aAh
                       @ r_aAf
                       (($dUpdateString_aAo
                         `cast` (Test.NTCo:T:UpdateString t_aAa
                                 :: Test.T:UpdateString t_aAa
                                      ~
                                    (forall a_aiL. (a_aiL -> a_aiL) -> t_aAa a_aiL -> t_aAa a_aiL)))
                          @ r_aAf f_ajs x_ajt);
                   Test.R x_ajv [Occ=Once] ->
                     Test.R
                       @ t_aAa
                       @ g_aAh
                       @ r_aAf
                       (($dUpdateString1_aAp
                         `cast` (Test.NTCo:T:UpdateString g_aAh
                                 :: Test.T:UpdateString g_aAh
                                      ~
                                    (forall a_aiL. (a_aiL -> a_aiL) -> g_aAh a_aiL -> g_aAh a_aiL)))
                          @ r_aAf f_ajs x_ajv)
                 }}]
Test.updateStringfPlus =
  \ (@ t_aAa::* -> *)
    (@ r_aAf)
    (@ g_aAh::* -> *)
    ($dUpdateString_aAo :: Test.UpdateString t_aAa)
    ($dUpdateString1_aAp :: Test.UpdateString g_aAh)
    (eta_B2 :: r_aAf -> r_aAf)
    (eta1_B1 :: (Test.:+:) t_aAa g_aAh r_aAf) ->
    case eta1_B1 of _ {
      Test.L x_ajt ->
        Test.L
          @ t_aAa
          @ g_aAh
          @ r_aAf
          (($dUpdateString_aAo
            `cast` (Test.NTCo:T:UpdateString t_aAa
                    :: Test.T:UpdateString t_aAa
                         ~
                       (forall a_aiL. (a_aiL -> a_aiL) -> t_aAa a_aiL -> t_aAa a_aiL)))
             @ r_aAf eta_B2 x_ajt);
      Test.R x_ajv ->
        Test.R
          @ t_aAa
          @ g_aAh
          @ r_aAf
          (($dUpdateString1_aAp
            `cast` (Test.NTCo:T:UpdateString g_aAh
                    :: Test.T:UpdateString g_aAh
                         ~
                       (forall a_aiL. (a_aiL -> a_aiL) -> g_aAh a_aiL -> g_aAh a_aiL)))
             @ r_aAf eta_B2 x_ajv)
    }

Test.$fUpdateString:+:1
  :: forall (f_ajZ :: * -> *) (g_ak0 :: * -> *).
     (Test.UpdateString f_ajZ, Test.UpdateString g_ak0) =>
     forall a_aPS.
     (a_aPS -> a_aPS)
     -> (Test.:+:) f_ajZ g_ak0 a_aPS
     -> (Test.:+:) f_ajZ g_ak0 a_aPS
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0 0 0] 5 0}]
Test.$fUpdateString:+:1 =
  \ (@ f_ajZ::* -> *)
    (@ g_ak0::* -> *)
    ($dUpdateString_aPM :: Test.UpdateString f_ajZ)
    ($dUpdateString1_aPN :: Test.UpdateString g_ak0)
    (@ a_aPS)
    (eta_B2 :: a_aPS -> a_aPS)
    (eta1_B1 :: (Test.:+:) f_ajZ g_ak0 a_aPS) ->
    Test.updateStringfPlus
      @ f_ajZ
      @ a_aPS
      @ g_ak0
      $dUpdateString_aPM
      $dUpdateString1_aPN
      eta_B2
      eta1_B1

Test.updateStringfC [InlPrag=INLINE]
  :: forall t_azv (t1_azy :: * -> *) r_azD c_azF.
     (Test.UpdateString t1_azy) =>
     (r_azD -> r_azD)
     -> Test.C t_azv t1_azy r_azD
     -> Test.C c_azF t1_azy r_azD
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azv)
                 (@ t1_azy::* -> *)
                 (@ r_azD)
                 (@ c_azF)
                 ($dUpdateString_azH [Occ=Once] :: Test.UpdateString t1_azy)
                 (f_ajz [Occ=Once] :: r_azD -> r_azD)
                 (ds_dS3 [Occ=Once!] :: Test.C t_azv t1_azy r_azD) ->
                 case ds_dS3 of _ { Test.C x_ajA [Occ=Once] ->
                 Test.C
                   @ c_azF
                   @ t1_azy
                   @ r_azD
                   (($dUpdateString_azH
                     `cast` (Test.NTCo:T:UpdateString t1_azy
                             :: Test.T:UpdateString t1_azy
                                  ~
                                (forall a_aiL. (a_aiL -> a_aiL) -> t1_azy a_aiL -> t1_azy a_aiL)))
                      @ r_azD f_ajz x_ajA)
                 }}]
Test.updateStringfC =
  \ (@ t_azv)
    (@ t1_azy::* -> *)
    (@ r_azD)
    (@ c_azF)
    ($dUpdateString_azH :: Test.UpdateString t1_azy)
    (eta_B2 :: r_azD -> r_azD)
    (eta1_B1 :: Test.C t_azv t1_azy r_azD) ->
    case eta1_B1 of _ { Test.C x_ajA ->
    Test.C
      @ c_azF
      @ t1_azy
      @ r_azD
      (($dUpdateString_azH
        `cast` (Test.NTCo:T:UpdateString t1_azy
                :: Test.T:UpdateString t1_azy
                     ~
                   (forall a_aiL. (a_aiL -> a_aiL) -> t1_azy a_aiL -> t1_azy a_aiL)))
         @ r_azD eta_B2 x_ajA)
    }

Test.$fUpdateStringC1
  :: forall (f_ajV :: * -> *) c_ajW.
     (Test.UpdateString f_ajV) =>
     forall a_aPx.
     (a_aPx -> a_aPx)
     -> Test.C c_ajW f_ajV a_aPx
     -> Test.C c_ajW f_ajV a_aPx
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_ajV::* -> *)
                 (@ c_ajW)
                 ($dUpdateString_aPs [Occ=Once] :: Test.UpdateString f_ajV)
                 (@ a_aPx)
                 (eta_B2 [Occ=Once] :: a_aPx -> a_aPx)
                 (eta1_B1 [Occ=Once] :: Test.C c_ajW f_ajV a_aPx) ->
                 Test.updateStringfC
                   @ c_ajW @ f_ajV @ a_aPx @ c_ajW $dUpdateString_aPs eta_B2 eta1_B1}]
Test.$fUpdateStringC1 =
  \ (@ f_ajV::* -> *)
    (@ c_ajW)
    ($dUpdateString_aPs :: Test.UpdateString f_ajV)
    (@ a_aPx)
    (eta_B2 :: a_aPx -> a_aPx)
    (eta1_B1 :: Test.C c_ajW f_ajV a_aPx) ->
    Test.updateStringfC
      @ c_ajW @ f_ajV @ a_aPx @ c_ajW $dUpdateString_aPs eta_B2 eta1_B1

Test.updateStringfI [InlPrag=INLINE]
  :: forall t_aBx r_aBA.
     (t_aBx -> r_aBA) -> Test.I t_aBx -> Test.I r_aBA
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= (\ (@ t_XFa)
                  (@ r_XFe)
                  (f_ajc [Occ=Once!] :: t_XFa -> r_XFe)
                  (ds_dSg [Occ=Once] :: Test.I t_XFa) ->
                  f_ajc (ds_dSg `cast` (Test.NTCo:I t_XFa :: Test.I t_XFa ~ t_XFa)))
               `cast` (forall t_XFa r_XFe.
                       (t_XFa -> r_XFe) -> Test.I t_XFa -> sym (Test.NTCo:I r_XFe)
                       :: (forall t_XFa r_XFe. (t_XFa -> r_XFe) -> Test.I t_XFa -> r_XFe)
                            ~
                          (forall t_XFa r_XFe.
                           (t_XFa -> r_XFe) -> Test.I t_XFa -> Test.I r_XFe))}]
Test.updateStringfI =
  a11_r1m7
  `cast` (forall t_aBx r_aBA.
          (t_aBx -> r_aBA) -> Test.I t_aBx -> sym (Test.NTCo:I r_aBA)
          :: (forall t_aBx r_aBA. (t_aBx -> r_aBA) -> Test.I t_aBx -> r_aBA)
               ~
             (forall t_aBx r_aBA.
              (t_aBx -> r_aBA) -> Test.I t_aBx -> Test.I r_aBA))

Test.updateStringfK [InlPrag=INLINE]
  :: forall t_aAu t1_aAw t2_aAx r_aAA.
     t_aAu -> Test.K t2_aAx t1_aAw -> Test.K t2_aAx r_aAA
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= (\ (@ t_XEf)
                  (@ t1_XEi)
                  (@ t2_XEk)
                  (@ r_XEo)
                  _
                  (ds_dSa [Occ=Once] :: Test.K t2_XEk t1_XEi) ->
                  ds_dSa)
               `cast` (forall t_XEf t1_XEi t2_XEk r_XEo.
                       t_XEf
                       -> Test.K t2_XEk t1_XEi
                       -> trans
                            (Test.NTCo:K t2_XEk t1_XEi) (sym (Test.NTCo:K t2_XEk r_XEo))
                       :: (forall t_XEf t1_XEi t2_XEk r_XEo.
                           t_XEf -> Test.K t2_XEk t1_XEi -> Test.K t2_XEk t1_XEi)
                            ~
                          (forall t_XEf t1_XEi t2_XEk r_XEo.
                           t_XEf -> Test.K t2_XEk t1_XEi -> Test.K t2_XEk r_XEo))}]
Test.updateStringfK =
  a_r1m5
  `cast` (forall t_aAu t1_aAw t2_aAx r_aAA.
          t_aAu
          -> Test.K t2_aAx t1_aAw
          -> trans
               (Test.NTCo:K t2_aAx t1_aAw) (sym (Test.NTCo:K t2_aAx r_aAA))
          :: (forall t_aAu t1_aAw t2_aAx r_aAA.
              t_aAu -> Test.K t2_aAx t1_aAw -> Test.K t2_aAx t1_aAw)
               ~
             (forall t_aAu t1_aAw t2_aAx r_aAA.
              t_aAu -> Test.K t2_aAx t1_aAw -> Test.K t2_aAx r_aAA))

Test.updateStringfKString [InlPrag=INLINE]
  :: forall t_aAG t1_aAI r_aAL.
     t_aAG
     -> Test.K [GHC.Types.Char] t1_aAI
     -> Test.K [GHC.Types.Char] r_aAL
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAG)
                 (@ t1_aAI)
                 (@ r_aAL)
                 _
                 (ds_dSb [Occ=Once] :: Test.K [GHC.Types.Char] t1_aAI) ->
                 case ds_dSb
                      `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAI
                              :: Test.K [GHC.Types.Char] t1_aAI ~ [GHC.Types.Char])
                 of _ {
                   [] ->
                     (GHC.Types.[] @ GHC.Types.Char)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAL)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAL);
                   : ipv_sU2 [Occ=Once] ipv1_sU3 ->
                     (GHC.Types.:
                        @ GHC.Types.Char
                        (GHC.List.last_last' @ GHC.Types.Char ipv_sU2 ipv1_sU3)
                        ipv1_sU3)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAL)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAL)
                 }}]
Test.updateStringfKString =
  \ (@ t_aAG)
    (@ t1_aAI)
    (@ r_aAL)
    _
    (eta1_X7H :: Test.K [GHC.Types.Char] t1_aAI) ->
    case eta1_X7H
         `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAI
                 :: Test.K [GHC.Types.Char] t1_aAI ~ [GHC.Types.Char])
    of _ {
      [] ->
        (GHC.Types.[] @ GHC.Types.Char)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAL)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAL);
      : ipv_sTF ipv1_sTG ->
        (GHC.Types.:
           @ GHC.Types.Char
           (GHC.List.last_last' @ GHC.Types.Char ipv_sTF ipv1_sTG)
           ipv1_sTG)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAL)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAL)
    }

Rec {
Test.testLogic_updateString :: Test.Logic -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
Test.testLogic_updateString =
  \ (x_aUo :: Test.Logic) ->
    case x_aUo of _ {
      Test.Var f0_ajB ->
        Test.Var
          (case f0_ajB of _ {
             [] -> GHC.Types.[] @ GHC.Types.Char;
             : ipv_sU2 ipv1_sU3 ->
               GHC.Types.:
                 @ GHC.Types.Char
                 (GHC.List.last_last' @ GHC.Types.Char ipv_sU2 ipv1_sU3)
                 ipv1_sU3
           });
      Test.T -> Test.T;
      Test.F -> Test.F;
      Test.Not f0_ajC -> Test.Not (Test.testLogic_updateString f0_ajC);
      Test.Impl f0_ajD f1_ajE ->
        Test.Impl
          (Test.testLogic_updateString f0_ajD)
          (Test.testLogic_updateString f1_ajE);
      Test.Equiv f0_ajF f1_ajG ->
        Test.Equiv
          (Test.testLogic_updateString f0_ajF)
          (Test.testLogic_updateString f1_ajG);
      Test.Conj f0_ajH f1_ajI ->
        Test.Conj
          (Test.testLogic_updateString f0_ajH)
          (Test.testLogic_updateString f1_ajI);
      Test.Disj f0_ajJ f1_ajK ->
        Test.Disj
          (Test.testLogic_updateString f0_ajJ)
          (Test.testLogic_updateString f1_ajK)
    }
end Rec }

Rec {
updateString1_r1m9 :: Test.Logic -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
updateString1_r1m9 =
  \ (x_aUo :: Test.Logic) ->
    case x_aUo of _ {
      Test.Var f0_ajB ->
        Test.Var
          (case f0_ajB of _ {
             [] -> GHC.Types.[] @ GHC.Types.Char;
             : ipv_sU2 ipv1_sU3 ->
               GHC.Types.:
                 @ GHC.Types.Char
                 (GHC.List.last_last' @ GHC.Types.Char ipv_sU2 ipv1_sU3)
                 ipv1_sU3
           });
      Test.T -> Test.T;
      Test.F -> Test.F;
      Test.Not f0_ajC -> Test.Not (updateString1_r1m9 f0_ajC);
      Test.Impl f0_ajD f1_ajE ->
        Test.Impl (updateString1_r1m9 f0_ajD) (updateString1_r1m9 f1_ajE);
      Test.Equiv f0_ajF f1_ajG ->
        Test.Equiv (updateString1_r1m9 f0_ajF) (updateString1_r1m9 f1_ajG);
      Test.Conj f0_ajH f1_ajI ->
        Test.Conj (updateString1_r1m9 f0_ajH) (updateString1_r1m9 f1_ajI);
      Test.Disj f0_ajJ f1_ajK ->
        Test.Disj (updateString1_r1m9 f0_ajJ) (updateString1_r1m9 f1_ajK)
    }
end Rec }

Test.updateString_$supdateString :: Test.Logic -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=InlineRule(sat, -)
         Tmpl= letrec {
                 updateString2_X14p [Occ=LoopBreaker] :: Test.Logic -> Test.Logic
                 [LclId, Arity=1]
                 updateString2_X14p =
                   \ (x_aUo [Occ=Once!] :: Test.Logic) ->
                     case x_aUo of _ {
                       Test.Var f0_ajB [Occ=Once!] ->
                         Test.Var
                           (case f0_ajB of _ {
                              [] -> GHC.Types.[] @ GHC.Types.Char;
                              : ipv_sU2 [Occ=Once] ipv1_sU3 ->
                                GHC.Types.:
                                  @ GHC.Types.Char
                                  (GHC.List.last_last' @ GHC.Types.Char ipv_sU2 ipv1_sU3)
                                  ipv1_sU3
                            });
                       Test.T -> Test.T;
                       Test.F -> Test.F;
                       Test.Not f0_ajC [Occ=Once] -> Test.Not (updateString2_X14p f0_ajC);
                       Test.Impl f0_ajD [Occ=Once] f1_ajE [Occ=Once] ->
                         Test.Impl (updateString2_X14p f0_ajD) (updateString2_X14p f1_ajE);
                       Test.Equiv f0_ajF [Occ=Once] f1_ajG [Occ=Once] ->
                         Test.Equiv (updateString2_X14p f0_ajF) (updateString2_X14p f1_ajG);
                       Test.Conj f0_ajH [Occ=Once] f1_ajI [Occ=Once] ->
                         Test.Conj (updateString2_X14p f0_ajH) (updateString2_X14p f1_ajI);
                       Test.Disj f0_ajJ [Occ=Once] f1_ajK [Occ=Once] ->
                         Test.Disj (updateString2_X14p f0_ajJ) (updateString2_X14p f1_ajK)
                     }; } in
               updateString2_X14p}]
Test.updateString_$supdateString = updateString1_r1m9

Test.updateStringfU [InlPrag=INLINE]
  :: forall t_aBF a_aBK. t_aBF -> a_aBK -> a_aBK
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= \ (@ t_aBF) (@ a_aBK) _ -> GHC.Base.id @ a_aBK}]
Test.updateStringfU =
  \ (@ t_aBF) (@ a_aBK) _ (eta1_B1 :: a_aBK) -> eta1_B1

Test.logic :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic = Test.Impl Test.logic4 Test.logic1

Test.testLogic :: Test.Logic
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testLogic = Test.testLogic_updateString Test.logic

Test.$fRegularLogic [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Logic
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
                                   :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                                        ~
                                      (Test.Logic -> Test.PF Test.Logic Test.Logic))),
                          ((Test.toLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
                                   :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                                        ~
                                      (Test.PF Test.Logic Test.Logic -> Test.Logic)))]]
Test.$fRegularLogic =
  Test.D:Regular
    @ Test.Logic
    ((Test.fromLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
             :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                  ~
                (Test.Logic -> Test.PF Test.Logic Test.Logic)))
    ((Test.toLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
             :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                  ~
                (Test.PF Test.Logic Test.Logic -> Test.Logic)))

Test.$fUpdateString:*:
  :: forall (f_ajX :: * -> *) (g_ajY :: * -> *).
     (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
     Test.UpdateString (f_ajX Test.:*: g_ajY)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateString:*:1
               `cast` (forall (f_ajX :: * -> *) (g_ajY :: * -> *).
                       (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
                       sym (Test.NTCo:T:UpdateString (f_ajX Test.:*: g_ajY))
                       :: (forall (f_ajX :: * -> *) (g_ajY :: * -> *).
                           (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
                           forall a_aiL.
                           (a_aiL -> a_aiL)
                           -> (Test.:*:) f_ajX g_ajY a_aiL
                           -> (Test.:*:) f_ajX g_ajY a_aiL)
                            ~
                          (forall (f_ajX :: * -> *) (g_ajY :: * -> *).
                           (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
                           Test.T:UpdateString (f_ajX Test.:*: g_ajY)))}]
Test.$fUpdateString:*: =
  Test.$fUpdateString:*:1
  `cast` (forall (f_ajX :: * -> *) (g_ajY :: * -> *).
          (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
          sym (Test.NTCo:T:UpdateString (f_ajX Test.:*: g_ajY))
          :: (forall (f_ajX :: * -> *) (g_ajY :: * -> *).
              (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
              forall a_aiL.
              (a_aiL -> a_aiL)
              -> (Test.:*:) f_ajX g_ajY a_aiL
              -> (Test.:*:) f_ajX g_ajY a_aiL)
               ~
             (forall (f_ajX :: * -> *) (g_ajY :: * -> *).
              (Test.UpdateString f_ajX, Test.UpdateString g_ajY) =>
              Test.T:UpdateString (f_ajX Test.:*: g_ajY)))

Test.$fUpdateString:+:
  :: forall (f_ajZ :: * -> *) (g_ak0 :: * -> *).
     (Test.UpdateString f_ajZ, Test.UpdateString g_ak0) =>
     Test.UpdateString (f_ajZ Test.:+: g_ak0)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateString:+: =
  Test.$fUpdateString:+:1
  `cast` (forall (f_ajZ :: * -> *) (g_ak0 :: * -> *).
          (Test.UpdateString f_ajZ, Test.UpdateString g_ak0) =>
          sym (Test.NTCo:T:UpdateString (f_ajZ Test.:+: g_ak0))
          :: (forall (f_ajZ :: * -> *) (g_ak0 :: * -> *).
              (Test.UpdateString f_ajZ, Test.UpdateString g_ak0) =>
              forall a_aiL.
              (a_aiL -> a_aiL)
              -> (Test.:+:) f_ajZ g_ak0 a_aiL
              -> (Test.:+:) f_ajZ g_ak0 a_aiL)
               ~
             (forall (f_ajZ :: * -> *) (g_ak0 :: * -> *).
              (Test.UpdateString f_ajZ, Test.UpdateString g_ak0) =>
              Test.T:UpdateString (f_ajZ Test.:+: g_ak0)))

Test.$fUpdateStringC
  :: forall (f_ajV :: * -> *) c_ajW.
     (Test.UpdateString f_ajV) =>
     Test.UpdateString (Test.C c_ajW f_ajV)
[GblId[DFunId(newtype)],
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateStringC1
               `cast` (forall (f_ajV :: * -> *) c_ajW.
                       (Test.UpdateString f_ajV) =>
                       sym (Test.NTCo:T:UpdateString (Test.C c_ajW f_ajV))
                       :: (forall (f_ajV :: * -> *) c_ajW.
                           (Test.UpdateString f_ajV) =>
                           forall a_aiL.
                           (a_aiL -> a_aiL)
                           -> Test.C c_ajW f_ajV a_aiL
                           -> Test.C c_ajW f_ajV a_aiL)
                            ~
                          (forall (f_ajV :: * -> *) c_ajW.
                           (Test.UpdateString f_ajV) =>
                           Test.T:UpdateString (Test.C c_ajW f_ajV)))}]
Test.$fUpdateStringC =
  Test.$fUpdateStringC1
  `cast` (forall (f_ajV :: * -> *) c_ajW.
          (Test.UpdateString f_ajV) =>
          sym (Test.NTCo:T:UpdateString (Test.C c_ajW f_ajV))
          :: (forall (f_ajV :: * -> *) c_ajW.
              (Test.UpdateString f_ajV) =>
              forall a_aiL.
              (a_aiL -> a_aiL)
              -> Test.C c_ajW f_ajV a_aiL
              -> Test.C c_ajW f_ajV a_aiL)
               ~
             (forall (f_ajV :: * -> *) c_ajW.
              (Test.UpdateString f_ajV) =>
              Test.T:UpdateString (Test.C c_ajW f_ajV)))

Test.$fUpdateStringI :: Test.UpdateString Test.I
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateStringI =
  (\ (@ a_aQd) -> Test.updateStringfI @ a_aQd @ a_aQd)
  `cast` (sym (Test.NTCo:T:UpdateString Test.I)
          :: (forall a_aiL. (a_aiL -> a_aiL) -> Test.I a_aiL -> Test.I a_aiL)
               ~
             Test.T:UpdateString Test.I)

Test.$fUpdateStringK
  :: forall x_ak1. Test.UpdateString (Test.K x_ak1)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ x_ak1) (@ a_aQ1) ->
                  Test.updateStringfK @ (a_aQ1 -> a_aQ1) @ a_aQ1 @ x_ak1 @ a_aQ1)
               `cast` (forall x_ak1. sym (Test.NTCo:T:UpdateString (Test.K x_ak1))
                       :: (forall x_ak1 a_aiL.
                           (a_aiL -> a_aiL) -> Test.K x_ak1 a_aiL -> Test.K x_ak1 a_aiL)
                            ~
                          (forall x_ak1. Test.T:UpdateString (Test.K x_ak1)))}]
Test.$fUpdateStringK =
  (\ (@ x_ak1) (@ a_aQ1) ->
     Test.updateStringfK @ (a_aQ1 -> a_aQ1) @ a_aQ1 @ x_ak1 @ a_aQ1)
  `cast` (forall x_ak1. sym (Test.NTCo:T:UpdateString (Test.K x_ak1))
          :: (forall x_ak1 a_aiL.
              (a_aiL -> a_aiL) -> Test.K x_ak1 a_aiL -> Test.K x_ak1 a_aiL)
               ~
             (forall x_ak1. Test.T:UpdateString (Test.K x_ak1)))

Test.$fUpdateStringK0 :: Test.UpdateString (Test.K GHC.Base.String)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQ7) ->
                  Test.updateStringfKString @ (a_aQ7 -> a_aQ7) @ a_aQ7 @ a_aQ7)
               `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
                       :: (forall a_aiL.
                           (a_aiL -> a_aiL)
                           -> Test.K GHC.Base.String a_aiL
                           -> Test.K GHC.Base.String a_aiL)
                            ~
                          Test.T:UpdateString (Test.K GHC.Base.String))}]
Test.$fUpdateStringK0 =
  (\ (@ a_aQ7) ->
     Test.updateStringfKString @ (a_aQ7 -> a_aQ7) @ a_aQ7 @ a_aQ7)
  `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
          :: (forall a_aiL.
              (a_aiL -> a_aiL)
              -> Test.K GHC.Base.String a_aiL
              -> Test.K GHC.Base.String a_aiL)
               ~
             Test.T:UpdateString (Test.K GHC.Base.String))

Test.$fUpdateStringU :: Test.UpdateString Test.U
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQj) ->
                  Test.updateStringfU @ (a_aQj -> a_aQj) @ (Test.U a_aQj))
               `cast` (sym (Test.NTCo:T:UpdateString Test.U)
                       :: (forall a_aiL. (a_aiL -> a_aiL) -> Test.U a_aiL -> Test.U a_aiL)
                            ~
                          Test.T:UpdateString Test.U)}]
Test.$fUpdateStringU =
  (\ (@ a_aQj) ->
     Test.updateStringfU @ (a_aQj -> a_aQj) @ (Test.U a_aQj))
  `cast` (sym (Test.NTCo:T:UpdateString Test.U)
          :: (forall a_aiL. (a_aiL -> a_aiL) -> Test.U a_aiL -> Test.U a_aiL)
               ~
             Test.T:UpdateString Test.U)

Test.$dmconFixity
  :: forall c_aiO.
     (Test.Constructor c_aiO) =>
     forall (t_aiS :: * -> (* -> *) -> * -> *) (f_aiT :: * -> *) r_aiU.
     t_aiS c_aiO f_aiT r_aiU -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aiO)
                 _
                 (@ t_aBQ::* -> (* -> *) -> * -> *)
                 (@ f_aBR::* -> *)
                 (@ r_aBS)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity =
  \ (@ c_aiO)
    _
    (@ t_aBQ::* -> (* -> *) -> * -> *)
    (@ f_aBR::* -> *)
    (@ r_aBS)
    _ ->
    Test.Prefix

Test.$dmconIsRecord
  :: forall c_aiO.
     (Test.Constructor c_aiO) =>
     forall (t_aiV :: * -> (* -> *) -> * -> *) (f_aiW :: * -> *) r_aiX.
     t_aiV c_aiO f_aiW r_aiX -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_aiO)
                 _
                 (@ t_aBY::* -> (* -> *) -> * -> *)
                 (@ f_aBZ::* -> *)
                 (@ r_aC0)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord =
  \ (@ c_aiO)
    _
    (@ t_aBY::* -> (* -> *) -> * -> *)
    (@ f_aBZ::* -> *)
    (@ r_aC0)
    _ ->
    GHC.Bool.False

Test.updateString [InlPrag=INLINE]
  :: forall a_ajb.
     (Test.Regular a_ajb, Test.UpdateString (Test.PF a_ajb)) =>
     a_ajb -> a_ajb
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ a_ay8)
                 ($dRegular_ayA :: Test.Regular a_ay8)
                 ($dUpdateString_ayB :: Test.UpdateString (Test.PF a_ay8)) ->
                 letrec {
                   sub_aQQ [Occ=OnceL!] :: Test.PF a_ay8 a_ay8 -> Test.PF a_ay8 a_ay8
                   [LclId]
                   sub_aQQ =
                     ($dUpdateString_ayB
                      `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay8)
                              :: Test.T:UpdateString (Test.PF a_ay8)
                                   ~
                                 (forall a_aiL.
                                  (a_aiL -> a_aiL) -> Test.PF a_ay8 a_aiL -> Test.PF a_ay8 a_aiL)))
                       @ a_ay8 updateString2_ay9;
                   updateString2_ay9 [Occ=LoopBreaker] :: a_ay8 -> a_ay8
                   [LclId, Arity=1]
                   updateString2_ay9 =
                     \ (x_aUo [Occ=Once] :: a_ay8) ->
                       Test.to
                         @ a_ay8
                         $dRegular_ayA
                         (sub_aQQ (Test.from @ a_ay8 $dRegular_ayA x_aUo)); } in
                 updateString2_ay9}]
Test.updateString =
  \ (@ a_ay8)
    ($dRegular_ayA :: Test.Regular a_ay8)
    ($dUpdateString_ayB :: Test.UpdateString (Test.PF a_ay8)) ->
    letrec {
      sub_s1k8 :: Test.PF a_ay8 a_ay8 -> Test.PF a_ay8 a_ay8
      [LclId, Str=DmdType]
      sub_s1k8 =
        ($dUpdateString_ayB
         `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay8)
                 :: Test.T:UpdateString (Test.PF a_ay8)
                      ~
                    (forall a_aiL.
                     (a_aiL -> a_aiL) -> Test.PF a_ay8 a_aiL -> Test.PF a_ay8 a_aiL)))
          @ a_ay8 updateString2_s1k9;
      updateString2_s1k9 [Occ=LoopBreaker] :: a_ay8 -> a_ay8
      [LclId, Arity=1, Str=DmdType L]
      updateString2_s1k9 =
        \ (x_aUo :: a_ay8) ->
          Test.to
            @ a_ay8
            $dRegular_ayA
            (sub_s1k8 (Test.from @ a_ay8 $dRegular_ayA x_aUo)); } in
    updateString2_s1k9

Test.$fConstructorLogic_Var__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_Var_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aPk::* -> (* -> *) -> * -> *)
                 (@ f_aPl::* -> *)
                 (@ r_aPm)
                 _ ->
                 Test.$fConstructorLogic_Var_1}]
Test.$fConstructorLogic_Var__$cconName =
  \ (@ t_aPk::* -> (* -> *) -> * -> *)
    (@ f_aPl::* -> *)
    (@ r_aPm)
    _ ->
    Test.$fConstructorLogic_Var_1

Test.$fConstructorLogic_Var_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Var_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Var__$cconName,
                              Test.$dmconFixity_$s$dmconFixity,
                              Test.$dmconIsRecord_$s$dmconIsRecord]]
Test.$fConstructorLogic_Var_ =
  Test.D:Constructor
    @ Test.Logic_Var_
    Test.$fConstructorLogic_Var__$cconName
    Test.$dmconFixity_$s$dmconFixity
    Test.$dmconIsRecord_$s$dmconIsRecord

Test.$fConstructorLogic_Impl__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_Impl_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aP8::* -> (* -> *) -> * -> *)
                 (@ f_aP9::* -> *)
                 (@ r_aPa)
                 _ ->
                 Test.$fConstructorLogic_Impl_1}]
Test.$fConstructorLogic_Impl__$cconName =
  \ (@ t_aP8::* -> (* -> *) -> * -> *)
    (@ f_aP9::* -> *)
    (@ r_aPa)
    _ ->
    Test.$fConstructorLogic_Impl_1

Test.$fConstructorLogic_Impl_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Impl_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Impl__$cconName,
                              Test.$dmconFixity_$s$dmconFixity1,
                              Test.$dmconIsRecord_$s$dmconIsRecord1]]
Test.$fConstructorLogic_Impl_ =
  Test.D:Constructor
    @ Test.Logic_Impl_
    Test.$fConstructorLogic_Impl__$cconName
    Test.$dmconFixity_$s$dmconFixity1
    Test.$dmconIsRecord_$s$dmconIsRecord1

Test.$fConstructorLogic_Equiv__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_Equiv_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOW::* -> (* -> *) -> * -> *)
                 (@ f_aOX::* -> *)
                 (@ r_aOY)
                 _ ->
                 Test.$fConstructorLogic_Equiv_1}]
Test.$fConstructorLogic_Equiv__$cconName =
  \ (@ t_aOW::* -> (* -> *) -> * -> *)
    (@ f_aOX::* -> *)
    (@ r_aOY)
    _ ->
    Test.$fConstructorLogic_Equiv_1

Test.$fConstructorLogic_Equiv_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Equiv_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Equiv__$cconName,
                              Test.$dmconFixity_$s$dmconFixity2,
                              Test.$dmconIsRecord_$s$dmconIsRecord2]]
Test.$fConstructorLogic_Equiv_ =
  Test.D:Constructor
    @ Test.Logic_Equiv_
    Test.$fConstructorLogic_Equiv__$cconName
    Test.$dmconFixity_$s$dmconFixity2
    Test.$dmconIsRecord_$s$dmconIsRecord2

Test.$fConstructorLogic_Conj__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_Conj_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOK::* -> (* -> *) -> * -> *)
                 (@ f_aOL::* -> *)
                 (@ r_aOM)
                 _ ->
                 Test.$fConstructorLogic_Conj_1}]
Test.$fConstructorLogic_Conj__$cconName =
  \ (@ t_aOK::* -> (* -> *) -> * -> *)
    (@ f_aOL::* -> *)
    (@ r_aOM)
    _ ->
    Test.$fConstructorLogic_Conj_1

Test.$fConstructorLogic_Conj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Conj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Conj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity3,
                              Test.$dmconIsRecord_$s$dmconIsRecord3]]
Test.$fConstructorLogic_Conj_ =
  Test.D:Constructor
    @ Test.Logic_Conj_
    Test.$fConstructorLogic_Conj__$cconName
    Test.$dmconFixity_$s$dmconFixity3
    Test.$dmconIsRecord_$s$dmconIsRecord3

Test.$fConstructorLogic_Disj__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_Disj_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOy::* -> (* -> *) -> * -> *)
                 (@ f_aOz::* -> *)
                 (@ r_aOA)
                 _ ->
                 Test.$fConstructorLogic_Disj_1}]
Test.$fConstructorLogic_Disj__$cconName =
  \ (@ t_aOy::* -> (* -> *) -> * -> *)
    (@ f_aOz::* -> *)
    (@ r_aOA)
    _ ->
    Test.$fConstructorLogic_Disj_1

Test.$fConstructorLogic_Disj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Disj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Disj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity4,
                              Test.$dmconIsRecord_$s$dmconIsRecord4]]
Test.$fConstructorLogic_Disj_ =
  Test.D:Constructor
    @ Test.Logic_Disj_
    Test.$fConstructorLogic_Disj__$cconName
    Test.$dmconFixity_$s$dmconFixity4
    Test.$dmconIsRecord_$s$dmconIsRecord4

Test.$fConstructorLogic_Not__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_Not_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOm::* -> (* -> *) -> * -> *)
                 (@ f_aOn::* -> *)
                 (@ r_aOo)
                 _ ->
                 Test.$fConstructorLogic_Not_1}]
Test.$fConstructorLogic_Not__$cconName =
  \ (@ t_aOm::* -> (* -> *) -> * -> *)
    (@ f_aOn::* -> *)
    (@ r_aOo)
    _ ->
    Test.$fConstructorLogic_Not_1

Test.$fConstructorLogic_Not_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Not_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Not__$cconName,
                              Test.$dmconFixity_$s$dmconFixity5,
                              Test.$dmconIsRecord_$s$dmconIsRecord5]]
Test.$fConstructorLogic_Not_ =
  Test.D:Constructor
    @ Test.Logic_Not_
    Test.$fConstructorLogic_Not__$cconName
    Test.$dmconFixity_$s$dmconFixity5
    Test.$dmconIsRecord_$s$dmconIsRecord5

Test.$fConstructorLogic_T__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_T_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOa::* -> (* -> *) -> * -> *)
                 (@ f_aOb::* -> *)
                 (@ r_aOc)
                 _ ->
                 Test.$fConstructorLogic_T_1}]
Test.$fConstructorLogic_T__$cconName =
  \ (@ t_aOa::* -> (* -> *) -> * -> *)
    (@ f_aOb::* -> *)
    (@ r_aOc)
    _ ->
    Test.$fConstructorLogic_T_1

Test.$fConstructorLogic_T_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_T_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_T__$cconName,
                              Test.$dmconFixity_$s$dmconFixity6,
                              Test.$dmconIsRecord_$s$dmconIsRecord6]]
Test.$fConstructorLogic_T_ =
  Test.D:Constructor
    @ Test.Logic_T_
    Test.$fConstructorLogic_T__$cconName
    Test.$dmconFixity_$s$dmconFixity6
    Test.$dmconIsRecord_$s$dmconIsRecord6

Test.$fConstructorLogic_F__$cconName
  :: forall (t_aiP :: * -> (* -> *) -> * -> *)
            (f_aiQ :: * -> *)
            r_aiR.
     t_aiP Test.Logic_F_ f_aiQ r_aiR -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aNY::* -> (* -> *) -> * -> *)
                 (@ f_aNZ::* -> *)
                 (@ r_aO0)
                 _ ->
                 Test.$fConstructorLogic_F_1}]
Test.$fConstructorLogic_F__$cconName =
  \ (@ t_aNY::* -> (* -> *) -> * -> *)
    (@ f_aNZ::* -> *)
    (@ r_aO0)
    _ ->
    Test.$fConstructorLogic_F_1

Test.$fConstructorLogic_F_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_F_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_F__$cconName,
                              Test.$dmconFixity_$s$dmconFixity7,
                              Test.$dmconIsRecord_$s$dmconIsRecord7]]
Test.$fConstructorLogic_F_ =
  Test.D:Constructor
    @ Test.Logic_F_
    Test.$fConstructorLogic_F__$cconName
    Test.$dmconFixity_$s$dmconFixity7
    Test.$dmconIsRecord_$s$dmconIsRecord7

Test.$fReadFixity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Fixity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run @ [Test.Fixity] Test.$fReadFixity4

Test.$fReadFixity_$creadsPrec
  :: GHC.Types.Int -> Text.ParserCombinators.ReadP.ReadS Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 5 6}]
Test.$fReadFixity_$creadsPrec =
  \ (eta_aXF :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Fixity
      (Test.$fReadFixity3
         eta_aXF
         @ Test.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ Test.Fixity))

Test.$fReadFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadFixity_$creadsPrec,
                           Test.$fReadFixity_readListDefault,
                           ((Test.$fReadFixity2
                             `cast` (Text.ParserCombinators.ReadPrec.Prec
                                     -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity)
                                     :: (Text.ParserCombinators.ReadPrec.Prec
                                         -> forall b_aWA.
                                            (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWA)
                                            -> Text.ParserCombinators.ReadP.P b_aWA)
                                          ~
                                        (Text.ParserCombinators.ReadPrec.Prec
                                         -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)))
                            `cast` (right
                                      (inst
                                         (forall a_aXl.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXl
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXl))
                                         Test.Fixity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity)),
                           (Test.$fReadFixity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))]]
Test.$fReadFixity =
  GHC.Read.D:Read
    @ Test.Fixity
    Test.$fReadFixity_$creadsPrec
    Test.$fReadFixity_readListDefault
    (Test.$fReadFixity2
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_aWA.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWA)
                    -> Text.ParserCombinators.ReadP.P b_aWA)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
    (Test.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))

Test.$fOrdFixity_$cmin :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 25 0}]
Test.$fOrdFixity_$cmin =
  \ (x_aYY :: Test.Fixity) (y_aYZ :: Test.Fixity) ->
    case x_aYY of wild_X8I {
      Test.Prefix -> case y_aYZ of _ { __DEFAULT -> Test.Prefix };
      Test.Infix ds_dSn ds1_dSo ->
        case y_aYZ of wild1_X8K {
          Test.Prefix -> Test.Prefix;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> wild1_X8K; GHC.Bool.True -> wild_X8I
                        };
                      GHC.Bool.True -> wild_X8I
                    }
                    }
                    };
                  Test.RightAssociative -> wild_X8I;
                  Test.NotAssociative -> wild_X8I
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> wild1_X8K;
                  Test.RightAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> wild1_X8K; GHC.Bool.True -> wild_X8I
                        };
                      GHC.Bool.True -> wild_X8I
                    }
                    }
                    };
                  Test.NotAssociative -> wild_X8I
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> wild1_X8K;
                  Test.NotAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> wild1_X8K; GHC.Bool.True -> wild_X8I
                        };
                      GHC.Bool.True -> wild_X8I
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$cmax :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 2] 24 0}]
Test.$fOrdFixity_$cmax =
  \ (x_aZ7 :: Test.Fixity) (y_aZ8 :: Test.Fixity) ->
    case x_aZ7 of wild_X8I {
      Test.Prefix -> y_aZ8;
      Test.Infix ds_dSn ds1_dSo ->
        case y_aZ8 of wild1_X8K {
          Test.Prefix -> wild_X8I;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> wild_X8I; GHC.Bool.True -> wild1_X8K
                        };
                      GHC.Bool.True -> wild1_X8K
                    }
                    }
                    };
                  Test.RightAssociative -> wild1_X8K;
                  Test.NotAssociative -> wild1_X8K
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> wild_X8I;
                  Test.RightAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> wild_X8I; GHC.Bool.True -> wild1_X8K
                        };
                      GHC.Bool.True -> wild1_X8K
                    }
                    }
                    };
                  Test.NotAssociative -> wild1_X8K
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> wild_X8I;
                  Test.NotAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> wild_X8I; GHC.Bool.True -> wild1_X8K
                        };
                      GHC.Bool.True -> wild1_X8K
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c<=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 22 8}]
Test.$fOrdFixity_$c<= =
  \ (x_aZg :: Test.Fixity) (y_aZh :: Test.Fixity) ->
    case x_aZg of _ {
      Test.Prefix -> case y_aZh of _ { __DEFAULT -> GHC.Bool.True };
      Test.Infix ds_dSn ds1_dSo ->
        case y_aZh of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False -> GHC.Prim.==# x#_aWT y#_aWX;
                      GHC.Bool.True -> GHC.Bool.True
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.True;
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False -> GHC.Prim.==# x#_aWT y#_aWX;
                      GHC.Bool.True -> GHC.Bool.True
                    }
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False -> GHC.Prim.==# x#_aWT y#_aWX;
                      GHC.Bool.True -> GHC.Bool.True
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c> :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 25 14}]
Test.$fOrdFixity_$c> =
  \ (x_aZn :: Test.Fixity) (y_aZo :: Test.Fixity) ->
    case x_aZn of _ {
      Test.Prefix -> case y_aZo of _ { __DEFAULT -> GHC.Bool.False };
      Test.Infix ds_dSn ds1_dSo ->
        case y_aZo of _ {
          Test.Prefix -> GHC.Bool.True;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                        };
                      GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> GHC.Bool.True;
                  Test.RightAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                        };
                      GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.NotAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False ->
                        case GHC.Prim.==# x#_aWT y#_aWX of _ {
                          GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                        };
                      GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c>=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 19 11}]
Test.$fOrdFixity_$c>= =
  \ (x_aZu :: Test.Fixity) (y_aZv :: Test.Fixity) ->
    case x_aZu of _ {
      Test.Prefix ->
        case y_aZv of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix ds_dSn ds1_dSo ->
        case y_aZv of _ {
          Test.Prefix -> GHC.Bool.True;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> GHC.Bool.True;
                  Test.RightAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.NotAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    case GHC.Prim.<# x#_aWT y#_aWX of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$c< :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 5}]
Test.$fOrdFixity_$c< =
  \ (x_aZB :: Test.Fixity) (y_aZC :: Test.Fixity) ->
    case x_aZB of _ {
      Test.Prefix ->
        case y_aZC of _ {
          Test.Prefix -> GHC.Bool.False; Test.Infix _ _ -> GHC.Bool.True
        };
      Test.Infix ds_dSn ds1_dSo ->
        case y_aZC of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    GHC.Prim.<# x#_aWT y#_aWX
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.True;
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    GHC.Prim.<# x#_aWT y#_aWX
                    }
                    };
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case ds1_dSo of _ { GHC.Types.I# x#_aWT ->
                    case ds3_XXx of _ { GHC.Types.I# y#_aWX ->
                    GHC.Prim.<# x#_aWT y#_aWX
                    }
                    }
                }
            }
        }
    }

Test.$fOrdFixity_$ccompare
  :: Test.Fixity -> Test.Fixity -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 5}]
Test.$fOrdFixity_$ccompare =
  \ (a2_atS :: Test.Fixity) (b_atT :: Test.Fixity) ->
    case a2_atS of _ {
      Test.Prefix ->
        case b_atT of _ {
          Test.Prefix -> GHC.Ordering.EQ; Test.Infix _ _ -> GHC.Ordering.LT
        };
      Test.Infix ds_dSn ds1_dSo ->
        case b_atT of _ {
          Test.Prefix -> GHC.Ordering.GT;
          Test.Infix ds2_XXv ds3_XXx ->
            case ds_dSn of _ {
              Test.LeftAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> GHC.Base.compareInt ds1_dSo ds3_XXx;
                  Test.RightAssociative -> GHC.Ordering.LT;
                  Test.NotAssociative -> GHC.Ordering.LT
                };
              Test.RightAssociative ->
                case ds2_XXv of _ {
                  Test.LeftAssociative -> GHC.Ordering.GT;
                  Test.RightAssociative -> GHC.Base.compareInt ds1_dSo ds3_XXx;
                  Test.NotAssociative -> GHC.Ordering.LT
                };
              Test.NotAssociative ->
                case ds2_XXv of _ {
                  __DEFAULT -> GHC.Ordering.GT;
                  Test.NotAssociative -> GHC.Base.compareInt ds1_dSo ds3_XXx
                }
            }
        }
    }

Test.$fShowFixity_$cshowsPrec
  :: GHC.Types.Int -> Test.Fixity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType LSL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 5 0] 97 3}]
Test.$fShowFixity_$cshowsPrec =
  \ (ds_dSP :: GHC.Types.Int)
    (ds1_dSQ :: Test.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_dSQ of _ {
      Test.Prefix ->
        GHC.Base.++ @ GHC.Types.Char Test.$fShowFixity3 eta_B1;
      Test.Infix b1_atQ b2_atR ->
        case ds_dSP of _ { GHC.Types.I# x_aX4 ->
        let {
          p_s1kb :: GHC.Show.ShowS
          [LclId, Arity=1, Str=DmdType L]
          p_s1kb =
            \ (x1_aUo :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                Test.$fShowFixity2
                (case b1_atQ of _ {
                   Test.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atR of _ { GHC.Types.I# ww_a19v ->
                           case GHC.Prim.<# ww_a19v 0 of _ {
                             GHC.Bool.False -> GHC.Show.$dmshow_itos' ww_a19v x1_aUo;
                             GHC.Bool.True ->
                               GHC.Types.:
                                 @ GHC.Types.Char
                                 GHC.Show.$dmshow6
                                 (case ww_a19v of wild11_a19S {
                                    __DEFAULT ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           (GHC.Prim.negateInt# wild11_a19S)
                                           (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUo));
                                    (-2147483648) ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           214748364
                                           (GHC.Show.$dmshow_itos'
                                              8
                                              (GHC.Types.:
                                                 @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUo)))
                                  })
                           }
                           }));
                   Test.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atR of _ { GHC.Types.I# ww_a19v ->
                           case GHC.Prim.<# ww_a19v 0 of _ {
                             GHC.Bool.False -> GHC.Show.$dmshow_itos' ww_a19v x1_aUo;
                             GHC.Bool.True ->
                               GHC.Types.:
                                 @ GHC.Types.Char
                                 GHC.Show.$dmshow6
                                 (case ww_a19v of wild11_a19S {
                                    __DEFAULT ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           (GHC.Prim.negateInt# wild11_a19S)
                                           (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUo));
                                    (-2147483648) ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           214748364
                                           (GHC.Show.$dmshow_itos'
                                              8
                                              (GHC.Types.:
                                                 @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUo)))
                                  })
                           }
                           }));
                   Test.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atR of _ { GHC.Types.I# ww_a19v ->
                           case GHC.Prim.<# ww_a19v 0 of _ {
                             GHC.Bool.False -> GHC.Show.$dmshow_itos' ww_a19v x1_aUo;
                             GHC.Bool.True ->
                               GHC.Types.:
                                 @ GHC.Types.Char
                                 GHC.Show.$dmshow6
                                 (case ww_a19v of wild11_a19S {
                                    __DEFAULT ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           (GHC.Prim.negateInt# wild11_a19S)
                                           (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUo));
                                    (-2147483648) ->
                                      GHC.Types.:
                                        @ GHC.Types.Char
                                        GHC.Show.$dmshow7
                                        (GHC.Show.$dmshow_itos'
                                           214748364
                                           (GHC.Show.$dmshow_itos'
                                              8
                                              (GHC.Types.:
                                                 @ GHC.Types.Char GHC.Show.$dmshow5 x1_aUo)))
                                  })
                           }
                           }))
                 }) } in
        case GHC.Prim.>=# x_aX4 11 of _ {
          GHC.Bool.False -> p_s1kb eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.$dmshow6
              (p_s1kb (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 eta_B1))
        }
        }
    }

Test.$fShowFixity_$cshow :: Test.Fixity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 4 0}]
Test.$fShowFixity_$cshow =
  \ (x_aYE :: Test.Fixity) ->
    Test.$fShowFixity_$cshowsPrec
      GHC.Base.zeroInt x_aYE (GHC.Types.[] @ GHC.Types.Char)

Test.$fShowFixity_$cshowList :: [Test.Fixity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 25 3}]
Test.$fShowFixity_$cshowList =
  \ (ds1_aYe :: [Test.Fixity]) (s_aYf :: GHC.Base.String) ->
    case ds1_aYe of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYf;
      : x_aYk xs_aYl ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (Test.$fShowFixity_$cshowsPrec
             Test.$fShowFixity1
             x_aYk
             (let {
                lvl141_s1kd :: [GHC.Types.Char]
                [LclId, Str=DmdType]
                lvl141_s1kd =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYf } in
              letrec {
                showl_s1kf [Occ=LoopBreaker] :: [Test.Fixity] -> [GHC.Types.Char]
                [LclId, Arity=1, Str=DmdType S]
                showl_s1kf =
                  \ (ds2_aYp :: [Test.Fixity]) ->
                    case ds2_aYp of _ {
                      [] -> lvl141_s1kd;
                      : y_aYu ys_aYv ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (Test.$fShowFixity_$cshowsPrec
                             Test.$fShowFixity1 y_aYu (showl_s1kf ys_aYv))
                    }; } in
              showl_s1kf xs_aYl))
    }

Test.$fShowFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowFixity_$cshowsPrec,
                           Test.$fShowFixity_$cshow, Test.$fShowFixity_$cshowList]]
Test.$fShowFixity =
  GHC.Show.D:Show
    @ Test.Fixity
    Test.$fShowFixity_$cshowsPrec
    Test.$fShowFixity_$cshow
    Test.$fShowFixity_$cshowList

Test.$fEqFixity_$c/= :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 19 10}]
Test.$fEqFixity_$c/= =
  \ (a2_atN :: Test.Fixity) (b_atO :: Test.Fixity) ->
    case a2_atN of _ {
      Test.Prefix ->
        case b_atO of _ {
          Test.Prefix -> GHC.Bool.False; Test.Infix _ _ -> GHC.Bool.True
        };
      Test.Infix a12_atF a21_atG ->
        case b_atO of _ {
          Test.Prefix -> GHC.Bool.True;
          Test.Infix b1_atH b2_atI ->
            case a12_atF of _ {
              Test.LeftAssociative ->
                case b1_atH of _ {
                  Test.LeftAssociative ->
                    case a21_atG of _ { GHC.Types.I# x_aXe ->
                    case b2_atI of _ { GHC.Types.I# y_aXi ->
                    case GHC.Prim.==# x_aXe y_aXi of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    };
                  Test.RightAssociative -> GHC.Bool.True;
                  Test.NotAssociative -> GHC.Bool.True
                };
              Test.RightAssociative ->
                case b1_atH of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.RightAssociative ->
                    case a21_atG of _ { GHC.Types.I# x_aXe ->
                    case b2_atI of _ { GHC.Types.I# y_aXi ->
                    case GHC.Prim.==# x_aXe y_aXi of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                };
              Test.NotAssociative ->
                case b1_atH of _ {
                  __DEFAULT -> GHC.Bool.True;
                  Test.NotAssociative ->
                    case a21_atG of _ { GHC.Types.I# x_aXe ->
                    case b2_atI of _ { GHC.Types.I# y_aXi ->
                    case GHC.Prim.==# x_aXe y_aXi of _ {
                      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
                    }
                    }
                    }
                }
            }
        }
    }

Test.$fEqFixity_$c== :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 4}]
Test.$fEqFixity_$c== =
  \ (ds_dSJ :: Test.Fixity) (ds1_dSK :: Test.Fixity) ->
    case ds_dSJ of _ {
      Test.Prefix ->
        case ds1_dSK of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix a12_atF a2_atG ->
        case ds1_dSK of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix b1_atH b2_atI ->
            case a12_atF of _ {
              Test.LeftAssociative ->
                case b1_atH of _ {
                  Test.LeftAssociative ->
                    case a2_atG of _ { GHC.Types.I# x_aXe ->
                    case b2_atI of _ { GHC.Types.I# y_aXi -> GHC.Prim.==# x_aXe y_aXi }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case b1_atH of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case a2_atG of _ { GHC.Types.I# x_aXe ->
                    case b2_atI of _ { GHC.Types.I# y_aXi -> GHC.Prim.==# x_aXe y_aXi }
                    }
                };
              Test.NotAssociative ->
                case b1_atH of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case a2_atG of _ { GHC.Types.I# x_aXe ->
                    case b2_atI of _ { GHC.Types.I# y_aXi -> GHC.Prim.==# x_aXe y_aXi }
                    }
                }
            }
        }
    }

Test.$fEqFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqFixity_$c==,
                            Test.$fEqFixity_$c/=]]
Test.$fEqFixity =
  GHC.Classes.D:Eq
    @ Test.Fixity Test.$fEqFixity_$c== Test.$fEqFixity_$c/=

Test.$fOrdFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqFixity,
                             Test.$fOrdFixity_$ccompare, Test.$fOrdFixity_$c<,
                             Test.$fOrdFixity_$c>=, Test.$fOrdFixity_$c>, Test.$fOrdFixity_$c<=,
                             Test.$fOrdFixity_$cmax, Test.$fOrdFixity_$cmin]]
Test.$fOrdFixity =
  GHC.Classes.D:Ord
    @ Test.Fixity
    Test.$fEqFixity
    Test.$fOrdFixity_$ccompare
    Test.$fOrdFixity_$c<
    Test.$fOrdFixity_$c>=
    Test.$fOrdFixity_$c>
    Test.$fOrdFixity_$c<=
    Test.$fOrdFixity_$cmax
    Test.$fOrdFixity_$cmin

Test.$fReadAssociativity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Associativity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [Test.Associativity] Test.$fReadAssociativity16

lvl21_r1mb
  :: forall b_a15H.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15H
[GblId, Arity=1, Caf=NoCafRefs]
lvl21_r1mb =
  \ (@ b_a15H) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15H
    }

lvl22_r1md :: forall b_a15H. Text.ParserCombinators.ReadP.P b_a15H
[GblId, Caf=NoCafRefs]
lvl22_r1md =
  \ (@ b_a15H) ->
    Text.ParserCombinators.ReadP.Look @ b_a15H (lvl21_r1mb @ b_a15H)

lvl23_r1mf
  :: forall b_a15H.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_a15H
[GblId, Arity=1, Caf=NoCafRefs]
lvl23_r1mf =
  \ (@ b_a15H) (a111_a16U :: GHC.Base.String) ->
    case a111_a16U of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_a15H
    }

lvl24_r1mh :: forall b_a15H. Text.ParserCombinators.ReadP.P b_a15H
[GblId, Caf=NoCafRefs]
lvl24_r1mh =
  \ (@ b_a15H) ->
    Text.ParserCombinators.ReadP.Look @ b_a15H (lvl23_r1mf @ b_a15H)

Rec {
Test.$fReadAssociativity_a1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId, Arity=1, Str=DmdType L]
Test.$fReadAssociativity_a1 =
  \ (n_a15F :: Text.ParserCombinators.ReadPrec.Prec) ->
    let {
      ds2_s1kj [Dmd=Just L]
        :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
      [LclId, Str=DmdType]
      ds2_s1kj =
        (Test.$fReadAssociativity3
         `cast` (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                   Test.Associativity
                 :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
                      ~
                    (Text.ParserCombinators.ReadPrec.Prec
                     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)))
          n_a15F } in
    (\ (@ b_a15H)
       (k1_a15I
          :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_a15H) ->
       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
         @ b_a15H
         ((ds2_s1kj
           `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                   :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                        ~
                      (forall b_aWA.
                       (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                       -> Text.ParserCombinators.ReadP.P b_aWA)))
            @ b_a15H k1_a15I)
         (Text.ParserCombinators.ReadP.Look
            @ b_a15H
            (let {
               $wk_s1kx [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15H
               [LclId, Str=DmdType]
               $wk_s1kx =
                 Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                   @ b_a15H
                   (lvl22_r1md @ b_a15H)
                   (Text.Read.Lex.hsLex2
                      @ b_a15H
                      (let {
                         lvl81_s1kt :: Text.ParserCombinators.ReadP.P b_a15H
                         [LclId, Str=DmdType]
                         lvl81_s1kt =
                           ((Test.$fReadAssociativity_a1
                               Text.ParserCombinators.ReadPrec.minPrec)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                                    :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                         ~
                                       (forall b_aWA.
                                        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                                        -> Text.ParserCombinators.ReadP.P b_aWA)))
                             @ b_a15H
                             (\ (a12_a162 :: Test.Associativity) ->
                                Text.ParserCombinators.ReadP.Look
                                  @ b_a15H
                                  (let {
                                     $wk1_s1kp [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_a15H
                                     [LclId, Str=DmdType]
                                     $wk1_s1kp =
                                       Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                                         @ b_a15H
                                         (lvl24_r1mh @ b_a15H)
                                         (Text.Read.Lex.hsLex2
                                            @ b_a15H
                                            (let {
                                               lvl91_s1kl :: Text.ParserCombinators.ReadP.P b_a15H
                                               [LclId, Str=DmdType]
                                               lvl91_s1kl = k1_a15I a12_a162 } in
                                             \ (a111_a164 :: Text.Read.Lex.Lexeme) ->
                                               case a111_a164 of _ {
                                                 __DEFAULT ->
                                                   Text.ParserCombinators.ReadP.Fail @ b_a15H;
                                                 Text.Read.Lex.Punc ds_a167 ->
                                                   case ds_a167 of _ {
                                                     [] ->
                                                       Text.ParserCombinators.ReadP.Fail @ b_a15H;
                                                     : ds1_a16c ds21_a16d ->
                                                       case ds1_a16c of _ { GHC.Types.C# ds3_a16h ->
                                                       case ds3_a16h of _ {
                                                         __DEFAULT ->
                                                           Text.ParserCombinators.ReadP.Fail
                                                             @ b_a15H;
                                                         ')' ->
                                                           case ds21_a16d of _ {
                                                             [] -> lvl91_s1kl;
                                                             : _ _ ->
                                                               Text.ParserCombinators.ReadP.Fail
                                                                 @ b_a15H
                                                           }
                                                       }
                                                       }
                                                   }
                                               })) } in
                                   let {
                                     k_s1kr :: () -> Text.ParserCombinators.ReadP.P b_a15H
                                     [LclId, Arity=1, Str=DmdType A]
                                     k_s1kr = \ _ -> $wk1_s1kp } in
                                   \ (a111_a17d :: GHC.Base.String) ->
                                     ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                                      `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                                              :: Text.ParserCombinators.ReadP.ReadP ()
                                                   ~
                                                 (forall b_aWA.
                                                  (() -> Text.ParserCombinators.ReadP.P b_aWA)
                                                  -> Text.ParserCombinators.ReadP.P b_aWA)))
                                       @ b_a15H k_s1kr)) } in
                       \ (a12_a16p :: Text.Read.Lex.Lexeme) ->
                         case a12_a16p of _ {
                           __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15H;
                           Text.Read.Lex.Punc ds_a16s ->
                             case ds_a16s of _ {
                               [] -> Text.ParserCombinators.ReadP.Fail @ b_a15H;
                               : ds1_a16x ds21_a16y ->
                                 case ds1_a16x of _ { GHC.Types.C# ds3_a16C ->
                                 case ds3_a16C of _ {
                                   __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_a15H;
                                   '(' ->
                                     case ds21_a16y of _ {
                                       [] -> lvl81_s1kt;
                                       : _ _ -> Text.ParserCombinators.ReadP.Fail @ b_a15H
                                     }
                                 }
                                 }
                             }
                         })) } in
             let {
               k_s1kz :: () -> Text.ParserCombinators.ReadP.P b_a15H
               [LclId, Arity=1, Str=DmdType A]
               k_s1kz = \ _ -> $wk_s1kx } in
             \ (a111_a17d :: GHC.Base.String) ->
               ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a17d)
                `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                        :: Text.ParserCombinators.ReadP.ReadP ()
                             ~
                           (forall b_aWA.
                            (() -> Text.ParserCombinators.ReadP.P b_aWA)
                            -> Text.ParserCombinators.ReadP.P b_aWA)))
                 @ b_a15H k_s1kz)))
    `cast` (sym
              (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity)
            :: (forall b_aWA.
                (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                -> Text.ParserCombinators.ReadP.P b_aWA)
                 ~
               Text.ParserCombinators.ReadP.ReadP Test.Associativity)
end Rec }

Test.$fReadAssociativity_$creadsPrec
  :: GHC.Types.Int
     -> Text.ParserCombinators.ReadP.ReadS Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 5 6}]
Test.$fReadAssociativity_$creadsPrec =
  \ (eta_aXF :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Associativity
      (((Test.$fReadAssociativity_a1 eta_aXF)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                     ~
                   (forall b_aWA.
                    (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWA)
                    -> Text.ParserCombinators.ReadP.P b_aWA)))
         @ Test.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn
            @ Test.Associativity))

Test.$fReadAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadAssociativity_$creadsPrec,
                           Test.$fReadAssociativity_readListDefault,
                           (Test.$fReadAssociativity2
                            `cast` (right
                                      (inst
                                         (forall a_aXl.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXl
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXl))
                                         Test.Associativity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         Test.Associativity)),
                           (Test.$fReadAssociativity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                         [Test.Associativity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         [Test.Associativity]))]]
Test.$fReadAssociativity =
  GHC.Read.D:Read
    @ Test.Associativity
    Test.$fReadAssociativity_$creadsPrec
    Test.$fReadAssociativity_readListDefault
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))
    (Test.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [Test.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Associativity]))

Test.$fOrdAssociativity_$cmin
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmin =
  \ (x_aYY :: Test.Associativity) (y_aYZ :: Test.Associativity) ->
    case x_aYY of _ {
      Test.LeftAssociative ->
        case y_aYZ of _ { __DEFAULT -> Test.LeftAssociative };
      Test.RightAssociative ->
        case y_aYZ of _ {
          Test.LeftAssociative -> Test.LeftAssociative;
          Test.RightAssociative -> Test.RightAssociative;
          Test.NotAssociative -> Test.RightAssociative
        };
      Test.NotAssociative -> y_aYZ
    }

Test.$fOrdAssociativity_$cmax
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmax =
  \ (x_aZ7 :: Test.Associativity) (y_aZ8 :: Test.Associativity) ->
    case x_aZ7 of _ {
      Test.LeftAssociative -> y_aZ8;
      Test.RightAssociative ->
        case y_aZ8 of _ {
          __DEFAULT -> Test.RightAssociative;
          Test.NotAssociative -> Test.NotAssociative
        };
      Test.NotAssociative ->
        case y_aZ8 of _ { __DEFAULT -> Test.NotAssociative }
    }

Test.$fOrdAssociativity_$c<=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c<= =
  \ (x_aZg :: Test.Associativity) (y_aZh :: Test.Associativity) ->
    case x_aZg of _ {
      Test.LeftAssociative ->
        case y_aZh of _ { __DEFAULT -> GHC.Bool.True };
      Test.RightAssociative ->
        case y_aZh of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZh of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fOrdAssociativity_$c>
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c> =
  \ (x_aZn :: Test.Associativity) (y_aZo :: Test.Associativity) ->
    case x_aZn of _ {
      Test.LeftAssociative ->
        case y_aZo of _ { __DEFAULT -> GHC.Bool.False };
      Test.RightAssociative ->
        case y_aZo of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZo of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fOrdAssociativity_$c>=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c>= =
  \ (x_aZu :: Test.Associativity) (y_aZv :: Test.Associativity) ->
    case x_aZu of _ {
      Test.LeftAssociative ->
        case y_aZv of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case y_aZv of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZv of _ { __DEFAULT -> GHC.Bool.True }
    }

Test.$fOrdAssociativity_$c<
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c< =
  \ (x_aZB :: Test.Associativity) (y_aZC :: Test.Associativity) ->
    case x_aZB of _ {
      Test.LeftAssociative ->
        case y_aZC of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case y_aZC of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZC of _ { __DEFAULT -> GHC.Bool.False }
    }

Test.$fOrdAssociativity_$ccompare
  :: Test.Associativity
     -> Test.Associativity
     -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$ccompare =
  \ (a2_atA :: Test.Associativity) (b_atB :: Test.Associativity) ->
    case a2_atA of _ {
      Test.LeftAssociative ->
        case b_atB of _ {
          Test.LeftAssociative -> GHC.Ordering.EQ;
          Test.RightAssociative -> GHC.Ordering.LT;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.RightAssociative ->
        case b_atB of _ {
          Test.LeftAssociative -> GHC.Ordering.GT;
          Test.RightAssociative -> GHC.Ordering.EQ;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.NotAssociative ->
        case b_atB of _ {
          __DEFAULT -> GHC.Ordering.GT;
          Test.NotAssociative -> GHC.Ordering.EQ
        }
    }

Test.$fShowAssociativity_$cshowList
  :: [Test.Associativity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 89 3}]
Test.$fShowAssociativity_$cshowList =
  \ (ds1_aYe :: [Test.Associativity]) (s_aYf :: GHC.Base.String) ->
    case ds1_aYe of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYf;
      : x_aYk xs_aYl ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (case x_aYk of _ {
             Test.LeftAssociative ->
               GHC.Base.++
                 @ GHC.Types.Char
                 Test.$fReadAssociativity15
                 (let {
                    lvl141_s1kB :: [GHC.Types.Char]
                    [LclId, Str=DmdType]
                    lvl141_s1kB =
                      GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYf } in
                  letrec {
                    showl_s1kD [Occ=LoopBreaker]
                      :: [Test.Associativity] -> [GHC.Types.Char]
                    [LclId, Arity=1, Str=DmdType S]
                    showl_s1kD =
                      \ (ds2_aYp :: [Test.Associativity]) ->
                        case ds2_aYp of _ {
                          [] -> lvl141_s1kB;
                          : y_aYu ys_aYv ->
                            GHC.Types.:
                              @ GHC.Types.Char
                              GHC.Show.showList__1
                              (case y_aYu of _ {
                                 Test.LeftAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity15
                                     (showl_s1kD ys_aYv);
                                 Test.RightAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity12
                                     (showl_s1kD ys_aYv);
                                 Test.NotAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1kD ys_aYv)
                               })
                        }; } in
                  showl_s1kD xs_aYl);
             Test.RightAssociative ->
               GHC.Base.++
                 @ GHC.Types.Char
                 Test.$fReadAssociativity12
                 (let {
                    lvl141_s1kF :: [GHC.Types.Char]
                    [LclId, Str=DmdType]
                    lvl141_s1kF =
                      GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYf } in
                  letrec {
                    showl_s1kH [Occ=LoopBreaker]
                      :: [Test.Associativity] -> [GHC.Types.Char]
                    [LclId, Arity=1, Str=DmdType S]
                    showl_s1kH =
                      \ (ds2_aYp :: [Test.Associativity]) ->
                        case ds2_aYp of _ {
                          [] -> lvl141_s1kF;
                          : y_aYu ys_aYv ->
                            GHC.Types.:
                              @ GHC.Types.Char
                              GHC.Show.showList__1
                              (case y_aYu of _ {
                                 Test.LeftAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity15
                                     (showl_s1kH ys_aYv);
                                 Test.RightAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity12
                                     (showl_s1kH ys_aYv);
                                 Test.NotAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1kH ys_aYv)
                               })
                        }; } in
                  showl_s1kH xs_aYl);
             Test.NotAssociative ->
               GHC.Base.++
                 @ GHC.Types.Char
                 Test.$fReadAssociativity9
                 (let {
                    lvl141_s1kJ :: [GHC.Types.Char]
                    [LclId, Str=DmdType]
                    lvl141_s1kJ =
                      GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYf } in
                  letrec {
                    showl_s1kL [Occ=LoopBreaker]
                      :: [Test.Associativity] -> [GHC.Types.Char]
                    [LclId, Arity=1, Str=DmdType S]
                    showl_s1kL =
                      \ (ds2_aYp :: [Test.Associativity]) ->
                        case ds2_aYp of _ {
                          [] -> lvl141_s1kJ;
                          : y_aYu ys_aYv ->
                            GHC.Types.:
                              @ GHC.Types.Char
                              GHC.Show.showList__1
                              (case y_aYu of _ {
                                 Test.LeftAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity15
                                     (showl_s1kL ys_aYv);
                                 Test.RightAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char
                                     Test.$fReadAssociativity12
                                     (showl_s1kL ys_aYv);
                                 Test.NotAssociative ->
                                   GHC.Base.++
                                     @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1kL ys_aYv)
                               })
                        }; } in
                  showl_s1kL xs_aYl)
           })
    }

Test.$fShowAssociativity_$cshow
  :: Test.Associativity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0}]
Test.$fShowAssociativity_$cshow =
  \ (x_aYE :: Test.Associativity) ->
    case x_aYE of _ {
      Test.LeftAssociative -> Test.$fReadAssociativity15;
      Test.RightAssociative -> Test.$fReadAssociativity12;
      Test.NotAssociative -> Test.$fReadAssociativity9
    }

Test.$fShowAssociativity_$cshowsPrec
  :: GHC.Types.Int -> Test.Associativity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType ASL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, -)
         Tmpl= \ _
                 (ds1_dSz [Occ=Once!] :: Test.Associativity)
                 (eta_B1 [Occ=Once*] :: GHC.Base.String) ->
                 case ds1_dSz of _ {
                   Test.LeftAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a18z)
                          (c_a18A [Occ=Once] :: GHC.Types.Char -> b_a18z -> b_a18z)
                          (n_a18B [Occ=Once] :: b_a18z) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a18z c_a18A n_a18B Test.$fReadAssociativity15)
                       eta_B1;
                   Test.RightAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a18z)
                          (c_a18A [Occ=Once] :: GHC.Types.Char -> b_a18z -> b_a18z)
                          (n_a18B [Occ=Once] :: b_a18z) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a18z c_a18A n_a18B Test.$fReadAssociativity12)
                       eta_B1;
                   Test.NotAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a18z)
                          (c_a18A [Occ=Once] :: GHC.Types.Char -> b_a18z -> b_a18z)
                          (n_a18B [Occ=Once] :: b_a18z) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a18z c_a18A n_a18B Test.$fReadAssociativity9)
                       eta_B1
                 }}]
Test.$fShowAssociativity_$cshowsPrec =
  \ _ (ds1_dSz :: Test.Associativity) (eta_B1 :: GHC.Base.String) ->
    case ds1_dSz of _ {
      Test.LeftAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_B1;
      Test.RightAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_B1;
      Test.NotAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_B1
    }

Test.$fShowAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowAssociativity_$cshowsPrec,
                           Test.$fShowAssociativity_$cshow,
                           Test.$fShowAssociativity_$cshowList]]
Test.$fShowAssociativity =
  GHC.Show.D:Show
    @ Test.Associativity
    Test.$fShowAssociativity_$cshowsPrec
    Test.$fShowAssociativity_$cshow
    Test.$fShowAssociativity_$cshowList

Test.$fEqAssociativity_$c/=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c/= =
  \ (a2_aty :: Test.Associativity) (b_atz :: Test.Associativity) ->
    case a2_aty of _ {
      Test.LeftAssociative ->
        case b_atz of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case b_atz of _ {
          __DEFAULT -> GHC.Bool.True; Test.RightAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case b_atz of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fEqAssociativity_$c==
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c== =
  \ (a2_atu :: Test.Associativity) (b_atv :: Test.Associativity) ->
    case a2_atu of _ {
      Test.LeftAssociative ->
        case b_atv of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case b_atv of _ {
          __DEFAULT -> GHC.Bool.False; Test.RightAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case b_atv of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fEqAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqAssociativity_$c==,
                            Test.$fEqAssociativity_$c/=]]
Test.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ Test.Associativity
    Test.$fEqAssociativity_$c==
    Test.$fEqAssociativity_$c/=

Test.$fOrdAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqAssociativity,
                             Test.$fOrdAssociativity_$ccompare, Test.$fOrdAssociativity_$c<,
                             Test.$fOrdAssociativity_$c>=, Test.$fOrdAssociativity_$c>,
                             Test.$fOrdAssociativity_$c<=, Test.$fOrdAssociativity_$cmax,
                             Test.$fOrdAssociativity_$cmin]]
Test.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ Test.Associativity
    Test.$fEqAssociativity
    Test.$fOrdAssociativity_$ccompare
    Test.$fOrdAssociativity_$c<
    Test.$fOrdAssociativity_$c>=
    Test.$fOrdAssociativity_$c>
    Test.$fOrdAssociativity_$c<=
    Test.$fOrdAssociativity_$cmax
    Test.$fOrdAssociativity_$cmin


------ Local rules for imported ids --------
"SPEC Test.$dmconFixity [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10E [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconFixity @ Test.Logic_F_ $dConstructor_s10E
      = Test.$dmconFixity_$s$dmconFixity7
"SPEC Test.$dmconFixity [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10H [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconFixity @ Test.Logic_T_ $dConstructor_s10H
      = Test.$dmconFixity_$s$dmconFixity6
"SPEC Test.$dmconFixity [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10K [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconFixity @ Test.Logic_Not_ $dConstructor_s10K
      = Test.$dmconFixity_$s$dmconFixity5
"SPEC Test.$dmconFixity [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10N [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconFixity @ Test.Logic_Disj_ $dConstructor_s10N
      = Test.$dmconFixity_$s$dmconFixity4
"SPEC Test.$dmconFixity [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10Q [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconFixity @ Test.Logic_Conj_ $dConstructor_s10Q
      = Test.$dmconFixity_$s$dmconFixity3
"SPEC Test.$dmconFixity [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s10T [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconFixity @ Test.Logic_Equiv_ $dConstructor_s10T
      = Test.$dmconFixity_$s$dmconFixity2
"SPEC Test.$dmconFixity [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s10W [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconFixity @ Test.Logic_Impl_ $dConstructor_s10W
      = Test.$dmconFixity_$s$dmconFixity1
"SPEC Test.$dmconFixity [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s10Z [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconFixity @ Test.Logic_Var_ $dConstructor_s10Z
      = Test.$dmconFixity_$s$dmconFixity
"SPEC Test.$dmconIsRecord [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10g [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconIsRecord @ Test.Logic_F_ $dConstructor_s10g
      = Test.$dmconIsRecord_$s$dmconIsRecord7
"SPEC Test.$dmconIsRecord [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10j [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconIsRecord @ Test.Logic_T_ $dConstructor_s10j
      = Test.$dmconIsRecord_$s$dmconIsRecord6
"SPEC Test.$dmconIsRecord [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10m [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconIsRecord @ Test.Logic_Not_ $dConstructor_s10m
      = Test.$dmconIsRecord_$s$dmconIsRecord5
"SPEC Test.$dmconIsRecord [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10p [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconIsRecord @ Test.Logic_Disj_ $dConstructor_s10p
      = Test.$dmconIsRecord_$s$dmconIsRecord4
"SPEC Test.$dmconIsRecord [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10s [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconIsRecord @ Test.Logic_Conj_ $dConstructor_s10s
      = Test.$dmconIsRecord_$s$dmconIsRecord3
"SPEC Test.$dmconIsRecord [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s10v [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconIsRecord @ Test.Logic_Equiv_ $dConstructor_s10v
      = Test.$dmconIsRecord_$s$dmconIsRecord2
"SPEC Test.$dmconIsRecord [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s10y [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconIsRecord @ Test.Logic_Impl_ $dConstructor_s10y
      = Test.$dmconIsRecord_$s$dmconIsRecord1
"SPEC Test.$dmconIsRecord [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s10B [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconIsRecord @ Test.Logic_Var_ $dConstructor_s10B
      = Test.$dmconIsRecord_$s$dmconIsRecord
"SPEC Test.updateString [Test.Logic]" ALWAYS
    forall {$dRegular_s104 :: Test.Regular Test.Logic
            $dUpdateString_X17m :: Test.UpdateString (Test.PF Test.Logic)}
      Test.updateString @ Test.Logic $dRegular_s104 $dUpdateString_X17m
      = Test.updateString_$supdateString


