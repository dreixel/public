
==================== Tidy Core ====================
Test.updateStringf
  :: forall (f_ah5 :: * -> *).
     (Test.UpdateString f_ah5) =>
     forall a_ah6. (a_ah6 -> a_ah6) -> f_ah5 a_ah6 -> f_ah5 a_ah6
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.updateStringf =
  \ (@ f_ah5::* -> *)
    (tpl_B1 [Occ=Once] :: Test.UpdateString f_ah5) ->
    tpl_B1
    `cast` (Test.NTCo:T:UpdateString f_ah5
            :: Test.T:UpdateString f_ah5
                 ~
               (forall a_ah6. (a_ah6 -> a_ah6) -> f_ah5 a_ah6 -> f_ah5 a_ah6))

Test.from [InlPrag=NOINLINE]
  :: forall a_ah7.
     (Test.Regular a_ah7) =>
     a_ah7 -> Test.PF a_ah7 a_ah7
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.from: "Class op from"]
Test.from =
  \ (@ a_ah7) (tpl_B1 [Occ=Once!] :: Test.Regular a_ah7) ->
    case tpl_B1 of _ { Test.D:Regular tpl_B2 [Occ=Once] _ -> tpl_B2 }

Test.to [InlPrag=NOINLINE]
  :: forall a_ah7.
     (Test.Regular a_ah7) =>
     Test.PF a_ah7 a_ah7 -> a_ah7
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.to: "Class op to"]
Test.to =
  \ (@ a_ah7) (tpl_B1 [Occ=Once!] :: Test.Regular a_ah7) ->
    case tpl_B1 of _ { Test.D:Regular _ tpl_B3 [Occ=Once] -> tpl_B3 }

Test.conName [InlPrag=NOINLINE]
  :: forall c_ah9.
     (Test.Constructor c_ah9) =>
     forall (t_aha :: * -> (* -> *) -> * -> *) (f_ahb :: * -> *) r_ahc.
     t_aha c_ah9 f_ahb r_ahc -> GHC.Base.String
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(SAA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conName: "Class op conName"]
Test.conName =
  \ (@ c_ah9) (tpl_B1 [Occ=Once!] :: Test.Constructor c_ah9) ->
    case tpl_B1 of _ { Test.D:Constructor tpl_B2 [Occ=Once] _ _ ->
    tpl_B2
    }

Test.conFixity [InlPrag=NOINLINE]
  :: forall c_ah9.
     (Test.Constructor c_ah9) =>
     forall (t_ahd :: * -> (* -> *) -> * -> *) (f_ahe :: * -> *) r_ahf.
     t_ahd c_ah9 f_ahe r_ahf -> Test.Fixity
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(ASA),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conFixity: "Class op conFixity"]
Test.conFixity =
  \ (@ c_ah9) (tpl_B1 [Occ=Once!] :: Test.Constructor c_ah9) ->
    case tpl_B1 of _ { Test.D:Constructor _ tpl_B3 [Occ=Once] _ ->
    tpl_B3
    }

Test.conIsRecord [InlPrag=NOINLINE]
  :: forall c_ah9.
     (Test.Constructor c_ah9) =>
     forall (t_ahg :: * -> (* -> *) -> * -> *) (f_ahh :: * -> *) r_ahi.
     t_ahg c_ah9 f_ahh r_ahi -> GHC.Bool.Bool
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(AAS),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0},
 RULES: Built in rule for Test.conIsRecord: "Class op conIsRecord"]
Test.conIsRecord =
  \ (@ c_ah9) (tpl_B1 [Occ=Once!] :: Test.Constructor c_ah9) ->
    case tpl_B1 of _ { Test.D:Constructor _ _ tpl_B4 [Occ=Once] ->
    tpl_B4
    }

Test.$fShowFixity2 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity2 = GHC.Base.unpackCString# "Infix "

Test.$fShowFixity3 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fShowFixity3 = GHC.Base.unpackCString# "Prefix"

Test.$fReadAssociativity9 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity9 =
  GHC.Base.unpackCString# "NotAssociative"

Test.$fReadAssociativity12 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity12 =
  GHC.Base.unpackCString# "RightAssociative"

Test.$fReadAssociativity15 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 7 0}]
Test.$fReadAssociativity15 =
  GHC.Base.unpackCString# "LeftAssociative"

lvl_r1d0 :: [GHC.Types.Char]
[GblId, Str=DmdType]
lvl_r1d0 = GHC.Base.unpackCString# "Infix"

lvl1_r1d2 :: forall c_awj. Test.C c_awj Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl1_r1d2 =
  \ (@ c_awj) ->
    Test.C @ c_awj @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl2_r1d4
  :: forall c_awj c1_awx c2_awQ c3_axc c4_axB c5_ay1.
     (Test.:+:)
       (Test.C c_awj Test.U)
       (Test.C c1_awx Test.I
        Test.:+: (Test.C c2_awQ (Test.I Test.:*: Test.I)
                  Test.:+: (Test.C c3_axc (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axB (Test.I Test.:*: Test.I)
                                      Test.:+: Test.C c5_ay1 (Test.I Test.:*: Test.I)))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl2_r1d4 =
  \ (@ c_awj)
    (@ c1_awx)
    (@ c2_awQ)
    (@ c3_axc)
    (@ c4_axB)
    (@ c5_ay1) ->
    Test.L
      @ (Test.C c_awj Test.U)
      @ (Test.C c1_awx Test.I
         Test.:+: (Test.C c2_awQ (Test.I Test.:*: Test.I)
                   Test.:+: (Test.C c3_axc (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axB (Test.I Test.:*: Test.I)
                                       Test.:+: Test.C c5_ay1 (Test.I Test.:*: Test.I)))))
      @ Test.Logic
      (lvl1_r1d2 @ c_awj)

lvl3_r1d6
  :: forall c_aw7 c1_awj c2_awx c3_awQ c4_axc c5_axB c6_ay1.
     (Test.:+:)
       (Test.C c_aw7 Test.U)
       (Test.C c1_awj Test.U
        Test.:+: (Test.C c2_awx Test.I
                  Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay1 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl3_r1d6 =
  \ (@ c_aw7)
    (@ c1_awj)
    (@ c2_awx)
    (@ c3_awQ)
    (@ c4_axc)
    (@ c5_axB)
    (@ c6_ay1) ->
    Test.R
      @ (Test.C c_aw7 Test.U)
      @ (Test.C c1_awj Test.U
         Test.:+: (Test.C c2_awx Test.I
                   Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay1 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl2_r1d4 @ c1_awj @ c2_awx @ c3_awQ @ c4_axc @ c5_axB @ c6_ay1)

lvl4_r1d8
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     (Test.:+:)
       (Test.C c_avX (Test.K GHC.Base.String))
       (Test.C c1_aw7 Test.U
        Test.:+: (Test.C c2_awj Test.U
                  Test.:+: (Test.C c3_awx Test.I
                            Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay1
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl4_r1d8 =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1) ->
    Test.R
      @ (Test.C c_avX (Test.K GHC.Base.String))
      @ (Test.C c1_aw7 Test.U
         Test.:+: (Test.C c2_awj Test.U
                   Test.:+: (Test.C c3_awx Test.I
                             Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay1
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl3_r1d6
         @ c1_aw7 @ c2_awj @ c3_awx @ c4_awQ @ c5_axc @ c6_axB @ c7_ay1)

lvl5_r1da :: forall c_aw7. Test.C c_aw7 Test.U Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType m]
lvl5_r1da =
  \ (@ c_aw7) ->
    Test.C @ c_aw7 @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl6_r1dc
  :: forall c_aw7 c1_awj c2_awx c3_awQ c4_axc c5_axB c6_ay1.
     (Test.:+:)
       (Test.C c_aw7 Test.U)
       (Test.C c1_awj Test.U
        Test.:+: (Test.C c2_awx Test.I
                  Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c6_ay1 (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl6_r1dc =
  \ (@ c_aw7)
    (@ c1_awj)
    (@ c2_awx)
    (@ c3_awQ)
    (@ c4_axc)
    (@ c5_axB)
    (@ c6_ay1) ->
    Test.L
      @ (Test.C c_aw7 Test.U)
      @ (Test.C c1_awj Test.U
         Test.:+: (Test.C c2_awx Test.I
                   Test.:+: (Test.C c3_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c4_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C
                                                            c6_ay1 (Test.I Test.:*: Test.I))))))
      @ Test.Logic
      (lvl5_r1da @ c_aw7)

lvl7_r1de
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     (Test.:+:)
       (Test.C c_avX (Test.K GHC.Base.String))
       (Test.C c1_aw7 Test.U
        Test.:+: (Test.C c2_awj Test.U
                  Test.:+: (Test.C c3_awx Test.I
                            Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     c7_ay1
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs, Str=DmdType]
lvl7_r1de =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1) ->
    Test.R
      @ (Test.C c_avX (Test.K GHC.Base.String))
      @ (Test.C c1_aw7 Test.U
         Test.:+: (Test.C c2_awj Test.U
                   Test.:+: (Test.C c3_awx Test.I
                             Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                           Test.:+: Test.C
                                                                      c7_ay1
                                                                      (Test.I Test.:*: Test.I)))))))
      @ Test.Logic
      (lvl6_r1dc
         @ c1_aw7 @ c2_awj @ c3_awx @ c4_awQ @ c5_axc @ c6_axB @ c7_ay1)

Test.$fConstructorLogic_Var_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Var_1 = GHC.Base.unpackCString# "Var"

Test.$fConstructorLogic_Impl_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Impl_1 = GHC.Base.unpackCString# "Impl"

Test.$fConstructorLogic_Equiv_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 5 0}]
Test.$fConstructorLogic_Equiv_1 = GHC.Base.unpackCString# "Equiv"

Test.$fConstructorLogic_Conj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Conj_1 = GHC.Base.unpackCString# "Conj"

Test.$fConstructorLogic_Disj_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Disj_1 = GHC.Base.unpackCString# "Disj"

Test.$fConstructorLogic_Not_1 :: [GHC.Types.Char]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 4 0}]
Test.$fConstructorLogic_Not_1 = GHC.Base.unpackCString# "Not"

Test.$fConstructorLogic_T_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_T_2 = GHC.Types.C# 'T'

Test.$fConstructorLogic_T_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_T_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_T_2
    (GHC.Types.[] @ GHC.Types.Char)

Test.$fConstructorLogic_F_2 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fConstructorLogic_F_2 = GHC.Types.C# 'F'

Test.$fConstructorLogic_F_1 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fConstructorLogic_F_1 =
  GHC.Types.:
    @ GHC.Types.Char
    Test.$fConstructorLogic_F_2
    (GHC.Types.[] @ GHC.Types.Char)

Test.$dmconIsRecord_$s$dmconIsRecord7
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_F_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord7 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord6
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_T_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord6 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord5
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_Not_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord5 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord4
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_Disj_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord4 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord3
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_Conj_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord3 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord2
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_Equiv_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord2 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord1
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_Impl_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord1 =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconIsRecord_$s$dmconIsRecord
  :: forall (t_ahg :: * -> (* -> *) -> * -> *)
            (f_ahh :: * -> *)
            r_ahi.
     t_ahg Test.Logic_Var_ f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord_$s$dmconIsRecord =
  \ (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconFixity_$s$dmconFixity7
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_F_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity7 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity6
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_T_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity6 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity5
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_Not_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity5 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity4
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_Disj_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity4 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity3
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_Conj_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity3 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity2
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_Equiv_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity2 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity1
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_Impl_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity1 =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$dmconFixity_$s$dmconFixity
  :: forall (t_ahd :: * -> (* -> *) -> * -> *)
            (f_ahe :: * -> *)
            r_ahf.
     t_ahd Test.Logic_Var_ f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity_$s$dmconFixity =
  \ (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.$fShowFixity1 :: GHC.Types.Int
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.$fShowFixity1 = GHC.Types.I# 0

Test.$fReadAssociativity14
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWo.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWo)
        -> Text.ParserCombinators.ReadP.P b_aWo
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity14 =
  \ _
    (@ b_aWo)
    (eta_aWp
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWo) ->
    eta_aWp Test.LeftAssociative

Test.$fReadAssociativity13
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity13 =
  (Test.$fReadAssociativity15,
   Test.$fReadAssociativity14
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWG.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWG)
                  -> Text.ParserCombinators.ReadP.P b_aWG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity11
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWo.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWo)
        -> Text.ParserCombinators.ReadP.P b_aWo
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity11 =
  \ _
    (@ b_aWo)
    (eta_aWp
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWo) ->
    eta_aWp Test.RightAssociative

Test.$fReadAssociativity10
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity10 =
  (Test.$fReadAssociativity12,
   Test.$fReadAssociativity11
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWG.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWG)
                  -> Text.ParserCombinators.ReadP.P b_aWG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity8
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWo.
        (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWo)
        -> Text.ParserCombinators.ReadP.P b_aWo
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 6] 2 0}]
Test.$fReadAssociativity8 =
  \ _
    (@ b_aWo)
    (eta_aWp
       :: Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWo) ->
    eta_aWp Test.NotAssociative

Test.$fReadAssociativity7
  :: ([GHC.Types.Char],
      Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity7 =
  (Test.$fReadAssociativity9,
   Test.$fReadAssociativity8
   `cast` (trans
             (Text.ParserCombinators.ReadPrec.Prec
              -> sym
                   (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity))
             (sym
                (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity))
           :: (Text.ParserCombinators.ReadPrec.Prec
               -> forall b_aWG.
                  (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWG)
                  -> Text.ParserCombinators.ReadP.P b_aWG)
                ~
              Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity6
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity6 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity7
    (GHC.Types.[]
       @ (GHC.Base.String,
          Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity5
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity5 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity10
    Test.$fReadAssociativity6

Test.$fReadAssociativity4
  :: [(GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)]
[GblId,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.$fReadAssociativity4 =
  GHC.Types.:
    @ (GHC.Base.String,
       Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity)
    Test.$fReadAssociativity13
    Test.$fReadAssociativity5

Test.$fReadAssociativity3
  :: Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity3 =
  GHC.Read.choose1 @ Test.Associativity Test.$fReadAssociativity4

Test.$fReadAssociativity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Associativity Test.$fReadAssociativity3

Test.$fReadAssociativity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadAssociativity1 =
  GHC.Read.$dmreadList2
    @ Test.Associativity
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))

Test.$fReadAssociativity16
  :: Text.ParserCombinators.ReadP.P [Test.Associativity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadAssociativity16 =
  ((Test.$fReadAssociativity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP
             [Test.Associativity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Associativity]
                ~
              (forall b_aWG.
               ([Test.Associativity] -> Text.ParserCombinators.ReadP.P b_aWG)
               -> Text.ParserCombinators.ReadP.P b_aWG)))
    @ [Test.Associativity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn
       @ [Test.Associativity])

lvl8_r1dU
  :: forall b_aWG.
     GHC.Base.String -> Text.ParserCombinators.ReadP.P b_aWG
[GblId, Arity=1, Caf=NoCafRefs]
lvl8_r1dU =
  \ (@ b_aWG) (a111_a198 :: GHC.Base.String) ->
    case a111_a198 of _ { __DEFAULT ->
    Text.ParserCombinators.ReadP.Fail @ b_aWG
    }

lvl9_r1dW :: forall b_aWG. Text.ParserCombinators.ReadP.P b_aWG
[GblId, Caf=NoCafRefs]
lvl9_r1dW =
  \ (@ b_aWG) ->
    Text.ParserCombinators.ReadP.Look @ b_aWG (lvl8_r1dU @ b_aWG)

lvl10_r1dY :: GHC.Types.Int
[GblId, Caf=NoCafRefs]
lvl10_r1dY = GHC.Types.I# 11

Test.$fReadFixity3
  :: Text.ParserCombinators.ReadPrec.Prec
     -> forall b_aWG.
        (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWG)
        -> Text.ParserCombinators.ReadP.P b_aWG
[GblId, Arity=2, Str=DmdType LL]
Test.$fReadFixity3 =
  \ (n_a16W :: Text.ParserCombinators.ReadPrec.Prec)
    (@ b_aWG)
    (eta_B1 :: Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWG) ->
    Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
      @ b_aWG
      (Text.ParserCombinators.ReadP.Look
         @ b_aWG
         (let {
            $wk_s1ac [Dmd=Just L] :: Text.ParserCombinators.ReadP.P b_aWG
            [LclId, Str=DmdType]
            $wk_s1ac =
              Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                @ b_aWG
                (lvl9_r1dW @ b_aWG)
                (Text.Read.Lex.hsLex2
                   @ b_aWG
                   (let {
                      lvl20_s1b7 :: Text.ParserCombinators.ReadP.P b_aWG
                      [LclId]
                      lvl20_s1b7 = eta_B1 Test.Prefix } in
                    \ (a11_aWU :: Text.Read.Lex.Lexeme) ->
                      case a11_aWU of _ {
                        __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_aWG;
                        Text.Read.Lex.Ident ds_dTa ->
                          case GHC.Base.eqString ds_dTa Test.$fShowFixity3 of _ {
                            GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_aWG;
                            GHC.Bool.True -> lvl20_s1b7
                          }
                      })) } in
          let {
            k_s1ae :: () -> Text.ParserCombinators.ReadP.P b_aWG
            [LclId, Arity=1, Str=DmdType A]
            k_s1ae = \ _ -> $wk_s1ac } in
          \ (a111_a19r :: GHC.Base.String) ->
            ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a19r)
             `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                     :: Text.ParserCombinators.ReadP.ReadP ()
                          ~
                        (forall b_aWG.
                         (() -> Text.ParserCombinators.ReadP.P b_aWG)
                         -> Text.ParserCombinators.ReadP.P b_aWG)))
              @ b_aWG k_s1ae))
      (case n_a16W of _ { GHC.Types.I# x_a16A ->
       case GHC.Prim.<=# x_a16A 10 of _ {
         GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_aWG;
         GHC.Bool.True ->
           Text.ParserCombinators.ReadP.Look
             @ b_aWG
             (let {
                $wk_s1am :: Text.ParserCombinators.ReadP.P b_aWG
                [LclId, Str=DmdType]
                $wk_s1am =
                  let {
                    lvl20_s1b8 :: Text.ParserCombinators.ReadP.P b_aWG
                    [LclId]
                    lvl20_s1b8 =
                      ((GHC.Read.$dmreadsPrec3
                          @ Test.Associativity Test.$fReadAssociativity3 lvl10_r1dY)
                       `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                               :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                                    ~
                                  (forall b_aWG.
                                   (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWG)
                                   -> Text.ParserCombinators.ReadP.P b_aWG)))
                        @ b_aWG
                        (\ (a11_X11f :: Test.Associativity) ->
                           ((GHC.Read.$dmreadsPrec11 lvl10_r1dY)
                            `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP GHC.Types.Int
                                    :: Text.ParserCombinators.ReadP.ReadP GHC.Types.Int
                                         ~
                                       (forall b_aWG.
                                        (GHC.Types.Int -> Text.ParserCombinators.ReadP.P b_aWG)
                                        -> Text.ParserCombinators.ReadP.P b_aWG)))
                             @ b_aWG
                             (\ (a12_X11M :: GHC.Types.Int) ->
                                eta_B1 (Test.Infix a11_X11f a12_X11M))) } in
                  Text.ParserCombinators.ReadP.$fMonadPlusP_$cmplus
                    @ b_aWG
                    (Text.ParserCombinators.ReadP.Look
                       @ b_aWG
                       (\ (a111_a198 :: GHC.Base.String) ->
                          case a111_a198 of _ { __DEFAULT ->
                          Text.ParserCombinators.ReadP.Fail @ b_aWG
                          }))
                    (Text.Read.Lex.hsLex2
                       @ b_aWG
                       (\ (a11_aWU :: Text.Read.Lex.Lexeme) ->
                          case a11_aWU of _ {
                            __DEFAULT -> Text.ParserCombinators.ReadP.Fail @ b_aWG;
                            Text.Read.Lex.Ident ds_dTf ->
                              case GHC.Base.eqString ds_dTf lvl_r1d0 of _ {
                                GHC.Bool.False -> Text.ParserCombinators.ReadP.Fail @ b_aWG;
                                GHC.Bool.True -> lvl20_s1b8
                              }
                          })) } in
              let {
                k_s1ao :: () -> Text.ParserCombinators.ReadP.P b_aWG
                [LclId, Arity=1, Str=DmdType A]
                k_s1ao = \ _ -> $wk_s1am } in
              \ (a111_a19r :: GHC.Base.String) ->
                ((Text.ParserCombinators.ReadP.skipSpaces_skip a111_a19r)
                 `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP ()
                         :: Text.ParserCombinators.ReadP.ReadP ()
                              ~
                            (forall b_aWG.
                             (() -> Text.ParserCombinators.ReadP.P b_aWG)
                             -> Text.ParserCombinators.ReadP.P b_aWG)))
                  @ b_aWG k_s1ao)
       }
       })

Test.$fReadFixity2
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity2 =
  GHC.Read.$dmreadsPrec3
    @ Test.Fixity
    (Test.$fReadFixity3
     `cast` (trans
               (Text.ParserCombinators.ReadPrec.Prec
                -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
               (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> forall b_aWG.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWG)
                    -> Text.ParserCombinators.ReadP.P b_aWG)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity1
  :: Text.ParserCombinators.ReadPrec.Prec
     -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.$fReadFixity1 =
  GHC.Read.$dmreadList2
    @ Test.Fixity
    (Test.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))

Test.$fReadFixity4 :: Text.ParserCombinators.ReadP.P [Test.Fixity]
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 3 0}]
Test.$fReadFixity4 =
  ((Test.$fReadFixity1 GHC.Read.$dmreadList1)
   `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP [Test.Fixity]
           :: Text.ParserCombinators.ReadP.ReadP [Test.Fixity]
                ~
              (forall b_aWG.
               ([Test.Fixity] -> Text.ParserCombinators.ReadP.P b_aWG)
               -> Text.ParserCombinators.ReadP.P b_aWG)))
    @ [Test.Fixity]
    (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ [Test.Fixity])

Test.unI1 :: forall r_auO. Test.I r_auO -> Test.I r_auO
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unI1 = \ (@ r_auO) (ds_dSA :: Test.I r_auO) -> ds_dSA

Test.unK1
  :: forall a_auQ r_auR. Test.K a_auQ r_auR -> Test.K a_auQ r_auR
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 0 0}]
Test.unK1 =
  \ (@ a_auQ) (@ r_auR) (ds_dSC :: Test.K a_auQ r_auR) -> ds_dSC

a_r1e6
  :: forall t_aBz r_aBC. (t_aBz -> r_aBC) -> Test.I t_aBz -> r_aBC
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType C(S)L]
a_r1e6 =
  \ (@ t_aBz)
    (@ r_aBC)
    (eta_X3J :: t_aBz -> r_aBC)
    (eta1_X7r :: Test.I t_aBz) ->
    eta_X3J
      (eta1_X7r `cast` (Test.NTCo:I t_aBz :: Test.I t_aBz ~ t_aBz))

a1_r1e8
  :: forall t_aAv t1_aAx t2_aAy r_aAB.
     t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy t1_aAx
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType AS]
a1_r1e8 =
  \ (@ t_aAv)
    (@ t1_aAx)
    (@ t2_aAy)
    (@ r_aAB)
    _
    (eta1_Xex :: Test.K t2_aAy t1_aAx) ->
    eta1_Xex

Test.logic3 :: GHC.Types.Char
[GblId,
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic3 = GHC.Types.C# 'x'

Test.logic2 :: [GHC.Types.Char]
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic2 =
  GHC.Types.:
    @ GHC.Types.Char Test.logic3 (GHC.Types.[] @ GHC.Types.Char)

Test.logic1 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic1 = Test.Var Test.logic2

Test.logic4 :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 2}]
Test.logic4 = Test.Not Test.T

Test.$dmconIsRecord
  :: forall c_ah9.
     (Test.Constructor c_ah9) =>
     forall (t_ahg :: * -> (* -> *) -> * -> *) (f_ahh :: * -> *) r_ahi.
     t_ahg c_ah9 f_ahh r_ahi -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_ah9)
                 _
                 (@ t_aC0::* -> (* -> *) -> * -> *)
                 (@ f_aC1::* -> *)
                 (@ r_aC2)
                 _ ->
                 GHC.Bool.False}]
Test.$dmconIsRecord =
  \ (@ c_ah9)
    _
    (@ t_aC0::* -> (* -> *) -> * -> *)
    (@ f_aC1::* -> *)
    (@ r_aC2)
    _ ->
    GHC.Bool.False

Test.$dmconFixity
  :: forall c_ah9.
     (Test.Constructor c_ah9) =>
     forall (t_ahd :: * -> (* -> *) -> * -> *) (f_ahe :: * -> *) r_ahf.
     t_ahd c_ah9 f_ahe r_ahf -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AA,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_ah9)
                 _
                 (@ t_aBS::* -> (* -> *) -> * -> *)
                 (@ f_aBT::* -> *)
                 (@ r_aBU)
                 _ ->
                 Test.Prefix}]
Test.$dmconFixity =
  \ (@ c_ah9)
    _
    (@ t_aBS::* -> (* -> *) -> * -> *)
    (@ f_aBT::* -> *)
    (@ r_aBU)
    _ ->
    Test.Prefix

Test.logic :: Test.Logic
[GblId,
 Caf=NoCafRefs,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 1 3}]
Test.logic = Test.Impl Test.logic4 Test.logic1

Test.toLogic [InlPrag=INLINE]
  :: forall t_av9 t1_avf t2_avj t3_avn t4_avs t5_avy t6_avE t7_avI.
     (Test.:+:)
       (Test.C t_av9 (Test.K GHC.Base.String))
       (Test.C t1_avf Test.U
        Test.:+: (Test.C t2_avj Test.U
                  Test.:+: (Test.C t3_avn Test.I
                            Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C t5_avy (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C t6_avE (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     t7_avI
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
     -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_av9)
                 (@ t1_avf)
                 (@ t2_avj)
                 (@ t3_avn)
                 (@ t4_avs)
                 (@ t5_avy)
                 (@ t6_avE)
                 (@ t7_avI)
                 (ds_dR3 [Occ=Once!]
                    :: (Test.:+:)
                         (Test.C t_av9 (Test.K GHC.Base.String))
                         (Test.C t1_avf Test.U
                          Test.:+: (Test.C t2_avj Test.U
                                    Test.:+: (Test.C t3_avn Test.I
                                              Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    t5_avy (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              t6_avE
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       t7_avI
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                         Test.Logic) ->
                 case ds_dR3 of _ {
                   Test.L ds1_dRw [Occ=Once!] ->
                     case ds1_dRw of _ { Test.C ds2_dRx [Occ=Once] ->
                     Test.Var
                       (ds2_dRx
                        `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                     };
                   Test.R ds1_dR4 [Occ=Once!] ->
                     case ds1_dR4 of _ {
                       Test.L ds2_dRu [Occ=Once!] ->
                         case ds2_dRu of _ { Test.C ds3_dRv [Occ=Once!] ->
                         case ds3_dRv of _ { Test.U -> Test.T }
                         };
                       Test.R ds2_dR5 [Occ=Once!] ->
                         case ds2_dR5 of _ {
                           Test.L ds3_dRs [Occ=Once!] ->
                             case ds3_dRs of _ { Test.C ds4_dRt [Occ=Once!] ->
                             case ds4_dRt of _ { Test.U -> Test.F }
                             };
                           Test.R ds3_dR6 [Occ=Once!] ->
                             case ds3_dR6 of _ {
                               Test.L ds4_dRq [Occ=Once!] ->
                                 case ds4_dRq of _ { Test.C ds5_dRr [Occ=Once] ->
                                 Test.Not
                                   (ds5_dRr
                                    `cast` (Test.NTCo:I Test.Logic
                                            :: Test.I Test.Logic ~ Test.Logic))
                                 };
                               Test.R ds4_dR7 [Occ=Once!] ->
                                 case ds4_dR7 of _ {
                                   Test.L ds5_dRm [Occ=Once!] ->
                                     case ds5_dRm of _ { Test.C ds6_dRn [Occ=Once!] ->
                                     case ds6_dRn
                                     of _ { Test.:*: ds7_dRo [Occ=Once] ds8_dRp [Occ=Once] ->
                                     Test.Impl
                                       (ds7_dRo
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                       (ds8_dRp
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     }
                                     };
                                   Test.R ds5_dR8 [Occ=Once!] ->
                                     case ds5_dR8 of _ {
                                       Test.L ds6_dRi [Occ=Once!] ->
                                         case ds6_dRi of _ { Test.C ds7_dRj [Occ=Once!] ->
                                         case ds7_dRj
                                         of _ { Test.:*: ds8_dRk [Occ=Once] ds9_dRl [Occ=Once] ->
                                         Test.Equiv
                                           (ds8_dRk
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds9_dRl
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds6_dR9 [Occ=Once!] ->
                                         case ds6_dR9 of _ {
                                           Test.L ds7_dRe [Occ=Once!] ->
                                             case ds7_dRe of _ { Test.C ds8_dRf [Occ=Once!] ->
                                             case ds8_dRf
                                             of _
                                             { Test.:*: ds9_dRg [Occ=Once] ds10_dRh [Occ=Once] ->
                                             Test.Conj
                                               (ds9_dRg
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dRh
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             };
                                           Test.R ds7_dRa [Occ=Once!] ->
                                             case ds7_dRa of _ { Test.C ds8_dRb [Occ=Once!] ->
                                             case ds8_dRb
                                             of _
                                             { Test.:*: ds9_dRc [Occ=Once] ds10_dRd [Occ=Once] ->
                                             Test.Disj
                                               (ds9_dRc
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds10_dRd
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }}]
Test.toLogic =
  \ (@ t_av9)
    (@ t1_avf)
    (@ t2_avj)
    (@ t3_avn)
    (@ t4_avs)
    (@ t5_avy)
    (@ t6_avE)
    (@ t7_avI)
    (eta_X74
       :: (Test.:+:)
            (Test.C t_av9 (Test.K GHC.Base.String))
            (Test.C t1_avf Test.U
             Test.:+: (Test.C t2_avj Test.U
                       Test.:+: (Test.C t3_avn Test.I
                                 Test.:+: (Test.C t4_avs (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C t5_avy (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 t6_avE (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          t7_avI
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
            Test.Logic) ->
    case eta_X74 of _ {
      Test.L ds_dRw ->
        case ds_dRw of _ { Test.C ds1_dRx ->
        Test.Var
          (ds1_dRx
           `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                   :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
        };
      Test.R ds_dR4 ->
        case ds_dR4 of _ {
          Test.L ds1_dRu ->
            case ds1_dRu of _ { Test.C ds2_dRv ->
            case ds2_dRv of _ { Test.U -> Test.T }
            };
          Test.R ds1_dR5 ->
            case ds1_dR5 of _ {
              Test.L ds2_dRs ->
                case ds2_dRs of _ { Test.C ds3_dRt ->
                case ds3_dRt of _ { Test.U -> Test.F }
                };
              Test.R ds2_dR6 ->
                case ds2_dR6 of _ {
                  Test.L ds3_dRq ->
                    case ds3_dRq of _ { Test.C ds4_dRr ->
                    Test.Not
                      (ds4_dRr
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    };
                  Test.R ds3_dR7 ->
                    case ds3_dR7 of _ {
                      Test.L ds4_dRm ->
                        case ds4_dRm of _ { Test.C ds5_dRn ->
                        case ds5_dRn of _ { Test.:*: ds6_dRo ds7_dRp ->
                        Test.Impl
                          (ds6_dRo
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds7_dRp
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds4_dR8 ->
                        case ds4_dR8 of _ {
                          Test.L ds5_dRi ->
                            case ds5_dRi of _ { Test.C ds6_dRj ->
                            case ds6_dRj of _ { Test.:*: ds7_dRk ds8_dRl ->
                            Test.Equiv
                              (ds7_dRk
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds8_dRl
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds5_dR9 ->
                            case ds5_dR9 of _ {
                              Test.L ds6_dRe ->
                                case ds6_dRe of _ { Test.C ds7_dRf ->
                                case ds7_dRf of _ { Test.:*: ds8_dRg ds9_dRh ->
                                Test.Conj
                                  (ds8_dRg
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRh
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                };
                              Test.R ds6_dRa ->
                                case ds6_dRa of _ { Test.C ds7_dRb ->
                                case ds7_dRb of _ { Test.:*: ds8_dRc ds9_dRd ->
                                Test.Disj
                                  (ds8_dRc
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRd
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

Test.fromLogic [InlPrag=INLINE]
  :: forall c_avX c1_aw7 c2_awj c3_awx c4_awQ c5_axc c6_axB c7_ay1.
     Test.Logic
     -> (Test.:+:)
          (Test.C c_avX (Test.K GHC.Base.String))
          (Test.C c1_aw7 Test.U
           Test.:+: (Test.C c2_awj Test.U
                     Test.:+: (Test.C c3_awx Test.I
                               Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        c7_ay1
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ c_avX)
                 (@ c1_aw7)
                 (@ c2_awj)
                 (@ c3_awx)
                 (@ c4_awQ)
                 (@ c5_axc)
                 (@ c6_axB)
                 (@ c7_ay1)
                 (ds_dS0 [Occ=Once!] :: Test.Logic) ->
                 case ds_dS0 of _ {
                   Test.Var f0_ahW [Occ=Once] ->
                     Test.L
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.C
                          @ c_avX
                          @ (Test.K GHC.Base.String)
                          @ Test.Logic
                          (f0_ahW
                           `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                                   :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
                   Test.T ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.L
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.C @ c1_aw7 @ Test.U @ Test.Logic (Test.U @ Test.Logic)));
                   Test.F ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.L
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.C @ c2_awj @ Test.U @ Test.Logic (Test.U @ Test.Logic))));
                   Test.Not f0_ahX [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.L
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.C
                                   @ c3_awx
                                   @ Test.I
                                   @ Test.Logic
                                   (f0_ahX
                                    `cast` (sym (Test.NTCo:I Test.Logic)
                                            :: Test.Logic ~ Test.I Test.Logic))))));
                   Test.Impl f0_ahY [Occ=Once] f1_ahZ [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.L
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.C
                                      @ c4_awQ
                                      @ (Test.I Test.:*: Test.I)
                                      @ Test.Logic
                                      (Test.:*:
                                         @ Test.I
                                         @ Test.I
                                         @ Test.Logic
                                         (f0_ahY
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))
                                         (f1_ahZ
                                          `cast` (sym (Test.NTCo:I Test.Logic)
                                                  :: Test.Logic ~ Test.I Test.Logic))))))));
                   Test.Equiv f0_ai0 [Occ=Once] f1_ai1 [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.L
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.C
                                         @ c5_axc
                                         @ (Test.I Test.:*: Test.I)
                                         @ Test.Logic
                                         (Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ Test.Logic
                                            (f0_ai0
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic))
                                            (f1_ai1
                                             `cast` (sym (Test.NTCo:I Test.Logic)
                                                     :: Test.Logic ~ Test.I Test.Logic)))))))));
                   Test.Conj f0_ai2 [Occ=Once] f1_ai3 [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.L
                                         @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c6_axB
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ai2
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ai3
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))));
                   Test.Disj f0_ai4 [Occ=Once] f1_ai5 [Occ=Once] ->
                     Test.R
                       @ (Test.C c_avX (Test.K GHC.Base.String))
                       @ (Test.C c1_aw7 Test.U
                          Test.:+: (Test.C c2_awj Test.U
                                    Test.:+: (Test.C c3_awx Test.I
                                              Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                        Test.:+: (Test.C
                                                                    c5_axc (Test.I Test.:*: Test.I)
                                                                  Test.:+: (Test.C
                                                                              c6_axB
                                                                              (Test.I
                                                                               Test.:*: Test.I)
                                                                            Test.:+: Test.C
                                                                                       c7_ay1
                                                                                       (Test.I
                                                                                        Test.:*: Test.I)))))))
                       @ Test.Logic
                       (Test.R
                          @ (Test.C c1_aw7 Test.U)
                          @ (Test.C c2_awj Test.U
                             Test.:+: (Test.C c3_awx Test.I
                                       Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                                 Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                           Test.:+: (Test.C
                                                                       c6_axB
                                                                       (Test.I Test.:*: Test.I)
                                                                     Test.:+: Test.C
                                                                                c7_ay1
                                                                                (Test.I
                                                                                 Test.:*: Test.I))))))
                          @ Test.Logic
                          (Test.R
                             @ (Test.C c2_awj Test.U)
                             @ (Test.C c3_awx Test.I
                                Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                          Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                    Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                              Test.:+: Test.C
                                                                         c7_ay1
                                                                         (Test.I
                                                                          Test.:*: Test.I)))))
                             @ Test.Logic
                             (Test.R
                                @ (Test.C c3_awx Test.I)
                                @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                   Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                             Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                       Test.:+: Test.C
                                                                  c7_ay1 (Test.I Test.:*: Test.I))))
                                @ Test.Logic
                                (Test.R
                                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                                   @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                                   @ Test.Logic
                                   (Test.R
                                      @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                                      @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                                         Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                      @ Test.Logic
                                      (Test.R
                                         @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                                         @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                                         @ Test.Logic
                                         (Test.C
                                            @ c7_ay1
                                            @ (Test.I Test.:*: Test.I)
                                            @ Test.Logic
                                            (Test.:*:
                                               @ Test.I
                                               @ Test.I
                                               @ Test.Logic
                                               (f0_ai4
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))
                                               (f1_ai5
                                                `cast` (sym (Test.NTCo:I Test.Logic)
                                                        :: Test.Logic ~ Test.I Test.Logic))))))))))
                 }}]
Test.fromLogic =
  \ (@ c_avX)
    (@ c1_aw7)
    (@ c2_awj)
    (@ c3_awx)
    (@ c4_awQ)
    (@ c5_axc)
    (@ c6_axB)
    (@ c7_ay1)
    (eta_X7c :: Test.Logic) ->
    case eta_X7c of _ {
      Test.Var f0_ahW ->
        Test.L
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.C
             @ c_avX
             @ (Test.K GHC.Base.String)
             @ Test.Logic
             (f0_ahW
              `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                      :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic)));
      Test.T ->
        lvl7_r1de
          @ c_avX
          @ c1_aw7
          @ c2_awj
          @ c3_awx
          @ c4_awQ
          @ c5_axc
          @ c6_axB
          @ c7_ay1;
      Test.F ->
        lvl4_r1d8
          @ c_avX
          @ c1_aw7
          @ c2_awj
          @ c3_awx
          @ c4_awQ
          @ c5_axc
          @ c6_axB
          @ c7_ay1;
      Test.Not f0_ahX ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.L
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.C
                      @ c3_awx
                      @ Test.I
                      @ Test.Logic
                      (f0_ahX
                       `cast` (sym (Test.NTCo:I Test.Logic)
                               :: Test.Logic ~ Test.I Test.Logic))))));
      Test.Impl f0_ahY f1_ahZ ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.L
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.C
                         @ c4_awQ
                         @ (Test.I Test.:*: Test.I)
                         @ Test.Logic
                         (Test.:*:
                            @ Test.I
                            @ Test.I
                            @ Test.Logic
                            (f0_ahY
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))
                            (f1_ahZ
                             `cast` (sym (Test.NTCo:I Test.Logic)
                                     :: Test.Logic ~ Test.I Test.Logic))))))));
      Test.Equiv f0_ai0 f1_ai1 ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.L
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.C
                            @ c5_axc
                            @ (Test.I Test.:*: Test.I)
                            @ Test.Logic
                            (Test.:*:
                               @ Test.I
                               @ Test.I
                               @ Test.Logic
                               (f0_ai0
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic))
                               (f1_ai1
                                `cast` (sym (Test.NTCo:I Test.Logic)
                                        :: Test.Logic ~ Test.I Test.Logic)))))))));
      Test.Conj f0_ai2 f1_ai3 ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.L
                            @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c6_axB
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ai2
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ai3
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))));
      Test.Disj f0_ai4 f1_ai5 ->
        Test.R
          @ (Test.C c_avX (Test.K GHC.Base.String))
          @ (Test.C c1_aw7 Test.U
             Test.:+: (Test.C c2_awj Test.U
                       Test.:+: (Test.C c3_awx Test.I
                                 Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 c6_axB (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          c7_ay1
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ Test.Logic
          (Test.R
             @ (Test.C c1_aw7 Test.U)
             @ (Test.C c2_awj Test.U
                Test.:+: (Test.C c3_awx Test.I
                          Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                                    Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                              Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                        Test.:+: Test.C
                                                                   c7_ay1
                                                                   (Test.I Test.:*: Test.I))))))
             @ Test.Logic
             (Test.R
                @ (Test.C c2_awj Test.U)
                @ (Test.C c3_awx Test.I
                   Test.:+: (Test.C c4_awQ (Test.I Test.:*: Test.I)
                             Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                       Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                                 Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))))
                @ Test.Logic
                (Test.R
                   @ (Test.C c3_awx Test.I)
                   @ (Test.C c4_awQ (Test.I Test.:*: Test.I)
                      Test.:+: (Test.C c5_axc (Test.I Test.:*: Test.I)
                                Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                          Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))))
                   @ Test.Logic
                   (Test.R
                      @ (Test.C c4_awQ (Test.I Test.:*: Test.I))
                      @ (Test.C c5_axc (Test.I Test.:*: Test.I)
                         Test.:+: (Test.C c6_axB (Test.I Test.:*: Test.I)
                                   Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I)))
                      @ Test.Logic
                      (Test.R
                         @ (Test.C c5_axc (Test.I Test.:*: Test.I))
                         @ (Test.C c6_axB (Test.I Test.:*: Test.I)
                            Test.:+: Test.C c7_ay1 (Test.I Test.:*: Test.I))
                         @ Test.Logic
                         (Test.R
                            @ (Test.C c6_axB (Test.I Test.:*: Test.I))
                            @ (Test.C c7_ay1 (Test.I Test.:*: Test.I))
                            @ Test.Logic
                            (Test.C
                               @ c7_ay1
                               @ (Test.I Test.:*: Test.I)
                               @ Test.Logic
                               (Test.:*:
                                  @ Test.I
                                  @ Test.I
                                  @ Test.Logic
                                  (f0_ai4
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))
                                  (f1_ai5
                                   `cast` (sym (Test.NTCo:I Test.Logic)
                                           :: Test.Logic ~ Test.I Test.Logic))))))))))
    }

Test.$fRegularLogic [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Regular Test.Logic
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Regular [((Test.fromLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
                                   :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                                        ~
                                      (Test.Logic -> Test.PF Test.Logic Test.Logic))),
                          ((Test.toLogic
                              @ Test.Logic_Var_
                              @ Test.Logic_T_
                              @ Test.Logic_F_
                              @ Test.Logic_Not_
                              @ Test.Logic_Impl_
                              @ Test.Logic_Equiv_
                              @ Test.Logic_Conj_
                              @ Test.Logic_Disj_)
                           `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
                                   :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                                        ~
                                      (Test.PF Test.Logic Test.Logic -> Test.Logic)))]]
Test.$fRegularLogic =
  Test.D:Regular
    @ Test.Logic
    ((Test.fromLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (Test.Logic -> sym Test.TFCo:R:PFLogic Test.Logic
             :: (Test.Logic -> Test.R:PFLogic Test.Logic)
                  ~
                (Test.Logic -> Test.PF Test.Logic Test.Logic)))
    ((Test.toLogic
        @ Test.Logic_Var_
        @ Test.Logic_T_
        @ Test.Logic_F_
        @ Test.Logic_Not_
        @ Test.Logic_Impl_
        @ Test.Logic_Equiv_
        @ Test.Logic_Conj_
        @ Test.Logic_Disj_)
     `cast` (sym Test.TFCo:R:PFLogic Test.Logic -> Test.Logic
             :: (Test.R:PFLogic Test.Logic -> Test.Logic)
                  ~
                (Test.PF Test.Logic Test.Logic -> Test.Logic)))

Test.updateStringfC [InlPrag=INLINE]
  :: forall t_azw (t1_azz :: * -> *) r_azE c_azG.
     (Test.UpdateString t1_azz) =>
     (r_azE -> r_azE)
     -> Test.C t_azw t1_azz r_azE
     -> Test.C c_azG t1_azz r_azE
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azw)
                 (@ t1_azz::* -> *)
                 (@ r_azE)
                 (@ c_azG)
                 ($dUpdateString_azI [Occ=Once] :: Test.UpdateString t1_azz)
                 (f_ahU [Occ=Once] :: r_azE -> r_azE)
                 (ds_dS9 [Occ=Once!] :: Test.C t_azw t1_azz r_azE) ->
                 case ds_dS9 of _ { Test.C x_ahV [Occ=Once] ->
                 Test.C
                   @ c_azG
                   @ t1_azz
                   @ r_azE
                   (($dUpdateString_azI
                     `cast` (Test.NTCo:T:UpdateString t1_azz
                             :: Test.T:UpdateString t1_azz
                                  ~
                                (forall a_ah6. (a_ah6 -> a_ah6) -> t1_azz a_ah6 -> t1_azz a_ah6)))
                      @ r_azE f_ahU x_ahV)
                 }}]
Test.updateStringfC =
  \ (@ t_azw)
    (@ t1_azz::* -> *)
    (@ r_azE)
    (@ c_azG)
    ($dUpdateString_azI :: Test.UpdateString t1_azz)
    (eta_X4w :: r_azE -> r_azE)
    (eta1_X91 :: Test.C t_azw t1_azz r_azE) ->
    case eta1_X91 of _ { Test.C x_ahV ->
    Test.C
      @ c_azG
      @ t1_azz
      @ r_azE
      (($dUpdateString_azI
        `cast` (Test.NTCo:T:UpdateString t1_azz
                :: Test.T:UpdateString t1_azz
                     ~
                   (forall a_ah6. (a_ah6 -> a_ah6) -> t1_azz a_ah6 -> t1_azz a_ah6)))
         @ r_azE eta_X4w x_ahV)
    }

Test.$fUpdateStringC1
  :: forall (f_aig :: * -> *) c_aih.
     (Test.UpdateString f_aig) =>
     forall a_aPD.
     (a_aPD -> a_aPD)
     -> Test.C c_aih f_aig a_aPD
     -> Test.C c_aih f_aig a_aPD
[GblId,
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_aig::* -> *)
                 (@ c_aih)
                 ($dUpdateString_aPy [Occ=Once] :: Test.UpdateString f_aig)
                 (@ a_aPD)
                 (eta_B2 [Occ=Once] :: a_aPD -> a_aPD)
                 (eta1_B1 [Occ=Once] :: Test.C c_aih f_aig a_aPD) ->
                 Test.updateStringfC
                   @ c_aih @ f_aig @ a_aPD @ c_aih $dUpdateString_aPy eta_B2 eta1_B1}]
Test.$fUpdateStringC1 =
  \ (@ f_aig::* -> *)
    (@ c_aih)
    ($dUpdateString_aPy :: Test.UpdateString f_aig)
    (@ a_aPD)
    (eta_B2 :: a_aPD -> a_aPD)
    (eta1_B1 :: Test.C c_aih f_aig a_aPD) ->
    Test.updateStringfC
      @ c_aih @ f_aig @ a_aPD @ c_aih $dUpdateString_aPy eta_B2 eta1_B1

Test.$fUpdateStringC
  :: forall (f_aig :: * -> *) c_aih.
     (Test.UpdateString f_aig) =>
     Test.UpdateString (Test.C c_aih f_aig)
[GblId[DFunId(newtype)],
 Arity=3,
 Caf=NoCafRefs,
 Str=DmdType LLU(L)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateStringC1
               `cast` (forall (f_aig :: * -> *) c_aih.
                       (Test.UpdateString f_aig) =>
                       sym (Test.NTCo:T:UpdateString (Test.C c_aih f_aig))
                       :: (forall (f_aig :: * -> *) c_aih.
                           (Test.UpdateString f_aig) =>
                           forall a_ah6.
                           (a_ah6 -> a_ah6)
                           -> Test.C c_aih f_aig a_ah6
                           -> Test.C c_aih f_aig a_ah6)
                            ~
                          (forall (f_aig :: * -> *) c_aih.
                           (Test.UpdateString f_aig) =>
                           Test.T:UpdateString (Test.C c_aih f_aig)))}]
Test.$fUpdateStringC =
  Test.$fUpdateStringC1
  `cast` (forall (f_aig :: * -> *) c_aih.
          (Test.UpdateString f_aig) =>
          sym (Test.NTCo:T:UpdateString (Test.C c_aih f_aig))
          :: (forall (f_aig :: * -> *) c_aih.
              (Test.UpdateString f_aig) =>
              forall a_ah6.
              (a_ah6 -> a_ah6)
              -> Test.C c_aih f_aig a_ah6
              -> Test.C c_aih f_aig a_ah6)
               ~
             (forall (f_aig :: * -> *) c_aih.
              (Test.UpdateString f_aig) =>
              Test.T:UpdateString (Test.C c_aih f_aig)))

Test.updateStringfTimes [InlPrag=INLINE]
  :: forall (t_azR :: * -> *) (t1_azT :: * -> *) r_azX.
     (Test.UpdateString t_azR, Test.UpdateString t1_azT) =>
     (r_azX -> r_azX)
     -> (Test.:*:) t_azR t1_azT r_azX
     -> (Test.:*:) t_azR t1_azT r_azX
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_azR::* -> *)
                 (@ t1_azT::* -> *)
                 (@ r_azX)
                 ($dUpdateString_aA1 [Occ=Once] :: Test.UpdateString t_azR)
                 ($dUpdateString1_aA2 [Occ=Once] :: Test.UpdateString t1_azT)
                 (f_ahR :: r_azX -> r_azX)
                 (ds_dSb [Occ=Once!] :: (Test.:*:) t_azR t1_azT r_azX) ->
                 case ds_dSb of _ { Test.:*: x_ahS [Occ=Once] y_ahT [Occ=Once] ->
                 Test.:*:
                   @ t_azR
                   @ t1_azT
                   @ r_azX
                   (($dUpdateString_aA1
                     `cast` (Test.NTCo:T:UpdateString t_azR
                             :: Test.T:UpdateString t_azR
                                  ~
                                (forall a_ah6. (a_ah6 -> a_ah6) -> t_azR a_ah6 -> t_azR a_ah6)))
                      @ r_azX f_ahR x_ahS)
                   (($dUpdateString1_aA2
                     `cast` (Test.NTCo:T:UpdateString t1_azT
                             :: Test.T:UpdateString t1_azT
                                  ~
                                (forall a_ah6. (a_ah6 -> a_ah6) -> t1_azT a_ah6 -> t1_azT a_ah6)))
                      @ r_azX f_ahR y_ahT)
                 }}]
Test.updateStringfTimes =
  \ (@ t_azR::* -> *)
    (@ t1_azT::* -> *)
    (@ r_azX)
    ($dUpdateString_aA1 :: Test.UpdateString t_azR)
    ($dUpdateString1_aA2 :: Test.UpdateString t1_azT)
    (eta_B2 :: r_azX -> r_azX)
    (eta1_B1 :: (Test.:*:) t_azR t1_azT r_azX) ->
    case eta1_B1 of _ { Test.:*: x_ahS y_ahT ->
    Test.:*:
      @ t_azR
      @ t1_azT
      @ r_azX
      (($dUpdateString_aA1
        `cast` (Test.NTCo:T:UpdateString t_azR
                :: Test.T:UpdateString t_azR
                     ~
                   (forall a_ah6. (a_ah6 -> a_ah6) -> t_azR a_ah6 -> t_azR a_ah6)))
         @ r_azX eta_B2 x_ahS)
      (($dUpdateString1_aA2
        `cast` (Test.NTCo:T:UpdateString t1_azT
                :: Test.T:UpdateString t1_azT
                     ~
                   (forall a_ah6. (a_ah6 -> a_ah6) -> t1_azT a_ah6 -> t1_azT a_ah6)))
         @ r_azX eta_B2 y_ahT)
    }

Test.$fUpdateString:*:1
  :: forall (f_aii :: * -> *) (g_aij :: * -> *).
     (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
     forall a_aPN.
     (a_aPN -> a_aPN)
     -> (Test.:*:) f_aii g_aij a_aPN
     -> (Test.:*:) f_aii g_aij a_aPN
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ f_aii::* -> *)
                 (@ g_aij::* -> *)
                 ($dUpdateString_aPH [Occ=Once] :: Test.UpdateString f_aii)
                 ($dUpdateString1_aPI [Occ=Once] :: Test.UpdateString g_aij)
                 (@ a_aPN)
                 (eta_B2 [Occ=Once] :: a_aPN -> a_aPN)
                 (eta1_B1 [Occ=Once] :: (Test.:*:) f_aii g_aij a_aPN) ->
                 Test.updateStringfTimes
                   @ f_aii
                   @ g_aij
                   @ a_aPN
                   $dUpdateString_aPH
                   $dUpdateString1_aPI
                   eta_B2
                   eta1_B1}]
Test.$fUpdateString:*:1 =
  \ (@ f_aii::* -> *)
    (@ g_aij::* -> *)
    ($dUpdateString_aPH :: Test.UpdateString f_aii)
    ($dUpdateString1_aPI :: Test.UpdateString g_aij)
    (@ a_aPN)
    (eta_B2 :: a_aPN -> a_aPN)
    (eta1_B1 :: (Test.:*:) f_aii g_aij a_aPN) ->
    Test.updateStringfTimes
      @ f_aii
      @ g_aij
      @ a_aPN
      $dUpdateString_aPH
      $dUpdateString1_aPI
      eta_B2
      eta1_B1

Test.$fUpdateString:*:
  :: forall (f_aii :: * -> *) (g_aij :: * -> *).
     (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
     Test.UpdateString (f_aii Test.:*: g_aij)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLU(LL)m,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= Test.$fUpdateString:*:1
               `cast` (forall (f_aii :: * -> *) (g_aij :: * -> *).
                       (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
                       sym (Test.NTCo:T:UpdateString (f_aii Test.:*: g_aij))
                       :: (forall (f_aii :: * -> *) (g_aij :: * -> *).
                           (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
                           forall a_ah6.
                           (a_ah6 -> a_ah6)
                           -> (Test.:*:) f_aii g_aij a_ah6
                           -> (Test.:*:) f_aii g_aij a_ah6)
                            ~
                          (forall (f_aii :: * -> *) (g_aij :: * -> *).
                           (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
                           Test.T:UpdateString (f_aii Test.:*: g_aij)))}]
Test.$fUpdateString:*: =
  Test.$fUpdateString:*:1
  `cast` (forall (f_aii :: * -> *) (g_aij :: * -> *).
          (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
          sym (Test.NTCo:T:UpdateString (f_aii Test.:*: g_aij))
          :: (forall (f_aii :: * -> *) (g_aij :: * -> *).
              (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
              forall a_ah6.
              (a_ah6 -> a_ah6)
              -> (Test.:*:) f_aii g_aij a_ah6
              -> (Test.:*:) f_aii g_aij a_ah6)
               ~
             (forall (f_aii :: * -> *) (g_aij :: * -> *).
              (Test.UpdateString f_aii, Test.UpdateString g_aij) =>
              Test.T:UpdateString (f_aii Test.:*: g_aij)))

Test.updateStringfPlus [InlPrag=INLINE]
  :: forall (t_aAb :: * -> *) r_aAg (g_aAi :: * -> *).
     (Test.UpdateString t_aAb, Test.UpdateString g_aAi) =>
     (r_aAg -> r_aAg)
     -> (Test.:+:) t_aAb g_aAi r_aAg
     -> (Test.:+:) t_aAb g_aAi r_aAg
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAb::* -> *)
                 (@ r_aAg)
                 (@ g_aAi::* -> *)
                 ($dUpdateString_aAp [Occ=Once] :: Test.UpdateString t_aAb)
                 ($dUpdateString1_aAq [Occ=Once] :: Test.UpdateString g_aAi)
                 (f_ahN [Occ=Once*] :: r_aAg -> r_aAg)
                 (ds_dSd [Occ=Once!] :: (Test.:+:) t_aAb g_aAi r_aAg) ->
                 case ds_dSd of _ {
                   Test.L x_ahO [Occ=Once] ->
                     Test.L
                       @ t_aAb
                       @ g_aAi
                       @ r_aAg
                       (($dUpdateString_aAp
                         `cast` (Test.NTCo:T:UpdateString t_aAb
                                 :: Test.T:UpdateString t_aAb
                                      ~
                                    (forall a_ah6. (a_ah6 -> a_ah6) -> t_aAb a_ah6 -> t_aAb a_ah6)))
                          @ r_aAg f_ahN x_ahO);
                   Test.R x_ahQ [Occ=Once] ->
                     Test.R
                       @ t_aAb
                       @ g_aAi
                       @ r_aAg
                       (($dUpdateString1_aAq
                         `cast` (Test.NTCo:T:UpdateString g_aAi
                                 :: Test.T:UpdateString g_aAi
                                      ~
                                    (forall a_ah6. (a_ah6 -> a_ah6) -> g_aAi a_ah6 -> g_aAi a_ah6)))
                          @ r_aAg f_ahN x_ahQ)
                 }}]
Test.updateStringfPlus =
  \ (@ t_aAb::* -> *)
    (@ r_aAg)
    (@ g_aAi::* -> *)
    ($dUpdateString_aAp :: Test.UpdateString t_aAb)
    ($dUpdateString1_aAq :: Test.UpdateString g_aAi)
    (eta_B2 :: r_aAg -> r_aAg)
    (eta1_B1 :: (Test.:+:) t_aAb g_aAi r_aAg) ->
    case eta1_B1 of _ {
      Test.L x_ahO ->
        Test.L
          @ t_aAb
          @ g_aAi
          @ r_aAg
          (($dUpdateString_aAp
            `cast` (Test.NTCo:T:UpdateString t_aAb
                    :: Test.T:UpdateString t_aAb
                         ~
                       (forall a_ah6. (a_ah6 -> a_ah6) -> t_aAb a_ah6 -> t_aAb a_ah6)))
             @ r_aAg eta_B2 x_ahO);
      Test.R x_ahQ ->
        Test.R
          @ t_aAb
          @ g_aAi
          @ r_aAg
          (($dUpdateString1_aAq
            `cast` (Test.NTCo:T:UpdateString g_aAi
                    :: Test.T:UpdateString g_aAi
                         ~
                       (forall a_ah6. (a_ah6 -> a_ah6) -> g_aAi a_ah6 -> g_aAi a_ah6)))
             @ r_aAg eta_B2 x_ahQ)
    }

Test.$fUpdateString:+:1
  :: forall (f_aik :: * -> *) (g_ail :: * -> *).
     (Test.UpdateString f_aik, Test.UpdateString g_ail) =>
     forall a_aPY.
     (a_aPY -> a_aPY)
     -> (Test.:+:) f_aik g_ail a_aPY
     -> (Test.:+:) f_aik g_ail a_aPY
[GblId,
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=4, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0 0 0] 5 0}]
Test.$fUpdateString:+:1 =
  \ (@ f_aik::* -> *)
    (@ g_ail::* -> *)
    ($dUpdateString_aPS :: Test.UpdateString f_aik)
    ($dUpdateString1_aPT :: Test.UpdateString g_ail)
    (@ a_aPY)
    (eta_B2 :: a_aPY -> a_aPY)
    (eta1_B1 :: (Test.:+:) f_aik g_ail a_aPY) ->
    Test.updateStringfPlus
      @ f_aik
      @ a_aPY
      @ g_ail
      $dUpdateString_aPS
      $dUpdateString1_aPT
      eta_B2
      eta1_B1

Test.$fUpdateString:+:
  :: forall (f_aik :: * -> *) (g_ail :: * -> *).
     (Test.UpdateString f_aik, Test.UpdateString g_ail) =>
     Test.UpdateString (f_aik Test.:+: g_ail)
[GblId[DFunId(newtype)],
 Arity=4,
 Caf=NoCafRefs,
 Str=DmdType LLLS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateString:+: =
  Test.$fUpdateString:+:1
  `cast` (forall (f_aik :: * -> *) (g_ail :: * -> *).
          (Test.UpdateString f_aik, Test.UpdateString g_ail) =>
          sym (Test.NTCo:T:UpdateString (f_aik Test.:+: g_ail))
          :: (forall (f_aik :: * -> *) (g_ail :: * -> *).
              (Test.UpdateString f_aik, Test.UpdateString g_ail) =>
              forall a_ah6.
              (a_ah6 -> a_ah6)
              -> (Test.:+:) f_aik g_ail a_ah6
              -> (Test.:+:) f_aik g_ail a_ah6)
               ~
             (forall (f_aik :: * -> *) (g_ail :: * -> *).
              (Test.UpdateString f_aik, Test.UpdateString g_ail) =>
              Test.T:UpdateString (f_aik Test.:+: g_ail)))

Test.updateStringfK [InlPrag=INLINE]
  :: forall t_aAv t1_aAx t2_aAy r_aAB.
     t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy r_aAB
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= (\ (@ t_XE8)
                  (@ t1_XEb)
                  (@ t2_XEd)
                  (@ r_XEh)
                  _
                  (ds_dSg [Occ=Once] :: Test.K t2_XEd t1_XEb) ->
                  ds_dSg)
               `cast` (forall t_XE8 t1_XEb t2_XEd r_XEh.
                       t_XE8
                       -> Test.K t2_XEd t1_XEb
                       -> trans
                            (Test.NTCo:K t2_XEd t1_XEb) (sym (Test.NTCo:K t2_XEd r_XEh))
                       :: (forall t_XE8 t1_XEb t2_XEd r_XEh.
                           t_XE8 -> Test.K t2_XEd t1_XEb -> Test.K t2_XEd t1_XEb)
                            ~
                          (forall t_XE8 t1_XEb t2_XEd r_XEh.
                           t_XE8 -> Test.K t2_XEd t1_XEb -> Test.K t2_XEd r_XEh))}]
Test.updateStringfK =
  a1_r1e8
  `cast` (forall t_aAv t1_aAx t2_aAy r_aAB.
          t_aAv
          -> Test.K t2_aAy t1_aAx
          -> trans
               (Test.NTCo:K t2_aAy t1_aAx) (sym (Test.NTCo:K t2_aAy r_aAB))
          :: (forall t_aAv t1_aAx t2_aAy r_aAB.
              t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy t1_aAx)
               ~
             (forall t_aAv t1_aAx t2_aAy r_aAB.
              t_aAv -> Test.K t2_aAy t1_aAx -> Test.K t2_aAy r_aAB))

Test.$fUpdateStringK
  :: forall x_aim. Test.UpdateString (Test.K x_aim)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ x_aim) (@ a_aQ7) ->
                  Test.updateStringfK @ (a_aQ7 -> a_aQ7) @ a_aQ7 @ x_aim @ a_aQ7)
               `cast` (forall x_aim. sym (Test.NTCo:T:UpdateString (Test.K x_aim))
                       :: (forall x_aim a_ah6.
                           (a_ah6 -> a_ah6) -> Test.K x_aim a_ah6 -> Test.K x_aim a_ah6)
                            ~
                          (forall x_aim. Test.T:UpdateString (Test.K x_aim)))}]
Test.$fUpdateStringK =
  (\ (@ x_aim) (@ a_aQ7) ->
     Test.updateStringfK @ (a_aQ7 -> a_aQ7) @ a_aQ7 @ x_aim @ a_aQ7)
  `cast` (forall x_aim. sym (Test.NTCo:T:UpdateString (Test.K x_aim))
          :: (forall x_aim a_ah6.
              (a_ah6 -> a_ah6) -> Test.K x_aim a_ah6 -> Test.K x_aim a_ah6)
               ~
             (forall x_aim. Test.T:UpdateString (Test.K x_aim)))

Test.updateStringfKString [InlPrag=INLINE]
  :: forall t_aAH t1_aAJ r_aAM.
     t_aAH
     -> Test.K [GHC.Types.Char] t1_aAJ
     -> Test.K [GHC.Types.Char] r_aAM
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ t_aAH)
                 (@ t1_aAJ)
                 (@ r_aAM)
                 _
                 (ds_dSh [Occ=Once] :: Test.K [GHC.Types.Char] t1_aAJ) ->
                 case ds_dSh
                      `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAJ
                              :: Test.K [GHC.Types.Char] t1_aAJ ~ [GHC.Types.Char])
                 of _ {
                   [] ->
                     (GHC.Types.[] @ GHC.Types.Char)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM);
                   : ipv_sUo [Occ=Once] ipv1_sUp ->
                     (GHC.Types.:
                        @ GHC.Types.Char
                        (GHC.List.last_last' @ GHC.Types.Char ipv_sUo ipv1_sUp)
                        ipv1_sUp)
                     `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                             :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM)
                 }}]
Test.updateStringfKString =
  \ (@ t_aAH)
    (@ t1_aAJ)
    (@ r_aAM)
    _
    (eta1_X7r :: Test.K [GHC.Types.Char] t1_aAJ) ->
    case eta1_X7r
         `cast` (Test.NTCo:K [GHC.Types.Char] t1_aAJ
                 :: Test.K [GHC.Types.Char] t1_aAJ ~ [GHC.Types.Char])
    of _ {
      [] ->
        (GHC.Types.[] @ GHC.Types.Char)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM);
      : ipv_sU1 ipv1_sU2 ->
        (GHC.Types.:
           @ GHC.Types.Char
           (GHC.List.last_last' @ GHC.Types.Char ipv_sU1 ipv1_sU2)
           ipv1_sU2)
        `cast` (sym (Test.NTCo:K [GHC.Types.Char] r_aAM)
                :: [GHC.Types.Char] ~ Test.K [GHC.Types.Char] r_aAM)
    }

Test.$fUpdateStringK0 :: Test.UpdateString (Test.K GHC.Base.String)
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQd) ->
                  Test.updateStringfKString @ (a_aQd -> a_aQd) @ a_aQd @ a_aQd)
               `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
                       :: (forall a_ah6.
                           (a_ah6 -> a_ah6)
                           -> Test.K GHC.Base.String a_ah6
                           -> Test.K GHC.Base.String a_ah6)
                            ~
                          Test.T:UpdateString (Test.K GHC.Base.String))}]
Test.$fUpdateStringK0 =
  (\ (@ a_aQd) ->
     Test.updateStringfKString @ (a_aQd -> a_aQd) @ a_aQd @ a_aQd)
  `cast` (sym (Test.NTCo:T:UpdateString (Test.K GHC.Base.String))
          :: (forall a_ah6.
              (a_ah6 -> a_ah6)
              -> Test.K GHC.Base.String a_ah6
              -> Test.K GHC.Base.String a_ah6)
               ~
             Test.T:UpdateString (Test.K GHC.Base.String))

Test.updateStringfI [InlPrag=INLINE]
  :: forall t_aBz r_aBC.
     (t_aBz -> r_aBC) -> Test.I t_aBz -> Test.I r_aBC
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= (\ (@ t_XFj)
                  (@ r_XFn)
                  (f_ahx [Occ=Once!] :: t_XFj -> r_XFn)
                  (ds_dSm [Occ=Once] :: Test.I t_XFj) ->
                  f_ahx (ds_dSm `cast` (Test.NTCo:I t_XFj :: Test.I t_XFj ~ t_XFj)))
               `cast` (forall t_XFj r_XFn.
                       (t_XFj -> r_XFn) -> Test.I t_XFj -> sym (Test.NTCo:I r_XFn)
                       :: (forall t_XFj r_XFn. (t_XFj -> r_XFn) -> Test.I t_XFj -> r_XFn)
                            ~
                          (forall t_XFj r_XFn.
                           (t_XFj -> r_XFn) -> Test.I t_XFj -> Test.I r_XFn))}]
Test.updateStringfI =
  a_r1e6
  `cast` (forall t_aBz r_aBC.
          (t_aBz -> r_aBC) -> Test.I t_aBz -> sym (Test.NTCo:I r_aBC)
          :: (forall t_aBz r_aBC. (t_aBz -> r_aBC) -> Test.I t_aBz -> r_aBC)
               ~
             (forall t_aBz r_aBC.
              (t_aBz -> r_aBC) -> Test.I t_aBz -> Test.I r_aBC))

Test.$fUpdateStringI :: Test.UpdateString Test.I
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType C(S)L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.$fUpdateStringI =
  (\ (@ a_aQj) -> Test.updateStringfI @ a_aQj @ a_aQj)
  `cast` (sym (Test.NTCo:T:UpdateString Test.I)
          :: (forall a_ah6. (a_ah6 -> a_ah6) -> Test.I a_ah6 -> Test.I a_ah6)
               ~
             Test.T:UpdateString Test.I)

Test.updateString1
  :: forall a_XVG.
     (a_XVG -> a_XVG)
     -> (Test.:+:)
          (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          (Test.C Test.Logic_T_ Test.U
           Test.:+: (Test.C Test.Logic_F_ Test.U
                     Test.:+: (Test.C Test.Logic_Not_ Test.I
                               Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Conj_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        Test.Logic_Disj_
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          a_XVG
     -> (Test.:+:)
          (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          (Test.C Test.Logic_T_ Test.U
           Test.:+: (Test.C Test.Logic_F_ Test.U
                     Test.:+: (Test.C Test.Logic_Not_ Test.I
                               Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Conj_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: Test.C
                                                                        Test.Logic_Disj_
                                                                        (Test.I
                                                                         Test.:*: Test.I)))))))
          a_XVG
[GblId, Arity=2, Caf=NoCafRefs, Str=DmdType LS]
Test.updateString1 =
  \ (@ a_XVG)
    (eta_X5L :: a_XVG -> a_XVG)
    (eta1_Xbv
       :: (Test.:+:)
            (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
            (Test.C Test.Logic_T_ Test.U
             Test.:+: (Test.C Test.Logic_F_ Test.U
                       Test.:+: (Test.C Test.Logic_Not_ Test.I
                                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 Test.Logic_Conj_
                                                                 (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          Test.Logic_Disj_
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
            a_XVG) ->
    case eta1_Xbv of _ {
      Test.L x_ahO ->
        Test.L
          @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          @ (Test.C Test.Logic_T_ Test.U
             Test.:+: (Test.C Test.Logic_F_ Test.U
                       Test.:+: (Test.C Test.Logic_Not_ Test.I
                                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 Test.Logic_Conj_
                                                                 (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          Test.Logic_Disj_
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ a_XVG
          (case x_ahO of _ { Test.C x1_ahV ->
           Test.C
             @ Test.Logic_Var_
             @ (Test.K GHC.Base.String)
             @ a_XVG
             (Test.updateStringfKString
                @ (a_XVG -> a_XVG) @ a_XVG @ a_XVG eta_X5L x1_ahV)
           });
      Test.R x_ahQ ->
        Test.R
          @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
          @ (Test.C Test.Logic_T_ Test.U
             Test.:+: (Test.C Test.Logic_F_ Test.U
                       Test.:+: (Test.C Test.Logic_Not_ Test.I
                                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                     Test.:+: (Test.C
                                                                 Test.Logic_Conj_
                                                                 (Test.I Test.:*: Test.I)
                                                               Test.:+: Test.C
                                                                          Test.Logic_Disj_
                                                                          (Test.I
                                                                           Test.:*: Test.I)))))))
          @ a_XVG
          (case x_ahQ of wild1_X5H {
             Test.L x1_ahO -> wild1_X5H;
             Test.R x1_XnG ->
               Test.R
                 @ (Test.C Test.Logic_T_ Test.U)
                 @ (Test.C Test.Logic_F_ Test.U
                    Test.:+: (Test.C Test.Logic_Not_ Test.I
                              Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                  Test.:+: (Test.C
                                                              Test.Logic_Conj_
                                                              (Test.I Test.:*: Test.I)
                                                            Test.:+: Test.C
                                                                       Test.Logic_Disj_
                                                                       (Test.I Test.:*: Test.I))))))
                 @ a_XVG
                 (case x1_XnG of wild2_X5B {
                    Test.L x2_ahO -> wild2_X5B;
                    Test.R x2_XnA ->
                      Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ a_XVG
                        (case x2_XnA of _ {
                           Test.L x3_ahO ->
                             Test.L
                               @ (Test.C Test.Logic_Not_ Test.I)
                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                  Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                      Test.:+: Test.C
                                                                 Test.Logic_Disj_
                                                                 (Test.I Test.:*: Test.I))))
                               @ a_XVG
                               (case x3_ahO of _ { Test.C x4_ahV ->
                                Test.C
                                  @ Test.Logic_Not_
                                  @ Test.I
                                  @ a_XVG
                                  (Test.updateStringfI @ a_XVG @ a_XVG eta_X5L x4_ahV)
                                });
                           Test.R x3_Xlj ->
                             Test.R
                               @ (Test.C Test.Logic_Not_ Test.I)
                               @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                  Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                      Test.:+: Test.C
                                                                 Test.Logic_Disj_
                                                                 (Test.I Test.:*: Test.I))))
                               @ a_XVG
                               (case x3_Xlj of _ {
                                  Test.L x4_ahO ->
                                    Test.L
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ a_XVG
                                      (case x4_ahO of _ { Test.C x5_ahV ->
                                       Test.C
                                         @ Test.Logic_Impl_
                                         @ (Test.I Test.:*: Test.I)
                                         @ a_XVG
                                         (case x5_ahV of _ { Test.:*: x6_ahS y_ahT ->
                                          Test.:*:
                                            @ Test.I
                                            @ Test.I
                                            @ a_XVG
                                            (Test.updateStringfI @ a_XVG @ a_XVG eta_X5L x6_ahS)
                                            (Test.updateStringfI @ a_XVG @ a_XVG eta_X5L y_ahT)
                                          })
                                       });
                                  Test.R x4_Xnp ->
                                    Test.R
                                      @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                                      @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                         Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                   Test.:+: Test.C
                                                              Test.Logic_Disj_
                                                              (Test.I Test.:*: Test.I)))
                                      @ a_XVG
                                      (case x4_Xnp of _ {
                                         Test.L x5_ahO ->
                                           Test.L
                                             @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                             @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))
                                             @ a_XVG
                                             (case x5_ahO of _ { Test.C x6_ahV ->
                                              Test.C
                                                @ Test.Logic_Equiv_
                                                @ (Test.I Test.:*: Test.I)
                                                @ a_XVG
                                                (case x6_ahV of _ { Test.:*: x7_ahS y_ahT ->
                                                 Test.:*:
                                                   @ Test.I
                                                   @ Test.I
                                                   @ a_XVG
                                                   (Test.updateStringfI
                                                      @ a_XVG @ a_XVG eta_X5L x7_ahS)
                                                   (Test.updateStringfI
                                                      @ a_XVG @ a_XVG eta_X5L y_ahT)
                                                 })
                                              });
                                         Test.R x5_Xlq ->
                                           Test.R
                                             @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                             @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))
                                             @ a_XVG
                                             (case x5_Xlq of _ {
                                                Test.L x6_ahO ->
                                                  Test.L
                                                    @ (Test.C
                                                         Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                                    @ (Test.C
                                                         Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                                    @ a_XVG
                                                    (case x6_ahO of _ { Test.C x7_ahV ->
                                                     Test.C
                                                       @ Test.Logic_Conj_
                                                       @ (Test.I Test.:*: Test.I)
                                                       @ a_XVG
                                                       (case x7_ahV of _ { Test.:*: x8_ahS y_ahT ->
                                                        Test.:*:
                                                          @ Test.I
                                                          @ Test.I
                                                          @ a_XVG
                                                          (Test.updateStringfI
                                                             @ a_XVG @ a_XVG eta_X5L x8_ahS)
                                                          (Test.updateStringfI
                                                             @ a_XVG @ a_XVG eta_X5L y_ahT)
                                                        })
                                                     });
                                                Test.R x6_Xnd ->
                                                  Test.R
                                                    @ (Test.C
                                                         Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                                    @ (Test.C
                                                         Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                                    @ a_XVG
                                                    (case x6_Xnd of _ { Test.C x7_ahV ->
                                                     Test.C
                                                       @ Test.Logic_Disj_
                                                       @ (Test.I Test.:*: Test.I)
                                                       @ a_XVG
                                                       (case x7_ahV of _ { Test.:*: x8_ahS y_ahT ->
                                                        Test.:*:
                                                          @ Test.I
                                                          @ Test.I
                                                          @ a_XVG
                                                          (Test.updateStringfI
                                                             @ a_XVG @ a_XVG eta_X5L x8_ahS)
                                                          (Test.updateStringfI
                                                             @ a_XVG @ a_XVG eta_X5L y_ahT)
                                                        })
                                                     })
                                              })
                                       })
                                })
                         })
                  })
           })
    }

lvl11_r1ei :: Test.C Test.Logic_F_ Test.U Test.Logic
[GblId, Caf=NoCafRefs]
lvl11_r1ei =
  Test.C @ Test.Logic_F_ @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl12_r1ek
  :: (Test.:+:)
       (Test.C Test.Logic_F_ Test.U)
       (Test.C Test.Logic_Not_ Test.I
        Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                  Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                      Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl12_r1ek =
  Test.L
    @ (Test.C Test.Logic_F_ Test.U)
    @ (Test.C Test.Logic_Not_ Test.I
       Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                 Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                           Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                     Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I)))))
    @ Test.Logic
    lvl11_r1ei

lvl13_r1em
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl13_r1em =
  Test.R
    @ (Test.C Test.Logic_T_ Test.U)
    @ (Test.C Test.Logic_F_ Test.U
       Test.:+: (Test.C Test.Logic_Not_ Test.I
                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                           Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                               Test.:+: Test.C
                                                          Test.Logic_Disj_
                                                          (Test.I Test.:*: Test.I))))))
    @ Test.Logic
    lvl12_r1ek

lvl14_r1eo
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl14_r1eo =
  Test.R
    @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
    @ (Test.C Test.Logic_T_ Test.U
       Test.:+: (Test.C Test.Logic_F_ Test.U
                 Test.:+: (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))))
    @ Test.Logic
    lvl13_r1em

lvl15_r1eq :: Test.C Test.Logic_T_ Test.U Test.Logic
[GblId, Caf=NoCafRefs]
lvl15_r1eq =
  Test.C @ Test.Logic_T_ @ Test.U @ Test.Logic (Test.U @ Test.Logic)

lvl16_r1es
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl16_r1es =
  Test.L
    @ (Test.C Test.Logic_T_ Test.U)
    @ (Test.C Test.Logic_F_ Test.U
       Test.:+: (Test.C Test.Logic_Not_ Test.I
                 Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                           Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                               Test.:+: Test.C
                                                          Test.Logic_Disj_
                                                          (Test.I Test.:*: Test.I))))))
    @ Test.Logic
    lvl15_r1eq

lvl17_r1eu
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId, Caf=NoCafRefs]
lvl17_r1eu =
  Test.R
    @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
    @ (Test.C Test.Logic_T_ Test.U
       Test.:+: (Test.C Test.Logic_F_ Test.U
                 Test.:+: (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))))
    @ Test.Logic
    lvl16_r1es

$j_r1ew
  :: (Test.:+:)
       (Test.C Test.Logic_T_ Test.U)
       (Test.C Test.Logic_F_ Test.U
        Test.:+: (Test.C Test.Logic_Not_ Test.I
                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                            Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                Test.:+: Test.C
                                                           Test.Logic_Disj_
                                                           (Test.I Test.:*: Test.I))))))
       Test.Logic
     -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs]
$j_r1ew =
  \ (ds_dR4
       :: (Test.:+:)
            (Test.C Test.Logic_T_ Test.U)
            (Test.C Test.Logic_F_ Test.U
             Test.:+: (Test.C Test.Logic_Not_ Test.I
                       Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                           Test.:+: (Test.C
                                                       Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                     Test.:+: Test.C
                                                                Test.Logic_Disj_
                                                                (Test.I Test.:*: Test.I))))))
            Test.Logic) ->
    case ds_dR4 of _ {
      Test.L ds1_dRu ->
        case ds1_dRu of _ { Test.C ds2_dRv ->
        case ds2_dRv of _ { Test.U -> Test.T }
        };
      Test.R ds1_dR5 ->
        case ds1_dR5 of _ {
          Test.L ds2_dRs ->
            case ds2_dRs of _ { Test.C ds3_dRt ->
            case ds3_dRt of _ { Test.U -> Test.F }
            };
          Test.R ds2_dR6 ->
            case ds2_dR6 of _ {
              Test.L ds3_dRq ->
                case ds3_dRq of _ { Test.C ds4_dRr ->
                Test.Not
                  (ds4_dRr
                   `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                };
              Test.R ds3_dR7 ->
                case ds3_dR7 of _ {
                  Test.L ds4_dRm ->
                    case ds4_dRm of _ { Test.C ds5_dRn ->
                    case ds5_dRn of _ { Test.:*: ds6_dRo ds7_dRp ->
                    Test.Impl
                      (ds6_dRo
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                      (ds7_dRp
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    }
                    };
                  Test.R ds4_dR8 ->
                    case ds4_dR8 of _ {
                      Test.L ds5_dRi ->
                        case ds5_dRi of _ { Test.C ds6_dRj ->
                        case ds6_dRj of _ { Test.:*: ds7_dRk ds8_dRl ->
                        Test.Equiv
                          (ds7_dRk
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds8_dRl
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds5_dR9 ->
                        case ds5_dR9 of _ {
                          Test.L ds6_dRe ->
                            case ds6_dRe of _ { Test.C ds7_dRf ->
                            case ds7_dRf of _ { Test.:*: ds8_dRg ds9_dRh ->
                            Test.Conj
                              (ds8_dRg
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds9_dRh
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds6_dRa ->
                            case ds6_dRa of _ { Test.C ds7_dRb ->
                            case ds7_dRb of _ { Test.:*: ds8_dRc ds9_dRd ->
                            Test.Disj
                              (ds8_dRc
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds9_dRd
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            }
                        }
                    }
                }
            }
        }
    }

Rec {
lvl18_r1ey
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId]
lvl18_r1ey =
  Test.updateString1
    @ Test.Logic Test.testLogic_updateString lvl14_r1eo

lvl19_r1eB
  :: (Test.:+:)
       (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
       (Test.C Test.Logic_T_ Test.U
        Test.:+: (Test.C Test.Logic_F_ Test.U
                  Test.:+: (Test.C Test.Logic_Not_ Test.I
                            Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                      Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                Test.:+: (Test.C
                                                            Test.Logic_Conj_
                                                            (Test.I Test.:*: Test.I)
                                                          Test.:+: Test.C
                                                                     Test.Logic_Disj_
                                                                     (Test.I Test.:*: Test.I)))))))
       Test.Logic
[GblId]
lvl19_r1eB =
  Test.updateString1
    @ Test.Logic Test.testLogic_updateString lvl17_r1eu

Test.testLogic_updateString :: Test.Logic -> Test.Logic
[GblId, Arity=1, Str=DmdType S]
Test.testLogic_updateString =
  \ (x_aTT :: Test.Logic) ->
    case x_aTT of _ {
      Test.Var f0_ahW ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.L
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.C
                     @ Test.Logic_Var_
                     @ (Test.K GHC.Base.String)
                     @ Test.Logic
                     (f0_ahW
                      `cast` (sym (Test.NTCo:K GHC.Base.String Test.Logic)
                              :: GHC.Base.String ~ Test.K GHC.Base.String Test.Logic))))
        of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.T ->
        case lvl19_r1eB of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.F ->
        case lvl18_r1ey of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.Not f0_ahX ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.L
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.C
                              @ Test.Logic_Not_
                              @ Test.I
                              @ Test.Logic
                              (f0_ahX
                               `cast` (sym (Test.NTCo:I Test.Logic)
                                       :: Test.Logic ~ Test.I Test.Logic)))))))
        of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.Impl f0_ahY f1_ahZ ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.L
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.C
                                 @ Test.Logic_Impl_
                                 @ (Test.I Test.:*: Test.I)
                                 @ Test.Logic
                                 (Test.:*:
                                    @ Test.I
                                    @ Test.I
                                    @ Test.Logic
                                    (f0_ahY
                                     `cast` (sym (Test.NTCo:I Test.Logic)
                                             :: Test.Logic ~ Test.I Test.Logic))
                                    (f1_ahZ
                                     `cast` (sym (Test.NTCo:I Test.Logic)
                                             :: Test.Logic ~ Test.I Test.Logic)))))))))
        of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.Equiv f0_ai0 f1_ai1 ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.L
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.C
                                    @ Test.Logic_Equiv_
                                    @ (Test.I Test.:*: Test.I)
                                    @ Test.Logic
                                    (Test.:*:
                                       @ Test.I
                                       @ Test.I
                                       @ Test.Logic
                                       (f0_ai0
                                        `cast` (sym (Test.NTCo:I Test.Logic)
                                                :: Test.Logic ~ Test.I Test.Logic))
                                       (f1_ai1
                                        `cast` (sym (Test.NTCo:I Test.Logic)
                                                :: Test.Logic ~ Test.I Test.Logic))))))))))
        of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.Conj f0_ai2 f1_ai3 ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.R
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.L
                                    @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                    @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                    @ Test.Logic
                                    (Test.C
                                       @ Test.Logic_Conj_
                                       @ (Test.I Test.:*: Test.I)
                                       @ Test.Logic
                                       (Test.:*:
                                          @ Test.I
                                          @ Test.I
                                          @ Test.Logic
                                          (f0_ai2
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic))
                                          (f1_ai3
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic)))))))))))
        of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        };
      Test.Disj f0_ai4 f1_ai5 ->
        case Test.updateString1
               @ Test.Logic
               Test.testLogic_updateString
               (Test.R
                  @ (Test.C Test.Logic_Var_ (Test.K GHC.Base.String))
                  @ (Test.C Test.Logic_T_ Test.U
                     Test.:+: (Test.C Test.Logic_F_ Test.U
                               Test.:+: (Test.C Test.Logic_Not_ Test.I
                                         Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                                   Test.:+: (Test.C
                                                               Test.Logic_Equiv_
                                                               (Test.I Test.:*: Test.I)
                                                             Test.:+: (Test.C
                                                                         Test.Logic_Conj_
                                                                         (Test.I Test.:*: Test.I)
                                                                       Test.:+: Test.C
                                                                                  Test.Logic_Disj_
                                                                                  (Test.I
                                                                                   Test.:*: Test.I)))))))
                  @ Test.Logic
                  (Test.R
                     @ (Test.C Test.Logic_T_ Test.U)
                     @ (Test.C Test.Logic_F_ Test.U
                        Test.:+: (Test.C Test.Logic_Not_ Test.I
                                  Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                            Test.:+: (Test.C
                                                        Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                                      Test.:+: (Test.C
                                                                  Test.Logic_Conj_
                                                                  (Test.I Test.:*: Test.I)
                                                                Test.:+: Test.C
                                                                           Test.Logic_Disj_
                                                                           (Test.I
                                                                            Test.:*: Test.I))))))
                     @ Test.Logic
                     (Test.R
                        @ (Test.C Test.Logic_F_ Test.U)
                        @ (Test.C Test.Logic_Not_ Test.I
                           Test.:+: (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                                     Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                               Test.:+: (Test.C
                                                           Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                         Test.:+: Test.C
                                                                    Test.Logic_Disj_
                                                                    (Test.I Test.:*: Test.I)))))
                        @ Test.Logic
                        (Test.R
                           @ (Test.C Test.Logic_Not_ Test.I)
                           @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I)
                              Test.:+: (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                        Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                                  Test.:+: Test.C
                                                             Test.Logic_Disj_
                                                             (Test.I Test.:*: Test.I))))
                           @ Test.Logic
                           (Test.R
                              @ (Test.C Test.Logic_Impl_ (Test.I Test.:*: Test.I))
                              @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I)
                                 Test.:+: (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                           Test.:+: Test.C
                                                      Test.Logic_Disj_ (Test.I Test.:*: Test.I)))
                              @ Test.Logic
                              (Test.R
                                 @ (Test.C Test.Logic_Equiv_ (Test.I Test.:*: Test.I))
                                 @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I)
                                    Test.:+: Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                 @ Test.Logic
                                 (Test.R
                                    @ (Test.C Test.Logic_Conj_ (Test.I Test.:*: Test.I))
                                    @ (Test.C Test.Logic_Disj_ (Test.I Test.:*: Test.I))
                                    @ Test.Logic
                                    (Test.C
                                       @ Test.Logic_Disj_
                                       @ (Test.I Test.:*: Test.I)
                                       @ Test.Logic
                                       (Test.:*:
                                          @ Test.I
                                          @ Test.I
                                          @ Test.Logic
                                          (f0_ai4
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic))
                                          (f1_ai5
                                           `cast` (sym (Test.NTCo:I Test.Logic)
                                                   :: Test.Logic ~ Test.I Test.Logic)))))))))))
        of _ {
          Test.L ds_dRw ->
            case ds_dRw of _ { Test.C ds1_dRx ->
            Test.Var
              (ds1_dRx
               `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                       :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
            };
          Test.R ds_dR4 -> $j_r1ew ds_dR4
        }
    }
end Rec }

Test.testLogic :: Test.Logic
[GblId,
 Str=DmdType,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=IF_ARGS [] 2 0}]
Test.testLogic = Test.testLogic_updateString Test.logic

Rec {
updateString2_r1eD :: Test.Logic -> Test.Logic
[GblId, Arity=1, Caf=NoCafRefs, Str=DmdType S]
updateString2_r1eD =
  \ (x_aTT :: Test.Logic) ->
    case Test.updateString1
           @ Test.Logic
           updateString2_r1eD
           (Test.fromLogic
              @ Test.Logic_Var_
              @ Test.Logic_T_
              @ Test.Logic_F_
              @ Test.Logic_Not_
              @ Test.Logic_Impl_
              @ Test.Logic_Equiv_
              @ Test.Logic_Conj_
              @ Test.Logic_Disj_
              x_aTT)
    of _ {
      Test.L ds_dRw ->
        case ds_dRw of _ { Test.C ds1_dRx ->
        Test.Var
          (ds1_dRx
           `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                   :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
        };
      Test.R ds_dR4 ->
        case ds_dR4 of _ {
          Test.L ds1_dRu ->
            case ds1_dRu of _ { Test.C ds2_dRv ->
            case ds2_dRv of _ { Test.U -> Test.T }
            };
          Test.R ds1_dR5 ->
            case ds1_dR5 of _ {
              Test.L ds2_dRs ->
                case ds2_dRs of _ { Test.C ds3_dRt ->
                case ds3_dRt of _ { Test.U -> Test.F }
                };
              Test.R ds2_dR6 ->
                case ds2_dR6 of _ {
                  Test.L ds3_dRq ->
                    case ds3_dRq of _ { Test.C ds4_dRr ->
                    Test.Not
                      (ds4_dRr
                       `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                    };
                  Test.R ds3_dR7 ->
                    case ds3_dR7 of _ {
                      Test.L ds4_dRm ->
                        case ds4_dRm of _ { Test.C ds5_dRn ->
                        case ds5_dRn of _ { Test.:*: ds6_dRo ds7_dRp ->
                        Test.Impl
                          (ds6_dRo
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                          (ds7_dRp
                           `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                        }
                        };
                      Test.R ds4_dR8 ->
                        case ds4_dR8 of _ {
                          Test.L ds5_dRi ->
                            case ds5_dRi of _ { Test.C ds6_dRj ->
                            case ds6_dRj of _ { Test.:*: ds7_dRk ds8_dRl ->
                            Test.Equiv
                              (ds7_dRk
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                              (ds8_dRl
                               `cast` (Test.NTCo:I Test.Logic :: Test.I Test.Logic ~ Test.Logic))
                            }
                            };
                          Test.R ds5_dR9 ->
                            case ds5_dR9 of _ {
                              Test.L ds6_dRe ->
                                case ds6_dRe of _ { Test.C ds7_dRf ->
                                case ds7_dRf of _ { Test.:*: ds8_dRg ds9_dRh ->
                                Test.Conj
                                  (ds8_dRg
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRh
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                };
                              Test.R ds6_dRa ->
                                case ds6_dRa of _ { Test.C ds7_dRb ->
                                case ds7_dRb of _ { Test.:*: ds8_dRc ds9_dRd ->
                                Test.Disj
                                  (ds8_dRc
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                  (ds9_dRd
                                   `cast` (Test.NTCo:I Test.Logic
                                           :: Test.I Test.Logic ~ Test.Logic))
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
end Rec }

Test.updateString_$supdateString :: Test.Logic -> Test.Logic
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=False, ConLike=False,
         Cheap=False, Expandable=False, Guidance=InlineRule(sat, -)
         Tmpl= letrec {
                 updateString3_X14G [Occ=LoopBreaker] :: Test.Logic -> Test.Logic
                 [LclId, Arity=1]
                 updateString3_X14G =
                   \ (x_aTT [Occ=Once] :: Test.Logic) ->
                     case Test.updateString1
                            @ Test.Logic
                            updateString3_X14G
                            (Test.fromLogic
                               @ Test.Logic_Var_
                               @ Test.Logic_T_
                               @ Test.Logic_F_
                               @ Test.Logic_Not_
                               @ Test.Logic_Impl_
                               @ Test.Logic_Equiv_
                               @ Test.Logic_Conj_
                               @ Test.Logic_Disj_
                               x_aTT)
                     of _ {
                       Test.L ds_dRw [Occ=Once!] ->
                         case ds_dRw of _ { Test.C ds1_dRx [Occ=Once] ->
                         Test.Var
                           (ds1_dRx
                            `cast` (Test.NTCo:K GHC.Base.String Test.Logic
                                    :: Test.K GHC.Base.String Test.Logic ~ GHC.Base.String))
                         };
                       Test.R ds_dR4 [Occ=Once!] ->
                         case ds_dR4 of _ {
                           Test.L ds1_dRu [Occ=Once!] ->
                             case ds1_dRu of _ { Test.C ds2_dRv [Occ=Once!] ->
                             case ds2_dRv of _ { Test.U -> Test.T }
                             };
                           Test.R ds1_dR5 [Occ=Once!] ->
                             case ds1_dR5 of _ {
                               Test.L ds2_dRs [Occ=Once!] ->
                                 case ds2_dRs of _ { Test.C ds3_dRt [Occ=Once!] ->
                                 case ds3_dRt of _ { Test.U -> Test.F }
                                 };
                               Test.R ds2_dR6 [Occ=Once!] ->
                                 case ds2_dR6 of _ {
                                   Test.L ds3_dRq [Occ=Once!] ->
                                     case ds3_dRq of _ { Test.C ds4_dRr [Occ=Once] ->
                                     Test.Not
                                       (ds4_dRr
                                        `cast` (Test.NTCo:I Test.Logic
                                                :: Test.I Test.Logic ~ Test.Logic))
                                     };
                                   Test.R ds3_dR7 [Occ=Once!] ->
                                     case ds3_dR7 of _ {
                                       Test.L ds4_dRm [Occ=Once!] ->
                                         case ds4_dRm of _ { Test.C ds5_dRn [Occ=Once!] ->
                                         case ds5_dRn
                                         of _ { Test.:*: ds6_dRo [Occ=Once] ds7_dRp [Occ=Once] ->
                                         Test.Impl
                                           (ds6_dRo
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                           (ds7_dRp
                                            `cast` (Test.NTCo:I Test.Logic
                                                    :: Test.I Test.Logic ~ Test.Logic))
                                         }
                                         };
                                       Test.R ds4_dR8 [Occ=Once!] ->
                                         case ds4_dR8 of _ {
                                           Test.L ds5_dRi [Occ=Once!] ->
                                             case ds5_dRi of _ { Test.C ds6_dRj [Occ=Once!] ->
                                             case ds6_dRj
                                             of _
                                             { Test.:*: ds7_dRk [Occ=Once] ds8_dRl [Occ=Once] ->
                                             Test.Equiv
                                               (ds7_dRk
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                               (ds8_dRl
                                                `cast` (Test.NTCo:I Test.Logic
                                                        :: Test.I Test.Logic ~ Test.Logic))
                                             }
                                             };
                                           Test.R ds5_dR9 [Occ=Once!] ->
                                             case ds5_dR9 of _ {
                                               Test.L ds6_dRe [Occ=Once!] ->
                                                 case ds6_dRe of _ { Test.C ds7_dRf [Occ=Once!] ->
                                                 case ds7_dRf
                                                 of _
                                                 { Test.:*: ds8_dRg [Occ=Once] ds9_dRh [Occ=Once] ->
                                                 Test.Conj
                                                   (ds8_dRg
                                                    `cast` (Test.NTCo:I Test.Logic
                                                            :: Test.I Test.Logic ~ Test.Logic))
                                                   (ds9_dRh
                                                    `cast` (Test.NTCo:I Test.Logic
                                                            :: Test.I Test.Logic ~ Test.Logic))
                                                 }
                                                 };
                                               Test.R ds6_dRa [Occ=Once!] ->
                                                 case ds6_dRa of _ { Test.C ds7_dRb [Occ=Once!] ->
                                                 case ds7_dRb
                                                 of _
                                                 { Test.:*: ds8_dRc [Occ=Once] ds9_dRd [Occ=Once] ->
                                                 Test.Disj
                                                   (ds8_dRc
                                                    `cast` (Test.NTCo:I Test.Logic
                                                            :: Test.I Test.Logic ~ Test.Logic))
                                                   (ds9_dRd
                                                    `cast` (Test.NTCo:I Test.Logic
                                                            :: Test.I Test.Logic ~ Test.Logic))
                                                 }
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }; } in
               updateString3_X14G}]
Test.updateString_$supdateString = updateString2_r1eD

Test.updateString [InlPrag=INLINE]
  :: forall a_ahw.
     (Test.Regular a_ahw, Test.UpdateString (Test.PF a_ahw)) =>
     a_ahw -> a_ahw
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType LL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, -)
         Tmpl= \ (@ a_ay9)
                 ($dRegular_ayB :: Test.Regular a_ay9)
                 ($dUpdateString_ayC :: Test.UpdateString (Test.PF a_ay9)) ->
                 letrec {
                   sub_aQW [Occ=OnceL!] :: Test.PF a_ay9 a_ay9 -> Test.PF a_ay9 a_ay9
                   [LclId]
                   sub_aQW =
                     ($dUpdateString_ayC
                      `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay9)
                              :: Test.T:UpdateString (Test.PF a_ay9)
                                   ~
                                 (forall a_ah6.
                                  (a_ah6 -> a_ah6) -> Test.PF a_ay9 a_ah6 -> Test.PF a_ay9 a_ah6)))
                       @ a_ay9 updateString3_aya;
                   updateString3_aya [Occ=LoopBreaker] :: a_ay9 -> a_ay9
                   [LclId, Arity=1]
                   updateString3_aya =
                     \ (x_aTT [Occ=Once] :: a_ay9) ->
                       Test.to
                         @ a_ay9
                         $dRegular_ayB
                         (sub_aQW (Test.from @ a_ay9 $dRegular_ayB x_aTT)); } in
                 updateString3_aya}]
Test.updateString =
  \ (@ a_ay9)
    ($dRegular_ayB :: Test.Regular a_ay9)
    ($dUpdateString_ayC :: Test.UpdateString (Test.PF a_ay9)) ->
    letrec {
      sub_s1aB :: Test.PF a_ay9 a_ay9 -> Test.PF a_ay9 a_ay9
      [LclId, Str=DmdType]
      sub_s1aB =
        ($dUpdateString_ayC
         `cast` (Test.NTCo:T:UpdateString (Test.PF a_ay9)
                 :: Test.T:UpdateString (Test.PF a_ay9)
                      ~
                    (forall a_ah6.
                     (a_ah6 -> a_ah6) -> Test.PF a_ay9 a_ah6 -> Test.PF a_ay9 a_ah6)))
          @ a_ay9 updateString3_s1aC;
      updateString3_s1aC [Occ=LoopBreaker] :: a_ay9 -> a_ay9
      [LclId, Arity=1, Str=DmdType L]
      updateString3_s1aC =
        \ (x_aTT :: a_ay9) ->
          Test.to
            @ a_ay9
            $dRegular_ayB
            (sub_s1aB (Test.from @ a_ay9 $dRegular_ayB x_aTT)); } in
    updateString3_s1aC

Test.updateStringfU [InlPrag=INLINE]
  :: forall t_aBH a_aBM. t_aBH -> a_aBM -> a_aBM
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(sat, small)
         Tmpl= \ (@ t_aBH) (@ a_aBM) _ -> GHC.Base.id @ a_aBM}]
Test.updateStringfU =
  \ (@ t_aBH) (@ a_aBM) _ (eta1_B1 :: a_aBM) -> eta1_B1

Test.$fUpdateStringU :: Test.UpdateString Test.U
[GblId[DFunId(newtype)],
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType AS,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= (\ (@ a_aQp) ->
                  Test.updateStringfU @ (a_aQp -> a_aQp) @ (Test.U a_aQp))
               `cast` (sym (Test.NTCo:T:UpdateString Test.U)
                       :: (forall a_ah6. (a_ah6 -> a_ah6) -> Test.U a_ah6 -> Test.U a_ah6)
                            ~
                          Test.T:UpdateString Test.U)}]
Test.$fUpdateStringU =
  (\ (@ a_aQp) ->
     Test.updateStringfU @ (a_aQp -> a_aQp) @ (Test.U a_aQp))
  `cast` (sym (Test.NTCo:T:UpdateString Test.U)
          :: (forall a_ah6. (a_ah6 -> a_ah6) -> Test.U a_ah6 -> Test.U a_ah6)
               ~
             Test.T:UpdateString Test.U)

Test.unK :: forall a_ahu r_ahv. Test.K a_ahu r_ahv -> a_ahu
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unK =
  Test.unK1
  `cast` (forall a_auQ r_auR.
          Test.K a_auQ r_auR -> Test.NTCo:K a_auQ r_auR
          :: (forall a_auQ r_auR. Test.K a_auQ r_auR -> Test.K a_auQ r_auR)
               ~
             (forall a_auQ r_auR. Test.K a_auQ r_auR -> a_auQ))

Test.unI :: forall r_aht. Test.I r_aht -> r_aht
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 0 6}]
Test.unI =
  Test.unI1
  `cast` (forall r_auO. Test.I r_auO -> Test.NTCo:I r_auO
          :: (forall r_auO. Test.I r_auO -> Test.I r_auO)
               ~
             (forall r_auO. Test.I r_auO -> r_auO))

Test.unC
  :: forall c_ahj (f_ahk :: * -> *) r_ahl.
     Test.C c_ahj f_ahk r_ahl -> f_ahk r_ahl
[GblId[[RecSel]],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType U(S),
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ c_auK)
                 (@ f_auL::* -> *)
                 (@ r_auM)
                 (ds_dSx [Occ=Once!] :: Test.C c_auK f_auL r_auM) ->
                 case ds_dSx of _ { Test.C ds1_dSy [Occ=Once] -> ds1_dSy }}]
Test.unC =
  \ (@ c_auK)
    (@ f_auL::* -> *)
    (@ r_auM)
    (ds_dSx :: Test.C c_auK f_auL r_auM) ->
    case ds_dSx of _ { Test.C ds1_dSy -> ds1_dSy }

Test.$fConstructorLogic_Var__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_Var_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aPq::* -> (* -> *) -> * -> *)
                 (@ f_aPr::* -> *)
                 (@ r_aPs)
                 _ ->
                 Test.$fConstructorLogic_Var_1}]
Test.$fConstructorLogic_Var__$cconName =
  \ (@ t_aPq::* -> (* -> *) -> * -> *)
    (@ f_aPr::* -> *)
    (@ r_aPs)
    _ ->
    Test.$fConstructorLogic_Var_1

Test.$fConstructorLogic_Var_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Var_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Var__$cconName,
                              Test.$dmconFixity_$s$dmconFixity,
                              Test.$dmconIsRecord_$s$dmconIsRecord]]
Test.$fConstructorLogic_Var_ =
  Test.D:Constructor
    @ Test.Logic_Var_
    Test.$fConstructorLogic_Var__$cconName
    Test.$dmconFixity_$s$dmconFixity
    Test.$dmconIsRecord_$s$dmconIsRecord

Test.$fConstructorLogic_Impl__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_Impl_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aPe::* -> (* -> *) -> * -> *)
                 (@ f_aPf::* -> *)
                 (@ r_aPg)
                 _ ->
                 Test.$fConstructorLogic_Impl_1}]
Test.$fConstructorLogic_Impl__$cconName =
  \ (@ t_aPe::* -> (* -> *) -> * -> *)
    (@ f_aPf::* -> *)
    (@ r_aPg)
    _ ->
    Test.$fConstructorLogic_Impl_1

Test.$fConstructorLogic_Impl_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Impl_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Impl__$cconName,
                              Test.$dmconFixity_$s$dmconFixity1,
                              Test.$dmconIsRecord_$s$dmconIsRecord1]]
Test.$fConstructorLogic_Impl_ =
  Test.D:Constructor
    @ Test.Logic_Impl_
    Test.$fConstructorLogic_Impl__$cconName
    Test.$dmconFixity_$s$dmconFixity1
    Test.$dmconIsRecord_$s$dmconIsRecord1

Test.$fConstructorLogic_Equiv__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_Equiv_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aP2::* -> (* -> *) -> * -> *)
                 (@ f_aP3::* -> *)
                 (@ r_aP4)
                 _ ->
                 Test.$fConstructorLogic_Equiv_1}]
Test.$fConstructorLogic_Equiv__$cconName =
  \ (@ t_aP2::* -> (* -> *) -> * -> *)
    (@ f_aP3::* -> *)
    (@ r_aP4)
    _ ->
    Test.$fConstructorLogic_Equiv_1

Test.$fConstructorLogic_Equiv_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Equiv_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Equiv__$cconName,
                              Test.$dmconFixity_$s$dmconFixity2,
                              Test.$dmconIsRecord_$s$dmconIsRecord2]]
Test.$fConstructorLogic_Equiv_ =
  Test.D:Constructor
    @ Test.Logic_Equiv_
    Test.$fConstructorLogic_Equiv__$cconName
    Test.$dmconFixity_$s$dmconFixity2
    Test.$dmconIsRecord_$s$dmconIsRecord2

Test.$fConstructorLogic_Conj__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_Conj_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOQ::* -> (* -> *) -> * -> *)
                 (@ f_aOR::* -> *)
                 (@ r_aOS)
                 _ ->
                 Test.$fConstructorLogic_Conj_1}]
Test.$fConstructorLogic_Conj__$cconName =
  \ (@ t_aOQ::* -> (* -> *) -> * -> *)
    (@ f_aOR::* -> *)
    (@ r_aOS)
    _ ->
    Test.$fConstructorLogic_Conj_1

Test.$fConstructorLogic_Conj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Conj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Conj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity3,
                              Test.$dmconIsRecord_$s$dmconIsRecord3]]
Test.$fConstructorLogic_Conj_ =
  Test.D:Constructor
    @ Test.Logic_Conj_
    Test.$fConstructorLogic_Conj__$cconName
    Test.$dmconFixity_$s$dmconFixity3
    Test.$dmconIsRecord_$s$dmconIsRecord3

Test.$fConstructorLogic_Disj__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_Disj_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOE::* -> (* -> *) -> * -> *)
                 (@ f_aOF::* -> *)
                 (@ r_aOG)
                 _ ->
                 Test.$fConstructorLogic_Disj_1}]
Test.$fConstructorLogic_Disj__$cconName =
  \ (@ t_aOE::* -> (* -> *) -> * -> *)
    (@ f_aOF::* -> *)
    (@ r_aOG)
    _ ->
    Test.$fConstructorLogic_Disj_1

Test.$fConstructorLogic_Disj_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Disj_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Disj__$cconName,
                              Test.$dmconFixity_$s$dmconFixity4,
                              Test.$dmconIsRecord_$s$dmconIsRecord4]]
Test.$fConstructorLogic_Disj_ =
  Test.D:Constructor
    @ Test.Logic_Disj_
    Test.$fConstructorLogic_Disj__$cconName
    Test.$dmconFixity_$s$dmconFixity4
    Test.$dmconIsRecord_$s$dmconIsRecord4

Test.$fConstructorLogic_Not__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_Not_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOs::* -> (* -> *) -> * -> *)
                 (@ f_aOt::* -> *)
                 (@ r_aOu)
                 _ ->
                 Test.$fConstructorLogic_Not_1}]
Test.$fConstructorLogic_Not__$cconName =
  \ (@ t_aOs::* -> (* -> *) -> * -> *)
    (@ f_aOt::* -> *)
    (@ r_aOu)
    _ ->
    Test.$fConstructorLogic_Not_1

Test.$fConstructorLogic_Not_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_Not_
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_Not__$cconName,
                              Test.$dmconFixity_$s$dmconFixity5,
                              Test.$dmconIsRecord_$s$dmconIsRecord5]]
Test.$fConstructorLogic_Not_ =
  Test.D:Constructor
    @ Test.Logic_Not_
    Test.$fConstructorLogic_Not__$cconName
    Test.$dmconFixity_$s$dmconFixity5
    Test.$dmconIsRecord_$s$dmconIsRecord5

Test.$fConstructorLogic_T__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_T_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aOg::* -> (* -> *) -> * -> *)
                 (@ f_aOh::* -> *)
                 (@ r_aOi)
                 _ ->
                 Test.$fConstructorLogic_T_1}]
Test.$fConstructorLogic_T__$cconName =
  \ (@ t_aOg::* -> (* -> *) -> * -> *)
    (@ f_aOh::* -> *)
    (@ r_aOi)
    _ ->
    Test.$fConstructorLogic_T_1

Test.$fConstructorLogic_T_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_T_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_T__$cconName,
                              Test.$dmconFixity_$s$dmconFixity6,
                              Test.$dmconIsRecord_$s$dmconIsRecord6]]
Test.$fConstructorLogic_T_ =
  Test.D:Constructor
    @ Test.Logic_T_
    Test.$fConstructorLogic_T__$cconName
    Test.$dmconFixity_$s$dmconFixity6
    Test.$dmconIsRecord_$s$dmconIsRecord6

Test.$fConstructorLogic_F__$cconName
  :: forall (t_aha :: * -> (* -> *) -> * -> *)
            (f_ahb :: * -> *)
            r_ahc.
     t_aha Test.Logic_F_ f_ahb r_ahc -> GHC.Base.String
[GblId,
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType A,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, small)
         Tmpl= \ (@ t_aO4::* -> (* -> *) -> * -> *)
                 (@ f_aO5::* -> *)
                 (@ r_aO6)
                 _ ->
                 Test.$fConstructorLogic_F_1}]
Test.$fConstructorLogic_F__$cconName =
  \ (@ t_aO4::* -> (* -> *) -> * -> *)
    (@ f_aO5::* -> *)
    (@ r_aO6)
    _ ->
    Test.$fConstructorLogic_F_1

Test.$fConstructorLogic_F_ [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: Test.Constructor Test.Logic_F_
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun Test.D:Constructor [Test.$fConstructorLogic_F__$cconName,
                              Test.$dmconFixity_$s$dmconFixity7,
                              Test.$dmconIsRecord_$s$dmconIsRecord7]]
Test.$fConstructorLogic_F_ =
  Test.D:Constructor
    @ Test.Logic_F_
    Test.$fConstructorLogic_F__$cconName
    Test.$dmconFixity_$s$dmconFixity7
    Test.$dmconIsRecord_$s$dmconIsRecord7

Test.$fReadFixity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Fixity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadFixity_readListDefault =
  Text.ParserCombinators.ReadP.run @ [Test.Fixity] Test.$fReadFixity4

Test.$fReadFixity_$creadsPrec
  :: GHC.Types.Int -> Text.ParserCombinators.ReadP.ReadS Test.Fixity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 6 6}]
Test.$fReadFixity_$creadsPrec =
  \ (eta_aXL :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Fixity
      (((GHC.Read.$dmreadsPrec3
           @ Test.Fixity
           (Test.$fReadFixity3
            `cast` (trans
                      (Text.ParserCombinators.ReadPrec.Prec
                       -> sym (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity))
                      (sym (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity))
                    :: (Text.ParserCombinators.ReadPrec.Prec
                        -> forall b_aWG.
                           (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWG)
                           -> Text.ParserCombinators.ReadP.P b_aWG)
                         ~
                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
           eta_aXL)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Fixity
                :: Text.ParserCombinators.ReadP.ReadP Test.Fixity
                     ~
                   (forall b_aWG.
                    (Test.Fixity -> Text.ParserCombinators.ReadP.P b_aWG)
                    -> Text.ParserCombinators.ReadP.P b_aWG)))
         @ Test.Fixity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn @ Test.Fixity))

Test.$fReadFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadFixity_$creadsPrec,
                           Test.$fReadFixity_readListDefault,
                           (Test.$fReadFixity2
                            `cast` (right
                                      (inst
                                         (forall a_aXr.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXr
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXr))
                                         Test.Fixity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity)),
                           (Test.$fReadFixity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))]]
Test.$fReadFixity =
  GHC.Read.D:Read
    @ Test.Fixity
    Test.$fReadFixity_$creadsPrec
    Test.$fReadFixity_readListDefault
    (Test.$fReadFixity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Fixity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Fixity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Fixity))
    (Test.$fReadFixity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec [Test.Fixity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Fixity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Fixity]))

Test.$fOrdFixity_$ccompare
  :: Test.Fixity -> Test.Fixity -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [6 6] 33 0}]
Test.$fOrdFixity_$ccompare =
  \ (a2_atT :: Test.Fixity) (b_atU :: Test.Fixity) ->
    let {
      $j1_s1aG :: GHC.Prim.Int# -> GHC.Ordering.Ordering
      [LclId, Arity=1, Str=DmdType L]
      $j1_s1aG =
        \ (a#_au0 :: GHC.Prim.Int#) ->
          let {
            $j2_s1aE :: GHC.Prim.Int# -> GHC.Ordering.Ordering
            [LclId, Arity=1, Str=DmdType L]
            $j2_s1aE =
              \ (b#_au1 :: GHC.Prim.Int#) ->
                case GHC.Prim.==# a#_au0 b#_au1 of _ {
                  GHC.Bool.False ->
                    case GHC.Prim.<# a#_au0 b#_au1 of _ {
                      GHC.Bool.False -> GHC.Ordering.GT; GHC.Bool.True -> GHC.Ordering.LT
                    };
                  GHC.Bool.True ->
                    case a2_atT of _ {
                      Test.Prefix -> GHC.Ordering.EQ;
                      Test.Infix a11_atW a21_atX ->
                        case b_atU of _ {
                          Test.Prefix -> GHC.Ordering.EQ;
                          Test.Infix b1_atY b2_atZ ->
                            case a11_atW of _ {
                              Test.LeftAssociative ->
                                case b1_atY of _ {
                                  Test.LeftAssociative -> GHC.Base.compareInt a21_atX b2_atZ;
                                  Test.RightAssociative -> GHC.Ordering.LT;
                                  Test.NotAssociative -> GHC.Ordering.LT
                                };
                              Test.RightAssociative ->
                                case b1_atY of _ {
                                  Test.LeftAssociative -> GHC.Ordering.GT;
                                  Test.RightAssociative -> GHC.Base.compareInt a21_atX b2_atZ;
                                  Test.NotAssociative -> GHC.Ordering.LT
                                };
                              Test.NotAssociative ->
                                case b1_atY of _ {
                                  __DEFAULT -> GHC.Ordering.GT;
                                  Test.NotAssociative -> GHC.Base.compareInt a21_atX b2_atZ
                                }
                            }
                        }
                    }
                } } in
          case b_atU of _ {
            Test.Prefix -> $j2_s1aE 0; Test.Infix _ _ -> $j2_s1aE 1
          } } in
    case a2_atT of _ {
      Test.Prefix -> $j1_s1aG 0; Test.Infix _ _ -> $j1_s1aG 1
    }

Test.$fOrdFixity_$c< :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c< =
  \ (x_aZH :: Test.Fixity) (y_aZI :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZH y_aZI of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.LT -> GHC.Bool.True
    }

Test.$fOrdFixity_$c>=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c>= =
  \ (x_aZA :: Test.Fixity) (y_aZB :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZA y_aZB of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.LT -> GHC.Bool.False
    }

Test.$fOrdFixity_$c> :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c> =
  \ (x_aZt :: Test.Fixity) (y_aZu :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZt y_aZu of _ {
      __DEFAULT -> GHC.Bool.False; GHC.Ordering.GT -> GHC.Bool.True
    }

Test.$fOrdFixity_$c<=
  :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fOrdFixity_$c<= =
  \ (x_aZm :: Test.Fixity) (y_aZn :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZm y_aZn of _ {
      __DEFAULT -> GHC.Bool.True; GHC.Ordering.GT -> GHC.Bool.False
    }

Test.$fOrdFixity_$cmax :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 0}]
Test.$fOrdFixity_$cmax =
  \ (x_aZd :: Test.Fixity) (y_aZe :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZd y_aZe of _ {
      __DEFAULT -> y_aZe; GHC.Ordering.GT -> x_aZd
    }

Test.$fOrdFixity_$cmin :: Test.Fixity -> Test.Fixity -> Test.Fixity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 0}]
Test.$fOrdFixity_$cmin =
  \ (x_aZ4 :: Test.Fixity) (y_aZ5 :: Test.Fixity) ->
    case Test.$fOrdFixity_$ccompare x_aZ4 y_aZ5 of _ {
      __DEFAULT -> x_aZ4; GHC.Ordering.GT -> y_aZ5
    }

Test.$fShowFixity_$cshowsPrec
  :: GHC.Types.Int -> Test.Fixity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType LSL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2 5 0] 46 3}]
Test.$fShowFixity_$cshowsPrec =
  \ (ds_dSV :: GHC.Types.Int)
    (ds1_dSW :: Test.Fixity)
    (eta_B1 :: GHC.Base.String) ->
    case ds1_dSW of _ {
      Test.Prefix ->
        GHC.Base.++ @ GHC.Types.Char Test.$fShowFixity3 eta_B1;
      Test.Infix b1_atR b2_atS ->
        case ds_dSV of _ { GHC.Types.I# x_aXa ->
        let {
          p_s1aI :: GHC.Show.ShowS
          [LclId, Arity=1, Str=DmdType L]
          p_s1aI =
            \ (x1_X12u :: GHC.Base.String) ->
              GHC.Base.++
                @ GHC.Types.Char
                Test.$fShowFixity2
                (case b1_atR of _ {
                   Test.LeftAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity15
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a18q ->
                           GHC.Show.$wshowSignedInt 11 ww_a18q x1_X12u
                           }));
                   Test.RightAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity12
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a18q ->
                           GHC.Show.$wshowSignedInt 11 ww_a18q x1_X12u
                           }));
                   Test.NotAssociative ->
                     GHC.Base.++
                       @ GHC.Types.Char
                       Test.$fReadAssociativity9
                       (GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showSpace1
                          (case b2_atS of _ { GHC.Types.I# ww_a18q ->
                           GHC.Show.$wshowSignedInt 11 ww_a18q x1_X12u
                           }))
                 }) } in
        case GHC.Prim.>=# x_aXa 11 of _ {
          GHC.Bool.False -> p_s1aI eta_B1;
          GHC.Bool.True ->
            GHC.Types.:
              @ GHC.Types.Char
              GHC.Show.$dmshow6
              (p_s1aI (GHC.Types.: @ GHC.Types.Char GHC.Show.$dmshow5 eta_B1))
        }
        }
    }

Test.$fShowFixity_$cshow :: Test.Fixity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 4 0}]
Test.$fShowFixity_$cshow =
  \ (x_X17d :: Test.Fixity) ->
    Test.$fShowFixity_$cshowsPrec
      GHC.Base.zeroInt x_X17d (GHC.Types.[] @ GHC.Types.Char)

Test.$fShowFixity_$cshowList :: [Test.Fixity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 25 3}]
Test.$fShowFixity_$cshowList =
  \ (ds1_aYk :: [Test.Fixity]) (s_aYl :: GHC.Base.String) ->
    case ds1_aYk of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYl;
      : x_aYq xs_aYr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (Test.$fShowFixity_$cshowsPrec
             Test.$fShowFixity1
             x_aYq
             (let {
                lvl141_s1aK :: [GHC.Types.Char]
                [LclId, Str=DmdType]
                lvl141_s1aK =
                  GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYl } in
              letrec {
                showl_s1aM [Occ=LoopBreaker] :: [Test.Fixity] -> [GHC.Types.Char]
                [LclId, Arity=1, Str=DmdType S]
                showl_s1aM =
                  \ (ds2_aYv :: [Test.Fixity]) ->
                    case ds2_aYv of _ {
                      [] -> lvl141_s1aK;
                      : y_aYA ys_aYB ->
                        GHC.Types.:
                          @ GHC.Types.Char
                          GHC.Show.showList__1
                          (Test.$fShowFixity_$cshowsPrec
                             Test.$fShowFixity1 y_aYA (showl_s1aM ys_aYB))
                    }; } in
              showl_s1aM xs_aYr))
    }

Test.$fShowFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Fixity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowFixity_$cshowsPrec,
                           Test.$fShowFixity_$cshow, Test.$fShowFixity_$cshowList]]
Test.$fShowFixity =
  GHC.Show.D:Show
    @ Test.Fixity
    Test.$fShowFixity_$cshowsPrec
    Test.$fShowFixity_$cshow
    Test.$fShowFixity_$cshowList

Test.$fEqFixity_$c== :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 16 4}]
Test.$fEqFixity_$c== =
  \ (ds_dSP :: Test.Fixity) (ds1_dSQ :: Test.Fixity) ->
    case ds_dSP of _ {
      Test.Prefix ->
        case ds1_dSQ of _ {
          Test.Prefix -> GHC.Bool.True; Test.Infix _ _ -> GHC.Bool.False
        };
      Test.Infix a11_atG a2_atH ->
        case ds1_dSQ of _ {
          Test.Prefix -> GHC.Bool.False;
          Test.Infix b1_atI b2_atJ ->
            case a11_atG of _ {
              Test.LeftAssociative ->
                case b1_atI of _ {
                  Test.LeftAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXk ->
                    case b2_atJ of _ { GHC.Types.I# y_aXo -> GHC.Prim.==# x_aXk y_aXo }
                    };
                  Test.RightAssociative -> GHC.Bool.False;
                  Test.NotAssociative -> GHC.Bool.False
                };
              Test.RightAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.RightAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXk ->
                    case b2_atJ of _ { GHC.Types.I# y_aXo -> GHC.Prim.==# x_aXk y_aXo }
                    }
                };
              Test.NotAssociative ->
                case b1_atI of _ {
                  __DEFAULT -> GHC.Bool.False;
                  Test.NotAssociative ->
                    case a2_atH of _ { GHC.Types.I# x_aXk ->
                    case b2_atJ of _ { GHC.Types.I# y_aXo -> GHC.Prim.==# x_aXk y_aXo }
                    }
                }
            }
        }
    }

Test.$fEqFixity_$c/= :: Test.Fixity -> Test.Fixity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0 0] 4 2}]
Test.$fEqFixity_$c/= =
  \ (a2_atO :: Test.Fixity) (b_atP :: Test.Fixity) ->
    case Test.$fEqFixity_$c== a2_atO b_atP of _ {
      GHC.Bool.False -> GHC.Bool.True; GHC.Bool.True -> GHC.Bool.False
    }

Test.$fEqFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqFixity_$c==,
                            Test.$fEqFixity_$c/=]]
Test.$fEqFixity =
  GHC.Classes.D:Eq
    @ Test.Fixity Test.$fEqFixity_$c== Test.$fEqFixity_$c/=

Test.$fOrdFixity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Fixity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqFixity,
                             Test.$fOrdFixity_$ccompare, Test.$fOrdFixity_$c<,
                             Test.$fOrdFixity_$c>=, Test.$fOrdFixity_$c>, Test.$fOrdFixity_$c<=,
                             Test.$fOrdFixity_$cmax, Test.$fOrdFixity_$cmin]]
Test.$fOrdFixity =
  GHC.Classes.D:Ord
    @ Test.Fixity
    Test.$fEqFixity
    Test.$fOrdFixity_$ccompare
    Test.$fOrdFixity_$c<
    Test.$fOrdFixity_$c>=
    Test.$fOrdFixity_$c>
    Test.$fOrdFixity_$c<=
    Test.$fOrdFixity_$cmax
    Test.$fOrdFixity_$cmin

Test.$fReadAssociativity_readListDefault
  :: Text.ParserCombinators.ReadP.ReadS [Test.Associativity]
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=0, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [] 2 6}]
Test.$fReadAssociativity_readListDefault =
  Text.ParserCombinators.ReadP.run
    @ [Test.Associativity] Test.$fReadAssociativity16

Test.$fReadAssociativity_$creadsPrec
  :: GHC.Types.Int
     -> Text.ParserCombinators.ReadP.ReadS Test.Associativity
[GblId,
 Arity=1,
 Str=DmdType L,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [0] 6 6}]
Test.$fReadAssociativity_$creadsPrec =
  \ (eta_aXL :: GHC.Types.Int) ->
    Text.ParserCombinators.ReadP.run
      @ Test.Associativity
      (((GHC.Read.$dmreadsPrec3
           @ Test.Associativity Test.$fReadAssociativity3 eta_aXL)
        `cast` (Text.ParserCombinators.ReadP.NTCo:ReadP Test.Associativity
                :: Text.ParserCombinators.ReadP.ReadP Test.Associativity
                     ~
                   (forall b_aWG.
                    (Test.Associativity -> Text.ParserCombinators.ReadP.P b_aWG)
                    -> Text.ParserCombinators.ReadP.P b_aWG)))
         @ Test.Associativity
         (Text.ParserCombinators.ReadP.$fMonadP_$creturn
            @ Test.Associativity))

Test.$fReadAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Read.Read Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Read.D:Read [Test.$fReadAssociativity_$creadsPrec,
                           Test.$fReadAssociativity_readListDefault,
                           (Test.$fReadAssociativity2
                            `cast` (right
                                      (inst
                                         (forall a_aXr.
                                          Text.ParserCombinators.ReadPrec.ReadPrec a_aXr
                                          -> sym
                                               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                                  a_aXr))
                                         Test.Associativity)
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         Test.Associativity)),
                           (Test.$fReadAssociativity1
                            `cast` (sym
                                      (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                                         [Test.Associativity])
                                    :: (Text.ParserCombinators.ReadPrec.Prec
                                        -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                                         ~
                                       Text.ParserCombinators.ReadPrec.ReadPrec
                                         [Test.Associativity]))]]
Test.$fReadAssociativity =
  GHC.Read.D:Read
    @ Test.Associativity
    Test.$fReadAssociativity_$creadsPrec
    Test.$fReadAssociativity_readListDefault
    (Test.$fReadAssociativity2
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec Test.Associativity)
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP Test.Associativity)
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec Test.Associativity))
    (Test.$fReadAssociativity1
     `cast` (sym
               (Text.ParserCombinators.ReadPrec.NTCo:ReadPrec
                  [Test.Associativity])
             :: (Text.ParserCombinators.ReadPrec.Prec
                 -> Text.ParserCombinators.ReadP.ReadP [Test.Associativity])
                  ~
                Text.ParserCombinators.ReadPrec.ReadPrec [Test.Associativity]))

Test.$fOrdAssociativity_$cmin
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmin =
  \ (x_aZ4 :: Test.Associativity) (y_aZ5 :: Test.Associativity) ->
    case x_aZ4 of _ {
      Test.LeftAssociative ->
        case y_aZ5 of _ { __DEFAULT -> Test.LeftAssociative };
      Test.RightAssociative ->
        case y_aZ5 of _ {
          Test.LeftAssociative -> Test.LeftAssociative;
          Test.RightAssociative -> Test.RightAssociative;
          Test.NotAssociative -> Test.RightAssociative
        };
      Test.NotAssociative -> y_aZ5
    }

Test.$fOrdAssociativity_$cmax
  :: Test.Associativity -> Test.Associativity -> Test.Associativity
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [3 4] 3 1}]
Test.$fOrdAssociativity_$cmax =
  \ (x_aZd :: Test.Associativity) (y_aZe :: Test.Associativity) ->
    case x_aZd of _ {
      Test.LeftAssociative -> y_aZe;
      Test.RightAssociative ->
        case y_aZe of _ {
          __DEFAULT -> Test.RightAssociative;
          Test.NotAssociative -> Test.NotAssociative
        };
      Test.NotAssociative ->
        case y_aZe of _ { __DEFAULT -> Test.NotAssociative }
    }

Test.$fOrdAssociativity_$c<=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c<= =
  \ (x_aZm :: Test.Associativity) (y_aZn :: Test.Associativity) ->
    case x_aZm of _ {
      Test.LeftAssociative ->
        case y_aZn of _ { __DEFAULT -> GHC.Bool.True };
      Test.RightAssociative ->
        case y_aZn of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZn of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fOrdAssociativity_$c>
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c> =
  \ (x_aZt :: Test.Associativity) (y_aZu :: Test.Associativity) ->
    case x_aZt of _ {
      Test.LeftAssociative ->
        case y_aZu of _ { __DEFAULT -> GHC.Bool.False };
      Test.RightAssociative ->
        case y_aZu of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZu of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fOrdAssociativity_$c>=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c>= =
  \ (x_aZA :: Test.Associativity) (y_aZB :: Test.Associativity) ->
    case x_aZA of _ {
      Test.LeftAssociative ->
        case y_aZB of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case y_aZB of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case y_aZB of _ { __DEFAULT -> GHC.Bool.True }
    }

Test.$fOrdAssociativity_$c<
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$c< =
  \ (x_aZH :: Test.Associativity) (y_aZI :: Test.Associativity) ->
    case x_aZH of _ {
      Test.LeftAssociative ->
        case y_aZI of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case y_aZI of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case y_aZI of _ { __DEFAULT -> GHC.Bool.False }
    }

Test.$fOrdAssociativity_$ccompare
  :: Test.Associativity
     -> Test.Associativity
     -> GHC.Ordering.Ordering
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fOrdAssociativity_$ccompare =
  \ (a2_atB :: Test.Associativity) (b_atC :: Test.Associativity) ->
    case a2_atB of _ {
      Test.LeftAssociative ->
        case b_atC of _ {
          Test.LeftAssociative -> GHC.Ordering.EQ;
          Test.RightAssociative -> GHC.Ordering.LT;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.RightAssociative ->
        case b_atC of _ {
          Test.LeftAssociative -> GHC.Ordering.GT;
          Test.RightAssociative -> GHC.Ordering.EQ;
          Test.NotAssociative -> GHC.Ordering.LT
        };
      Test.NotAssociative ->
        case b_atC of _ {
          __DEFAULT -> GHC.Ordering.GT;
          Test.NotAssociative -> GHC.Ordering.EQ
        }
    }

Test.$fShowAssociativity_$cshowList
  :: [Test.Associativity] -> GHC.Show.ShowS
[GblId,
 Arity=2,
 Str=DmdType SL,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [7 0] 42 3}]
Test.$fShowAssociativity_$cshowList =
  \ (ds1_aYk :: [Test.Associativity]) (s_aYl :: GHC.Base.String) ->
    case ds1_aYk of _ {
      [] -> GHC.Base.unpackAppendCString# "[]" s_aYl;
      : x_aYq xs_aYr ->
        GHC.Types.:
          @ GHC.Types.Char
          GHC.Show.showList__3
          (let {
             eta_s1aS [Dmd=Just L] :: GHC.Base.String
             [LclId, Str=DmdType]
             eta_s1aS =
               let {
                 lvl141_s1aO :: [GHC.Types.Char]
                 [LclId, Str=DmdType]
                 lvl141_s1aO =
                   GHC.Types.: @ GHC.Types.Char GHC.Show.showList__2 s_aYl } in
               letrec {
                 showl_s1aQ [Occ=LoopBreaker]
                   :: [Test.Associativity] -> [GHC.Types.Char]
                 [LclId, Arity=1, Str=DmdType S]
                 showl_s1aQ =
                   \ (ds2_aYv :: [Test.Associativity]) ->
                     case ds2_aYv of _ {
                       [] -> lvl141_s1aO;
                       : y_aYA ys_aYB ->
                         GHC.Types.:
                           @ GHC.Types.Char
                           GHC.Show.showList__1
                           (case y_aYA of _ {
                              Test.LeftAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity15 (showl_s1aQ ys_aYB);
                              Test.RightAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity12 (showl_s1aQ ys_aYB);
                              Test.NotAssociative ->
                                GHC.Base.++
                                  @ GHC.Types.Char Test.$fReadAssociativity9 (showl_s1aQ ys_aYB)
                            })
                     }; } in
               showl_s1aQ xs_aYr } in
           case x_aYq of _ {
             Test.LeftAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_s1aS;
             Test.RightAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_s1aS;
             Test.NotAssociative ->
               GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_s1aS
           })
    }

Test.$fShowAssociativity_$cshow
  :: Test.Associativity -> GHC.Base.String
[GblId,
 Arity=1,
 Str=DmdType S,
 Unf=Unf{TopLvl=True, Arity=1, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [2] 1 0}]
Test.$fShowAssociativity_$cshow =
  \ (x_aYK :: Test.Associativity) ->
    case x_aYK of _ {
      Test.LeftAssociative -> Test.$fReadAssociativity15;
      Test.RightAssociative -> Test.$fReadAssociativity12;
      Test.NotAssociative -> Test.$fReadAssociativity9
    }

Test.$fShowAssociativity_$cshowsPrec
  :: GHC.Types.Int -> Test.Associativity -> GHC.Show.ShowS
[GblId,
 Arity=3,
 Str=DmdType ASL,
 Unf=Unf{TopLvl=True, Arity=3, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=InlineRule(unsat, -)
         Tmpl= \ _
                 (ds1_dSF [Occ=Once!] :: Test.Associativity)
                 (eta_B1 [Occ=Once*] :: GHC.Base.String) ->
                 case ds1_dSF of _ {
                   Test.LeftAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a17u)
                          (c_a17v [Occ=Once] :: GHC.Types.Char -> b_a17u -> b_a17u)
                          (n_a17w [Occ=Once] :: b_a17u) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a17u c_a17v n_a17w Test.$fReadAssociativity15)
                       eta_B1;
                   Test.RightAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a17u)
                          (c_a17v [Occ=Once] :: GHC.Types.Char -> b_a17u -> b_a17u)
                          (n_a17w [Occ=Once] :: b_a17u) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a17u c_a17v n_a17w Test.$fReadAssociativity12)
                       eta_B1;
                   Test.NotAssociative ->
                     GHC.Base.augment
                       @ GHC.Types.Char
                       (\ (@ b_a17u)
                          (c_a17v [Occ=Once] :: GHC.Types.Char -> b_a17u -> b_a17u)
                          (n_a17w [Occ=Once] :: b_a17u) ->
                          GHC.Base.foldr
                            @ GHC.Types.Char @ b_a17u c_a17v n_a17w Test.$fReadAssociativity9)
                       eta_B1
                 }}]
Test.$fShowAssociativity_$cshowsPrec =
  \ _ (ds1_dSF :: Test.Associativity) (eta_B1 :: GHC.Base.String) ->
    case ds1_dSF of _ {
      Test.LeftAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity15 eta_B1;
      Test.RightAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity12 eta_B1;
      Test.NotAssociative ->
        GHC.Base.++ @ GHC.Types.Char Test.$fReadAssociativity9 eta_B1
    }

Test.$fShowAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Show.Show Test.Associativity
[GblId[DFunId],
 Str=DmdType m,
 Unf=DFun GHC.Show.D:Show [Test.$fShowAssociativity_$cshowsPrec,
                           Test.$fShowAssociativity_$cshow,
                           Test.$fShowAssociativity_$cshowList]]
Test.$fShowAssociativity =
  GHC.Show.D:Show
    @ Test.Associativity
    Test.$fShowAssociativity_$cshowsPrec
    Test.$fShowAssociativity_$cshow
    Test.$fShowAssociativity_$cshowList

Test.$fEqAssociativity_$c/=
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c/= =
  \ (a2_atz :: Test.Associativity) (b_atA :: Test.Associativity) ->
    case a2_atz of _ {
      Test.LeftAssociative ->
        case b_atA of _ {
          Test.LeftAssociative -> GHC.Bool.False;
          Test.RightAssociative -> GHC.Bool.True;
          Test.NotAssociative -> GHC.Bool.True
        };
      Test.RightAssociative ->
        case b_atA of _ {
          __DEFAULT -> GHC.Bool.True; Test.RightAssociative -> GHC.Bool.False
        };
      Test.NotAssociative ->
        case b_atA of _ {
          __DEFAULT -> GHC.Bool.True; Test.NotAssociative -> GHC.Bool.False
        }
    }

Test.$fEqAssociativity_$c==
  :: Test.Associativity -> Test.Associativity -> GHC.Bool.Bool
[GblId,
 Arity=2,
 Caf=NoCafRefs,
 Str=DmdType SS,
 Unf=Unf{TopLvl=True, Arity=2, Value=True, ConLike=True, Cheap=True,
         Expandable=True, Guidance=IF_ARGS [4 6] 4 1}]
Test.$fEqAssociativity_$c== =
  \ (a2_atv :: Test.Associativity) (b_atw :: Test.Associativity) ->
    case a2_atv of _ {
      Test.LeftAssociative ->
        case b_atw of _ {
          Test.LeftAssociative -> GHC.Bool.True;
          Test.RightAssociative -> GHC.Bool.False;
          Test.NotAssociative -> GHC.Bool.False
        };
      Test.RightAssociative ->
        case b_atw of _ {
          __DEFAULT -> GHC.Bool.False; Test.RightAssociative -> GHC.Bool.True
        };
      Test.NotAssociative ->
        case b_atw of _ {
          __DEFAULT -> GHC.Bool.False; Test.NotAssociative -> GHC.Bool.True
        }
    }

Test.$fEqAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Eq Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Eq [Test.$fEqAssociativity_$c==,
                            Test.$fEqAssociativity_$c/=]]
Test.$fEqAssociativity =
  GHC.Classes.D:Eq
    @ Test.Associativity
    Test.$fEqAssociativity_$c==
    Test.$fEqAssociativity_$c/=

Test.$fOrdAssociativity [InlPrag=NOINLINE CONLIKE ALWAYS]
  :: GHC.Classes.Ord Test.Associativity
[GblId[DFunId],
 Caf=NoCafRefs,
 Str=DmdType m,
 Unf=DFun GHC.Classes.D:Ord [Test.$fEqAssociativity,
                             Test.$fOrdAssociativity_$ccompare, Test.$fOrdAssociativity_$c<,
                             Test.$fOrdAssociativity_$c>=, Test.$fOrdAssociativity_$c>,
                             Test.$fOrdAssociativity_$c<=, Test.$fOrdAssociativity_$cmax,
                             Test.$fOrdAssociativity_$cmin]]
Test.$fOrdAssociativity =
  GHC.Classes.D:Ord
    @ Test.Associativity
    Test.$fEqAssociativity
    Test.$fOrdAssociativity_$ccompare
    Test.$fOrdAssociativity_$c<
    Test.$fOrdAssociativity_$c>=
    Test.$fOrdAssociativity_$c>
    Test.$fOrdAssociativity_$c<=
    Test.$fOrdAssociativity_$cmax
    Test.$fOrdAssociativity_$cmin


------ Local rules for imported ids --------
"SPEC Test.$dmconIsRecord [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10Q [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconIsRecord @ Test.Logic_F_ $dConstructor_s10Q
      = Test.$dmconIsRecord_$s$dmconIsRecord7
"SPEC Test.$dmconIsRecord [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10T [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconIsRecord @ Test.Logic_T_ $dConstructor_s10T
      = Test.$dmconIsRecord_$s$dmconIsRecord6
"SPEC Test.$dmconIsRecord [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10W [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconIsRecord @ Test.Logic_Not_ $dConstructor_s10W
      = Test.$dmconIsRecord_$s$dmconIsRecord5
"SPEC Test.$dmconIsRecord [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10Z [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconIsRecord @ Test.Logic_Disj_ $dConstructor_s10Z
      = Test.$dmconIsRecord_$s$dmconIsRecord4
"SPEC Test.$dmconIsRecord [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s112 [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconIsRecord @ Test.Logic_Conj_ $dConstructor_s112
      = Test.$dmconIsRecord_$s$dmconIsRecord3
"SPEC Test.$dmconIsRecord [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s115 [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconIsRecord @ Test.Logic_Equiv_ $dConstructor_s115
      = Test.$dmconIsRecord_$s$dmconIsRecord2
"SPEC Test.$dmconIsRecord [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s118 [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconIsRecord @ Test.Logic_Impl_ $dConstructor_s118
      = Test.$dmconIsRecord_$s$dmconIsRecord1
"SPEC Test.$dmconIsRecord [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s11b [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconIsRecord @ Test.Logic_Var_ $dConstructor_s11b
      = Test.$dmconIsRecord_$s$dmconIsRecord
"SPEC Test.$dmconFixity [Test.Logic_F_]" ALWAYS
    forall {$dConstructor_s10s [Occ=Dead]
              :: Test.Constructor Test.Logic_F_}
      Test.$dmconFixity @ Test.Logic_F_ $dConstructor_s10s
      = Test.$dmconFixity_$s$dmconFixity7
"SPEC Test.$dmconFixity [Test.Logic_T_]" ALWAYS
    forall {$dConstructor_s10v [Occ=Dead]
              :: Test.Constructor Test.Logic_T_}
      Test.$dmconFixity @ Test.Logic_T_ $dConstructor_s10v
      = Test.$dmconFixity_$s$dmconFixity6
"SPEC Test.$dmconFixity [Test.Logic_Not_]" ALWAYS
    forall {$dConstructor_s10y [Occ=Dead]
              :: Test.Constructor Test.Logic_Not_}
      Test.$dmconFixity @ Test.Logic_Not_ $dConstructor_s10y
      = Test.$dmconFixity_$s$dmconFixity5
"SPEC Test.$dmconFixity [Test.Logic_Disj_]" ALWAYS
    forall {$dConstructor_s10B [Occ=Dead]
              :: Test.Constructor Test.Logic_Disj_}
      Test.$dmconFixity @ Test.Logic_Disj_ $dConstructor_s10B
      = Test.$dmconFixity_$s$dmconFixity4
"SPEC Test.$dmconFixity [Test.Logic_Conj_]" ALWAYS
    forall {$dConstructor_s10E [Occ=Dead]
              :: Test.Constructor Test.Logic_Conj_}
      Test.$dmconFixity @ Test.Logic_Conj_ $dConstructor_s10E
      = Test.$dmconFixity_$s$dmconFixity3
"SPEC Test.$dmconFixity [Test.Logic_Equiv_]" ALWAYS
    forall {$dConstructor_s10H [Occ=Dead]
              :: Test.Constructor Test.Logic_Equiv_}
      Test.$dmconFixity @ Test.Logic_Equiv_ $dConstructor_s10H
      = Test.$dmconFixity_$s$dmconFixity2
"SPEC Test.$dmconFixity [Test.Logic_Impl_]" ALWAYS
    forall {$dConstructor_s10K [Occ=Dead]
              :: Test.Constructor Test.Logic_Impl_}
      Test.$dmconFixity @ Test.Logic_Impl_ $dConstructor_s10K
      = Test.$dmconFixity_$s$dmconFixity1
"SPEC Test.$dmconFixity [Test.Logic_Var_]" ALWAYS
    forall {$dConstructor_s10N [Occ=Dead]
              :: Test.Constructor Test.Logic_Var_}
      Test.$dmconFixity @ Test.Logic_Var_ $dConstructor_s10N
      = Test.$dmconFixity_$s$dmconFixity
"SPEC Test.updateString [Test.Logic]" ALWAYS
    forall {$dRegular_s10g :: Test.Regular Test.Logic
            $dUpdateString_X17r :: Test.UpdateString (Test.PF Test.Logic)}
      Test.updateString @ Test.Logic $dRegular_s10g $dUpdateString_X17r
      = Test.updateString_$supdateString


