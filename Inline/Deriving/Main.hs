{-# LANGUAGE FlexibleInstances #-}

module Main where

import Base
import GHC.Generics
import Enum


instance GEnum (Tree Int) where genum = genumTree
instance GEnum Logic


{-
{-# SPECIALISE genum :: [Tree Int] #-}
{-# SPECIALISE genum :: [Logic]    #-}
-}

genumTree :: [Tree Int]
genumTree = map to enum'

mainTree :: Tree Int
mainTree  = genumTree !! 3000000

mainLogic :: Logic
mainLogic = genum !! 1000000

main :: IO ()
main = print (mainTree, mainLogic)
