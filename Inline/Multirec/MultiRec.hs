{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE TypeFamilies           #-}

module MultiRec where

import Base
import Eq
import Update
import Datatypes

-- {-# SPECIALIZE eq :: TreeF (Tree Int) -> Tree Int -> Tree Int -> Bool #-}
-- {-# SPECIALIZE updateInt :: TreeF (Tree Int) -> Tree Int -> Tree Int #-}

-- Test
testEq = eq Tree tree tree
testUpdate = updateInt Tree tree
