{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE TypeFamilies           #-}

module Datatypes (TreeF(..), Tree(..), tree) where

import Base

-- Tree datatype
data Tree a = Leaf | Bin a (Tree a) (Tree a) deriving (Show, Eq)

tree :: Tree Int
tree = Bin 2 Leaf (Bin 3 Leaf Leaf)

-- MultiRec Tree instances
data TreeF :: * -> * where
  Tree :: TreeF (Tree Int)
  
type instance PF TreeF =
      ((C Bin  (K Int :*: I (Tree Int) :*: I (Tree Int))) -- |Bin|
  :+: (C Leaf U))                                         -- |Leaf|
    :>: (Tree Int)

instance Fam TreeF where
  {-# INLINE [1] from #-}
  from Tree (Bin x l r) = Tag (L (C (K x :*: I (I0 l) :*: I (I0 r))))
  from Tree Leaf        = Tag (R (C U))
  {-# INLINE [1] to #-}
  to Tree (Tag (L (C (K x :*: (I (I0 l)) :*: (I (I0 r)))))) = Bin x l r
  to Tree (Tag (R (C U)))                                   = Leaf

instance El TreeF (Tree Int) where
  {-# INLINE [1] proof #-}
  proof = Tree


data Bin
instance Constructor Bin  where conName _ = "Bin"
data Leaf
instance Constructor Leaf where conName _ = "Leaf"

instance EqS TreeF where
  {-# INLINE eqS #-}
  eqS Tree Tree = Just Refl
