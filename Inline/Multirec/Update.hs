{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE OverlappingInstances   #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE Rank2Types             #-}
{-# LANGUAGE GADTs                  #-}

module Update where

import Base

--
import Datatypes
{-# SPECIALIZE updateInt :: TreeF (Tree Int) -> Tree Int -> Tree Int #-}
--

  
class UpdateInt phi (f :: (* -> *) -> * -> *) where
  {-# INLINE updateIntf #-}
  updateIntf :: (forall ix. phi ix -> r ix -> r ix)
             -> phi ix -> f r ix -> f r ix

instance UpdateInt phi U where
  {-# INLINE updateIntf #-}
  updateIntf _ _ = id

instance (El phi xi) => UpdateInt phi (I xi) where
  {-# INLINE updateIntf #-}
  updateIntf f _ (I x) = I (f proof x)

instance UpdateInt phi (K Int) where
  {-# INLINE updateIntf #-}
  updateIntf f _ (K n) | odd n     = K (n + 1)
                       | otherwise = K (n - 1)

instance UpdateInt phi (K x) where
  {-# INLINE updateIntf #-}
  updateIntf f _ (K x) = K x

instance (UpdateInt phi f, UpdateInt phi g) => UpdateInt phi (f :+: g) where
  {-# INLINE updateIntf #-}
  updateIntf f p (L x) = L (updateIntf f p x)
  updateIntf f p (R x) = R (updateIntf f p x)

instance (UpdateInt phi f, UpdateInt phi g) => UpdateInt phi (f :*: g) where
  {-# INLINE updateIntf #-}
  updateIntf f p (x :*: y) = updateIntf f p x :*: updateIntf f p y

instance (UpdateInt phi f) => UpdateInt phi (C c f) where
  {-# INLINE updateIntf #-}
  updateIntf f p (C x) = C (updateIntf f p x)

instance (UpdateInt phi f) => UpdateInt phi (f :>: xi) where
  {-# INLINE updateIntf #-}
  updateIntf f p (Tag x) = Tag (updateIntf f p x)
 
{-# INLINE updateInt #-}
updateInt :: (Fam phi, UpdateInt phi (PF phi)) => phi ix -> ix -> ix
updateInt p = to p . updateIntf (\p (I0 x) -> I0 (updateInt p x)) p . from p
