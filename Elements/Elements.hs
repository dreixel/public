{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE PatternSignatures     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE GADTs                 #-}

{-# OPTIONS_GHC -Wall -fno-warn-name-shadowing #-}

module Elements where

import Generics.MultiRec.Base

split :: Int -> [(Int,Int)]
split n = [(a,n-a) | a <- [0..n]]

class Elements (f :: (* -> *) -> (* -> *) -> * -> *) (s :: * -> *) where 
    helements :: s ix -> f s r ix -> 
              (forall ix. Ix s ix => s ix -> Int -> Integer) -> Int -> Integer


instance (Elements f s, Constructor c) => Elements (C c f) s where
    helements _  _                     _ 0 = 0
    helements ix (_ :: (C c f) s r xi) f n = helements ix (undefined :: f s r ix) f (n - 1)

instance (Elements f s, Elements g s) => Elements (f :+: g) s where
    helements ix (_ :: (f :+: g) s r ix) f n = helements ix (undefined :: f s r ix) f n 
                                           + helements ix (undefined :: g s r ix) f n

instance (Ix s ix, Elements f s, EqS s) => Elements (f :>: ix) s where
    helements ix (_ :: (f :>: ix) s r xi) f n = 
      case eqS ix (index :: s ix) of
        Nothing   -> 0
        Just Refl -> helements ix (undefined :: f s r xi) f n

instance (Ix s ix) => Elements (I ix) s where
    helements _ (_ :: (I ix) s r xi) f n = f (index :: s ix) n

instance (Elements f s, Elements g s) => Elements (f :*: g) s where
    helements ix (_ :: (f :*: g) s r ix) f n = sum
                                          [  helements ix (undefined :: f s r ix) f a
                                           * helements ix (undefined :: g s r ix) f b
                                          | (a,b) <- split n]

instance Elements (K a) s where
    helements _ _ _ 1 = 1
    helements _ _ _ _ = 0

instance Elements U s where
    helements _ _ _ 0 = 1
    helements _ _ _ _ = 0

-- Simple memoization (only for the initial s ix elementsM is called upon)
elementsM :: forall s ix. (EqS s, Ix s ix, Elements (PF s) s) => s ix -> Int -> Integer
elementsM s = elements'
  where
    elements' :: Int -> Integer
    elements' = let table :: [Integer]
                    table = map (helements s (undefined :: PF s s I0 ix) elements'') [0..]
                in \ n -> table !! n

    elements'' :: (Ix s ix') => s ix' -> Int -> Integer
    elements'' s' = case eqS s s' of
                      Nothing -> elementsM s'
                      Just Refl -> elements'

-- No memoization
elements :: (Ix s ix, Elements (PF s) s) => s ix -> Int -> Integer
elements s = helements s (undefined :: PF s s I0 ix) elements
