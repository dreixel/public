{-# LANGUAGE FlexibleContexts      #-}

module SingleExamples where

import Generics.MultiRec.Base

import SingleUse
import Single

import Elements

{-
testD = elementsM [ExS Logic] Logic

test :: Int -> IO ()
test n = let t = take n [ (n, length (testD n), testD n) | n <- [0..]]
             io (n,a,b) = do 
                            putStr (show n ++ " (" ++ show a ++ "):")
                            sequence_ (map (\x -> putStrLn ("\t" ++ show x)) b)
                            putStrLn ""
         in sequence_ (map io t)
-}