{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE PatternSignatures     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE GADTs                 #-}

{-# OPTIONS_GHC -Wall -fno-warn-name-shadowing #-}

module Elements2 where

import Generics.MultiRec.Base


split :: Int -> [(Int,Int)]
split n = [(a,n-a) | a <- [0..n]]

class Elements (f :: (* -> *) -> * -> *) (s :: * -> *) where 
    helements :: s ix -> f r ix -> 
              (forall ix. El s ix => s ix -> Int -> [r ix]) -> Int -> [f r ix]


instance (Elements f s, Constructor c) => Elements (C c f) s where
    helements _  _                     _ 0 = []
    helements ix (_ :: (C c f) r xi) f n = map C $ helements ix (undefined :: f r ix) f (n - 1)

instance (Elements f s, Elements g s) => Elements (f :+: g) s where
    helements ix (_ :: (f :+: g) r ix) f n =  map L (helements ix (undefined :: f r ix) f n)
                                           ++ map R (helements ix (undefined :: g r ix) f n)

instance (El s ix, Elements f s, EqS s) => Elements (f :>: ix) s where
    helements ix (_ :: (f :>: ix) r xi) f n = 
      case eqS ix (proof :: s ix) of
        Nothing   -> []
        Just Refl -> map Tag $ helements ix (undefined :: f r xi) f n

instance (El s ix) => Elements (I ix) s where
    helements _ (_ :: (I ix) r xi) f n = map I (f (proof :: s ix) n)

instance (Elements f s, Elements g s) => Elements (f :*: g) s where
    helements ix (_ :: (f :*: g) r ix) f n = 
        [ l :*: r | (a,b) <- split n,
                    l     <- helements ix (undefined :: f r ix) f a,
                    r     <- helements ix (undefined :: g r ix) f b
        ]

-- Constant cases
instance Elements (K Int) s where
    helements _ _ _ 1 = [K 0]
    helements _ _ _ _ = []

instance Elements (K Integer) s where
    helements _ _ _ 1 = [K 0]
    helements _ _ _ _ = []

instance Elements (K Float) s where
    helements _ _ _ 1 = [K 0]
    helements _ _ _ _ = []

instance Elements (K Double) s where
    helements _ _ _ 1 = [K 0]
    helements _ _ _ _ = []

instance Elements (K Char) s where
    helements _ _ _ 1 = [K '\0']
    helements _ _ _ _ = []

instance Elements (K [a]) s where
    helements _ _ _ 1 = [K []]
    helements _ _ _ _ = []

instance Elements U s where
    helements _ _ _ 0 = [U]
    helements _ _ _ _ = []


-- No memoization
elements :: (El s ix, Fam s, Elements (PF s) s) => s ix -> Int -> [ix]
elements s n = map (to s) $ helements s undefined (\s -> map I0 . elements s) n


-- Memoized version

data ExS s where
  ExS :: s ix -> ExS s
  
data IxEl s where
  IxEl :: s ix -> ix -> IxEl s

unIxEl :: (EqS s) => s ix -> IxEl s -> ix
unIxEl s' (IxEl s ix) = case eqS s s' of
                          Nothing   -> error "unIxEl"
                          Just Refl -> ix

unExS :: (EqS s) => ExS s -> s ix -> s ix
unExS (ExS s) s' = case eqS s s' of
                      Nothing   -> error "unExS"
                      Just Refl -> s
{-
exS2IxEl :: (Ix s ix) => ExS s -> ix -> IxEl s
exS2IxEl (ExS s) ix = IxEl s ix
-}

elementsM :: forall s ix. (Fam s, EqS s, El s ix, Elements (PF s) s) 
          => [ExS s] -> s ix -> Int -> [ix]
elementsM ss s = elements'
  where
    elements' :: Int -> [ix]
    elements' n = let table :: [[ [IxEl s] ]]
                      table = [ [ map (IxEl (unExS s' s) . to s)
                                    (helements (unExS s' s) (undefined :: PF s I0 ix)
                                      (\s -> map I0 . elements'' s)
                                      n)
                                | n <- [0..] ]
                              | s' <- ss]
                  in map (unIxEl s) ((table !! (index s ss)) !! n)

    elements'' :: (El s ix') => s ix' -> Int -> [ix']
    elements'' s' = case eqS s s' of
                      Nothing -> elementsM ss s'
                      Just Refl -> elements'

    index :: (EqS s) => s ix -> [ExS s] -> Int
    index = index' 0
    index' _ _ []     = error "Incorrect arguments to elementsM"
    index' n s (ExS s':t) = case eqS s s' of
                              Nothing -> index' (n + 1) s t
                              Just _  -> n
