{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE TypeFamilies         #-}

module ASTExamples where

import Data.Maybe (fromJust)
import Control.Arrow ((>>>))
import Control.Monad ((>=>))

import AST
import ASTTHUse

import Generics.MultiRec.Base

import Elements2

{-
testED = elementsM [ExS Expr, ExS Decl, ExS Var] Expr
testDD = elementsM [ExS Expr, ExS Decl, ExS Var] Decl
testVD = elementsM [ExS Expr, ExS Decl, ExS Var] Var

--test :: a -> Int -> IO ()
test g n = let t = take n [ (n, length (g n), g n) | n <- [0..]]
               io (n,a,b) = do 
                              putStr (show n ++ " (" ++ show a ++ "):")
                              sequence_ (map (\x -> putStrLn ("\t" ++ show x)) b)
                              putStrLn ""
           in sequence_ (map io t)

testE = test testED
testD = test testDD
testV = test testVD
-}

test g n = let t = take n [ (n, length (g n), g n) | n <- [0..]]
               io (n,a,b) = do 
                              putStr (show n ++ " (" ++ show a ++ "):")
                              sequence_ (map (\x -> putStrLn ("\t" ++ show x)) b)
                              putStrLn ""
           in sequence_ (map io t)
